# Milestone Project - 1

- Introduction to Warm Up Project Exercises.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/1.%20Introduction%20to%20Warm%20Up%20Project%20Exercises.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/1.%20Introduction%20to%20Warm%20Up%20Project%20Exercises.mp4)
    
- Displaying Information.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/2.%20Displaying%20Information.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/2.%20Displaying%20Information.mp4)
    
- Accepting User Input.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/3.%20Accepting%20User%20Input.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/3.%20Accepting%20User%20Input.mp4)
    
- Validating User Input.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/4.%20Validating%20User%20Input.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/4.%20Validating%20User%20Input.mp4)
    
- Simple User Interaction.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/5.%20Simple%20User%20Interaction.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/5.%20Simple%20User%20Interaction.mp4)
    
- First Python Milestone Project Overview.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/6.%20First%20Python%20Milestone%20Project%20Overview.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/6.%20First%20Python%20Milestone%20Project%20Overview.mp4)
    
- Solution Overview for MileStone Project 1 - Part One.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/8.%20Solution%20Overview%20for%20MileStone%20Project%201%20-%20Part%20One.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/8.%20Solution%20Overview%20for%20MileStone%20Project%201%20-%20Part%20One.mp4)
    
- Solution Overview for MileStone Project 1 - Part Two.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/9.%20Solution%20Overview%20for%20MileStone%20Project%201%20-%20Part%20Two.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/07.%20Milestone%20Project%20-%201/9.%20Solution%20Overview%20for%20MileStone%20Project%201%20-%20Part%20Two.mp4)
