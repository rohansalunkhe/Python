# Modules and Packages

- Pip Install and PyPi
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/09.%20Modules%20and%20Packages/1.%20Pip%20Install%20and%20PyPi.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/09.%20Modules%20and%20Packages/1.%20Pip%20Install%20and%20PyPi.mp4)
    
- Modules and Packages
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/09.%20Modules%20and%20Packages/2.%20Modules%20and%20Packages.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/09.%20Modules%20and%20Packages/2.%20Modules%20and%20Packages.mp4)
    
- __name__ and __main__
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/09.%20Modules%20and%20Packages/3.%20__name__%20and%20__main__.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/09.%20Modules%20and%20Packages/3.%20__name__%20and%20__main__.mp4)
