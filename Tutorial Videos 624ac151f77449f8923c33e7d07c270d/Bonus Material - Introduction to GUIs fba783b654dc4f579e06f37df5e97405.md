# Bonus Material - Introduction to GUIs

- Interact Functionality with GUIs
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/3.%20Interact%20Functionality%20with%20GUIs.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/3.%20Interact%20Functionality%20with%20GUIs.mp4)
    
- GUI Widget Basics
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/4.%20GUI%20Widget%20Basics.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/4.%20GUI%20Widget%20Basics.mp4)
    
- List of Possible Widgets
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/5.%20List%20of%20Possible%20Widgets.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/5.%20List%20of%20Possible%20Widgets.mp4)
    
- Widget Styling and Layouts
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/6.%20Widget%20Styling%20and%20Layouts.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/6.%20Widget%20Styling%20and%20Layouts.mp4)
    
- Example of what a Widget can do!
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/7.%20Example%20of%20what%20a%20Widget%20can%20do!.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/21.%20Bonus%20Material%20-%20Introduction%20to%20GUIs/7.%20Example%20of%20what%20a%20Widget%20can%20do!.mp4)
