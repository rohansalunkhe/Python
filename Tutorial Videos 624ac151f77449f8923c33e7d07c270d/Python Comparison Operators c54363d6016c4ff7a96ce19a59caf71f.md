# Python Comparison Operators

- Comparison Operators in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/04.%20Python%20Comparison%20Operators/1.%20Comparison%20Operators%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/04.%20Python%20Comparison%20Operators/1.%20Comparison%20Operators%20in%20Python.mp4)
    

---

- Chaining Comparison Operators in Python with Logical Operators
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/04.%20Python%20Comparison%20Operators/2.%20Chaining%20Comparison%20Operators%20in%20Python%20with%20Logical%20Operators.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/04.%20Python%20Comparison%20Operators/2.%20Chaining%20Comparison%20Operators%20in%20Python%20with%20Logical%20Operators.mp4)
