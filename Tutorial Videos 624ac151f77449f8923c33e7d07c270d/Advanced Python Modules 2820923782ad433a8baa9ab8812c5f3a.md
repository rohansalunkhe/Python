# Advanced Python Modules

- Introduction to Advanced Python Modules
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/1.%20Introduction%20to%20Advanced%20Python%20Modules.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/1.%20Introduction%20to%20Advanced%20Python%20Modules.mp4)
    
- Python Collections Module
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/2.%20Python%20Collections%20Module.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/2.%20Python%20Collections%20Module.mp4)
    
- Opening and Reading Files and Folders (Python OS Module)
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/3.%20Opening%20and%20Reading%20Files%20and%20Folders%20(Python%20OS%20Module).mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/3.%20Opening%20and%20Reading%20Files%20and%20Folders%20(Python%20OS%20Module).mp4)
    
- Python Datetime Module
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/4.%20Python%20Datetime%20Module.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/4.%20Python%20Datetime%20Module.mp4)
    
- Python Math and Random Modules
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/5.%20Python%20Math%20and%20Random%20Modules.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/5.%20Python%20Math%20and%20Random%20Modules.mp4)
    
- Python Debugger
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/6.%20Python%20Debugger.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/6.%20Python%20Debugger.mp4)
    
- Python Regular Expressions Part One
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/7.%20Python%20Regular%20Expressions%20Part%20One.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/7.%20Python%20Regular%20Expressions%20Part%20One.mp4)
    
- Python Regular Expressions Part Two
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/8.%20Python%20Regular%20Expressions%20Part%20Two.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/8.%20Python%20Regular%20Expressions%20Part%20Two.mp4)
    
- Python Regular Expressions Part Three
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/9.%20Python%20Regular%20Expressions%20Part%20Three.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/9.%20Python%20Regular%20Expressions%20Part%20Three.mp4)
    
- Timing Your Python Code
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/10.%20Timing%20Your%20Python%20Code.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/10.%20Timing%20Your%20Python%20Code.mp4)
    
- **Zipping and Unzipping files with Python**
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/11.%20Zipping%20and%20Unzipping%20files%20with%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/11.%20Zipping%20and%20Unzipping%20files%20with%20Python.mp4)
    
- Advanced Python Module Puzzle - Overview
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/12.%20Advanced%20Python%20Module%20Puzzle%20-%20Overview.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/12.%20Advanced%20Python%20Module%20Puzzle%20-%20Overview.mp4)
    
- Advanced Python Module Puzzle - Solution
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/13.%20Advanced%20Python%20Module%20Puzzle%20-%20Solution.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/14.%20Advanced%20Python%20Modules/13.%20Advanced%20Python%20Module%20Puzzle%20-%20Solution.mp4)
