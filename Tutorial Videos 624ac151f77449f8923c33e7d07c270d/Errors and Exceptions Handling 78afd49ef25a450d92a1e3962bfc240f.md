# Errors and Exceptions Handling

- Errors and Exception Handling
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/1.%20Errors%20and%20Exception%20Handling.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/1.%20Errors%20and%20Exception%20Handling.mp4)
    
- Errors and Exceptions Homework
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/2.%20Errors%20and%20Exceptions%20Homework.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/2.%20Errors%20and%20Exceptions%20Homework.mp4)
    
- Errors and Exception Homework - Solutions
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/3.%20Errors%20and%20Exception%20Homework%20-%20Solutions.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/3.%20Errors%20and%20Exception%20Homework%20-%20Solutions.mp4)
    
- Pylint Overview
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/5.%20Pylint%20Overview.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/5.%20Pylint%20Overview.mp4)
    
- Running tests with the Unittest Library
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/6.%20Running%20tests%20with%20the%20Unittest%20Library.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/10.%20Errors%20and%20Exceptions%20Handling/6.%20Running%20tests%20with%20the%20Unittest%20Library.mp4)
