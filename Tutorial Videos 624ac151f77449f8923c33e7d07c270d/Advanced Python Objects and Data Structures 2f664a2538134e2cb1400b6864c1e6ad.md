# Advanced Python Objects and Data Structures

- Advanced Numbers
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/1.%20Advanced%20Numbers.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/1.%20Advanced%20Numbers.mp4)
    
- Advanced Strings
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/2.%20Advanced%20Strings.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/2.%20Advanced%20Strings.mp4)
    
- Advanced Sets
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/3.%20Advanced%20Sets.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/3.%20Advanced%20Sets.mp4)
    
- Advanced Dictionaries
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/4.%20Advanced%20Dictionaries.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/4.%20Advanced%20Dictionaries.mp4)
    
- Advanced Lists.mp4
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/5.%20Advanced%20Lists.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/5.%20Advanced%20Lists.mp4)
    
- Advanced Python Objects Assessment Test
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/6.%20Advanced%20Python%20Objects%20Assessment%20Test.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/6.%20Advanced%20Python%20Objects%20Assessment%20Test.mp4)
    
- Advanced Python Objects Test - Solutions
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/7.%20Advanced%20Python%20Objects%20Test%20-%20Solutions.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/20.%20Advanced%20Python%20Objects%20and%20Data%20Structures/7.%20Advanced%20Python%20Objects%20Test%20-%20Solutions.mp4)
