# Working with PDFs and Spreadsheet CSV Files

- Introduction to PDFs and Spreadsheets with Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/1.%20Introduction%20to%20PDFs%20and%20Spreadsheets%20with%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/1.%20Introduction%20to%20PDFs%20and%20Spreadsheets%20with%20Python.mp4)
    
- Working with CSV Files in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/2.%20Working%20with%20CSV%20Files%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/2.%20Working%20with%20CSV%20Files%20in%20Python.mp4)
    
- Working with PDF Files in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/3.%20Working%20with%20PDF%20Files%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/3.%20Working%20with%20PDF%20Files%20in%20Python.mp4)
    
- PDFs and Spreadsheets Python Puzzle Exercise
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/4.%20PDFs%20and%20Spreadsheets%20Python%20Puzzle%20Exercise.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/4.%20PDFs%20and%20Spreadsheets%20Python%20Puzzle%20Exercise.mp4)
    
- PDFs and Spreadsheets Python Puzzle Exercise - Solutions
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/5.%20PDFs%20and%20Spreadsheets%20Python%20Puzzle%20Exercise%20-%20Solutions.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/17.%20Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files/5.%20PDFs%20and%20Spreadsheets%20Python%20Puzzle%20Exercise%20-%20Solutions.mp4)
