# Python Statements

- If Elif and Else Statements in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/1.%20If%20Elif%20and%20Else%20Statements%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/1.%20If%20Elif%20and%20Else%20Statements%20in%20Python.mp4)
    

---

- For Loops in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/2.%20For%20Loops%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/2.%20For%20Loops%20in%20Python.mp4)
    

---

- While Loops in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/3.%20While%20Loops%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/3.%20While%20Loops%20in%20Python.mp4)
    

---

- Useful Operators in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/4.%20Useful%20Operators%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/4.%20Useful%20Operators%20in%20Python.mp4)
    

---

- List Comprehensions in Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/5.%20List%20Comprehensions%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/5.%20List%20Comprehensions%20in%20Python.mp4)
    

---

- Python Statements Test Overview
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/6.%20Python%20Statements%20Test%20Overview.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/6.%20Python%20Statements%20Test%20Overview.mp4)
    

---

- Python Statements Test Solutions
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/7.%20Python%20Statements%20Test%20Solutions.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/05.%20Python%20Statements/7.%20Python%20Statements%20Test%20Solutions.mp4)
