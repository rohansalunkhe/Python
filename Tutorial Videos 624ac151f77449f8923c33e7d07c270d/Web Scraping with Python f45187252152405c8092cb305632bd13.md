# Web Scraping with Python

- Introduction to Web Scraping
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/1.%20Introduction%20to%20Web%20Scraping.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/1.%20Introduction%20to%20Web%20Scraping.mp4)
    
- Setting Up Web Scraping Libraries
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/2.%20Setting%20Up%20Web%20Scraping%20Libraries.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/2.%20Setting%20Up%20Web%20Scraping%20Libraries.mp4)
    
- Python Web Scraping - Grabbing a Title
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/3.%20Python%20Web%20Scraping%20-%20Grabbing%20a%20Title.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/3.%20Python%20Web%20Scraping%20-%20Grabbing%20a%20Title.mp4)
    
- Python Web Scraping - Grabbing a Class
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/4.%20Python%20Web%20Scraping%20-%20Grabbing%20a%20Class.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/4.%20Python%20Web%20Scraping%20-%20Grabbing%20a%20Class.mp4)
    
- Python Web Scraping - Grabbing an Image
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/5.%20Python%20Web%20Scraping%20-%20Grabbing%20an%20Image.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/5.%20Python%20Web%20Scraping%20-%20Grabbing%20an%20Image.mp4)
    
- Python Web Scraping - Book Examples Part One
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/6.%20Python%20Web%20Scraping%20-%20Book%20Examples%20Part%20One.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/6.%20Python%20Web%20Scraping%20-%20Book%20Examples%20Part%20One.mp4)
    
- Python Web Scraping - Book Examples Part Two
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/7.%20Python%20Web%20Scraping%20-%20Book%20Examples%20Part%20Two.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/7.%20Python%20Web%20Scraping%20-%20Book%20Examples%20Part%20Two.mp4)
    
- Python Web Scraping - Exercise Overview
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/8.%20Python%20Web%20Scraping%20-%20Exercise%20Overview.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/8.%20Python%20Web%20Scraping%20-%20Exercise%20Overview.mp4)
    
- Python Web Scraping - Exercise Solutions
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/9.%20Python%20Web%20Scraping%20-%20Exercise%20Solutions.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/15.%20Web%20Scraping%20with%20Python/9.%20Python%20Web%20Scraping%20-%20Exercise%20Solutions.mp4)
