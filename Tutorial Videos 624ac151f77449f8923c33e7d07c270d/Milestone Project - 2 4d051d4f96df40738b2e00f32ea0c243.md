# Milestone Project - 2

- Introduction to Milestone Project 2 Section Warmup
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/1.%20Introduction%20to%20Milestone%20Project%202%20Section%20Warmup.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/1.%20Introduction%20to%20Milestone%20Project%202%20Section%20Warmup.mp4)
    
- Solution Walkthrough - Hand and Chip Classes
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/10.%20Solution%20Walkthrough%20-%20Hand%20and%20Chip%20Classes.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/10.%20Solution%20Walkthrough%20-%20Hand%20and%20Chip%20Classes.mp4)
    
- Solution Walkthrough - Functions for Game Play
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/11.%20Solution%20Walkthrough%20-%20Functions%20for%20Game%20Play.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/11.%20Solution%20Walkthrough%20-%20Functions%20for%20Game%20Play.mp4)
    
- Solutions Walkthrough - Final Gameplay Script
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/12.%20Solutions%20Walkthrough%20-%20Final%20Gameplay%20Script.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/12.%20Solutions%20Walkthrough%20-%20Final%20Gameplay%20Script.mp4)
    
- Card Class
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/2.%20Card%20Class.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/2.%20Card%20Class.mp4)
    
- Deck Class
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/3.%20Deck%20Class.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/3.%20Deck%20Class.mp4)
    
- Player Class
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/4.%20Player%20Class.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/4.%20Player%20Class.mp4)
    
- Game Logic - Part One
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/5.%20Game%20Logic%20-%20Part%20One.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/5.%20Game%20Logic%20-%20Part%20One.mp4)
    
- Game Logic - Part Two
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/6.%20Game%20Logic%20-%20Part%20Two.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/6.%20Game%20Logic%20-%20Part%20Two.mp4)
    
- Game Logic - Part Three
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/7.%20Game%20Logic%20-%20Part%20Three.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/7.%20Game%20Logic%20-%20Part%20Three.mp4)
    
- Milestone Project 2 Overview
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/8.%20Milestone%20Project%202%20Overview.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/8.%20Milestone%20Project%202%20Overview.mp4)
    
- Solution Walkthrough - Card and Deck classes
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/9.%20Solution%20Walkthrough%20-%20Card%20and%20Deck%20classes.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/11.%20Milestone%20Project%20-%202/9.%20Solution%20Walkthrough%20-%20Card%20and%20Deck%20classes.mp4)
