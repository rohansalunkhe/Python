# PPENDIX OLDER PYTHON 2 MATERIAL

- Objects and Data Structures Assessment - Solutions
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/22.%20APPENDIX%20OLDER%20PYTHON%202%20MATERIAL/1.%20Objects%20and%20Data%20Structures%20Assessment%20-%20Solutions.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/22.%20APPENDIX%20OLDER%20PYTHON%202%20MATERIAL/1.%20Objects%20and%20Data%20Structures%20Assessment%20-%20Solutions.mp4)
    
- Comparison Operators
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/22.%20APPENDIX%20OLDER%20PYTHON%202%20MATERIAL/2.%20Comparison%20Operators.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/22.%20APPENDIX%20OLDER%20PYTHON%202%20MATERIAL/2.%20Comparison%20Operators.mp4)
    
- Chained Comparison Operators
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/22.%20APPENDIX%20OLDER%20PYTHON%202%20MATERIAL/3.%20Chained%20Comparison%20Operators.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/22.%20APPENDIX%20OLDER%20PYTHON%202%20MATERIAL/3.%20Chained%20Comparison%20Operators.mp4)
