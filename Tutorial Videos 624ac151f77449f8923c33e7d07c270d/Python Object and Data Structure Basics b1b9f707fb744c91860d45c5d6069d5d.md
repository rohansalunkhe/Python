# Python Object and Data Structure Basics

- Introduction to Python Data Type
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/1.%20Introduction%20to%20Python%20Data%20Types.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/1.%20Introduction%20to%20Python%20Data%20Types.mp4)
    

---

- Python Numbers
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/2.%20Python%20Numbers.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/2.%20Python%20Numbers.mp4)
    

---

- Variable Assignments
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/6.%20Variable%20Assignments.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/6.%20Variable%20Assignments.mp4)
    

---

- Introduction to Strings
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/7.%20Introduction%20to%20Strings.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/7.%20Introduction%20to%20Strings.mp4)
    

---

- Indexing and Slicing with Strings
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/9.%20Indexing%20and%20Slicing%20with%20Strings.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/9.%20Indexing%20and%20Slicing%20with%20Strings.mp4)
    

---

- String Properties and Methods
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/12.%20String%20Properties%20and%20Methods.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/12.%20String%20Properties%20and%20Methods.mp4)
    

---

- Print Formatting with Strings
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/15.%20Print%20Formatting%20with%20Strings.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/15.%20Print%20Formatting%20with%20Strings.mp4)
    

---

- List in Python
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/18.%20Lists%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/18.%20Lists%20in%20Python.mp4)
    

---

- Dictionaries in Python
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/22.%20Dictionaries%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/22.%20Dictionaries%20in%20Python.mp4)
    

---

- Tuples with Python
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/26.%20Tuples%20with%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/26.%20Tuples%20with%20Python.mp4)
    

---

- Sets in python
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/28.%20Sets%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/28.%20Sets%20in%20Python.mp4)
    

---

- Booleans in Python
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/30.%20Booleans%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/30.%20Booleans%20in%20Python.mp4)
    

---

- IO with Basic Files in Python
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/32.%20IO%20with%20Basic%20Files%20in%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/32.%20IO%20with%20Basic%20Files%20in%20Python.mp4)
    

---

- Python Objects and Data Structures Assessment Test Overview
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/35.%20Python%20Objects%20and%20Data%20Structures%20Assessment%20Test%20Overview.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/35.%20Python%20Objects%20and%20Data%20Structures%20Assessment%20Test%20Overview.mp4)
    

---

- Python Objects and Data Structures Assessment Test Solutions
    
    [https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/36.%20Python%20Objects%20and%20Data%20Structures%20Assessment%20Test%20Solutions.mp4](https://gitlab.com/rohansalunkhe/Python/-/raw/main/Content/03.%20Python%20Object%20and%20Data%20Structure%20Basics/36.%20Python%20Objects%20and%20Data%20Structures%20Assessment%20Test%20Solutions.mp4)
    

---
