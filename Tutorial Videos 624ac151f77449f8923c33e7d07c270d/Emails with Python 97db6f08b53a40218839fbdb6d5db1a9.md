# Emails with Python

- Introduction to Emails with Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/18.%20Emails%20with%20Python/1.%20Introduction%20to%20Emails%20with%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/18.%20Emails%20with%20Python/1.%20Introduction%20to%20Emails%20with%20Python.mp4)
    
- Sending Emails with Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/18.%20Emails%20with%20Python/2.%20Sending%20Emails%20with%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/18.%20Emails%20with%20Python/2.%20Sending%20Emails%20with%20Python.mp4)
    
- Receiving Emails with Python
    
    [https://gitlab.com/rohansalunkhe/Python/raw/main/Content/18.%20Emails%20with%20Python/3.%20Receiving%20Emails%20with%20Python.mp4](https://gitlab.com/rohansalunkhe/Python/raw/main/Content/18.%20Emails%20with%20Python/3.%20Receiving%20Emails%20with%20Python.mp4)
