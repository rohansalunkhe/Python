# Tutorial Videos

[Course Overview ](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Course%20Overview%202392279493ea4067b92b687dd335c5e2.md)

---

[Python Setup ](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Python%20Setup%20f30b20c200074b229b312c2a9e216e76.md)

---

[Python Object and Data Structure Basics](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Python%20Object%20and%20Data%20Structure%20Basics%20b1b9f707fb744c91860d45c5d6069d5d.md)

---

[Python Comparison Operators](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Python%20Comparison%20Operators%20c54363d6016c4ff7a96ce19a59caf71f.md)

---

[Python Statements](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Python%20Statements%201bb2d05c705f495a80ac235e4ec6acae.md)

---

[Methods and Functions](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Methods%20and%20Functions%2017a1bcc5269a4e46a3bac6fc0af3f463.md)

---

[Milestone Project - 1](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Milestone%20Project%20-%201%20dd30cfb6465843f38134e7ec3cdf7cf0.md)

---

[Object Oriented Programming](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Object%20Oriented%20Programming%202d459158d5c848349b936fbff14d550e.md)

---

[Modules and Packages](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Modules%20and%20Packages%20022efc5a942e4108b61ae26789cb8ed9.md)

---

[Errors and Exceptions Handling](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Errors%20and%20Exceptions%20Handling%2078afd49ef25a450d92a1e3962bfc240f.md)

---

[Milestone Project - 2](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Milestone%20Project%20-%202%204d051d4f96df40738b2e00f32ea0c243.md)

---

[Python Decorators](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Python%20Decorators%20b98bf1418d114cbca341e6bafc31f6fc.md)

---

[Python Generators](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Python%20Generators%206807556a6b26470da3f1364cc16c2855.md)

---

[Advanced Python Modules](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Advanced%20Python%20Modules%202820923782ad433a8baa9ab8812c5f3a.md)

---

[Web Scraping with Python](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Web%20Scraping%20with%20Python%20f45187252152405c8092cb305632bd13.md)

---

[Working with Images with Python](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Working%20with%20Images%20with%20Python%20d4512ad16b0b445489eee53ea55afb40.md)

---

[Working with PDFs and Spreadsheet CSV Files](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Working%20with%20PDFs%20and%20Spreadsheet%20CSV%20Files%2026034ca727594426a68c0d2458c29e83.md)

---

[Emails with Python](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Emails%20with%20Python%2097db6f08b53a40218839fbdb6d5db1a9.md)

---

[Final Capstone Python Project](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Final%20Capstone%20Python%20Project%209945f34d2d694dbbb0a6984953d73ce0.md)

---

[Advanced Python Objects and Data Structures](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Advanced%20Python%20Objects%20and%20Data%20Structures%202f664a2538134e2cb1400b6864c1e6ad.md)

---

[Bonus Material - Introduction to GUIs](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/Bonus%20Material%20-%20Introduction%20to%20GUIs%20fba783b654dc4f579e06f37df5e97405.md)

---

[PPENDIX OLDER PYTHON 2 MATERIAL](Tutorial%20Videos%20624ac151f77449f8923c33e7d07c270d/PPENDIX%20OLDER%20PYTHON%202%20MATERIAL%206d5c3854d3f24b6e88e7e788b384c96a.md)

---

- Media links
    
    [Meet Google Drive - One place for all your files](https://drive.google.com/drive/folders/18XYlDc-i4fVg3WiYhLEeF0JP1y_73K-4?usp=sharing)
    
    [2021 Complete Python Bootcamp From Zero to Hero in Python - Google Drive](https://drive.google.com/drive/folders/18XYlDc-i4fVg3WiYhLEeF0JP1y_73K-4?usp=sharing)