1
00:00:05,890 --> 00:00:06,880
Welcome back, everyone.

2
00:00:07,180 --> 00:00:12,670
And this lecture, we're going to have an overview of the PDAF and see S.V. Puzzle exercise in the next

3
00:00:12,670 --> 00:00:13,090
lecture.

4
00:00:13,150 --> 00:00:14,290
We'll go through the solutions.

5
00:00:14,740 --> 00:00:15,790
Let's check out the notebook.

6
00:00:17,090 --> 00:00:17,280
All right.

7
00:00:17,300 --> 00:00:18,500
Here I am in Jupiter.

8
00:00:18,860 --> 00:00:24,460
If you notice, under PDX and spreadsheets, there's a file called PDX Spreadsheets Puzzle that's are

9
00:00:24,470 --> 00:00:26,000
working with in the next lecture.

10
00:00:26,020 --> 00:00:27,140
We'll go over the solutions.

11
00:00:27,650 --> 00:00:30,260
Open up PDA apps and spreadsheets, puzzle exercise.

12
00:00:30,710 --> 00:00:33,680
And again, since this is the puzzle, we're not going to give you too many hints.

13
00:00:33,720 --> 00:00:35,600
But basically you have two tasks here.

14
00:00:36,170 --> 00:00:41,780
One is you need to use Python to extract the Google Drive link from the CSP file.

15
00:00:42,200 --> 00:00:47,190
And we gave you a hint that it's actually located along the diagonal from top left to bottom right.

16
00:00:47,750 --> 00:00:51,740
And where you can find the CSB file is if you look under PDAF and spreadsheets.

17
00:00:52,180 --> 00:00:54,170
There's a folder called Exercise Files.

18
00:00:55,860 --> 00:00:58,740
If you open up that folder, you'll see ACSU file.

19
00:00:58,890 --> 00:01:03,570
So what you have to do is you have to figure out how to open up this CSP file with Python.

20
00:01:04,440 --> 00:01:07,650
So you'll do that here in their task one and along a diagonal.

21
00:01:07,680 --> 00:01:11,190
Figure out how to use a python loop or something similar to extract it.

22
00:01:11,400 --> 00:01:16,860
And it should take you to this Google Drive link, then go to that Google Drive link and download the

23
00:01:16,860 --> 00:01:18,420
PDAF document from Google Drive.

24
00:01:18,840 --> 00:01:20,700
In case you can't access Google Drive.

25
00:01:20,750 --> 00:01:24,120
We already downloaded for you in this PDF file here.

26
00:01:24,900 --> 00:01:32,610
So then your task, too, is to figure out how to find a hidden phone number within this PDAF documents.

27
00:01:33,000 --> 00:01:37,680
And keep in mind, we also are not going to tell you what the correct formatting of that phone number

28
00:01:37,680 --> 00:01:38,070
is.

29
00:01:38,460 --> 00:01:44,010
So you have to use regular expressions along with what you've learned about PDA apps and scanning the

30
00:01:44,010 --> 00:01:46,620
text to figure out where that phone number is.

31
00:01:46,950 --> 00:01:51,290
There's basically an unlimited number of ways you can figure out task two.

32
00:01:51,660 --> 00:01:54,720
But at the end, you should figure out that it's this phone number right here.

33
00:01:55,710 --> 00:01:55,910
OK.

34
00:01:56,400 --> 00:01:59,250
So in the next lecture, we'll go through an example solution.

35
00:01:59,610 --> 00:02:00,120
I'll see you there.
