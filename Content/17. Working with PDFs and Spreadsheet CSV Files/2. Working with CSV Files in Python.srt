1
00:00:05,240 --> 00:00:08,960
Welcome, everyone, to this lecture on working with CSC files in Python.

2
00:00:10,240 --> 00:00:16,390
CSB stands for comma separated variables, and it's a very common output for spreadsheet programs and

3
00:00:16,390 --> 00:00:20,240
examples, CSP file would look something like this usually at the top.

4
00:00:20,260 --> 00:00:23,500
You have the actual names of the columns separated by commas.

5
00:00:23,830 --> 00:00:28,420
So here we can see some sort of employee spreadsheet where we have the name of the employee, the number

6
00:00:28,420 --> 00:00:30,760
of hours they work, and then maybe their hourly rate.

7
00:00:31,150 --> 00:00:36,880
And then each subsequent row represents a data point, such as David, who works 20 hours at a fifteen

8
00:00:36,880 --> 00:00:37,690
dollar power rate.

9
00:00:38,080 --> 00:00:39,490
And then Claire and so on.

10
00:00:39,970 --> 00:00:42,940
Note that all the columns are separated by that comma.

11
00:00:43,450 --> 00:00:49,900
And you can technically separate it by other things, like a tab or a horizontal bar or various other

12
00:00:50,140 --> 00:00:52,900
punctuation points like semicolons and only deal.

13
00:00:52,900 --> 00:00:57,430
If this will see that, we can actually specify what that separator actually is.

14
00:00:58,800 --> 00:01:04,470
They should know that while it's possible to export Excel files and Google spreadsheets to dot CSP files,

15
00:01:04,830 --> 00:01:07,110
it only exports the information.

16
00:01:07,140 --> 00:01:13,210
So that is to say things like formulas of an excel or images or general functions like macros.

17
00:01:13,560 --> 00:01:15,960
Those actually can't be within a CFC file.

18
00:01:16,350 --> 00:01:21,480
So simply put, the DOT see as we file only contains the raw data from the spreadsheet.

19
00:01:22,230 --> 00:01:26,760
In this lecture, we're gonna be working with the builtin CSP module for Python, which will allow us

20
00:01:26,760 --> 00:01:31,890
to grab columns, rows and values from that CSC file, as well as write information to add that CSP

21
00:01:31,890 --> 00:01:32,190
file.

22
00:01:32,640 --> 00:01:37,800
So our main goals here is just be able to extract information and then write information.

23
00:01:37,980 --> 00:01:41,820
And technically in between those steps, you can do whatever you want with the information within a

24
00:01:41,820 --> 00:01:42,630
Python script.

25
00:01:43,170 --> 00:01:47,980
Now, keep in mind, this is a very popular space for outside libraries that don't come builtin with

26
00:01:48,000 --> 00:01:50,070
Python, which you may want to explore.

27
00:01:50,400 --> 00:01:52,110
If you're using a particular program.

28
00:01:53,210 --> 00:01:55,830
So I just want to tell you a couple of other libraries to consider.

29
00:01:56,220 --> 00:01:58,170
One of my favorites is called Pandas.

30
00:01:58,320 --> 00:02:03,540
And this is technically a full data analysis library and it can work with almost any tabular data type.

31
00:02:03,630 --> 00:02:05,070
Not just things like CSP file.

32
00:02:05,460 --> 00:02:09,960
It also works with things like SQL files, directly off Excel files and so much more.

33
00:02:10,310 --> 00:02:13,020
It can also run visualizations and data analysis.

34
00:02:13,380 --> 00:02:14,820
It's one of my personal favorites.

35
00:02:15,060 --> 00:02:19,110
However, it is a much larger library than just something like the CSP module.

36
00:02:19,440 --> 00:02:23,820
So we teach it and various data science courses here and you can check those out if you're interested

37
00:02:23,820 --> 00:02:24,060
in it.

38
00:02:24,480 --> 00:02:28,740
The reason we don't cover it here is because it's such a huge library with so many facets to it.

39
00:02:29,070 --> 00:02:32,640
It would just take an entirely new course to really cover it in detail.

40
00:02:32,790 --> 00:02:35,400
However, it is one of my favorite Python libraries.

41
00:02:36,990 --> 00:02:39,930
Another library to consider is called Open Pie Excel.

42
00:02:40,290 --> 00:02:43,080
And this one is actually designed specifically for Excel files.

43
00:02:43,410 --> 00:02:48,690
So if you find yourself being work, in fact, sell a lot or you're an Excel power user, but you want

44
00:02:48,690 --> 00:02:50,460
to add in some python functionality.

45
00:02:50,970 --> 00:02:56,460
Open Pie Excel actually retains a lot of Excel specific functionality, things like supporting Excel

46
00:02:56,460 --> 00:02:58,230
formulas within the library.

47
00:02:58,680 --> 00:03:04,290
You can also check out Python Dash Excel dot org, which actually tracks various other Excel based Python

48
00:03:04,290 --> 00:03:04,860
libraries.

49
00:03:06,540 --> 00:03:09,240
And there's also the Google Seat's Python API.

50
00:03:09,720 --> 00:03:14,850
If you find yourself using Google spreadsheets a lot and you would like to have a direct Python interface

51
00:03:14,850 --> 00:03:15,180
for it.

52
00:03:15,450 --> 00:03:17,670
You can use this Google Sheets Python API.

53
00:03:18,150 --> 00:03:22,590
And keep in mind, this now then allows you to directly make changes to the spreadsheets that are hosted

54
00:03:22,680 --> 00:03:23,250
online.

55
00:03:23,670 --> 00:03:27,720
So you're actually running a script locally that will reach out through the Internet, through the API

56
00:03:27,720 --> 00:03:31,320
call and then edit the sheets that are hosted on your Google Drive.

57
00:03:31,800 --> 00:03:36,150
Now, the backdrop of that is that this is technically a more complex syntax than what you're going

58
00:03:36,150 --> 00:03:36,720
to see here.

59
00:03:37,110 --> 00:03:40,320
And that's also because it's available in many programming languages.

60
00:03:40,530 --> 00:03:43,590
It's not just a Python specific API.

61
00:03:43,740 --> 00:03:45,750
It's available in many programming languages.

62
00:03:45,870 --> 00:03:49,440
So if you find yourself using Google sheets a lot, you can check out the Python API.

63
00:03:49,740 --> 00:03:51,830
They actually have really good documentation online.

64
00:03:51,960 --> 00:03:55,860
Simply Google search, Google Sheets, Python API and the first link should take you there.

65
00:03:57,310 --> 00:04:02,920
OK, so even though there's lots of options here, the common factor really between all of these spreadsheet

66
00:04:02,920 --> 00:04:08,710
programs is that they should always be able to export to adopt CSP file essentially the raw data inside

67
00:04:08,710 --> 00:04:09,280
the spreadsheet.

68
00:04:09,820 --> 00:04:13,810
Let's go ahead and explore Python's built in capabilities with the CSB module.

69
00:04:14,200 --> 00:04:15,650
It's up over to Jupiter notebook.

70
00:04:17,090 --> 00:04:17,280
All right.

71
00:04:17,300 --> 00:04:19,910
Here I am inside Jupiter as a quick note.

72
00:04:20,030 --> 00:04:26,030
I highly recommend that you open up your new notebook to follow along inside of the PDX and spreadsheets

73
00:04:26,030 --> 00:04:26,480
folder.

74
00:04:26,540 --> 00:04:29,230
Because if you scroll down here, we have the lecture notebooks.

75
00:04:29,270 --> 00:04:31,640
But we also have the CSP files.

76
00:04:31,700 --> 00:04:32,750
We're going to be working with.

77
00:04:33,050 --> 00:04:37,070
And if you don't want to worry about having to provide a full file path through them, you can simply

78
00:04:37,070 --> 00:04:40,850
do like I just did and open up a new in this case, untitled notebook.

79
00:04:41,150 --> 00:04:42,050
In the same location.

80
00:04:42,950 --> 00:04:43,200
All right.

81
00:04:43,700 --> 00:04:46,790
If you ever have any questions of weird Jupiter, no book is actually located.

82
00:04:47,150 --> 00:04:47,830
You can always type.

83
00:04:47,890 --> 00:04:49,010
There'll be these in a cell.

84
00:04:49,430 --> 00:04:53,570
Run it and it will report back to you when you're actually located on your computer.

85
00:04:54,350 --> 00:04:57,320
So let's begin by showing you how to read in ACSU file.

86
00:04:57,770 --> 00:05:00,840
We're actually going to show you a very common encoding error first.

87
00:05:01,040 --> 00:05:04,370
So we will actually open this technically in the wrong way.

88
00:05:05,680 --> 00:05:11,740
So the first thing we're going to do is import CSB and then we're going to do is we have a couple of

89
00:05:11,740 --> 00:05:12,310
steps here.

90
00:05:12,550 --> 00:05:16,240
We first just open the file, just as you would have any other file.

91
00:05:16,810 --> 00:05:20,650
Then the next step is to call CSB dot reader on it.

92
00:05:21,370 --> 00:05:24,730
And then is to try to reformat it.

93
00:05:26,400 --> 00:05:29,910
Into a python object, typically the list of lists.

94
00:05:32,210 --> 00:05:35,200
So let's go ahead and try this and then we'll show you what may happen to you.

95
00:05:35,270 --> 00:05:37,810
Four in encoding error.

96
00:05:38,980 --> 00:05:40,540
OK, so first thing.

97
00:05:40,600 --> 00:05:41,290
Open the file.

98
00:05:41,440 --> 00:05:42,850
We do this just like any other file.

99
00:05:43,210 --> 00:05:45,340
So I will say data is equal to open.

100
00:05:45,760 --> 00:05:53,260
And we'll be working with example that CSP, which is located in this same PDAF and spreadsheets folder.

101
00:05:53,560 --> 00:05:54,130
It's right here.

102
00:05:54,160 --> 00:05:58,150
Example, that's ESV If you're working somewhere else, then you need to provide the full file path,

103
00:05:58,450 --> 00:06:01,690
which essentially attaching all of this in front of example.

104
00:06:01,690 --> 00:06:02,180
That's easy.

105
00:06:02,820 --> 00:06:03,020
OK.

106
00:06:03,670 --> 00:06:05,500
So open the file.

107
00:06:05,560 --> 00:06:06,370
Nothing new there.

108
00:06:06,670 --> 00:06:10,710
Now we're going to do is try to convert it into CSB data.

109
00:06:12,920 --> 00:06:22,160
By saying CSB data is equal to CSB, Reidar, and we pass in that data and then we will try to reformat

110
00:06:22,160 --> 00:06:24,500
it into a python object, lists of lists.

111
00:06:25,370 --> 00:06:29,810
So that means I'm going to say data lines is equal to.

112
00:06:30,200 --> 00:06:33,200
And I'm going to attempt to call list on CSB data.

113
00:06:34,310 --> 00:06:39,470
And you should notice an error here and the error, which is a super common error when you're working

114
00:06:39,470 --> 00:06:42,980
with various CSP files, text files or paedophiles.

115
00:06:43,340 --> 00:06:48,590
Is this Unicode decode error and essentially just says, hey, this character mapping codec, I don't

116
00:06:48,590 --> 00:06:49,220
understand it.

117
00:06:49,550 --> 00:06:50,390
I can't decode it.

118
00:06:50,870 --> 00:06:55,370
And essentially what that means is there's different encodings that a file can have.

119
00:06:55,760 --> 00:07:03,530
And what the encoding is, is basically able to read or not read different types of special characters.

120
00:07:03,890 --> 00:07:09,170
So the actual special character that's missing this particular file read up is the at Symbol.

121
00:07:09,350 --> 00:07:14,300
So this particular CSP file actually contains emails that has this at Symbol.

122
00:07:14,630 --> 00:07:16,790
And so the default encoding doesn't work.

123
00:07:17,120 --> 00:07:22,280
And you may experience the same sort of encoding problems if you're dealing with files that are written

124
00:07:22,280 --> 00:07:23,150
in another language.

125
00:07:23,420 --> 00:07:28,610
For example, if you're dealing with a Spanish written file and it has special accents over certain

126
00:07:28,610 --> 00:07:32,060
characters, you may need to use an encoding like a Latin encoding.

127
00:07:32,420 --> 00:07:37,610
And typically, you can easily Google search for the correct encodings depending on what the file is.

128
00:07:38,240 --> 00:07:43,080
So what we're gonna do here is we'll come back and we'll say this is equal to open example.

129
00:07:43,080 --> 00:07:52,460
That's ESV But I'm going to add in an encoding argument and say encoding is equal to UTF Dash eight.

130
00:07:53,030 --> 00:07:58,070
And again, you can easily search online for different encodings that may be useful to you, depending

131
00:07:58,070 --> 00:08:01,940
on what kind of files you're trying to open in what language those files are in.

132
00:08:02,450 --> 00:08:07,820
So we'd add in this UTF eight encoding, which essentially telling it what characters it can read.

133
00:08:08,570 --> 00:08:11,390
And then if we run these lines again, we don't get an error.

134
00:08:11,660 --> 00:08:17,390
So if you get this sort of Unicode decoder error, you can start looking up the different kinds of encodings.

135
00:08:17,540 --> 00:08:22,900
And it's specifically helpful if you already know what special characters are in your actual doxie as

136
00:08:22,900 --> 00:08:23,360
we file.

137
00:08:23,690 --> 00:08:24,920
I knew there were emails here.

138
00:08:25,190 --> 00:08:29,960
So that means I know there's an at symbol which is causing the error, which means I need to use UTF

139
00:08:29,960 --> 00:08:34,580
Dash eight encoding and after I recommend this one as a good starting point, especially if you're files

140
00:08:34,580 --> 00:08:35,150
in English.

141
00:08:35,910 --> 00:08:37,940
OK, so what is Dateline's?

142
00:08:38,240 --> 00:08:38,930
Let's take a look at it.

143
00:08:40,040 --> 00:08:45,350
We will say data lines and it looks like it's a list of lists.

144
00:08:45,620 --> 00:08:51,980
So the very first item are the actual column names, I.D., first name, last name, email, gender,

145
00:08:52,010 --> 00:08:53,090
IP address, city.

146
00:08:53,600 --> 00:08:56,660
And then the second item is going to be an actual data row.

147
00:08:57,080 --> 00:09:01,450
So we have Joseph and then Zanta, Allouni or excuse me, Zand Illini.

148
00:09:01,850 --> 00:09:03,560
And then we have simple machines to Oregon.

149
00:09:03,570 --> 00:09:05,900
Keep in mind, all this information is fake and made up.

150
00:09:06,420 --> 00:09:11,180
OK, so what we're gonna do here is we're going to show you how you can kind of loop through this to

151
00:09:11,180 --> 00:09:13,640
actually read in some of the data information.

152
00:09:14,090 --> 00:09:18,560
So we know that the very first item in this list, we check out zero here.

153
00:09:18,800 --> 00:09:20,180
Those are the actual column names.

154
00:09:20,450 --> 00:09:24,080
So typically CSC data, the very first row are going to be the column names.

155
00:09:25,710 --> 00:09:28,830
If you want to know how many rows there actually are, you can check the length.

156
00:09:30,150 --> 00:09:31,070
Of data lines.

157
00:09:31,670 --> 00:09:36,620
And I know there's actually a thousand rows plus one row that is the column information.

158
00:09:36,950 --> 00:09:38,480
So there's 1000 data points.

159
00:09:38,660 --> 00:09:43,940
So one of these these rows right here is very first one, which is the column names technically not

160
00:09:43,940 --> 00:09:44,510
a data point.

161
00:09:45,050 --> 00:09:54,260
And what we could do as well is say for line in data lines and we can say starting from the beginning,

162
00:09:54,290 --> 00:09:56,660
go all the way up to, let's say, row number five.

163
00:09:57,290 --> 00:09:58,940
Go ahead and print the line.

164
00:09:59,100 --> 00:10:02,250
And I want to zoom out here just a little bit so we can see this formatted nicely.

165
00:10:02,750 --> 00:10:04,430
And we should be able to see something like this.

166
00:10:04,460 --> 00:10:09,290
And if I zoom out a little more, I can see now the rows formatted nicely.

167
00:10:11,990 --> 00:10:13,190
So what can I do if this.

168
00:10:13,340 --> 00:10:18,560
Well, I now know that I can essentially extract any Roe I want, for example, if I want to wanted

169
00:10:18,710 --> 00:10:21,860
Roe number ten, I simply need to zoom back in here.

170
00:10:23,630 --> 00:10:26,130
To see row number 10, I simply need to say Dateline's.

171
00:10:27,380 --> 00:10:30,140
And then you have to kind of figure out what number is correct here.

172
00:10:30,650 --> 00:10:35,120
Recall that zero is going to be the column names.

173
00:10:35,570 --> 00:10:42,410
So that means if I were to provide 10, it actually works out nicely because zero indexing starts off

174
00:10:42,470 --> 00:10:48,470
at zero in Python, which means that very first item corresponds to the column header.

175
00:10:48,740 --> 00:10:51,500
So that index one is actually your very first data point.

176
00:10:51,650 --> 00:10:52,640
So then so on.

177
00:10:53,170 --> 00:10:55,610
Index ten is actually the tenth data point.

178
00:10:56,390 --> 00:10:56,570
All right.

179
00:10:56,990 --> 00:10:58,790
So I know how to extract a row.

180
00:10:59,180 --> 00:11:01,310
What if I wanted to extract a single value?

181
00:11:01,430 --> 00:11:03,320
Well, then I simply say, what do I want?

182
00:11:03,740 --> 00:11:06,110
And then here, notice this is a list.

183
00:11:06,230 --> 00:11:08,630
So I can maybe grab this person's email.

184
00:11:08,910 --> 00:11:09,730
So that's zero.

185
00:11:09,800 --> 00:11:10,970
One, two, three.

186
00:11:12,950 --> 00:11:16,210
And now I can grab the email of this particular person.

187
00:11:19,220 --> 00:11:20,030
Let's imagine that.

188
00:11:20,090 --> 00:11:21,890
I want an entire column.

189
00:11:22,310 --> 00:11:26,240
So, for example, let's say I want to just all the e-mails in every single row.

190
00:11:26,450 --> 00:11:27,140
How can I do that?

191
00:11:27,650 --> 00:11:31,400
Well, I know that set index three, so I could simply say.

192
00:11:32,660 --> 00:11:34,040
My list of all emails.

193
00:11:35,020 --> 00:11:36,670
Physical to an empty list so far.

194
00:11:37,330 --> 00:11:41,320
And then I'll go for every line and data lines.

195
00:11:41,650 --> 00:11:43,090
Or you could take it for a subset.

196
00:11:44,080 --> 00:11:46,640
Recall that at this point you should be starting at 1:00.

197
00:11:46,750 --> 00:11:49,570
Since you don't actually want to grab that e-mail column name.

198
00:11:50,030 --> 00:11:51,970
And let's go out to row fifteen here.

199
00:11:52,030 --> 00:11:53,080
We could go all the way to the end.

200
00:11:54,160 --> 00:11:56,560
We'll simply say all emails append.

201
00:11:57,720 --> 00:12:00,570
And then we'll say line index three.

202
00:12:01,110 --> 00:12:05,920
So all we're saying here is go through every row and make sure you start at one.

203
00:12:06,030 --> 00:12:08,850
If you want, you can go all the way to the end to grab everything.

204
00:12:09,330 --> 00:12:11,790
And then we'll say all emails, a pen and every line.

205
00:12:12,300 --> 00:12:14,610
Recall index three is the email.

206
00:12:15,300 --> 00:12:17,040
So if you run this, you should notice.

207
00:12:17,040 --> 00:12:22,800
Now, all emails is just this giant list of all the emails inside that data set.

208
00:12:24,820 --> 00:12:29,740
Now, you notice that, let's say for one of these lines, let's go ahead and grab one.

209
00:12:29,770 --> 00:12:30,400
So we'll say.

210
00:12:31,360 --> 00:12:33,690
Data lines just grab 10 again.

211
00:12:34,470 --> 00:12:38,490
You'll notice that right now the first name and last name are separated.

212
00:12:38,940 --> 00:12:43,980
Let's imagine we wanted a list of the full names because we want to maybe automate an email to them.

213
00:12:44,430 --> 00:12:45,930
And we want to address them by their full name.

214
00:12:46,440 --> 00:12:48,210
Well, that's actually pretty easy to do.

215
00:12:48,610 --> 00:12:53,750
All you do is very similar, say full names, empty list.

216
00:12:54,660 --> 00:12:56,910
And then when we iterate, we just add in a little bit of logic.

217
00:12:57,060 --> 00:13:01,660
We say for line and data lines starting at 1:00.

218
00:13:01,740 --> 00:13:02,940
Let's say go all the way to the end.

219
00:13:04,200 --> 00:13:07,080
We can say full names and we're going to a pen.

220
00:13:07,560 --> 00:13:08,760
And this where you can do the logic.

221
00:13:09,270 --> 00:13:13,020
So I know that index one is the first name.

222
00:13:13,740 --> 00:13:19,440
This index zero is an I.D. and then I'm going to simply concatenate it with, let's say.

223
00:13:20,780 --> 00:13:21,400
Line two.

224
00:13:21,710 --> 00:13:24,770
However, this would leave no space between the first name and last name.

225
00:13:25,340 --> 00:13:31,010
So let's go ahead and concatenate it one more time in between with an empty space, just so there's

226
00:13:31,010 --> 00:13:33,110
a space between the first name and the last name.

227
00:13:33,950 --> 00:13:35,720
You run that and now you have.

228
00:13:36,930 --> 00:13:38,250
A list of the full names.

229
00:13:40,100 --> 00:13:44,510
So now you can see a simple index saying you can pretty much grab any information you want.

230
00:13:45,380 --> 00:13:48,650
So we're going to do now is review how you can write to a CSP file.

231
00:13:49,370 --> 00:13:56,150
So the way we're going to do this is we're going to say file underscore to underscore.

232
00:13:56,900 --> 00:13:59,240
Output is equal to open.

233
00:14:00,020 --> 00:14:03,500
And there's where you can decide what the name of your CSC file is going to be.

234
00:14:04,040 --> 00:14:08,900
So let's go ahead and call this to save file that CSB.

235
00:14:09,550 --> 00:14:14,030
And you should note that the way we're gonna set it up here is this will overwrite any existing file

236
00:14:14,030 --> 00:14:15,110
with the same name.

237
00:14:15,530 --> 00:14:19,820
And you can always change your mode depending on what you want to do here.

238
00:14:20,480 --> 00:14:26,570
So in this case, I'm going to say comma mode is equal to W, which is right mode.

239
00:14:26,840 --> 00:14:30,740
And then this will also overwrite any existing CSG file, the same name.

240
00:14:31,280 --> 00:14:37,340
And then the last thing we have to do is mean to specify that new line is an empty string.

241
00:14:37,490 --> 00:14:38,600
Note there's no space here.

242
00:14:38,900 --> 00:14:41,030
It's just one quote followed by another quote.

243
00:14:43,740 --> 00:14:52,160
Then we're going to hit enter here and then we're going to say CSB underscore writer is equal to CSB.

244
00:14:52,880 --> 00:14:55,710
The writer, essentially opposite of CSB thought reader.

245
00:14:56,660 --> 00:14:58,920
And we call file to output.

246
00:14:59,310 --> 00:15:02,670
And then we have to specify the delimiter that we want.

247
00:15:03,180 --> 00:15:08,370
Delimiter is just another word for what is a separator, what is actually separating one column from

248
00:15:08,370 --> 00:15:08,790
another.

249
00:15:09,210 --> 00:15:10,500
And you can say it's a comma.

250
00:15:10,980 --> 00:15:13,710
Keep in mind, this can technically be anything you want it to be.

251
00:15:14,010 --> 00:15:17,850
It's also really common to have semicolons as a separator.

252
00:15:18,030 --> 00:15:20,600
And it's even common to have what's sometimes known as a T.

253
00:15:20,670 --> 00:15:26,070
S V file or tab separated, which you can denote with backslash T for a tab.

254
00:15:26,580 --> 00:15:27,900
We will keep it as comma.

255
00:15:28,230 --> 00:15:33,620
The last thing to know is CSB writer is essentially the sister to CSB reader.

256
00:15:33,960 --> 00:15:37,500
And if you're opening a C as we file that has a different separator than a comma.

257
00:15:37,800 --> 00:15:44,340
You could have also specified the delimiter parameter when we called up here CSP dot reader.

258
00:15:44,790 --> 00:15:49,470
And you can check out the online documentation for the other arguments you can pass on to CSB.

259
00:15:50,400 --> 00:15:54,150
OK, so we're going to do is write this file to Output Delimiter.

260
00:15:54,960 --> 00:15:56,310
We go ahead and run this.

261
00:15:57,420 --> 00:15:59,130
And then there's different ways we can do this.

262
00:15:59,310 --> 00:16:03,840
So, for example, I can write a single row so I can say C as V writer.

263
00:16:05,380 --> 00:16:05,850
Dot.

264
00:16:06,370 --> 00:16:07,080
Right, row.

265
00:16:07,750 --> 00:16:15,190
And they just passed in a single row of items like A, B, C.

266
00:16:18,200 --> 00:16:25,640
Or what I could do is write multiple rows so I could say CSB writer Dot right, Rose.

267
00:16:28,270 --> 00:16:31,330
And then just takes in a list of lists and keep mine.

268
00:16:31,600 --> 00:16:35,830
This time it has to actually match up with the rows that are already there.

269
00:16:37,300 --> 00:16:41,620
So if there's three columns already, the next entries should also have just three values.

270
00:16:41,950 --> 00:16:43,030
So I'll say one, two, three.

271
00:16:43,750 --> 00:16:46,330
And then let's say four, five, six.

272
00:16:46,390 --> 00:16:51,130
Notice, these are strings that are being written here and you keep writing rows that way.

273
00:16:51,370 --> 00:16:56,500
So you can imagine that if I really wanted to, I could make adjustments to the data lines here.

274
00:16:56,950 --> 00:16:59,590
And then eventually just say CSB writer that right.

275
00:16:59,590 --> 00:17:00,830
Rose Dateline's.

276
00:17:01,480 --> 00:17:04,570
And you can check out the different modes from before.

277
00:17:04,600 --> 00:17:07,630
So if you wanted to append to a file, you can do that as well.

278
00:17:07,940 --> 00:17:12,940
If you want to just read a file, then that's just mode are as we saw above, but all the modes.

279
00:17:12,940 --> 00:17:16,720
So we previously discussed of opening text files with Python apply here.

280
00:17:17,620 --> 00:17:18,940
So we're just doing an overwrite.

281
00:17:18,970 --> 00:17:20,440
So we're writing ABC.

282
00:17:20,770 --> 00:17:21,700
Those are my column names.

283
00:17:21,730 --> 00:17:22,690
And then here's my data.

284
00:17:22,740 --> 00:17:23,770
One, two, three is one row.

285
00:17:24,100 --> 00:17:25,120
Then four, five, six.

286
00:17:25,150 --> 00:17:27,700
And we have to remember that we need to actually close the file.

287
00:17:28,030 --> 00:17:30,040
So you don't close the file through CSP writer.

288
00:17:30,070 --> 00:17:32,230
That's just writing to the file to close file.

289
00:17:32,350 --> 00:17:34,540
You actually Graham file to output again.

290
00:17:36,190 --> 00:17:42,880
And then you go ahead and close it in now, you should be able to come over here and see you have to

291
00:17:42,880 --> 00:17:45,640
save file that C as V as created.

292
00:17:46,210 --> 00:17:47,170
Now, let's imagine that.

293
00:17:47,530 --> 00:17:51,610
I've already worked with this file, and instead of overwriting it, I just want to add Neuros to it.

294
00:17:51,970 --> 00:17:54,550
As I mention, this all works of just editing the mode.

295
00:17:55,060 --> 00:17:56,920
So only to do here is say F.

296
00:17:58,200 --> 00:18:04,860
Is equal to open, call that same file to save file that CSB and then the mode we're going to have here

297
00:18:04,860 --> 00:18:08,430
is a for a pen, which essentially means I don't want to overwrite this file.

298
00:18:08,520 --> 00:18:09,810
I just want to append to it.

299
00:18:10,350 --> 00:18:18,300
So we'll say new line is equal to empty string again and we'll say CSB writer.

300
00:18:20,710 --> 00:18:24,100
Is equal to CSB dot writer F.

301
00:18:25,030 --> 00:18:27,670
So we have this writer object and then I can write Neuros to it.

302
00:18:28,180 --> 00:18:30,250
So I simply need to say writer.

303
00:18:32,130 --> 00:18:33,690
Let's go ahead and write a single row.

304
00:18:34,110 --> 00:18:37,170
Let's just have this be one, one, one.

305
00:18:38,380 --> 00:18:40,180
Or one, two, three, again, doesn't really matter here.

306
00:18:41,560 --> 00:18:44,110
And then you should get an output here, essentially telling you.

307
00:18:46,090 --> 00:18:51,040
The number of characters written, so recall that we're not just writing one, two, three, but we're

308
00:18:51,040 --> 00:18:53,440
also writing commas for them as well as a new line.

309
00:18:54,100 --> 00:18:57,160
And then finally, when you're done with this file, you can close it as before.

310
00:18:58,660 --> 00:18:58,940
OK.

311
00:18:59,110 --> 00:19:01,240
So that's the basics of working of CSB file.

312
00:19:01,630 --> 00:19:06,640
Technically, there's usually just three things you do if a CSP file you read in the information and

313
00:19:06,640 --> 00:19:08,560
then you do whatever you want with it as a second part.

314
00:19:08,890 --> 00:19:12,970
And then once you're done, you can either write that information back or just save it somewhere within

315
00:19:13,360 --> 00:19:15,460
your Python script as a python object.

316
00:19:15,610 --> 00:19:22,240
So you open and read a C, we file SEP to do whatever you want with the information of Python set three.

317
00:19:22,330 --> 00:19:24,910
You can write to a new file or write to an existing file.

318
00:19:25,630 --> 00:19:25,990
Thanks.

319
00:19:26,080 --> 00:19:27,130
And we'll see you at the next lecture.
