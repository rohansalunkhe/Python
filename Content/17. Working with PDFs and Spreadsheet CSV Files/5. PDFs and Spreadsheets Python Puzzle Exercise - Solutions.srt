1
00:00:05,300 --> 00:00:06,290
Welcome back, everyone.

2
00:00:06,620 --> 00:00:12,050
In this lecture, we're gonna go through an example solution for the PDAF and CSP file puzzle exercise

3
00:00:12,050 --> 00:00:12,560
question.

4
00:00:12,950 --> 00:00:14,510
Let's head over to a Jupiter notebook.

5
00:00:14,900 --> 00:00:15,110
OK.

6
00:00:15,200 --> 00:00:17,210
Here I am inside of the Jupiter notebook.

7
00:00:17,300 --> 00:00:18,810
And let's start with the first task.

8
00:00:18,950 --> 00:00:20,120
Just going to zoom in here.

9
00:00:20,240 --> 00:00:21,920
We can see it a little better.

10
00:00:22,580 --> 00:00:27,350
And the first task is to grab the Google Drive link from the CSB file and we get the hint that it's

11
00:00:27,350 --> 00:00:29,060
located along the diagonal.

12
00:00:29,630 --> 00:00:30,530
So how do we do this?

13
00:00:31,070 --> 00:00:32,300
Well, we first have to open this up.

14
00:00:32,300 --> 00:00:34,250
So we'll say data is equal to open.

15
00:00:34,880 --> 00:00:37,700
And recall, it's underneath the exercise files.

16
00:00:37,790 --> 00:00:40,840
And it's called Find the link that CSB.

17
00:00:41,810 --> 00:00:48,920
And eventually you'll discover they should have encoding equal to UTF Dash eight.

18
00:00:49,950 --> 00:00:51,900
Because of the possible different symbols here.

19
00:00:52,380 --> 00:00:54,120
And this should be a national equal sign.

20
00:00:54,780 --> 00:00:55,260
There we go.

21
00:00:55,860 --> 00:01:02,460
And next, we're going to say a CSB data is equal to see US v. dot reader on that data.

22
00:01:02,790 --> 00:01:07,950
And to make sure we can use CSP that reader, I should import CSG.

23
00:01:09,360 --> 00:01:10,200
So we'll go ahead and run.

24
00:01:10,200 --> 00:01:11,730
That looks like it worked.

25
00:01:11,890 --> 00:01:13,590
Let's go ahead and check out the data lines.

26
00:01:15,110 --> 00:01:17,540
By saying list the CSB data.

27
00:01:19,500 --> 00:01:20,880
And confirmed that that worked.

28
00:01:21,420 --> 00:01:23,520
And it looks like we have the list of lists.

29
00:01:23,970 --> 00:01:25,560
So what do we need to actually grab here?

30
00:01:26,010 --> 00:01:30,270
It's a long diagonal from the top left to the bottom right.

31
00:01:30,780 --> 00:01:32,190
So if we think about this.

32
00:01:33,640 --> 00:01:37,750
It means that the first letter is the first item in the first row.

33
00:01:38,290 --> 00:01:42,430
Then the second letter of the link should be the second item in the second row and so on.

34
00:01:43,030 --> 00:01:47,800
Which means I can easily solve this by doing something like the following, because if I take a look

35
00:01:47,800 --> 00:01:49,450
at Dateline's zero.

36
00:01:50,770 --> 00:01:52,210
Actually want item zero?

37
00:01:52,630 --> 00:01:54,340
If I take a look at Dateline's.

38
00:01:55,350 --> 00:02:01,570
One, I should want item one from this, and then two, I want item two.

39
00:02:01,640 --> 00:02:03,840
So notice it's starting to spell HTP.

40
00:02:04,710 --> 00:02:07,680
So let's create a simple for loop that should be able to solve this for us.

41
00:02:08,820 --> 00:02:13,950
I'll start off by doing this as a empty string and then I will just concatenate to that string.

42
00:02:14,700 --> 00:02:21,180
So then I will say for row number, comma data in.

43
00:02:21,780 --> 00:02:24,150
And here's the trick I'm going to use enumerate.

44
00:02:25,540 --> 00:02:26,830
On data lines.

45
00:02:28,450 --> 00:02:31,210
I will say my link string.

46
00:02:32,690 --> 00:02:36,490
And then I will take the current link string and simply concatenate it with data.

47
00:02:38,030 --> 00:02:45,890
At that road number, so recall data itself is going to be the row row number is the current number

48
00:02:46,070 --> 00:02:46,940
of the row we're on.

49
00:02:47,420 --> 00:02:56,490
So this will essentially say row zero, row number zero, row to row numbers two and so on which side

50
00:02:56,490 --> 00:02:58,040
want to use as indexing here.

51
00:02:58,610 --> 00:03:04,190
So if we run this, I should then be able to see the full link as a stream.

52
00:03:04,870 --> 00:03:05,140
OK.

53
00:03:05,540 --> 00:03:10,940
And the reason we're able to do this is because it's specifically the diagonal is from the top left

54
00:03:11,000 --> 00:03:12,920
to the bottom right of the CSP file.

55
00:03:13,310 --> 00:03:15,830
And you'll notice that it's a kind of a square see as we file.

56
00:03:16,160 --> 00:03:18,350
There are as many columns as there are rows.

57
00:03:18,740 --> 00:03:21,560
And it's specifically designed that way because it's a puzzle.

58
00:03:22,460 --> 00:03:25,880
So if you go to this link, you should then be able to download the PDA file.

59
00:03:26,210 --> 00:03:30,470
We also already download it for you, just in case you weren't able to download it yourself.

60
00:03:32,960 --> 00:03:34,010
So we're going to do here.

61
00:03:35,510 --> 00:03:38,690
Is staffed by importing Pi pdaf to.

62
00:03:41,510 --> 00:03:46,980
And then we'll say F is equal to open underneath.

63
00:03:47,120 --> 00:03:49,190
Exercise files are going to open.

64
00:03:49,910 --> 00:03:51,950
Find the phone number that PEF.

65
00:03:52,930 --> 00:03:54,190
We've read binary.

66
00:03:55,830 --> 00:04:00,300
And then we'll say PDAF is equal to May call pie PDAF, too.

67
00:04:00,960 --> 00:04:04,530
And since I just want to read this will save PDAF Reader.

68
00:04:05,800 --> 00:04:08,830
On that file, the check, the number of pages.

69
00:04:10,040 --> 00:04:11,720
So the number of pages it has.

70
00:04:11,780 --> 00:04:14,000
Remember, that's an attribute is 17 pages.

71
00:04:14,330 --> 00:04:20,510
So somewhere on these pages is text that I eventually want to do a regular express and search on because

72
00:04:20,510 --> 00:04:22,070
I need to find the phone number.

73
00:04:22,490 --> 00:04:24,920
And we didn't give you a hint that this is what the phone number should look like.

74
00:04:25,250 --> 00:04:29,060
But we didn't actually tell you what the actual formatting was.

75
00:04:29,160 --> 00:04:32,060
So note, there are different ways of formatting a phone number.

76
00:04:32,480 --> 00:04:39,000
So I actually don't know if the phone number is going to have, for instance, dashes or if it's going

77
00:04:39,000 --> 00:04:44,660
to have periods between it or if there's going to be parentheses here in this fashion.

78
00:04:44,810 --> 00:04:50,270
So what I need to do is first figure out what the actual format of the number is.

79
00:04:50,510 --> 00:04:55,130
And then what I can do is start trying to figure out how to actually grab it with regular expressions.

80
00:04:55,730 --> 00:05:01,610
Something I do know, though, is that regardless of how this is actually formatted, it's highly likely

81
00:05:01,910 --> 00:05:04,520
that there's gonna be three digits in a row somewhere.

82
00:05:04,970 --> 00:05:09,350
So I should probably start searching for three digits in a row and then the remaining text after that.

83
00:05:09,500 --> 00:05:12,560
And that way I can begin to discover how this is actually formatted.

84
00:05:13,490 --> 00:05:17,310
So, again, it's come an unlimited amount of ways when you're dealing with regular expressions that

85
00:05:17,330 --> 00:05:18,380
you can search for this.

86
00:05:18,830 --> 00:05:25,250
But what I'm going to do is start off by inputting regular expressions and we'll say pattern is equal

87
00:05:25,250 --> 00:05:25,650
to.

88
00:05:25,730 --> 00:05:30,890
And let's start off with just a very simple pattern of three digits in a row.

89
00:05:32,220 --> 00:05:36,060
So we run that and now let's grab all the text within this document.

90
00:05:36,660 --> 00:05:37,440
So we'll say.

91
00:05:39,230 --> 00:05:40,040
All text.

92
00:05:41,410 --> 00:05:46,350
Is equal to and what I'll have here is just a string, and eventually it's gonna be a giant string of

93
00:05:46,360 --> 00:05:51,300
hollow text and we'll say for N in range PDAF.

94
00:05:52,410 --> 00:05:53,340
NUM pages.

95
00:05:54,950 --> 00:06:00,070
Say page is equal to PDAF, get page at that number.

96
00:06:01,350 --> 00:06:03,660
Say, page text.

97
00:06:05,160 --> 00:06:06,480
Is equal to page that.

98
00:06:07,990 --> 00:06:09,100
Extract, text.

99
00:06:10,330 --> 00:06:12,160
And then what I'm going to do is say.

100
00:06:13,620 --> 00:06:24,000
All text is equal to the current all text, and I'll add an empty space and then all concatenate page

101
00:06:24,000 --> 00:06:24,840
text itself.

102
00:06:25,380 --> 00:06:26,250
So what does that do?

103
00:06:26,430 --> 00:06:29,400
Well, if I run this, it's going to take a little bit of time.

104
00:06:29,430 --> 00:06:33,720
But now I have a giant string with all the text here.

105
00:06:34,410 --> 00:06:37,980
And that means I can't begin to actually use regular expression search.

106
00:06:38,200 --> 00:06:41,790
And if I wanted to, I could search the pages in this loop over and over again.

107
00:06:42,120 --> 00:06:45,510
It's really up to you how you can basically perform this task.

108
00:06:45,930 --> 00:06:49,650
So let's start actually looking for a pattern within all text.

109
00:06:50,250 --> 00:06:55,080
And if you take a look at the lecture notebook that goes along with this, we actually have a hint link

110
00:06:55,440 --> 00:07:01,200
that takes you to this stack overflow page that asks, how can I find all the matches to a regular expression

111
00:07:01,200 --> 00:07:01,890
in Python?

112
00:07:02,430 --> 00:07:08,040
Unfortunately, if we try something like search, that only brings up the very first match.

113
00:07:08,400 --> 00:07:15,900
So if I research for my pattern of three numbers and I want to search all the text notice, I'll just

114
00:07:15,900 --> 00:07:21,150
get back one match and it's actually zero zero zero, which given what we actually know is not the correct

115
00:07:21,150 --> 00:07:21,570
number.

116
00:07:21,990 --> 00:07:27,180
Which means I need to find multiple matches because I don't know if there's maybe page numbers here

117
00:07:27,210 --> 00:07:30,570
that are three in a row or some other reference that could be messing this up.

118
00:07:31,170 --> 00:07:32,460
So how do we find all the matches?

119
00:07:32,880 --> 00:07:33,810
We have two methods.

120
00:07:34,080 --> 00:07:40,020
Find all, which will only return the matching strings and then find itor, which will return over the

121
00:07:40,020 --> 00:07:40,920
match objects.

122
00:07:41,370 --> 00:07:42,750
So let's go ahead and check that out.

123
00:07:43,260 --> 00:07:45,180
If I were to say find all.

124
00:07:46,190 --> 00:07:51,490
For my three numbers, I can begin to see that later on in the text, my three numbers start appearing

125
00:07:51,500 --> 00:07:56,570
five five five oh three in the first three digits of the last part of that phone number.

126
00:07:56,960 --> 00:08:01,970
But what I really want to do is figure out how can I get to something like five or five and then figure

127
00:08:01,970 --> 00:08:03,590
out what's the remaining text.

128
00:08:04,010 --> 00:08:06,160
So to do that, we're gonna do the following.

129
00:08:06,170 --> 00:08:07,640
We'll say find itor.

130
00:08:08,640 --> 00:08:11,320
And we'll say for match in.

131
00:08:12,840 --> 00:08:19,150
R e find itor since this an iteration is going to return these match objects, we'll go ahead and just

132
00:08:19,150 --> 00:08:19,540
say.

133
00:08:21,000 --> 00:08:21,660
Print out.

134
00:08:22,700 --> 00:08:23,390
My match.

135
00:08:24,320 --> 00:08:28,550
And if we run that, then we can begin to see the actual locations of these.

136
00:08:28,940 --> 00:08:32,590
So let's go ahead and grab the location of five or five.

137
00:08:33,620 --> 00:08:36,740
So I'm gonna run this again so I can see all text.

138
00:08:38,290 --> 00:08:40,590
And let's start at that index location.

139
00:08:41,290 --> 00:08:48,550
And let's say we'll go up to that index location plus let's say 20 more characters.

140
00:08:49,060 --> 00:08:54,310
So you run that and you can begin to see five five five oh three four four five five.

141
00:08:54,670 --> 00:08:59,740
And if you want a little bit more context, we can go ahead and maybe grab a little bit before that

142
00:09:00,160 --> 00:09:03,740
to a little bit after run that we can begin to see something.

143
00:09:03,750 --> 00:09:05,380
It says like phone number is.

144
00:09:05,470 --> 00:09:06,220
And so on.

145
00:09:06,670 --> 00:09:10,690
So here I can finally see the way the phone number is formatted.

146
00:09:10,840 --> 00:09:16,390
It looks like there's actually dots in between it, which means I can either just say my problem is

147
00:09:16,390 --> 00:09:23,370
solved and grab the issue here, or I could come back up here and edit my pattern to say something like

148
00:09:23,470 --> 00:09:25,330
three dots.

149
00:09:26,540 --> 00:09:31,210
D4, run that again and then instead of this.

150
00:09:31,240 --> 00:09:34,810
I just need to say, run that once and I can see the full match.

151
00:09:34,910 --> 00:09:38,140
Five five five three four four five five.

152
00:09:38,530 --> 00:09:42,670
And if you're curious about what the sexual phone number is, you can check out the lecture notebook

153
00:09:42,730 --> 00:09:43,690
that goes along with this.

154
00:09:43,840 --> 00:09:44,740
It's actually from the show.

155
00:09:44,770 --> 00:09:45,670
Better call Saul.

156
00:09:46,270 --> 00:09:49,420
And it's the phone number for his fake law services there.

157
00:09:49,990 --> 00:09:51,370
OK, thanks.

158
00:09:51,460 --> 00:09:54,280
And if you have any questions, feel free to post the Q&amp;A forums.

159
00:09:54,580 --> 00:09:55,090
I'll see you there.
