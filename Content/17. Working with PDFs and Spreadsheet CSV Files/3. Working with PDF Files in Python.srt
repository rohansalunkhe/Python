1
00:00:05,460 --> 00:00:09,300
Welcome, everyone, to this lecture on working with PDAF files with Python.

2
00:00:10,540 --> 00:00:15,680
PDAF stands for portable document format, and it was developed by Adobe in the nineteen nineties,

3
00:00:15,730 --> 00:00:21,160
which is probably why when you think of PDAF you think of Adobes PDAF Reader or another Adobe program.

4
00:00:21,760 --> 00:00:27,190
Now the most important thing to keep in mind is that while PDX share the same extension dot pdaf and

5
00:00:27,190 --> 00:00:32,310
can be viewed in multiple PDAF readers, many PDA apps are not machine readable through Python.

6
00:00:33,770 --> 00:00:39,830
Since PDX mainly encapsulate and then display a fixed layout, flat document, there is sometimes no

7
00:00:39,830 --> 00:00:41,450
machine readable standard format.

8
00:00:41,660 --> 00:00:47,060
Unlike CSB files, which pretty much once you have a CSP file in place, Python can easily read it.

9
00:00:47,720 --> 00:00:49,990
So unfortunately, that means that a PDA.

10
00:00:50,060 --> 00:00:55,100
That was, for instance, simply scanned is highly unlikely to be readable by Python.

11
00:00:56,500 --> 00:01:01,510
Additions to P.D.A, such as images, tables, format adjustments, or even things like simple font

12
00:01:01,510 --> 00:01:04,900
changes, can also render a PDAF unreadable by Python.

13
00:01:05,500 --> 00:01:11,290
Now, there are many paid PDAF programs outside of Python that can read and extract from these specialized

14
00:01:11,290 --> 00:01:11,830
files.

15
00:01:12,160 --> 00:01:17,920
But we're going to be focusing on free open source software such as the extremely popular PI PDAF to

16
00:01:17,950 --> 00:01:18,490
library.

17
00:01:20,060 --> 00:01:24,980
Now, we've made sure that the paedophiles included in this coarse material are readable by Pi PDAF

18
00:01:24,990 --> 00:01:25,310
too.

19
00:01:25,970 --> 00:01:31,400
Unfortunately, we can't offer assistance for your own personal paedophiles if they are not readable

20
00:01:31,430 --> 00:01:32,440
by pipe, too.

21
00:01:33,110 --> 00:01:37,640
So basically, if you have your own paedophile and you try to follow along what we've done here, and

22
00:01:37,640 --> 00:01:42,500
for some reason it shows up as empty texts, like there's nothing in the PDAF file when you know there's

23
00:01:42,500 --> 00:01:47,580
something in there that basically means that typedef two will not be able to read your file, unfortunately.

24
00:01:47,850 --> 00:01:50,630
And there really isn't much we can do on our end to help out.

25
00:01:52,190 --> 00:01:56,330
OK, so let's see what we can do with paedophiles in Python.

26
00:01:56,720 --> 00:02:02,450
Remember your first going to need to install pipe PDAF to your command line with PIP install pipe PDAF

27
00:02:02,450 --> 00:02:02,810
two.

28
00:02:02,960 --> 00:02:04,490
And note the capitalization here.

29
00:02:05,240 --> 00:02:07,370
OK, let's head over to Jupiter notebook.

30
00:02:07,970 --> 00:02:08,170
All right.

31
00:02:08,180 --> 00:02:09,800
Here I am at the Jupiter notebook.

32
00:02:10,160 --> 00:02:15,920
A couple of things I wanted to point out is, again, we recommend that you open up this new notebook

33
00:02:15,980 --> 00:02:21,380
in the same folder, PDX and spreadsheets, so you can actively grab this PDAF documents we're gonna

34
00:02:21,380 --> 00:02:22,070
be working with.

35
00:02:22,550 --> 00:02:23,780
So we'll come back to the notebook.

36
00:02:23,870 --> 00:02:28,610
And the second thing I want to mention is the lecture notebook that goes along with this called Working

37
00:02:28,610 --> 00:02:34,250
of Paedophiles has a link in the very top to other Python libraries that you may be interested in.

38
00:02:34,460 --> 00:02:36,230
If you find yourself working of PBS a lot.

39
00:02:36,800 --> 00:02:42,710
So that link basically has a survey of tools that allow you to do things like read from specialized

40
00:02:42,790 --> 00:02:43,700
PDA files.

41
00:02:44,030 --> 00:02:48,920
So if you ever have problems with Piper, you have to you may want to explore these other libraries.

42
00:02:49,280 --> 00:02:52,760
Keep in mind in general, we can only read from PDA files.

43
00:02:53,090 --> 00:02:57,350
There are some specialized libraries that can attempt to write to paedophiles, but I can often result

44
00:02:57,440 --> 00:02:58,070
in errors.

45
00:02:58,160 --> 00:03:03,710
So in this lecture, we're just going to be focusing on radion from PDA files, possibly adding pages

46
00:03:03,710 --> 00:03:07,460
to PDA files and then really working off the text from a PDA file.

47
00:03:08,060 --> 00:03:09,230
So let's go ahead and get started.

48
00:03:10,540 --> 00:03:15,300
First thing to do is make sure you've already installed Pae PDAF two if you haven't.

49
00:03:15,400 --> 00:03:20,740
Just go to your command line and type PIP install capital P Y Capital PDAF two.

50
00:03:21,190 --> 00:03:22,870
So already ran that my command line.

51
00:03:22,900 --> 00:03:23,800
So I haven't installed.

52
00:03:24,160 --> 00:03:26,010
Which means I should be able to import it now.

53
00:03:26,680 --> 00:03:27,570
Pipe PDAF two.

54
00:03:28,210 --> 00:03:31,900
And there it is just as before the CSF reader.

55
00:03:31,970 --> 00:03:35,200
What we're gonna do is we're gonna open using normal python.

56
00:03:35,440 --> 00:03:39,610
The PDF file and then we're gonna do is attach a reader object to it.

57
00:03:40,270 --> 00:03:46,420
So I will say F is equal to open and the file we're working with is called Working Business Proposal

58
00:03:46,480 --> 00:03:47,290
Dot PDAF.

59
00:03:48,100 --> 00:03:52,750
And the second thing you have to note here is kind of like we're working off image files before we had

60
00:03:52,750 --> 00:03:54,820
to read and write the binary code.

61
00:03:55,120 --> 00:04:02,110
In this case, we have to have the Mode B Arbi for read binary since this is not just a normal DOT text

62
00:04:02,110 --> 00:04:02,440
file.

63
00:04:03,280 --> 00:04:09,550
Next, in order to make sure we can actually work with this, we say PDAF Reader and then we call the

64
00:04:09,550 --> 00:04:14,700
PI PDAF to library and call PDAF File Reader.

65
00:04:15,910 --> 00:04:17,260
And then we're gonna pass an F there.

66
00:04:18,460 --> 00:04:24,640
And if that worked correctly, we should be able to view how many pages this PDA file had by calling

67
00:04:24,640 --> 00:04:26,740
the NUM pages attribute.

68
00:04:27,580 --> 00:04:29,920
And so there's five pages in this PDA file.

69
00:04:30,370 --> 00:04:37,450
Let's grab the first page by saying page one is equal to our PDAF reader object.

70
00:04:37,840 --> 00:04:40,090
And then the method here is get page.

71
00:04:40,600 --> 00:04:43,330
And then you just pass in the indexed position of the page you want.

72
00:04:43,480 --> 00:04:46,090
So if you want the first page, that's indexed zero.

73
00:04:47,540 --> 00:04:55,310
Then if you want the text on that page, we say page one text is equal to you, grab that page one object

74
00:04:55,370 --> 00:04:58,040
and you call, extract, text.

75
00:04:59,820 --> 00:05:00,390
Run that.

76
00:05:00,600 --> 00:05:03,060
And now you should have the text as a python strain.

77
00:05:04,050 --> 00:05:08,190
And this is where you can do things like perform a regular expression, search for a particular pattern.

78
00:05:08,970 --> 00:05:15,900
So, again, the steps are to read in the file with Arbie as your mode UPDF file a reader on that file,

79
00:05:16,260 --> 00:05:17,610
confirm the number of pages.

80
00:05:18,000 --> 00:05:23,160
And then check if it worked by saying get page extract text and then view the text.

81
00:05:23,610 --> 00:05:29,490
Keep in mind, even if you see the number of pages is correct for certain paedophiles such as a scanned

82
00:05:29,490 --> 00:05:33,330
paedophile or something that's too blurry in order to extract the text.

83
00:05:33,810 --> 00:05:38,130
This number of pages will still work, but when you extract the text, you'll just get an empty string.

84
00:05:38,250 --> 00:05:43,290
So if you're getting an empty string, it means your PDA file is not convert or is not compatible with

85
00:05:43,290 --> 00:05:44,280
PI PDAF two.

86
00:05:44,580 --> 00:05:48,870
And you may want to check out some of those other libraries that we link to inside of the lecture notebook.

87
00:05:49,500 --> 00:05:53,580
Once you're done with working off the text, you'll go ahead and close this off.

88
00:05:57,150 --> 00:06:02,460
So as you can imagine, we could have easily created a for loop for the range in number of pages to

89
00:06:02,460 --> 00:06:07,800
extract all the text from every page by simply just having this be a variable call instead of hardcoded

90
00:06:07,800 --> 00:06:08,160
zero.

91
00:06:08,910 --> 00:06:11,220
Now, let's talk about adding to paedophiles.

92
00:06:11,640 --> 00:06:14,370
Well, we can technically write to a paedophile.

93
00:06:14,400 --> 00:06:16,860
We can only write another PDAF page.

94
00:06:17,190 --> 00:06:23,340
We can't go into the middle of a page in a paedophile and write text to it with PI PDAF to some paid

95
00:06:23,340 --> 00:06:24,660
programs allow you to do that.

96
00:06:24,960 --> 00:06:29,250
But I haven't seen anything in open source python that allows you to do that consistently.

97
00:06:29,400 --> 00:06:34,950
And it really just has to do with the complexity behind the PDAF format if you plan on reading and writing

98
00:06:34,950 --> 00:06:35,580
to a file.

99
00:06:35,820 --> 00:06:37,800
It should probably not be in a PDAF format.

100
00:06:38,130 --> 00:06:41,250
It should be in something like a text document or even a word document.

101
00:06:41,700 --> 00:06:45,600
PDX in general are not designed to be easily editable.

102
00:06:46,380 --> 00:06:50,000
So we're going to do here is we're gonna show you how to add pages to PDAF.

103
00:06:50,730 --> 00:06:54,520
So let's go ahead and open up that file again.

104
00:06:55,680 --> 00:07:00,780
Just as we did before, we're going to say working business proposal, that P.D.A.

105
00:07:00,960 --> 00:07:02,560
I just just have to autocomplete there.

106
00:07:03,390 --> 00:07:06,530
Open it Winfried binary and then assign the P.D.A.

107
00:07:06,550 --> 00:07:07,860
Friedler, just as we did before.

108
00:07:09,190 --> 00:07:10,360
So PDAF reader.

109
00:07:11,770 --> 00:07:19,780
And then let's make sure we call it off by PDAF, so pi pdaf to PDAF File Reader.

110
00:07:23,140 --> 00:07:23,620
There we go.

111
00:07:23,680 --> 00:07:25,180
So we're reading it just before.

112
00:07:25,480 --> 00:07:26,660
I'll grab the first page.

113
00:07:28,200 --> 00:07:32,650
Equal to PDAF Reader Kepp Page zero.

114
00:07:34,020 --> 00:07:34,190
OK.

115
00:07:34,300 --> 00:07:35,590
So nothing changed before.

116
00:07:35,890 --> 00:07:40,990
Right now, I have this document working business proposal, and I'm only interested in the first page.

117
00:07:41,020 --> 00:07:41,590
So I got it.

118
00:07:42,070 --> 00:07:47,050
So maybe I have a colleague or co-worker that says, hey, can you programmatically grab all the first

119
00:07:47,050 --> 00:07:50,680
pages off the PDAF or maybe all the last pages off the PDAF?

120
00:07:50,680 --> 00:07:54,640
Because that has certain information that I'm looking for and I don't need the entire PDAF.

121
00:07:55,090 --> 00:07:59,620
So we could do is programmatically do that by then writing this to a new file.

122
00:08:00,430 --> 00:08:03,490
And to do this, I create a writer object.

123
00:08:03,580 --> 00:08:04,330
We'll say P.D.A.

124
00:08:04,480 --> 00:08:05,260
Underscore writer.

125
00:08:06,450 --> 00:08:07,950
It's a pie pdaf, too.

126
00:08:09,330 --> 00:08:11,850
And in this case, we'll say P.D.A file writer.

127
00:08:14,160 --> 00:08:15,910
And this creates a writer object.

128
00:08:17,240 --> 00:08:19,520
And then we add pages to this writer object.

129
00:08:19,880 --> 00:08:27,380
So we say PDAF writer at Page and the page, we should add should already be a specialized file type.

130
00:08:27,890 --> 00:08:34,310
So what I mean by that is if I were to insert a cell below here, let's check the type of first page.

131
00:08:34,850 --> 00:08:40,310
It should be a PDAF page object in order to be added as a PDAF writer.

132
00:08:40,400 --> 00:08:42,320
It shouldn't just be a raw python string.

133
00:08:43,390 --> 00:08:49,210
So what we will do here is we'll go ahead and type in first page, since I know that's a correct page

134
00:08:49,240 --> 00:08:52,150
object and I could keep adding pages.

135
00:08:52,180 --> 00:08:54,970
But for some reason, let's say only one of the first page.

136
00:08:55,330 --> 00:08:59,230
And now what I need to do is open up a new file and then write to it.

137
00:09:00,070 --> 00:09:02,200
So we will create an object called PDAF output.

138
00:09:03,590 --> 00:09:08,340
And you could have done this before this had to be in this exact order, but I'll open up some new file

139
00:09:08,490 --> 00:09:11,790
so we'll say some brand new.

140
00:09:14,320 --> 00:09:19,270
Doc, Doc, pdaf, and we'll open it now with Right Binary.

141
00:09:20,530 --> 00:09:25,570
Keep in mind this will overwrite the current existing paedophile if you have something called that and

142
00:09:25,570 --> 00:09:26,890
then we say PDAF writer.

143
00:09:28,150 --> 00:09:28,900
That right.

144
00:09:29,710 --> 00:09:35,910
And we say PDAF underscore output and then we can close off the file.

145
00:09:38,350 --> 00:09:38,570
OK.

146
00:09:38,900 --> 00:09:40,160
So what actually happened here?

147
00:09:40,730 --> 00:09:48,800
Notice that we read the file just before we got a specific page object, then in order to write to a

148
00:09:48,800 --> 00:09:49,430
new file.

149
00:09:49,700 --> 00:09:52,150
We went ahead and created P.D.A file writer.

150
00:09:52,700 --> 00:09:55,760
We add pages as many as we want to this writer object.

151
00:09:56,210 --> 00:10:00,110
Then we open some new file using standard python with write binary.

152
00:10:00,560 --> 00:10:03,110
And then we use the PDAF writer to write to that.

153
00:10:03,620 --> 00:10:04,760
And then we can close them off.

154
00:10:04,790 --> 00:10:06,380
So knows I have f close.

155
00:10:06,770 --> 00:10:11,120
And then I will say PDAF output and I will close that one as well.

156
00:10:11,330 --> 00:10:15,200
And now both the file I read and the file I wrote to are closed.

157
00:10:17,330 --> 00:10:20,660
So let's finish off this lecture by just working through a simple example.

158
00:10:20,960 --> 00:10:24,740
Let's imagine I wanted to grab all the texts that exists within a paedophile.

159
00:10:25,130 --> 00:10:26,660
I can do that with a simple for loop.

160
00:10:27,440 --> 00:10:30,680
Just to reiterate the steps again using standard python.

161
00:10:31,190 --> 00:10:32,560
I open up that PDA file.

162
00:10:33,880 --> 00:10:38,410
We've read binary and then what I'm going to do is have a place holder.

163
00:10:39,920 --> 00:10:45,020
I will make this place holder a list where the index of the list will correspond to the page number.

164
00:10:45,380 --> 00:10:49,340
So essentially I'll have one really large string per position in this list.

165
00:10:50,480 --> 00:10:54,890
Then we create our reader object with PI.

166
00:10:57,080 --> 00:10:59,810
PDAF to make sure you have the capitalization, correct?

167
00:11:00,530 --> 00:11:12,140
We create a in this case, a reader object pass in the file we opened and then we'll say for NUM in

168
00:11:12,140 --> 00:11:12,740
range.

169
00:11:14,150 --> 00:11:17,120
PDAF reader number of pages.

170
00:11:17,520 --> 00:11:24,710
So recall, that's an integer that's returned, then I will say page is equal to PDAF reader.

171
00:11:25,490 --> 00:11:32,570
I will get that page at that particular number position and then I will say PDAF text.

172
00:11:33,670 --> 00:11:41,140
Go ahead and append the extract, the text, which I can get by saying, get that page object and extract

173
00:11:41,290 --> 00:11:41,980
the text.

174
00:11:43,770 --> 00:11:44,550
On that page.

175
00:11:46,700 --> 00:11:53,570
So if we run this and went ahead and worked, so then I should be able to say PDAF text and I get this

176
00:11:53,570 --> 00:11:58,610
long list where each item in that list is the corresponding text.

177
00:11:58,640 --> 00:12:00,290
So this is the text on the first page.

178
00:12:01,730 --> 00:12:03,800
Here's a text on the second page and so on.

179
00:12:03,830 --> 00:12:05,140
You see these backslash ends.

180
00:12:05,180 --> 00:12:06,290
Those are just new lines.

181
00:12:06,560 --> 00:12:11,930
Which means if I really wanted to, I could just print out the string instead of returning it, which

182
00:12:11,930 --> 00:12:15,110
will actually show you kind of where those new lines stand.

183
00:12:15,410 --> 00:12:19,070
And I'm really zoomed in here, so it might look a little strange, but it should look something like

184
00:12:19,070 --> 00:12:19,290
this.

185
00:12:19,320 --> 00:12:21,140
You can see the paragraphs and new lines.

186
00:12:21,800 --> 00:12:22,080
All right.

187
00:12:22,640 --> 00:12:27,090
So that's an example of working with PDAF and reading and writing to PDX.

188
00:12:27,530 --> 00:12:32,180
Coming up next, we're going to set you loose on a puzzle exercise that will combine both work and if

189
00:12:32,180 --> 00:12:34,880
CSP files and working with PDA files.

190
00:12:35,240 --> 00:12:36,200
I'll see you at the next lecture.
