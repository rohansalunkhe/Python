1
00:00:05,410 --> 00:00:09,010
Welcome, everyone, to this section of the course on PDX and spreadsheets.

2
00:00:10,120 --> 00:00:16,360
Python has the ability to work with PDA files and spreadsheet files such as a DOT CSP file in this section

3
00:00:16,360 --> 00:00:16,900
of the course.

4
00:00:16,990 --> 00:00:21,730
We're going to be exploring libraries that allow us to interact with these files, such as reading information

5
00:00:21,730 --> 00:00:22,180
from them.

6
00:00:22,840 --> 00:00:24,340
As a quick note before we begin.

7
00:00:24,640 --> 00:00:28,300
We highly recommend you work in the same location as the lecture notebooks.

8
00:00:28,630 --> 00:00:33,130
Since we'll be referencing many files that we've provided for you that are already in that location.

9
00:00:33,580 --> 00:00:36,730
Otherwise, you'll need to be worrying about providing the full file path.

10
00:00:36,760 --> 00:00:41,470
So it's a lot easier if you just start your notebook in the same location as election books.

11
00:00:41,890 --> 00:00:43,210
OK, let's get started.

12
00:00:43,450 --> 00:00:44,410
I'll see at the next lecture.
