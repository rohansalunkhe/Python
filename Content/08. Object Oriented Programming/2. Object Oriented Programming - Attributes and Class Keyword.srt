1
00:00:05,430 --> 00:00:06,700
Welcome back everyone.

2
00:00:06,720 --> 00:00:12,230
It's time to actually dive into Python and use object oriented programming to create our own objects.

3
00:00:12,300 --> 00:00:18,210
In Part 1 of this series of lectures we're going to focus on the class key word as well as creating

4
00:00:18,270 --> 00:00:20,560
attributes for our objects.

5
00:00:20,700 --> 00:00:23,870
Let's jump to Jupiter in a book and get started.

6
00:00:23,870 --> 00:00:29,150
All right so let's start off by actually realizing what an object already looks like in Python.

7
00:00:29,180 --> 00:00:36,440
If I were to create my list one two three I would be able to then call the variable my list say Dot

8
00:00:36,770 --> 00:00:41,150
hit tab and see a bunch of attributes and methods off of this object.

9
00:00:41,150 --> 00:00:42,960
And the same thing goes for a set.

10
00:00:43,040 --> 00:00:49,730
And we've seen how to create a set using my set equal to then we have this set keyword and then if I

11
00:00:49,730 --> 00:00:55,040
take a look at my set I can do that tab and see a bunch of attributes and methods.

12
00:00:55,250 --> 00:01:00,680
Well we're going to do is see if we can create our own user defined objects and everything in Python

13
00:01:00,770 --> 00:01:01,690
is an object.

14
00:01:01,730 --> 00:01:08,240
So I could say what is the type of my set and I'll report back that it's a set and I can do the same

15
00:01:08,240 --> 00:01:12,140
thing for that list I just created reports back that it's a list.

16
00:01:12,140 --> 00:01:16,950
So let's explore how we can use the class keyword to create a user defined object.

17
00:01:16,950 --> 00:01:18,470
So these are built in objects.

18
00:01:18,470 --> 00:01:20,890
Let's use Class C L.

19
00:01:20,900 --> 00:01:22,810
SS That's a key word here.

20
00:01:23,000 --> 00:01:28,760
For a user defined object the class is basically a blueprint that defines the nature of a future object

21
00:01:28,880 --> 00:01:29,930
from classes.

22
00:01:29,930 --> 00:01:36,110
We can then construct an instance of the object in an instance is a specific object created from a particular

23
00:01:36,110 --> 00:01:37,280
class.

24
00:01:37,520 --> 00:01:40,870
So we're going to now create the simplest class possible.

25
00:01:41,030 --> 00:01:42,800
So I'm going to say sample.

26
00:01:42,830 --> 00:01:45,640
Notice how my class name is capitalized.

27
00:01:45,800 --> 00:01:53,160
So by convention for classes we follow camel casing which is to say that every word here has parts of

28
00:01:53,180 --> 00:01:57,590
a capital letter and like Snake casing for variable names and function names or we have underscores

29
00:01:58,070 --> 00:02:02,350
so by convention we use capitalized names for classes.

30
00:02:02,390 --> 00:02:05,710
We have class sample word and I'm going to say sample here.

31
00:02:06,650 --> 00:02:08,300
Open close parentheses.

32
00:02:08,600 --> 00:02:11,420
Colon and then we'll say pass.

33
00:02:11,650 --> 00:02:16,130
So recall the past Key word basically just says don't do anything and it's a placeholder there.

34
00:02:16,180 --> 00:02:18,900
If we didn't have it we would get some sort of syntax error.

35
00:02:19,000 --> 00:02:19,590
And the file.

36
00:02:19,590 --> 00:02:21,810
So instead we have pass and that's it.

37
00:02:21,850 --> 00:02:27,220
We've just created our first class and all it's doing is just saying hey you create a sample class and

38
00:02:27,250 --> 00:02:27,760
that's it.

39
00:02:27,760 --> 00:02:29,040
There's nothing else there.

40
00:02:29,500 --> 00:02:37,180
So let's create an instance of the sample class we'll say my sample is equal to and then we call sample

41
00:02:37,540 --> 00:02:41,500
open close Princie we don't passen anything into it yet.

42
00:02:41,500 --> 00:02:42,610
Shift enter to run that.

43
00:02:42,640 --> 00:02:48,310
And if I check the type of my sample you should see something that lets you know that it's a sample

44
00:02:48,310 --> 00:02:49,240
type.

45
00:02:49,240 --> 00:02:49,720
All right.

46
00:02:49,870 --> 00:02:54,210
So we've done so far is we use the keyword class created our own sample class.

47
00:02:54,250 --> 00:02:55,700
It doesn't really do anything.

48
00:02:55,750 --> 00:03:02,630
Then we create an instance of that class by saying the variable name is equal to an instance of sample.

49
00:03:02,770 --> 00:03:07,510
And then I can check my sample and it says its main thought sample later on in the future lecture we're

50
00:03:07,510 --> 00:03:12,550
going to discuss what this underscore underscore main underscore underscore key word actually means

51
00:03:12,840 --> 00:03:18,640
basically is just letting you know that this particular instance of sample is connected to your main

52
00:03:18,640 --> 00:03:20,320
script here.

53
00:03:20,340 --> 00:03:20,810
All right.

54
00:03:21,040 --> 00:03:23,170
So it's the simplest class possible.

55
00:03:23,320 --> 00:03:26,200
It's not very useful though because it doesn't have any attributes.

56
00:03:26,200 --> 00:03:32,090
So let's now see how we can create attributes so inside of class here.

57
00:03:32,220 --> 00:03:34,470
I'm going to write two lines of code.

58
00:03:34,770 --> 00:03:40,500
I'm going to say DPF just like we would for a function except when it's inside of a class we refer to

59
00:03:40,500 --> 00:03:41,830
this as a method.

60
00:03:42,150 --> 00:03:47,610
And this is a special method called the init method which is going to be called upon whenever you actually

61
00:03:47,610 --> 00:03:49,680
create an instance of the class.

62
00:03:50,010 --> 00:03:56,760
And we always start off with the self keyword which basically connects this method to the instance of

63
00:03:56,760 --> 00:04:03,750
the class and it allows us to refer to itself and then we pass in any attributes that we want the user

64
00:04:03,750 --> 00:04:05,250
to define.

65
00:04:05,250 --> 00:04:13,770
So for example instead of calling our class sample I'm going to say that it's a dog class so dogs have

66
00:04:13,830 --> 00:04:17,620
attributes to them and an attribute is a characteristic of an object.

67
00:04:17,640 --> 00:04:21,310
Later on we'll discuss methods which is an operation we can perform with the object.

68
00:04:21,480 --> 00:04:23,750
But right now focusing on attributes.

69
00:04:23,880 --> 00:04:28,100
So we may be thinking well what kind of attributes or characteristics do dogs have.

70
00:04:28,320 --> 00:04:29,590
Well they have a breed.

71
00:04:29,810 --> 00:04:34,060
So we may want to pass is the breed of the dog.

72
00:04:34,220 --> 00:04:41,200
I remember when we had my set and my list I could do dot tab and then see a bunch of options.

73
00:04:41,240 --> 00:04:45,890
Some of these are methods some of these are attributes we're going to do now is try to create an attribute

74
00:04:45,890 --> 00:04:52,310
here so that when I create an instance of the dog class I can hit dot and see that breede pop up.

75
00:04:52,310 --> 00:04:56,960
So let me delete these lines because we don't need them anymore since the sample class will be used

76
00:04:57,860 --> 00:05:03,560
and then we will say self-taught breed is equal to breed.

77
00:05:03,780 --> 00:05:08,500
And these two lines can be really confusing at first since this is the first time we see the self key

78
00:05:08,510 --> 00:05:11,060
word and we see the sort of like strange assignments.

79
00:05:11,270 --> 00:05:15,570
But I want to run this first and then break down what's happening in these two lines of code.

80
00:05:16,130 --> 00:05:20,460
So let's run this and notice my indentation there everything's indented a little bit.

81
00:05:20,710 --> 00:05:22,560
And let's create a variable.

82
00:05:22,780 --> 00:05:27,970
I will say My dog is equal to an instance of the dog class.

83
00:05:27,970 --> 00:05:31,990
Now if I don't pass anything I will actually get an error.

84
00:05:32,050 --> 00:05:37,630
So you'll notice that we actually get an error when we run this with no positional arguments because

85
00:05:37,910 --> 00:05:41,380
we are expecting the Breede parameter.

86
00:05:41,380 --> 00:05:46,650
So what this python is trying to say to us is hey when you create a dog it's expecting you to pass any

87
00:05:46,660 --> 00:05:48,370
parameter for breed.

88
00:05:48,460 --> 00:05:50,130
So let's do that.

89
00:05:50,140 --> 00:05:53,670
We're going to say breed of the dog is a lab.

90
00:05:54,930 --> 00:05:58,130
And now when we run this we don't get an error.

91
00:05:58,440 --> 00:06:05,340
And I can check the type of this my dog very bull and it says hey you now have an instance of the dog

92
00:06:05,370 --> 00:06:06,300
class.

93
00:06:06,660 --> 00:06:12,970
And if I say my dog dots and then hit tab I should be able to autocomplete breed.

94
00:06:13,140 --> 00:06:17,370
Now the reason breed autocomplete is because right now the only attribute later on when we added more

95
00:06:17,370 --> 00:06:19,310
attributes will see a dropdown list.

96
00:06:19,530 --> 00:06:27,370
But if I run this reports back hey this instance of the dog class called my dog its breed is lab so

97
00:06:27,370 --> 00:06:34,060
let's scroll back up here and break down what's going on in it can basically be thought of as the constructor

98
00:06:34,060 --> 00:06:39,850
for a class and it's going to be called automatically when you create an instance of the class this

99
00:06:39,850 --> 00:06:40,610
self.

100
00:06:40,770 --> 00:06:44,690
Keyword it represents the instance of the object itself.

101
00:06:44,710 --> 00:06:49,750
And actually most object oriented languages passed this as a hidden parameter to the methods defined

102
00:06:49,750 --> 00:06:50,700
on an object.

103
00:06:50,800 --> 00:06:52,480
But Python doesn't do this.

104
00:06:52,480 --> 00:06:57,130
You have to declare explicitly and by convention we use the word self here.

105
00:06:57,130 --> 00:07:01,300
Technically you could write in any variable name you wanted but you should definitely stick with self

106
00:07:01,600 --> 00:07:01,860
that way.

107
00:07:01,870 --> 00:07:05,520
Other programmers when they look your code it makes sense to them.

108
00:07:05,560 --> 00:07:10,090
Now I want to focus on Breede because I found that confusing when I was just beginning.

109
00:07:10,190 --> 00:07:16,480
You see Breede here three times you see Breede self-taught Breede is equal to breed and I want to show

110
00:07:16,480 --> 00:07:19,030
you something that's not going to be correct.

111
00:07:19,030 --> 00:07:25,940
Conventionalized will hopefully help you understand how Breede is being worked into this entire object.

112
00:07:26,350 --> 00:07:31,930
So what happens is when you create a instance of the dog class Python is going to call this method and

113
00:07:31,930 --> 00:07:36,760
it's going to use self in order to represent the instance of the object itself and then it's going to

114
00:07:36,760 --> 00:07:42,620
expect you to passen this argument breed and I'm going to call this my breed.

115
00:07:42,880 --> 00:07:50,480
And what happens is when you say my breed here I can set my abrade here and then you assign the attribute

116
00:07:50,510 --> 00:07:52,710
equal to whatever this parameter was.

117
00:07:52,970 --> 00:07:56,790
By convention these three have the same name.

118
00:07:56,930 --> 00:08:01,940
But I want to show you how changing the name will hopefully make this clear to you what the actual connection

119
00:08:01,940 --> 00:08:03,940
is when you pass in the parameter.

120
00:08:03,950 --> 00:08:11,090
So I'm going to rerun this and if I take a look at dog and then do shift tab and you can also call help

121
00:08:11,090 --> 00:08:11,890
on dog.

122
00:08:11,930 --> 00:08:15,360
You notice that it's expecting my breed to be passed in.

123
00:08:15,770 --> 00:08:22,010
So what happens is you Pessin this variable for my breed and then the next step is you take whatever

124
00:08:22,010 --> 00:08:29,060
variable was provided for my breed and then said equal to the attribute breed or self-taught breed because

125
00:08:29,060 --> 00:08:32,000
this represents the instance of the object itself.

126
00:08:32,000 --> 00:08:38,440
So all we're doing when it comes to attributes which again are characteristics of the object later on

127
00:08:38,440 --> 00:08:44,410
we'll learn about methods if we take in the argument.

128
00:08:44,760 --> 00:08:54,170
In this case it's my Breede and we assign it using self-taught attribute name.

129
00:08:54,180 --> 00:08:58,050
So that's just a convention of what you're actually typing self-taught.

130
00:08:58,070 --> 00:08:59,480
And then you choose the attribute name.

131
00:08:59,480 --> 00:09:01,950
In this case I'm calling it the breed.

132
00:09:02,390 --> 00:09:04,290
So let's run this again.

133
00:09:04,370 --> 00:09:11,510
Make sure you rerun the cell so we have my breed there and then I can say my breed is equal to let's

134
00:09:11,510 --> 00:09:16,080
say Sam is a husky now I run this I run this again.

135
00:09:16,190 --> 00:09:22,550
I still have a dog class and if I take a look at my dog if I hit tab I only have Breede I don't have

136
00:09:22,550 --> 00:09:27,860
my breed available because my breed was just the parameter name we chose for the argument.

137
00:09:27,860 --> 00:09:33,110
So he said Oh passen the my breed argument and I'm going to assign it to self-taught breed here and

138
00:09:33,110 --> 00:09:34,780
I want to make this even more clear.

139
00:09:36,280 --> 00:09:43,900
By saying breed is now my attribute.

140
00:09:44,050 --> 00:09:50,940
If I run this again what happens is I pass and again my breed is Huskie the type again staes dog.

141
00:09:51,220 --> 00:09:57,660
But if I say my dog tab it's now my attribute and it happens to be Huskey.

142
00:09:57,680 --> 00:10:04,070
So all we're doing here is you Pessin the parameter or argument and then it gets assigned to the attribute

143
00:10:04,200 --> 00:10:06,680
you can later call on your object.

144
00:10:06,680 --> 00:10:12,180
However by convention you're just going to use the same parameter name for all three of these.

145
00:10:12,290 --> 00:10:20,470
Which is why you end up seeing something like self-taught breed is equal to breed and you passen breed.

146
00:10:20,470 --> 00:10:25,510
This is confusing at first for beginners because they see Brehon here three times but you can tell that

147
00:10:25,510 --> 00:10:30,100
once you're familiar of object oriented programming it's just cleaner is to have all three of these

148
00:10:30,490 --> 00:10:35,600
be the same term that we you know what's being passed then and where it's being assigned.

149
00:10:35,740 --> 00:10:39,740
There's no real reason that they should all have those separate names.

150
00:10:39,760 --> 00:10:42,580
So right now our dog only has a breed.

151
00:10:42,700 --> 00:10:45,280
But let's go ahead and give it more attributes.

152
00:10:45,280 --> 00:10:46,840
So what else could a dog have.

153
00:10:46,840 --> 00:10:48,400
Well dog could have a name

154
00:10:51,580 --> 00:10:58,190
and maybe we can't say does the dog have spots or not.

155
00:10:58,220 --> 00:11:01,400
So we have a breed name and spots.

156
00:11:03,060 --> 00:11:08,850
So here we just say create some more attributes soft but name of the dog is equal to whatever name you

157
00:11:08,850 --> 00:11:15,760
pasan and self-taught spots is equal to it every person for spots.

158
00:11:15,920 --> 00:11:22,670
And notice here what I'm doing is I'm kind of expecting certain data types 10:02 I expect breed to be

159
00:11:22,670 --> 00:11:23,570
a string.

160
00:11:23,600 --> 00:11:29,720
I expect the name to be a string and spots the way I'm thinking about it is actually going to be a boolean.

161
00:11:29,720 --> 00:11:31,440
So me have a comment here.

162
00:11:32,980 --> 00:11:35,700
I expect the boolean true or false.

163
00:11:35,730 --> 00:11:37,970
Does the dog have spots or not.

164
00:11:38,070 --> 00:11:42,450
So I hope you get the idea here every attribute you pass in is going to be a string but it can be an

165
00:11:42,450 --> 00:11:48,710
integer a floating point can be a list can be a lot of stuff so I rerun this.

166
00:11:48,950 --> 00:11:53,080
And now let's take a look at what we need to define our dog.

167
00:11:53,180 --> 00:12:00,860
We need the breed of the dog the name of the dog and true or false does it have spots so we'll have

168
00:12:00,860 --> 00:12:06,430
the breed of the dog be a lab will have the name of the dog.

169
00:12:07,900 --> 00:12:13,520
B Sammy and we'll have spots be false Sammy doesn't have spots.

170
00:12:14,690 --> 00:12:16,870
So I run this I run that again.

171
00:12:16,880 --> 00:12:23,360
I still have that dog class but now when I say my dog and I hit tab I see the options I can see Breede

172
00:12:23,630 --> 00:12:30,920
name and spots so I can call these check with the breed of that instance of the dog classes I can check

173
00:12:30,920 --> 00:12:34,850
what the name of the dog is and I can check.

174
00:12:35,300 --> 00:12:36,590
The dog have spots.

175
00:12:36,890 --> 00:12:37,810
They'll tell me false.

176
00:12:37,850 --> 00:12:39,490
It doesn't have spots.

177
00:12:39,500 --> 00:12:44,930
One of the issues of Python being so flexible is that later on when you're a programmer and expecting

178
00:12:44,990 --> 00:12:50,270
other people to use your classes you're going to need to add in some documentation to let them know

179
00:12:50,300 --> 00:12:55,510
hey I'm expecting strings for Breede strings for name and spots should be a boolean.

180
00:12:55,520 --> 00:13:03,190
Because right now I could say something like no spots and Python is not going to complain here because

181
00:13:03,190 --> 00:13:07,270
I didn't do any sort of type check to make sure Spotts was a boolean.

182
00:13:07,270 --> 00:13:10,360
So if I were to run this all again there wouldn't be any error.

183
00:13:10,360 --> 00:13:14,130
Just when you say that spots it would print out that string spots.

184
00:13:14,700 --> 00:13:17,770
OK so those are the very basics of attributes.

185
00:13:17,800 --> 00:13:23,710
We'll discuss better ways to have type control to make sure of things like spots are booleans.

186
00:13:23,710 --> 00:13:29,440
Later on in the course but right now what we really want to focus on is the basic idea that you use

187
00:13:29,440 --> 00:13:34,920
the class keyword then with capitalized names you'd find the name of your class.

188
00:13:35,140 --> 00:13:40,870
And the very first method in your class is going to be a special method called the in its method and

189
00:13:40,880 --> 00:13:43,120
the method it acts as a constructor.

190
00:13:43,120 --> 00:13:48,910
And we have this self keyword which is a reference to the instance of the class to the instance of the

191
00:13:48,910 --> 00:13:55,070
class is then going to have these attributes that you pass in based off what parameters you define up

192
00:13:55,090 --> 00:13:55,770
here.

193
00:13:55,870 --> 00:14:00,880
And by convention the parameter name the attribute name are the same.

194
00:14:00,880 --> 00:14:06,490
Which means you will see it three times once when you pass it in and then once here for the attribute

195
00:14:06,640 --> 00:14:10,150
and once for the actual assignment call OK.

196
00:14:10,340 --> 00:14:13,540
Those are the basics of creating attributes in the next lecture.

197
00:14:13,550 --> 00:14:18,300
We're going to thus discuss class object attributes and then we'll move on to talking about methods.

198
00:14:18,440 --> 00:14:19,070
We'll see if they're.
