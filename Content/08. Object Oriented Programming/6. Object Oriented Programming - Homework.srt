1
00:00:05,890 --> 00:00:07,310
Welcome back everybody.

2
00:00:07,360 --> 00:00:11,800
Now that we've gone over a series of lectures showing you how to work with Python for object oriented

3
00:00:11,800 --> 00:00:16,140
programming Let's test your new skills by giving you a quick homework assignment.

4
00:00:16,180 --> 00:00:21,220
I'm going to go over the notebook that contains this homework assignment or so here I am at the hub

5
00:00:21,220 --> 00:00:23,610
Ribault just to show you where the notebook is available.

6
00:00:23,740 --> 00:00:29,020
If you go to complete three bootcamp under the object oriented programming folder you can find the object

7
00:00:29,050 --> 00:00:31,750
oriented programming homework notebook and that's the notebook.

8
00:00:31,750 --> 00:00:35,540
That's right after that main lecture note book about object oriented programming.

9
00:00:35,770 --> 00:00:38,330
And this homework assignment has two simple problems.

10
00:00:38,410 --> 00:00:43,330
If you recall during the lectures we created a circle object so we create instances of a circle and

11
00:00:43,340 --> 00:00:45,820
that circle object had a radius.

12
00:00:45,820 --> 00:00:47,620
So what you're going to be doing is something similar.

13
00:00:47,650 --> 00:00:53,350
We're going to use geometry to create two different objects we'll have a line object and a cylinder

14
00:00:53,350 --> 00:00:56,380
object or three line class and a still in there class.

15
00:00:56,590 --> 00:00:57,800
Let's go to the first one.

16
00:00:58,090 --> 00:01:00,690
So here we have three methods free to fill out.

17
00:01:00,750 --> 00:01:02,810
One is the first special method.

18
00:01:03,040 --> 00:01:07,410
The second is the distance method and the third is this slope method.

19
00:01:07,450 --> 00:01:12,620
So what we expect to be able to do is given two coordinates in the form of a tuple.

20
00:01:12,820 --> 00:01:20,710
So we have x y coordinates coordinate one equals three to coordinate 2 6 8 10 are really whatever numbers

21
00:01:21,070 --> 00:01:23,110
you'd be able to pass in those coordinates.

22
00:01:23,110 --> 00:01:28,130
Remember there are tuples and then using that you'd be able to call the distance method to get back

23
00:01:28,150 --> 00:01:33,540
the distance between these two points and the slope method to get the slope between these two points.

24
00:01:33,610 --> 00:01:38,080
And you can just google around for the actual formulas for distance and slope.

25
00:01:38,200 --> 00:01:40,670
But in case you want to pop your screen will crack.

26
00:01:40,810 --> 00:01:48,090
Here's the actual distance formula given two points just x of two minus X of 1 squared plus y of two

27
00:01:48,100 --> 00:01:49,560
minus y of one squared.

28
00:01:49,570 --> 00:01:55,690
And then take square of that where you have y to an X to r one point and then x y and y one or the other

29
00:01:55,690 --> 00:02:00,750
point to the distance formula and the slope formula as you may recall is just y to mine.

30
00:02:00,760 --> 00:02:03,080
So I 1 x 2 most x 1.

31
00:02:03,100 --> 00:02:07,550
So these are the formulas that you're going to be using and you're going to take in these coordinates

32
00:02:07,970 --> 00:02:13,040
and convert those formulas into method calls here and basically returning the result to that formula

33
00:02:14,280 --> 00:02:19,350
and then you have problem too which is quite similar to that circle class except this time it's a cylinder.

34
00:02:19,350 --> 00:02:25,830
So you need to do is again fill out this form right here to set the attributes and then have volume

35
00:02:26,070 --> 00:02:30,910
method filled out and surface area and you can always google search the Wikipedia page for a cylinder.

36
00:02:31,050 --> 00:02:34,720
They have the formulas for the volume and surface area of a cylinder there.

37
00:02:34,770 --> 00:02:40,300
So an example output is given a cylinder with a height of two and a radius of three units.

38
00:02:41,090 --> 00:02:45,650
Past the volume method to actually get a volume back and then execute the surface area method to get

39
00:02:45,650 --> 00:02:46,810
the surface area.

40
00:02:47,210 --> 00:02:49,590
And that's basically it for this quick homework assignment.

41
00:02:49,790 --> 00:02:50,710
I would say go.

42
00:02:50,720 --> 00:02:56,450
Problem to first actually because it tends to be a little simpler than the line class in case you want

43
00:02:56,540 --> 00:02:58,140
a more gradual pacing.

44
00:02:58,460 --> 00:02:58,870
OK.

45
00:02:58,920 --> 00:03:01,520
We'll see at the very next lecture or we go over the solutions.

46
00:03:01,520 --> 00:03:05,100
Best of luck and remember that the coordinates here are being passed and as tuples.

47
00:03:05,360 --> 00:03:06,400
We'll see at the solutions.
