1
00:00:05,490 --> 00:00:07,310
Welcome back everyone in this lecture.

2
00:00:07,320 --> 00:00:12,390
We're going to be discussing object oriented programming again and particularly We'll talk about class

3
00:00:12,480 --> 00:00:13,720
object attributes.

4
00:00:13,830 --> 00:00:17,850
And these are attributes that are going to be the same for any instance of the class and then we'll

5
00:00:17,850 --> 00:00:23,370
move on to discussing methods which are basically actions that you perform with the object you created.

6
00:00:23,370 --> 00:00:26,400
Let's jump back to Jupiter note book where we left off last time.

7
00:00:26,400 --> 00:00:32,580
OK so here we can see our dog class that we created last time and we also saw how the user could define

8
00:00:32,640 --> 00:00:38,490
various attributes they could find the breed of the dog such as lab or husky or golden retriever that

9
00:00:38,490 --> 00:00:40,050
could define the name of the dog.

10
00:00:40,050 --> 00:00:45,240
Sammy Franki Whatever happened to your dog and then we can find some sort of boolean variable or even

11
00:00:45,240 --> 00:00:47,460
a string whether or not has spots.

12
00:00:47,520 --> 00:00:54,150
And he resaw we passed in the parameter as the user and then it got assigned to the attribute over here.

13
00:00:54,150 --> 00:01:00,210
Now there may be attributes that are going to be the same for any instance of the dog class for example

14
00:01:00,450 --> 00:01:06,330
we know that it doesn't matter whether a dog has a certain breed a certain name whether or not has spots

15
00:01:06,600 --> 00:01:08,510
it's always going to be a mammal.

16
00:01:08,760 --> 00:01:14,820
So something we may want to do is have an attribute be defined as a class object level instead of having

17
00:01:14,820 --> 00:01:20,580
the particular instance always say that the dog is a mammal and said well defined an attribute above

18
00:01:20,610 --> 00:01:22,490
our in its method here.

19
00:01:22,500 --> 00:01:31,140
So above this special method at the very start we can define what's known as a class object attribute.

20
00:01:31,360 --> 00:01:38,380
And these are the same for any instance of a class.

21
00:01:38,400 --> 00:01:42,370
So let's go ahead and define a class object attribute and what's special about these.

22
00:01:42,390 --> 00:01:48,030
Since we know it's going to be the same for any instance of a class we don't use the self keyword because

23
00:01:48,030 --> 00:01:51,950
we know that the self keyword is a reference to this particular instance of a class.

24
00:01:52,170 --> 00:01:56,880
But since the class object attribute is the same for any instance of a class we can just say something

25
00:01:56,880 --> 00:02:01,620
like the species attribute is equal to mammal.

26
00:02:01,620 --> 00:02:07,380
Now a quick note if you're familiar of biological scientific classification technically species is the

27
00:02:07,380 --> 00:02:08,870
wrong word to use here.

28
00:02:09,000 --> 00:02:14,340
That goes Kingdom phylum and then later on class and classes actually where mammal falls but I don't

29
00:02:14,340 --> 00:02:17,250
want to end up accidentally using the classic keyword twice here.

30
00:02:17,250 --> 00:02:20,790
So instead we'll just pretend that the species of a dog is a mammal.

31
00:02:21,100 --> 00:02:23,110
OK so that's a side note for biology.

32
00:02:23,230 --> 00:02:24,350
But basically what we have here.

33
00:02:24,360 --> 00:02:25,650
Back to Python.

34
00:02:25,650 --> 00:02:29,130
The class object attribute is going to be the same for any instance of a class.

35
00:02:29,130 --> 00:02:30,480
So let's test this theory.

36
00:02:30,630 --> 00:02:32,850
I want to add this class object actually.

37
00:02:32,890 --> 00:02:38,850
Again no I'm not using the self keyword because it's going to be true regardless of the instance I rerun

38
00:02:38,850 --> 00:02:39,580
that cell.

39
00:02:39,810 --> 00:02:47,330
And if I call dog again and do shift tab I still only need to define breed name and spots.

40
00:02:47,670 --> 00:02:59,720
So let's say the breed is again a lab will say the name was the name is equal to Sam and spots will

41
00:02:59,720 --> 00:03:00,490
say false.

42
00:03:00,500 --> 00:03:02,570
Sammy doesn't have spots.

43
00:03:02,570 --> 00:03:03,500
So we run this.

44
00:03:03,500 --> 00:03:05,700
Now I have my dog ready to go.

45
00:03:05,750 --> 00:03:06,710
It's still a dog.

46
00:03:06,830 --> 00:03:13,370
And if I look at that tab here I can see Breede name and species so species even though we didn't define

47
00:03:13,370 --> 00:03:13,630
it.

48
00:03:13,730 --> 00:03:15,020
It's always available to us.

49
00:03:15,140 --> 00:03:20,480
And this is going to be the same mammal regardless of the instance of my dog so it doesn't matter what

50
00:03:20,480 --> 00:03:25,930
it passes for breed name or spots because this attribute is not connected to self.

51
00:03:25,940 --> 00:03:28,200
It's not connected to any particular instance.

52
00:03:28,250 --> 00:03:30,320
So it's a class object attribute.

53
00:03:30,320 --> 00:03:35,240
And the way we form at it is we have the classic word the name of the class and then any class object

54
00:03:35,270 --> 00:03:36,110
attributes.

55
00:03:36,110 --> 00:03:40,580
And then you have your special in it method for user defined attributes.

56
00:03:40,580 --> 00:03:45,440
OK so those are the basics of attributes mainly from the previous lecture for you the final attributes

57
00:03:45,740 --> 00:03:47,930
and then we have class object attributes.

58
00:03:47,930 --> 00:03:54,620
What I want to do now is actually discuss methods methods are essentially functions defined inside the

59
00:03:54,620 --> 00:04:00,560
body of the class and they're used to perform operations that sometimes utilize the actual attributes

60
00:04:00,650 --> 00:04:02,340
of the object we created.

61
00:04:02,360 --> 00:04:07,190
So you can basically think of methods as functions acting on an object that take the object itself into

62
00:04:07,190 --> 00:04:11,760
account through the use of the self argument or self keyword.

63
00:04:12,050 --> 00:04:14,770
So it will do here is create our first method.

64
00:04:14,990 --> 00:04:19,570
So again methods unlike attributes are more like actions.

65
00:04:19,620 --> 00:04:28,220
So these are operations slash actions and we can think of these formally as methods and they basically

66
00:04:28,220 --> 00:04:31,000
look like functions inside of a class.

67
00:04:31,040 --> 00:04:35,580
And a lot of times beginners ask me hey what's the difference between a function and a method.

68
00:04:35,870 --> 00:04:42,410
Basically a method is a function that is inside of a class that will actually work with the object in

69
00:04:42,410 --> 00:04:44,340
some way.

70
00:04:44,390 --> 00:04:49,240
So let's define the F and let's define an action for this dog to take.

71
00:04:49,280 --> 00:04:55,560
For example maybe our dog could bark and all we're going to do is say self.

72
00:04:55,610 --> 00:04:58,460
That way we connect this to the actual object.

73
00:04:58,490 --> 00:04:59,980
So the dog can bark.

74
00:05:00,440 --> 00:05:06,560
And what's going to happen is when you call the bark on the actual dog instance it's going to print

75
00:05:06,560 --> 00:05:07,210
something.

76
00:05:07,390 --> 00:05:09,460
Let's have it print what.

77
00:05:10,110 --> 00:05:14,840
And let's go ahead and delete the name or let's delete the spots attribute.

78
00:05:14,870 --> 00:05:19,280
So we're not dealing with so many attributes here or delete spots here as well.

79
00:05:19,400 --> 00:05:24,030
And I'm going to rerun this so I'll rerun the cell.

80
00:05:24,470 --> 00:05:30,300
And now if we take a look at my dog tab here it only needs to breed and the name.

81
00:05:30,530 --> 00:05:32,380
So let's define a breed for this dog again.

82
00:05:32,390 --> 00:05:34,040
We're going to say lab.

83
00:05:34,040 --> 00:05:38,600
Notice now I'm not bothered passing the actual argument name there and just putting them in order.

84
00:05:38,780 --> 00:05:40,970
And then the next one was the name of the dog.

85
00:05:40,970 --> 00:05:48,710
So we'll have the name of the dog the Frankie say rerun this and now I have my Franki which has a lab

86
00:05:48,710 --> 00:05:49,380
here.

87
00:05:49,580 --> 00:05:51,100
And the type is dog.

88
00:05:51,370 --> 00:05:53,960
And if we take a look at the species it's still a mammal.

89
00:05:54,170 --> 00:05:57,030
And now the name of the dog is Franki.

90
00:05:57,080 --> 00:06:02,250
So one of the key differences between attributes and methods is in the way we called them notice that

91
00:06:02,270 --> 00:06:07,400
attributes never had open in close parentheses and that's because attributes aren't really something

92
00:06:07,400 --> 00:06:08,960
that you execute.

93
00:06:08,960 --> 00:06:12,710
Instead it's just something that's a characteristic of the object that you call back.

94
00:06:12,710 --> 00:06:15,860
So it's just information there about the actual dog.

95
00:06:15,890 --> 00:06:19,920
And I'm going to zoom out one level so we can see everything together here.

96
00:06:19,940 --> 00:06:25,010
So as I mentioned for the attributes themselves there's nothing to actually execute.

97
00:06:25,010 --> 00:06:27,050
Now for methods that's a little different.

98
00:06:27,110 --> 00:06:30,130
And in fact methods will need to be executed.

99
00:06:30,200 --> 00:06:34,280
So that means we need to have the open and close parentheses when we do a method call.

100
00:06:34,670 --> 00:06:36,510
And I'll show you why.

101
00:06:36,560 --> 00:06:44,070
So right now I have my dog it my dog is a mammal it's a lab and it's shrinky and I want my dog to bark

102
00:06:45,240 --> 00:06:49,430
if I just call bark right now with no open close princes.

103
00:06:49,530 --> 00:06:54,630
You'll get back something that looks like this and this basically says hey you have this method that's

104
00:06:54,630 --> 00:07:00,180
bound to your object dog at this location at your computer's memory.

105
00:07:00,330 --> 00:07:06,120
If you actually want to execute the bark method you have open and close print sees you run that and

106
00:07:06,120 --> 00:07:08,670
then you get back whatever the actual method does.

107
00:07:08,670 --> 00:07:10,860
In this case it's just printing out Wolf.

108
00:07:11,010 --> 00:07:13,070
So that's how a method works.

109
00:07:13,080 --> 00:07:16,700
It's basically just an action that the actual object can take.

110
00:07:16,860 --> 00:07:21,750
In this case it didn't really do anything besides the print statement but usually methods are going

111
00:07:21,750 --> 00:07:24,680
to use information about the object itself.

112
00:07:24,840 --> 00:07:29,610
And we know that information about a particular instance of the object could be something like the name.

113
00:07:29,640 --> 00:07:32,310
So let's have the dog bark its own name.

114
00:07:32,310 --> 00:07:43,900
In this case we'll say Wolf and here we're going to see my name is and we'll use that format here to

115
00:07:43,900 --> 00:07:46,570
pass in the actual dog's name.

116
00:07:46,600 --> 00:07:50,710
Now a really common mistake that people do when they're first learning about object oriented programming

117
00:07:51,190 --> 00:07:55,150
is they just passen name however or working with objects.

118
00:07:55,150 --> 00:07:56,350
That's actually not good enough.

119
00:07:56,350 --> 00:08:01,630
What we need to do is reference the particular instance of the dog's name meaning we need to reference

120
00:08:01,750 --> 00:08:07,810
not name but self-taught name because when you pass the name at the beginning it gets connected to the

121
00:08:07,810 --> 00:08:10,620
object through the use of this self keyword.

122
00:08:10,630 --> 00:08:13,630
So here we need to say self-taught name.

123
00:08:13,780 --> 00:08:20,030
And that's going to when you run bark it's going say Prince Wolf my name is and it's going to say hey

124
00:08:20,260 --> 00:08:25,900
what is this dog's name for this particular instance of the dog object find its name and it does that

125
00:08:25,900 --> 00:08:31,000
through the self-taught named call and then we'll just insert it automatically because we only have

126
00:08:31,000 --> 00:08:33,120
one curly brace there.

127
00:08:33,480 --> 00:08:35,660
So we run that and we're using print formatting there.

128
00:08:35,710 --> 00:08:41,210
You can always review case you're unfamiliar with this format call again I'll say LaFranchi rerun all

129
00:08:41,210 --> 00:08:44,730
of this to make sure that the bark operation has been set.

130
00:08:45,020 --> 00:08:49,030
And now when we call bark it's going to say Wolf my name is Frankie.

131
00:08:49,030 --> 00:08:54,460
So it's able to actually reference things in the attribute sector.

132
00:08:54,460 --> 00:08:59,520
So you're able to say self-taught name and pass that in successfully into bark.

133
00:08:59,620 --> 00:09:04,930
Notice here how we call the self automatically that way in case we ever need to reference things it

134
00:09:04,930 --> 00:09:07,830
knows what instance of the object that's talking about.

135
00:09:08,350 --> 00:09:15,260
Now methods can take outside arguments so we can pass it some other argument here.

136
00:09:15,280 --> 00:09:24,220
So we could say on some number so we could do here is when you call Barch you need to pass in some numbers

137
00:09:24,220 --> 00:09:36,920
will say Wolf my name is blank and the number is and will say comma number.

138
00:09:36,960 --> 00:09:38,040
So note the difference here.

139
00:09:38,070 --> 00:09:43,050
I'm no longer saying self-taught number because number is already being provided for us when we actually

140
00:09:43,050 --> 00:09:44,300
call bark.

141
00:09:44,310 --> 00:09:47,260
So that means when he's actually pasan a number call.

142
00:09:47,370 --> 00:09:51,480
So I'm going to again rerun everything to make the changes to bark here.

143
00:09:51,820 --> 00:09:55,100
Rerun Franki rerun this rerun that.

144
00:09:55,470 --> 00:10:00,970
And now my dog if I hit tab here has bark breed name and species.

145
00:10:01,050 --> 00:10:06,090
So we're going to do is called bark and if I just call bark like this.

146
00:10:06,090 --> 00:10:10,170
So let me zoom in quickly home delete the old line here.

147
00:10:10,290 --> 00:10:15,050
So if I just call bark like this I'm not going to get an air that's going to say hey barks missing one

148
00:10:15,060 --> 00:10:17,700
required position or argument number.

149
00:10:17,700 --> 00:10:24,660
So now this method is expecting some sort of added argument number because when the dog barks it's going

150
00:10:24,660 --> 00:10:30,660
to say its name and the number you passed because I'm passing an errand here with number again note

151
00:10:30,660 --> 00:10:30,920
here.

152
00:10:30,930 --> 00:10:36,180
Now I'm not using self that number because the user is provided that it's not referencing some particular

153
00:10:36,210 --> 00:10:40,640
attribute for that instance of the class like the breed or that name.

154
00:10:40,710 --> 00:10:44,130
So let's give it a number to bark for us all say 10.

155
00:10:44,280 --> 00:10:48,080
Run that and then it says Wolf my name is Frankie and the number is 10.

156
00:10:48,420 --> 00:10:52,110
And those are the basics of using a method for class.

157
00:10:52,170 --> 00:10:54,790
You say DMF the name of your method.

158
00:10:54,870 --> 00:10:55,970
Now we're using lowercase.

159
00:10:55,970 --> 00:11:01,160
It follows the conventions of a normal function call except the inside of a class and then you pass

160
00:11:01,160 --> 00:11:08,010
and always the self keyword and then you can work with other attributes by referencing them in a self

161
00:11:08,220 --> 00:11:09,030
manner.

162
00:11:09,060 --> 00:11:14,910
Or you can pass new arguments that the user provides and reference those as well no longer needing the

163
00:11:14,910 --> 00:11:18,400
self call because it's not connected to a particular instance of the class.

164
00:11:18,420 --> 00:11:21,440
It's just something that the user provides for you when they make the call.

165
00:11:22,320 --> 00:11:22,810
OK.

166
00:11:23,040 --> 00:11:24,320
So we went over methods right.

167
00:11:24,330 --> 00:11:28,360
Now I'm going to start a new class just to really drive the point home.

168
00:11:28,740 --> 00:11:34,320
So make a couple of new cells here and I'm going to create a class called circle.

169
00:11:34,540 --> 00:11:39,880
So my circle class let's give it a class object attribute.

170
00:11:39,960 --> 00:11:45,720
I remember a class object attribute is something that should be true when you have any instance of the

171
00:11:45,720 --> 00:11:46,410
class.

172
00:11:46,410 --> 00:11:52,740
So for example I know that pi is going to be equal to more or less three point one for no matter how

173
00:11:52,740 --> 00:11:53,780
big a circle is.

174
00:11:53,790 --> 00:11:56,320
So I will set that as a class object attribute.

175
00:11:56,400 --> 00:12:02,180
So regardless of the instance of a circle I make I will always be able to reference PI up here is three

176
00:12:02,180 --> 00:12:07,160
point 1 4 so now we create our init method.

177
00:12:07,270 --> 00:12:10,660
So a special method we always passen self.

178
00:12:10,930 --> 00:12:14,430
So it's very common to forget that when you're first starting to work with objects but you always pasand

179
00:12:14,440 --> 00:12:15,130
self.

180
00:12:15,190 --> 00:12:18,150
And now whatever we need to define a circle.

181
00:12:18,220 --> 00:12:24,400
So an argument we may need to define a circle is a radius and just like when we learn about functions

182
00:12:24,490 --> 00:12:27,170
we can actually provide default parameters here.

183
00:12:27,310 --> 00:12:30,910
So we'll find a default parameter of one.

184
00:12:31,000 --> 00:12:42,040
So it's a default value for the radius and also a self-taught radius is equal to the radius that was

185
00:12:42,040 --> 00:12:46,570
passed in and then I'm going to make a method here.

186
00:12:46,710 --> 00:12:58,510
So now we have our method and we'll say this method is called Get circumference.

187
00:12:58,740 --> 00:13:05,190
And what this does is it has itself so can reference the particular instance of the object and it's

188
00:13:05,190 --> 00:13:07,550
going to know I'm returning it on the printing.

189
00:13:07,550 --> 00:13:11,210
Now I'm going to return the actual circumference of the circle.

190
00:13:11,250 --> 00:13:16,530
So if you do a little bit of math you know that circumference of a circle is the radius of that circle

191
00:13:17,250 --> 00:13:21,670
times pi times 2.

192
00:13:22,090 --> 00:13:26,220
So 2 pi r is the circumference of a circle.

193
00:13:26,680 --> 00:13:27,410
I run that.

194
00:13:27,530 --> 00:13:37,690
Let's create an instance of the circle so I can say My circle is equal to circle and then when I call

195
00:13:37,690 --> 00:13:45,980
my circle here and hit dot tab I can't have circumference and I can actually see here I have pie.

196
00:13:46,270 --> 00:13:49,150
And here I have radious that actually spelled circumference wrong.

197
00:13:49,150 --> 00:13:51,440
But let's actually fix that real quick.

198
00:13:51,490 --> 00:14:01,660
So come for now we run this rerun circle tab and I see it gets to conference PI and radius.

199
00:14:01,690 --> 00:14:02,650
So it's called pi.

200
00:14:02,650 --> 00:14:03,670
See what happens.

201
00:14:03,820 --> 00:14:09,340
You get back three point 1 4 makes sense the class attribute attribute that's going to be the same regardless

202
00:14:09,340 --> 00:14:11,460
of what instance of the class.

203
00:14:11,640 --> 00:14:18,310
Then I'm going to say my circle dots and let's call it radius and it's one.

204
00:14:18,310 --> 00:14:19,600
So why is radius 1.

205
00:14:19,630 --> 00:14:25,900
Because that was the default value I provided up here what I could have done is provided another value

206
00:14:25,900 --> 00:14:26,750
for radius.

207
00:14:26,770 --> 00:14:32,420
Let's provide maybe 30 for radius here and then it is overwritten default values.

208
00:14:32,430 --> 00:14:38,280
So when I call radious again after rerunning that assignments I have 30.

209
00:14:38,320 --> 00:14:43,560
So you can have a default value here and you can always overwrite it OK.

210
00:14:43,780 --> 00:14:52,320
So now let's see what happens when we call this method we'll call my circle and I will get the circumference

211
00:14:53,070 --> 00:14:56,850
open close princes run that and it returns back to the circumference.

212
00:14:56,850 --> 00:15:00,860
In this case hundred eighty eight point four.

213
00:15:00,870 --> 00:15:06,150
Now those are the basics of methods and I want to come back up here and show you a couple of more things

214
00:15:06,150 --> 00:15:10,360
that you may consider when working with object oriented programming.

215
00:15:10,560 --> 00:15:17,250
One thing to consider is that if you have an attribute it doesn't necessarily have to be defined from

216
00:15:18,090 --> 00:15:20,150
a particular parameter call.

217
00:15:20,160 --> 00:15:24,040
So for example I can create an attribute for the area.

218
00:15:24,330 --> 00:15:31,220
So a circle can have an area attribute and I can say self-taught area is equal to the radius that's

219
00:15:31,230 --> 00:15:36,180
passed 10 times the radius that's passed in times pi.

220
00:15:36,180 --> 00:15:38,940
So pire squared is the area of a circle.

221
00:15:38,940 --> 00:15:45,610
So I can say self-taught PI and now I will have an area when I define the circle so I can say circle

222
00:15:45,610 --> 00:15:46,560
30.

223
00:15:47,080 --> 00:15:56,740
My circle that pi radius and if I call my circle here and ask for the area back I get back the area

224
00:15:56,800 --> 00:15:58,030
of that circle.

225
00:15:58,350 --> 00:16:03,580
The last thing I want to point out is because this is the class object attribute actually has two ways

226
00:16:03,580 --> 00:16:04,270
of calling it.

227
00:16:04,330 --> 00:16:11,980
I could say self-taught pi because self references a particular instance of this object or something

228
00:16:11,990 --> 00:16:18,520
people sometimes like to do is actually say circle dot pi if it's a class object attribute.

229
00:16:18,520 --> 00:16:23,110
So if it's a class object attribute you can reference it in this manner which is the name of the class

230
00:16:23,230 --> 00:16:24,280
and then the attribute.

231
00:16:24,430 --> 00:16:28,760
Because you know this is going to be the same regardless of the instance of the class.

232
00:16:28,780 --> 00:16:30,800
So you may see it this way as well.

233
00:16:30,910 --> 00:16:34,750
And sometimes programmers prefer it this way because if you're reading someone else's code it's very

234
00:16:34,750 --> 00:16:36,640
easy to see down the line.

235
00:16:36,700 --> 00:16:40,450
Oh this is a reference to a class object attribute.

236
00:16:40,450 --> 00:16:42,650
So maybe obvious here because our class is smaller.

237
00:16:42,670 --> 00:16:46,900
You can imagine if we had hundreds of methods instead of saying self-taught PI here it might be nice

238
00:16:46,900 --> 00:16:48,690
to say circle that pi.

239
00:16:48,700 --> 00:16:52,900
That way when you're on line 101 or whatever it is you can see here.

240
00:16:52,930 --> 00:16:57,740
Oh they're referencing a class object attribute at the very top of this class.

241
00:16:57,790 --> 00:16:59,870
So you run this and you'll get back all the same results.

242
00:16:59,870 --> 00:17:01,630
It'll all work the same.

243
00:17:01,630 --> 00:17:06,910
So again you can use self-taught pi or to really make it clear that it's a class object attribute the

244
00:17:06,910 --> 00:17:09,700
name of the class that the attribute name.

245
00:17:09,700 --> 00:17:10,030
All right.

246
00:17:10,030 --> 00:17:12,160
That's it for the basics of methods.

247
00:17:12,190 --> 00:17:16,220
Coming up next we're going to discuss inheritance and polymorphism.

248
00:17:16,270 --> 00:17:17,410
We'll see you at the next lecture.
