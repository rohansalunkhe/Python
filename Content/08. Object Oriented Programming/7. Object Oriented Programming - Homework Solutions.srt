1
00:00:05,330 --> 00:00:06,300
Welcome back everybody.

2
00:00:06,410 --> 00:00:10,220
Let's not go over the solutions for the homework assignment from the previous lecture.

3
00:00:10,220 --> 00:00:12,900
All right so here we are we first had the line class.

4
00:00:12,920 --> 00:00:14,620
So let's start with that one.

5
00:00:14,630 --> 00:00:17,310
Remember that we're taking in these coordinates as tuples.

6
00:00:17,360 --> 00:00:20,010
So there's lots of different ways you could have done this.

7
00:00:20,230 --> 00:00:24,210
One way is to create two coordinates as attributes.

8
00:00:24,440 --> 00:00:31,200
So we could say self-talk coordinate 1 is equal to the co-ordinate one that's passed in and then self-taught

9
00:00:32,460 --> 00:00:36,680
coordinate 2 is equal to that second coordinate that's passed in.

10
00:00:36,810 --> 00:00:38,080
So this is one way to do it.

11
00:00:38,310 --> 00:00:44,460
And then later on we do it's just tuple and packing to grab the individual components of these tuples.

12
00:00:44,460 --> 00:00:49,230
So for example in the distance formula we know that distance formula at the end of the day should look

13
00:00:49,230 --> 00:00:50,370
something like this.

14
00:00:50,560 --> 00:01:04,910
It should be X to minus x 1 to the power of two and then we'll say plus y to minus y one to the power

15
00:01:04,910 --> 00:01:05,390
of two.

16
00:01:05,420 --> 00:01:07,280
And then this whole thing squared.

17
00:01:07,280 --> 00:01:10,520
So one way to do that is just to the power of 0.5.

18
00:01:10,550 --> 00:01:15,470
So that's one way to take that distance formula and in order to get X to an x 1 what we could've done

19
00:01:15,590 --> 00:01:17,360
is just tuple and packing here.

20
00:01:17,530 --> 00:01:29,440
I could add x y and y one is equal to self dot coordinate one and then x to y two is equal to self-taught

21
00:01:30,600 --> 00:01:31,690
according to.

22
00:01:32,130 --> 00:01:33,440
So you could have done it this way.

23
00:01:33,480 --> 00:01:37,920
The other approach you could have taken is just right off the bat when you actually create attributes

24
00:01:37,960 --> 00:01:39,780
is create an attribute for each of these.

25
00:01:39,810 --> 00:01:45,570
You could say self-taught X1 is equal to co-ordinate one and then maybe use some indexing here.

26
00:01:45,660 --> 00:01:48,440
It's really up to you what approach you want to take.

27
00:01:48,690 --> 00:01:54,180
The main idea being that eventually you have to realize you need to somehow extract the actual components

28
00:01:54,270 --> 00:01:58,150
of that tuple either through indexing or fruit tuple and packing.

29
00:01:58,230 --> 00:02:02,100
And this is something that I wanted you to get practice with because often you're not going to just

30
00:02:02,100 --> 00:02:05,270
passen single variables into your argument calls.

31
00:02:05,280 --> 00:02:07,230
They may be something like a list or a tuple.

32
00:02:07,260 --> 00:02:11,100
So you should have classes that are able to work with that.

33
00:02:11,130 --> 00:02:16,550
Finally let's have the slope equation that we can just copy and paste these lines from up here

34
00:02:19,520 --> 00:02:22,220
and then for the slope we just return.

35
00:02:22,220 --> 00:02:23,040
Rise over run.

36
00:02:23,060 --> 00:02:33,020
So rise is white to minus y one and then we're going to divide that by X to minus X1 will run that and

37
00:02:33,020 --> 00:02:34,420
then let's check this out.

38
00:02:34,430 --> 00:02:39,020
So we'll say see one is equal to three to co-ordinate.

39
00:02:39,020 --> 00:02:42,580
See two is equal to a 10 coordinate.

40
00:02:42,710 --> 00:02:51,780
I'll create my line object and say pass and see one and see two here and I'll check out the method calls

41
00:02:52,550 --> 00:02:54,030
will say distance.

42
00:02:54,030 --> 00:02:56,450
Open Closed print sees because we execute that.

43
00:02:56,790 --> 00:02:58,090
And that looks like it matches up.

44
00:02:58,230 --> 00:03:04,050
And then we say my line check the slope and looks like that matches up as well.

45
00:03:04,100 --> 00:03:09,380
If you're getting different numbers for distance and slope given these two coordinates double check

46
00:03:09,470 --> 00:03:10,820
your actual equations here.

47
00:03:10,820 --> 00:03:14,930
You may have mixed up x 1 and x 2 or Y tune y 1.

48
00:03:14,930 --> 00:03:16,240
So keep that in mind.

49
00:03:16,280 --> 00:03:18,940
These should be the results you get for distance and slope.

50
00:03:18,950 --> 00:03:21,400
Given these coordinates so definitely check your math.

51
00:03:21,500 --> 00:03:26,120
If you're not getting those same results it's really easy to make a typo here or you print season the

52
00:03:26,120 --> 00:03:26,920
wrong way.

53
00:03:27,140 --> 00:03:32,630
So just keep that in mind as you continue now we're going to move along to this cylinder class.

54
00:03:32,660 --> 00:03:34,300
So as I mentioned we were going over the homework.

55
00:03:34,310 --> 00:03:37,340
This was actually a little simpler than that line class.

56
00:03:37,400 --> 00:03:41,650
So here we have high end radius of default high end radius.

57
00:03:41,720 --> 00:03:47,830
What I'm going to do is make those bouffe attributes straight up since are integers being passed in.

58
00:03:47,960 --> 00:03:54,860
So self-taught high attribute is equal to height and then the instances radius is going to be equal

59
00:03:54,860 --> 00:03:58,310
to the radius that is passed then.

60
00:03:58,730 --> 00:04:03,350
And then we're going to just return the volume equation the volume for a cylinder.

61
00:04:03,350 --> 00:04:04,430
You could have googled this.

62
00:04:04,430 --> 00:04:07,730
It's basically just Height's times Pi.

63
00:04:07,870 --> 00:04:12,350
It could have also then a class object attribute there but I'll just pasand three point 1 4 and then

64
00:04:12,350 --> 00:04:14,300
it's going to be the radius.

65
00:04:14,360 --> 00:04:16,480
So self-taught radius.

66
00:04:16,640 --> 00:04:17,620
So the power of two.

67
00:04:17,670 --> 00:04:22,500
And notice here how I'm using Salta high and self-taught radius inside these method calls because I

68
00:04:22,500 --> 00:04:26,670
need to reference the instance of the object's own height and own radius.

69
00:04:26,670 --> 00:04:28,730
I can't just reference high in radius by itself.

70
00:04:28,740 --> 00:04:33,170
I need that self-promoter then for surface area.

71
00:04:33,250 --> 00:04:35,430
This one's a little more complicated a formula.

72
00:04:35,620 --> 00:04:41,140
Basically you have the top and bottom of the cylinder which is pire squared.

73
00:04:41,320 --> 00:04:50,300
So I'm going to say that the top of the cylinder is Pi squared so self radius to the power of two.

74
00:04:50,420 --> 00:04:51,580
There's a top and bottom.

75
00:04:51,580 --> 00:04:59,490
So for the whole surface area I'm going to return to Time's top one for the top one for the bottom and

76
00:04:59,490 --> 00:05:10,010
then plus the rest of the formula which is two times pi times self-taught radius times self-taught height

77
00:05:11,360 --> 00:05:15,110
and the formulas basically just grabbing that middle section of the cylinder and then flattening it

78
00:05:15,110 --> 00:05:15,350
out.

79
00:05:15,350 --> 00:05:17,290
So you actually get a rectangle.

80
00:05:18,050 --> 00:05:18,590
OK.

81
00:05:18,770 --> 00:05:19,970
So that's surface area.

82
00:05:20,000 --> 00:05:23,400
Let's run this and make sure that our results match up.

83
00:05:23,450 --> 00:05:28,500
So say my cylinder or my SIL call the cylinder here.

84
00:05:28,550 --> 00:05:31,910
Two three will say see the volume

85
00:05:34,790 --> 00:05:42,760
and it looks like I have an error named see whoops my cylinder not see fifty six point five to perfect.

86
00:05:42,770 --> 00:05:47,100
And let's check out my cylinder surface area or on that.

87
00:05:47,150 --> 00:05:48,890
And we have ninety four point two.

88
00:05:49,320 --> 00:05:49,800
OK.

89
00:05:49,800 --> 00:05:54,000
So this should have been pretty straightforward especially because it's very similar to the Circle class

90
00:05:54,360 --> 00:05:55,950
if you're having any issues on this.

91
00:05:55,950 --> 00:05:59,220
Or there's something that's confusing to you as far as create these methods.

92
00:05:59,250 --> 00:06:02,910
Please post the Kewney forums and we're happy to give further explanation.

93
00:06:02,910 --> 00:06:06,340
I'm going to give a quick overview of what we just did here.

94
00:06:06,540 --> 00:06:07,960
So let's start the line class.

95
00:06:08,010 --> 00:06:11,370
First thing you have to realize is that these coordinates are being passed and it's tuples and you have

96
00:06:11,370 --> 00:06:13,450
various options of how you want to work with these here.

97
00:06:13,560 --> 00:06:17,350
Either you can do the unpacking in your actual attributes.

98
00:06:17,370 --> 00:06:20,870
So then you can have self-taught X-1 self-taught x 2 and so on.

99
00:06:20,940 --> 00:06:26,010
Or once you assign these coordinates you can do the unpacking and the methods themselves again totally

100
00:06:26,010 --> 00:06:32,610
up to you then just make sure that you're applying these equations correctly for both distance and slope.

101
00:06:34,260 --> 00:06:36,310
For the cylinder it's a very similar thing.

102
00:06:36,330 --> 00:06:40,950
We assign the attributes height and radius and then we just return back the actual mathematical formulas

103
00:06:41,220 --> 00:06:44,010
for volume and surface area of a cylinder.

104
00:06:44,010 --> 00:06:44,530
All right.

105
00:06:44,700 --> 00:06:45,510
Thanks everyone.

106
00:06:45,510 --> 00:06:49,430
Again if you have any questions posed to the Kewney forums and we're happy to help you out there.

107
00:06:49,440 --> 00:06:50,440
We'll see you at the next lecture.
