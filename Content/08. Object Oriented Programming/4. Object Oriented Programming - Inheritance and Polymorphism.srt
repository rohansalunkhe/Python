1
00:00:05,330 --> 00:00:10,460
Welcome back everyone to part three on our discussion of object oriented programming in this lecture.

2
00:00:10,460 --> 00:00:16,850
We're going to specifically focus on inheritance and polymorphism and inheritance is basically a way

3
00:00:16,850 --> 00:00:20,050
to form new classes using classes that have already been defined.

4
00:00:20,240 --> 00:00:23,240
Let's jump to Jupiter notebook and see how this works.

5
00:00:23,240 --> 00:00:28,070
OK so let's begin by talking about inheritance and as we just mentioned it's a way to form new classes

6
00:00:28,370 --> 00:00:33,230
using classes that have already been defined and really important benefits of inheritance or the ability

7
00:00:33,230 --> 00:00:38,950
to reuse code they've already worked on and to reduce the complexity of a program.

8
00:00:38,990 --> 00:00:43,390
So I'll start off by creating what's called a base class.

9
00:00:43,520 --> 00:00:49,880
So this base class is going to be called animal and squit to be a very simple class.

10
00:00:49,920 --> 00:00:56,490
In fact init method is actually going to take in any arguments instead of it's going to do is Prince

11
00:00:57,830 --> 00:01:00,590
animal created.

12
00:01:00,710 --> 00:01:02,640
So we have a very simple class here.

13
00:01:02,660 --> 00:01:08,850
So for instance if I made an instance of animal it would just print out animal created because in that

14
00:01:08,850 --> 00:01:13,370
method it gets automatically executed when you actually create an animal.

15
00:01:13,860 --> 00:01:15,590
Let's get on two more methods.

16
00:01:15,750 --> 00:01:18,350
One is called Who am I.

17
00:01:22,020 --> 00:01:34,220
And all this is going to do is print out I am an animal and will have another method here called Eat.

18
00:01:34,420 --> 00:01:40,730
And this is going to print out I am eating.

19
00:01:40,900 --> 00:01:47,420
OK so we can see that if we have an animal here can do dot tab and we can ask it to eat.

20
00:01:47,450 --> 00:01:51,040
So open close Princie is to actually execute eating and see print out.

21
00:01:51,070 --> 00:01:55,210
I am eating and the other one was who am I.

22
00:01:55,270 --> 00:01:56,570
And I'll report back.

23
00:01:56,720 --> 00:01:58,030
I am an animal.

24
00:01:58,030 --> 00:02:06,970
This is going to serve as our base class so newly formed classes can use the Animal class in order to

25
00:02:06,970 --> 00:02:08,420
inherit some of its methods.

26
00:02:08,420 --> 00:02:09,700
You may want to use again.

27
00:02:10,030 --> 00:02:17,330
So for example if I want to create another instance of the dog class or actually recreate that dog class

28
00:02:19,450 --> 00:02:21,300
so I'm going to recreate that class.

29
00:02:21,310 --> 00:02:26,920
But I started thinking hey a lot of the features and methods of my Animal class are useful to a dog.

30
00:02:26,920 --> 00:02:29,140
For instance dogs they like to eat.

31
00:02:29,200 --> 00:02:35,500
So maybe I want to reduce some of my prior work in my new class the way I can do that is by inheriting

32
00:02:35,980 --> 00:02:38,450
the base classes Animal class.

33
00:02:38,680 --> 00:02:41,230
So I'm going to pass an animal here.

34
00:02:41,230 --> 00:02:42,690
Before we weren't passing anything.

35
00:02:42,700 --> 00:02:48,030
But now if I pass an animal I'm going to inherit or derive from this base class.

36
00:02:48,020 --> 00:02:53,920
So this is now known as a derived class because I'm deriving some of my features from this base class

37
00:02:53,920 --> 00:02:55,150
of animal.

38
00:02:55,330 --> 00:02:58,510
So I always say dog inherits animal.

39
00:02:58,900 --> 00:03:08,350
And then in my in it call what I'm going to do is call animal dot underscore underscore in its core

40
00:03:08,520 --> 00:03:17,500
underscore self so I'm going to actually create an instance of the Animal class when I create an instance

41
00:03:17,500 --> 00:03:24,020
of my dog class and I am able to do that because I am inheriting from the Animal class and I'm also

42
00:03:24,020 --> 00:03:29,520
going to do is print out dog created.

43
00:03:29,590 --> 00:03:30,850
So let me run that Annmarie.

44
00:03:30,910 --> 00:03:32,940
Alt enters the cell right below.

45
00:03:33,190 --> 00:03:39,910
So I have my base class animal and then I have my other class of dog which is now going to inherit the

46
00:03:39,910 --> 00:03:47,780
methods of the animal because inside my method I called animal in it method self.

47
00:03:47,790 --> 00:03:49,370
So now let's see this in action.

48
00:03:49,410 --> 00:03:56,930
I'm going to say My dog is equal to dog and I run this and I see that an animal is created because when

49
00:03:56,930 --> 00:04:04,070
I run this line of dog this method gets called which in turn runs the init method off animal which in

50
00:04:04,070 --> 00:04:05,650
turn prints animal created.

51
00:04:05,870 --> 00:04:10,670
And then after that's created I print out dog crated But here's where it gets interesting.

52
00:04:10,670 --> 00:04:17,550
All those old methods that were available for the animal who a mind eats are now available for my dog.

53
00:04:17,570 --> 00:04:24,350
So even though inside this dog class I don't have eat or who am I find I'm still able to use them because

54
00:04:24,350 --> 00:04:27,580
I was able to drive them from my base class of animal.

55
00:04:27,740 --> 00:04:29,740
And that's the main idea behind inheritance.

56
00:04:29,780 --> 00:04:34,620
I can now reuse methods that should be common between classes by just inheriting.

57
00:04:34,610 --> 00:04:38,540
So I don't need to rewrite all these methods for my new class.

58
00:04:38,540 --> 00:04:43,110
Now you may be wondering well what if I want to overwrite one of the older methods.

59
00:04:43,160 --> 00:04:44,780
Well that's actually no problem.

60
00:04:44,780 --> 00:04:51,770
All you need to do is say DPF in your new class and then just make sure to use the same name as the

61
00:04:51,770 --> 00:04:53,860
old class method such as who am I.

62
00:04:55,260 --> 00:05:03,180
And now instead of saying I'm an animal we're going to print out I am a dog.

63
00:05:03,540 --> 00:05:09,210
So rerun this to make sure we read the fine the rerun of my dog and it still says animal created dog

64
00:05:09,220 --> 00:05:09,880
created.

65
00:05:10,140 --> 00:05:11,850
I can still call eat.

66
00:05:12,350 --> 00:05:21,290
But now when I call my dog who am I it says I am a dog before it would have said I am an animal.

67
00:05:21,310 --> 00:05:22,830
So let's test that theory.

68
00:05:22,990 --> 00:05:25,360
I'm going to delete this.

69
00:05:25,360 --> 00:05:27,620
Who am I rerun this rerun.

70
00:05:27,640 --> 00:05:33,700
This will create our dogs again animal created dog created eat if Now if I say Who am I.

71
00:05:33,700 --> 00:05:35,870
Notice I didn't overwrite it this time.

72
00:05:35,930 --> 00:05:37,840
He'll just say I am an animal sir.

73
00:05:37,870 --> 00:05:41,080
Able to overwrite the previous methods of your base class.

74
00:05:41,110 --> 00:05:44,940
In case you need to you're also able to add on methods.

75
00:05:45,490 --> 00:05:59,380
So we're going to say the F Barch self and then print out Wolf so we can add methods and we can overwrite.

76
00:05:59,450 --> 00:06:01,600
So let me copy this method here.

77
00:06:04,750 --> 00:06:10,750
And paste it in and I won't say I am a dog eating.

78
00:06:11,380 --> 00:06:12,740
So I run this again.

79
00:06:13,630 --> 00:06:20,650
I recreate the instance of my dog here animal created dog created and now if I call eat it says I am

80
00:06:20,650 --> 00:06:23,650
a dog in eating because I overwrote the original method.

81
00:06:23,650 --> 00:06:28,660
I can still call him my because I inherited from that base class and it says I am an animal because

82
00:06:28,660 --> 00:06:30,740
I no longer overwrote it here.

83
00:06:30,940 --> 00:06:38,800
You can add in the method again call and then rerun this but now I also bark so I could say my dog bark

84
00:06:39,200 --> 00:06:44,830
and will print out what the main ideas here is that all you need to do is inherit from your base class

85
00:06:45,130 --> 00:06:50,350
and now you have access to all these old methods that you wanted and if you ever want to overwrite them

86
00:06:50,440 --> 00:06:52,510
you can just overwrite them as so.

87
00:06:52,660 --> 00:06:58,280
Recall that same method name and then do whatever you want there and you can always add in more methods.

88
00:06:58,480 --> 00:07:03,250
So that's how inheritance works and let's move on to discussing polymorphism.

89
00:07:03,250 --> 00:07:08,320
So we just learned that while functions can take in different arguments methods belong to the objects

90
00:07:08,320 --> 00:07:09,760
they act on.

91
00:07:09,760 --> 00:07:13,700
Let's finish this lecture by talking about polymorphism in Python.

92
00:07:13,840 --> 00:07:19,470
Polymorphism refers to the way in which different object classes can share the same method name.

93
00:07:19,720 --> 00:07:24,340
And then those methods can be called from the same place even though a variety of different objects

94
00:07:24,430 --> 00:07:25,640
might be passed in.

95
00:07:25,870 --> 00:07:28,350
The best way to explain this is an example.

96
00:07:28,750 --> 00:07:31,850
I'm going to create two new classes.

97
00:07:32,020 --> 00:07:40,040
I'm going to redefine my dog class and then also create a similar cat class will say D F in it's and

98
00:07:40,040 --> 00:07:43,110
the dog just takes in a name so you can name your dog.

99
00:07:43,520 --> 00:07:50,930
So we assign the dog's name attribute the name that's passed then and then the dog is going to be able

100
00:07:50,930 --> 00:07:58,650
to speak and in this case when it speaks it's just going to return its own name.

101
00:07:58,680 --> 00:08:06,320
So self-taught name plus or say says what if.

102
00:08:06,380 --> 00:08:11,540
So when the dog actually speaks it reports Beko to every name you've signed and then says Wolf and I'm

103
00:08:11,540 --> 00:08:13,210
going to copy this.

104
00:08:13,640 --> 00:08:19,580
Make sure I run the cell and right below it I'm going to create a very similar cat class.

105
00:08:19,580 --> 00:08:21,350
Same deal fits in it method.

106
00:08:21,380 --> 00:08:29,290
You provide a name but instead of speaking says Wolf we're going to say speak says meow.

107
00:08:29,310 --> 00:08:34,310
So almost the same class except the cat says meow and the dog says Wolf.

108
00:08:34,680 --> 00:08:35,050
OK.

109
00:08:35,100 --> 00:08:37,610
So I'm going to zoom out so we can see a little more of our code.

110
00:08:37,620 --> 00:08:40,400
We've defined the dog class and the cat class.

111
00:08:40,470 --> 00:08:49,180
Now it's create two instances of them will say Nico is a dog with the name Neco and we'll say Felix

112
00:08:50,200 --> 00:08:54,910
is a variable name for cat string name Felix.

113
00:08:55,560 --> 00:09:00,140
So I'm going to print out what happens when Nico speaks.

114
00:09:01,850 --> 00:09:03,140
So says Wolf.

115
00:09:03,220 --> 00:09:07,440
Looks like I forgot a space here so let's clean that up a little bit.

116
00:09:07,480 --> 00:09:09,800
We run these two cells.

117
00:09:09,880 --> 00:09:10,800
Nico says Wolf.

118
00:09:10,810 --> 00:09:11,600
Nice.

119
00:09:11,620 --> 00:09:18,640
And we have a little space there and then have Felix also speak notice here I'm actually executing speak

120
00:09:18,640 --> 00:09:21,630
with open close princes and Felix says meow.

121
00:09:21,640 --> 00:09:24,060
So here we have a dog class and a cat class.

122
00:09:24,070 --> 00:09:30,610
Each of them has the speak method when called each object speak method returns a result that's unique

123
00:09:30,610 --> 00:09:36,060
to the object that is to say it's unique for that dog to say Wolf and it's unique to the cat to say

124
00:09:36,070 --> 00:09:41,190
now as well as their names are going to be unique to that particular instance of the class.

125
00:09:41,200 --> 00:09:43,930
Now there's a few different ways to demonstrate polymorphism.

126
00:09:43,930 --> 00:09:55,400
One of them that's pretty simple is if a for loop so I can say for class in an hour I'm going to say

127
00:09:55,430 --> 00:10:03,460
Nico and Felix and what I'm going to do is I'm going to iterate through this list of items and I want

128
00:10:03,460 --> 00:10:04,430
you to notice something here.

129
00:10:04,450 --> 00:10:12,190
I'm going to say prints the type of pet class and in fact I'm just going to say pet.

130
00:10:12,220 --> 00:10:13,980
So that way don't confuse you.

131
00:10:14,470 --> 00:10:17,020
So say for every pet in this list.

132
00:10:17,170 --> 00:10:19,510
Print what type of pet it is.

133
00:10:19,510 --> 00:10:20,760
So what type of class it is.

134
00:10:20,800 --> 00:10:26,050
And then also print what happens when you say pet that speak.

135
00:10:26,400 --> 00:10:29,550
So I run this and this is an example of polymorphism.

136
00:10:29,580 --> 00:10:34,840
So both Nico and Felix share the same method name called speak.

137
00:10:35,070 --> 00:10:38,100
However there are different types here.

138
00:10:38,380 --> 00:10:43,900
So we have main dog and then main cat and let me remove this type call there for returning a string

139
00:10:44,320 --> 00:10:49,180
but it then should be noted that one of them saying Wolf and one of them is saying Meow.

140
00:10:49,200 --> 00:10:52,940
So another way to show this besides iteration is through a function.

141
00:10:53,130 --> 00:10:57,240
And this is probably the more common way to see it being used.

142
00:10:57,420 --> 00:11:01,170
And I can say the F pet speak.

143
00:11:01,170 --> 00:11:05,970
So this takes in some pet it doesn't really care if it's a dog or cat because what it's going to do

144
00:11:06,060 --> 00:11:12,000
inside of the function is just take that object and then call it to speak.

145
00:11:12,000 --> 00:11:19,020
So notice when I say Neco pet speak it says Neco says Wolf.

146
00:11:19,210 --> 00:11:23,300
And if I were to pasan Felix to pet speak it would say Felix says meow.

147
00:11:23,620 --> 00:11:28,840
So pet speak itself doesn't actually know whether you're going to pasan a dog or a cat.

148
00:11:28,840 --> 00:11:33,370
Remember these are two different classes and they are pretty similar in our case but they could technically

149
00:11:33,370 --> 00:11:34,540
be wildly different.

150
00:11:34,540 --> 00:11:36,120
Dog could have a hundred methods.

151
00:11:36,160 --> 00:11:40,750
Cat could just have these two methods but they share the same method name speak.

152
00:11:40,870 --> 00:11:46,000
Which means I can leverage that capability by happening to either iterate through a list of these different

153
00:11:46,000 --> 00:11:51,700
classes and happened to call the same method name or most likely I passed it into some function where

154
00:11:51,700 --> 00:11:54,520
later on it calls that method called speak.

155
00:11:54,520 --> 00:11:59,470
So in both cases we're able to pass in different objects and we obtain object specific results from

156
00:11:59,470 --> 00:12:01,880
the same mechanism that same method call.

157
00:12:02,200 --> 00:12:09,370
Now a more common practice is to use abstract classes and inheritance and what an abstract classes is

158
00:12:09,370 --> 00:12:11,600
one that never expects to be instantiated.

159
00:12:11,610 --> 00:12:14,730
You never actually expect to create an instance of this class.

160
00:12:14,740 --> 00:12:21,360
Instead it's just designed to basically only serve as a base class.

161
00:12:21,420 --> 00:12:30,350
So we're going to say the animal class again and the animal class is going to have in a method that

162
00:12:30,360 --> 00:12:35,380
takes self name is the constructor of the class.

163
00:12:35,380 --> 00:12:41,370
And we're going to assign animals the attribute name based off what's passed then and then this base

164
00:12:41,370 --> 00:12:48,610
class is going to have speak however later on in our code we never actually expect to say something

165
00:12:48,610 --> 00:12:51,780
like my animal is equal to an instance of animal.

166
00:12:51,820 --> 00:12:56,230
In fact this animal class is never actually expected to be instantiated.

167
00:12:56,230 --> 00:12:59,040
I don't expect to ever make an instance of the Animal class.

168
00:12:59,110 --> 00:13:02,700
Instead this is basically to be only used as a base class.

169
00:13:02,950 --> 00:13:08,800
So this speak method and this is really common of polymorphism as you get into more advanced object

170
00:13:08,830 --> 00:13:09,970
oriented programming.

171
00:13:10,150 --> 00:13:12,490
Well we're going to do is raise an error.

172
00:13:12,610 --> 00:13:16,730
Now we're going to learn about error errors and exceptions in the next section of the course.

173
00:13:16,870 --> 00:13:21,790
But essentially what's going to do is say hey we have some sort of error here.

174
00:13:21,860 --> 00:13:32,980
So we'll see not implemented air and we'll say subclass must implements this abstract method.

175
00:13:35,190 --> 00:13:36,100
So we run this.

176
00:13:36,150 --> 00:13:42,510
And what's going to happen is if we actually create an instance of animal something which we're not

177
00:13:42,510 --> 00:13:49,450
expected to do so well say Fred it is animal.

178
00:13:49,670 --> 00:13:53,130
If I say animal speak I get back.

179
00:13:53,170 --> 00:13:57,730
Hey not implemented air and this is actually telling me Hey you're supposed to use a subclass to implement

180
00:13:57,730 --> 00:13:59,470
this abstract method.

181
00:13:59,470 --> 00:14:05,500
So the reason this is an abstract method is because in the base class itself it doesn't actually do

182
00:14:05,500 --> 00:14:06,070
anything.

183
00:14:06,250 --> 00:14:11,560
It's expecting you to inherit the animal class and then overwrite the speak method.

184
00:14:11,740 --> 00:14:18,550
So I could delete this line because as we just said we never actually expect to use animal in that fashion.

185
00:14:18,680 --> 00:14:26,690
In Siddall we expect you to do is called Dog and then pass animal as you construct this dog class and

186
00:14:26,690 --> 00:14:30,950
now you know we no longer need to have this sort of method here.

187
00:14:31,090 --> 00:14:46,030
Instead what you can do is say DPF speak self and then return self self-taught name plus says what it

188
00:14:46,030 --> 00:14:48,120
will do the exact same thing for cat.

189
00:14:48,130 --> 00:14:53,790
So I'm going to copy and paste this and then I say instead of saying the law will sit there instead

190
00:14:53,860 --> 00:15:04,340
of saying Wolf will say yo run that I will say phyto is equal to a dog with the name phyto and I will

191
00:15:04,340 --> 00:15:17,030
say ISIS is an instance of cat with the name ISIS and I can print out phyto speak and I can print out

192
00:15:17,090 --> 00:15:17,800
as well.

193
00:15:18,750 --> 00:15:22,010
ISIS speak where ISIS is male.

194
00:15:22,600 --> 00:15:29,190
OK so some real life examples of polymorphism since these are really basic examples of using the same

195
00:15:29,250 --> 00:15:30,210
method name.

196
00:15:30,300 --> 00:15:34,910
And then also having these abstract methods is maybe opening different file types.

197
00:15:35,100 --> 00:15:41,550
So you may want to have the method name called Open which makes sense you're going to open a file but

198
00:15:41,550 --> 00:15:46,290
there's lots of actually different files there's word files pedia files and excel files.

199
00:15:46,400 --> 00:15:51,660
And if you are trying to make a class that could be a base class for opening files and then you have

200
00:15:51,660 --> 00:15:56,580
other classes that would inherit that maybe a class for opening c s ves in the class for opening excels

201
00:15:56,580 --> 00:15:58,620
another class for opening Word files.

202
00:15:58,740 --> 00:16:04,740
You'd want them to be able to share the same method name the open name.

203
00:16:04,740 --> 00:16:06,510
So that's a more realistic example.

204
00:16:06,780 --> 00:16:12,480
And that's an example point morphism where you can have some function maybe a general open file function

205
00:16:12,750 --> 00:16:18,420
can take in any of those classes that you have and just call the Open on them and it's up to the class

206
00:16:18,420 --> 00:16:22,000
itself to decide what actual type of files being opened.

207
00:16:23,040 --> 00:16:23,450
OK.

208
00:16:23,530 --> 00:16:24,790
So let's play morphism.

209
00:16:24,790 --> 00:16:30,370
Basic idea is you have these two separate classes that happen to share the same method name allowing

210
00:16:30,370 --> 00:16:36,010
you to then call those same method names without needing to worry about the specific class that's being

211
00:16:36,010 --> 00:16:37,220
passed in.

212
00:16:37,450 --> 00:16:41,890
We'll finish off our entire discussion of object oriented programming in the next lecture where we finally

213
00:16:41,890 --> 00:16:43,780
go over special methods.

214
00:16:43,780 --> 00:16:44,380
We'll see if they're.
