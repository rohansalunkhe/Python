1
00:00:05,350 --> 00:00:06,760
Welcome back everybody.

2
00:00:06,790 --> 00:00:11,800
We just had your homework assignment for object oriented programming where we had two geometric classes

3
00:00:12,190 --> 00:00:17,410
that were basically structured and you just had to fill in a couple of lines in the attributes and methods.

4
00:00:17,410 --> 00:00:19,240
Part of that class object.

5
00:00:19,240 --> 00:00:23,680
Now for your challenge it's going to be a little more freeform and we're going to have you create a

6
00:00:23,680 --> 00:00:28,750
bank account object with a couple of attributes and a couple of methods as well as some important special

7
00:00:28,750 --> 00:00:30,660
methods and self-checkouts.

8
00:00:30,680 --> 00:00:34,630
So let's go to the challenge notebook and explain what we expect you to do.

9
00:00:34,630 --> 00:00:38,770
All right so here I am at the repo and this should be the same if you've downloaded the notes but under

10
00:00:38,800 --> 00:00:44,080
the object oriented programming folder you should be able to find the O.P. challenge and in this object

11
00:00:44,110 --> 00:00:48,670
oriented programming challenge we're going to want you to create a bank account that has two attributes

12
00:00:49,030 --> 00:00:51,640
an owner attribute and a balance attribute.

13
00:00:51,940 --> 00:00:56,680
And then there's going to be two methods for that attribute deposit to deposit money to your bank account

14
00:00:56,770 --> 00:00:59,150
and withdraw to withdraw money from your bank account.

15
00:00:59,470 --> 00:01:04,060
And as an added requirement withdrawals may not exceed the available balance.

16
00:01:04,120 --> 00:01:07,950
So for this withdraw method you're going to have some sort of IF statement to check.

17
00:01:07,990 --> 00:01:09,710
Are you drawing too much.

18
00:01:10,240 --> 00:01:13,810
So here we basically only have class accounts and then pass.

19
00:01:14,050 --> 00:01:19,000
So this is a little more free form because it's going to be up to you to fill out the method the deposit

20
00:01:19,000 --> 00:01:23,240
method the withdrawal method and then the owner balance attributes.

21
00:01:23,260 --> 00:01:27,730
So once you instantiate the class with an owner which is going to be a string the name of the owner

22
00:01:28,090 --> 00:01:33,970
and some current balance to some initial balance for the account you should be able to print out the

23
00:01:33,970 --> 00:01:34,560
object.

24
00:01:34,570 --> 00:01:40,390
Recall that there are special methods specifically the SDR special method that allows you to print out

25
00:01:40,390 --> 00:01:43,030
whatever you want when you call print on your object.

26
00:01:43,030 --> 00:01:45,940
So in this case I have the account owner and the balance.

27
00:01:45,940 --> 00:01:51,130
You don't need to format your SDR method exactly like this but you should be able to report back some

28
00:01:51,130 --> 00:01:53,170
useful information.

29
00:01:53,300 --> 00:01:55,760
Then you should also have the owner attribute.

30
00:01:55,820 --> 00:02:01,970
In this case the owner's Jose and the balance attribute in this case the current balance is $100.

31
00:02:01,970 --> 00:02:07,990
Then you can make a series of deposits and withdrawals that deposit 50 we deposit and actually add in

32
00:02:08,000 --> 00:02:09,440
$50 to the balance.

33
00:02:09,440 --> 00:02:14,690
So that should affect the current balance and then we should be able to withdraw money and we see withdrawal

34
00:02:14,690 --> 00:02:15,160
except there.

35
00:02:15,200 --> 00:02:18,140
So we withdrew $75 from the balance.

36
00:02:18,140 --> 00:02:22,570
Now if you make overall that exceeds the available balance in this case we try to withdraw 500.

37
00:02:22,760 --> 00:02:26,540
You should be able to see funds unavailable as the output.

38
00:02:26,540 --> 00:02:26,940
Okay.

39
00:02:27,140 --> 00:02:30,800
So that's your challenge for this object oriented programming assignment.

40
00:02:30,980 --> 00:02:37,250
And I quickly want to touch upon this idea of being able to call methods that affect attributes we haven't

41
00:02:37,250 --> 00:02:38,390
truly seen that yet.

42
00:02:38,570 --> 00:02:45,250
So I want to show you a very simple example I'm going to create a class and this is going to be a simple

43
00:02:45,250 --> 00:02:46,480
class.

44
00:02:46,690 --> 00:02:47,800
So I'm just calling it simple

45
00:02:51,010 --> 00:02:52,270
and the method.

46
00:02:52,290 --> 00:02:55,140
All we're going to do is say some value here.

47
00:02:56,770 --> 00:03:05,520
And we'll say self value is equal to the value passed and then I'm going to have a method called add

48
00:03:05,520 --> 00:03:10,450
to value and this will be self.

49
00:03:10,660 --> 00:03:21,780
And then some amount and what this does is it takes an the current value of self or current self-taught

50
00:03:21,780 --> 00:03:30,590
value and then it says self-taught value plus the amount and this is something that we haven't really

51
00:03:30,590 --> 00:03:34,570
done before because notice here this method actually doesn't return in thing.

52
00:03:34,610 --> 00:03:38,810
All it does is it takes the amount that's passed then and then add it to the value.

53
00:03:38,810 --> 00:03:43,330
So that's kind of a hint of what you're going to need to do in your bank account class.

54
00:03:43,370 --> 00:03:49,740
So I'll say my object is equal to an instance of simple.

55
00:03:50,090 --> 00:03:53,330
And I will say that the current value is 300.

56
00:03:53,420 --> 00:03:57,120
So when I call my object value attribute I get back 300.

57
00:03:57,170 --> 00:04:01,850
You can kind of think of this as a kin to the balance of the bank account class.

58
00:04:02,090 --> 00:04:11,340
And if I say my object and I call add value let's say add another 500 to my value notice nothing is

59
00:04:11,340 --> 00:04:15,930
returned because all it's happening when you call this method is it takes in the amount you passed in

60
00:04:16,260 --> 00:04:17,610
here it's five hundred.

61
00:04:17,640 --> 00:04:23,550
It grabs your current value that is a self call as the amount to it and then reassign self-taught value

62
00:04:23,850 --> 00:04:25,680
to reflect this change.

63
00:04:25,680 --> 00:04:31,490
So now when I call in my object value it's 800.

64
00:04:31,530 --> 00:04:31,810
OK.

65
00:04:31,830 --> 00:04:36,570
So that's basically the little example I wanted to show you of how you would actually grab a current

66
00:04:36,840 --> 00:04:42,400
attribute and then affect the change on it and it's up to you if you want to print something like we

67
00:04:42,400 --> 00:04:51,840
just added curly braces to your value and then you could say something like the format amounts.

68
00:04:51,860 --> 00:04:57,590
So if you rerun this I would then get back an actual printed message here just so I know that it's actually

69
00:04:57,590 --> 00:04:58,420
working.

70
00:04:58,670 --> 00:04:59,150
OK.

71
00:04:59,360 --> 00:05:00,940
Best of luck on that challenge again.

72
00:05:00,980 --> 00:05:05,570
It's definitely more freeform because we want to push you to that next level where you're able to break

73
00:05:05,570 --> 00:05:11,360
down a concept into code if you have any questions feel free to post to the Q&amp;A forums and the next

74
00:05:11,360 --> 00:05:11,760
lecture.

75
00:05:11,780 --> 00:05:13,640
We'll go over an example solution.

76
00:05:13,640 --> 00:05:14,560
We'll see it there.
