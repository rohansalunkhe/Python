1
00:00:05,490 --> 00:00:06,700
Welcome back everybody.

2
00:00:06,930 --> 00:00:12,630
In this lecture we're going to be discussing object oriented programming object oriented programming

3
00:00:12,660 --> 00:00:17,690
or O.P. allows programmers to create their own objects have methods and attributes.

4
00:00:17,820 --> 00:00:19,110
And it's a really great way.

5
00:00:19,200 --> 00:00:22,230
Once you begin scaling your code to organize your code.

6
00:00:22,680 --> 00:00:28,440
So recall that after we define things such as a string list dictionary or other built in Python objects

7
00:00:28,770 --> 00:00:34,740
we were able to call methods offer them with the method names syntax such as when we were able to call

8
00:00:34,740 --> 00:00:40,980
the append the method off of the list object or the index method off of the tuple.

9
00:00:41,210 --> 00:00:46,940
These methods act as functions that use information about the object as well as the object itself to

10
00:00:46,940 --> 00:00:53,400
return results or add or change to a current object somehow such as when you called that sort on a list.

11
00:00:53,480 --> 00:00:56,570
It went ahead and edited the current list.

12
00:00:56,570 --> 00:01:01,240
For example this includes appending to a list or counting the occurrences of an element in a tuple.

13
00:01:01,280 --> 00:01:06,220
So those methods use information about the object and then either return a result or the edit and change

14
00:01:06,230 --> 00:01:13,860
occur object in some manner O.P. or object oriented programming allows users to create their own objects

15
00:01:13,860 --> 00:01:16,140
with their own methods and own attributes.

16
00:01:16,140 --> 00:01:21,000
And the general format is often confusing when first encountered particularly because of the use of

17
00:01:21,000 --> 00:01:24,790
a self keyword and its usefulness may not be completely clear.

18
00:01:24,810 --> 00:01:27,090
First for beginners but in general.

19
00:01:27,180 --> 00:01:32,280
Object oriented programming allows us to create code that is repeatable and organized and at a certain

20
00:01:32,280 --> 00:01:37,350
scale you're going to need to understand how to work with object oriented programming in order to make

21
00:01:37,350 --> 00:01:43,020
sure your programs themselves are flexible and when you're working with outside libraries you'll see

22
00:01:43,050 --> 00:01:45,510
how they make use of object oriented programming.

23
00:01:45,530 --> 00:01:51,920
And a lot of their code for much larger scripts of Python code functions by themselves really aren't

24
00:01:51,920 --> 00:01:57,650
going to be enough organization repeatability commonly repeated tasks and objects can be defined with

25
00:01:57,650 --> 00:02:02,750
O.P. to create code that is more usable especially across organization.

26
00:02:02,750 --> 00:02:07,160
So let's check out the syntax of object oriented programming and don't worry about completely understanding

27
00:02:07,160 --> 00:02:08,540
everything we show you right now.

28
00:02:08,660 --> 00:02:12,920
We're going to take it step by step and it you know put through a series of lectures.

29
00:02:12,920 --> 00:02:18,050
But right now I just want to give you a brief overview of some special syntax calls that we haven't

30
00:02:18,050 --> 00:02:19,530
really seen before.

31
00:02:19,700 --> 00:02:22,040
The first one being the class keyword.

32
00:02:22,040 --> 00:02:26,060
So the basic way that you define an object is using the class keyword.

33
00:02:26,060 --> 00:02:29,230
And that's why sometimes objects are also called classes in Python.

34
00:02:29,390 --> 00:02:34,280
Sometimes people like to interchange those two terms but basically we have the class keyword and then

35
00:02:34,280 --> 00:02:35,460
we have the name of the class.

36
00:02:35,510 --> 00:02:41,600
And notice here that the name of the class now is following what's called Camel casing where every individual

37
00:02:41,600 --> 00:02:46,520
word has that first letter capitalized which is why we were telling you before to have variable names

38
00:02:46,790 --> 00:02:48,450
and function names in lower case.

39
00:02:48,560 --> 00:02:54,360
Because classes those are the reserved uppercase words and uppercase naming schemes.

40
00:02:54,470 --> 00:02:58,490
So we have class and then we define the name of the class.

41
00:02:58,510 --> 00:03:03,360
The next thing we notice here is this thing that looks a lot like a function but it's actually called

42
00:03:03,360 --> 00:03:06,940
the method when it's inside of an object or inside of a class call.

43
00:03:07,210 --> 00:03:09,500
And here we have the left just like we would have a function.

44
00:03:09,640 --> 00:03:15,280
And we have the special method called in its method and its underscore underscore and I-T and this allows

45
00:03:15,280 --> 00:03:18,230
you to create an instance of the actual object.

46
00:03:18,310 --> 00:03:23,390
You'll lose here we have that a self keyword as well as some parameters to be passed.

47
00:03:23,470 --> 00:03:29,500
So Paramo one and perhaps two are going to be parameters that Python expects you to pass when you actually

48
00:03:29,500 --> 00:03:31,550
create an instance of this object.

49
00:03:31,630 --> 00:03:37,000
And then what it does is when you pass it a parameter for instance parameter to you go ahead and assign

50
00:03:37,000 --> 00:03:39,340
it to an attribute of the function.

51
00:03:39,340 --> 00:03:45,100
That way Python knows that when you're referring to self-taught Paramo to you're referring to the attribute

52
00:03:45,220 --> 00:03:50,720
Paramo to that's connected to this actual instance of the class instead of a global variable called

53
00:03:50,740 --> 00:03:55,900
pram 2 and we'll explain this in a lot more detail because this is a step that is really confusing at

54
00:03:55,900 --> 00:03:56,600
first.

55
00:03:57,620 --> 00:04:02,630
But again all we're doing here is we're going to pass in a parameter and then connect that parameter

56
00:04:02,630 --> 00:04:09,500
with the use of that self that arguments or self-taught parameter to link it to the actual object itself.

57
00:04:10,870 --> 00:04:15,270
After that then we can have various other method calls and these look a lot like functions with say

58
00:04:15,280 --> 00:04:17,870
the E-F the name or whatever you want your method to be.

59
00:04:18,010 --> 00:04:23,680
And then you pass in that self keyword again to let Python know that this isn't just some function it's

60
00:04:23,680 --> 00:04:28,120
a method that's connected to this class and that's where we use the self word to connect these methods

61
00:04:28,180 --> 00:04:32,950
to the class so that when you call something like self-taught program on it knows that you're referring

62
00:04:32,950 --> 00:04:37,090
to the attribute program one that's connected to the class itself.

63
00:04:38,290 --> 00:04:42,000
OK so we're going to explore object oriented programming in a lot more detail.

64
00:04:42,040 --> 00:04:45,540
Take it really slowly step by step in the next series of lectures.

65
00:04:45,580 --> 00:04:50,410
So if that was all kind of a real quick blur for you don't worry we're going to go step by step.

66
00:04:50,410 --> 00:04:52,120
Coming up next we'll see if they're.
