1
00:00:05,350 --> 00:00:10,180
Welcome back everybody in this lecture we're going to go over an example solution for the OPI challenge

2
00:00:10,330 --> 00:00:15,490
remember for this more open ended project your solution probably very than the one shown here.

3
00:00:15,490 --> 00:00:18,510
But as long as you're able to get the same results it should be OK.

4
00:00:18,760 --> 00:00:20,380
Let's go to Jupiter notebook now.

5
00:00:20,920 --> 00:00:24,070
OK so let's begin by creating our account class.

6
00:00:25,870 --> 00:00:31,550
And I'm going to start off with the method will say self as always.

7
00:00:31,620 --> 00:00:34,690
You know there's two attributes that are going to be defined here.

8
00:00:34,720 --> 00:00:40,930
The owner and the balance and if we wanted to we could set a default balance to be set equal to zero

9
00:00:40,930 --> 00:00:42,600
that we only need to provide the owner.

10
00:00:42,790 --> 00:00:50,080
That's optional and we'll say owner is equal to the owner that's passed in and we'll say the balance

11
00:00:50,080 --> 00:00:54,150
of this instance is equal to the balance that's passed then.

12
00:00:54,210 --> 00:01:03,340
Next we're going to do is set up the posit and withdraw methods we'll say the posit self and we're going

13
00:01:03,340 --> 00:01:04,820
to need some amount to the posit.

14
00:01:04,840 --> 00:01:13,200
So we'll say the posit amounts or the underscore empty for short and what we do here is we grab our

15
00:01:13,200 --> 00:01:22,520
current balance and we'll set it equal to our current balance plus that the positive amounts and then

16
00:01:22,520 --> 00:01:32,940
what we can do is instead of returning anything we can just print out added blank to the balance and

17
00:01:32,940 --> 00:01:37,130
we can use string literals if we want to hear or that format whatever you prefer.

18
00:01:37,320 --> 00:01:41,080
And then just passen the amounts.

19
00:01:41,180 --> 00:01:45,480
Now for the withdrawal it's going to be a little different because remember that withdrawal should check

20
00:01:45,780 --> 00:01:47,770
whether or not you have enough funds.

21
00:01:47,820 --> 00:01:54,520
So that means we're probably going to need if sustained here will say withdrawal amounts and we'll see

22
00:01:54,580 --> 00:02:03,100
if self-taught balance is greater than or equal to the withdrawal amounts.

23
00:02:05,050 --> 00:02:12,670
Then self-taught balance it's going to be equal to the current self-taught balance and just using TAP

24
00:02:12,670 --> 00:02:25,080
to autocomplete here minus the withdrawal amounts and then I'm going to print out withdrawal except

25
00:02:25,090 --> 00:02:32,940
that else that means that the balance is less than the half drawl amount I'm going to print.

26
00:02:33,660 --> 00:02:39,000
Sorry not enough funds.

27
00:02:39,250 --> 00:02:43,470
Then after all of this I want to be able to print out my account object.

28
00:02:43,810 --> 00:02:46,730
So I will have the special SDR done their method.

29
00:02:46,900 --> 00:02:48,870
They'll just taken self.

30
00:02:48,970 --> 00:02:51,420
And here we can decide what we're going to return.

31
00:02:51,430 --> 00:02:52,260
It's really up to you.

32
00:02:52,270 --> 00:03:06,470
But in our case I'm going to return the owner say owner is color braces balances curly braces and if

33
00:03:06,470 --> 00:03:12,370
we wanted a new line to be printed as we did in the example we could say forward slash new line and

34
00:03:12,370 --> 00:03:16,540
then you can either use that format or string literals if you wanted the string literal you just put

35
00:03:16,540 --> 00:03:19,180
an F there and then passing on the information.

36
00:03:19,180 --> 00:03:24,790
So in our case it's self the owner and for the balance it's self-taught balance.

37
00:03:24,870 --> 00:03:27,790
OK so remember you need to return that for the string function.

38
00:03:27,790 --> 00:03:29,000
You don't just print it out.

39
00:03:29,860 --> 00:03:33,420
And let's try to create an account here and see if we get any errors.

40
00:03:33,500 --> 00:03:38,510
We'll say Sam is the owner and Sam's going to have 500 as their balance.

41
00:03:38,560 --> 00:03:41,230
So let's check that he can grab the owner.

42
00:03:41,330 --> 00:03:42,420
Sam's looking good.

43
00:03:42,710 --> 00:03:45,020
Let's check that we can grab the balance.

44
00:03:45,050 --> 00:03:45,560
Five hundred.

45
00:03:45,560 --> 00:03:46,730
That's looking good.

46
00:03:46,730 --> 00:03:50,940
Now let's see what happens when he prints out a get owner Sam balance.

47
00:03:50,950 --> 00:03:51,900
Five hundred.

48
00:03:51,920 --> 00:03:52,950
Let's make a deposit.

49
00:03:54,040 --> 00:04:00,160
So I'm going to make $100 deposit at 100 for the balance let's print out again make sure that the balance

50
00:04:00,160 --> 00:04:01,660
has been affected.

51
00:04:01,660 --> 00:04:01,920
Great.

52
00:04:01,930 --> 00:04:03,670
Now we see 600 there.

53
00:04:03,670 --> 00:04:11,550
Let's make a withdrawal a withdrawal of let's do let's do six hundred itself to empty out the account

54
00:04:13,540 --> 00:04:16,780
so that worked out will print a current balance is zero.

55
00:04:16,780 --> 00:04:22,100
Now if I try to do one more trawl of anything I should get an error because I have a balance of 0 early

56
00:04:22,130 --> 00:04:23,680
at I should get a print statement.

57
00:04:24,130 --> 00:04:25,890
Ok sorry not enough funds.

58
00:04:25,890 --> 00:04:29,130
And if I print a we can confirm that I have zero.

59
00:04:29,230 --> 00:04:31,060
That's all you had to do for this project.

60
00:04:31,060 --> 00:04:32,770
Let's do a quick little overview.

61
00:04:32,800 --> 00:04:37,960
We set up our attributes to that the instance of owner and balance were equal to at every pass then

62
00:04:37,960 --> 00:04:44,410
here we have a simple deposit method and it's quick note for these actions of equal to the same thing

63
00:04:44,440 --> 00:04:49,990
plus what you could do as an alternative is instead of writing equals self-taught balance plus as you

64
00:04:49,990 --> 00:04:51,450
say plus equals.

65
00:04:51,670 --> 00:04:52,960
And it's the same thing over here.

66
00:04:52,990 --> 00:04:56,160
You could have just said minus equals.

67
00:04:56,160 --> 00:04:59,310
So this is kind of sleeker but it's up to you it's more readable.

68
00:04:59,340 --> 00:05:01,630
So you could then that for the deposit and withdraw methods.

69
00:05:01,650 --> 00:05:05,580
Remember the withdrawal method you have an if statement in order to check that we have enough money

70
00:05:05,580 --> 00:05:08,670
in the balance and then for the string method we just returned back.

71
00:05:08,670 --> 00:05:13,440
Really whatever you want as long as it conveys the information of the owner and the balance.

72
00:05:13,440 --> 00:05:17,460
Okay you can check out the solutions for more information and if you have any questions posed to the

73
00:05:17,460 --> 00:05:20,110
Q&amp;A forums and we're happy to help you there.

74
00:05:20,130 --> 00:05:21,540
We'll see you at the next lecture.
