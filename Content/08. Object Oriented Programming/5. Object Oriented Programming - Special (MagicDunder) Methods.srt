1
00:00:05,440 --> 00:00:06,490
Welcome back everyone.

2
00:00:06,520 --> 00:00:11,290
To finish off our series of lectures on actually working Object-Oriented Programming in Python we're

3
00:00:11,290 --> 00:00:17,050
going to talk about special methods special methods allow us to use some built in operations in Python

4
00:00:17,350 --> 00:00:22,840
such as the length function or the print function with our own user created objects to get an idea of

5
00:00:22,840 --> 00:00:23,620
how this all works.

6
00:00:23,620 --> 00:00:25,130
Let's jump to Jupiter notebook.

7
00:00:25,990 --> 00:00:28,230
Let's start off with a simple example.

8
00:00:28,330 --> 00:00:29,840
I'm going to create a list.

9
00:00:29,950 --> 00:00:31,270
One two three.

10
00:00:31,660 --> 00:00:34,380
And then let's say I wanted to check the length of my list.

11
00:00:34,660 --> 00:00:37,960
Well I could just call Eliane on my list.

12
00:00:37,960 --> 00:00:41,680
What if I wanted to check the length of one of my own objects.

13
00:00:41,750 --> 00:00:46,660
So what it's going to happen is if I create my own sample class and it's a very simple class we just

14
00:00:46,660 --> 00:00:48,390
say pass here.

15
00:00:48,680 --> 00:00:50,570
I create an instance of my sample

16
00:00:53,370 --> 00:00:59,880
and then try to check the length of my sample what's going to happen is I get a type error object of

17
00:00:59,880 --> 00:01:08,070
type sample has no link and maybe I want to try to print out my sample so I say print sample and I just

18
00:01:08,070 --> 00:01:12,150
get back the report of where my sample object is located at my memory.

19
00:01:12,150 --> 00:01:18,910
Unlike my list where if I tried to print it out I would actually get back my list in a string form.

20
00:01:18,990 --> 00:01:25,410
So the question arises how am I able to actually use built in Python functions such as length or print

21
00:01:25,680 --> 00:01:27,860
with my own user defined objects.

22
00:01:28,020 --> 00:01:30,570
And this is where those special methods come into play.

23
00:01:30,600 --> 00:01:35,580
So special methods some people also call them magic methods or done their methods because they use double

24
00:01:35,610 --> 00:01:38,680
underscores work in the following manner.

25
00:01:38,940 --> 00:01:41,340
And there's more special methods and what we're going to show here.

26
00:01:41,550 --> 00:01:44,200
But for now these are the main ones you're going to be using.

27
00:01:44,340 --> 00:01:50,180
I'm going to create a book class and will say in it.

28
00:01:50,620 --> 00:01:54,610
And it was the first special method we learned about and it's special because it's basically called

29
00:01:54,640 --> 00:01:56,530
automatically when you create the object.

30
00:01:56,860 --> 00:02:06,050
So our books are going to have a title and author and pages and we're going to do here say self-taught

31
00:02:06,050 --> 00:02:09,540
title of the book is equal to the title you pass in when you create it.

32
00:02:09,770 --> 00:02:15,470
The author attribute is equal to the author you pass and when you create it and then the pages attribute

33
00:02:15,560 --> 00:02:19,770
is equal to the pages that you pass in when you create your instance of your book.

34
00:02:20,090 --> 00:02:28,160
And then imagine I wanted to print out my book right now if I say B is equal to book where the title

35
00:02:28,160 --> 00:02:32,260
is python rocks let's say the authors Jose.

36
00:02:32,480 --> 00:02:34,790
And it has 200 pages.

37
00:02:34,790 --> 00:02:38,840
If I try to print my book it just says hey you have this book object in memory.

38
00:02:38,960 --> 00:02:43,940
What happens is when you call the print function what the print function does is it asks What is the

39
00:02:43,940 --> 00:02:46,750
string representation of B.

40
00:02:46,760 --> 00:02:53,480
So if I try to cast or transform B into a string what happens is I just get a string version of this

41
00:02:53,480 --> 00:02:55,220
report when I print it.

42
00:02:55,250 --> 00:03:02,360
Instead what we can do is actually use the special method related to that string call which is underscore

43
00:03:02,360 --> 00:03:04,860
underscore as TR underscore underscore.

44
00:03:05,180 --> 00:03:06,100
And what this does.

45
00:03:06,140 --> 00:03:11,720
If there's ever any function that asks for the string representation of your book class then it's going

46
00:03:11,720 --> 00:03:15,400
to return whatever this method returns which is why it's a special method.

47
00:03:16,700 --> 00:03:18,850
And make sure you use return here and not print.

48
00:03:19,080 --> 00:03:21,920
And I'm going to return whatever I want to be printed out.

49
00:03:22,460 --> 00:03:29,120
So when I print a book it might make sense to kind of print out a lot of things so we'll use the string

50
00:03:29,120 --> 00:03:30,350
literal format.

51
00:03:30,500 --> 00:03:41,270
I will say self-taught title by self-taught author and shift enter here.

52
00:03:41,380 --> 00:03:46,540
So what happens is when I'm going to call the print function the print function asks the book object.

53
00:03:46,550 --> 00:03:50,590
Hey do you have a string representation of yourself to which the book object then says.

54
00:03:50,620 --> 00:03:57,280
Yes I do because I have this special as a method and I would turn this string or the string using string

55
00:03:57,370 --> 00:03:58,600
literal formatting.

56
00:03:58,600 --> 00:04:04,260
I can actually pasand self-taught titles so it grabs this title and sulpha author grabs this author.

57
00:04:04,420 --> 00:04:06,080
So let's rerun these cells.

58
00:04:06,110 --> 00:04:08,050
Now if I say print B I get back.

59
00:04:08,050 --> 00:04:09,530
Python rocks by Jose.

60
00:04:09,760 --> 00:04:13,930
So if I asked for the string representation it's the same thing because the print function.

61
00:04:13,930 --> 00:04:18,190
All it does is it prints out what string representation returns back.

62
00:04:18,190 --> 00:04:22,900
So that's a special method for strings and printing in general.

63
00:04:23,050 --> 00:04:27,730
Next we can do the same thing for length because right now if I try to check the length of my book B

64
00:04:28,480 --> 00:04:31,500
It says Hey an object of type book has no length.

65
00:04:31,570 --> 00:04:38,510
So as you may have guessed the special method that goes along with this is Elyon and we're going to

66
00:04:38,510 --> 00:04:42,430
say self here and then we can return back whatever we want to be printed out.

67
00:04:42,440 --> 00:04:46,430
When you ask for the length of the book and here it probably makes the most sense to just return back

68
00:04:46,490 --> 00:04:48,090
the number of pages the book has.

69
00:04:48,140 --> 00:04:51,290
So will return back sulf pages.

70
00:04:51,480 --> 00:04:57,760
We run these things and when I print these books out and ask for their length I get back 200 finally.

71
00:04:57,780 --> 00:05:01,350
Well we could do is let's say we wanted to delete a book.

72
00:05:01,520 --> 00:05:07,610
Well you can do it you can call the key word the l space and then pass in the variable name and that

73
00:05:07,610 --> 00:05:11,660
will actually delete that book from the memory on your computer.

74
00:05:11,660 --> 00:05:15,580
So if you asked for B again it's going to say name B is not defined.

75
00:05:15,590 --> 00:05:19,310
So this is a way to delete variables from your computer's memory.

76
00:05:19,310 --> 00:05:24,740
Sometimes you may want to print out report upon deleting the variable or you may want other things to

77
00:05:24,740 --> 00:05:31,070
occur when you delete the variable in order to basically specify extra things that may occur when you

78
00:05:31,070 --> 00:05:32,310
call the key.

79
00:05:32,420 --> 00:05:39,990
You can use underscore underscore the underscore underscore self and then we can add certain things

80
00:05:39,990 --> 00:05:43,600
to happen when you actually delete an instance of the book.

81
00:05:43,740 --> 00:05:55,960
So I will say Prince a book object has been deleted and then I will say Schiff's run here or shift enter.

82
00:05:56,120 --> 00:06:03,340
Let's define our book again and let's print out the book I'll do alt enter here to get some cells below.

83
00:06:03,500 --> 00:06:04,630
So I printed out my book.

84
00:06:04,640 --> 00:06:09,590
Let's check to make sure the book still exists print to be and then to delete the variable.

85
00:06:09,590 --> 00:06:15,440
I can say DTL be and now when I run this I get out the extra statement.

86
00:06:15,440 --> 00:06:17,310
A book object has been deleted.

87
00:06:17,450 --> 00:06:20,560
So when I called B again it's now deleted.

88
00:06:20,570 --> 00:06:25,370
B is no longer fine and I was able to add a print statement to maybe give the user more information

89
00:06:25,640 --> 00:06:28,410
when to use the special DL keyword.

90
00:06:28,430 --> 00:06:32,630
All right those are the main special methods that you're going to be using as you're just starting off

91
00:06:32,630 --> 00:06:33,610
with Python.

92
00:06:33,620 --> 00:06:38,570
You can check the documentation for many more Dunder or special or magic methods but really these are

93
00:06:38,570 --> 00:06:42,710
the main ones you're going to be using especially as TR and LDN.

94
00:06:42,740 --> 00:06:46,350
So again as TR It returns back the actual string representations.

95
00:06:46,350 --> 00:06:48,410
You can print out easily defined objects.

96
00:06:48,440 --> 00:06:54,230
Boolean returns back the length so you can return back the length of the find objects and that ends

97
00:06:54,230 --> 00:06:56,400
our discussion on object oriented programming.

98
00:06:56,600 --> 00:07:00,390
Let's not test her skills with a homework assignment and a small challenge project.
