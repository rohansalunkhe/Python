1
00:00:05,580 --> 00:00:08,850
Welcome, everyone, to this section on using e mails with Python.

2
00:00:10,190 --> 00:00:14,540
So in this section, we're going to be exploring how to send e-mails of Python and how to check our

3
00:00:14,540 --> 00:00:16,250
inbox for received messages.

4
00:00:16,670 --> 00:00:22,130
Please keep in mind that this process sexually highly reliant on your administrative privileges, on

5
00:00:22,130 --> 00:00:24,890
both your local computer, your Internet and your e-mail.

6
00:00:26,180 --> 00:00:30,980
So it's highly likely that if you're running this on a corporate network or a work computer or if you're

7
00:00:30,980 --> 00:00:35,940
trying to use a work email that you don't have full admin permissions for, these methods are gonna

8
00:00:35,960 --> 00:00:39,020
be blocked by your company for security reasons.

9
00:00:39,260 --> 00:00:43,970
And if you encounter those sort of issues because of either a corporate network or computer or work

10
00:00:44,030 --> 00:00:47,810
email, please contact your I.T. department, because that's not really an issue.

11
00:00:47,810 --> 00:00:52,580
You can really work around easily with code because essentially trying then hack into your own e-mail.

12
00:00:52,910 --> 00:00:54,770
So it's that contact your I.T. department.

13
00:00:56,170 --> 00:00:59,530
Now, lastly, there's actually no full exercise for these lecture topics.

14
00:00:59,830 --> 00:01:04,540
We'll simply cover how to send out e-mail with Python automatically and how to check your inbox for

15
00:01:04,540 --> 00:01:05,920
messages from Python.

16
00:01:06,430 --> 00:01:11,170
And because of this, there's no real way we can actually create a true self-assessment for things that

17
00:01:11,170 --> 00:01:12,910
involve your personal email address.

18
00:01:13,300 --> 00:01:17,710
But we do have a notebook that's called an exercise notebook, which is really just a list of ideas

19
00:01:17,710 --> 00:01:18,580
for you to explore.

20
00:01:18,880 --> 00:01:20,260
And we encourage you to get creative.

21
00:01:20,350 --> 00:01:25,600
Given what you've already learned so far with Python and combine that with the ability to send and receive

22
00:01:25,630 --> 00:01:26,940
e-mail programmatically.

23
00:01:27,800 --> 00:01:29,050
OK, let's get started.

24
00:01:29,290 --> 00:01:30,220
I'll see you at the next lecture.
