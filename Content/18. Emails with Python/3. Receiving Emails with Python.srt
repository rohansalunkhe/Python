1
00:00:05,280 --> 00:00:06,300
Welcome back, everyone.

2
00:00:06,450 --> 00:00:11,580
In this lecture, we're going to discuss receiving e-mails with Python, and when we talk about receiving

3
00:00:11,670 --> 00:00:15,630
really what we're talking about is being able to explore your inbox with Python.

4
00:00:17,120 --> 00:00:23,440
To be received e-mails of Python, we can use the built in I map lib and email libraries in Python.

5
00:00:23,500 --> 00:00:25,470
So it's actually two separate libraries names.

6
00:00:25,750 --> 00:00:27,350
That's a map lab is one library.

7
00:00:27,430 --> 00:00:30,730
And then there's actually an e-mail library just called email in Python.

8
00:00:31,300 --> 00:00:37,120
And what the eye map lib library alost to do is use its special syntax for searching your inbox.

9
00:00:37,240 --> 00:00:42,550
And this is essentially the same kind of things you can search for by going to just a normal computer

10
00:00:42,550 --> 00:00:43,720
and search your inbox.

11
00:00:44,380 --> 00:00:49,000
So what I mean by that is if you take a look at this table and this table is inside the lecture notebook,

12
00:00:49,360 --> 00:00:54,370
you essentially have special syntax keywords and then a corresponding definition for them.

13
00:00:54,520 --> 00:00:59,290
So what this allows to do is search for specific things from your e-mail folder.

14
00:00:59,620 --> 00:01:04,120
So, for example, if you were just to use a keyword, all that would attempt to return all the messages

15
00:01:04,120 --> 00:01:06,490
from your e-mail folder or your e-mail inbox.

16
00:01:06,800 --> 00:01:08,470
And there are usually size limits of this.

17
00:01:08,680 --> 00:01:13,240
So if you want to change them, you can begin actually editing the attributes to the map library.

18
00:01:13,720 --> 00:01:16,640
And usually it's one hundred is going to be the limit.

19
00:01:17,430 --> 00:01:22,180
However, you can't change that to really any number you want, but you should be careful with all because

20
00:01:22,180 --> 00:01:23,500
that's just grabbing everything.

21
00:01:24,100 --> 00:01:27,910
Usually, however, you're going to be filtering things by things like dates.

22
00:01:28,000 --> 00:01:34,280
So you can say get all the messages before they are on a date or sense a date or do things like retrieval

23
00:01:34,300 --> 00:01:39,520
messages from a particular user or email address or all the messages that went to an email address using

24
00:01:39,520 --> 00:01:40,870
the two keyword.

25
00:01:41,080 --> 00:01:45,190
And then you can also check things based off who is CCD, who is blind carbon copy.

26
00:01:45,730 --> 00:01:51,250
Or you can do things like check for the subject, the body text or whether you're not, you've answered

27
00:01:51,250 --> 00:01:51,400
it.

28
00:01:51,430 --> 00:01:52,090
Have seen it.

29
00:01:52,240 --> 00:01:53,170
Deleted it or not.

30
00:01:53,170 --> 00:01:53,650
Deleted it.

31
00:01:53,770 --> 00:01:54,400
And so on.

32
00:01:54,460 --> 00:01:55,780
So lots of keywords here.

33
00:01:56,080 --> 00:01:58,660
Basically, all the things you would expect to be able to filter by.

34
00:02:00,400 --> 00:02:04,690
Now, before beginning this discussion, what I would like you to do is go ahead and send yourself a

35
00:02:04,690 --> 00:02:10,720
test e-mail with a very unique subject line that doesn't actually correspond to any other e-mail in

36
00:02:10,720 --> 00:02:11,500
your inbox.

37
00:02:11,530 --> 00:02:16,270
So just basically choose a random color, maybe a random animal or something.

38
00:02:16,330 --> 00:02:20,830
Something very unique that, you know, would only be returned by this particular e-mail, because we're

39
00:02:20,830 --> 00:02:25,240
going to do is use this as a test e-mail in order to retrieve it with Python.

40
00:02:25,300 --> 00:02:26,500
So we want to make sure this works.

41
00:02:26,800 --> 00:02:28,670
So don't send yourself just a generic e-mail.

42
00:02:28,780 --> 00:02:30,280
Otherwise, we'll probably grab too many.

43
00:02:30,700 --> 00:02:33,760
So let's begin exploring checking received e-mails of Python.

44
00:02:34,030 --> 00:02:34,960
Let's hop to a notebook.

45
00:02:36,710 --> 00:02:39,530
The first thing we're going to do is import the I IMAP.

46
00:02:40,660 --> 00:02:44,560
Lib Library that's built into Python and we'll create an instance of it.

47
00:02:45,310 --> 00:02:50,110
And you can call this variable whatever you want, but a lot of times people call it a capital M and

48
00:02:50,110 --> 00:02:55,000
we'll say I map lib and then call a map for S.

49
00:02:55,090 --> 00:03:03,310
S L, and then we'll pass in the I'm map server for a G mail, which is a map that G.M. dot com.

50
00:03:05,210 --> 00:03:08,410
And then we need to grab our password and e-mail.

51
00:03:08,990 --> 00:03:12,790
So to do that, you can either use input or get pass.

52
00:03:13,760 --> 00:03:15,770
We'll say that our.

53
00:03:16,960 --> 00:03:18,550
E-mail is equal to.

54
00:03:19,850 --> 00:03:21,680
Get pass, get pass.

55
00:03:23,280 --> 00:03:26,840
Email, and then we'll also grab our actual password for the e-mail.

56
00:03:28,190 --> 00:03:29,990
Get pass, get pass.

57
00:03:31,660 --> 00:03:34,590
And remember, for G.M. users, this is your app password.

58
00:03:35,310 --> 00:03:36,390
So let's run this.

59
00:03:36,570 --> 00:03:41,100
We'll go ahead, enter your e-mail for whatever you happen to be using.

60
00:03:44,280 --> 00:03:48,000
And then you can also put in your password in this case, I'm putting in the app password.

61
00:03:48,990 --> 00:03:50,690
Let's now connect, we can say.

62
00:03:50,960 --> 00:03:51,930
That log in.

63
00:03:52,890 --> 00:03:55,080
And Passant, email and password.

64
00:03:56,190 --> 00:03:56,730
Run that.

65
00:03:56,880 --> 00:04:02,060
And you should be able to log in and you should see some sort of authenticated success message there.

66
00:04:02,640 --> 00:04:07,020
Once you've done that, you'll be able to type in and that list as a method.

67
00:04:07,650 --> 00:04:11,850
Run that and you can see everything that you can check in your particular email.

68
00:04:12,270 --> 00:04:14,370
Typically, you'll be checking your inbox beacon all to see.

69
00:04:14,370 --> 00:04:20,370
You can check personal receipts sent trash things, label to travel or work drafts, important sent

70
00:04:20,370 --> 00:04:22,290
mail, spam, start trash.

71
00:04:22,320 --> 00:04:27,360
So there's lots of different options, different flags that G.M. or other e-mail providers have for

72
00:04:27,360 --> 00:04:27,600
you.

73
00:04:28,080 --> 00:04:30,450
We're going to select the most common one, which is your inbox.

74
00:04:31,620 --> 00:04:36,390
Meaning we'll say m select and we'll passant inbox.

75
00:04:38,180 --> 00:04:42,780
Run that and then it should say, OK, and some sort of connection, no to your inbox.

76
00:04:42,800 --> 00:04:44,890
Your number will likely be different than mine here.

77
00:04:45,800 --> 00:04:49,580
And then now that we've connected to our mail, we're going to be able to search for it using the specialized

78
00:04:49,580 --> 00:04:50,840
syntax of a map.

79
00:04:51,200 --> 00:04:55,850
Now, the next step after we've logged in is to actually search our inbox.

80
00:04:56,180 --> 00:05:00,950
And this is the sort of syntax you can use to specify what messages you're looking for.

81
00:05:01,460 --> 00:05:05,750
In our case, we will attempt to search for that e-mail that we just sent earlier.

82
00:05:05,780 --> 00:05:08,420
That was a subject line, new test Python.

83
00:05:09,920 --> 00:05:17,180
So we say type and data to basically doing tuple unpacking, because the actual search function that

84
00:05:17,180 --> 00:05:22,520
we call is going to return a tuple and you passan none as the first argument.

85
00:05:23,060 --> 00:05:26,990
And then the second argument is the actual string that uses this sort of code.

86
00:05:27,410 --> 00:05:35,310
So you can say, for instance, before and then format a date such as zero one November 2000.

87
00:05:35,540 --> 00:05:40,190
And this will essentially search our email and return back everything sent before zero one November

88
00:05:40,190 --> 00:05:40,670
2000.

89
00:05:41,210 --> 00:05:43,970
Or you could do on a certain date or since a certain date.

90
00:05:44,300 --> 00:05:45,930
You can do from a certain string.

91
00:05:46,220 --> 00:05:49,490
So, for example, you could say from.

92
00:05:50,510 --> 00:05:52,610
User at exampled dot com.

93
00:05:53,390 --> 00:05:56,890
And maybe you didn't know who the actual user was at this example.

94
00:05:56,920 --> 00:05:57,470
Dot com.

95
00:05:57,710 --> 00:05:58,120
Do the thing.

96
00:05:58,430 --> 00:06:00,940
You could say it's just from example.

97
00:06:01,160 --> 00:06:05,690
And it will search for all the email addresses from and see if the string example is in there.

98
00:06:06,350 --> 00:06:09,410
The other thing you can do that we're going to be doing is search by subject.

99
00:06:10,670 --> 00:06:12,720
So notice how these keywords are capitalized.

100
00:06:12,750 --> 00:06:13,710
We'll say subject.

101
00:06:14,160 --> 00:06:17,700
And then inside of this string, we'll have another substring.

102
00:06:17,760 --> 00:06:19,050
And this will be the actual string.

103
00:06:19,080 --> 00:06:20,190
We're checking for.

104
00:06:20,510 --> 00:06:24,950
So the subject line that we passed in last time was new test python.

105
00:06:25,500 --> 00:06:26,850
So that's what are going to be searching here.

106
00:06:26,880 --> 00:06:28,770
Notice how it's a string inside of a string.

107
00:06:29,400 --> 00:06:30,270
And let's run this.

108
00:06:31,890 --> 00:06:35,940
And see what we get, if you select type, it should say, OK.

109
00:06:36,240 --> 00:06:39,210
And if you have data, it should have some sort of number.

110
00:06:39,330 --> 00:06:43,560
If you don't have any sort of number returned here, it means it didn't find any messages.

111
00:06:43,980 --> 00:06:49,380
If you have multiple numbers here and a list, that means it found more than one message that relates

112
00:06:49,380 --> 00:06:51,240
to whatever search term you are looking for.

113
00:06:52,470 --> 00:06:56,640
So this unique I.D. is the I.D. that references the actual email.

114
00:06:56,760 --> 00:06:59,080
We don't have the e-mail itself yet to do that.

115
00:06:59,100 --> 00:07:01,170
We now need to fetch the actual data.

116
00:07:01,680 --> 00:07:08,640
So if we take a look at the first item in this list, it's this unique I.D. So we will say I.D. is equal

117
00:07:08,640 --> 00:07:10,130
to also e-mail I.D..

118
00:07:11,590 --> 00:07:15,390
E-mail I.D. is equal to that item in the list, and then we'll say results.

119
00:07:16,180 --> 00:07:19,450
E-mail underscore data is equal to M..

120
00:07:19,760 --> 00:07:23,330
And then we end up fetching that particular e-mail I.D..

121
00:07:25,060 --> 00:07:30,820
And then for a second argument, we need a pass in a protocol and the perfect call for that as a string

122
00:07:30,850 --> 00:07:34,730
with parentheses are F, C, eight to two.

123
00:07:35,790 --> 00:07:40,230
Now that we've actually fetch the e-mail data, we can take a look at this e-mail data.

124
00:07:40,920 --> 00:07:43,320
And it's basically going to be a bit of a sloppy mess.

125
00:07:43,580 --> 00:07:46,580
There's lots of information here, the emails.

126
00:07:46,680 --> 00:07:51,000
But if you take a look down here, we can see that we have the subject and we actually have the message

127
00:07:51,000 --> 00:07:51,420
we sent.

128
00:07:51,720 --> 00:07:51,960
Hello.

129
00:07:51,990 --> 00:07:55,440
This is a test and and is all within a list within some tuples.

130
00:07:55,740 --> 00:07:58,230
So we just want to grab the actual message itself.

131
00:07:58,710 --> 00:08:01,720
So we're going to do is index for it.

132
00:08:03,350 --> 00:08:09,110
We'll type out raw email is equal to email data and its indexed position.

133
00:08:09,140 --> 00:08:10,310
You may need to play around for this.

134
00:08:10,340 --> 00:08:11,900
But in our case, it's at zero one.

135
00:08:12,860 --> 00:08:14,420
So that's the raw email itself.

136
00:08:15,110 --> 00:08:17,150
And we want the raw email stream.

137
00:08:18,860 --> 00:08:24,980
We'll type out raw e-mail string is equal to this raw email, and we're going to need to decode it.

138
00:08:25,130 --> 00:08:29,420
And the reason for that is because there may be symbols such as the at symbol, which is confusing the

139
00:08:29,420 --> 00:08:30,350
python sometimes.

140
00:08:30,710 --> 00:08:34,360
So we want to clarify that we're going to decode it if UTF Dash eight encoding.

141
00:08:35,360 --> 00:08:39,530
And then once we've done that, we can use the built in e-mail library to help pass that string.

142
00:08:40,580 --> 00:08:41,240
We will import.

143
00:08:41,310 --> 00:08:41,750
Email.

144
00:08:43,040 --> 00:08:50,030
And this email library helps grab the actual message from a stream, and the syntax is still a bit strange,

145
00:08:50,060 --> 00:08:51,920
but it looks like this.

146
00:08:51,950 --> 00:08:53,120
You say e-mail message.

147
00:08:53,800 --> 00:09:00,020
And then from the email that we just imported, we'll type out message from in this case or driving

148
00:09:00,020 --> 00:09:01,550
the message from a string.

149
00:09:02,590 --> 00:09:05,230
And then we pass that raw e-mail string that we just created.

150
00:09:06,880 --> 00:09:08,920
And then once you've done that, we need to do the following.

151
00:09:10,290 --> 00:09:17,820
The actual e-mail message object here, if we take a look at it, it's this iterator and what we need

152
00:09:17,820 --> 00:09:19,080
to do is say for.

153
00:09:20,170 --> 00:09:26,950
Every part in the email message, the walk, so you can kind of walk through this email message, object

154
00:09:28,300 --> 00:09:33,130
if part dot get content type.

155
00:09:35,770 --> 00:09:36,760
Is equal to.

156
00:09:37,690 --> 00:09:39,760
Text forward slash plane.

157
00:09:42,570 --> 00:09:48,120
Then body is equal to parts dot get.

158
00:09:49,390 --> 00:09:50,110
Payload.

159
00:09:51,590 --> 00:09:53,840
And say Decode is equal to true.

160
00:09:54,970 --> 00:09:57,160
And then prince the body.

161
00:09:57,790 --> 00:10:03,100
Now, this is quite complicated, but the reason this is complicated is because we're basically going

162
00:10:03,100 --> 00:10:09,520
from that rostering e-mail message and trying to get rid of all the other information that came in through

163
00:10:09,520 --> 00:10:10,330
the e-mail data.

164
00:10:10,780 --> 00:10:13,910
There's lots of different ways you could decide to look through the subject.

165
00:10:14,230 --> 00:10:18,740
Once you have the sexual e-mail data yourself, it's optional to use this e-mail library.

166
00:10:18,760 --> 00:10:23,470
Sometimes people feel that it's more of a hassle to use this e-mail library and rather make their own

167
00:10:23,470 --> 00:10:25,660
custom scrip to grab the e-mail data.

168
00:10:25,750 --> 00:10:30,190
And if you're dealing with e-mails that look the same every time, maybe it's from some API service.

169
00:10:30,220 --> 00:10:33,670
It might make sense to not use a sexual e-mail library that's built in.

170
00:10:34,060 --> 00:10:35,950
But if you do want to use this e-mail library that's built in.

171
00:10:36,250 --> 00:10:41,860
Once you have the raw e-mail string from your actual e-mail data, after you've decoded it, he type

172
00:10:41,860 --> 00:10:43,300
out e-mail message from string.

173
00:10:43,720 --> 00:10:49,540
And then you say for part an e-mail message walk if the part get content type, which basically asks,

174
00:10:49,600 --> 00:10:51,790
hey, what is the content type of the part.

175
00:10:52,030 --> 00:10:56,080
And if it's equal to text four, slash plain meaning, it's plain text.

176
00:10:56,470 --> 00:11:01,180
You grab the payload of that part, decoding it, and then that's the actual variable that we want to

177
00:11:01,180 --> 00:11:01,540
see.

178
00:11:01,630 --> 00:11:06,490
So if we print this out, we should be able to see the actual body of the message, which is, hello,

179
00:11:06,520 --> 00:11:11,530
this is a test and this B indicates that it's a bite string just because the way we read in that e-mail

180
00:11:11,710 --> 00:11:12,100
data.

181
00:11:12,910 --> 00:11:19,510
Keep in mind this sort of content type check, text forward slash plain, that's if you only expect

182
00:11:19,870 --> 00:11:21,070
just plain text.

183
00:11:21,400 --> 00:11:25,640
Well, you could also do is maybe expect text slash H.T. Amelle.

184
00:11:26,050 --> 00:11:26,890
And that's really common.

185
00:11:26,920 --> 00:11:29,980
If someone has provided a link inside of the email.

186
00:11:30,430 --> 00:11:35,560
So if you expect another link to be in the email that was sent to you, you may want to check for text

187
00:11:35,560 --> 00:11:38,920
forward slash h t mail instead of text forward slash plain.

188
00:11:40,050 --> 00:11:40,310
OK.

189
00:11:40,680 --> 00:11:44,100
That's it for the basics of checking your received email with Python.

190
00:11:44,430 --> 00:11:46,020
Let's quickly go over all the steps.

191
00:11:46,950 --> 00:11:51,810
All you have to do is import a map lib, make the connection to your map server.

192
00:11:51,840 --> 00:11:56,880
So it's a map that G.M. dot com or I'm up that Yahoo dot com, depending on what your actual domain

193
00:11:56,880 --> 00:11:57,630
provider is.

194
00:11:58,020 --> 00:12:01,440
Then grab your email and password, log in.

195
00:12:01,860 --> 00:12:06,330
You can see a list of options, but typically you'll be searching your inbox and then using the special

196
00:12:06,330 --> 00:12:07,320
syntax code.

197
00:12:07,710 --> 00:12:12,690
You search for whatever you're looking for, whether it's particular subject, whether it's particular

198
00:12:12,690 --> 00:12:15,030
email and it's from a date that it was sent on.

199
00:12:15,390 --> 00:12:22,200
You get back the type and data and the data recall is just a list of actual email I.D. Once you have

200
00:12:22,200 --> 00:12:24,240
the email I.D., you need to fetch them.

201
00:12:24,600 --> 00:12:29,120
And here are fetching a singular email I.D. and we pass in this protocol for fetching it.

202
00:12:29,850 --> 00:12:34,080
And then we have the email data and it's kind of up to you what you want to do at that point, because

203
00:12:34,350 --> 00:12:35,190
it's a bit of a mess.

204
00:12:35,220 --> 00:12:40,530
But one way you can deal fit is using this email library and then using this walk functionality to check

205
00:12:40,530 --> 00:12:42,000
for particular content types.

206
00:12:42,810 --> 00:12:43,090
All right.

207
00:12:43,140 --> 00:12:43,620
That's it.

208
00:12:43,860 --> 00:12:44,340
Thank you.

209
00:12:44,430 --> 00:12:45,690
And I'll see you at the next lecture.
