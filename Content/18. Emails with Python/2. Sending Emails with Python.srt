1
00:00:05,530 --> 00:00:06,250
Welcome back.

2
00:00:06,370 --> 00:00:09,400
In this lecture, we're going to explore sending emails with Python.

3
00:00:10,510 --> 00:00:11,920
To send emails with Python.

4
00:00:11,980 --> 00:00:14,050
We actually have to go through a lot of steps.

5
00:00:14,350 --> 00:00:19,930
We have to connect to what's known as an e-mail server, confirm that connection, setup up a communication

6
00:00:19,930 --> 00:00:24,220
protocol, log into your actual e-mail account and then send the message.

7
00:00:25,240 --> 00:00:32,200
Now, fortunately, the built in s.m T.P Lib library that's already in Python makes these steps just

8
00:00:32,200 --> 00:00:33,550
really simple function calls.

9
00:00:33,820 --> 00:00:39,580
So essentially, just making sure that you call the function from the s.m T.P Lib library in the correct

10
00:00:39,610 --> 00:00:40,000
order.

11
00:00:40,690 --> 00:00:42,700
So I might be wondering what is s.m T.P?

12
00:00:43,420 --> 00:00:49,900
Well, each major email provider has her own simple mail transfer protocol server and it's essentially

13
00:00:49,930 --> 00:00:54,460
a domain name that you connect to when you're trying to access your email programmatically.

14
00:00:54,970 --> 00:01:01,570
And since G Mail is the most popular email hosting service at this time, we're gonna do is walk you

15
00:01:01,570 --> 00:01:03,620
through how to connect to G Mail.

16
00:01:03,700 --> 00:01:05,820
So we'll be using that G mail domain name.

17
00:01:07,200 --> 00:01:09,150
So we'll go over this process vagy mail account.

18
00:01:09,660 --> 00:01:15,360
Now, something the special of G.M. is for G.M. users because of security, things that have to do that

19
00:01:15,360 --> 00:01:16,830
are built into a G.M. account.

20
00:01:17,190 --> 00:01:21,510
You're going to need to generate what's known as an app password instead of your normal password.

21
00:01:21,650 --> 00:01:27,930
And this is essentially not the password you use to log in onto your email from just any computer.

22
00:01:28,380 --> 00:01:32,100
Instead, what's going to happen is will create a specialized app password.

23
00:01:32,430 --> 00:01:37,320
And that's basically telling G.M., hey, you have a special application somewhere out there in the

24
00:01:37,320 --> 00:01:41,700
world and it's going to connect to your Gmail account through this separate password.

25
00:01:42,090 --> 00:01:47,160
That way, in case this application gets compromised, somehow that password that you originally used

26
00:01:47,220 --> 00:01:48,100
is not compromised.

27
00:01:48,150 --> 00:01:52,300
So you're able to create a special password just for your Python script.

28
00:01:52,650 --> 00:01:57,930
And it also lets [REMOVED] know that, hey, you're specifically authorizing this other application.

29
00:01:58,020 --> 00:02:01,230
Our Python script to actually access your account.

30
00:02:02,430 --> 00:02:02,620
OK.

31
00:02:02,670 --> 00:02:04,410
So let's explore this entire process.

32
00:02:05,220 --> 00:02:08,860
We'll begin by importing the S empty IP.

33
00:02:08,920 --> 00:02:11,310
That's a simple mail transfer protocol library.

34
00:02:11,370 --> 00:02:12,570
So s.m TDP Lib.

35
00:02:13,900 --> 00:02:18,880
And then what we're going to do is create an s.m T.P object for a server and you can look at the table

36
00:02:18,880 --> 00:02:21,730
in the corresponding notebook for your server domain name.

37
00:02:21,760 --> 00:02:24,100
But we'll be using Gmail as our server.

38
00:02:24,820 --> 00:02:27,610
So we'll say s m t.p object.

39
00:02:30,360 --> 00:02:34,140
Is equal to M t.P Lib.

40
00:02:34,740 --> 00:02:42,150
And we'll call s.m T.P function off of this, and then we'll provide the actual domain server which

41
00:02:42,150 --> 00:02:45,630
is s.m t.p dot G.M. dot com.

42
00:02:46,350 --> 00:02:48,830
And then for G.M., we can also provide a port.

43
00:02:48,840 --> 00:02:49,190
No.

44
00:02:49,350 --> 00:02:51,090
So we'll try five, eight, seven.

45
00:02:51,570 --> 00:02:56,400
If for some reason the number five, eight, seven doesn't work and when you try using this and running

46
00:02:56,400 --> 00:02:58,020
this, you get back some sort of error.

47
00:02:58,380 --> 00:02:59,350
Something else you can try.

48
00:02:59,370 --> 00:03:01,350
Is using the port number four six five.

49
00:03:01,800 --> 00:03:04,960
If that also doesn't work, you can try not passing in any number for the port.

50
00:03:04,980 --> 00:03:05,340
No.

51
00:03:05,760 --> 00:03:11,100
Keep in mind, if you have a firewall or antivirus that may prevent your Python script from trying to

52
00:03:11,100 --> 00:03:12,810
reach out and connect over the Internet.

53
00:03:14,080 --> 00:03:16,340
The next step we need to do is run an e h.

54
00:03:16,490 --> 00:03:21,130
L o method command, which greets the server and establishes the connection.

55
00:03:21,610 --> 00:03:24,370
This method calls should be done directly after creating the object.

56
00:03:24,520 --> 00:03:25,720
So make sure you follow these steps.

57
00:03:25,720 --> 00:03:26,260
Exactly.

58
00:03:26,800 --> 00:03:27,540
Calling it after.

59
00:03:27,570 --> 00:03:31,360
Other methods will basically result in errors and connecting later on.

60
00:03:31,750 --> 00:03:36,660
So right after you create the object, the very next line should be running this E.H., L.O..

61
00:03:37,150 --> 00:03:44,410
So we'll say grab our object and run E.H. Yellow, which essentially greets the server.

62
00:03:44,650 --> 00:03:48,830
And when you run this, you should see some sort of code that starts with 250.

63
00:03:49,000 --> 00:03:51,910
And this basically says that we had a successful connection.

64
00:03:52,900 --> 00:03:55,600
Now, when using the Port five, eight, seven.

65
00:03:55,660 --> 00:03:58,090
This means you're using t ellas encryption.

66
00:03:58,510 --> 00:04:00,760
So basically all email as you send it is encrypted.

67
00:04:00,790 --> 00:04:04,510
That way people can't read your emails without being the actual recipient.

68
00:04:05,350 --> 00:04:11,650
And in order to actually initiate this sort of encryption, we're going to grab our object again.

69
00:04:12,460 --> 00:04:13,750
s.M T.P object.

70
00:04:14,050 --> 00:04:17,530
And then we'll call the method starts T.L. s.

71
00:04:20,010 --> 00:04:24,930
If you ended up using the port four, six, five, you can basically skip the step because that means

72
00:04:24,930 --> 00:04:25,950
you're using SSL.

73
00:04:28,340 --> 00:04:28,520
OK.

74
00:04:28,580 --> 00:04:31,250
You should've gone to code to 20 and it says, Ray, it's a start.

75
00:04:31,320 --> 00:04:35,420
T. Ellis, now it's time to set up the e-mail and the passwords.

76
00:04:35,810 --> 00:04:41,210
Keep in mind that you should never save the rostering of your password or e-mail in a script, because

77
00:04:41,210 --> 00:04:45,680
basically that means that anyone that sees the script will be able to then see your e-mail and password.

78
00:04:46,220 --> 00:04:50,300
Instead, what you should do is try using the input function to get that information.

79
00:04:50,840 --> 00:04:55,130
If you don't want your password to be visible when you're typing it in, you can also use the built-In

80
00:04:55,160 --> 00:04:58,490
get pass library that will hide your password as you type it in.

81
00:04:58,820 --> 00:05:04,040
To further explain what we mean by this, let's imagine we use the input function to save a password.

82
00:05:04,670 --> 00:05:07,840
So we'll say, what is your password?

83
00:05:09,000 --> 00:05:12,930
And let's say we're going to save this to some variable called password.

84
00:05:13,350 --> 00:05:18,180
If we were to run this, if I were to type my password here, that would be visible to the to the user.

85
00:05:18,420 --> 00:05:19,380
So what if I type here?

86
00:05:19,620 --> 00:05:22,020
Someone's going to be able to see it if they're watching my computer.

87
00:05:22,980 --> 00:05:27,480
Now, a better way of doing this is say import get pass.

88
00:05:28,720 --> 00:05:35,920
And then once you've done that, we'll say password is equal to and you'll run get past that get pass.

89
00:05:35,980 --> 00:05:37,990
And you should be saving this to a variable.

90
00:05:39,610 --> 00:05:40,660
And then we can provide.

91
00:05:42,160 --> 00:05:43,570
Password, please.

92
00:05:44,080 --> 00:05:48,040
And then if we run this and we start typing something, you'll notice that it's invisible.

93
00:05:48,100 --> 00:05:49,780
And you can type a really long password here.

94
00:05:50,030 --> 00:05:53,740
And even after you run it, it won't really indicate how long your password was.

95
00:05:53,860 --> 00:05:58,200
So this is a secure way to pass on information for people that are viewing your computer.

96
00:05:58,240 --> 00:05:58,760
That rectally.

97
00:05:58,840 --> 00:06:02,950
So just in case that happens, you should be using the get pass instead of just a simple input.

98
00:06:03,370 --> 00:06:07,240
It really depends on how secure you want your script to be or your notebook to be.

99
00:06:07,450 --> 00:06:11,290
But we should never do is just save your password as a variable directly.

100
00:06:11,590 --> 00:06:17,110
So you should never just do something like this password and then say, you know, password one, two,

101
00:06:17,110 --> 00:06:17,440
three.

102
00:06:17,530 --> 00:06:19,720
That's pretty insecure and pretty dangerous.

103
00:06:19,750 --> 00:06:22,030
So you may end up losing your password that way.

104
00:06:23,700 --> 00:06:26,670
OK, now let's go over a second step.

105
00:06:26,700 --> 00:06:31,950
And this is specifically for G.M. users where we need to generate an app password instead of our normal

106
00:06:31,950 --> 00:06:32,700
email password.

107
00:06:33,150 --> 00:06:35,820
And this also requires enabling two step authentication.

108
00:06:36,300 --> 00:06:41,670
So there's a link in the notebook that you can follow as far as the instructions set up, two step factor

109
00:06:41,670 --> 00:06:43,710
authentication as well as the app password.

110
00:06:44,160 --> 00:06:46,230
So let's go to that link right now.

111
00:06:46,980 --> 00:06:52,080
So if you go to that link, it will take you to sign it using app passwords and it will say right here

112
00:06:52,110 --> 00:06:53,610
if you use two step verification.

113
00:06:53,640 --> 00:06:56,520
So click on that two step verification link.

114
00:06:56,610 --> 00:06:59,100
And we need to turn on two step verification.

115
00:06:59,580 --> 00:07:01,430
So this is actually quite simple.

116
00:07:01,440 --> 00:07:03,570
Just go to the two step verification page here.

117
00:07:03,870 --> 00:07:07,590
You might have to sign into your Google account, select, get started and then follow the step by step

118
00:07:07,590 --> 00:07:08,220
process.

119
00:07:08,340 --> 00:07:10,560
So if you click here on two step verification.

120
00:07:12,240 --> 00:07:14,940
They'll say, protect your account with two step verification.

121
00:07:15,060 --> 00:07:20,220
And I actually recommend this doing any doing this anyways, even if you don't intend to use python

122
00:07:20,220 --> 00:07:22,890
free e-mail because it's just safer for your email address.

123
00:07:23,250 --> 00:07:25,550
So click get started once you've signed in.

124
00:07:25,620 --> 00:07:30,300
You should see something that looks like this and that basically sets up your phone number as the second

125
00:07:30,300 --> 00:07:31,980
factor of authentication.

126
00:07:32,140 --> 00:07:36,840
So you're going to need to do this if you want to be able to send and receive emails with Python on

127
00:07:36,840 --> 00:07:37,650
a G mail account.

128
00:07:38,070 --> 00:07:42,930
So you'll go ahead and type in the phone number that you want to use and they'll confirm you with either

129
00:07:42,930 --> 00:07:44,670
a text message or a phone call.

130
00:07:44,760 --> 00:07:48,150
And you'll go through those steps and then you'll have to step verification.

131
00:07:48,480 --> 00:07:53,400
Ready to go if you already have two step verification that you won't actually see this pop up.

132
00:07:53,430 --> 00:07:55,900
Instead, it'll just take you to your security's page and Gmail.

133
00:07:56,430 --> 00:07:58,440
And that basically indicates that you're actually ready to go.

134
00:07:58,440 --> 00:07:59,970
You already had to step verification.

135
00:08:00,360 --> 00:08:06,720
So you can confirm that if you've ever received some sort of security code on your phone from G.M. So

136
00:08:06,720 --> 00:08:08,430
that's setting up two step verification.

137
00:08:08,760 --> 00:08:11,660
Once you've done that, we'll be able to setup our app password.

138
00:08:11,730 --> 00:08:13,080
So let's go back to that link.

139
00:08:13,680 --> 00:08:16,110
And this is the link to sign and using app passwords.

140
00:08:16,590 --> 00:08:19,860
So we'll scroll down here and we're going to now generate an app password.

141
00:08:20,310 --> 00:08:23,400
We'll open this and we'll click this app passwords page.

142
00:08:23,730 --> 00:08:25,650
And there's some instructions here on how to generate them.

143
00:08:25,980 --> 00:08:27,420
Go ahead, click on out passwords.

144
00:08:28,080 --> 00:08:32,910
If you get this result here, it means that you have not setup two factor authentication and you need

145
00:08:32,910 --> 00:08:34,350
to go back and do that.

146
00:08:34,830 --> 00:08:40,080
So let's go ahead and show you what it looks like when you have set up two factor authentication with

147
00:08:40,080 --> 00:08:42,050
two step verification authorized.

148
00:08:42,090 --> 00:08:44,100
You'll have this app passwords page.

149
00:08:44,520 --> 00:08:46,200
You'll go ahead and select an app.

150
00:08:46,260 --> 00:08:48,330
In this case, we want to use our mail app.

151
00:08:48,630 --> 00:08:50,370
So that's the app we want to interact with.

152
00:08:50,530 --> 00:08:51,810
Then we can select the device.

153
00:08:52,280 --> 00:08:54,240
We will select our custom name.

154
00:08:54,360 --> 00:08:59,070
And this will just be something like Script Python or really whatever you want to call it.

155
00:08:59,130 --> 00:09:04,320
Just something memorable so you can understand why you generated this app password then hit generate.

156
00:09:05,560 --> 00:09:07,540
And it will generate an app, passwd, for you.

157
00:09:07,630 --> 00:09:09,220
So you will copy this.

158
00:09:10,340 --> 00:09:15,410
And save it somewhere on your computer, because that's the password are going to be using for our actual

159
00:09:15,460 --> 00:09:24,770
log in, so we'll come back here to our notebook and let's actually set up our e-mail so we'll show

160
00:09:24,770 --> 00:09:26,540
you how to use get past just to remind you.

161
00:09:26,720 --> 00:09:27,550
We'll say get passed.

162
00:09:27,550 --> 00:09:28,220
Get pass.

163
00:09:29,290 --> 00:09:34,840
E mail, we will say password is equal to get pass.

164
00:09:36,180 --> 00:09:37,470
Don't get pass.

165
00:09:39,210 --> 00:09:39,930
Password.

166
00:09:42,230 --> 00:09:44,750
And then finally, we will say s.m T.P.

167
00:09:45,920 --> 00:09:51,260
Object we'll call the Log-in method from that email and password.

168
00:09:52,450 --> 00:09:53,560
So we're going to run this.

169
00:09:53,770 --> 00:09:55,720
So go ahead and type in your email address.

170
00:09:55,930 --> 00:09:58,930
In this case, I'm typing in a G.M. address.

171
00:10:02,620 --> 00:10:07,780
And the next thing I'm going to do is I'm going to paste in that app password.

172
00:10:07,840 --> 00:10:09,790
So, again, this is not your normal Gmail password.

173
00:10:09,850 --> 00:10:11,170
This is the app password.

174
00:10:12,040 --> 00:10:16,390
So once you've done that, you should see a code indicating that it was accepted.

175
00:10:17,110 --> 00:10:18,960
Now we're able to send email.

176
00:10:19,360 --> 00:10:24,460
Keep in mind, if you stop right here, walk away from your computer for a few hours and then come back,

177
00:10:24,730 --> 00:10:27,400
you may be disconnected just because it took so long.

178
00:10:28,270 --> 00:10:29,720
So keep so keep that in mind.

179
00:10:29,980 --> 00:10:34,510
You're going to want to if you're in the notebook setting, do all your sending of your email immediately

180
00:10:34,510 --> 00:10:35,510
after logging in.

181
00:10:35,830 --> 00:10:38,320
Otherwise, you may get disconnected and you'll have to regenerate.

182
00:10:38,380 --> 00:10:43,150
Another app password so we can basically send an email.

183
00:10:44,410 --> 00:10:47,370
We're going to need a couple of things, we'll need a from address.

184
00:10:47,440 --> 00:10:50,410
So we need to indicate where the actual e-mails coming from.

185
00:10:50,680 --> 00:10:52,340
This is going just to be your email again.

186
00:10:52,390 --> 00:10:54,630
So we'll say from address is equal to my email.

187
00:10:55,450 --> 00:10:57,190
We need a two address.

188
00:10:57,310 --> 00:10:59,950
And that's going to be the email address that you want to send it to.

189
00:11:00,010 --> 00:11:01,660
That's the e-mail of your recipient.

190
00:11:01,990 --> 00:11:06,640
I will send myself an email here and then the subject line.

191
00:11:07,930 --> 00:11:09,400
So we'll have that being input.

192
00:11:09,430 --> 00:11:12,310
So enter the subject line.

193
00:11:13,990 --> 00:11:16,300
And then we'll have the actual body or message.

194
00:11:16,780 --> 00:11:21,060
We can also have that be input and through the body message.

195
00:11:23,060 --> 00:11:30,460
And then the way the message is constructed is asked to be subject colon space.

196
00:11:32,220 --> 00:11:36,520
Then the actual subject itself and then a new line.

197
00:11:36,770 --> 00:11:39,270
And that indicates that the subject has stopped.

198
00:11:39,810 --> 00:11:44,880
And then you paste in the rest of the message because the way the sexual function is going to work is

199
00:11:44,880 --> 00:11:49,670
it's going to accept one giant string to indicate both the subject and the message body.

200
00:11:50,070 --> 00:11:54,730
And in order to differentiate between the subject and the message, it needs this sort of format.

201
00:11:54,810 --> 00:11:55,590
Subject.

202
00:11:56,080 --> 00:11:58,760
Colon space than the actual subject title.

203
00:11:59,190 --> 00:12:00,450
And then a new line.

204
00:12:00,750 --> 00:12:01,950
And then the rest of your message.

205
00:12:03,130 --> 00:12:07,400
So once you've done that, you'll say s.m T.P object.

206
00:12:08,220 --> 00:12:11,040
You'll call the send the male method off of this.

207
00:12:11,460 --> 00:12:13,500
And then you'll passant you're from address.

208
00:12:15,610 --> 00:12:18,150
Your to address, so the others are sending, too.

209
00:12:18,250 --> 00:12:20,260
And then the actual string message.

210
00:12:21,310 --> 00:12:24,700
So I'm sending right now an address for myself to myself.

211
00:12:24,910 --> 00:12:28,180
I recommend you just do the same that we can quickly check it once you log in.

212
00:12:28,630 --> 00:12:29,530
But let's run this.

213
00:12:30,400 --> 00:12:33,590
We'll enter the subject line and let's just enter subject.

214
00:12:34,240 --> 00:12:37,090
New test, python, whatever really you want.

215
00:12:37,540 --> 00:12:38,740
And then enter the body message.

216
00:12:38,770 --> 00:12:39,340
Hello.

217
00:12:39,400 --> 00:12:40,330
This is a test.

218
00:12:40,530 --> 00:12:41,910
Or again, whatever you want to send.

219
00:12:42,460 --> 00:12:42,970
Run that.

220
00:12:43,330 --> 00:12:44,770
And right now it's sending it.

221
00:12:45,220 --> 00:12:48,520
If you get back an empty dictionary, that means the sending was successful.

222
00:12:48,550 --> 00:12:51,520
So you should get something like this back if you're getting an error.

223
00:12:51,610 --> 00:12:57,370
It's most likely having to do with either your connection up here or your actual email and password

224
00:12:57,370 --> 00:12:58,000
combination.

225
00:12:58,930 --> 00:13:02,590
Once you've been able to do that, you'll want to quit and close your session.

226
00:13:03,430 --> 00:13:04,870
So you will say SMK, t.P.

227
00:13:06,560 --> 00:13:11,030
Object dot quit, and that will now close my connection.

228
00:13:11,480 --> 00:13:14,960
Let's log in using our browser to see if we received the message.

229
00:13:15,080 --> 00:13:18,800
And then later on we'll see how we could do that same sort of functionality with Python.

230
00:13:19,100 --> 00:13:21,170
So I'm going to now go to my e-mail inbox.

231
00:13:21,580 --> 00:13:26,240
And in my email inbox I can see sent zero minutes ago is a message from myself.

232
00:13:26,270 --> 00:13:27,170
So blind carbon.

233
00:13:27,170 --> 00:13:27,770
Copy me.

234
00:13:28,070 --> 00:13:31,010
Hello, this is the test and the title is New Test Python.

235
00:13:31,400 --> 00:13:32,810
Looks like it was all successful.

236
00:13:33,440 --> 00:13:34,400
Excellent work so far.

237
00:13:34,730 --> 00:13:36,080
We'll see you in the next lesson.

238
00:13:36,110 --> 00:13:40,560
Or we discuss how to do this sort of checking automatically with Python.
