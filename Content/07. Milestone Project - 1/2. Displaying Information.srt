1
00:00:05,230 --> 00:00:11,140
Welcome back, everyone, to this lecture on displaying information and really what we're going to learn

2
00:00:11,140 --> 00:00:19,240
here is how we can combine a custom function using a DFAT deaf call along with the already built in

3
00:00:19,240 --> 00:00:25,660
print function in order to show something to the user that actually displays like a board game, for

4
00:00:25,660 --> 00:00:26,200
instance.

5
00:00:26,560 --> 00:00:30,250
So let's go ahead and jump to Jupiter, notebook and explore more of what I'm talking about.

6
00:00:30,730 --> 00:00:30,950
All right.

7
00:00:30,970 --> 00:00:32,770
Here I am within a Jupiter notebook.

8
00:00:32,980 --> 00:00:36,670
And as I mentioned, we already know that I can print out variables.

9
00:00:36,880 --> 00:00:41,350
So if I have a list, something like one, two, three, I can simply print that out.

10
00:00:41,800 --> 00:00:47,410
And if I want to print out multiple lines, what I would have to do is have essentially a print statement

11
00:00:47,770 --> 00:00:49,570
for those lines.

12
00:00:49,600 --> 00:00:53,320
So let's say I wanted to print one, two, three, and then maybe right below it.

13
00:00:53,440 --> 00:00:55,840
I also want to print out four, five, six.

14
00:00:56,080 --> 00:00:59,230
And let's say right below that, we'll have one more list.

15
00:00:59,650 --> 00:01:00,370
Seven, eight, nine.

16
00:01:00,940 --> 00:01:03,310
So when I run that all in one cell, I get back.

17
00:01:03,910 --> 00:01:04,840
This display.

18
00:01:05,530 --> 00:01:10,300
Now, instead of having to call each of these three print functions over and over again, what it could

19
00:01:10,300 --> 00:01:13,270
do is put all of this within my own custom function.

20
00:01:13,990 --> 00:01:18,430
So I'll say the F and let's say call this display.

21
00:01:20,110 --> 00:01:28,550
And let's imagine that we have three rows of information here, so I'll pass in row one, row two and

22
00:01:28,550 --> 00:01:31,670
row three as arguments or parameters into this function.

23
00:01:32,360 --> 00:01:36,020
And then what this function does is it simply prints out row one.

24
00:01:37,040 --> 00:01:45,590
On top of Rotu, which is then on top of row three, so I've defined the function, which means now

25
00:01:45,590 --> 00:01:46,400
I can display it.

26
00:01:47,470 --> 00:01:50,110
So let's go ahead and make an example, Ro.

27
00:01:52,710 --> 00:01:55,260
And my example, Roe, for now, we'll just have it be.

28
00:01:56,430 --> 00:01:57,270
One, two, three.

29
00:01:57,900 --> 00:02:03,510
Which means if I pass an example, a row for each of these rows or one row to row three.

30
00:02:04,110 --> 00:02:06,540
Let's go ahead and pass it in two more times.

31
00:02:06,570 --> 00:02:07,560
I'll say example row.

32
00:02:07,650 --> 00:02:10,950
And I'm just using tab here to complete this example.

33
00:02:10,950 --> 00:02:16,920
Row one, I run this notice now instead of having to call print, print, print three times.

34
00:02:17,250 --> 00:02:23,490
I simply call display once and now it's flexible to whatever variables or parameters I have.

35
00:02:25,020 --> 00:02:29,970
And then the last thing I want to show you to kind of drive this point home, the flexibility of showing

36
00:02:30,030 --> 00:02:33,840
a display to a user is technically this doesn't have to be a list.

37
00:02:33,870 --> 00:02:36,000
This could be a string or any other object.

38
00:02:36,390 --> 00:02:39,090
So what I want to do is it's going to make three unique rows.

39
00:02:39,160 --> 00:02:42,600
We'll say row one is one, two, three, row two.

40
00:02:43,170 --> 00:02:47,220
And what I'm going to do here is I'm actually going to have just some empty strings.

41
00:02:49,130 --> 00:02:51,980
And we'll have row three equal to.

42
00:02:53,160 --> 00:02:54,690
We'll have another set of empty strings here.

43
00:02:57,300 --> 00:03:00,750
In fact, let's just copy and paste this and make this row one as well.

44
00:03:01,140 --> 00:03:05,400
So we essentially have just three empty strings and three lists.

45
00:03:06,060 --> 00:03:14,370
So now I'm going to changes from example row to reflect Roe one, Roe two in row three.

46
00:03:15,070 --> 00:03:18,000
And something to keep in mind is this can go in another cell.

47
00:03:18,450 --> 00:03:19,740
So it does have to be in the same cell.

48
00:03:19,800 --> 00:03:24,810
I could add one cell, define row one, row two or three, and then the next cell.

49
00:03:24,840 --> 00:03:26,670
Then have this display function.

50
00:03:27,330 --> 00:03:31,800
So when I run this, I get back these lists with these empty spaces.

51
00:03:32,460 --> 00:03:38,100
And what we can do is later on in our program, let's say we've set up kind of a rudimentary tic tac

52
00:03:38,100 --> 00:03:38,640
toe board.

53
00:03:39,530 --> 00:03:46,350
I could say road to go ahead and grab the center position, which if we think about this, the center

54
00:03:46,350 --> 00:03:54,450
position here of this three by three is going to be Rotu index one because we go zero one.

55
00:03:55,110 --> 00:03:57,870
So that means wrote to an index one.

56
00:03:58,610 --> 00:04:01,200
And let's reassign this to be something like an X.

57
00:04:02,680 --> 00:04:08,610
And now what I'm going to do is display those rows again, row one row to row three.

58
00:04:09,270 --> 00:04:11,850
I run this and now I see an X here.

59
00:04:12,440 --> 00:04:18,660
And this is a very rudimentary way of just taking in inputs and then displaying something that looks

60
00:04:18,660 --> 00:04:22,170
like something a user can visually interpret.

61
00:04:22,620 --> 00:04:25,260
This is just a very simple tic tac toe board.

62
00:04:25,620 --> 00:04:27,460
It's not the nicest looking tic tac toe board.

63
00:04:27,570 --> 00:04:30,800
You can get even more flexible with using things like strings.

64
00:04:30,840 --> 00:04:33,390
In order to not actually see these quotes.

65
00:04:33,420 --> 00:04:36,990
So instead of having a list here, you just kind of have this be a long string.

66
00:04:37,320 --> 00:04:41,490
And then you can add things like dashes in order to make it look even nicer.

67
00:04:41,760 --> 00:04:44,520
But this is just a very rudimentary display.

68
00:04:45,000 --> 00:04:50,520
And the next step we want to do is to be able to take in user input in order to have something like

69
00:04:50,520 --> 00:04:58,590
this, a reassignment of a particular position in a row or a list be actually specified by the user

70
00:04:58,590 --> 00:04:59,220
themselves.

71
00:04:59,700 --> 00:05:07,140
So this is just our overall lecture on displaying information in a simple way that the user can understand.

72
00:05:07,530 --> 00:05:10,920
The next thing I want to do is be able to accept user input.

73
00:05:11,460 --> 00:05:14,790
So we've already gone over the input function and useful operators lecture.

74
00:05:15,120 --> 00:05:17,730
So if you don't remember it, you might want to do a quick review of that lecture.

75
00:05:18,000 --> 00:05:24,450
But coming up next, we're going to expand this idea of displaying information to accepting user input.

76
00:05:25,030 --> 00:05:25,930
Okay, thanks.

77
00:05:26,010 --> 00:05:27,060
And I'll see you at the next lecture.
