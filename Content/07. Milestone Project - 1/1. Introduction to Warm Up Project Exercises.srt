1
00:00:05,340 --> 00:00:10,920
Welcome, everyone, to this milestone project section of the course, we're going to begin this section

2
00:00:10,950 --> 00:00:14,250
by going through a couple of warm up project exercises.

3
00:00:15,770 --> 00:00:21,260
We're almost ready for you to begin creating a full interactive Python program for your upcoming milestone

4
00:00:21,260 --> 00:00:21,740
project.

5
00:00:22,100 --> 00:00:24,710
You'll be creating interactive tic tac toe game.

6
00:00:26,020 --> 00:00:31,450
In order to warm up for this project, we're going to coat along with a few exercises in order for you

7
00:00:31,450 --> 00:00:34,030
to see how to use Python code to do the following.

8
00:00:34,600 --> 00:00:41,080
Grab user input, manipulate a variable in your Python script or program based off that input and then

9
00:00:41,080 --> 00:00:43,300
return back the adjusted variable.

10
00:00:43,930 --> 00:00:49,660
So you actually already know all the python necessary in order to create something like a tic tac toe

11
00:00:49,660 --> 00:00:49,930
game.

12
00:00:50,440 --> 00:00:56,200
However, often for beginners, it's hard to see how to connect all these moving parts and pieces to

13
00:00:56,200 --> 00:00:59,290
create something that appears to be an interactive program.

14
00:00:59,650 --> 00:01:04,660
So that's what we're gonna have just a couple of exercises and short lectures in order to show you just

15
00:01:04,660 --> 00:01:06,640
some simple examples of how this can be done.

16
00:01:07,180 --> 00:01:11,740
But first, let's just describe what an interactive program does in general.

17
00:01:13,190 --> 00:01:17,810
So you have to have some sort of visual representation that your user will see.

18
00:01:18,260 --> 00:01:24,140
So, for example, here I'm using slides to actually display information and then.

19
00:01:25,130 --> 00:01:28,820
If you're building a game, that could be something like a tic tac toe board.

20
00:01:29,240 --> 00:01:33,890
So in any interactive program, there should be some sort of visual representation that a user can see.

21
00:01:34,520 --> 00:01:41,000
And then in order to update this program, the user has to make some sort of decision and input that

22
00:01:41,000 --> 00:01:43,610
decision based off what they visually see.

23
00:01:44,210 --> 00:01:49,430
So, for example, if you're playing tic tac toe, a user input would then be something like choose

24
00:01:49,430 --> 00:01:53,240
the top right corner for the oh in order to win this game.

25
00:01:54,990 --> 00:01:58,470
Then that user input would go into some sort of python function.

26
00:01:59,720 --> 00:02:07,010
Then inside that Python script or notebook, we update whatever the board variable is, and then we

27
00:02:07,010 --> 00:02:08,620
have a new visual to show the user.

28
00:02:09,760 --> 00:02:15,040
And then we have the updated visual representation and essentially this cycle can happen over and over

29
00:02:15,040 --> 00:02:18,520
again in order to have just an interactive fronting program.

30
00:02:18,760 --> 00:02:21,730
So this is a very simple example for a tic tac toe board.

31
00:02:22,000 --> 00:02:28,060
But essentially, the interactive piece of software you've used follows some very simple logic like

32
00:02:28,060 --> 00:02:28,390
this.

33
00:02:28,780 --> 00:02:34,450
Now, the actual script that runs that can be extremely complicated based off how many options there

34
00:02:34,450 --> 00:02:37,810
are and how complex the calculations can be.

35
00:02:38,080 --> 00:02:41,470
But overall, you're just simply showing a visual representation.

36
00:02:41,740 --> 00:02:48,190
Accepting user input, then having that be updated inside your Python script and then showing an updated

37
00:02:48,190 --> 00:02:49,780
visual over and over again.

38
00:02:51,430 --> 00:02:56,680
So, as I mentioned, most programs at our interactive work on this very simple idea display something

39
00:02:56,680 --> 00:02:57,670
visual to the user.

40
00:02:58,030 --> 00:03:02,140
Let the user update or an interaction update the variables within the program.

41
00:03:02,500 --> 00:03:07,120
And then those variables are then passed through in order to display the updated visual.

42
00:03:07,480 --> 00:03:12,220
So there's a connection there between the internal variables and what the user sees as the visual.

43
00:03:13,590 --> 00:03:18,210
So what we're going to do is in the next series of short lectures, we're going to guide you through

44
00:03:18,210 --> 00:03:21,870
examples of how to perform each of these individual tasks with Python.

45
00:03:22,530 --> 00:03:27,570
Keep in mind, there are many different ways of performing the same task, such as the splaying output

46
00:03:27,720 --> 00:03:30,990
or manipulating variables internally in your Python script.

47
00:03:31,380 --> 00:03:36,180
So I don't want you to feel restricted by the examples we show here as the only way you can do this.

48
00:03:36,510 --> 00:03:38,040
These are just simple examples.

49
00:03:38,130 --> 00:03:43,680
In order for you to kind of connect the dots between what you've already learned and creating an interactive

50
00:03:43,680 --> 00:03:44,100
program.

51
00:03:44,550 --> 00:03:45,600
OK, let's get started.

52
00:03:45,810 --> 00:03:46,790
I'll see you at the next lecture.
