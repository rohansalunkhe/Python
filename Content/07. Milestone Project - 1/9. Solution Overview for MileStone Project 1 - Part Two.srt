1
00:00:05,290 --> 00:00:06,650
Welcome back everyone.

2
00:00:06,700 --> 00:00:10,990
So finally it comes to Step 10 which is the harder part where we need to use while loops and the function

3
00:00:10,990 --> 00:00:14,110
we just defined above in order to actually run the game.

4
00:00:14,110 --> 00:00:18,820
So let's write out a little bit of logic and comments on the steps we need to take.

5
00:00:18,820 --> 00:00:26,410
So first off we're going to need some sort of while loop to keep running the game and then probably

6
00:00:26,410 --> 00:00:33,510
at the bottom of this while loop somewhere we're going to break out of the while loop on the replay

7
00:00:33,510 --> 00:00:34,300
function.

8
00:00:34,560 --> 00:00:37,260
So remember the replay function just asked if they want to play again.

9
00:00:37,440 --> 00:00:42,540
So if they don't want to play again we'll break out of this while loop that continually runs the game.

10
00:00:42,630 --> 00:00:49,580
So let's do that right now at the very top We'll just print welcome to tic tac toe tic tac toe

11
00:00:52,670 --> 00:01:00,170
and then we'll say while true and then we're going to play the game here.

12
00:01:00,240 --> 00:01:06,800
And finally somewhere at the very bottom we'll ask for the actual result of that replay.

13
00:01:06,810 --> 00:01:15,060
So say if not replay break because after they've played the game and somebody has one or there's been

14
00:01:15,060 --> 00:01:20,550
a time we're going to ask them we're going to call replay and then break out of this continuous while

15
00:01:20,550 --> 00:01:22,560
tree loop that any should always be very careful.

16
00:01:22,560 --> 00:01:25,550
These troops should always be a way to break out of them.

17
00:01:26,760 --> 00:01:28,050
Now it's time to play the game.

18
00:01:28,050 --> 00:01:32,210
So in order to play the game we first need to set everything up.

19
00:01:32,310 --> 00:01:39,550
What do you mean by that is like the board whose first and then the other thing we probably need to

20
00:01:39,550 --> 00:01:42,940
set up is what markers they choose.

21
00:01:42,940 --> 00:01:46,620
So let's say choose markers.

22
00:01:46,710 --> 00:01:48,760
So those are X's and O's.

23
00:01:49,480 --> 00:01:56,150
OK so you set those things up and then after that we'll have some sort of actual game play and the game

24
00:01:56,150 --> 00:02:06,300
play will probably consist of player 1 turn and then player to turn.

25
00:02:06,310 --> 00:02:08,590
So let's start off by setting everything up.

26
00:02:08,590 --> 00:02:10,670
The first thing I want to set up is the board.

27
00:02:10,670 --> 00:02:12,610
This should be pretty straightforward.

28
00:02:12,610 --> 00:02:17,560
We'll just set up a board and in my case I'm going to set up the board as a bunch of empty strings because

29
00:02:17,560 --> 00:02:22,300
when I set up the display board function up above I expect it to be kind of a bunch of empty strings

30
00:02:22,300 --> 00:02:22,650
there.

31
00:02:22,660 --> 00:02:24,670
There's lots of different ways to do this.

32
00:02:24,670 --> 00:02:28,360
And it really depends on how you set up your display board function.

33
00:02:28,780 --> 00:02:36,200
But in this case I'm going to set up my board as just a list of 10 empty strings and I'll say that player

34
00:02:36,200 --> 00:02:45,980
1 marker comma player 2 marker are going to be the result of player underscore input.

35
00:02:45,990 --> 00:02:51,210
So if we scroll back up to realize what player input was remember that's one of the very first functions

36
00:02:51,270 --> 00:02:57,600
you actually created which asked what player chose X or O.

37
00:02:57,630 --> 00:03:03,230
So we're going to say Player 1 A marker is equal to x and then Player 2 to 0 or the opposite.

38
00:03:03,330 --> 00:03:07,020
Because we're using tuple in packing here so that's what we're running that very first player input

39
00:03:07,020 --> 00:03:08,130
function.

40
00:03:08,250 --> 00:03:12,210
So we're calling that all the way at the bottom and I see that there are actual markers that we can

41
00:03:12,210 --> 00:03:19,130
call those variables later on then we choose who's goes first and say turn is equal to the function

42
00:03:19,130 --> 00:03:19,490
call.

43
00:03:19,520 --> 00:03:24,700
Choose first number that was basically that coin flip function and then let's print out a little report.

44
00:03:26,420 --> 00:03:30,280
We'll say turn we'll go first.

45
00:03:30,330 --> 00:03:36,480
So here if we remember what turn is choose first actually littery that returns just a string player

46
00:03:36,480 --> 00:03:38,490
one or player two.

47
00:03:38,520 --> 00:03:40,610
So we're going to use that as the indicator.

48
00:03:40,620 --> 00:03:41,780
Who's going first.

49
00:03:42,720 --> 00:03:47,480
So we say turn choose first player 1 or player two will go first because turn is the actual string.

50
00:03:47,670 --> 00:03:48,770
So we can cannonade it.

51
00:03:49,080 --> 00:03:55,920
And then finally once this is ready to go let's asks if they're ready to play the game so we'll create

52
00:03:55,920 --> 00:04:03,140
a variable called Play game and the input will be want to play I just say ready to play.

53
00:04:03,150 --> 00:04:03,620
Really.

54
00:04:03,660 --> 00:04:07,230
Because we already have a bunch of stuff ready to play.

55
00:04:07,230 --> 00:04:09,780
So they've set up the players.

56
00:04:09,780 --> 00:04:11,420
We decided who's going to go first.

57
00:04:11,580 --> 00:04:13,310
And we asked them Are you ready to play.

58
00:04:13,680 --> 00:04:21,260
And we'll have them say lowercase Y or N and it's up to you how you want to do this.

59
00:04:21,290 --> 00:04:27,970
We'll say if play a game is equal to y then the game is on.

60
00:04:27,990 --> 00:04:32,180
So I'm going to create a variable called game on and said equal to true.

61
00:04:32,410 --> 00:04:39,710
If for some reason they weren't ready to play it we'll just say game on is equal to false and albu.

62
00:04:39,750 --> 00:04:41,350
OK so now it's time for the game play.

63
00:04:41,470 --> 00:04:48,610
We'll assume that game on is true and then we'll say while game is on.

64
00:04:48,770 --> 00:04:50,150
So we have Player 1 turn.

65
00:04:50,210 --> 00:04:59,960
So let's say if turn is equal to the string player 1 we'll have players 1 actual turn and otherwise

66
00:04:59,990 --> 00:05:10,050
we'll have player to turn so if this will say else and this is player to turn OK.

67
00:05:10,050 --> 00:05:12,930
So let's set up the logic on player once turn.

68
00:05:12,930 --> 00:05:14,280
So what do we actually need to do here.

69
00:05:14,280 --> 00:05:18,840
Well the first thing I want to do is display the board otherwise the player won't know what the heck

70
00:05:18,840 --> 00:05:19,620
they're looking at.

71
00:05:20,760 --> 00:05:25,700
So I'll say the splay board and we'll pass and that board object that we just created.

72
00:05:25,940 --> 00:05:30,520
And the next thing we do is well it's right up the steps here.

73
00:05:30,530 --> 00:05:31,730
We'll show the board.

74
00:05:32,750 --> 00:05:34,270
What's the player seen the board.

75
00:05:34,430 --> 00:05:39,320
They can choose a position once they've done that.

76
00:05:39,440 --> 00:05:49,140
We will place the marker on the position that they chose and then we just need to check if they won

77
00:05:52,770 --> 00:05:57,520
or check if there is a tie.

78
00:05:57,550 --> 00:06:07,660
And then finally no tie and no win then next player's turn.

79
00:06:07,660 --> 00:06:09,460
So that's the logic of what we're going to do here.

80
00:06:09,640 --> 00:06:13,610
And this entire logic is going to be the exact same thing for a player one player or two.

81
00:06:13,690 --> 00:06:16,160
We're just going to add at the actual variable names.

82
00:06:16,300 --> 00:06:20,320
So we showed the board that's easy the splay board function we defined earlier.

83
00:06:20,320 --> 00:06:25,060
Now we want to choose a position that's also pretty straightforward because we actually already have

84
00:06:25,930 --> 00:06:29,350
a position function called player choice.

85
00:06:29,410 --> 00:06:34,330
So remember that if we call back player choice and let's make sure we've defined that appear so.

86
00:06:34,370 --> 00:06:40,180
Player choice was just this one right here where we have him choose an actual position on the map or

87
00:06:40,240 --> 00:06:42,380
I should say all aboard.

88
00:06:42,610 --> 00:06:53,150
We say position is equal to player choice and we pass in the board here and then finally we're going

89
00:06:53,150 --> 00:07:01,020
to call the place marker function where we take in the current board we say Player 1 marker because

90
00:07:01,020 --> 00:07:09,470
this is the player 1 turn and then we pass in the position they chose which is defined up here.

91
00:07:09,470 --> 00:07:11,500
So we showed the board we choose a position.

92
00:07:11,570 --> 00:07:15,650
We placed a marker on that position and I went to check if they won again.

93
00:07:15,650 --> 00:07:17,330
We already defined the function for that.

94
00:07:17,330 --> 00:07:22,640
It was called Win check and it takes in the board and then that player's marker in our case it's going

95
00:07:22,640 --> 00:07:27,320
to be Player 1 marker and we can display the board

96
00:07:30,130 --> 00:07:30,930
the board.

97
00:07:31,180 --> 00:07:35,130
So if when Check happens to be true then Player 1 has one.

98
00:07:35,410 --> 00:07:45,080
So that case will display the board and print out player 1 has 1 and once somebody is going to win the

99
00:07:45,080 --> 00:07:47,890
game we'll just say game on is not equal to false.

100
00:07:47,900 --> 00:07:52,160
So the game is no longer active which will break out of this while loop we'll still have this while

101
00:07:52,160 --> 00:07:56,120
loop right here asking people if they want to play again but this while loop of the actual game play

102
00:07:56,450 --> 00:07:57,530
will now be broken.

103
00:07:57,530 --> 00:07:59,870
When we say the game has been won.

104
00:08:00,020 --> 00:08:04,820
So if we don't have a win then this is an excuse which means we should check if there's a tie.

105
00:08:04,850 --> 00:08:06,350
So how do we check if there's a tie.

106
00:08:06,590 --> 00:08:08,410
Well we can check if there's a tie.

107
00:08:08,630 --> 00:08:10,210
If the board is full.

108
00:08:10,310 --> 00:08:15,020
So if we've already check and nobody is one then we should check if the board is full.

109
00:08:15,080 --> 00:08:19,050
And if the board is fool and nobody's won then that means we have a tie.

110
00:08:19,120 --> 00:08:23,560
So we already actually have a full board check and let's check it up over here.

111
00:08:24,850 --> 00:08:27,770
Remember over here we have the full board check function.

112
00:08:27,880 --> 00:08:30,690
So we're going to be using that and returns back the Blinn.

113
00:08:30,700 --> 00:08:31,720
True or false.

114
00:08:31,810 --> 00:08:39,950
So I'll say if full board check on the current board there were kind of passing around here will display

115
00:08:39,950 --> 00:08:40,400
the board.

116
00:08:40,400 --> 00:08:48,650
So this means that someone hasn't won an else if the board is full will display the board and then we'll

117
00:08:48,650 --> 00:08:50,600
say something like The game's a tie

118
00:08:53,510 --> 00:08:58,300
tie game and then we will break out of the loop.

119
00:08:58,330 --> 00:09:01,630
Could also set game on equal to false either way.

120
00:09:01,860 --> 00:09:07,860
And then we can turn equal to Player 2.

121
00:09:08,180 --> 00:09:13,480
So let's actually just keep this game on convention so instead of breaking we'll say game on is equal

122
00:09:13,480 --> 00:09:14,470
to false.

123
00:09:14,470 --> 00:09:16,190
So what's actually happening here.

124
00:09:16,450 --> 00:09:21,750
For players one turn we display the board we choose a position or player one chooses the position.

125
00:09:21,760 --> 00:09:23,620
We put that marker on the board.

126
00:09:23,710 --> 00:09:25,760
We check if they've won if they did win.

127
00:09:25,780 --> 00:09:27,090
Game on is now called the False.

128
00:09:27,130 --> 00:09:29,530
And this entire while loop is over.

129
00:09:30,040 --> 00:09:35,850
If they never won then we have two things that we need to check if the board is full.

130
00:09:36,000 --> 00:09:38,770
And if the board is full then it's a tie game.

131
00:09:38,770 --> 00:09:39,550
We print out the board.

132
00:09:39,550 --> 00:09:40,530
Show them what it looks like.

133
00:09:40,530 --> 00:09:42,900
That is full and we say game on is it false.

134
00:09:43,000 --> 00:09:47,160
And this entire while loop is over otherwise nobody's won.

135
00:09:47,170 --> 00:09:48,940
There's still space on the board to go.

136
00:09:48,940 --> 00:09:51,010
That means it's time for Player 2 to go.

137
00:09:51,370 --> 00:09:56,680
And we're going to essentially copy and paste all this code because player 2 is going to do a really

138
00:09:56,680 --> 00:09:57,590
similar thing.

139
00:09:57,670 --> 00:09:59,330
It's just wherever player one is.

140
00:09:59,410 --> 00:10:01,550
Player 2 is going to be and vice versa.

141
00:10:02,970 --> 00:10:04,710
So let's delete these comments here.

142
00:10:05,830 --> 00:10:10,940
And now we have this statement and I want to zoom out just a little bit so we can see the entire logic

143
00:10:10,940 --> 00:10:11,630
here.

144
00:10:11,630 --> 00:10:14,310
Notice how I have if turn is equal to player 1.

145
00:10:14,360 --> 00:10:18,460
That's this entire block of code else is now lined up with this.

146
00:10:18,470 --> 00:10:24,650
If so else here signifies that this player to turn and I'm going to paste all the code here.

147
00:10:24,650 --> 00:10:29,690
So same thing I need to show the board I need to choose a position I need to place the marker except

148
00:10:29,690 --> 00:10:32,520
this time it's going to be the player to marker.

149
00:10:32,570 --> 00:10:34,280
So that's a change we're making.

150
00:10:34,400 --> 00:10:40,220
And then we want to win check for again the player 2 marker and we display the board.

151
00:10:40,310 --> 00:10:44,730
And in this case player two would have won that game on it was a false.

152
00:10:44,730 --> 00:10:48,500
Else we say if full bore check this play the board game.

153
00:10:48,530 --> 00:10:55,010
And finally if Player 2 didn't win and there's no tie then we go back to turn being player 1 and that's

154
00:10:55,010 --> 00:10:55,130
it.

155
00:10:55,270 --> 00:10:56,440
That's player twos turn.

156
00:10:56,510 --> 00:10:59,430
And let's check this out and see if that actually worked.

157
00:10:59,480 --> 00:11:04,030
We'll run this coup we'll choose X for player one will say ready to play.

158
00:11:04,190 --> 00:11:04,730
Yes.

159
00:11:04,790 --> 00:11:08,850
Choose a position 1 and let's kind of play this out.

160
00:11:09,110 --> 00:11:10,310
Keep going like this.

161
00:11:10,460 --> 00:11:14,480
And then we should see Player X wins so player 1 has one perfect.

162
00:11:14,660 --> 00:11:16,810
And let's go ahead and play again.

163
00:11:17,990 --> 00:11:21,350
OK so we just saw how the win works.

164
00:11:21,350 --> 00:11:26,500
Now let's run this again and check to make sure that our tie checking is also working.

165
00:11:26,540 --> 00:11:30,400
So I'm going to type in 0 2 Player 1 is going to be 0.

166
00:11:30,410 --> 00:11:31,550
Player 2 will go first.

167
00:11:31,550 --> 00:11:33,230
That means X is going to go first.

168
00:11:33,320 --> 00:11:34,220
We're ready to play.

169
00:11:34,250 --> 00:11:35,510
Time to choose a position.

170
00:11:35,780 --> 00:11:37,990
And hopefully I can actually get a tie here.

171
00:11:38,180 --> 00:11:42,010
This is harder than it looks to play against yourself and try to purposefully tie.

172
00:11:42,140 --> 00:11:43,790
But I will do my best here.

173
00:11:43,910 --> 00:11:51,410
Let's say two let's say four and now 0 is going to block at position 7 and then x.

174
00:11:51,410 --> 00:11:59,770
We'll have them go let's say position in the middle five we'll have 0 block at position 9 and then X

175
00:11:59,780 --> 00:12:06,800
we'll have them go kind of somewhere useless let's say 3 and then oh will purposefully block off X here

176
00:12:07,310 --> 00:12:10,550
and we'll have them go at position 6.

177
00:12:10,710 --> 00:12:15,160
And then finally we'll have X go in position 8.

178
00:12:15,660 --> 00:12:16,640
And it's a tie game.

179
00:12:16,650 --> 00:12:22,500
So the tie check worked as well and we don't no longer need to play again or say no and that's it.

180
00:12:22,610 --> 00:12:24,220
That's the game.

181
00:12:24,260 --> 00:12:24,630
All right.

182
00:12:24,680 --> 00:12:27,040
So definitely a really big project.

183
00:12:27,060 --> 00:12:33,110
But hopefully now that you're on the other end of it you are able to see how you can use functions to

184
00:12:33,110 --> 00:12:35,510
create a larger block of code.

185
00:12:35,510 --> 00:12:40,400
Now if you're treating this as a code along project what I would highly suggest is if you just viewed

186
00:12:40,400 --> 00:12:46,070
the entire solutions go back and see if you can now go through the workbook notebook on your own after

187
00:12:46,070 --> 00:12:48,570
you've referenced this entire solution code.

188
00:12:48,710 --> 00:12:53,570
It's going to be a really great help to your skills to be able to try to attempt this independently

189
00:12:53,930 --> 00:12:56,870
even if you already watched the solutions lecture.

190
00:12:56,870 --> 00:12:57,900
All right thanks everyone.

191
00:12:57,950 --> 00:13:02,660
If you have any questions feel free to post to the Q&amp;A forums and I will be there to help you out.

192
00:13:02,930 --> 00:13:03,950
I'll see you at the next lecture.
