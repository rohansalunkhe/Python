1
00:00:05,340 --> 00:00:07,140
Welcome back everyone in this lecture.

2
00:00:07,140 --> 00:00:11,550
We're going to formally go through the solutions for the Workbook for the Milestone project one that

3
00:00:11,550 --> 00:00:12,650
tic tac toe game.

4
00:00:12,960 --> 00:00:15,020
Let's open up the notebook and get started.

5
00:00:15,050 --> 00:00:15,350
Right.

6
00:00:15,360 --> 00:00:18,790
Here we are at Milestone project one walks through steps workbook.

7
00:00:18,840 --> 00:00:22,770
Let's start with step one which is to create a function that can print out the board.

8
00:00:22,770 --> 00:00:25,530
And there's lots of different ways you could have printed out the board or you could have gone if the

9
00:00:25,530 --> 00:00:29,520
really fancy board or a really simple one that just displays the list.

10
00:00:29,520 --> 00:00:32,660
But remember we're expecting board to be a list.

11
00:00:32,670 --> 00:00:37,470
So the first couple of things we're going to need is actually print out the index positions of that

12
00:00:37,470 --> 00:00:38,070
list.

13
00:00:38,070 --> 00:00:46,440
So would be nice if we did something like board one plus and then some sort of indicator that we have

14
00:00:46,530 --> 00:00:55,900
a new column there so we can do that and then we'll do board that two plus and then another pipe operator

15
00:00:57,600 --> 00:01:02,220
and then finally board three and then I'm going to just copy and paste this

16
00:01:05,560 --> 00:01:08,710
and knowledge change up the index positions.

17
00:01:08,710 --> 00:01:14,140
So 7 8 and 9 on top and it's up to you if you want to reverse that.

18
00:01:14,200 --> 00:01:19,860
But four five six will always be in the middle there and let's check with this looks like if we display

19
00:01:19,860 --> 00:01:20,640
the board.

20
00:01:20,880 --> 00:01:22,690
So we run this.

21
00:01:23,100 --> 00:01:23,910
We display the board.

22
00:01:23,940 --> 00:01:25,740
And right now my test board is blank.

23
00:01:25,740 --> 00:01:32,150
So let me copy over the test board from the original workbook and I want to run this I get back my board.

24
00:01:32,160 --> 00:01:35,740
So this is a really simple version of the board but it works fine.

25
00:01:35,760 --> 00:01:39,540
Now something to note here is that if I run display board multiple times.

26
00:01:39,540 --> 00:01:41,750
So let me call it twice here.

27
00:01:41,850 --> 00:01:45,890
What's going to happen is actually see a history of all the boards here.

28
00:01:46,230 --> 00:01:51,180
So in order to avoid that what we could do is call clear output first.

29
00:01:51,240 --> 00:01:55,590
So if I call clear output and as an alternative if you're running that's the command line you can use

30
00:01:55,590 --> 00:02:01,170
this print statement we talked about if I now have clear output what's going to happen is I only see

31
00:02:01,230 --> 00:02:02,580
one version of the board.

32
00:02:02,670 --> 00:02:06,610
So this would display the board and the next time the board gets displayed.

33
00:02:06,630 --> 00:02:09,990
So next time you call this play board it's going to clear the output for you.

34
00:02:10,350 --> 00:02:14,590
So this is a really simple version of printing up the board and we can keep it for now.

35
00:02:14,940 --> 00:02:21,000
But on wound's copy over and paste the solution workbook which is essentially the same idea.

36
00:02:21,060 --> 00:02:24,120
It just has a little sleeker bit of formatting here.

37
00:02:24,120 --> 00:02:28,260
So if I run this I get back kind of a more spacious board to look at.

38
00:02:28,260 --> 00:02:29,830
It's up to you what you want to do here.

39
00:02:29,850 --> 00:02:30,930
Lots of different options.

40
00:02:30,930 --> 00:02:36,070
Main idea was to be able to index board and then printed out.

41
00:02:36,190 --> 00:02:41,830
Step two was to write a function that can take in a player input and assign their marker as X or O.

42
00:02:42,010 --> 00:02:43,870
Again lots of different ways we can do this.

43
00:02:43,910 --> 00:02:48,560
That's something we definitely wanted to do was to use a while loop that where we can continually ask.

44
00:02:48,610 --> 00:02:50,830
And so you get a correct answer.

45
00:02:50,830 --> 00:02:58,570
So I'm going to set marker as an empty string basically anything that's not X or so and I could do this

46
00:02:58,660 --> 00:03:08,100
two ways I could see it while marker is not equal to x and marker is not equal to 0.

47
00:03:08,540 --> 00:03:21,680
So I could do it this way and then say marker is equal to input player 1 choose X or 0.

48
00:03:23,690 --> 00:03:29,120
And then I can do is uppercase this in case you accidently put in a lower case X or lower case so.

49
00:03:29,210 --> 00:03:31,820
And finally I'll return some sort of tuple assignment.

50
00:03:32,180 --> 00:03:41,800
So I'll say if marker is equal to x return x comma.

51
00:03:42,210 --> 00:03:44,770
And so the format here I can write myself a little no.

52
00:03:44,840 --> 00:03:53,250
In fact this would be a good case for a docstring is to say that the output is in the form of a tuple

53
00:03:53,970 --> 00:04:03,440
with the player marker comma player two marker.

54
00:04:03,460 --> 00:04:08,130
So then when the check player input the docstring said Tell me what the actual output looks like.

55
00:04:08,350 --> 00:04:12,780
So you can check out the previous lecture for a more detailed version of this.

56
00:04:12,790 --> 00:04:15,580
This is kind of a cleaner version and we'll say.

57
00:04:15,580 --> 00:04:17,130
Else return.

58
00:04:18,410 --> 00:04:22,050
Oh and X and this allows us then to use tuple and packing.

59
00:04:22,050 --> 00:04:26,970
So if I run this then I can use tuple in packing something like this to get back player 1 marker and

60
00:04:26,970 --> 00:04:28,350
player 2 marker.

61
00:04:28,350 --> 00:04:33,750
So I run the input if I pass an X enter and I took up my player to marker it's going to be the opposite

62
00:04:33,750 --> 00:04:34,490
of X which is.

63
00:04:34,490 --> 00:04:38,180
Oh and if I check on my player one marker it's going to be X..

64
00:04:38,190 --> 00:04:39,830
So different ways you could do this.

65
00:04:39,840 --> 00:04:44,400
The other thing I want to mention is instead of doing it with not equal to and not equal to you could

66
00:04:44,400 --> 00:04:56,140
have said while not marker equal to x or marker equal to zero.

67
00:04:57,770 --> 00:05:03,340
So while not this case or this case two different ways to think about it logically.

68
00:05:03,410 --> 00:05:08,320
It's kind of up to you which you prefer living on two step three.

69
00:05:08,500 --> 00:05:14,830
We wanted to write a function that takes the board list object a marker X or 0 0 to position number

70
00:05:14,890 --> 00:05:17,220
1 through 9 and then assigns it to the board.

71
00:05:17,440 --> 00:05:28,530
So we can do here is a place marker board marker position and I'll say board at that position is equal

72
00:05:28,530 --> 00:05:32,310
to marker.

73
00:05:32,350 --> 00:05:35,530
So let's run a test to make sure this actually works.

74
00:05:35,660 --> 00:05:39,120
We're going to do is say place marker I have right now a test board.

75
00:05:39,310 --> 00:05:44,230
So if I take a look at my test board looks like it's the one from above where I have a hash tag first

76
00:05:44,380 --> 00:05:47,240
to kind of block out the zero and the X's and O's.

77
00:05:47,380 --> 00:05:49,690
I'm going to say put in something new there.

78
00:05:49,690 --> 00:05:53,340
So let's make it really obvious a dollar sign and then put it up position 8.

79
00:05:53,510 --> 00:05:57,290
So going to placed a marker and then display the test board here.

80
00:05:57,310 --> 00:06:02,650
So let's run that and we can see that it suspects successfully put in the dollar sign at the position

81
00:06:02,650 --> 00:06:04,570
we want.

82
00:06:04,660 --> 00:06:05,920
And that was just a simple.

83
00:06:05,980 --> 00:06:13,110
Basically at that point board position placed a marker there OK step four was to write a function that

84
00:06:13,110 --> 00:06:19,440
takes and that board list and some marker like X or 0 and checks to see if that marker has won.

85
00:06:19,440 --> 00:06:22,540
So we have to ask ourselves what does it mean to win.

86
00:06:22,830 --> 00:06:30,000
Tic tac toe Well it means that we need to check in the possible combinations so we know that there's

87
00:06:30,020 --> 00:06:32,280
going to be three across.

88
00:06:32,480 --> 00:06:43,420
So we need to check all rows and check to see if they all share the same marker so if they all share

89
00:06:43,420 --> 00:06:45,190
the same Xs or they all share the same.

90
00:06:45,190 --> 00:06:47,690
Those two need to do that for all the rows.

91
00:06:47,740 --> 00:06:49,800
Then when needed to that for all the columns.

92
00:06:49,870 --> 00:06:52,870
So that's going to be three columns there.

93
00:06:53,100 --> 00:06:56,790
Check to see if marker matches.

94
00:06:56,790 --> 00:06:59,540
And then finally we have two diagonals to check.

95
00:07:00,710 --> 00:07:01,550
Diagonals

96
00:07:04,220 --> 00:07:06,000
and check to see if we have matricular

97
00:07:09,220 --> 00:07:14,940
now we can use indexing to grab the example rows the example columns in the example diagonals.

98
00:07:15,040 --> 00:07:19,270
I'm going to show you different ways to do this and then we'll copy and paste from the solutions because

99
00:07:19,270 --> 00:07:21,300
it's a lot of checks.

100
00:07:21,340 --> 00:07:26,860
So one way we can check for everything matching on all the rows is the way we showed you in the solutions

101
00:07:26,860 --> 00:07:30,570
notebook which is to say bored to the bottom row.

102
00:07:30,670 --> 00:07:40,900
So we need a check board of one equal to the mark and board to make sure I check that right board to

103
00:07:42,170 --> 00:07:50,330
equal to the mark and board to three equal to the mark.

104
00:07:50,340 --> 00:07:57,740
So what this does is it goes ahead and checks hey is the position at board 1 equal to the marker is

105
00:07:58,020 --> 00:08:01,620
position up or equal to that marker where were that markers are saying that you passed to they're going

106
00:08:01,620 --> 00:08:02,540
to be X or O.

107
00:08:02,670 --> 00:08:05,700
So if it passed then x Don't say at this position is X.

108
00:08:05,910 --> 00:08:07,700
And at this position it is x.

109
00:08:07,710 --> 00:08:09,150
And at this position is x.

110
00:08:09,300 --> 00:08:12,380
If we have this case true then we know someone has won.

111
00:08:12,390 --> 00:08:15,160
It's whatever this mark is has won.

112
00:08:15,270 --> 00:08:21,480
And in that case we would just return back some sort of bullying that they've won the game and then

113
00:08:21,480 --> 00:08:28,770
all we could do is wrap this in a princes right here and then have an OR statement and then check for

114
00:08:28,770 --> 00:08:36,860
the next row so we could say board four is equal to Mark and so on and so on five six in the next it

115
00:08:36,860 --> 00:08:38,160
would be seven eight nine.

116
00:08:38,400 --> 00:08:45,690
And then we could do the same for the columns and the columns would be things like 7 4 1 8 5 2 9 6 3.

117
00:08:45,750 --> 00:08:51,870
And then we had the two animals which is going to be those numbers 7 5 3 and 9 5 1.

118
00:08:51,900 --> 00:08:56,160
Now this is how we did it in the notebook's solution because it's a little more obvious what's happening

119
00:08:56,160 --> 00:08:56,790
here.

120
00:08:56,880 --> 00:09:02,430
But as an alternative instead of using these and operators along with checking mark for each individual

121
00:09:02,430 --> 00:09:12,360
index position which you could also do is just check that board of four is equal to four to five is

122
00:09:12,360 --> 00:09:16,070
equal to board at six.

123
00:09:16,120 --> 00:09:18,990
And finally check that this is equal to the mark.

124
00:09:19,000 --> 00:09:24,610
The reason you cannot just do no checks to the mark is because if the board starts starts off blank

125
00:09:24,670 --> 00:09:26,110
then it's going to report back.

126
00:09:26,170 --> 00:09:29,110
Hey these three blank positions match and someone's already won.

127
00:09:29,290 --> 00:09:32,310
So then you have to check it off with the Mark finally.

128
00:09:32,320 --> 00:09:35,650
So this right here is the same logic as what we saw up here.

129
00:09:35,650 --> 00:09:38,190
It's just maybe a little sleeker a little cleaner.

130
00:09:38,200 --> 00:09:38,810
It depends.

131
00:09:38,810 --> 00:09:40,200
To you what is more readable.

132
00:09:40,450 --> 00:09:45,910
But I'm going to copy and paste this from the solution book because it's a lot of checks and if you

133
00:09:45,910 --> 00:09:50,410
want to be a little clever you can make a little for loop for the rows a little for that for the columns

134
00:09:50,620 --> 00:09:52,580
and then manually right in the diagonals.

135
00:09:52,690 --> 00:09:55,230
But let me copy and paste the solution.

136
00:09:55,240 --> 00:09:56,580
And briefly go over it.

137
00:09:56,590 --> 00:10:01,300
Basically what we're doing here is we're doing that first way I mention we're checking across the top

138
00:10:01,450 --> 00:10:07,090
across the middle across the bottom down the middle and then down the other columns and then the two

139
00:10:07,090 --> 00:10:08,560
diagonals.

140
00:10:08,590 --> 00:10:08,960
All right.

141
00:10:08,980 --> 00:10:13,330
And what we're doing here is that we have an OR statement for each of these possible checks and then

142
00:10:13,420 --> 00:10:18,120
after all of this we're going to return the boolean in the kitting true or false.

143
00:10:18,120 --> 00:10:20,500
Has somebody won.

144
00:10:20,630 --> 00:10:21,960
So we can have a win check here.

145
00:10:21,980 --> 00:10:28,270
Let's define this win tic tac toe the end check and it looks like someone did win the tic tac toe.

146
00:10:28,580 --> 00:10:34,590
So let's can actually display the board so we can see the display board test board.

147
00:10:34,640 --> 00:10:39,680
So if we see this definitely X has one in both fashions here and it returns back true that this person

148
00:10:39,680 --> 00:10:40,290
has won.

149
00:10:40,430 --> 00:10:41,140
Great.

150
00:10:41,570 --> 00:10:42,370
So that's up 5.

151
00:10:42,380 --> 00:10:47,400
We want to write a function that uses the ran the module to randomly decide which player goes first.

152
00:10:47,480 --> 00:10:51,870
And as we mentioned you may want to look up random rant and we talked a little bit about it.

153
00:10:51,880 --> 00:10:54,680
The useful operator lectures but let's take a closer look.

154
00:10:54,680 --> 00:11:01,880
Now one way we can use the random dot ran into is to just pasand 0 and 1 and treat that as what is essentially

155
00:11:01,880 --> 00:11:03,120
a coin flip.

156
00:11:03,320 --> 00:11:14,050
So we can say the following we'll have our flip number equal to random Rand I.A. zero common 1.

157
00:11:14,060 --> 00:11:16,980
So this is randomly going to choose either 0 or 1.

158
00:11:17,120 --> 00:11:25,440
So we can think of that as heads or tails and then we'll say if the flip is equal to zero then we return

159
00:11:25,770 --> 00:11:29,530
let's say Player 1 as they're going first.

160
00:11:29,530 --> 00:11:34,970
Else we return the string player to for going first.

161
00:11:35,020 --> 00:11:38,770
And there's lots and lots of different ways it could get done this but it's kind of a simple way with

162
00:11:38,770 --> 00:11:40,520
the random rant.

163
00:11:40,570 --> 00:11:42,720
I.A. a random integer function.

164
00:11:43,090 --> 00:11:44,150
So let's run that.

165
00:11:44,530 --> 00:11:48,820
OK step number six was to write a function that returns a boolean indicating where there is space and

166
00:11:48,820 --> 00:11:50,370
the board is freely available.

167
00:11:50,560 --> 00:11:55,180
So it's going to take in that board which has a list and then taken a position in it wants to check

168
00:11:55,270 --> 00:11:56,680
if it's freely available.

169
00:11:56,830 --> 00:12:01,660
In our case freely available means that that position is currently an empty string.

170
00:12:01,750 --> 00:12:12,540
So to do that what I'm going to say is get the board at that position and see if it equals a blank string

171
00:12:13,260 --> 00:12:17,830
and since that's already itself going to be a boolean we're just going to return that.

172
00:12:18,040 --> 00:12:22,370
Then step seven is to write a function that checks the board is full and returns a boolean value.

173
00:12:22,470 --> 00:12:25,430
True Full false otherwise.

174
00:12:25,480 --> 00:12:34,740
So to do this we're going to run a for loop that goes for I in range 1 through 10 because there's nine

175
00:12:34,740 --> 00:12:38,440
spaces in Laborde starting from 1 and then up to and including 10.

176
00:12:38,680 --> 00:12:44,950
We're going to say if and then we can call our space check function that we just defined appear.

177
00:12:45,320 --> 00:12:51,220
So if space check using that board position.

178
00:12:51,260 --> 00:12:53,680
So that's all the positions possible on the board.

179
00:12:53,750 --> 00:13:00,740
If space check returns true then I'm going to just kind of kind of a double negative here but we're

180
00:13:00,740 --> 00:13:07,210
going to return false so what this means is if I have a space that means my board is not fool.

181
00:13:07,250 --> 00:13:13,780
So I will return false otherwise if I go throughout this entire four loop and I don't return false then

182
00:13:13,840 --> 00:13:16,630
I'll return true because my board is full.

183
00:13:17,020 --> 00:13:19,310
So bored is fool.

184
00:13:19,420 --> 00:13:26,740
If we return TRUE OK so that's the full board check function.

185
00:13:26,740 --> 00:13:32,770
Step is to write a function that asks for a player's next position as a number 1 through nine and then

186
00:13:32,770 --> 00:13:36,170
uses the function from Step 6 to check if it's in a free position.

187
00:13:36,310 --> 00:13:40,970
If it is we return the position for later use.

188
00:13:41,080 --> 00:13:47,150
So we'll do the following we'll say position and we'll have it be a placeholder at zero because we know

189
00:13:47,450 --> 00:13:48,860
that index 0.

190
00:13:48,910 --> 00:13:50,340
Nothing is going on there.

191
00:13:50,580 --> 00:13:57,780
And we can say while position is not in the list of possible positions.

192
00:13:57,780 --> 00:14:06,490
So that can be 1 2 3 4 5 6 7 8 9 we could have used range a long full list to make that happen.

193
00:14:06,770 --> 00:14:17,300
Or that space is not free so we can say or not space check Borda position will ask the player for a

194
00:14:17,300 --> 00:14:22,910
position we'll say position is equal to the integer of the input.

195
00:14:23,270 --> 00:14:30,780
Choose a position 9.

196
00:14:30,970 --> 00:14:34,000
And then finally after all this we are going to return that position.

197
00:14:34,210 --> 00:14:39,280
So all we're doing here is the following we'll say position starts off at zero and wealth position.

198
00:14:39,350 --> 00:14:43,660
If the player for some reason inputting the wrong number there in putting a letter that's going to be

199
00:14:43,660 --> 00:14:48,620
this check to see if it's an actual number that's on the board or it's going to check.

200
00:14:48,630 --> 00:14:50,830
Hey is that space still available.

201
00:14:51,160 --> 00:14:55,900
And then we're going to return the position once these have been satisfied so we should only be calling

202
00:14:55,900 --> 00:15:01,300
player choice later on after we check because the board has been fool to the board being fool is going

203
00:15:01,300 --> 00:15:05,290
to be a nice way to check if there's a tie because the board is fool and nobody's won yet.

204
00:15:05,290 --> 00:15:06,660
Then we know there's a tie.

205
00:15:07,150 --> 00:15:12,370
OK step nine is to write a function that asks the player if they want to play again and returns a boolean

206
00:15:12,370 --> 00:15:12,660
true.

207
00:15:12,670 --> 00:15:14,250
If they do want to play again.

208
00:15:14,520 --> 00:15:18,120
So lots of different ways we can do this.

209
00:15:18,170 --> 00:15:22,420
One way is to just say put play again.

210
00:15:23,150 --> 00:15:26,550
Question mark answer yes or no.

211
00:15:27,530 --> 00:15:30,510
And we're going to say that's going to be the response.

212
00:15:30,550 --> 00:15:37,770
Let's just have that be their choice and we'll say return choice equal to yes.

213
00:15:37,780 --> 00:15:44,200
So here we're assuming the person is actually putting in the correct values if we get choice not equal

214
00:15:44,200 --> 00:15:45,320
to yes.

215
00:15:45,370 --> 00:15:46,290
Basically they have to right.

216
00:15:46,300 --> 00:15:47,140
Yes perfectly.

217
00:15:47,140 --> 00:15:49,610
Otherwise we'll assume they don't want to play again.

218
00:15:49,630 --> 00:15:49,930
All right.

219
00:15:49,950 --> 00:15:51,340
And that's a replay function.

220
00:15:51,550 --> 00:15:53,110
Now step 10 is the hardest part.

221
00:15:53,110 --> 00:15:54,980
We actually have to code out all the logic.

222
00:15:55,090 --> 00:15:57,410
So this is going to be our largest block of code.

223
00:15:57,520 --> 00:15:59,640
So we'll finish this off in the next lecture.

224
00:15:59,650 --> 00:16:00,280
We'll see if there.
