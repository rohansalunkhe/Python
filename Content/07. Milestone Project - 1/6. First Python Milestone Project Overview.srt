1
00:00:05,600 --> 00:00:11,780
Welcome everyone to your first milestone project you now know enough to actually create a real program.

2
00:00:11,780 --> 00:00:17,640
And so since your first milestone project we're going to create a tic tac toe game for two human players.

3
00:00:17,660 --> 00:00:22,490
Let's describe what the game will be like and what approach you should take when tackling on your first

4
00:00:22,490 --> 00:00:23,760
milestone project.

5
00:00:24,690 --> 00:00:29,700
First off the basic idea of the game is that two players should be able to play the game both sitting

6
00:00:29,700 --> 00:00:34,870
at the same computer and the board is going to be printed out every time a player makes a move.

7
00:00:34,950 --> 00:00:39,600
You should be able to accept input of the player position and then place a symbol on the board.

8
00:00:39,600 --> 00:00:43,680
So we have these kind of larger problems and what you should do when you're thinking about this milestone

9
00:00:43,680 --> 00:00:47,520
project is try to break these down into smaller subproblems.

10
00:00:47,520 --> 00:00:52,590
And that idea of breaking down the larger problems into smaller subproblems is why we created a workbook

11
00:00:53,010 --> 00:00:54,120
DUPERE notebook for you.

12
00:00:54,120 --> 00:00:59,510
They can kind of go through and create functions in step by step fashion.

13
00:00:59,510 --> 00:01:00,310
Now this is crime.

14
00:01:00,320 --> 00:01:05,980
The actual idea here of putting keyboard input and converting that into something that corresponds to

15
00:01:05,980 --> 00:01:07,520
a tic tac toe board.

16
00:01:07,520 --> 00:01:10,490
We're going to be using the idea of a number pad.

17
00:01:10,490 --> 00:01:13,840
So if you have a larger keyboard often it has a number pad to the right of it.

18
00:01:14,120 --> 00:01:18,590
And we're going to match those numbers to the grid on a tic tac toe board and it's up to you which where

19
00:01:18,590 --> 00:01:21,890
you want your number pad to go on a keyboard it looks like this.

20
00:01:21,890 --> 00:01:26,510
One two three at the bottom seven eight nine top but often on an older phone model.

21
00:01:26,510 --> 00:01:31,960
When you deal the number pad it goes the opposite way has 1 to 3 on top 79 on bottom.

22
00:01:31,960 --> 00:01:33,420
Again totally up to you.

23
00:01:33,500 --> 00:01:39,890
The main idea here though is that in our mind we're going to have a tic tac toe board over this number

24
00:01:39,890 --> 00:01:43,890
pad and then a user they're going to using the input function.

25
00:01:43,970 --> 00:01:47,430
It's going to ask them hey where would you like to place your X or.

26
00:01:47,450 --> 00:01:51,990
Oh and then they'll provide a number that corresponds to the tic tac toe board.

27
00:01:52,160 --> 00:01:57,380
And what we're going to do is print out the tic tac toe board with an X at that location and then the

28
00:01:57,380 --> 00:02:01,960
next user is going to choose their number such as eight and they'll put the next symbol Oh there.

29
00:02:01,970 --> 00:02:04,070
So we're going to alternate between X and O.

30
00:02:04,250 --> 00:02:07,290
Ask the user for the input number and then placed that symbol on the board.

31
00:02:07,490 --> 00:02:12,330
And then later on we'll check to see if someone has one or if there is a tie.

32
00:02:12,670 --> 00:02:16,360
Now since creating your first full program is always a big leap forward.

33
00:02:16,480 --> 00:02:19,070
You're going to come out the other end a much better programmer.

34
00:02:19,150 --> 00:02:24,010
But since this is such a challenge we've set up a walkthrough notebook to help guide you along with

35
00:02:24,010 --> 00:02:26,670
the functions you're going to need to create.

36
00:02:26,680 --> 00:02:29,640
So let's explore what the games are going to look like once it's done.

37
00:02:29,770 --> 00:02:33,650
And then once you've done that we're going to cover a few useful functions and go through the walkthrough

38
00:02:33,670 --> 00:02:37,510
notebook and show you just a couple of things to help get you started.

39
00:02:37,510 --> 00:02:41,260
OK let's jump over to the actual notebook that has the solutions.

40
00:02:41,260 --> 00:02:43,330
I'm not going to show you the solutions right now.

41
00:02:43,330 --> 00:02:45,840
I'm just going to show you what the game looks like when you run it.

42
00:02:46,030 --> 00:02:47,730
OK here I am at the solution notebook.

43
00:02:47,740 --> 00:02:48,710
I'm pretty zoomed in.

44
00:02:48,730 --> 00:02:52,620
We only see a few lines of the solution code but there's a lot more code through this.

45
00:02:52,630 --> 00:02:56,710
But I just want to show you what this actually looks like an idea of what the game should look like

46
00:02:56,770 --> 00:03:00,360
when you run it doesn't have to look exactly like this but this is the main idea.

47
00:03:00,370 --> 00:03:03,730
First we asked the first player Hey do you want to be X or O.

48
00:03:03,970 --> 00:03:09,880
So using the input function we can pass in a string x enter and then I'll say hey player one is going

49
00:03:09,880 --> 00:03:10,490
to go first.

50
00:03:10,510 --> 00:03:11,700
Are you ready to play.

51
00:03:11,710 --> 00:03:13,150
Answer yes or no.

52
00:03:13,150 --> 00:03:18,760
So then we enter Yes and then you can imagine that that's starting some sort of while loop to continue

53
00:03:18,760 --> 00:03:19,730
playing the game.

54
00:03:19,870 --> 00:03:21,640
And then we have choose your next position.

55
00:03:21,700 --> 00:03:24,230
And notice here we've been able to print out a board.

56
00:03:24,640 --> 00:03:28,700
And these are basically just print functions with kind of larger strings.

57
00:03:28,730 --> 00:03:31,430
And in the middle of some of these are going to be curly braces.

58
00:03:31,660 --> 00:03:36,400
You can't see them right now because they're empty but they're curly braces that correspond to the index

59
00:03:36,410 --> 00:03:38,260
position of some list.

60
00:03:38,260 --> 00:03:44,110
And then when the player using the input function asks for the next position let's put in one we can

61
00:03:44,110 --> 00:03:49,990
see here that we automatically draw an X at that one location and then the next player says hey choose

62
00:03:49,990 --> 00:03:53,470
your next position let's have him choose a like we did in the slides.

63
00:03:53,470 --> 00:03:59,570
They hit enter and now we have oh there and then we choose an exposition and so on and so on until someone's

64
00:03:59,580 --> 00:04:00,300
won.

65
00:04:00,460 --> 00:04:07,310
So I can say two here and we'll have let's say nine for oh and let's make X win by putting in three.

66
00:04:07,630 --> 00:04:09,980
So here we have three in a row ticktock to has been won.

67
00:04:10,000 --> 00:04:12,130
And it says Congratulations you have won the game.

68
00:04:12,130 --> 00:04:14,450
And then we ask the player Hey do you want to play again.

69
00:04:14,500 --> 00:04:15,540
Answer yes or no.

70
00:04:15,790 --> 00:04:17,090
And let's answer no.

71
00:04:17,260 --> 00:04:17,920
Hit enter.

72
00:04:17,950 --> 00:04:20,100
And that breaks this cell.

73
00:04:20,140 --> 00:04:21,630
So you can see here there's a little hint here.

74
00:04:21,640 --> 00:04:27,690
If not replay so we can see here replay some sort of function that was accepting yes or no there.

75
00:04:27,730 --> 00:04:30,000
All right so that's the basics of what it should look like.

76
00:04:30,070 --> 00:04:35,440
Let's now hop over to the workbook notebook explain what that looks like in order to get you comfortable

77
00:04:35,440 --> 00:04:37,750
with the idea of lining up these functions.

78
00:04:37,960 --> 00:04:43,510
And then we're going to show you a couple of useful things in case you want a little head start on the

79
00:04:43,510 --> 00:04:46,010
solutions.

80
00:04:46,010 --> 00:04:50,960
All right so the actual notebooks if you go to complete Python 3 bootcamp either you download this or

81
00:04:50,960 --> 00:04:55,820
you're out at the repo come over it's a milestone project and you'll see a couple of useful notebooks.

82
00:04:55,820 --> 00:04:57,390
One is the assignment notebook.

83
00:04:57,500 --> 00:05:00,790
This assignment book is just a description of what it is you need to do.

84
00:05:00,800 --> 00:05:05,060
It's basically the notebook that corresponds to a lecture right now that you're viewing and it looks

85
00:05:05,060 --> 00:05:05,620
like this.

86
00:05:05,630 --> 00:05:09,470
It just says congratulations and a couple of bits of information.

87
00:05:09,470 --> 00:05:14,150
The note book they are mainly going to be working with is this walk through steps workbook and this

88
00:05:14,180 --> 00:05:16,500
notebook looks like this.

89
00:05:16,520 --> 00:05:21,410
So this is the walk through steps workbook and this is something we've designed to help you guide you

90
00:05:21,410 --> 00:05:23,390
through the functions you're going to need to create.

91
00:05:23,440 --> 00:05:28,550
Has some suggested tools such as the input function that you've seen before and then it also notes that

92
00:05:28,640 --> 00:05:34,330
if you want the input to be an integer it may be a good idea to cast it to an integer using I.A..

93
00:05:34,470 --> 00:05:38,160
Then we also have some information about how to clear the screen between moves.

94
00:05:38,180 --> 00:05:40,350
This is a special Jupiter notebook function.

95
00:05:40,490 --> 00:05:45,410
There's also kind of a simpler way to clear the screen which is just to print a hundred new lines and

96
00:05:45,410 --> 00:05:49,180
then you can scroll up to view the previous boards.

97
00:05:49,450 --> 00:05:52,950
All right so what does this actually look like as far as a workbook notebook.

98
00:05:52,960 --> 00:05:58,540
Well we've divided up the entire project into steps which have placeholder functions for you to fill

99
00:05:58,540 --> 00:05:58,920
out.

100
00:05:59,140 --> 00:06:04,680
And then we have test for each of those steps and you can see here step two test step two step three.

101
00:06:04,780 --> 00:06:06,280
And eventually you go along.

102
00:06:06,490 --> 00:06:10,430
And at the very end there's going to be less tests because you don't really need them.

103
00:06:10,660 --> 00:06:15,370
And finally they'll be kind of some ideas of how you should set up the logic for the game.

104
00:06:16,880 --> 00:06:18,880
So what does these functions actually look like.

105
00:06:18,890 --> 00:06:24,680
Well for instance the very first function wants you to write a function that can print out a board so

106
00:06:24,680 --> 00:06:29,480
you set up your board as a list or each index one through nine corresponds to a number on the number

107
00:06:29,480 --> 00:06:30,070
pad.

108
00:06:30,110 --> 00:06:32,610
So you get a three by three board representation.

109
00:06:32,900 --> 00:06:38,900
So what this is expected to happen is when you pass in a board list into this display board function

110
00:06:39,200 --> 00:06:44,990
you actually print out that board that way later on in your code you can actually call this function

111
00:06:45,080 --> 00:06:47,650
and it will continually print out the board whenever you want it.

112
00:06:47,870 --> 00:06:53,270
So in order to test this function we have test step 1 where we've created a test board for you some

113
00:06:53,270 --> 00:06:58,040
board that's already full and then you can say this play board then the next one is to write a function

114
00:06:58,040 --> 00:07:01,380
that takes player input and assign their marker as XOR o.

115
00:07:01,580 --> 00:07:05,430
Think about using loops and we continue along with these cells.

116
00:07:06,210 --> 00:07:08,490
All right so that's the basics of the workbook.

117
00:07:08,580 --> 00:07:12,870
Now since this is such a big project it's going to be kind of a big leap forward.

118
00:07:12,900 --> 00:07:17,940
What I would recommend you do is if you ever feel uncomfortable jump to the solutions and treat it as

119
00:07:17,940 --> 00:07:19,110
a code along lecture.

120
00:07:19,140 --> 00:07:24,330
And then once you're done coding along with that see if you can come back and independently fill out

121
00:07:24,330 --> 00:07:28,360
some of these functions on your own and that will really help you connect the dots.

122
00:07:28,380 --> 00:07:31,660
Dividing this problem into smaller subproblems.

123
00:07:31,720 --> 00:07:32,000
OK.

124
00:07:32,010 --> 00:07:35,100
So that's the main idea of the Milestone project if you want.

125
00:07:35,100 --> 00:07:37,020
Right now you can just stop this lecture.

126
00:07:37,020 --> 00:07:42,230
Go ahead and jump to it and you can also just open up a script or open up an empty Jupiter notebook

127
00:07:42,240 --> 00:07:45,570
file and see if you can do this on your own without the workbook.

128
00:07:45,600 --> 00:07:50,750
What I'm going to do now is give you a little bit of a headstart because I know it can seem very daunting.

129
00:07:50,850 --> 00:07:54,470
I know a lot of students they look at this step one and they just get totally stuck.

130
00:07:54,480 --> 00:07:58,650
There's kind of this roadblock in their mind of where it's actually begin.

131
00:07:58,650 --> 00:08:03,480
So I think it's really helpful if you're completely stuck on the idea of how do I even approach this

132
00:08:03,480 --> 00:08:04,290
task.

133
00:08:04,290 --> 00:08:08,820
I'm going to show you a way you could display the board and then I'll show you a little bit of step

134
00:08:08,820 --> 00:08:10,560
2 and a little bit of step three.

135
00:08:10,590 --> 00:08:11,660
So these are just hints.

136
00:08:11,670 --> 00:08:13,460
It's not the full solution walkthrough.

137
00:08:13,650 --> 00:08:15,490
It's just something to help get you started.

138
00:08:15,640 --> 00:08:18,930
Going open up this workbook and a Jupiter note book and continue on.

139
00:08:19,050 --> 00:08:23,420
If you don't want any hints on how to get started go ahead and just stop the lecture here.

140
00:08:23,430 --> 00:08:25,120
OK let's go to the Jupiter notebook.

141
00:08:25,450 --> 00:08:28,280
OK here I am at the walk through steps workbook.

142
00:08:28,320 --> 00:08:32,790
I'm going to work through a couple of first steps just to give you a little help pushing you forward

143
00:08:32,790 --> 00:08:33,790
through this project.

144
00:08:33,810 --> 00:08:38,010
Again if you want to tackle this on your own to start off with you can stop watching the lecture.

145
00:08:38,220 --> 00:08:42,280
First thing we're going to do is discuss actually printing out the board.

146
00:08:42,390 --> 00:08:47,470
So as we mentioned we kind of want you to set up the board as a list that looks like this.

147
00:08:47,580 --> 00:08:53,310
And at first that test board list or the board list it's going to be empty and we're going to use user

148
00:08:53,370 --> 00:08:58,110
input to index this list and then assign it the actual string value.

149
00:08:58,930 --> 00:09:02,130
So how do we actually display this board if it's a list.

150
00:09:02,470 --> 00:09:08,410
Well we could just use a bunch of print functions and I'm going to do a very simple version of the board

151
00:09:08,410 --> 00:09:11,320
here where I'm going to say prints.

152
00:09:11,680 --> 00:09:25,600
Let's say the board index 1 plus as a string some pipe operator and then the board object index to plus

153
00:09:26,140 --> 00:09:30,080
some type operator or any symbol you want really.

154
00:09:30,230 --> 00:09:39,720
And then concatenated with board at index three and then we're going to copy this and fill out the rest.

155
00:09:39,730 --> 00:09:42,370
So I say this and this.

156
00:09:42,370 --> 00:09:47,260
So one to three is at the bottom again you can kind of reverse that if you want and we'll have four

157
00:09:47,260 --> 00:09:48,350
five six here.

158
00:09:50,230 --> 00:09:52,090
And then nine eight and seven

159
00:09:54,910 --> 00:09:57,100
and let's run this and see what happens.

160
00:09:57,110 --> 00:09:57,530
Whoops.

161
00:09:57,550 --> 00:09:58,780
Invalid syntax.

162
00:09:58,780 --> 00:10:02,730
I forgot the plus sign here plus plus plus.

163
00:10:02,740 --> 00:10:03,060
OK.

164
00:10:03,100 --> 00:10:07,400
Now when I run this what I'm doing here is I'm going to print out whatever string is.

165
00:10:07,430 --> 00:10:11,770
Board 7 a pipe operator then whatever strings that board.

166
00:10:11,960 --> 00:10:17,230
Another pipe operator and then board 9 this is not going to look as nice as the tic tac toe board I

167
00:10:17,230 --> 00:10:17,880
just showed you.

168
00:10:17,980 --> 00:10:23,350
But it should work to some degree so I rerun this and just display the board so I'm going to pass on

169
00:10:23,350 --> 00:10:24,520
this list.

170
00:10:24,760 --> 00:10:32,950
I run this and here's my board x x x 0 0 0 0 0 0 and that's the basic idea of displaying the board and

171
00:10:32,950 --> 00:10:36,530
then later on you can get a little fancier if you want those horizontal lines.

172
00:10:36,550 --> 00:10:40,540
All you need to do is just print out a couple of dashes that maybe look like this.

173
00:10:40,540 --> 00:10:41,730
That's probably way too many.

174
00:10:41,770 --> 00:10:43,310
Let's just do something like that.

175
00:10:43,380 --> 00:10:47,810
And now when I rerun this you can see how that's slowly starting to kind of effect the board there.

176
00:10:48,010 --> 00:10:53,200
And maybe you want uppercase dashes for yourself so maybe you want something like these pipe operators

177
00:10:53,200 --> 00:10:59,100
here start lining them up and you can start to get really clever with the way this is going to look.

178
00:10:59,140 --> 00:11:02,070
So that's the basic idea of just displaying the board.

179
00:11:02,120 --> 00:11:08,080
We've taken the board itself as a list grab the index positions and then we will print out whatever

180
00:11:08,080 --> 00:11:09,790
value is there.

181
00:11:09,790 --> 00:11:14,740
Now keep in mind it's test board at the very start of the game is going to be probably a bunch of empty

182
00:11:14,740 --> 00:11:15,650
strings.

183
00:11:15,700 --> 00:11:19,000
So it's probably realistically going to look something like this.

184
00:11:19,800 --> 00:11:24,910
Well say just times ten here.

185
00:11:24,920 --> 00:11:26,630
It's going to look like some sort of empty board.

186
00:11:26,630 --> 00:11:29,280
So if I rerun this ill look like that.

187
00:11:29,510 --> 00:11:30,970
So that's where the board starts off.

188
00:11:31,010 --> 00:11:36,760
And then as people start actually putting in X's and O's it will clean up the board.

189
00:11:36,770 --> 00:11:43,010
Now if you keep calling display board multiple times you'll be able to see a history of the board if

190
00:11:43,010 --> 00:11:49,220
you don't want that what you could do is clear the output from the previous board.

191
00:11:49,280 --> 00:11:54,140
So later on you can experiment with running this clear output line or if you're running a Python script

192
00:11:54,470 --> 00:11:57,760
running this line right here which kind of prints out 100 new lines.

193
00:11:57,890 --> 00:12:03,170
And what this does is because later on during your logic if you keep asking to display the board it

194
00:12:03,200 --> 00:12:07,750
will show you the entire history of all the print functions or print statements you used to do.

195
00:12:07,960 --> 00:12:12,460
Well you could do instead is clear the output and then only show the board itself.

196
00:12:12,500 --> 00:12:16,540
That way it actually appears to the user that the board is being updated.

197
00:12:16,700 --> 00:12:18,990
Instead of printing out a new version of the board.

198
00:12:19,130 --> 00:12:22,150
So you can play around this to get an idea of what I'm talking about.

199
00:12:22,220 --> 00:12:25,450
And you should be doing this before you actually print out the new version of the board.

200
00:12:25,670 --> 00:12:31,400
So that's how you could display the board and I'll give you a couple more hints about that too.

201
00:12:31,860 --> 00:12:36,270
OK the next thing we want to do is write a function to taken a player input and assign their marker

202
00:12:36,270 --> 00:12:36,850
as XOR.

203
00:12:36,860 --> 00:12:41,620
So we want you to think about using while loops to continually ask until you get a correct answer.

204
00:12:41,730 --> 00:12:46,270
And there are a ton of ways you could do this but I'm going to show you just a quick example ways you

205
00:12:46,270 --> 00:12:48,840
can hopefully get the mindset ready.

206
00:12:49,290 --> 00:12:56,570
One way is to set your marker as an empty string and then we're going to do is keep asking one of the

207
00:12:56,570 --> 00:13:01,300
players such as Player 1 to choose X or O.

208
00:13:02,370 --> 00:13:12,760
And then once they've chosen we'll assigned player to the opposite marker so how can you keep asking

209
00:13:12,770 --> 00:13:25,030
clear want to choose X or so we can say while the marker is not equal to x and the marker is not equal

210
00:13:25,030 --> 00:13:26,160
to 0.

211
00:13:26,890 --> 00:13:30,360
So that means the markers were not equal to x and not equal to 0.

212
00:13:30,520 --> 00:13:36,520
Meaning someone hasn't supplied the correct input when I'm going to do a set marker equal to the input

213
00:13:37,610 --> 00:13:45,690
player 1 choose X or 0 That way if the player actually puts in a number or puts in another letter it

214
00:13:45,690 --> 00:13:52,080
just asks them for the input again and then once we have that input what I could do is say Player 1

215
00:13:53,040 --> 00:13:58,230
is going to be equal to that marker and then we'll have a little if statement here for player to say

216
00:13:58,230 --> 00:14:10,640
if Player 1 is equal to x player to well set that equal to 0 else.

217
00:14:10,860 --> 00:14:15,450
Player 2 is an equal to x because we know Player 1 chose.

218
00:14:15,900 --> 00:14:21,150
And finally what it can do in order to return both player once that is in player two status for their

219
00:14:21,150 --> 00:14:27,310
markers I can return this either as a list or a tuple that we can use tuple and packing later on.

220
00:14:27,330 --> 00:14:31,410
So I can say a return player 1 player to.

221
00:14:31,460 --> 00:14:33,730
So let's run this and make sure it works.

222
00:14:33,740 --> 00:14:36,150
So I'm going to call player input function here.

223
00:14:36,260 --> 00:14:38,740
Run that and it says Player 1 choose X row.

224
00:14:38,900 --> 00:14:41,960
So let's give it a wrong answer and see what happens.

225
00:14:41,960 --> 00:14:43,760
OK let's give it a number see what happens.

226
00:14:43,760 --> 00:14:46,550
So he keeps asking us hey choose X row.

227
00:14:46,910 --> 00:14:49,390
So if we choose an X then it reports back.

228
00:14:49,400 --> 00:14:50,750
OK X-No.

229
00:14:50,900 --> 00:14:54,270
So player 1 is now x and player 2 is no.

230
00:14:54,440 --> 00:14:57,690
And the way you can work with this is actually assign them to a tuple.

231
00:14:57,710 --> 00:15:01,890
So you could do is say tuple and packing in the following form.

232
00:15:02,060 --> 00:15:11,270
Player 1 marker variable comma player to mark a variable and set that equal to the output of player

233
00:15:11,270 --> 00:15:12,100
input.

234
00:15:12,110 --> 00:15:15,300
That way when you run this then you actually choose a value 0.

235
00:15:15,560 --> 00:15:20,300
You can then say Hey what was player 1's marker later on your code and I'll tell you it was 0 and if

236
00:15:20,300 --> 00:15:23,780
you asked for players to marker it was X. OK.

237
00:15:23,820 --> 00:15:26,990
So those are a couple of things to help you get started in the next lecture.

238
00:15:26,990 --> 00:15:30,950
We're going to go over all the formal solutions and we'll see you there.

239
00:15:30,950 --> 00:15:32,780
Best of luck on your first mousehole project.

240
00:15:32,930 --> 00:15:36,770
If you ever get stuck or have any questions feel free to post to the Q&amp;A forums.

241
00:15:36,770 --> 00:15:37,520
We'll see you there.
