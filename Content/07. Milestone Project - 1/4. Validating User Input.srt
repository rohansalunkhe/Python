1
00:00:05,390 --> 00:00:09,050
Welcome back, everyone, to this lecture on validating user input.

2
00:00:10,360 --> 00:00:15,760
So we've already seen how to use the input function to interact as a user and then how to convert that

3
00:00:15,760 --> 00:00:20,830
string data type from the input function into another data type, such as an integer.

4
00:00:21,160 --> 00:00:27,340
That way we can then use that input for other things within our program, such as indexing from a list.

5
00:00:27,940 --> 00:00:33,070
However, recall that if the user passed on something that couldn't be converted correctly, we would

6
00:00:33,070 --> 00:00:33,730
get an error.

7
00:00:34,240 --> 00:00:40,660
So what we want to explore now is how to further validate that user input to avoid errors for invalid

8
00:00:40,690 --> 00:00:46,210
conversions and instead basically have a while loop that keeps asking the user for input.

9
00:00:46,540 --> 00:00:49,870
Until we get a valid input and then continue onto the next step.

10
00:00:50,410 --> 00:00:52,960
Let's see how this is done by jumping to the Jupiter notebook.

11
00:00:54,940 --> 00:00:55,180
OK.

12
00:00:55,250 --> 00:01:00,260
Here I am at a Jupiter notebook recall, previously, we were playing around with this input function

13
00:01:00,620 --> 00:01:04,930
as well as coming back up and then trying to convert inputs.

14
00:01:05,360 --> 00:01:11,690
And the main thing to note here is that if the user provides an input, which isn't correct for a conversion,

15
00:01:11,990 --> 00:01:17,750
such as the string t w o for two, instead of just the actual number two, then we're gonna get this

16
00:01:17,840 --> 00:01:20,960
value error saying we can't actually convert this.

17
00:01:21,380 --> 00:01:28,220
So what we're going to do is use a while loop along with checking for the data type in order to not

18
00:01:28,220 --> 00:01:34,310
have this error occur, but instead keep asking the user for the correct input in order to do this.

19
00:01:34,340 --> 00:01:40,190
The first thing I want to do is basically take what we have here through the input function and actually

20
00:01:40,220 --> 00:01:42,350
make it within our own custom function.

21
00:01:43,100 --> 00:01:45,290
So we're gonna have D.F..

22
00:01:46,100 --> 00:01:51,800
And I'm going to call this user choice doesn't take in any parameters, but basically what it's going

23
00:01:51,800 --> 00:01:58,250
to do is say the choice is equal to the input and it will say please.

24
00:01:59,920 --> 00:02:01,240
Enter a number.

25
00:02:02,340 --> 00:02:04,690
And let's just go ahead and say maybe zero through 10.

26
00:02:06,960 --> 00:02:08,370
And then a call in space.

27
00:02:09,590 --> 00:02:15,710
And then it's going to return and it's going to attempt to make that choice string into an integer.

28
00:02:16,610 --> 00:02:20,840
So we have that and we're we're going to do here, say, user choice.

29
00:02:21,260 --> 00:02:22,190
Let's go ahead and run it.

30
00:02:22,430 --> 00:02:24,230
Again, we're not passing in any parameters.

31
00:02:24,470 --> 00:02:28,010
We just run this and we can see this input function gets called.

32
00:02:28,310 --> 00:02:31,710
It says, please enter a number between zero and 10 if we pass in.

33
00:02:31,880 --> 00:02:35,150
For instance, let's say nine hit enter.

34
00:02:35,300 --> 00:02:38,300
It returns back the integer nine, not the string nine.

35
00:02:38,870 --> 00:02:41,180
Now, there's two problems of this right now.

36
00:02:41,780 --> 00:02:44,660
One is that it's going to run user choice again.

37
00:02:45,170 --> 00:02:48,840
I could technically try to again, maybe provide a string.

38
00:02:48,920 --> 00:02:51,120
Nine, I hit enter and I get.

39
00:02:51,170 --> 00:02:53,900
Hey, we give an invalid litoral for integer.

40
00:02:54,440 --> 00:02:59,970
The other problem with this is I'm specifically asking for the numbers, maybe zero through 10.

41
00:03:00,410 --> 00:03:05,080
And I'm not doing any sort of check to make sure that number is actually between zero and 10.

42
00:03:05,690 --> 00:03:12,170
So if I were to run this again, let's say I enter something like one hundred, I enter the function

43
00:03:12,170 --> 00:03:15,890
still works because it's able to convert one hundred into an integer.

44
00:03:16,100 --> 00:03:18,560
But I'm outside the limits that I wanted.

45
00:03:18,740 --> 00:03:20,840
So 100 is outside limit of zero and 10.

46
00:03:21,350 --> 00:03:27,260
So as a couple of things you have to do, one is to make sure that the data type is valid in order to

47
00:03:27,260 --> 00:03:33,170
run this integer conversion and to check against the actual inputs, I expect.

48
00:03:33,470 --> 00:03:38,870
So I can check against just a few inputs or I can check within a list of inputs.

49
00:03:39,320 --> 00:03:43,490
And what I want to also show you is in the actual notebook that corresponds with this lecture.

50
00:03:43,970 --> 00:03:47,630
This warm up project, exercise notebook that I have opened here.

51
00:03:48,080 --> 00:03:51,290
I've added some links for Stack Overflow.

52
00:03:51,710 --> 00:03:55,910
So if you just do a Google search for validating user input.

53
00:03:56,030 --> 00:03:58,910
So, for example, here's a Google search for Python.

54
00:03:58,940 --> 00:04:00,230
Check if input is a number.

55
00:04:00,500 --> 00:04:02,870
There's lots of different ways you can do this.

56
00:04:02,960 --> 00:04:05,360
And I want you to explore these various methods.

57
00:04:05,660 --> 00:04:11,680
And I added a couple links here for specific stack overflow posts that answer this sort of question.

58
00:04:11,690 --> 00:04:17,270
Basically, how do I check if a specific python input is a data type?

59
00:04:17,570 --> 00:04:21,090
And later on, we're going to be learning about things like try and accept statements.

60
00:04:21,440 --> 00:04:23,810
But for these sort of simple checks, we actually don't need them.

61
00:04:24,200 --> 00:04:26,210
And there's lots of different ways you can do this.

62
00:04:26,450 --> 00:04:29,080
Here, there's a more advanced method such as try except.

63
00:04:29,390 --> 00:04:33,440
But there's also the is digit which returns true or false.

64
00:04:33,500 --> 00:04:37,250
A boolean, depending if this can be converted into a digit.

65
00:04:37,820 --> 00:04:39,500
So there's lots of different methods here.

66
00:04:39,770 --> 00:04:40,790
So here's another while.

67
00:04:40,790 --> 00:04:45,790
True with try, except here's one that's basically a function, et cetera.

68
00:04:45,950 --> 00:04:46,850
And there's another stack.

69
00:04:46,850 --> 00:04:47,690
Overflow posts.

70
00:04:48,020 --> 00:04:51,050
That again, checks if something represents an integer.

71
00:04:51,380 --> 00:04:53,080
And this one's without using, try and accept.

72
00:04:53,170 --> 00:04:54,290
So you scroll down here.

73
00:04:54,950 --> 00:04:55,940
Lots of different ways.

74
00:04:56,030 --> 00:04:58,630
That is digit ways, starts with, et cetera.

75
00:04:59,030 --> 00:05:05,030
So there's many different ways you can actually solve this question of checking for the correct data

76
00:05:05,030 --> 00:05:05,840
type input.

77
00:05:06,500 --> 00:05:10,770
The favorite method here that I found in my own research is that is digit.

78
00:05:10,790 --> 00:05:11,240
Check.

79
00:05:11,360 --> 00:05:12,330
So this is digit.

80
00:05:12,560 --> 00:05:13,910
It's probably the simplest way to do this.

81
00:05:14,210 --> 00:05:15,620
So that's what we'll go ahead and do.

82
00:05:16,130 --> 00:05:21,080
Keep in mind, it sometimes has errors for negative numbers, but for right now, we'll go ahead and

83
00:05:21,080 --> 00:05:23,420
ignore that test case.

84
00:05:23,720 --> 00:05:27,770
So we'll assume that the user is going to pass in a positive number.

85
00:05:28,160 --> 00:05:33,200
Keep in mind, always be careful with assumptions of what a user is going to do because users can often

86
00:05:33,200 --> 00:05:34,070
break your program.

87
00:05:34,310 --> 00:05:35,840
If you're not checking for edge cases.

88
00:05:36,140 --> 00:05:37,400
But since we're keeping things simple.

89
00:05:37,460 --> 00:05:38,180
This is digit.

90
00:05:38,240 --> 00:05:39,110
Should be enough for us.

91
00:05:39,590 --> 00:05:45,530
So we'll come back here and warm up project exercises and we'll continue with validating user input.

92
00:05:46,830 --> 00:05:47,010
All right.

93
00:05:47,520 --> 00:05:51,180
So we already created this function user choice, and I have two problems with it.

94
00:05:51,450 --> 00:05:56,370
One is a user can pass on something that isn't a digit and two is a user.

95
00:05:56,370 --> 00:05:59,640
Kompass and something that's outside of the range I want.

96
00:06:00,000 --> 00:06:04,830
But luckily, I have this choice variable that I can then do checks against.

97
00:06:04,950 --> 00:06:08,850
So let's expand this user choice function.

98
00:06:09,420 --> 00:06:12,600
And based off that research from Google and Stack Overflow.

99
00:06:13,050 --> 00:06:18,540
I'm going to check with the IS digit method that's actually available on a string.

100
00:06:19,050 --> 00:06:25,500
So to show you what I mean by that is let's say some value is equal to the string.

101
00:06:25,500 --> 00:06:26,170
One hundred.

102
00:06:27,330 --> 00:06:28,440
So I run that cell.

103
00:06:28,680 --> 00:06:33,630
And in a new cell, I'll say some value dot hit tab in that new cell.

104
00:06:34,080 --> 00:06:39,480
And eventually you'll scroll down and you'll see that there's this convenience method of is alphanumeric

105
00:06:39,600 --> 00:06:43,530
is alpha, is decimal, is digit, et cetera, is space is upper.

106
00:06:43,890 --> 00:06:46,050
And really what I'm looking for is that is digit.

107
00:06:46,890 --> 00:06:50,280
So I'm going to say some value is digit.

108
00:06:50,970 --> 00:06:53,370
And I would never expect someone to memorize all these methods.

109
00:06:53,400 --> 00:06:57,840
Instead, you would do a Google search and stack overflow post search looking for the correct method.

110
00:06:58,330 --> 00:07:01,140
And if you do shift tab here, basically going to return.

111
00:07:01,140 --> 00:07:01,440
True.

112
00:07:01,440 --> 00:07:05,790
If all characters in this string are digits, which is pretty much exactly what I want.

113
00:07:06,360 --> 00:07:12,270
So opening close parentheses, since this is a method call, we run this and it returns.

114
00:07:12,300 --> 00:07:14,790
True, since each of these is a digit.

115
00:07:15,210 --> 00:07:20,640
Which means this can actually be passed into this conversion of an integer.

116
00:07:20,670 --> 00:07:23,220
So I can say integer some value.

117
00:07:23,490 --> 00:07:26,280
I run this and I get back the integer of 100.

118
00:07:26,940 --> 00:07:27,240
Perfect.

119
00:07:27,270 --> 00:07:28,500
That's exactly what I want here.

120
00:07:28,800 --> 00:07:32,010
Since we're looking for basically a whole integer number.

121
00:07:32,700 --> 00:07:39,030
So what I need to do is basically have a check to make sure that this choice is a digit.

122
00:07:39,660 --> 00:07:44,550
So how can we do this to basically keep asking the question of the input?

123
00:07:45,530 --> 00:07:48,080
Well, we need to do first is start off with.

124
00:07:49,370 --> 00:07:50,840
A default choice value.

125
00:07:51,350 --> 00:07:56,180
So what I'm going to do is say my original choice value is something that is not an integer.

126
00:07:56,720 --> 00:07:58,850
So let's just start with wrong.

127
00:07:59,180 --> 00:08:04,040
And technically, this can be anything, any string you want, any variable you want, as long as it's

128
00:08:04,040 --> 00:08:06,170
not going to stay true.

129
00:08:06,170 --> 00:08:07,010
Four is digit.

130
00:08:07,550 --> 00:08:10,340
So obviously, the word wrong is not a digit.

131
00:08:10,460 --> 00:08:12,080
So we start with the wrong choice.

132
00:08:12,560 --> 00:08:15,440
And then I'm going to add a little bit of logic here a while loop.

133
00:08:15,620 --> 00:08:20,060
Remember, a while loop basically continues until a condition is met.

134
00:08:20,420 --> 00:08:21,800
So I'm going to say while.

135
00:08:23,190 --> 00:08:25,380
Choice dot is digit.

136
00:08:28,910 --> 00:08:31,570
Is equal to false meaning.

137
00:08:32,060 --> 00:08:34,430
Currently, our choice is not a digit.

138
00:08:34,790 --> 00:08:38,570
So while the choice is not a digit recall, we start off with choice not being a digit.

139
00:08:39,070 --> 00:08:45,530
Then I'm going to keep asking for this input, which means I need to indent this line in order for it

140
00:08:45,530 --> 00:08:47,780
to be part of this while loop.

141
00:08:48,320 --> 00:08:50,300
And this is where we're basically being a little clever here.

142
00:08:50,440 --> 00:08:55,670
What we're saying is we start off with this string and since it starts off not being a digit, we're

143
00:08:55,670 --> 00:09:01,280
going to keep asking the user for input over and over again in this while loop until we finally get

144
00:09:01,370 --> 00:09:02,750
is digit to be true.

145
00:09:03,350 --> 00:09:08,660
And since it's while Loop will continue to run until we have a digit, then I no longer have to worry

146
00:09:08,900 --> 00:09:14,030
about this conversion, giving me a value error for something that isn't a digit because it's while

147
00:09:14,030 --> 00:09:15,290
Loop takes care of that.

148
00:09:15,560 --> 00:09:17,090
With this is digit call.

149
00:09:17,930 --> 00:09:20,800
So let's go ahead and delete these cells below.

150
00:09:22,060 --> 00:09:23,770
We'll read the fine user choice.

151
00:09:25,900 --> 00:09:27,320
And let's see it run now.

152
00:09:28,360 --> 00:09:30,730
So it says, please enter a number between zero and 10.

153
00:09:31,070 --> 00:09:37,990
Let's go ahead and pass on something like to hit, enter and notice because the string two is not a

154
00:09:37,990 --> 00:09:38,320
digit.

155
00:09:38,380 --> 00:09:40,480
So that means is digit is equal to false.

156
00:09:40,810 --> 00:09:44,400
We simply asked the user again, hey, please enter number between zero and 10.

157
00:09:44,920 --> 00:09:46,990
And I can type in whatever I want here.

158
00:09:47,260 --> 00:09:54,730
But until I pass in that actual digit such as three, it's going to keep running and asking asking over

159
00:09:54,730 --> 00:09:55,060
again.

160
00:09:55,420 --> 00:10:01,720
So this is a really nice way to basically make sure that your user is inputting something that you expect.

161
00:10:02,140 --> 00:10:07,210
And as you can imagine, that we can basically keep adding in conditions for this while loop with and

162
00:10:07,210 --> 00:10:08,140
or Oracle's.

163
00:10:09,840 --> 00:10:15,060
Additionally, something we can do is add in an error message just so the user knows what happened.

164
00:10:15,450 --> 00:10:20,290
For example, after we get that choice, let's go ahead and just have another a little check.

165
00:10:20,310 --> 00:10:22,980
We'll say if choice is digit.

166
00:10:24,190 --> 00:10:25,390
Is equal to false.

167
00:10:25,780 --> 00:10:29,320
Then I'll just print out a little error message for the user saying sorry.

168
00:10:29,350 --> 00:10:30,700
That is not a digit.

169
00:10:32,220 --> 00:10:33,360
So we rerun this again.

170
00:10:34,200 --> 00:10:40,920
Notice that this if statement is within the while loop and it's in line with this choice, basically

171
00:10:41,220 --> 00:10:45,120
after the user gets a choice, we do a little quick check to see if it's false.

172
00:10:45,510 --> 00:10:49,980
And if so, we'll go ahead and print warning the user that, hey, sorry, which you entered was not

173
00:10:49,980 --> 00:10:50,370
a digit.

174
00:10:50,880 --> 00:10:52,140
So essentially the logic is the same.

175
00:10:52,170 --> 00:10:55,890
But we added this little message just for the user's sake.

176
00:10:56,670 --> 00:11:03,750
So we'll say user choice passan to enter and it says, hey, sorry, that is not a digit and we can

177
00:11:03,750 --> 00:11:05,360
keep doing this over and over again.

178
00:11:05,370 --> 00:11:09,630
It's gonna keep saying sorry, that is not a digit until we have something that is a digit.

179
00:11:11,610 --> 00:11:16,350
And now the last thing I want to show you to finish up this lecture is checking against multiple possible

180
00:11:16,350 --> 00:11:16,920
values.

181
00:11:17,350 --> 00:11:20,850
So remember, we had this issue of this zero dash 10.

182
00:11:21,300 --> 00:11:25,110
So basically, I want to make sure that the number is within a certain range.

183
00:11:25,530 --> 00:11:27,510
And there's lots of different ways you can do this.

184
00:11:27,810 --> 00:11:32,760
But probably easiest way is to check against a list of acceptable values.

185
00:11:33,210 --> 00:11:41,220
For example, let's say my result is just the string wrong value and I'm going to create a list of acceptable

186
00:11:41,220 --> 00:11:41,700
values.

187
00:11:42,180 --> 00:11:49,590
So acceptable values and let's say my acceptable values are zero, one and two, then I can use the

188
00:11:49,680 --> 00:11:55,340
in operator recall the in operator was discussed and useful operators lecture and it basically checks

189
00:11:55,340 --> 00:12:00,180
if something is in either a string or a list or any sort of iterable object.

190
00:12:00,600 --> 00:12:05,100
So I can say is my result in acceptable values.

191
00:12:06,120 --> 00:12:07,500
I run that and it says false.

192
00:12:07,710 --> 00:12:10,560
This string wrong value is not within this list.

193
00:12:11,190 --> 00:12:13,140
I can also check for not in.

194
00:12:13,760 --> 00:12:18,090
So saying is my result not in acceptable values.

195
00:12:18,990 --> 00:12:19,370
Run that.

196
00:12:19,410 --> 00:12:24,500
And that happens to be true because wrong value here is not within zero one N2.

197
00:12:25,170 --> 00:12:30,150
So that means you can add this sort of condition of make sure that the result is an acceptable values

198
00:12:30,630 --> 00:12:37,050
along with a condition that it's a digit or you can have multiple checks later on in order to keep asking

199
00:12:37,380 --> 00:12:40,110
in case the user is putting in the wrong value.

200
00:12:40,590 --> 00:12:45,960
So in order to do that, we're going to reach a little bit higher level of complexity than we did before.

201
00:12:46,350 --> 00:12:48,780
But if you follow the logic, it's actually not too bad.

202
00:12:49,150 --> 00:12:55,110
What I'm going to do is scroll back up here and grab this user choice function again.

203
00:12:56,750 --> 00:12:58,010
And then paste it down here.

204
00:12:58,640 --> 00:13:03,710
And what I need to do is start commenting this just so we can't keep track where logic flow.

205
00:13:04,190 --> 00:13:06,290
First off, there's a couple of variables that we need.

206
00:13:07,160 --> 00:13:08,780
One are the initial choice.

207
00:13:08,870 --> 00:13:11,360
So we have some initial values here.

208
00:13:12,630 --> 00:13:17,220
And one is for the choice, which cannot be a digit in order to make sure this loop runs.

209
00:13:17,640 --> 00:13:21,720
The other one I want to make sure is if we have some sort of acceptable range.

210
00:13:22,230 --> 00:13:23,710
Let's go ahead and say that here.

211
00:13:23,820 --> 00:13:24,960
Acceptable range.

212
00:13:25,740 --> 00:13:32,040
And you can either manually do this, something like 012 or you can use something like range.

213
00:13:32,910 --> 00:13:34,690
And since our ranges zero to 10.

214
00:13:34,800 --> 00:13:36,620
Let's go ahead and say zero comma 10.

215
00:13:37,260 --> 00:13:40,800
And if you're unfamiliar with the range function, go ahead and check out useful operators.

216
00:13:41,040 --> 00:13:46,110
Basically, it just makes an iterable that has the numbers zero through 10 with a default step size

217
00:13:46,200 --> 00:13:46,980
of one integer.

218
00:13:47,760 --> 00:13:54,270
And finally, we need to have just like choice, an understanding of whether or not we are within range.

219
00:13:54,780 --> 00:13:59,850
So I'm going to say within range is equal to false.

220
00:14:00,850 --> 00:14:07,150
OK, so next step is to actually have the conditions we're checking for and this case, it's going to

221
00:14:07,150 --> 00:14:09,220
be two conditions to check.

222
00:14:09,820 --> 00:14:16,210
I want to make sure that I keep asking in this while loop when my choice is a digit.

223
00:14:16,360 --> 00:14:18,190
So I need to check the digit.

224
00:14:19,790 --> 00:14:24,050
Or I keep asking this, if my within range.

225
00:14:26,040 --> 00:14:26,640
Is false.

226
00:14:28,600 --> 00:14:32,140
So basically, while the choice is not a digit.

227
00:14:33,720 --> 00:14:36,960
Or when I'm not within range, so within range.

228
00:14:38,280 --> 00:14:39,360
Equal to false.

229
00:14:39,990 --> 00:14:46,650
Then I'm going to ask for this input, and then if I happen to be a digit equal to false sorry, that

230
00:14:46,650 --> 00:14:47,340
is not a digit.

231
00:14:47,970 --> 00:14:52,290
And now it's time to use this within range parameter in order to update it to.

232
00:14:52,290 --> 00:14:52,800
True.

233
00:14:52,920 --> 00:14:54,810
In case I do happen to be within range.

234
00:14:55,290 --> 00:14:56,910
So this is our.

235
00:14:58,440 --> 00:14:59,310
Digit check.

236
00:15:00,630 --> 00:15:03,930
And now let's add in a range.

237
00:15:03,960 --> 00:15:04,410
Check.

238
00:15:05,870 --> 00:15:06,770
So how do we do this?

239
00:15:06,860 --> 00:15:11,510
First, we confirm that we actually have a digit first, if not, we just keep looping.

240
00:15:11,540 --> 00:15:17,390
So we assume that by the time we're getting to the range check that it is digit happens to be true.

241
00:15:17,870 --> 00:15:20,210
So nobody is going to say if the choice now.

242
00:15:22,060 --> 00:15:22,570
Is Zigic.

243
00:15:22,650 --> 00:15:24,250
Let's make sure we spell choice correctly.

244
00:15:24,700 --> 00:15:27,730
So if the choice stop is digit happens to be true.

245
00:15:30,230 --> 00:15:32,540
Then let's check if we are within range.

246
00:15:33,210 --> 00:15:35,690
So because now choice is digit is true.

247
00:15:35,990 --> 00:15:42,530
I know I can cast that string choice as an integer and then check if happens to be within my acceptable

248
00:15:42,530 --> 00:15:45,620
values or acceptable range.

249
00:15:46,310 --> 00:15:52,610
And if that happens to be true, then I know I'm within range so I can update within range to be true.

250
00:15:53,390 --> 00:15:57,770
And keep in mind that the double equal science is checking for equality.

251
00:15:58,250 --> 00:16:00,470
A single equal signs is an actual assignment.

252
00:16:01,940 --> 00:16:07,170
However, if that happens to not be the case, meaning I'm outside the acceptable range, then I'm going

253
00:16:07,170 --> 00:16:12,440
to say else I'm still within range is equal to false.

254
00:16:13,160 --> 00:16:17,150
Keep in mind, there's a lot of checks here that you technically don't have to do.

255
00:16:17,510 --> 00:16:20,330
For example, choice is digit equals to true.

256
00:16:21,050 --> 00:16:22,850
This is actually a boolean itself.

257
00:16:23,030 --> 00:16:27,890
So I could simplify things by actually just saying if choice dot is digit.

258
00:16:28,400 --> 00:16:33,890
But for the sake of readability, I'll go ahead and keep a lot of these checks in place and also do

259
00:16:33,890 --> 00:16:36,440
things like keep assigning something as false.

260
00:16:36,680 --> 00:16:38,540
Even though technically is already equal to false.

261
00:16:38,840 --> 00:16:40,340
So this could have just been a pass.

262
00:16:41,340 --> 00:16:45,840
All right, so let's quickly go over the logic here that we added in, we have our initial values,

263
00:16:45,990 --> 00:16:47,460
the choice starts off as wrong.

264
00:16:47,790 --> 00:16:49,440
I have some acceptable range of values.

265
00:16:49,770 --> 00:16:53,160
And I'm also going to say that to start off, we are not within that range.

266
00:16:53,640 --> 00:16:56,160
So that means there's two things I need to check for.

267
00:16:56,550 --> 00:16:58,860
So either of these if they're false.

268
00:16:59,100 --> 00:17:04,410
So if either the digit is false or within range is false, then I keep asking the user for input over

269
00:17:04,410 --> 00:17:05,040
and over again.

270
00:17:05,580 --> 00:17:07,590
So we first check if happens to be a digit.

271
00:17:08,100 --> 00:17:11,700
And we basically warn them, hey, this is a digit sorry, it's on a digit.

272
00:17:12,150 --> 00:17:14,490
And then we check if that does happen to be a digit.

273
00:17:14,880 --> 00:17:20,070
They will say if the integer choice is an acceptable range within range equal to true else.

274
00:17:20,460 --> 00:17:21,480
Let's go ahead and say.

275
00:17:21,530 --> 00:17:23,510
Else within range, equal to false.

276
00:17:23,890 --> 00:17:24,930
But it's also warned these are.

277
00:17:24,930 --> 00:17:26,610
Hey, sorry.

278
00:17:26,910 --> 00:17:28,740
You are out of.

279
00:17:29,680 --> 00:17:32,350
Acceptable range, zero to 10.

280
00:17:33,280 --> 00:17:38,560
And now we should be able to inform the user whether or not they're it with a digit or if they're out

281
00:17:38,560 --> 00:17:39,490
of acceptable range.

282
00:17:40,030 --> 00:17:42,490
So there's a lot of lines and a code going on here.

283
00:17:42,760 --> 00:17:44,410
But really, take your time.

284
00:17:44,680 --> 00:17:47,430
Review the notebook and the logic should make sense to you.

285
00:17:47,620 --> 00:17:49,930
Essentially, just checking for two things here.

286
00:17:51,440 --> 00:17:53,750
Go ahead and read this and I'll run user choice.

287
00:17:55,370 --> 00:17:57,930
So I going to do a couple of wrong inputs here.

288
00:17:58,260 --> 00:18:00,210
First, let's try and input that is not a digit.

289
00:18:00,540 --> 00:18:01,200
We'll say two.

290
00:18:01,730 --> 00:18:03,090
It says sorry, it's on a digit.

291
00:18:03,270 --> 00:18:03,690
Perfect.

292
00:18:03,990 --> 00:18:08,220
And Allus try something that is out of range, like one hundred and that says, sorry, you're out of

293
00:18:08,220 --> 00:18:09,120
acceptable range.

294
00:18:09,210 --> 00:18:12,360
Now let's try something like five and we're good to go.

295
00:18:12,910 --> 00:18:14,010
OK, awesome.

296
00:18:14,370 --> 00:18:20,820
So now we understand how to check if something is the correct data type and to check if something is

297
00:18:20,820 --> 00:18:22,140
an acceptable value.

298
00:18:22,530 --> 00:18:27,090
Which are the two main things are going out there checking when creating interactive Python script.

299
00:18:27,780 --> 00:18:29,830
Coming up next, we're going to bring this all together.

300
00:18:30,120 --> 00:18:35,250
Just a couple of functions in order to show you how to create a simple user interactive experience.

301
00:18:35,370 --> 00:18:37,920
And after that, we'll set you loose on the milestone project.

302
00:18:38,310 --> 00:18:39,390
I'll see you at the next lecture.
