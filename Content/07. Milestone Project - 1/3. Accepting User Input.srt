1
00:00:05,520 --> 00:00:06,410
Welcome back, everyone.

2
00:00:06,690 --> 00:00:10,590
In this lecture, we're going to talk about accepting user input.

3
00:00:11,130 --> 00:00:13,320
Let's head over to the Jupiter notebook and get started.

4
00:00:14,010 --> 00:00:14,280
All right.

5
00:00:14,310 --> 00:00:19,530
Now, previously, we talked about just displaying information through a display function, which is

6
00:00:19,530 --> 00:00:23,340
essentially just a little placeholder for multiple print functions.

7
00:00:23,820 --> 00:00:29,700
Now, we want to do is accept user input and also want to show you just a couple of key things to be

8
00:00:29,700 --> 00:00:32,010
aware of when using the input function.

9
00:00:32,520 --> 00:00:36,570
Now, we've actually already learned about the input function and the useful operators lecture.

10
00:00:36,840 --> 00:00:38,970
But here we're going to show you a couple more things.

11
00:00:39,750 --> 00:00:43,380
So the input function is simply the key word input.

12
00:00:44,100 --> 00:00:45,810
And then open and close parentheses.

13
00:00:45,870 --> 00:00:49,470
And what you do is you type out the string you want the user to see.

14
00:00:49,950 --> 00:00:56,760
So, for example, we'll say, please enter a value and then we'll have a colon and a space.

15
00:00:57,150 --> 00:01:00,750
And once I run the cell, what's going to happen is we'll see.

16
00:01:00,780 --> 00:01:04,710
Please enter a value colon space and then a little input box.

17
00:01:04,860 --> 00:01:11,960
So, for example, I can say to then it just hit enter and we get back the string to.

18
00:01:12,870 --> 00:01:17,520
So what I can also do is instead of just displaying the output here.

19
00:01:18,530 --> 00:01:20,210
I'll save it as a result.

20
00:01:20,270 --> 00:01:22,260
So I'll say result is equal to input.

21
00:01:23,030 --> 00:01:28,430
Please enter a value column, space shift enter to run this.

22
00:01:29,000 --> 00:01:35,300
And when I say two, we actually don't see the output result because I've saved it as a variable, which

23
00:01:35,300 --> 00:01:37,170
means I can now call results.

24
00:01:38,220 --> 00:01:44,790
And I get back to now notice that the input function, whatever we actually pass into it, it's going

25
00:01:44,790 --> 00:01:47,250
to return a string.

26
00:01:47,760 --> 00:01:53,460
So if I check the type of this with the built in type function in Python, it's SDR, which is a string

27
00:01:53,550 --> 00:01:53,970
type.

28
00:01:54,750 --> 00:02:00,630
If I wanted this to actually be some other type, such as a floating point or an integer, what I would

29
00:02:00,630 --> 00:02:04,650
have to do is convert it into an integer or floating point.

30
00:02:04,830 --> 00:02:08,670
And I can do that with the built in casting functions, essentially.

31
00:02:08,760 --> 00:02:13,980
So I can say result is equal to and then input.

32
00:02:15,200 --> 00:02:16,250
Let's say enter value.

33
00:02:18,020 --> 00:02:19,940
We'll do shift enter to run this.

34
00:02:20,300 --> 00:02:21,310
I'm going to put in two.

35
00:02:22,160 --> 00:02:29,360
And then once I have the results, I can convert it to an integer with I n t open close prince's pass

36
00:02:29,540 --> 00:02:30,530
and result here.

37
00:02:31,190 --> 00:02:37,400
And then we'll say results underscore I.A. is equal to integer result.

38
00:02:38,330 --> 00:02:46,460
Run this and to the type of my original result is a string and the type of my result integer.

39
00:02:48,360 --> 00:02:49,100
Is an integer.

40
00:02:49,580 --> 00:02:56,930
So essentially this type in python is also a function that allows you to convert from one type to another.

41
00:02:57,560 --> 00:03:04,640
So if I were to check the type of two point three, for instance, it brings back float, which means

42
00:03:05,150 --> 00:03:08,120
float can actually convert to floating point numbers.

43
00:03:08,480 --> 00:03:12,460
So if I had a string, for instance, that was, let's say, three point one four.

44
00:03:13,220 --> 00:03:16,940
It actually converts it to the number or floating point number.

45
00:03:17,390 --> 00:03:20,330
So keep that in mind that the main thing here is input.

46
00:03:20,330 --> 00:03:25,140
Function is always going to return a string, which means you may need to convert it.

47
00:03:25,280 --> 00:03:28,460
If you actually need a specific data type within your function.

48
00:03:29,240 --> 00:03:37,430
So that's gonna be important because if we have something like position, index is equal to input of

49
00:03:37,940 --> 00:03:40,580
choose an index position.

50
00:03:41,590 --> 00:03:42,490
Colin space.

51
00:03:43,150 --> 00:03:43,690
Later on.

52
00:03:43,810 --> 00:03:46,780
I may want to use this position index as part of my list.

53
00:03:47,260 --> 00:03:49,870
So, for example, we'll go ahead and run this.

54
00:03:49,930 --> 00:03:52,330
And we'll say one enter.

55
00:03:52,920 --> 00:03:55,480
And let's imagine I still have those rose above.

56
00:03:55,540 --> 00:04:00,930
So if I scroll up here, recall, I have row one, row two or three, et cetera.

57
00:04:01,630 --> 00:04:02,920
So I have row one here.

58
00:04:03,250 --> 00:04:04,150
I do shift enter.

59
00:04:05,210 --> 00:04:12,950
Unfortunately, I can't just say position index pass, then here I'll get an error, and that's because

60
00:04:13,250 --> 00:04:18,110
the list index or list indices, they must be integers or slices, they can't be a string.

61
00:04:18,560 --> 00:04:21,680
So position index here is actually still a string.

62
00:04:21,860 --> 00:04:28,040
So really what's happening here is I'm trying to say row one and I'm passing in the string one, which

63
00:04:28,040 --> 00:04:33,400
is the error I actually to pass in an integer, as you've already seen in the list lecture.

64
00:04:33,620 --> 00:04:42,020
So what I can do here is I can either cast position index or convert it into an integer type using int

65
00:04:42,170 --> 00:04:45,830
result or into position index or to do this all in one line.

66
00:04:46,370 --> 00:04:51,920
I'm going to say I MDT and wrap the input function around it.

67
00:04:52,400 --> 00:04:58,820
So essentially what this does is this grabs your input as a string and then converts it into an integer

68
00:04:59,090 --> 00:05:02,090
before assigning it to the variable position index.

69
00:05:02,510 --> 00:05:04,520
Let's go ahead and run these cells.

70
00:05:04,850 --> 00:05:06,560
Going to delete these real quick.

71
00:05:07,520 --> 00:05:10,110
So I'm going to shift enter here, run this.

72
00:05:10,130 --> 00:05:11,720
It says choose an indexed position.

73
00:05:12,110 --> 00:05:14,780
Let's try again with two hit enter.

74
00:05:15,170 --> 00:05:17,450
And now what's nice about this is the type.

75
00:05:18,460 --> 00:05:25,170
Of my position, index is an integer, which means if I take back, for example, one of those rows,

76
00:05:25,710 --> 00:05:26,670
let's say row two.

77
00:05:27,540 --> 00:05:29,730
I can now Passan Position Index.

78
00:05:30,090 --> 00:05:31,260
And it works just fine.

79
00:05:31,800 --> 00:05:36,420
So this is the sort of thing you have to be aware of when using user input.

80
00:05:36,690 --> 00:05:40,170
Is that Python will automatically pass it in as a string.

81
00:05:40,500 --> 00:05:45,090
And in order to use it in your program, you may actually need to convert it into a different data types

82
00:05:45,090 --> 00:05:47,370
such as an integer or a floating point number.

83
00:05:48,770 --> 00:05:52,040
Now, there's two more things I want to tell you before we conclude this lecture.

84
00:05:52,520 --> 00:05:58,970
The first one is if you're using some sort of conversion, such as trying to convert an input into an

85
00:05:58,970 --> 00:06:04,310
integer, it has to be a legitimate input, which essentially means it has to be something that looks

86
00:06:04,310 --> 00:06:08,360
like a normal integer, but just happens to be wrapped in quotes as a string.

87
00:06:09,230 --> 00:06:16,670
So let's imagine that it says choose an indexed position and the player types out to like this.

88
00:06:17,000 --> 00:06:23,270
Well, Python is actually smart enough to be able to convert this t w o into an integer directly.

89
00:06:23,270 --> 00:06:28,970
So when a hit enter here actually get this value error which says, hey, invalid Litoral for integer

90
00:06:28,970 --> 00:06:36,710
with base 10 to essentially saying I don't know how to convert this string to into some sort of integer.

91
00:06:37,100 --> 00:06:38,390
So you have to be careful of that.

92
00:06:38,840 --> 00:06:42,980
And what we're going to do later on is actually show you how you can validate user input.

93
00:06:43,490 --> 00:06:50,600
So we'll be able to actually check to make sure that this is valid before attempting to convert it into

94
00:06:50,600 --> 00:06:51,170
an integer.

95
00:06:51,620 --> 00:06:53,420
So we'll learn about that in the next lecture.

96
00:06:53,750 --> 00:06:59,540
And the last thing I want to mention is the input function itself can be a little tricky because it

97
00:06:59,630 --> 00:07:07,040
awaits a user interaction, which means if you accidentally run it twice, your Python program or script

98
00:07:07,370 --> 00:07:10,760
may get stuck waiting for a reply that doesn't actually come.

99
00:07:11,210 --> 00:07:12,650
So I want to show you what that looks like.

100
00:07:13,490 --> 00:07:16,880
Let's say we have a result here is equal to input.

101
00:07:18,040 --> 00:07:18,880
Enter a number.

102
00:07:19,560 --> 00:07:21,960
Call in space and I do shift enter.

103
00:07:22,420 --> 00:07:24,790
And right now, notice that there's an Asterix there.

104
00:07:25,090 --> 00:07:27,310
And it says, hey, I'm waiting for a result.

105
00:07:27,340 --> 00:07:28,420
Please enter a number.

106
00:07:28,960 --> 00:07:34,840
If I absolutely forgot that I ran this cell and I kept going down my notebook and try to run another

107
00:07:34,840 --> 00:07:36,640
cell such as two plus two.

108
00:07:37,060 --> 00:07:42,430
This cell isn't going to run until you actually get a result here for enter number.

109
00:07:42,640 --> 00:07:46,210
So this is essentially waiting for the user interaction from the input.

110
00:07:46,810 --> 00:07:52,180
So it's only until I provide a number here and hit enter that the rest of the cells actually execute.

111
00:07:52,720 --> 00:07:57,460
And the other thing to remember, and this is the one that's really tricky with input, is sometimes

112
00:07:57,460 --> 00:07:59,530
people accidentally run the cell twice.

113
00:07:59,950 --> 00:08:01,900
Which means we'll say something like input.

114
00:08:04,180 --> 00:08:06,280
Enter no coal in space.

115
00:08:06,580 --> 00:08:13,480
I do shift, enter and then maybe accidentally click on this cell again and I'm going to hit shift enter

116
00:08:13,510 --> 00:08:14,170
again here.

117
00:08:15,570 --> 00:08:22,380
And now we're kind of in trouble because I accidentally try to overwrite that input cell.

118
00:08:22,590 --> 00:08:27,390
But that initial input is still waiting for its interaction, its value.

119
00:08:27,810 --> 00:08:33,720
Which means essentially now we're in, for lack of a better word, screwed because we're still stuck

120
00:08:33,720 --> 00:08:34,980
waiting on that original input.

121
00:08:35,400 --> 00:08:39,870
And Jupiter note, because he raised that first box, because I tried to run the cell again, which

122
00:08:39,870 --> 00:08:41,310
now means any other cell.

123
00:08:41,570 --> 00:08:48,690
If I try to say, for instance, 20 plus 20, everything now is stuck waiting on the input that I no

124
00:08:48,690 --> 00:08:49,890
longer have access to.

125
00:08:50,370 --> 00:08:57,030
And unfortunately, if this happens to you, the only way to fix this is to go to kernel and then restart

126
00:08:57,030 --> 00:08:57,630
your kernel.

127
00:08:58,630 --> 00:09:03,370
You may be able to try to interrupt it, but typically the interruption doesn't actually work.

128
00:09:03,400 --> 00:09:06,310
So the best way to fix that is to say again, Colonel.

129
00:09:06,730 --> 00:09:11,050
And then restart and notice here, we kind of saw the reloading of that restart.

130
00:09:11,470 --> 00:09:16,270
So now that it's been restarted, I can actually run any of these cells again.

131
00:09:16,300 --> 00:09:20,500
But the thing to keep in mind is all the previous variables in memory have been erased.

132
00:09:20,590 --> 00:09:22,900
And we're starting fresh from input one.

133
00:09:23,380 --> 00:09:29,230
So now, if I were to run this two plus two sulien notice, it's now the second cell and all the variables

134
00:09:29,260 --> 00:09:32,440
are defined above here, such as results and row.

135
00:09:32,710 --> 00:09:34,770
They're no longer available to me.

136
00:09:35,380 --> 00:09:41,050
They'll say, hey, Roe is not the fine, because in order to redefine everything after you've restarted

137
00:09:41,050 --> 00:09:43,660
your colonel is you have to run all these cells again.

138
00:09:43,720 --> 00:09:48,030
You have to say print this print that redefined these cells, et cetera.

139
00:09:48,190 --> 00:09:49,510
You have to keep running all these cells.

140
00:09:49,870 --> 00:09:55,330
A quick way to do that, though, is you can always say cell run all run all above or run all below.

141
00:09:55,420 --> 00:09:57,280
So that's kind of a quick little tooltip there.

142
00:09:57,880 --> 00:09:58,090
OK.

143
00:09:58,390 --> 00:10:04,480
So often a common mistake for beginners is to accidentally run an input cell, function twice, and

144
00:10:04,480 --> 00:10:05,440
then you're kind of stuck.

145
00:10:05,560 --> 00:10:08,620
So if that happens to you, go ahead and just hit kernel restart.

146
00:10:09,460 --> 00:10:09,730
All right.

147
00:10:10,180 --> 00:10:13,720
So you mentioned validating user input is going to be important.

148
00:10:14,020 --> 00:10:16,690
So let's go and explore how we can do that in the next lecture.

149
00:10:17,020 --> 00:10:17,560
I'll see you there.
