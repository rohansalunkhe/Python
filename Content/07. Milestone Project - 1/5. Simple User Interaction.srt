1
00:00:05,340 --> 00:00:08,850
Welcome back, everyone, to finalize the series of lectures.

2
00:00:08,910 --> 00:00:14,160
We're going to build a very simple game, which is essentially just an example of a simple user interaction.

3
00:00:15,240 --> 00:00:16,680
So putting everything together.

4
00:00:17,010 --> 00:00:21,900
And so what this program is going to do, it's going to display a list consisting of just three items.

5
00:00:22,290 --> 00:00:26,700
Then we're gonna have the user choose an index position and an input value.

6
00:00:27,090 --> 00:00:32,340
So we're gonna have to make sure that that index position is both within the range of zero one and two.

7
00:00:32,700 --> 00:00:34,170
And that happens to be a digit.

8
00:00:34,860 --> 00:00:40,920
Then once they've chosen a valid index position, we'll also have them choose just a string to input

9
00:00:41,010 --> 00:00:41,940
into that position.

10
00:00:42,450 --> 00:00:47,850
Then we're going to replace the value of that list with the user's chosen input value.

11
00:00:48,270 --> 00:00:53,010
And we'll also check to see if the user wants to exit out of this game at any time.

12
00:00:53,430 --> 00:00:55,530
So we already have all the pieces needed.

13
00:00:55,830 --> 00:01:00,420
We just need to essentially string together the functions that we've already created in order to create

14
00:01:00,420 --> 00:01:02,310
an interactive user experience.

15
00:01:02,760 --> 00:01:06,270
So let's show you what the actual finished game looks like.

16
00:01:06,420 --> 00:01:09,720
And then we'll construct it ourselves using the functions we've already made.

17
00:01:10,080 --> 00:01:10,260
All right.

18
00:01:10,290 --> 00:01:11,760
Here I am at the Jupiter notebook.

19
00:01:12,060 --> 00:01:17,100
I've gone ahead and just copied and pasted some things from the Electra notebook, which are the functions

20
00:01:17,100 --> 00:01:19,920
we're gonna be creating and coding along with in this lecture.

21
00:01:20,220 --> 00:01:27,240
We have one for displaying the actual game, one for the position choice that the user chooses, a second

22
00:01:27,240 --> 00:01:28,590
one for the replacement choice.

23
00:01:28,650 --> 00:01:31,980
Essentially asking the user what they want to replace the current item with.

24
00:01:32,370 --> 00:01:37,710
And then a game on choice, which is really similar to the exclusion choice function, except checking

25
00:01:37,710 --> 00:01:39,390
to see if the user wants to stop playing.

26
00:01:40,170 --> 00:01:44,970
And then we have a little bit of game logic here dependent on whether or not the user wants to keep

27
00:01:44,970 --> 00:01:45,240
playing.

28
00:01:45,770 --> 00:01:46,000
OK.

29
00:01:46,470 --> 00:01:47,760
So let's go ahead and run this.

30
00:01:47,790 --> 00:01:52,410
So we can see what the functions all look like together and then we'll code it out ourselves.

31
00:01:53,010 --> 00:01:53,230
All right.

32
00:01:53,700 --> 00:01:55,460
So I run this last cell.

33
00:01:55,650 --> 00:01:59,580
And it says pick a position to replace zero one or two.

34
00:02:00,150 --> 00:02:02,440
First, let's try to pick something that's wrong, like 10.

35
00:02:03,300 --> 00:02:06,030
And it says, hey, sorry, we did not choose a valid position.

36
00:02:06,470 --> 00:02:08,660
Got to be the same thing if I try to do something like two.

37
00:02:09,090 --> 00:02:10,770
Sorry, did you not choose Eveillard position?

38
00:02:11,130 --> 00:02:13,110
Now, let's actually choose a position.

39
00:02:13,170 --> 00:02:17,760
We'll say index one and then it's going to say, hey, type of string to place that position.

40
00:02:18,270 --> 00:02:19,570
So it's to say new.

41
00:02:20,580 --> 00:02:21,090
Run that.

42
00:02:21,550 --> 00:02:23,070
And now I can see the current list.

43
00:02:23,550 --> 00:02:25,350
Zero new and two.

44
00:02:25,800 --> 00:02:26,820
Would you like to keep playing.

45
00:02:26,850 --> 00:02:27,480
Yes or no.

46
00:02:27,960 --> 00:02:29,040
Go ahead and say yes.

47
00:02:30,070 --> 00:02:31,380
And here's a current list.

48
00:02:31,470 --> 00:02:34,080
Zero on you to choose a position to replace.

49
00:02:34,560 --> 00:02:35,820
Go ahead or replace one again.

50
00:02:36,970 --> 00:02:40,770
And now we're gonna replace it with something like brand new.

51
00:02:42,030 --> 00:02:43,080
Would you like to keep playing?

52
00:02:43,110 --> 00:02:43,680
Yes or no.

53
00:02:43,710 --> 00:02:44,640
So it says zero.

54
00:02:44,690 --> 00:02:45,540
Brand new two.

55
00:02:46,260 --> 00:02:47,820
I'm going to say no to keep playing.

56
00:02:48,240 --> 00:02:48,960
And there we have it.

57
00:02:49,440 --> 00:02:56,010
Very, very simple idea where I have this variable inside my Python script that the user can interact

58
00:02:56,010 --> 00:02:56,310
with.

59
00:02:56,400 --> 00:03:00,180
They can choose a position in that list and then replace it with their own value.

60
00:03:00,690 --> 00:03:02,940
So let's go ahead and build out this game.

61
00:03:03,330 --> 00:03:05,100
There's essentially a couple of things you have to do.

62
00:03:05,970 --> 00:03:09,060
One is to have a function that can display the game.

63
00:03:09,660 --> 00:03:12,960
Second is to have a function where the user chooses a position.

64
00:03:14,070 --> 00:03:16,530
Third is to have a replacement choice function.

65
00:03:16,650 --> 00:03:17,190
Very similar.

66
00:03:17,190 --> 00:03:19,860
We just asked the user what string they want to choose.

67
00:03:20,220 --> 00:03:24,780
And then the game on choice, very similar to the previous two, basically asking, do you want to keep

68
00:03:24,780 --> 00:03:25,140
playing?

69
00:03:25,800 --> 00:03:29,610
So let's go ahead and then build out the game logic.

70
00:03:30,160 --> 00:03:31,740
Gonna make a couple new cells here.

71
00:03:31,770 --> 00:03:33,030
So we don't see that old stuff.

72
00:03:33,540 --> 00:03:37,560
And let's start off by creating the display function.

73
00:03:37,980 --> 00:03:42,930
So just as we did at the beginning of the series of lectures, we're going to go ahead and display that

74
00:03:42,930 --> 00:03:43,650
game list.

75
00:03:44,050 --> 00:03:44,880
OK, I do this.

76
00:03:45,420 --> 00:03:51,810
Let's go ahead and make an example game list, which just starts off as zero one two.

77
00:03:52,620 --> 00:03:56,010
So my display function essentially is going to be a print statement.

78
00:03:56,460 --> 00:03:58,050
It's going to say display game.

79
00:03:58,950 --> 00:04:01,500
It takes in the game list variable.

80
00:04:02,190 --> 00:04:03,510
It's going to print.

81
00:04:04,290 --> 00:04:07,170
Here is the currents list.

82
00:04:08,700 --> 00:04:10,600
And then let's go ahead and print that.

83
00:04:11,920 --> 00:04:13,000
Game list variable.

84
00:04:14,020 --> 00:04:14,530
Run that.

85
00:04:14,890 --> 00:04:17,240
Let's make sure this works by saying display game.

86
00:04:18,380 --> 00:04:19,220
In the game list.

87
00:04:19,270 --> 00:04:20,620
And we see here's current list.

88
00:04:20,800 --> 00:04:21,350
012.

89
00:04:21,940 --> 00:04:22,570
Very simple.

90
00:04:22,960 --> 00:04:27,070
And as you can imagine, this could be expanded to any sort of board game that you can display.

91
00:04:27,100 --> 00:04:28,730
Could this play something like battleship?

92
00:04:28,750 --> 00:04:29,740
Board games, et cetera?

93
00:04:30,640 --> 00:04:30,830
OK.

94
00:04:31,270 --> 00:04:34,400
Now comes the time to get a position choice.

95
00:04:34,930 --> 00:04:39,100
And this is very similar to the functions that we've been creating throughout the series of lectures.

96
00:04:39,550 --> 00:04:41,410
We'll say position choice.

97
00:04:43,710 --> 00:04:47,580
We'll say the initial choice is invalid, so say the first choice is wrong.

98
00:04:48,650 --> 00:04:50,840
So how can we do this?

99
00:04:50,870 --> 00:04:57,830
Well, for this very simple game, I know that my choice has to be either zero one or two lots and lots

100
00:04:57,830 --> 00:04:59,870
of different ways you can do this if you want.

101
00:04:59,870 --> 00:05:02,030
You can follow along with the previous method shown.

102
00:05:02,300 --> 00:05:09,110
But I'm going to make this even simpler by saying while my choice is not in, because there's only three

103
00:05:09,110 --> 00:05:15,800
options here as to be either the string zero, the string one or the string two, because I no choice

104
00:05:15,860 --> 00:05:17,930
is past then as a string from the input.

105
00:05:19,160 --> 00:05:24,320
So I'm kind of simplifying things here, but you can make it more flexible using the functions we've

106
00:05:24,320 --> 00:05:25,130
previously shown.

107
00:05:26,550 --> 00:05:28,550
And I'm going to say my choice is simply the input.

108
00:05:29,240 --> 00:05:35,050
Pick a position zero one or two colon space.

109
00:05:36,810 --> 00:05:39,420
And let's go ahead and create a little message here.

110
00:05:40,020 --> 00:05:40,890
If the choice.

111
00:05:44,110 --> 00:05:46,570
Is not in zero.

112
00:05:49,330 --> 00:05:50,830
One or two.

113
00:05:50,920 --> 00:05:53,650
And if you want, it could have made this a variable called acceptable values.

114
00:05:54,760 --> 00:05:56,510
Then I'm simply going to say, Prince.

115
00:05:58,770 --> 00:06:01,170
Sorry, invalid choice.

116
00:06:04,200 --> 00:06:07,380
OK, so that while Loop keeps going over and over again.

117
00:06:07,770 --> 00:06:14,490
Once we have a valid choice, actually want it as an integer song, I say return the integer choice.

118
00:06:15,840 --> 00:06:17,310
Let's go ahead to make sure this works.

119
00:06:17,430 --> 00:06:18,440
Position choice.

120
00:06:19,290 --> 00:06:20,280
Go ahead and run this.

121
00:06:20,700 --> 00:06:23,820
So notice if I type in a string that's actually not in that list.

122
00:06:23,850 --> 00:06:27,220
So that's not going to work if I type in something like 24.

123
00:06:27,660 --> 00:06:28,710
That's not in that list.

124
00:06:28,740 --> 00:06:32,190
So there's only three things I can type in order to close off this while loop.

125
00:06:32,220 --> 00:06:34,110
It's either a zero, a one or two.

126
00:06:34,680 --> 00:06:36,750
So if I say one, then we're good to go.

127
00:06:37,320 --> 00:06:38,880
So that's the position choice.

128
00:06:38,940 --> 00:06:39,990
Very simple function.

129
00:06:40,350 --> 00:06:41,970
And it's simpler than what we've seen before.

130
00:06:42,020 --> 00:06:45,540
We could always expand this to be more flexible, given what we already learned.

131
00:06:46,170 --> 00:06:48,180
Next, it's time to choose a replacement.

132
00:06:48,630 --> 00:06:51,780
So let's imagine the player has already chosen their position correctly.

133
00:06:52,380 --> 00:06:54,180
They need to choose a replacement value.

134
00:06:54,420 --> 00:06:56,610
So what do they actually want to insert there?

135
00:06:57,330 --> 00:07:00,120
And in this case, we're going to take in two parameters.

136
00:07:00,630 --> 00:07:04,020
I'm going to take in the game list and the position.

137
00:07:04,500 --> 00:07:11,850
So this position choice is going to return position and then game list will be a variable that I keep

138
00:07:11,850 --> 00:07:15,150
sending back and forth between functions in order to update it.

139
00:07:16,760 --> 00:07:17,450
So we'll say.

140
00:07:18,580 --> 00:07:19,630
User placement.

141
00:07:21,770 --> 00:07:22,730
Is equal to.

142
00:07:23,970 --> 00:07:24,630
Input.

143
00:07:25,750 --> 00:07:30,090
Also, a type of string to place at position.

144
00:07:32,010 --> 00:07:37,200
And then I'll simply grab the game list at the position previously passed.

145
00:07:39,280 --> 00:07:41,350
And then we'll say is equal to.

146
00:07:42,520 --> 00:07:46,090
User placement, and it could be user choice or whatever you want to call this variable.

147
00:07:46,690 --> 00:07:51,400
And then finally, in order to keep passing that game list that's updated to different functions.

148
00:07:51,880 --> 00:07:54,280
I'm going to return that game list.

149
00:07:55,360 --> 00:07:56,380
So go ahead and check this out.

150
00:07:56,860 --> 00:07:58,150
We'll say replacement choice.

151
00:07:58,630 --> 00:08:01,210
Going to choose my game list.

152
00:08:02,170 --> 00:08:06,750
And let's say my current position has already been returned as integer choice.

153
00:08:06,860 --> 00:08:07,680
I'm going to pass in.

154
00:08:07,840 --> 00:08:09,520
Let's say one here.

155
00:08:10,680 --> 00:08:14,880
So I run this, it says type of string to replace a position and it's a test.

156
00:08:15,630 --> 00:08:16,170
There we go.

157
00:08:16,560 --> 00:08:23,130
Returns to that game list with zero test and two perfect looks like replacement choice is also working.

158
00:08:23,850 --> 00:08:26,910
And then finally, we want to make sure that the user keeps playing.

159
00:08:27,780 --> 00:08:31,050
So let's go ahead and have something that's like a game on choice.

160
00:08:31,590 --> 00:08:37,350
And I'm going to copy and paste my position choice because it's so similar to the game on choice.

161
00:08:37,380 --> 00:08:40,380
I'm just going to edit it until it works for game on.

162
00:08:40,420 --> 00:08:43,560
Sensitive position choice will say game on.

163
00:08:43,740 --> 00:08:46,080
It's essentially going to ask the player, do you want to keep playing?

164
00:08:46,590 --> 00:08:47,850
So the choice starts off wrong.

165
00:08:48,240 --> 00:08:50,070
And there's only two options for the choice.

166
00:08:50,130 --> 00:08:51,570
It's either gonna be yes or no.

167
00:08:55,920 --> 00:09:00,540
So we'll say while choice, not N, y or n ask the user, do you want to keep playing?

168
00:09:01,170 --> 00:09:01,530
So.

169
00:09:02,800 --> 00:09:08,370
Keep playing question mark and then we'll tell him that's either capital Y or N.

170
00:09:09,110 --> 00:09:11,660
And then of choice is not in wire.

171
00:09:11,760 --> 00:09:12,460
And so we'll see.

172
00:09:12,760 --> 00:09:13,270
Copy that.

173
00:09:15,660 --> 00:09:17,170
We'll say something like sorry.

174
00:09:18,670 --> 00:09:21,130
I don't understand.

175
00:09:23,340 --> 00:09:26,940
Please choose Y or N.

176
00:09:28,530 --> 00:09:32,390
And then finally, if that happens to be the case, then we know they have a Y or N.

177
00:09:32,790 --> 00:09:35,370
So I want to return whether or not they want to keep playing.

178
00:09:36,150 --> 00:09:37,170
So if the choice.

179
00:09:38,630 --> 00:09:40,970
Is equal to capital, why then, that's a yes.

180
00:09:41,000 --> 00:09:44,230
They want to keep playing, which means I'm going to return.

181
00:09:44,240 --> 00:09:46,730
True that the game is still on.

182
00:09:48,970 --> 00:09:49,450
Else.

183
00:09:50,710 --> 00:09:53,860
I'll say the game's over and it's going to return false.

184
00:09:53,980 --> 00:09:58,300
So essentially what game on choice does is ask the player, do you want to keep playing?

185
00:09:58,330 --> 00:10:03,430
It accepts either capital Y or capital N and then returns a boolean value that corresponds to those

186
00:10:03,430 --> 00:10:03,880
letters.

187
00:10:04,300 --> 00:10:09,070
Whether it's true or false, which means I can use this choice to maintain some while loop.

188
00:10:09,910 --> 00:10:11,200
Go ahead and make sure this works.

189
00:10:11,470 --> 00:10:12,610
Say game on choice.

190
00:10:14,900 --> 00:10:15,510
Run this.

191
00:10:15,710 --> 00:10:21,440
And so says, do you want to keep playing if I type in something wrong, says sorry, I don't understand.

192
00:10:21,900 --> 00:10:23,750
I type in why it returns back.

193
00:10:23,750 --> 00:10:24,080
True.

194
00:10:24,640 --> 00:10:25,520
Spread this again.

195
00:10:25,550 --> 00:10:28,490
If I type in capital N returns back.

196
00:10:28,550 --> 00:10:29,030
False.

197
00:10:29,420 --> 00:10:29,900
Perfect.

198
00:10:30,290 --> 00:10:34,970
So now I have a function that basically asks player you want to be playing and whether or not it's true

199
00:10:34,970 --> 00:10:36,770
or false to finish us off.

200
00:10:36,800 --> 00:10:39,230
We simply have to put this all together.

201
00:10:40,040 --> 00:10:41,810
So we'll start off with some initial variables.

202
00:10:42,320 --> 00:10:47,760
We'll say game on starts off as true and then we'll have our initial game list.

203
00:10:48,810 --> 00:10:50,520
Equal to simply 012.

204
00:10:52,000 --> 00:10:53,950
And so while the game is on.

205
00:10:56,260 --> 00:11:02,800
So I can do while game on is equal to true or since game on is already going to be a boolean.

206
00:11:02,890 --> 00:11:05,650
I'll just say while the game is on.

207
00:11:07,220 --> 00:11:08,420
Then I'm going to.

208
00:11:09,910 --> 00:11:10,900
Display the game.

209
00:11:12,090 --> 00:11:14,550
Which means I have to display the current game list.

210
00:11:16,300 --> 00:11:21,350
Then I need to have the player choose a position, so I'll say position is equal to that position,

211
00:11:21,350 --> 00:11:22,430
choice function we made.

212
00:11:24,530 --> 00:11:30,500
Then the player has chosen a position which means we need to rewrite that position and update the game

213
00:11:30,500 --> 00:11:30,890
list.

214
00:11:31,310 --> 00:11:39,320
So recall, we have this replacement choice function which takes in the game list and the position.

215
00:11:39,440 --> 00:11:45,260
And you should also recall, if we scroll back up here, that replacement choice returns the updated

216
00:11:45,260 --> 00:11:48,800
game list, which means I want to actually update.

217
00:11:50,170 --> 00:11:54,070
Game list based off that replacement choice.

218
00:11:54,790 --> 00:12:02,680
And then finally, we will display the game again, essentially displaying that updated game list.

219
00:12:04,080 --> 00:12:05,760
And this would just keep going on forever.

220
00:12:06,000 --> 00:12:06,930
While game is on.

221
00:12:07,260 --> 00:12:11,850
So let's give the user a chance to say whether game on should be equal to false or not.

222
00:12:12,180 --> 00:12:13,890
Which is this game on choice.

223
00:12:14,490 --> 00:12:17,460
So we'll say game on is equal to.

224
00:12:18,980 --> 00:12:19,850
Game on choice.

225
00:12:21,860 --> 00:12:22,340
Perfect.

226
00:12:22,940 --> 00:12:25,160
And that's the very simple logic we have here.

227
00:12:25,280 --> 00:12:25,820
Go and run it.

228
00:12:25,880 --> 00:12:26,630
Make sure it works.

229
00:12:27,870 --> 00:12:29,970
OK, so here's the current list.

230
00:12:30,330 --> 00:12:33,090
Zero, one and two pick a position.

231
00:12:33,480 --> 00:12:35,910
Let's double check to make sure if I choose something wrong.

232
00:12:35,940 --> 00:12:37,530
It still works if I choose to.

233
00:12:38,040 --> 00:12:40,470
Says, hey, sorry, invalid choice.

234
00:12:40,830 --> 00:12:41,820
Pick a position again.

235
00:12:42,000 --> 00:12:42,930
Here's the current list.

236
00:12:43,440 --> 00:12:44,610
So let's choose one.

237
00:12:45,560 --> 00:12:48,240
Hit enter type of string to place that position.

238
00:12:48,870 --> 00:12:49,560
We'll say.

239
00:12:50,760 --> 00:12:52,770
My choice hit Enter.

240
00:12:53,320 --> 00:12:55,650
And now the current list is zero.

241
00:12:55,770 --> 00:12:58,380
My choice to go ahead and keep playing.

242
00:12:59,180 --> 00:13:01,590
You can see how the functions are being called in order here.

243
00:13:02,710 --> 00:13:03,930
OK, here's the current list.

244
00:13:04,080 --> 00:13:04,560
Zero.

245
00:13:04,650 --> 00:13:07,470
My choice to pick zero position.

246
00:13:08,190 --> 00:13:08,940
Type A string.

247
00:13:09,300 --> 00:13:10,860
We'll say test hit.

248
00:13:10,890 --> 00:13:11,340
Enter.

249
00:13:11,640 --> 00:13:12,510
And there's current list.

250
00:13:12,550 --> 00:13:13,840
Test my choice too.

251
00:13:14,760 --> 00:13:17,250
And I'm say all done playing and.

252
00:13:17,520 --> 00:13:19,440
And the loop is finished executing.

253
00:13:20,070 --> 00:13:20,490
That's it.

254
00:13:21,240 --> 00:13:21,520
OK.

255
00:13:22,020 --> 00:13:28,200
So we've definitely put a lot of pieces together in order to create an interactive python program.

256
00:13:28,560 --> 00:13:34,590
And the main things we have to be aware of is the ability to display information to the user except

257
00:13:34,680 --> 00:13:35,850
information from the user.

258
00:13:36,330 --> 00:13:40,800
Validate that information and then update whatever the user is actually saying.

259
00:13:41,250 --> 00:13:47,010
And with just a couple of functions, we can see that we're able to do that here, displaying information.

260
00:13:47,940 --> 00:13:51,720
Getting user information in order to validate that information.

261
00:13:52,080 --> 00:13:57,780
Then update what we're actually displaying that game list and then getting more user information to

262
00:13:57,780 --> 00:13:59,380
see if we want to keep going with the program.

263
00:14:00,240 --> 00:14:00,510
OK.

264
00:14:00,960 --> 00:14:06,390
You pretty much have everything you need within this lecture notebook and these lecture videos in order

265
00:14:06,390 --> 00:14:13,350
to make another higher level jump to program something that is a little more interactive, such as a

266
00:14:13,350 --> 00:14:14,670
tic tac toe board game.

267
00:14:15,150 --> 00:14:18,570
So let's get you ready by explaining your milestone project coming up.

268
00:14:18,840 --> 00:14:19,890
I'll see you in the next lecture.
