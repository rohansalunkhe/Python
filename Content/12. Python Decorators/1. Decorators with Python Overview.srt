1
00:00:05,430 --> 00:00:11,700
Welcome everyone to this topic on decorator's we're going to discuss a more advanced Python concept

2
00:00:11,760 --> 00:00:16,390
which is the decorator and decorators allow you to quote decorate a function.

3
00:00:16,620 --> 00:00:20,610
Let's discuss what that word Duckery actually means in this context.

4
00:00:21,970 --> 00:00:26,490
Imagine that you created a function here representing it by just a simple function.

5
00:00:26,500 --> 00:00:29,340
It does some simple stuff and then returns something.

6
00:00:29,380 --> 00:00:33,490
So you create this simple function and now you want to add some more functionality to it.

7
00:00:33,610 --> 00:00:39,730
Or in other words add a more code where you could do is take your original simple function then add

8
00:00:39,730 --> 00:00:43,350
some more stuff to it keep doing the old stuff and then return something.

9
00:00:43,360 --> 00:00:49,660
But this idea of adding new functionality basically represents a problem because you have two options

10
00:00:49,660 --> 00:00:50,170
here.

11
00:00:50,240 --> 00:00:55,510
Either you can add the extra code otherwise known as adding functionality to your old function but then

12
00:00:55,510 --> 00:00:58,750
you have a problem of not being able to call your original function.

13
00:00:58,750 --> 00:01:01,620
You've added it in some way with that new functionality.

14
00:01:01,630 --> 00:01:06,790
The other option is to create a brand new function where you copied all the old code and then added

15
00:01:06,790 --> 00:01:08,040
new code to that.

16
00:01:08,050 --> 00:01:12,790
The problem with that though is that you had to recreate the entire function over again even though

17
00:01:12,790 --> 00:01:13,780
it's just a copy and paste.

18
00:01:13,780 --> 00:01:14,670
It's not ideal.

19
00:01:14,690 --> 00:01:20,710
You had to actually make another function but what you actually want to remove that extra functionality

20
00:01:20,720 --> 00:01:21,620
at a later date.

21
00:01:21,650 --> 00:01:28,430
Well it have to do is delete that function or make sure you don't call it so be it manually making sure

22
00:01:28,430 --> 00:01:29,650
you don't have the old function.

23
00:01:29,660 --> 00:01:30,870
There's got to be a better way.

24
00:01:30,920 --> 00:01:36,110
So what would be nice is if you had an on off switch where you could quickly add that new code or add

25
00:01:36,110 --> 00:01:39,340
that new functionality and then turn it back off again.

26
00:01:39,920 --> 00:01:42,220
This is where the decorator comes into play.

27
00:01:42,230 --> 00:01:47,330
Python has a concept called decorator's that allow you to quickly tack on extra functionality to an

28
00:01:47,360 --> 00:01:52,100
already existing function and then if you no longer want that functionality you just delete one line

29
00:01:52,130 --> 00:01:54,400
from the decorator so they use the.

30
00:01:54,400 --> 00:02:01,160
Operator and are then placed on top of the original function so you can easily add on or turn off extra

31
00:02:01,160 --> 00:02:05,630
functionality with a decorator so you just take your original simple function and then you have some

32
00:02:05,630 --> 00:02:11,180
decorator which connects to some extra code and then you can add in extra functionality.

33
00:02:11,210 --> 00:02:15,540
Then if you don't want that extra functionality you just delete that at some decorator line.

34
00:02:15,560 --> 00:02:17,930
On top of the function.

35
00:02:18,100 --> 00:02:23,060
So this idea is pretty abstract when you actually practice it with Python syntax because it's kind of

36
00:02:23,060 --> 00:02:24,530
this mysterious one liner here.

37
00:02:24,560 --> 00:02:26,240
On top of your function.

38
00:02:26,240 --> 00:02:31,130
So what we're going to do is we're going to go through the steps of manually building out a decorator

39
00:02:31,160 --> 00:02:35,440
ourselves to show what that operator is actually doing behind the scenes.

40
00:02:35,460 --> 00:02:39,770
We're also going to learn about some concepts such as passing a function within another function.

41
00:02:39,950 --> 00:02:42,810
We're going to go step by step and build this out very slowly.

42
00:02:42,950 --> 00:02:45,680
So let's open up a Jupiter notebook and get started.

43
00:02:45,680 --> 00:02:49,620
OK lets start off by actually building out our own decorator's manually.

44
00:02:49,730 --> 00:02:54,590
We need to review a few concepts though and introduce some new ones such as assigning functions or passing

45
00:02:54,590 --> 00:02:57,800
in functions into other functions.

46
00:02:58,370 --> 00:03:06,650
So we can do is create a simple function called func and this is going to return one so I can call that

47
00:03:06,920 --> 00:03:09,780
function and execute it and it get back 1.

48
00:03:09,810 --> 00:03:14,900
Remember we also discussed that if you just say func with open and closed print CS you'll get back the

49
00:03:14,900 --> 00:03:16,870
information that hey you have a function here.

50
00:03:17,000 --> 00:03:19,420
You won't actually execute the function.

51
00:03:19,460 --> 00:03:25,370
That means we can actually assign functions to other variables and then execute them off that variable.

52
00:03:25,370 --> 00:03:26,810
So let me show you what I mean by that.

53
00:03:27,840 --> 00:03:38,420
For example I will say create the hello function that returns back hello so I can execute hello or I

54
00:03:38,420 --> 00:03:40,510
can just ask Hey hello what is that.

55
00:03:40,550 --> 00:03:42,650
Well it's assigned to a function.

56
00:03:42,920 --> 00:03:43,510
I couldn't say.

57
00:03:43,510 --> 00:03:49,170
Greets is equal to hello and Python won't complain.

58
00:03:49,170 --> 00:03:49,650
In fact.

59
00:03:49,650 --> 00:03:51,730
Now check out what I can do.

60
00:03:51,780 --> 00:03:56,410
I can see called greets and it returns back the string Hello.

61
00:03:56,820 --> 00:03:59,530
So what we want to ask ourselves is greet.

62
00:03:59,530 --> 00:04:00,840
Just pointing to hello.

63
00:04:00,930 --> 00:04:03,690
Or has it made its own copy of the hello function.

64
00:04:03,690 --> 00:04:08,700
We can easily test that by deleting hello and then seeing if we can still call great.

65
00:04:08,850 --> 00:04:13,470
So right now I can still execute hello I'm going to delete.

66
00:04:13,500 --> 00:04:19,790
Hello and if I try executing Hello Now I'll say hey hello is not the find.

67
00:04:19,820 --> 00:04:20,370
Makes sense.

68
00:04:20,360 --> 00:04:21,600
We just deleted it.

69
00:04:21,680 --> 00:04:23,430
Now let's see what happens when I call accrete.

70
00:04:26,100 --> 00:04:27,270
So grete still returns.

71
00:04:27,270 --> 00:04:27,770
Hello.

72
00:04:27,820 --> 00:04:32,970
So even though we deleted the name Hello the name Green is actually still pointing to the original function

73
00:04:33,000 --> 00:04:38,330
object and it's important to know that functions are objects that can be passed into other objects.

74
00:04:38,370 --> 00:04:44,250
So now is shown example of passing in a function within another function or calling functions within

75
00:04:44,250 --> 00:04:45,090
other function.

76
00:04:45,330 --> 00:04:52,380
And this is going to tie in a lot with that scope and nested statements lecture that we had previously.

77
00:04:52,520 --> 00:04:57,610
So I'm going to make a couple of new cells here just to clear that screen.

78
00:04:57,960 --> 00:05:05,240
Now let's create a function called hello and it takes in a name will have the default name be Jose and

79
00:05:05,240 --> 00:05:15,670
the simple hello function is going to say hello function has been executed.

80
00:05:16,450 --> 00:05:17,400
So right now if I call.

81
00:05:17,400 --> 00:05:18,920
Hello.

82
00:05:19,000 --> 00:05:21,110
It just says hello function has been executed.

83
00:05:21,290 --> 00:05:22,580
Makes sense.

84
00:05:22,580 --> 00:05:25,580
Now let's define a function inside of this function.

85
00:05:26,370 --> 00:05:32,460
I'm going to now say grete and what this does is it just whips.

86
00:05:32,800 --> 00:05:38,760
It's going to return sort of I'm using return a longer print and it's going to print backslash T.

87
00:05:38,820 --> 00:05:48,460
That's a way to escape character for a tab and let's say this is the great function inside.

88
00:05:49,170 --> 00:05:52,240
Hello.

89
00:05:52,250 --> 00:05:56,350
So if I rerun these cells I still just get back hello.

90
00:05:56,360 --> 00:05:57,810
Function has been executed.

91
00:05:57,890 --> 00:05:59,930
So let's think about what's happening here.

92
00:06:00,140 --> 00:06:01,130
I have this function.

93
00:06:01,130 --> 00:06:01,940
Hello.

94
00:06:01,940 --> 00:06:07,160
It takes in some default name by Jose and it just prints out the hello function has been executed.

95
00:06:07,160 --> 00:06:10,990
We're actually not using the name yet inside of this function.

96
00:06:11,060 --> 00:06:14,980
I'm defining another function called greet and greet returns.

97
00:06:14,990 --> 00:06:16,990
This is a great function inside Hello.

98
00:06:17,120 --> 00:06:19,150
Notice here I'm just defining great.

99
00:06:19,160 --> 00:06:25,370
I'm not actually calling it so let's call it outside of Greece.

100
00:06:25,370 --> 00:06:26,380
I'm going to print

101
00:06:29,330 --> 00:06:30,220
greets.

102
00:06:30,330 --> 00:06:35,230
Notice I'm printing it because greed returns back the string.

103
00:06:35,260 --> 00:06:37,440
So now when I run this I get back.

104
00:06:37,450 --> 00:06:38,170
Hello.

105
00:06:38,320 --> 00:06:39,640
Function has been executed.

106
00:06:39,760 --> 00:06:41,400
And then I get back tab.

107
00:06:41,620 --> 00:06:42,810
This is the great function site.

108
00:06:42,820 --> 00:06:43,420
Hello.

109
00:06:43,600 --> 00:06:48,600
Because we find the inside hello and then we executed it inside hello as well.

110
00:06:49,870 --> 00:06:52,220
So let's define another function inside.

111
00:06:52,240 --> 00:06:56,390
Hello call welcome and we'll do the same thing.

112
00:06:56,420 --> 00:06:59,010
We'll see a return tab.

113
00:06:59,260 --> 00:07:02,440
This is inside.

114
00:07:02,560 --> 00:07:08,200
This is say this is welcome inside Hello.

115
00:07:08,480 --> 00:07:14,710
And then after that we're going to execute welcome and it's really important here that you pay attention

116
00:07:14,710 --> 00:07:17,970
to the indentation because it's really easy to mess things up.

117
00:07:17,980 --> 00:07:23,200
I have the indentation here for hello and everything else here is lined up.

118
00:07:23,200 --> 00:07:27,790
And then I have these two lines indented because they're part of the Greek definition and they're part

119
00:07:27,790 --> 00:07:29,160
of the welcome definition.

120
00:07:29,170 --> 00:07:34,010
I don't actually call greet and welcome inside of these functions that I call it only inside Hello.

121
00:07:34,010 --> 00:07:36,640
So notice here that these print statements are lined up.

122
00:07:36,670 --> 00:07:37,080
Hello.

123
00:07:37,100 --> 00:07:41,140
You can always copy and paste from the notebooks in case you're getting any sort of indentation or syntax

124
00:07:41,170 --> 00:07:42,040
errors.

125
00:07:42,550 --> 00:07:45,190
So when I run this I get back.

126
00:07:45,190 --> 00:07:50,140
Hello function has been executed and we have these tabs here to kind of show that the great and welcome

127
00:07:50,140 --> 00:07:56,260
functions are actually inside the hello function and at the very end of all this I can still say this

128
00:07:56,350 --> 00:08:05,390
is the end of the hello function so I run this the whole function is executed.

129
00:08:05,410 --> 00:08:06,210
I define.

130
00:08:06,280 --> 00:08:10,320
I find welcome that I actually execute gry execute welcome and print.

131
00:08:10,350 --> 00:08:12,320
This is the end of Hello.

132
00:08:12,340 --> 00:08:17,920
Now something to notice here is that greet is defined inside of the hello function and welcomes the

133
00:08:17,920 --> 00:08:20,050
find inside of the hello function.

134
00:08:20,050 --> 00:08:23,080
That means the scope is limited to the hello function.

135
00:08:23,230 --> 00:08:27,960
I can only execute greet and welcome inside of hello if I try to execute them.

136
00:08:28,000 --> 00:08:30,040
Outside of this hello function.

137
00:08:30,040 --> 00:08:31,480
For example if I try to call.

138
00:08:31,480 --> 00:08:33,390
Welcome.

139
00:08:33,550 --> 00:08:38,270
It says Hey welcome is not the find that which makes sense because it was only defined inside of this

140
00:08:38,290 --> 00:08:39,200
hello function.

141
00:08:39,370 --> 00:08:44,360
And as long as you didn't define green earlier it will also end up being an error.

142
00:08:44,390 --> 00:08:47,690
So here we have this idea that greet and welcome.

143
00:08:47,710 --> 00:08:49,640
Their scope is limited to hello.

144
00:08:49,940 --> 00:08:53,250
But what if you want to access these functions outside of Hello.

145
00:08:53,470 --> 00:08:58,280
Well we could do is have the hello function actually return a function.

146
00:08:58,400 --> 00:09:04,140
Remember that up here we were able to assign functionality in this manner.

147
00:09:04,190 --> 00:09:09,560
It would be really cool if our helo function instead of just printing out the execution of greet it

148
00:09:09,560 --> 00:09:10,280
returned.

149
00:09:10,280 --> 00:09:11,230
Great.

150
00:09:11,360 --> 00:09:13,070
So here's what I'm going to do.

151
00:09:13,100 --> 00:09:15,040
I'm going to delete these three lines.

152
00:09:16,220 --> 00:09:18,980
And then I'm going to print out.

153
00:09:19,220 --> 00:09:32,500
I am going to return a function and will say if the name is equal to Jose return.

154
00:09:32,760 --> 00:09:40,410
Greets else return welcome.

155
00:09:40,430 --> 00:09:42,820
OK so let's think what's going to happen here.

156
00:09:44,380 --> 00:09:46,730
Right now my default name is Jose.

157
00:09:46,750 --> 00:09:48,830
I print out Hello function has been executed.

158
00:09:49,000 --> 00:09:50,440
Then I define two functions.

159
00:09:50,440 --> 00:09:52,540
One is called gry one's called Welcome.

160
00:09:52,570 --> 00:09:57,720
I print out hey I'm going to return a function and then we say hey if the name was Jose return the group

161
00:09:57,730 --> 00:10:03,160
function or else return the welcome function with that's going to allow us to do is it's going to allow

162
00:10:03,160 --> 00:10:08,630
us to return a function which we can then assigned to a variable just like we did earlier appear.

163
00:10:08,890 --> 00:10:12,010
So let's see an example of that.

164
00:10:12,160 --> 00:10:18,160
We're going to say my new funk is equal to hello.

165
00:10:18,520 --> 00:10:23,220
And we'll pass in the name Jose which is also a default name so you could just say hello.

166
00:10:24,550 --> 00:10:26,980
And I get printed out the low function has been executed.

167
00:10:27,010 --> 00:10:28,570
I'm going to return a function.

168
00:10:28,750 --> 00:10:33,700
So because the name was hello this function after doing these two print statements it actually returned

169
00:10:33,700 --> 00:10:38,130
back the function which was defined inside of Hello.

170
00:10:38,410 --> 00:10:46,110
And now if I call my new phunk notice here it says function main.

171
00:10:46,160 --> 00:10:46,920
Hello.

172
00:10:47,090 --> 00:10:48,780
Locals greet.

173
00:10:48,860 --> 00:10:53,900
So it's actually that pointed to that greet function inside hello and I can execute it and it returns

174
00:10:53,900 --> 00:10:56,230
back the string tab.

175
00:10:56,230 --> 00:11:02,140
This is the great function inside hello which I can then print just like we did before to actually see

176
00:11:02,200 --> 00:11:03,010
the string itself.

177
00:11:03,010 --> 00:11:04,110
This is the great function inside.

178
00:11:04,120 --> 00:11:05,390
Hello.

179
00:11:05,410 --> 00:11:10,810
So this is the idea of being able to return a function within another function.

180
00:11:10,810 --> 00:11:13,980
Let's see one last very simple example.

181
00:11:14,110 --> 00:11:22,060
I'm going to have a function here called Cool and what it's going to do is have another function inside

182
00:11:22,060 --> 00:11:25,210
of it called Let's Go supercool

183
00:11:27,980 --> 00:11:32,510
and this is just the string all this function does is it returns back.

184
00:11:32,510 --> 00:11:34,630
I am very cool.

185
00:11:36,620 --> 00:11:46,920
And then this function cool is actually going to return super cool so I can say that some func is equal

186
00:11:46,920 --> 00:11:50,680
to the result of executing cool.

187
00:11:50,810 --> 00:11:54,100
Remember that cool itself returns super cool.

188
00:11:54,110 --> 00:11:59,210
So now some phunk is actually the supercool function.

189
00:12:00,720 --> 00:12:04,030
And if execute it Ill say I am very cool.

190
00:12:04,100 --> 00:12:09,920
So this is just a simple idea of having a function defining a function inside of that function and then

191
00:12:09,920 --> 00:12:15,680
returning that function and we're going to use this as our idea of building out a decorator.

192
00:12:15,680 --> 00:12:20,390
The last thing we need to think about is actually having a function as an argument with the idea of

193
00:12:20,390 --> 00:12:25,250
being able to return a function and assign that to something and then passing in a function its argument

194
00:12:25,310 --> 00:12:29,710
will have all the tools we need to actually create our own decorator.

195
00:12:29,880 --> 00:12:35,950
So let's quickly see an example of passing in a function as an argument.

196
00:12:36,160 --> 00:12:43,440
I'm going to say hello and the hello function now is just going to say hi.

197
00:12:43,460 --> 00:12:46,660
Jose so we have that function.

198
00:12:46,660 --> 00:12:47,400
Hello.

199
00:12:47,920 --> 00:12:50,000
And I will create another function called other.

200
00:12:50,410 --> 00:12:57,130
And this takes an some defined function.

201
00:12:57,140 --> 00:13:02,070
So notice here this is just a variable name I could call this whatever I want it and what it's going

202
00:13:02,070 --> 00:13:07,070
to do is say Prince other code runs here.

203
00:13:09,510 --> 00:13:11,520
And then it's actually going to execute that function.

204
00:13:11,540 --> 00:13:18,030
I'll say taking that function and open close Princie executed.

205
00:13:18,050 --> 00:13:19,300
So what does this mean.

206
00:13:19,310 --> 00:13:25,640
It means I'm going to build actually pass in a function into this other function do some stuff and then

207
00:13:25,790 --> 00:13:26,970
execute the function.

208
00:13:27,140 --> 00:13:29,550
This is known as passing a function as an argument.

209
00:13:29,690 --> 00:13:34,700
Previously we just saw how we could return functions and then execute them when we assign them to a

210
00:13:34,700 --> 00:13:35,590
new variable name.

211
00:13:36,470 --> 00:13:41,180
Over here we're going to see now how we can pass in functions into another function.

212
00:13:41,210 --> 00:13:42,630
This is pretty cool stuff.

213
00:13:42,800 --> 00:13:45,820
Will say other and I'm going to pasan.

214
00:13:45,920 --> 00:13:46,680
Hello.

215
00:13:46,970 --> 00:13:52,040
Something that's key to notice here is that I'm passing in hello I'm just passing in the wrong function.

216
00:13:52,040 --> 00:13:56,230
Hello I'm not executing hello So just to make it clear there is.

217
00:13:56,240 --> 00:13:59,220
Hello when you just pass it in.

218
00:13:59,220 --> 00:14:00,930
And then there's hello when you execute it.

219
00:14:01,140 --> 00:14:05,820
I just actually want to pass on the raw function because this raw function is going to be executed inside

220
00:14:05,820 --> 00:14:06,870
the other function.

221
00:14:06,870 --> 00:14:12,390
I don't want to actually execute the function here so I won't say other passen hello so I don't have

222
00:14:12,420 --> 00:14:14,310
open and close princes because I don't want.

223
00:14:14,310 --> 00:14:14,950
Hi Jose.

224
00:14:14,970 --> 00:14:19,500
I want to actually pass in the main function there because other function is going to then execute it

225
00:14:19,500 --> 00:14:21,360
for me and say Hi Jose.

226
00:14:21,360 --> 00:14:24,780
So think about what you expect to see when you run the cell.

227
00:14:24,840 --> 00:14:30,530
I should be seeing other code runs here then the other code is going to grab hello and execute it in

228
00:14:30,540 --> 00:14:33,270
this line and print out the result so that I should see.

229
00:14:33,330 --> 00:14:34,270
Hi Jose.

230
00:14:34,300 --> 00:14:35,670
So the other could run here.

231
00:14:35,700 --> 00:14:37,150
Hi Jose when I run this.

232
00:14:37,300 --> 00:14:39,210
And that's exactly what we see.

233
00:14:39,240 --> 00:14:44,550
So now we understand that we can return functions and we can have functions arguments with those two

234
00:14:44,550 --> 00:14:45,480
main tools.

235
00:14:45,480 --> 00:14:47,880
We're actually going to now be able to create a decorator.

236
00:14:47,880 --> 00:14:53,250
We have the tools we need to quickly create some sort of device that is an on off switch when we want

237
00:14:53,250 --> 00:14:59,610
to add more functionality to a decorator so I'm going to create a couple of these cells here and finally

238
00:14:59,610 --> 00:15:03,220
get to the part where we create a new decorator.

239
00:15:03,290 --> 00:15:14,020
I will say the new decorator and this takes an we'll call it original function

240
00:15:17,040 --> 00:15:26,780
and inside of my new decorator function I'm going to say wrap func and what this wrap func represents

241
00:15:26,870 --> 00:15:31,150
is the extra functionality that you want to decorate this original function with.

242
00:15:31,370 --> 00:15:39,330
So what this does is we're going to print some extra code before the original function.

243
00:15:39,850 --> 00:15:41,750
And you can imagine this doesn't have to be a print statement.

244
00:15:41,750 --> 00:15:46,850
It could be a bunch of other code then we're going to take in that original function that was passed

245
00:15:48,010 --> 00:15:57,420
and executed and then we're going to print some extra code after the original function

246
00:16:00,180 --> 00:16:01,680
so notice what we're able to do.

247
00:16:01,730 --> 00:16:07,580
We're passing in the original function adding in some X code before executing the original function

248
00:16:07,790 --> 00:16:09,180
and then adding some code after

249
00:16:12,100 --> 00:16:17,580
then we're going to do here is just return the wrap function.

250
00:16:18,100 --> 00:16:23,130
So you can kind of think about this as the idea of a president with some wrapping paper.

251
00:16:23,260 --> 00:16:28,630
The actual original function is the present and then we're going to essentially put it inside a box

252
00:16:28,690 --> 00:16:29,920
and wrap around it.

253
00:16:30,100 --> 00:16:31,780
Which is why it's called Decoration.

254
00:16:31,780 --> 00:16:36,290
So you're kind of decorating the function with some wrapping paper the wrapping papers is the extra

255
00:16:36,290 --> 00:16:40,260
code that can go on top of the function before or after the function below it.

256
00:16:41,850 --> 00:16:47,760
So this is a function just like we did before if hello and greet except now the names reflect what's

257
00:16:47,760 --> 00:16:48,830
actually happening here.

258
00:16:48,830 --> 00:16:51,060
We created some new decorator function.

259
00:16:51,060 --> 00:16:57,150
It takes in an original function and inside of this we find this wrapper which has some extra code before

260
00:16:57,150 --> 00:17:03,060
it executes that original function puts in some X code after it and then returns the wrap function.

261
00:17:04,280 --> 00:17:12,140
So then I'm going to create a function here and this function is going to need a decorator so that we

262
00:17:12,140 --> 00:17:16,040
call func needs decorator.

263
00:17:16,530 --> 00:17:18,620
And what this does is it prints out.

264
00:17:18,930 --> 00:17:23,620
I want to be decorated.

265
00:17:24,710 --> 00:17:29,480
So the idea is we want to add in some extra functionality to this original guy.

266
00:17:29,480 --> 00:17:34,970
This function needs a decorator right now if I just call function needs a decorator I just get back

267
00:17:35,060 --> 00:17:36,340
I want to be decorated.

268
00:17:36,560 --> 00:17:42,350
What would be really cool is if I could somehow add in some extra code using the framework that I've

269
00:17:42,350 --> 00:17:45,770
built out here and this is actually really easy.

270
00:17:45,770 --> 00:17:56,120
All I need to do is say grab my decorator and pass in this funk that needs a decorator because remember

271
00:17:56,480 --> 00:17:58,570
it's going to take the original function here.

272
00:17:59,790 --> 00:18:03,340
So let's say phunk needs a decorator and pass that in.

273
00:18:03,390 --> 00:18:05,690
No I'm not executing it and just saying.

274
00:18:05,730 --> 00:18:13,380
New decorator and then phunk needs a decorator so that I can call this my decorated function is equal

275
00:18:13,380 --> 00:18:17,390
to this function that needs a decorator pass than the new decorator.

276
00:18:17,550 --> 00:18:22,550
So it's going to happen is when you pass in this original func needs a decorator.

277
00:18:22,800 --> 00:18:24,720
It says OK that's erosional function.

278
00:18:24,720 --> 00:18:29,820
And then instead of the wrap function we add in some X code before we execute that original function.

279
00:18:29,820 --> 00:18:33,830
So we would print I want to be decorated then we print out some extra after the function.

280
00:18:33,960 --> 00:18:40,020
And with this new defined wrapping function we return the wrapped version of the original function which

281
00:18:40,020 --> 00:18:41,880
is all that this line is doing here.

282
00:18:41,970 --> 00:18:46,230
It's taking in that function that wants that greater wrapping it and some extra stuff and then returning

283
00:18:46,230 --> 00:18:49,070
back a wrapped version of that function.

284
00:18:49,590 --> 00:18:55,580
So I run this and then I can call my decorated function and I see this.

285
00:18:55,600 --> 00:19:01,930
So Mexico before the original function I want to be decorated and some X code after the original function.

286
00:19:01,930 --> 00:19:07,090
Now what we did is actually quite complicated because I had to define all this stuff here.

287
00:19:07,540 --> 00:19:12,270
But there's a special syntax for what is essentially this line.

288
00:19:12,760 --> 00:19:16,520
And the special syntax is that operator.

289
00:19:16,550 --> 00:19:29,780
So if I wanted to actually create new decorator what I could do is say at new decorator say DMF.

290
00:19:29,870 --> 00:19:32,610
Well let's just actually grab the original one.

291
00:19:33,020 --> 00:19:37,600
We're going to copy this here paste it here.

292
00:19:37,980 --> 00:19:38,940
Run this.

293
00:19:38,940 --> 00:19:45,520
And now when I call func needs a decorator I get back some extra code for the original function.

294
00:19:45,540 --> 00:19:49,080
I want to be decorated some actual code after the original function.

295
00:19:49,110 --> 00:19:51,190
So notice what happens here.

296
00:19:51,390 --> 00:19:57,720
This keyword of at new decorator when you put it on top of function it just says OK I'm going to pass

297
00:19:57,720 --> 00:20:01,840
this function into this as the original function.

298
00:20:01,840 --> 00:20:08,710
I'm going to do something to it add some X could after Add some x recode or x x x could before call

299
00:20:08,710 --> 00:20:09,530
the original function.

300
00:20:09,580 --> 00:20:14,680
Add some extra code after wrap that into a nice function and then return that wrapped version.

301
00:20:14,680 --> 00:20:18,800
That's all the decorators doing here essentially wrapping it around.

302
00:20:18,940 --> 00:20:24,430
Then if you ever want to turn this off well it's easy you just commet this out and if you were to rerun

303
00:20:24,430 --> 00:20:26,580
this you no longer have that wrapping.

304
00:20:26,590 --> 00:20:30,700
So this is now your on off switch having that at symbol there.

305
00:20:31,000 --> 00:20:36,910
Realistically you really won't be having to do this sort of coding of a new decorator of the wrapping

306
00:20:36,910 --> 00:20:38,180
function et cetera.

307
00:20:38,290 --> 00:20:39,690
You don't have to worry about that.

308
00:20:39,840 --> 00:20:44,050
Well you'll be doing is you're going to be using a web framework or someone else's library and just

309
00:20:44,110 --> 00:20:49,110
adding in these new decorators to maybe render a new Web site or point to another page.

310
00:20:49,120 --> 00:20:54,340
So they're really commonly used in web frameworks such as jingle or flask which is why it's important

311
00:20:54,340 --> 00:20:58,270
to understand behind the scenes what the decorator's actually doing.

312
00:20:58,270 --> 00:21:00,300
Now they stand behind the scenes what it's doing.

313
00:21:00,370 --> 00:21:05,740
You can simply just use this abstract idea that when you tack on this new decorator it's just decorating

314
00:21:05,740 --> 00:21:08,140
your function with some extra code.

315
00:21:08,140 --> 00:21:08,470
All right.

316
00:21:08,470 --> 00:21:10,410
I know that was definitely a lot to take in.

317
00:21:10,540 --> 00:21:14,590
You really want to check out the notebook for this one where it lays out all the steps with some explanatory

318
00:21:14,590 --> 00:21:18,470
text for you if you have any questions feel free to post the Q&amp;A forums.

319
00:21:18,790 --> 00:21:19,730
I'll see at the next lecture.
