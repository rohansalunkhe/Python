1
00:00:05,300 --> 00:00:07,940
Welcome back, everyone, to this Python overview lecture.

2
00:00:09,290 --> 00:00:14,400
In this lecture, I'm going to do a brief overview of what Python is and a little bit of its history.

3
00:00:14,820 --> 00:00:18,510
While you may want to choose Python for programming and what you can do if Python.

4
00:00:18,990 --> 00:00:23,070
And this lecture in particular is really geared towards people who are new to programming in Python

5
00:00:23,130 --> 00:00:25,260
is their first language as a beginner.

6
00:00:25,410 --> 00:00:30,690
It's often unclear what you can actually do with Python and what the difference between base python

7
00:00:30,690 --> 00:00:33,180
is versus lots of additional libraries.

8
00:00:33,360 --> 00:00:35,670
So hopefully this lecture can clear that up for you.

9
00:00:36,610 --> 00:00:39,040
Let's start with just a very brief history of Python.

10
00:00:39,370 --> 00:00:45,370
It was created in 1990 by Guido van Rossum, who is pictured here, and Python three was released in

11
00:00:45,370 --> 00:00:48,560
2008 with subsequent releases happening after that.

12
00:00:48,580 --> 00:00:49,750
Such as three point one.

13
00:00:49,840 --> 00:00:50,590
Three point two.

14
00:00:50,710 --> 00:00:51,360
And so on.

15
00:00:53,720 --> 00:00:59,900
Now, Python, when developed by Guido van Rossum, was specifically designed as an easy to use language

16
00:01:00,140 --> 00:01:03,110
with a very high focus on readability of code.

17
00:01:03,560 --> 00:01:08,120
And these are just a few of the things that have really a lot of python to explode in popularity.

18
00:01:08,600 --> 00:01:11,690
Readability and ease of use was something that was set.

19
00:01:11,930 --> 00:01:14,390
Ever since the start of the creation of the language.

20
00:01:15,840 --> 00:01:17,430
So why would you want to choose Python?

21
00:01:17,940 --> 00:01:23,040
As I mention, ever since the beginning, Python was designed for clear, logical code that is easy

22
00:01:23,040 --> 00:01:24,960
to read and easy to learn.

23
00:01:25,410 --> 00:01:31,200
Lots of other programming languages use braces in brackets that can make code convoluted and maybe hard

24
00:01:31,200 --> 00:01:31,710
to read.

25
00:01:32,070 --> 00:01:37,110
But Python makes use of something called whitespace and indentation that we'll learn about later on

26
00:01:37,110 --> 00:01:41,770
in this course that makes its code very accessible, even if you don't know Python.

27
00:01:41,820 --> 00:01:43,230
But you know another programming language.

28
00:01:43,530 --> 00:01:47,130
You can usually read Python without knowing the real syntax behind it.

29
00:01:47,400 --> 00:01:49,320
That's how readable it is now.

30
00:01:49,380 --> 00:01:52,070
Lots of existing libraries and frameworks are already written.

31
00:01:52,080 --> 00:01:56,490
Python as well, allowing users to apply Python to a wide variety of tasks.

32
00:01:56,850 --> 00:02:00,660
Another reason that Python has really become popular in recent years.

33
00:02:02,940 --> 00:02:08,550
Another reason to choose Python is it really focuses on optimizing developer time rather than a computer's

34
00:02:08,550 --> 00:02:09,420
processing time.

35
00:02:09,870 --> 00:02:15,540
Usually a developers time is much more valuable as a person rather than just the computational processing

36
00:02:15,540 --> 00:02:15,840
time.

37
00:02:16,290 --> 00:02:23,070
So the whole language of Python is designed for users to really get prototyping up and running very

38
00:02:23,070 --> 00:02:23,580
quickly.

39
00:02:23,970 --> 00:02:27,780
It also has fantastic documentation online and lots of helpful resources.

40
00:02:27,810 --> 00:02:29,190
If you just Google Python help.

41
00:02:29,490 --> 00:02:31,740
So the official documentation is fantastic.

42
00:02:32,050 --> 00:02:33,860
And you can check it out at Docs, not Python.

43
00:02:33,870 --> 00:02:36,090
The org forward slash three.

44
00:02:37,700 --> 00:02:40,610
OK, so what can you actually do with Python?

45
00:02:41,120 --> 00:02:46,460
This course really first focuses on what I'm going to call base python, which consists of the core

46
00:02:46,460 --> 00:02:50,090
components of the language and writing scripts and small programs.

47
00:02:50,480 --> 00:02:56,000
Python is often referred to as a program that comes with, quote, batteries included, which means

48
00:02:56,060 --> 00:03:02,300
even the base version of Python actually comes in with lots of additional modules that you can use for

49
00:03:02,360 --> 00:03:03,260
writing scripts.

50
00:03:03,810 --> 00:03:08,570
And his example of that could be something like the random module that comes with Python, which allows

51
00:03:08,570 --> 00:03:12,920
you to quickly create random numbers or the math module of Python cetera.

52
00:03:13,580 --> 00:03:19,850
Now, once we learn about base Python and its base modules, well, we can do is later learn about outside

53
00:03:19,850 --> 00:03:23,300
libraries and frameworks that greatly expand python capabilities.

54
00:03:23,660 --> 00:03:28,550
These are frameworks that are not included with base python, but you can easily download and then work

55
00:03:28,550 --> 00:03:30,800
with Wishard basic python skills.

56
00:03:31,970 --> 00:03:35,480
So what can you do once you start using those outside libraries?

57
00:03:35,870 --> 00:03:40,250
Well, a really useful part of Python, especially if you're someone who works in an office or as an

58
00:03:40,250 --> 00:03:42,650
analyst, is automating simple tasks.

59
00:03:42,680 --> 00:03:48,260
You can do things like have Python automatically search for files and then edit them, scrape information

60
00:03:48,260 --> 00:03:49,100
from a Web site.

61
00:03:49,220 --> 00:03:52,980
Automatically it can read and write Excel files, work with PD FS.

62
00:03:53,240 --> 00:03:56,630
It can even automate emails and text messages and fill out forms for you.

63
00:03:58,410 --> 00:04:02,280
Python is also extremely popular in the data science and machine learning world.

64
00:04:02,700 --> 00:04:07,240
It can do things like analyze large data files using libraries like num, pi and pandas.

65
00:04:07,620 --> 00:04:11,760
It can also create visualizations using outside libraries like Seabourne and matplotlib.

66
00:04:12,270 --> 00:04:17,760
There's also machine learning tasks, and they can create and run predictive algorithms using outside

67
00:04:17,760 --> 00:04:19,950
libraries like psychic learn and Tensor Flow.

68
00:04:20,310 --> 00:04:24,870
So lots of outside libraries have actually been developed for data science and machine learning, and

69
00:04:24,870 --> 00:04:27,810
Python is the most popular language in that field.

70
00:04:30,080 --> 00:04:31,880
Now, you can also create Web sites.

71
00:04:31,910 --> 00:04:36,170
So if you're interested in Web development, people have developed a Web frameworks such as Django and

72
00:04:36,170 --> 00:04:41,510
Flask to handle the back end of a Web site and user data that can communicate with the front end of

73
00:04:41,510 --> 00:04:42,020
a Web site.

74
00:04:42,410 --> 00:04:48,020
You can also do things like create interactive dashboards for users using Python libraries like Plotty

75
00:04:48,080 --> 00:04:48,860
and Dash.

76
00:04:50,370 --> 00:04:55,320
So once you begin to understand based Python in this course and begin working of a few libraries, I

77
00:04:55,320 --> 00:04:59,970
think you'll quickly begin to see the vast potential Python has for you in your own projects.

78
00:05:00,270 --> 00:05:03,750
So keep in mind, this course really starts from the basics and base python.

79
00:05:04,080 --> 00:05:08,970
And then as you get more, more skilled, we'll begin to introduce these outside libraries to perform

80
00:05:08,970 --> 00:05:12,300
tasks which has working with PD, FS or Web scraping sites.

81
00:05:12,660 --> 00:05:15,180
So let's go ahead to get started with setting you up for the course.

82
00:05:15,630 --> 00:05:16,020
Thanks.

83
00:05:16,140 --> 00:05:17,160
And I'll see at the next lecture.
