1
00:00:05,520 --> 00:00:11,300
To finish off this section of the course I want to discuss these two ideas of name and then the string

2
00:00:11,390 --> 00:00:17,950
main So it often confusing part of Python is when you start looking at larger pie scripts that you have

3
00:00:17,950 --> 00:00:18,880
written yourself.

4
00:00:19,030 --> 00:00:23,740
You often see at the very bottom of this mysterious line of code that says if underscore underscore

5
00:00:23,740 --> 00:00:30,040
name underscore underscore is equal to the string underscore underscore main underscore underscore colon

6
00:00:30,160 --> 00:00:32,380
and then there's a block of code to be executed.

7
00:00:33,510 --> 00:00:38,840
So sometimes the whole idea behind that line of code is that we're importing from a module.

8
00:00:38,970 --> 00:00:44,370
You would actually like to know whether a modular function is being used as an import or if you're using

9
00:00:44,370 --> 00:00:46,780
the original but py file of that module.

10
00:00:47,960 --> 00:00:50,700
So we're going to do is explore all of this a little bit more.

11
00:00:50,900 --> 00:00:55,250
But make sure to check the full explanatory text file that has a written explanation of everything we're

12
00:00:55,250 --> 00:00:56,610
going to cover here.

13
00:00:56,620 --> 00:01:01,280
Well we're going to do basically is just create two simple postscripts and try to show you the idea

14
00:01:01,310 --> 00:01:02,210
behind that.

15
00:01:02,300 --> 00:01:08,390
If name is equal to main block of code that is usually at the end a larger pie script files.

16
00:01:08,390 --> 00:01:15,450
Let's get started OK let's begin by actually creating two new files will create them at my desktop.

17
00:01:15,450 --> 00:01:20,990
I will save for this first file save as and I'm going to say this as one dot pi.

18
00:01:21,210 --> 00:01:24,240
So this one is one of pi.

19
00:01:24,300 --> 00:01:32,630
Then I'll create a new file and I'm going to save this one as to that pi to that PI again at my desk

20
00:01:32,700 --> 00:01:35,430
top swivel this one to that pi.

21
00:01:35,450 --> 00:01:38,420
So make sure you have those saved.

22
00:01:38,420 --> 00:01:43,450
So let's think about what actually happens when you call a Python script at your command line.

23
00:01:43,520 --> 00:01:47,860
You go to your command line and you type python space the name your script the PI.

24
00:01:47,930 --> 00:01:54,920
So what's actually happening when you call Python my script the PI all the code that's the indentation

25
00:01:54,920 --> 00:01:55,910
level 0.

26
00:01:56,090 --> 00:01:59,810
Essentially something like this is going to get run.

27
00:01:59,810 --> 00:02:03,840
So that means any code that takes the level 0 is going to get run.

28
00:02:03,890 --> 00:02:09,290
And that would mean that your functions which are automatically usually going to be defined in the station

29
00:02:09,290 --> 00:02:10,970
level 0 in your classes.

30
00:02:11,050 --> 00:02:14,640
Also indentation level 0 are all going to be defined.

31
00:02:14,690 --> 00:02:20,990
So unlike other languages in Python there's no main function that you call somewhere at the top of your

32
00:02:20,990 --> 00:02:23,430
script that gets run automatically.

33
00:02:23,480 --> 00:02:29,240
Instead what happens is just implicitly all the code at the indentation level zero gets run when you

34
00:02:29,240 --> 00:02:33,920
call the script file and you probably have an intuition of that by now since we've been working through

35
00:02:33,980 --> 00:02:34,860
that strategy.

36
00:02:34,970 --> 00:02:43,550
The entire first half of the Course now in Python there is actually a built in variable called name.

37
00:02:43,670 --> 00:02:48,470
So just like there's built in functions this is a built in variable and it's quite obvious that it's

38
00:02:48,470 --> 00:02:52,970
built in because we have two sets of double underscores in the beginning and in the back of it.

39
00:02:52,970 --> 00:02:57,550
So that would allow you to have to really try to overwrite it by accident.

40
00:02:57,560 --> 00:03:04,640
So what happens is this variable name gets assigned a string depending on how you're running the actual

41
00:03:04,640 --> 00:03:05,370
script.

42
00:03:05,540 --> 00:03:08,110
And if you run the script directly.

43
00:03:08,120 --> 00:03:14,750
So if I went to the command line and I wrote out Python space one that pi what Python is going to do

44
00:03:14,750 --> 00:03:20,060
is going to assign this built in a variable called name to be equal to

45
00:03:22,770 --> 00:03:24,260
underscore underscore mean.

46
00:03:24,270 --> 00:03:26,250
So Python does this in the background.

47
00:03:26,430 --> 00:03:28,220
Well it really does this.

48
00:03:28,230 --> 00:03:31,080
It assigns the string to the name variable.

49
00:03:31,080 --> 00:03:32,660
When you run it directly.

50
00:03:32,880 --> 00:03:37,560
And that allows you to have an IF block to check if they're equal to each other.

51
00:03:37,860 --> 00:03:39,480
So often will you do something like this.

52
00:03:39,480 --> 00:03:46,830
You say if name is equal to Main and then you can do something because you know that this particular

53
00:03:46,970 --> 00:03:49,410
PI file is being run correctly.

54
00:03:49,710 --> 00:03:53,880
And if you ever run something directly of Python you know this case happens to be true.

55
00:03:54,180 --> 00:03:59,370
So happens is often people just at the very bottom of their code they'll say if name is equal to Main

56
00:03:59,550 --> 00:04:03,450
and then they say kind of run a bunch of functions that they find here.

57
00:04:03,690 --> 00:04:09,930
So at top they do a bunch of function definitions so function of one function and two so on and so on.

58
00:04:09,930 --> 00:04:13,770
So you can imagine hundreds of lines of function definitions or classes and then all the way at the

59
00:04:13,770 --> 00:04:14,150
bottom.

60
00:04:14,160 --> 00:04:19,160
They actually organized or code on what they actually want to execute and they know that they want to

61
00:04:19,160 --> 00:04:24,210
execute it because they say if this built in variable called name happens to be equal to the string

62
00:04:24,300 --> 00:04:28,360
main then I know I'm running this py file directly.

63
00:04:28,590 --> 00:04:33,630
So we're going to explore this whole idea a little further and to do that we're going to do the following

64
00:04:34,050 --> 00:04:35,530
in one pie.

65
00:04:35,550 --> 00:04:37,370
So let's go back here.

66
00:04:37,440 --> 00:04:38,590
This is one that pi.

67
00:04:38,730 --> 00:04:40,070
I will write out the following code.

68
00:04:40,110 --> 00:04:45,830
I will say DPF create a function and this function is going to be very simple.

69
00:04:45,840 --> 00:04:50,390
It will say func in one PI.

70
00:04:50,790 --> 00:04:56,760
So when you call that function it says func and one that pi just printed that out and then I'm also

71
00:04:56,760 --> 00:05:00,490
going to do here is at an indentation level of 0.

72
00:05:00,480 --> 00:05:09,210
So outside of this function I will say top level in 1 that pi.

73
00:05:09,220 --> 00:05:14,570
So now I have a function definition as well as a global print statement top level and one that pi.

74
00:05:14,780 --> 00:05:19,330
Then at the bottom of this I will say if you'll notice that sublime text that it will actually autocomplete

75
00:05:19,330 --> 00:05:22,840
this for you if name is equal to mean.

76
00:05:22,990 --> 00:05:24,550
I'm going to delete this.

77
00:05:24,550 --> 00:05:26,560
So I have an IF block of code here.

78
00:05:27,540 --> 00:05:31,350
So if name is equal to made that I know I'm running this directly.

79
00:05:31,380 --> 00:05:37,990
So I will say one dot pi is being run correctly.

80
00:05:40,190 --> 00:05:43,430
And then I'm actually going to line up in ELSE block with this if statement.

81
00:05:43,460 --> 00:05:47,150
You don't see this that often usually you see if and then you execute your code.

82
00:05:47,150 --> 00:05:51,900
But just to make it clear how this variable's working will also add in an else there will say.

83
00:05:51,920 --> 00:05:52,790
Else.

84
00:05:52,940 --> 00:06:01,320
Print 1 pi has been imported so I'm going to say that.

85
00:06:01,470 --> 00:06:03,130
And let's take a look at what happens.

86
00:06:03,300 --> 00:06:04,980
I find this function.

87
00:06:04,980 --> 00:06:10,740
I print out this statement whenever you call one that pie and then depending if one PI is the main script

88
00:06:11,040 --> 00:06:14,970
meaning and calling one that PI directly at the command line I will print this out.

89
00:06:15,390 --> 00:06:21,480
Else That means the variable name was not set to mean an old print this one that pi has been imported.

90
00:06:21,930 --> 00:06:30,630
So now let's work with our second file to that pi will do here is say import 1 and I will print out

91
00:06:30,720 --> 00:06:34,830
at the very top level.

92
00:06:35,030 --> 00:06:37,230
Let's print out something like.

93
00:06:37,730 --> 00:06:47,030
Top level in two dot PI and since I just imported one let's execute the function that's from 1 or say

94
00:06:47,030 --> 00:06:55,340
one that func and here are going to have an if statement will autocomplete this if name is equal to

95
00:06:55,390 --> 00:06:56,620
mean.

96
00:06:56,830 --> 00:07:14,470
I will print to the PI is being run directly else print to the PI has been imported.

97
00:07:14,580 --> 00:07:20,270
So let's save the changes in both of these and then open up our command line.

98
00:07:20,270 --> 00:07:26,600
All right so the first thing we're going to do at our command line is actually run one up PI directly.

99
00:07:26,780 --> 00:07:27,580
So we run this.

100
00:07:27,620 --> 00:07:28,690
And we notice what happens.

101
00:07:28,730 --> 00:07:35,000
We get that print statement top level in one up high and then we see from that if name is equal to mean

102
00:07:35,260 --> 00:07:37,270
one that pie's being run directly.

103
00:07:37,280 --> 00:07:41,630
So we can see here that that built in variable underscore underscore name underscore underscore has

104
00:07:41,630 --> 00:07:43,820
been assigned that main string.

105
00:07:43,910 --> 00:07:49,300
So Python that knows and the entire script can know that one that pie has been called directly.

106
00:07:49,490 --> 00:07:54,240
But now let's check out Python to the PI.

107
00:07:54,250 --> 00:07:58,580
So now when I run this notifier happens I get back top level and one that pi.

108
00:07:58,840 --> 00:08:07,260
But now one that pi it's built in variable name has not been set to mean because it's been imported.

109
00:08:07,420 --> 00:08:13,340
So now we get back one that pi has been imported and then I get top level and to that pie funk in one

110
00:08:13,480 --> 00:08:19,690
PI and then to that Pye's being run directly so that built in variable actually allows you to see whether

111
00:08:19,690 --> 00:08:23,220
something is being imported or run directly.

112
00:08:23,300 --> 00:08:29,110
So that's the idea in concept behind the variable name name and checking a physical to main.

113
00:08:29,120 --> 00:08:34,550
Typically however what you see is people they don't really do it the way we've done here with a check

114
00:08:34,870 --> 00:08:36,540
is something being run directly or not.

115
00:08:36,740 --> 00:08:44,650
Instead we come back to Sublime Text Editor what they do is instead of having an L statement here since

116
00:08:44,700 --> 00:08:50,820
they just say if name is equal to me this is the same thing as saying run the script right.

117
00:08:50,920 --> 00:08:55,770
Because we already know that if this happens to be the case then someone is running one that PI directly.

118
00:08:55,810 --> 00:09:00,640
So what they do is they define a bunch of functions up here or find their classes.

119
00:09:00,640 --> 00:09:03,320
So here's another function pass.

120
00:09:03,460 --> 00:09:08,380
You write some other function function to say and then down here at the bottom what you end up doing

121
00:09:08,380 --> 00:09:14,140
is you just call your function so you'll say OK execute function to then execute function 1 and then

122
00:09:14,140 --> 00:09:19,090
maybe have a little bit of logic here but the main idea is that this is more for code organization.

123
00:09:19,090 --> 00:09:23,290
You have all your functions and classes defined up here and then your logic where you actually execute

124
00:09:23,290 --> 00:09:24,600
things are defined.

125
00:09:24,610 --> 00:09:29,500
Here at the bottom and this is a really common structure you will see for larger that by scripts especially

126
00:09:29,500 --> 00:09:32,480
when you begin to use modules and packages.

127
00:09:32,840 --> 00:09:33,270
All right.

128
00:09:33,280 --> 00:09:34,940
That's the basics of this line.

129
00:09:34,960 --> 00:09:39,460
If name is equal to mean definitely check out the explanation that text file that goes along with this

130
00:09:39,460 --> 00:09:40,140
lecture.

131
00:09:40,190 --> 00:09:43,240
And if you have any questions feel free to post the Q&amp;A forums.

132
00:09:43,240 --> 00:09:44,380
I'll see you at the next lecture.
