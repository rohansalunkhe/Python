1
00:00:05,360 --> 00:00:09,760
Welcome everyone to the section on modules and packages in this lecture.

2
00:00:09,760 --> 00:00:12,420
Don't start off by talking about pipeline with Pip.

3
00:00:12,450 --> 00:00:18,150
Install pipeline is a repository for open source third party Python packages.

4
00:00:18,280 --> 00:00:22,810
And if you've worked of other programming languages it's similar to Ruby gen's in the Ruby world ph

5
00:00:23,050 --> 00:00:26,400
packages C Panther Perl and PM for notes.

6
00:00:26,450 --> 00:00:33,010
Yes so far we've really only use libraries that come internally of python and these are known as the

7
00:00:33,010 --> 00:00:39,400
standard library or the built in libraries for Python things like the math library.

8
00:00:39,400 --> 00:00:43,690
Now there are many other libraries available that people have open source and shared on Type-I And these

9
00:00:43,690 --> 00:00:48,490
are also called packages and we can use Pipp installed at the command line to install these external

10
00:00:48,490 --> 00:00:49,380
packages.

11
00:00:50,430 --> 00:00:56,070
By installing Python from Python the OR or by installing the Anaconda's distribution as shown earlier

12
00:00:56,070 --> 00:00:56,940
in this course.

13
00:00:56,940 --> 00:01:02,130
You actually have already installed Pip Pip is a simple way to download packages at your command line

14
00:01:02,160 --> 00:01:05,340
directly from the pipe pipe repository.

15
00:01:05,530 --> 00:01:08,860
Now there are packages already created for almost any use case you can think of.

16
00:01:08,860 --> 00:01:14,770
Python is a hugely popular language and it's growing very quickly so that means if you ever want to

17
00:01:14,770 --> 00:01:20,620
work with Python with some other external use case maybe you want to make web development of Python

18
00:01:20,680 --> 00:01:25,600
so you would Google around and eventually discover that there's libraries such as Django or flask for

19
00:01:25,600 --> 00:01:31,420
web development of Python or maybe have a more specific use case like working with PDSA Python or working

20
00:01:31,420 --> 00:01:33,880
with Excel documentation with Python.

21
00:01:33,940 --> 00:01:38,110
In that case you can always do a quick Google search and almost always the links that come up first

22
00:01:38,170 --> 00:01:43,600
are either at the pipe pipe page for the package or the package documentation and will show you a little

23
00:01:43,600 --> 00:01:46,500
exercise of what a good google search looks like.

24
00:01:47,650 --> 00:01:52,170
So we're going to quickly do now show you how to download and install external packages to do this.

25
00:01:52,170 --> 00:01:55,510
We'll open up our browser but we're also going to keep working at our command line.

26
00:01:55,660 --> 00:01:59,040
So remember for Windows users that means open up your command prompt.

27
00:01:59,220 --> 00:02:02,060
And for Mac OS and Linux users that means open up your terminal.

28
00:02:02,140 --> 00:02:05,280
So let's have the browser open along with our command prompt.

29
00:02:05,470 --> 00:02:12,010
OK so here I am at my command line and let's show you how to install a package that's hosted on Type-I

30
00:02:12,460 --> 00:02:18,790
something you do need to be aware of is that your firewall may block Pipp from accessing the Internet.

31
00:02:18,970 --> 00:02:23,530
So if you're on a work network or you're on a work computer you don't have full admin permissions you

32
00:02:23,530 --> 00:02:26,630
may need to check that or talk to your network administrator.

33
00:02:26,830 --> 00:02:34,720
But let's move on and we're all going to do here is type P IP install space and then here's where you

34
00:02:34,720 --> 00:02:36,560
would actually put in your package name.

35
00:02:36,610 --> 00:02:38,550
So there are a ton of packages out there.

36
00:02:38,720 --> 00:02:43,090
We're going to start with a package that you may already have installed because you've installed Anaconda

37
00:02:43,370 --> 00:02:48,810
and that's the request package which allows you to request information from web sites online.

38
00:02:48,880 --> 00:02:51,980
So this is just a general syntax of how you would download a package.

39
00:02:52,060 --> 00:02:57,880
Notice here I'm typing this directly into the command line or directly onto my terminal or command prompt.

40
00:02:57,880 --> 00:03:04,600
I'm not actually calling Python first a command line utility so you hit enter here and you may get the

41
00:03:04,600 --> 00:03:06,270
requirement already satisfied.

42
00:03:06,430 --> 00:03:10,470
Most likely if you install anaconda you already have requests installed.

43
00:03:10,690 --> 00:03:12,940
So this is what it looks like when you already have it.

44
00:03:13,390 --> 00:03:17,920
What I'm going through now is a library that you most likely do not have yet.

45
00:03:18,010 --> 00:03:24,310
And that's the color Ramah library and it is just a bit of a silly library that allows you to print

46
00:03:24,370 --> 00:03:28,370
out colorize text at your command prompt or at your terminal.

47
00:03:28,370 --> 00:03:30,310
So type Pitman's tall Kalorama.

48
00:03:30,320 --> 00:03:31,810
Notice the spelling here.

49
00:03:31,840 --> 00:03:34,420
Enter an.

50
00:03:34,450 --> 00:03:40,900
We actually already have it installed because I ran this line already so we're going to do now is run

51
00:03:41,290 --> 00:03:45,120
the actual Colorada external package so we just installed.

52
00:03:45,130 --> 00:03:52,430
So what I'm going to do now is type python and let's try checking out Colorada so to do this.

53
00:03:52,430 --> 00:03:53,570
It's actually quite simple.

54
00:03:53,810 --> 00:04:03,240
All it needs to do is say from color Rahma import in it and then on the next line call in it.

55
00:04:03,270 --> 00:04:08,610
So basically is a bit of an initialization function that allows you to set up depending on what platform

56
00:04:08,610 --> 00:04:17,280
you're using and then what we can do is say from color Ramah import and what we're going to do here

57
00:04:17,280 --> 00:04:19,840
is say import for.

58
00:04:19,880 --> 00:04:24,420
So that's capital F O R E which stands for foreground.

59
00:04:24,670 --> 00:04:33,360
And then I will create a print statement that says for dot and then an all caps red plus and I will

60
00:04:33,360 --> 00:04:40,010
just say some red text and then hit enter.

61
00:04:40,150 --> 00:04:48,190
And now you see I have a text and we can do that for a variety of colors so I can say now prints for

62
00:04:49,430 --> 00:04:56,020
green plus switch to green.

63
00:04:56,270 --> 00:04:57,540
And now it's switch to green.

64
00:04:57,560 --> 00:05:03,200
So again just a very simple library that allows you to incorporate color directly at your command line.

65
00:05:03,200 --> 00:05:07,040
This is not a library we will ever really be using again but it's just something fun that you can play

66
00:05:07,040 --> 00:05:11,670
with and you can check the docs documentation simply by Google searching Colorada.

67
00:05:11,780 --> 00:05:12,680
What I want to show you now.

68
00:05:12,710 --> 00:05:15,230
Let me quit out of this Python session.

69
00:05:15,230 --> 00:05:20,390
Show you how you would actually create a Google search and what kind of things to expect to pop up when

70
00:05:20,390 --> 00:05:23,890
you are looking for an external package that you may not know ahead of time.

71
00:05:24,080 --> 00:05:29,480
So for example let's try to solve the problem of trying to work Excel files in Python and figure out

72
00:05:29,480 --> 00:05:31,230
a package that allows us to do that.

73
00:05:31,490 --> 00:05:35,520
So we're looking for a Python package for any particular workflow.

74
00:05:35,660 --> 00:05:37,940
It's always best to start off just the simple Google search.

75
00:05:38,030 --> 00:05:42,680
And I know hopefully this is pretty obvious but a lot of times the best thing to do and pretty much

76
00:05:42,710 --> 00:05:47,300
everybody does this no matter how experienced developer they are is it just google search Python package

77
00:05:47,300 --> 00:05:52,490
for and then whatever they're looking for whether it's Excel for PDA apps and you should always end

78
00:05:52,490 --> 00:05:56,600
up seeing the top results some sort of link to helpful information.

79
00:05:56,600 --> 00:05:58,990
So I'll start off with the example for Excel.

80
00:05:59,270 --> 00:06:05,690
And as we scroll down we'll eventually see Python excel or if you click on that link it actually reports

81
00:06:05,690 --> 00:06:07,900
back a bunch of packages that are available.

82
00:06:08,150 --> 00:06:13,970
Open Excel excel or D X you tills a ton of stuff that you can play around with.

83
00:06:13,970 --> 00:06:18,780
So then we end up doing is you either go to the documentation or you go straight to the download link.

84
00:06:18,860 --> 00:06:23,510
And typically when you go to the download link it takes you to the Python software foundation Web site

85
00:06:23,510 --> 00:06:24,330
for pipeline.

86
00:06:24,430 --> 00:06:26,660
So why Python python.

87
00:06:27,030 --> 00:06:33,700
And here's just official information about the actual package the pending on how big the packages if

88
00:06:33,710 --> 00:06:34,880
it has more users.

89
00:06:34,880 --> 00:06:39,350
It almost always has a separate documentation page and the documentation will be linked somewhere in

90
00:06:39,350 --> 00:06:43,700
the top few lines so you click here and pipe Packers documentation and it will take you to the official

91
00:06:43,700 --> 00:06:47,570
documentation for that actual package.

92
00:06:47,570 --> 00:06:51,340
And a lot of times they're hosted by read the docs thought I know.

93
00:06:51,440 --> 00:06:56,360
So now here we see some nice documentation and all we can do is figure out how to install it for this

94
00:06:56,360 --> 00:07:01,430
documentation or Typically we end up realizing it's it's always just Pipp install and then the name

95
00:07:01,430 --> 00:07:02,600
of the actual package.

96
00:07:02,810 --> 00:07:04,730
So we know that it's open by Excel.

97
00:07:04,910 --> 00:07:06,400
Let's now go back to our command line.

98
00:07:06,400 --> 00:07:07,090
Install it.

99
00:07:07,280 --> 00:07:09,240
So then here back or our command line.

100
00:07:09,230 --> 00:07:17,540
We're going to do is say Pipp install open pie Excel hit enter and I've actually already done this before

101
00:07:17,570 --> 00:07:22,910
but that's how you would install a package from the internet from Type-I and then later on you could

102
00:07:22,910 --> 00:07:29,720
call it Python and then you should be able to import open pie excel with no issues.

103
00:07:30,550 --> 00:07:35,800
That's the basics of using Pipp install with Python and figuring out what external packages are a good

104
00:07:35,800 --> 00:07:36,700
fit for you.

105
00:07:36,760 --> 00:07:37,650
Hope those are useful.

106
00:07:37,710 --> 00:07:40,290
If you have any questions feel free to post the Q&amp;A forms.

107
00:07:40,330 --> 00:07:44,910
Coming up next we're going to learn how to create our own modules and packages using Python.

108
00:07:44,920 --> 00:07:45,690
We'll see you there.
