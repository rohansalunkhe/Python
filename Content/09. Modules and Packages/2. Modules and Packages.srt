1
00:00:05,320 --> 00:00:09,670
Welcome back everyone in this lecture we're now finally going to discuss how to write your own modules

2
00:00:09,730 --> 00:00:10,800
and packages.

3
00:00:11,790 --> 00:00:15,760
So we understand how to install external packages that people uploaded to pie pie.

4
00:00:15,900 --> 00:00:20,880
Let's explore how to career own modules and packages and to clarify some of the semantics here.

5
00:00:20,950 --> 00:00:23,470
Module's it's really just a PI script.

6
00:00:23,670 --> 00:00:28,690
So it's a fancy way of saying here's a postscript that I'm using in another dot PI script.

7
00:00:28,800 --> 00:00:33,490
So that act of using it in another PI script allows you to call it a module.

8
00:00:33,510 --> 00:00:38,120
Again this is really just semantics but packages are then a collection of modules.

9
00:00:38,120 --> 00:00:42,780
However there's a key that postscript called underscore underscore in its underscore underscore the

10
00:00:42,840 --> 00:00:48,810
pie that needs to be placed inside of a folder to let Python know that this collection of Pi scripts

11
00:00:48,870 --> 00:00:51,480
should be treated as an entire package.

12
00:00:51,510 --> 00:00:54,160
So we're going to show all of this through some examples.

13
00:00:54,180 --> 00:00:58,260
Keep in mind since we're dealing with postscripts here we're not going to be able to show these examples

14
00:00:58,320 --> 00:00:59,200
through the notebook.

15
00:00:59,280 --> 00:01:05,940
Instead we're going to need to do is use Sublime Text Editor or any other idea that works without PI

16
00:01:05,940 --> 00:01:09,240
files in order to create the scripts and folders.

17
00:01:09,240 --> 00:01:12,490
Let's get started by opening up Sublime Text Editor.

18
00:01:12,530 --> 00:01:16,200
All right so we're going to start off by creating two files here.

19
00:01:16,260 --> 00:01:22,860
The first one will save it to the desktop will say file new file or we can actually save on it currently

20
00:01:22,860 --> 00:01:25,350
have save as and on my desktop.

21
00:01:25,530 --> 00:01:30,230
I'm going to say this as my module not pi.

22
00:01:30,660 --> 00:01:31,880
So this is the module.

23
00:01:31,950 --> 00:01:37,440
And remember all the module is just the top high script being called in from another file.

24
00:01:37,440 --> 00:01:42,040
So let's create the other file say new file and this one we're going to save it.

25
00:01:42,120 --> 00:01:48,840
Also on the desktop in the same location as my model of that pie and I will say my program got pie and

26
00:01:48,900 --> 00:01:50,290
save that.

27
00:01:50,340 --> 00:01:56,580
So inside of my module what I'm going to do is just create a very simple function will say F and then

28
00:01:56,580 --> 00:02:06,680
say my func and all this does is it prints out hey I am in my module.

29
00:02:06,700 --> 00:02:09,840
Hi control s to save that.

30
00:02:10,080 --> 00:02:15,480
And then in my program this is another dot PI script and I want to use the code from my module.

31
00:02:15,660 --> 00:02:19,740
So here we can get the idea that if you have a really large script you're not going to put everything

32
00:02:19,740 --> 00:02:21,440
in one giant pie file.

33
00:02:21,450 --> 00:02:27,030
Instead it will be nice to split it up between multiple dot PI files which means you split up between

34
00:02:27,030 --> 00:02:31,910
different modules and then later we'll see how he can aggregate bunch of modules to create a package.

35
00:02:32,130 --> 00:02:42,090
So far we're going to do is in my program say from my module import and then we're going to import my

36
00:02:42,090 --> 00:02:43,980
funk and let me make sure I spell that right.

37
00:02:43,980 --> 00:02:49,600
OK it my underscore phunk and that means I can now call it here.

38
00:02:49,670 --> 00:02:51,610
So I'll say my phunk.

39
00:02:51,650 --> 00:02:52,690
I'm going to say this.

40
00:02:52,730 --> 00:02:55,470
And let's see if it works by going to our command line.

41
00:02:55,490 --> 00:02:57,120
So here I am now at my command line.

42
00:02:57,140 --> 00:02:59,450
Remember I save these files at my desktop.

43
00:02:59,510 --> 00:03:06,740
So let's CD to the desktop change directory and now I should be able to call Python and then call my

44
00:03:06,740 --> 00:03:11,730
program that PI enter and I see hey I and my module pi.

45
00:03:11,810 --> 00:03:17,870
So we were able to successfully call the other function from the other module the PI file into my program

46
00:03:17,870 --> 00:03:24,290
that pi and this is the very basic way of how you would actually format the PI scripts in order to not

47
00:03:24,290 --> 00:03:26,720
have everything in a single script.

48
00:03:26,720 --> 00:03:31,490
Now at a certain point even this is going to start to be too much and now you're going to want to aggregate

49
00:03:31,880 --> 00:03:36,890
modules together to form a package and that way you can have different folders for different packages.

50
00:03:36,920 --> 00:03:40,550
So let's now show you how to create a package's.

51
00:03:40,670 --> 00:03:47,090
So we're going to come back here to my program that pi in my module top PI and will open up and say

52
00:03:47,150 --> 00:03:53,460
new file and then we're going to save this Save As and here at my desktop.

53
00:03:53,460 --> 00:03:56,260
I'm going to do a few things.

54
00:03:56,330 --> 00:04:06,240
The first thing I'm going to do is create a new folder here and this folder will call it my main package.

55
00:04:08,410 --> 00:04:09,170
Enter.

56
00:04:09,410 --> 00:04:16,580
And then inside of this folder we're going to create another new folder and we'll call this sub package.

57
00:04:16,810 --> 00:04:20,010
So we're going to be creating lots of files and folders here.

58
00:04:20,120 --> 00:04:26,090
So right now we have at my desktop my module up high and I program that pie and I've also created my

59
00:04:26,090 --> 00:04:30,460
main package folder and inside of that I have my sub package.

60
00:04:30,560 --> 00:04:35,090
Now since these are just directories or folders we need to let Python be able to understand that we

61
00:04:35,090 --> 00:04:39,640
want to treat these directories not just as a normal directory but as an actual package.

62
00:04:39,680 --> 00:04:47,220
So to do that inside both my main package and sub package I'm going to have to create a file called

63
00:04:47,280 --> 00:04:56,530
underscore underscore in it I NIIT underscore underscore that PI save that and make sure you have this

64
00:04:56,830 --> 00:04:57,100
in it.

65
00:04:57,100 --> 00:04:59,410
Py file in your package.

66
00:04:59,410 --> 00:05:02,230
Now you don't actually need to write anything in this file.

67
00:05:02,260 --> 00:05:07,060
It just needs to be there so that when Python goes searching through these packages it understands that

68
00:05:07,060 --> 00:05:09,950
it's not just the normal directory it's an actual package.

69
00:05:09,970 --> 00:05:13,690
So we're going to create one more files a new file.

70
00:05:13,690 --> 00:05:21,100
Now we're going to say this under some package will create another underscore underscore in it underscore

71
00:05:21,100 --> 00:05:23,310
underscore that py file.

72
00:05:23,380 --> 00:05:25,560
And again this one is going to be blank as well.

73
00:05:26,420 --> 00:05:28,370
So we have both of these.

74
00:05:28,370 --> 00:05:32,720
Notice here how sublime is telling us which are referring to this one in my main package and this one

75
00:05:32,720 --> 00:05:33,640
in the package.

76
00:05:33,650 --> 00:05:34,580
So we're going to close these.

77
00:05:34,580 --> 00:05:36,380
We're not going to be working with them anymore.

78
00:05:36,470 --> 00:05:38,900
We just need to have them name there.

79
00:05:39,340 --> 00:05:39,780
OK.

80
00:05:39,860 --> 00:05:51,840
So I had my module and my program what I'm going to do now is say file new file and I will say save

81
00:05:51,840 --> 00:06:05,710
as an inside of my main package and I'm going to create some main script that PI save that and then

82
00:06:05,720 --> 00:06:08,160
we'll also do a file new file.

83
00:06:08,540 --> 00:06:12,100
And let's put another PI script instead of my sub package.

84
00:06:12,100 --> 00:06:20,630
So we will say file save as an inside of the sub package along that other in that file We're going to

85
00:06:20,810 --> 00:06:21,800
create the following.

86
00:06:21,800 --> 00:06:28,370
We will say my sub script that pi.

87
00:06:28,390 --> 00:06:34,660
So now I have this outside module some main script inside of that main package folder.

88
00:06:34,850 --> 00:06:39,630
This subscript inside of that sub package folder and then my main program.

89
00:06:40,030 --> 00:06:44,810
So we can do is we can start playing around with this all the way for my program.

90
00:06:44,860 --> 00:06:54,130
So what I'm going to do is inside of my subscript will say if than say sub report and have that print

91
00:06:54,130 --> 00:06:54,760
out.

92
00:06:55,190 --> 00:07:02,470
Hey I'm function inside my subscript

93
00:07:06,170 --> 00:07:11,330
control us to save that we're going to do a very similar thing here in some main script and we will

94
00:07:11,330 --> 00:07:16,210
say the E-F will call this report main

95
00:07:18,970 --> 00:07:32,010
and this will print out hey I am in some main script in main pack which say that as well.

96
00:07:32,910 --> 00:07:33,250
OK.

97
00:07:33,300 --> 00:07:35,820
So let's take a look at the rectory right now.

98
00:07:35,820 --> 00:07:38,780
We'll say open a file just so we can explore this.

99
00:07:38,810 --> 00:07:43,830
So at my desk right now I have this my module that PI won't really be playing with that anymore but

100
00:07:43,830 --> 00:07:49,330
instead I have this my program that pi and we can think of my program that pi as the main the PI script.

101
00:07:49,350 --> 00:07:53,540
I want to run and I want to organize my files into packages.

102
00:07:53,550 --> 00:07:58,260
So right now we've explored how to actually create a single module which is just the dot PI script and

103
00:07:58,280 --> 00:08:03,480
you can imagine we'd be importing from various modules so various up-I scripts that at a certain point

104
00:08:03,570 --> 00:08:06,400
it's going to be too much and we want to organize them into folders.

105
00:08:06,450 --> 00:08:11,540
So the way you would do that is you would have packages which are just directories that have this in

106
00:08:11,820 --> 00:08:12,440
the pie.

107
00:08:12,450 --> 00:08:15,260
So underscore underscore in an underscore underscore that pie.

108
00:08:15,390 --> 00:08:20,040
And that tells Python hey this actual directory it has a bunch of modules in it so you can call them

109
00:08:20,340 --> 00:08:23,320
using a certain syntax which we're about to show you.

110
00:08:23,340 --> 00:08:24,790
So here we have some main script.

111
00:08:24,930 --> 00:08:30,900
You can imagine I could have many more PI scripts are many more modules inside of this package and we

112
00:08:30,900 --> 00:08:34,740
can keep going with sub packages as long as they have this in that pie.

113
00:08:34,850 --> 00:08:37,230
And here you can see my subscript.

114
00:08:37,230 --> 00:08:43,350
So I'm going to cancel this and I'm going to do is inside my program that PI I've already know how to

115
00:08:43,350 --> 00:08:44,380
import from a module.

116
00:08:44,390 --> 00:08:50,910
But now let's realize how to import from a package to import from package what I can say is from my

117
00:08:51,060 --> 00:08:54,010
main package.

118
00:08:54,250 --> 00:08:57,340
I can either import the actual module itself.

119
00:08:57,490 --> 00:08:59,720
Remember the module in my main package.

120
00:08:59,770 --> 00:09:06,290
It was called some main script so we'll say some main scripts.

121
00:09:07,560 --> 00:09:16,900
And if I wanted to import from a sub package what I would say from my main package dot sub package and

122
00:09:16,900 --> 00:09:19,810
then I can import some module from there.

123
00:09:19,810 --> 00:09:29,290
So in this case it was called My subscript So this is now how you can organize not just into a single

124
00:09:29,350 --> 00:09:33,290
directory but you can actually organize into subdirectories as well.

125
00:09:33,310 --> 00:09:38,200
And this is the syntax for that you say dot the name of that sub package import and then import the

126
00:09:38,200 --> 00:09:38,910
modules.

127
00:09:38,990 --> 00:09:43,480
And if you want you can go even further and actually import only specific functions.

128
00:09:44,890 --> 00:09:46,640
So we'll show you how to do that in just a second.

129
00:09:46,660 --> 00:09:50,180
But let's actually call the functions from some main script.

130
00:09:50,290 --> 00:09:56,010
So from some main script right here we want to call the function that's inside that script so if we

131
00:09:56,020 --> 00:10:01,820
click back here we realized that that one is just called Report main.

132
00:10:01,960 --> 00:10:07,740
So coming back world say some main scrip report main close parentheses.

133
00:10:07,770 --> 00:10:12,510
So we're actually executing the function report Main which is under some main script module which is

134
00:10:12,540 --> 00:10:14,230
under my main package.

135
00:10:14,280 --> 00:10:18,480
Then we also want to if we look at my subscript that has the subreport.

136
00:10:18,600 --> 00:10:19,600
So let's run that as well.

137
00:10:19,620 --> 00:10:30,280
Will say my subscript call sub report open close princes So let's save that and then let's run my program

138
00:10:30,280 --> 00:10:31,800
that pi at the command line.

139
00:10:32,020 --> 00:10:33,550
So we can see what happens.

140
00:10:33,550 --> 00:10:40,150
All right so now here back on my top of the command line I will say Python call my program that pi and

141
00:10:40,150 --> 00:10:46,360
I hit enter and I see hey I am some main script in main package and then hey I'm a function inside my

142
00:10:46,380 --> 00:10:48,070
subscript and that's it.

143
00:10:48,070 --> 00:10:53,380
Now you've successfully not only created a module in the first part lecture but you also created packages

144
00:10:53,440 --> 00:10:54,990
and sub packages.

145
00:10:55,090 --> 00:11:00,640
And in general the idea here is just how do you organize various Paice scripts as you begin to grow

146
00:11:00,640 --> 00:11:01,840
better and better in Python.

147
00:11:01,840 --> 00:11:04,190
You're going to write more and more Python code.

148
00:11:04,300 --> 00:11:07,820
So at the beginning you'll create a couple of modules to all be in the same folder.

149
00:11:07,840 --> 00:11:08,820
No big deal.

150
00:11:08,980 --> 00:11:14,140
Then as it starts getting more and more unwieldy to have it all in the same folder all you need to do

151
00:11:14,140 --> 00:11:18,880
is create directories that have that special underscore underscore in it underscore underscore that

152
00:11:18,880 --> 00:11:19,830
py file.

153
00:11:19,840 --> 00:11:24,660
That just tells Python hey this is now a package and then allows you to run.

154
00:11:25,120 --> 00:11:25,970
What we see here.

155
00:11:26,020 --> 00:11:29,620
This my main package my main mean that's a package.

156
00:11:29,670 --> 00:11:30,130
OK.

157
00:11:30,340 --> 00:11:32,870
That's the basic idea of packages and modules.

158
00:11:32,920 --> 00:11:37,860
If you have any questions feel free to post the Kewney forums and we'll be happy to help you out there.

159
00:11:37,870 --> 00:11:38,830
I'll see you at the next lecture.
