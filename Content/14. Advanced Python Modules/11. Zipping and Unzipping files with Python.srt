1
00:00:05,240 --> 00:00:08,780
Welcome back, everyone, to this lecture on unzipping and zipping files.

2
00:00:09,230 --> 00:00:14,570
A zip file is just a compressed file, meaning you can take a bunch of files and compress them to safe

3
00:00:14,570 --> 00:00:16,760
space inside of a zip file.

4
00:00:17,010 --> 00:00:20,540
Where we're going to do here is learn how we can do this programmatically with Python.

5
00:00:20,990 --> 00:00:26,750
We will create a zip file, compress text files and then insert them into the zip file, close it and

6
00:00:26,750 --> 00:00:30,530
then we'll show you how to unzip the information back into a folder you're choosing.

7
00:00:30,890 --> 00:00:31,520
Let's get started.

8
00:00:31,820 --> 00:00:32,020
All right.

9
00:00:32,030 --> 00:00:33,650
Here I am in the Jupiter notebook.

10
00:00:34,070 --> 00:00:38,270
What I'm going to do is create a couple of text files for us to work with and compress.

11
00:00:38,780 --> 00:00:44,180
So the first method we're going to be showing you uses the zip file built in to Python.

12
00:00:44,540 --> 00:00:49,970
And later on, we'll show you how to do shell utilities to essentially archive an entire folder at once.

13
00:00:50,150 --> 00:00:55,100
But we'll start off by showing you how to zip individual files and compress them, but will first need

14
00:00:55,100 --> 00:00:56,540
to create those individual files.

15
00:00:57,050 --> 00:01:01,280
So I will say F is equal to open, let's say file one dot text.

16
00:01:02,770 --> 00:01:05,890
Open it with Mode W plus so we can write to it and create it.

17
00:01:07,330 --> 00:01:08,740
And then I'm going to say F right.

18
00:01:09,010 --> 00:01:13,570
And let's say one file and then F close.

19
00:01:14,290 --> 00:01:15,760
So there's our first text file.

20
00:01:15,880 --> 00:01:17,920
And if you're running this in the same directory, I am.

21
00:01:17,920 --> 00:01:20,950
And scroll down, you search the file one that text there.

22
00:01:21,040 --> 00:01:23,170
And if we open it up, we can see one file.

23
00:01:23,840 --> 00:01:25,000
And let's do this one more time.

24
00:01:25,090 --> 00:01:28,780
I'm simply going to copy and paste the code and create a second file.

25
00:01:28,960 --> 00:01:31,840
So we will say file to dot text.

26
00:01:31,990 --> 00:01:33,910
And let's write what's in it as.

27
00:01:35,020 --> 00:01:40,780
To file, so I have these two text files confirm that right here, file to that text.

28
00:01:40,930 --> 00:01:41,650
We're ready to go.

29
00:01:42,310 --> 00:01:47,260
So the next step is, let's say these were really large text files and I wanted to compress them because

30
00:01:47,260 --> 00:01:50,440
I want to send them in an email and maybe I want to do this programmatically.

31
00:01:50,800 --> 00:01:54,310
Well, what I can do is import zip file.

32
00:01:54,340 --> 00:01:57,970
This is built into Python and we can use it to compress files.

33
00:01:58,060 --> 00:02:02,440
Essentially, it creates a zip file and then you can compress individual files and insert them into

34
00:02:02,440 --> 00:02:03,070
the zip file.

35
00:02:03,640 --> 00:02:06,190
The first thing you do is you actually create the zip file first.

36
00:02:07,090 --> 00:02:13,060
So I will say Komp underscore file as my variable, which is just going to stand for compressed file.

37
00:02:13,690 --> 00:02:18,040
And the way we do this is we say zip file, dot, zip file.

38
00:02:18,130 --> 00:02:20,220
Notice the capitalized Z and F.

39
00:02:21,190 --> 00:02:23,590
Then you call whatever you want to call your dot zip file.

40
00:02:23,800 --> 00:02:27,040
So we'll call it comp underscore, file, dot, zip.

41
00:02:28,510 --> 00:02:29,860
And we're going to right to it.

42
00:02:30,040 --> 00:02:32,470
So we basically have the mode equal to W.

43
00:02:34,120 --> 00:02:35,470
So there's a compressed file.

44
00:02:35,650 --> 00:02:37,690
And if you come back to the photo you're working with.

45
00:02:38,050 --> 00:02:39,760
You'll notice you actually have a zip file here.

46
00:02:40,030 --> 00:02:41,500
Now, the zip file is empty.

47
00:02:41,860 --> 00:02:46,660
So what I'm going to do is I'm going to compress these text files and then insert them into the zip

48
00:02:46,660 --> 00:02:47,020
file.

49
00:02:47,800 --> 00:02:50,770
So the way the zip file library works is you essentially.

50
00:02:51,730 --> 00:02:56,500
Take your compressed file and then you write to it.

51
00:02:57,130 --> 00:02:58,540
So you say go ahead and grab.

52
00:02:58,600 --> 00:03:01,460
Let's grab file one dot text.

53
00:03:02,570 --> 00:03:06,050
And then the other thing to specify is the compression type.

54
00:03:06,110 --> 00:03:12,580
You're going to use and if you take a look at the library, the link online for the documentation will

55
00:03:12,590 --> 00:03:14,450
show you various compression methods.

56
00:03:14,480 --> 00:03:17,030
But the most standard one is zip deflated.

57
00:03:17,480 --> 00:03:20,090
So that means we're just going to say compress.

58
00:03:23,040 --> 00:03:28,860
Type is equal to zip, file, dot, zip, underscore, deflated.

59
00:03:28,950 --> 00:03:30,940
You should be able to use tab two autocomplete that.

60
00:03:32,220 --> 00:03:37,410
So what that has done is it's grab this text file, compressed it and return it to this zip file.

61
00:03:37,860 --> 00:03:41,910
And this whole time, the zip file is still open to us inside of Jupiter.

62
00:03:42,000 --> 00:03:44,460
So Python has a zip file open and it's working with it.

63
00:03:45,120 --> 00:03:48,300
Let's do the exact same thing for the second file.

64
00:03:48,510 --> 00:03:54,600
So we'll say file that right and I will grab file to that text.

65
00:03:55,320 --> 00:03:57,840
And then same as before, we'll use the same compression method.

66
00:03:58,890 --> 00:04:01,350
So we will copy this and paste this here.

67
00:04:02,310 --> 00:04:03,450
And I'll just like the first one.

68
00:04:03,540 --> 00:04:08,160
It's grab file to that text, compress it and written it into this that zip file.

69
00:04:08,190 --> 00:04:08,760
So that's it.

70
00:04:08,760 --> 00:04:12,840
Found that contains compressed versions of file one and file two.

71
00:04:13,800 --> 00:04:18,480
Once you're done using that file, simply close it by saying Komp file Duclos.

72
00:04:22,900 --> 00:04:27,040
OK, so now we have out here this Komp file, that zip.

73
00:04:27,400 --> 00:04:28,330
It's no longer empty.

74
00:04:28,390 --> 00:04:31,780
It actually contains the compressed versions of file one and file two.

75
00:04:32,590 --> 00:04:33,400
So what's the next step?

76
00:04:33,760 --> 00:04:38,350
Well, you're probably going to want to know how to extract the items from a zip file to do this.

77
00:04:38,680 --> 00:04:40,300
We simply create a variable.

78
00:04:40,360 --> 00:04:42,850
So I will call this zip object.

79
00:04:44,220 --> 00:04:48,120
And you call the zip file library, call zip file again.

80
00:04:48,750 --> 00:04:53,940
Notice the capitalization, if you point it to the zip failure going to extract from.

81
00:04:54,530 --> 00:04:56,850
So in this case, it's Komp file, dot, zip.

82
00:04:57,180 --> 00:04:59,970
So this hopefully feels similar to open up a normal text file.

83
00:05:00,560 --> 00:05:04,740
And in this case, since we're extracting stuff from it, I just want to open it up and read a method.

84
00:05:06,360 --> 00:05:08,370
Once you run that, you have two options.

85
00:05:09,540 --> 00:05:14,310
If you're looking for one specific file, you can call just the extract method.

86
00:05:14,820 --> 00:05:20,100
And here you basically have to say, I just want to extract, for instance, file one dot text or two.

87
00:05:20,130 --> 00:05:21,210
You want to extract everything.

88
00:05:21,700 --> 00:05:27,870
The more likely case, you're just going to say extract all and then you have to decide what the actual

89
00:05:27,870 --> 00:05:28,530
full there's going to be.

90
00:05:28,550 --> 00:05:30,270
So what path do you want to extract this to?

91
00:05:30,930 --> 00:05:32,160
So let's go ahead and call this.

92
00:05:33,250 --> 00:05:34,430
Extracted underscore.

93
00:05:34,630 --> 00:05:35,110
Contents.

94
00:05:36,800 --> 00:05:37,280
Run that.

95
00:05:37,730 --> 00:05:42,590
And now what's happened is we've taken the contents of the zip file in read mode and we've extracted

96
00:05:42,590 --> 00:05:47,620
them to a folder called Extract that underscore content, which exists in the same location as this

97
00:05:47,620 --> 00:05:48,380
Jupiter notebook.

98
00:05:48,830 --> 00:05:50,600
And if you ever need to confirm what location you're at.

99
00:05:50,720 --> 00:05:52,850
Just type pido d into a notebook.

100
00:05:53,450 --> 00:05:53,630
All right.

101
00:05:53,960 --> 00:05:55,280
So I come back up here.

102
00:05:55,670 --> 00:05:58,840
If you scroll up to the top, you should now see a folder called Extracted Content.

103
00:05:59,440 --> 00:06:05,180
Open it up and nicely are extracted and decompressed versions of file one that text and file to that

104
00:06:05,180 --> 00:06:05,450
text.

105
00:06:05,480 --> 00:06:06,260
So you open those up.

106
00:06:06,710 --> 00:06:08,060
And here you can see one file.

107
00:06:08,570 --> 00:06:09,290
Open up the other.

108
00:06:09,770 --> 00:06:10,970
You should see to file.

109
00:06:11,630 --> 00:06:11,840
All right.

110
00:06:12,410 --> 00:06:15,950
So that is the built in zip file library with Python.

111
00:06:16,700 --> 00:06:23,390
Now, keep in mind, typically, when you're doing things like compressing folders and files, you often

112
00:06:23,390 --> 00:06:26,330
just compress an entire folder and extract an entire folder.

113
00:06:26,810 --> 00:06:31,820
You usually are not compressing single text items like this over and over again.

114
00:06:32,060 --> 00:06:39,290
So what we want to do is if we just want to archive an entire folder or extract an entire folder, usually

115
00:06:39,290 --> 00:06:41,840
the Shell Utility Library is a better tool for that.

116
00:06:42,320 --> 00:06:46,700
So we've already played around a little bit with the Shell Utility Library, which we can import as

117
00:06:46,810 --> 00:06:48,680
S.H., you T.L..

118
00:06:50,540 --> 00:06:57,050
And what we can do here is essentially point out a directory that we want to turn into a zip file.

119
00:06:57,650 --> 00:07:03,620
Let's go ahead and turn this extracted contents folder into a zip file.

120
00:07:04,070 --> 00:07:08,270
So we're going to do is grab now an entire directory folder.

121
00:07:08,990 --> 00:07:12,980
So I'm going to say that my directory is it.

122
00:07:13,340 --> 00:07:15,910
So say DSR to zip.

123
00:07:16,300 --> 00:07:22,970
And let's give it the file path to this, which is going to be this guy, this full path here as a string,

124
00:07:23,600 --> 00:07:27,800
except there's one more file which is called extracted.

125
00:07:29,860 --> 00:07:30,430
Content.

126
00:07:30,790 --> 00:07:35,170
So what I'm doing, unlike before, where I had to individually add in these text files.

127
00:07:35,560 --> 00:07:36,970
I'm just gonna say, you know what?

128
00:07:37,000 --> 00:07:37,310
Go ahead.

129
00:07:37,420 --> 00:07:42,040
And we're going to compress this entire archive, extract extracted content.

130
00:07:42,790 --> 00:07:43,930
So let's go ahead and do that.

131
00:07:43,960 --> 00:07:44,860
We know the directory.

132
00:07:44,860 --> 00:07:45,420
Want to zip.

133
00:07:46,120 --> 00:07:50,350
Make sure your backslash is or forward slashes match up to what is written out in PWP.

134
00:07:51,190 --> 00:07:54,280
And then we're going to give an output file name.

135
00:07:55,840 --> 00:07:57,250
So we'll say output file name.

136
00:07:57,640 --> 00:07:59,110
Let's just say it's called.

137
00:07:59,140 --> 00:07:59,680
Example.

138
00:08:01,730 --> 00:08:04,160
And then we call Shell Utility.

139
00:08:05,410 --> 00:08:12,340
Dots make underscore archive, and this accepts the output file name.

140
00:08:12,460 --> 00:08:16,390
Essentially, where do you actually want to output this zipped version?

141
00:08:17,350 --> 00:08:20,650
And the second parameter, you'll notice, is called format.

142
00:08:20,980 --> 00:08:22,180
What format should it be in?

143
00:08:22,480 --> 00:08:24,880
It can be something like a zip file or a dot tar file.

144
00:08:25,300 --> 00:08:26,800
So let's go ahead and choose zip file.

145
00:08:28,510 --> 00:08:31,330
And then there's a third thing we have to choose is the directory.

146
00:08:31,630 --> 00:08:32,260
Two zip.

147
00:08:32,320 --> 00:08:34,970
So what are we actually zipping, which in this case was that.

148
00:08:34,990 --> 00:08:36,190
Extracted content folder.

149
00:08:36,700 --> 00:08:39,220
So we'll just say directory to it.

150
00:08:39,640 --> 00:08:41,760
So we had to put in that whole file path there.

151
00:08:43,060 --> 00:08:46,680
OK, so let's go ahead and run this notice now.

152
00:08:46,720 --> 00:08:51,550
There exists an example that zip file, and if we come back up here, we should be able to scroll down

153
00:08:51,670 --> 00:08:53,310
and I'll see example, dot, zip.

154
00:08:53,980 --> 00:08:59,560
So this is typically easier to use the shell utilities because it works with just an entire folder instead

155
00:08:59,560 --> 00:09:01,480
of having to do this one by one.

156
00:09:02,110 --> 00:09:04,390
So we were able to zip up an entire folder.

157
00:09:04,720 --> 00:09:09,210
Now, the last thing we need to show you is how to actually extract the contents from a zip file with

158
00:09:09,220 --> 00:09:10,030
shell utilities.

159
00:09:10,760 --> 00:09:12,040
That's a very similar process.

160
00:09:12,130 --> 00:09:17,050
You simply call Shell utilities, and instead of making an archive, you are now going to unpack and

161
00:09:17,050 --> 00:09:17,500
archive.

162
00:09:18,440 --> 00:09:23,170
And you'll notice here it takes the file name you wish to unpack, which is this zip file.

163
00:09:23,710 --> 00:09:26,020
So we'll go ahead and say example's it.

164
00:09:27,280 --> 00:09:27,730
Right here.

165
00:09:28,810 --> 00:09:33,340
Then the next parameter is what should the extract directory call it?

166
00:09:33,880 --> 00:09:37,290
So let's call this final unzip.

167
00:09:37,540 --> 00:09:39,520
Essentially, it'll create a folder called File Unzip.

168
00:09:40,190 --> 00:09:42,940
And then you have to say, what actual file type is this?

169
00:09:43,030 --> 00:09:44,080
So it's a dot zip file.

170
00:09:44,650 --> 00:09:46,780
So we pass in zip here.

171
00:09:47,200 --> 00:09:48,790
So that third parameter is format.

172
00:09:49,690 --> 00:09:50,680
We go ahead and run this.

173
00:09:51,520 --> 00:09:53,560
And now it's taken exampled zip.

174
00:09:53,950 --> 00:09:56,140
If it's in a different file, you'll have to or excuse me.

175
00:09:56,170 --> 00:09:56,800
Different folder.

176
00:09:57,070 --> 00:09:58,450
You have to provide the full file path.

177
00:09:58,480 --> 00:09:59,440
But we're impacting it.

178
00:09:59,480 --> 00:10:00,760
It's right here in this location.

179
00:10:01,240 --> 00:10:05,290
And if we come back up here, we shouldn't be able to scroll up and see final unzip.

180
00:10:05,860 --> 00:10:08,350
There's file one dot text and file to that text.

181
00:10:08,740 --> 00:10:11,710
OK, so usually you're probably going to be using shell utility.

182
00:10:12,070 --> 00:10:14,300
It's a lot simpler to use just with the name.

183
00:10:14,560 --> 00:10:18,880
The make archive, an entire folder and the unpacked archive on an entire folder.

184
00:10:19,480 --> 00:10:19,720
All right.

185
00:10:20,230 --> 00:10:21,220
That's it for this lecture.

186
00:10:21,400 --> 00:10:23,720
If you have any questions, feel free to post the Q&amp;A forms.

187
00:10:24,310 --> 00:10:25,240
I'll see you at the next lecture.
