1
00:00:05,460 --> 00:00:08,850
Welcome back to this series of lectures on regular expressions.

2
00:00:09,000 --> 00:00:09,980
And this is part one.

3
00:00:11,580 --> 00:00:16,860
We already know that we can actually search for sub strings within a larger string with the end operator.

4
00:00:17,310 --> 00:00:18,600
Here's a very simple example.

5
00:00:18,660 --> 00:00:25,530
I could check if dog is within a larger string of text by saying the string dog in my dog is great.

6
00:00:25,830 --> 00:00:27,330
In this case, to return back.

7
00:00:27,390 --> 00:00:27,750
True.

8
00:00:29,310 --> 00:00:31,860
Now, we can already see this has severe limitations.

9
00:00:32,160 --> 00:00:37,320
We need to know the exact strain we're looking for and we need to perform additional operations to account

10
00:00:37,320 --> 00:00:40,080
for things like capitalization and punctuation.

11
00:00:40,740 --> 00:00:45,780
So what if we only want to search for a pattern structure of a string we're looking for?

12
00:00:46,230 --> 00:00:48,650
That is to say, what if we know that we're looking for an email?

13
00:00:49,030 --> 00:00:51,270
We don't know the exact email we're looking for.

14
00:00:51,330 --> 00:00:55,920
We're just looking for all the emails within a document or we're looking for something like a phone

15
00:00:55,920 --> 00:00:59,820
number where we know the general structure of the way phone number is presented.

16
00:01:00,090 --> 00:01:03,540
But we don't actually know the exact phone number we're looking for.

17
00:01:04,860 --> 00:01:10,560
This is where regular expressions or regex for short allow us to search for general patterns in text

18
00:01:10,560 --> 00:01:10,890
data.

19
00:01:11,580 --> 00:01:13,460
For example, let's go back to that email.

20
00:01:13,950 --> 00:01:18,960
We know that a very simple email format could be something like user email, dot com.

21
00:01:19,470 --> 00:01:25,370
And we know in this case for this very particular domain, if I wanted to find all the emails within

22
00:01:25,380 --> 00:01:32,340
a very large document of text, I know I'm looking for some pattern of text at text dot com.

23
00:01:33,030 --> 00:01:40,140
So essentially what we're looking for is what we don't actually know exactly or precisely that is.

24
00:01:40,200 --> 00:01:44,400
I know there's going to be some text for the username and I know there's some sort of domain name.

25
00:01:44,700 --> 00:01:50,850
And then I have the things that I assume I do know in this case, I will assume that there's an app

26
00:01:51,000 --> 00:01:52,620
in between the user and the domain.

27
00:01:53,010 --> 00:01:55,800
And in this case, I assume the domain name ends in dot com.

28
00:01:56,190 --> 00:01:59,870
I know there's other domain names that I o you know, dot, x, y, z, et cetera.

29
00:02:00,180 --> 00:02:05,340
But for this particular pattern, let's say I'm only looking for emails that end in dot com.

30
00:02:06,210 --> 00:02:12,690
So what I can use regular expressions for is to construct a generalized pattern to search for something

31
00:02:12,690 --> 00:02:13,230
like this.

32
00:02:14,980 --> 00:02:20,110
So Python comes with this built in regular expressions, library or ar e for short, and you import

33
00:02:20,110 --> 00:02:25,600
it as our eat and the arty library allows us to create specialized patterned strings and then search

34
00:02:25,600 --> 00:02:27,040
for matches within text.

35
00:02:27,430 --> 00:02:32,380
And the main point I want to get across right now is the primary skill set for understanding regular

36
00:02:32,380 --> 00:02:37,000
expressions is really understanding the special syntax for these patterned strings.

37
00:02:38,090 --> 00:02:41,550
And I don't want you to feel like you need to memorize all these patterns.

38
00:02:41,880 --> 00:02:45,750
Regular expressions are notoriously difficult to memorize and understand.

39
00:02:46,280 --> 00:02:50,880
What you should really focus on is understanding how to look up the necessary information so you can

40
00:02:50,880 --> 00:02:55,890
quickly find the appropriate or correct patterns, especially if you don't use regular expressions that

41
00:02:55,890 --> 00:03:00,150
often you won't find yourself memorizing the specialized syntax codes.

42
00:03:00,510 --> 00:03:07,440
Instead, understand how to convert what you're looking for into a specialized pattern syntax.

43
00:03:07,710 --> 00:03:10,020
And then you can look up the necessary patterns.

44
00:03:10,080 --> 00:03:12,450
Quantifiers or identifiers that you need.

45
00:03:13,020 --> 00:03:19,080
So here's a simple example of what a generalized regular expression could look like.

46
00:03:19,470 --> 00:03:24,960
Let's imagine I'm looking for a phone number and I know that it's in this format somewhere within a

47
00:03:24,960 --> 00:03:26,160
very large document.

48
00:03:26,520 --> 00:03:32,070
I know it's going to have three numbers in parentheses, a dash and other set of three numbers, a dash

49
00:03:32,160 --> 00:03:33,480
and then a set of four numbers.

50
00:03:33,990 --> 00:03:36,630
An example, regular expression pattern could look like this.

51
00:03:36,990 --> 00:03:38,460
So let's focus on what's happening here.

52
00:03:39,300 --> 00:03:44,490
Notice that outside of the string there's an R and that basically informs Python.

53
00:03:44,550 --> 00:03:49,050
Especially when you're using the regular expression library that don't treat this as just a normal string.

54
00:03:49,260 --> 00:03:51,540
There's actually identifiers within this string.

55
00:03:51,930 --> 00:03:56,850
You'll notice that there's a bunch of back slashes which correspond to the individual identifiers.

56
00:03:58,390 --> 00:04:04,780
So these identifiers are essentially just placeholders, almost like wild cards, waiting for a match

57
00:04:04,810 --> 00:04:06,610
based off a particular data type.

58
00:04:07,000 --> 00:04:10,360
And in this particular case, Backslash D stands for digit.

59
00:04:10,840 --> 00:04:14,590
So this is essentially saying I'm looking for three digits in a row.

60
00:04:14,920 --> 00:04:18,430
It doesn't actually care what the digits are because we don't know what the digits are yet.

61
00:04:18,700 --> 00:04:21,190
We just know that they're going to be in this format.

62
00:04:21,790 --> 00:04:25,960
And then you'll notice the other strings that are present are the format strings themselves.

63
00:04:25,990 --> 00:04:27,370
I know there's going to be parentheses.

64
00:04:27,430 --> 00:04:30,220
I know there's going to be a dash and there's going to be another dash there.

65
00:04:30,280 --> 00:04:35,410
And those, you'll note, don't have a backslash associate of them because they're not an actual identifier

66
00:04:35,410 --> 00:04:36,460
for regular expression.

67
00:04:36,690 --> 00:04:38,710
We are actually looking for that exact string.

68
00:04:39,370 --> 00:04:41,890
So, again, we have these general identifiers.

69
00:04:42,460 --> 00:04:44,020
And then the exact strings we're looking for.

70
00:04:44,380 --> 00:04:47,770
And this in turn constructs a general regular expression pattern.

71
00:04:48,400 --> 00:04:50,530
Now, this is kind of those simplified version of one.

72
00:04:51,130 --> 00:04:52,900
They can begin getting more complicated.

73
00:04:52,960 --> 00:04:53,680
You'll see here.

74
00:04:53,980 --> 00:04:58,030
This is a regular expression pattern that actually solves the exact same problem as the one before.

75
00:04:58,360 --> 00:05:02,980
But now it's using things like quantifiers in order to reduce the amount of text you actually have to

76
00:05:02,980 --> 00:05:03,250
write.

77
00:05:03,640 --> 00:05:06,280
So this is essentially saying find three digits in a row.

78
00:05:06,610 --> 00:05:07,720
Then another three digits.

79
00:05:07,810 --> 00:05:09,040
And then another four digits.

80
00:05:09,130 --> 00:05:11,680
Now we're going to learn how to do this throughout this series of lectures.

81
00:05:13,130 --> 00:05:13,350
OK.

82
00:05:13,830 --> 00:05:18,270
So we're going to start off first in this particular lecture is focusing, focusing on how to use the

83
00:05:18,330 --> 00:05:21,690
regular expression library to search for patterns within texts.

84
00:05:21,930 --> 00:05:26,850
And then afterwards, we'll focus on understanding the actual regular expression syntax codes, those

85
00:05:26,850 --> 00:05:32,130
patterns that we just saw earlier, like backslash the identifiers, qualifiers, groupings and so on.

86
00:05:32,580 --> 00:05:33,840
OK, let's get started.

87
00:05:34,020 --> 00:05:35,340
I'll open up a Jupiter notebook.

88
00:05:35,970 --> 00:05:38,790
Let's begin by understanding how to search for basic patterns.

89
00:05:39,450 --> 00:05:48,020
You'll create some example texts saying something like the agents phone number is four zero eight five

90
00:05:48,240 --> 00:05:49,980
five five one, two, three, four.

91
00:05:51,710 --> 00:05:52,310
Call soon.

92
00:05:53,780 --> 00:06:01,760
As we just discussed, you could search for simple strings in the text, such as phone and text, and

93
00:06:01,760 --> 00:06:06,320
you would get back a boolean indicating true or false, is that actual string in the text.

94
00:06:06,830 --> 00:06:10,010
But now let's show the format for regular expressions to do this.

95
00:06:10,070 --> 00:06:13,280
We will import the regular expressions module r.

96
00:06:13,310 --> 00:06:13,610
E.

97
00:06:15,480 --> 00:06:19,620
And then we'll assign the pattern phone to a variable called pattern.

98
00:06:20,460 --> 00:06:23,490
So you will say that the string phone is equal to pattern.

99
00:06:24,240 --> 00:06:30,000
Then we can say are called the search function off of the regular expressions module.

100
00:06:30,480 --> 00:06:31,830
And then you pass in the pattern.

101
00:06:32,920 --> 00:06:36,880
And then you pass on the text and you get back this match object.

102
00:06:36,970 --> 00:06:39,040
And so notice how much more information is here.

103
00:06:40,850 --> 00:06:46,550
This match object will report back not just whether there was a match to the phone, but also where

104
00:06:46,550 --> 00:06:48,990
the actual index location spanned two.

105
00:06:49,070 --> 00:06:52,730
So it starts to index twelve and then ends the index 17.

106
00:06:53,570 --> 00:06:56,390
So now let's search for a pattern that we know is not in the text.

107
00:06:56,480 --> 00:06:59,460
So we'll create another pattern variable or really overwrite it.

108
00:07:00,110 --> 00:07:02,750
And then something like not in text, which we know isn't there.

109
00:07:03,530 --> 00:07:10,220
And if we search for the new pattern in our text, we get back none, which means we don't really get

110
00:07:10,220 --> 00:07:12,470
back anything because there is no match.

111
00:07:13,130 --> 00:07:18,590
So now that we've seen that this hour, that search function will take the pattern, scan the text and

112
00:07:18,590 --> 00:07:19,910
return a match object.

113
00:07:20,180 --> 00:07:25,040
And if no pattern is found, none is returned, which in the notebook setting just means that nothing

114
00:07:25,040 --> 00:07:26,540
is output to the cell.

115
00:07:27,140 --> 00:07:32,500
Let's take a closer look at this match object from before we're going to reset pattern.

116
00:07:32,840 --> 00:07:36,440
As phone and we're going to run this search again.

117
00:07:36,680 --> 00:07:37,340
We'll say.

118
00:07:39,280 --> 00:07:41,510
R e that search pattern and the text.

119
00:07:41,950 --> 00:07:44,290
And we're actually say this to the variable match.

120
00:07:44,320 --> 00:07:46,650
So we can explore this match object.

121
00:07:48,370 --> 00:07:53,020
So this is a regular expression match object and it has a lot of information that we can grab from it.

122
00:07:53,920 --> 00:07:58,240
We can call this band method in order to get the actual index location of the span.

123
00:07:59,750 --> 00:08:02,510
And then you could also ask for the start.

124
00:08:04,100 --> 00:08:07,520
Or the end in that's by themselves.

125
00:08:07,970 --> 00:08:14,630
And keep in mind, if we had multiple matches inside the string, we would unfortunately only get back

126
00:08:14,690 --> 00:08:15,650
the first match.

127
00:08:16,220 --> 00:08:22,460
So if I had a string that said my phone once, my phone twice.

128
00:08:23,930 --> 00:08:26,240
And then I tried running the same command as before.

129
00:08:26,430 --> 00:08:30,020
So they match our E search for phone.

130
00:08:32,280 --> 00:08:36,710
Inside of this new text, and then I check the match itself.

131
00:08:36,900 --> 00:08:41,130
It only turns back one span, three eight, because that's the first match right here.

132
00:08:41,730 --> 00:08:46,860
If I want to find multiple matches or all the matches, I can usually find all function instead.

133
00:08:47,550 --> 00:08:51,750
So I can say matches is equal to our E.

134
00:08:53,300 --> 00:08:54,080
Find all.

135
00:08:56,160 --> 00:08:59,600
And I will search again, phone in that text.

136
00:09:00,410 --> 00:09:05,300
And then if I check my matches, I get back this list of how many matches I had.

137
00:09:05,780 --> 00:09:11,060
So if we wanted to check comedy matches, there were I could get the length of this list.

138
00:09:11,590 --> 00:09:15,710
And if I want to get back actual match objects, then I use the iterator.

139
00:09:16,220 --> 00:09:20,880
So to show you what I mean by that, I can say for match in r e.

140
00:09:21,700 --> 00:09:22,250
Fine.

141
00:09:22,370 --> 00:09:23,960
And sort of find all I can say.

142
00:09:23,960 --> 00:09:24,450
Find it.

143
00:09:25,610 --> 00:09:27,650
And then pass on whatever pattern I'm looking for.

144
00:09:27,950 --> 00:09:31,090
So it could be looking for phone again and then passing the text.

145
00:09:31,100 --> 00:09:31,920
I want to search.

146
00:09:32,930 --> 00:09:38,810
And what this does is it iterates through this text and then returns each match object that's found.

147
00:09:39,230 --> 00:09:44,390
So just like before when we were using search, we returned back the first match object, essentially

148
00:09:44,390 --> 00:09:46,370
indicating where we first matched up.

149
00:09:46,850 --> 00:09:50,450
If we use find all, it just returns back a list of the strings themselves.

150
00:09:50,750 --> 00:09:56,450
If I want to kind of combine these two by iterating through the text object and then finding every single

151
00:09:56,450 --> 00:09:58,500
match object I use, find it.

152
00:09:59,030 --> 00:10:01,670
So you're going to need to essentially use a for loop with this to iterate.

153
00:10:02,200 --> 00:10:07,970
So we're saying for match in r e that find itor you pass in the pattern and the text you want to search

154
00:10:07,970 --> 00:10:08,360
for.

155
00:10:09,200 --> 00:10:10,250
And then you can print out.

156
00:10:11,230 --> 00:10:14,660
The actual match object, and then we get back the match objects.

157
00:10:14,750 --> 00:10:16,550
You can do whatever you want, these match objects.

158
00:10:16,570 --> 00:10:20,620
For instance, if you're just interested in the span's, you could call the span method off of it.

159
00:10:21,370 --> 00:10:23,440
Run that and return back to Span's.

160
00:10:24,100 --> 00:10:28,270
Now, if you wanted the actual text that matched, all you need to do is use the group method.

161
00:10:29,170 --> 00:10:30,580
So you could say here.

162
00:10:31,710 --> 00:10:36,120
Group that returns back, the actual text that matched up that you were searching for.

163
00:10:36,330 --> 00:10:38,200
Essentially what you get back when you use.

164
00:10:38,250 --> 00:10:38,910
Find it all.

165
00:10:39,450 --> 00:10:44,310
So far, we've been able to realize the critical functions of the regular expressions module.

166
00:10:44,460 --> 00:10:49,590
And that is the search function where, again, you just pass in a pattern and the text and it returns

167
00:10:49,590 --> 00:10:51,380
back the first match object.

168
00:10:51,780 --> 00:10:58,560
The find all function, which again, pattern text and returns back just the list of matches indicating

169
00:10:58,560 --> 00:10:59,550
the strings that matched.

170
00:10:59,910 --> 00:11:04,830
And then they find itor, which is essentially a combination of these two where you go through the entire

171
00:11:04,830 --> 00:11:11,190
string and you return back match objects for the actual pattern you're searching for in the text.

172
00:11:11,340 --> 00:11:14,670
Then you can call any method you want off of that match object.

173
00:11:15,030 --> 00:11:19,890
Well, we haven't discussed yet is the actual special regular expression syntax for general patterns.

174
00:11:20,190 --> 00:11:22,890
Right now, we've only been searching for basic strings.

175
00:11:23,220 --> 00:11:29,640
Up next, we're going to discuss how to build these special identifying regular expression pattern codes.

176
00:11:30,270 --> 00:11:31,410
I'll see you at the next lecture.
