1
00:00:05,410 --> 00:00:08,890
Welcome, everyone, to this lecture on the Python debugger.

2
00:00:10,400 --> 00:00:15,080
When trying to figure out what errors there are in your code, you've probably used the print function

3
00:00:15,170 --> 00:00:19,340
to try to track down the error by printing out variables within your Python script.

4
00:00:19,940 --> 00:00:25,820
Fortunately, Python comes with a built in debugger tool that allows you to interactively explore variables

5
00:00:25,850 --> 00:00:28,190
within Midde operation of your Python code.

6
00:00:28,760 --> 00:00:31,130
Let's check it out by hopping over to a Jupiter notebook.

7
00:00:31,970 --> 00:00:34,010
OK, here we are in the Jupiter notebook.

8
00:00:34,670 --> 00:00:40,400
First, what I want to do is to show you an example of an error and then how you would try debugging

9
00:00:40,400 --> 00:00:45,980
it with just using print statements versus how the Python debugger can be kind of a more powerful interactive

10
00:00:45,980 --> 00:00:46,580
tool for you.

11
00:00:47,120 --> 00:00:49,160
Let's begin by.

12
00:00:50,140 --> 00:00:54,580
Having, let's say, a list, so we'll say X is a list.

13
00:00:54,640 --> 00:00:55,390
One, two, three.

14
00:00:56,300 --> 00:00:57,340
Y, we'll have that.

15
00:00:57,340 --> 00:00:58,060
Just be two.

16
00:00:58,360 --> 00:00:59,510
And then Z.

17
00:01:00,340 --> 00:01:03,040
Let's have that equal to three in the same cell.

18
00:01:03,700 --> 00:01:04,440
I'm going to say.

19
00:01:04,630 --> 00:01:07,960
Result is equal to Y plus Z.

20
00:01:08,500 --> 00:01:09,710
And let's go ahead and say result.

21
00:01:09,820 --> 00:01:14,140
Two is equal to X plus Y.

22
00:01:15,590 --> 00:01:17,540
Clips that should be X.

23
00:01:17,870 --> 00:01:18,230
There we go.

24
00:01:18,830 --> 00:01:19,010
All right.

25
00:01:19,130 --> 00:01:24,800
So notice here this should induce an error because while I can add to integers together, it doesn't

26
00:01:24,800 --> 00:01:28,430
make sense for me to try to add a list with an integer.

27
00:01:28,500 --> 00:01:29,070
Doesn't make sense.

28
00:01:29,240 --> 00:01:30,500
Say, one, two, three.

29
00:01:30,500 --> 00:01:30,970
Has a list.

30
00:01:31,100 --> 00:01:31,730
Plus two.

31
00:01:32,290 --> 00:01:33,140
So that's not allowed.

32
00:01:33,290 --> 00:01:36,710
And if I run this, it reports back the type of error that's happening here.

33
00:01:36,710 --> 00:01:40,610
It says can only concatenate list, not integer to list.

34
00:01:40,670 --> 00:01:44,910
So I can add a list to lists through concatenation or a number to a number.

35
00:01:45,230 --> 00:01:48,140
But let's imagine your error is a little more complicated than this.

36
00:01:48,380 --> 00:01:51,860
You get some sort of error report, but you're still not sure what exactly is going on.

37
00:01:52,340 --> 00:01:57,980
Well, you could try to begin putting in print statements or print function calls, maybe try printing

38
00:01:57,980 --> 00:02:01,730
out the results on both to see if that helps.

39
00:02:02,090 --> 00:02:08,720
Maybe try printing out what Y is and you keep running this, but you'll notice that eventually the type

40
00:02:08,720 --> 00:02:09,680
error may show up.

41
00:02:09,800 --> 00:02:11,840
It shows the same type error information.

42
00:02:12,170 --> 00:02:14,570
Let's imagine that you still to understand what that error is.

43
00:02:14,890 --> 00:02:19,190
And at a certain point, you're just adding too many print functions to really try to hone in on what

44
00:02:19,190 --> 00:02:19,910
the error is.

45
00:02:20,450 --> 00:02:24,620
This is useful, however, because we did get to see at least that the first result printed.

46
00:02:24,680 --> 00:02:27,650
So we know the error some time after this.

47
00:02:28,010 --> 00:02:32,540
But we're going to do here is show you hope kiddo's the python debugger to set a trace.

48
00:02:32,960 --> 00:02:36,410
So I'm going to remove for now these print statements.

49
00:02:37,910 --> 00:02:46,190
Let's go ahead and just have these three variables and what I'll do and you sell is import PDB, the

50
00:02:46,190 --> 00:02:54,140
Python debugger, and then what I'm going to do is let's copy again these three variables and then I'll

51
00:02:54,140 --> 00:02:56,030
say result.

52
00:02:57,130 --> 00:03:02,500
One is equal to Y plus Z, which does work.

53
00:03:03,510 --> 00:03:09,770
And essentially saying, quote, We had last time result two is equal to Y plus X, which we know won't

54
00:03:09,770 --> 00:03:10,160
work.

55
00:03:10,640 --> 00:03:12,200
So you run this and you get your error.

56
00:03:12,860 --> 00:03:18,410
Now, what you could do is use print functions as we did before, or you could set a trace using python

57
00:03:18,410 --> 00:03:18,920
debugger.

58
00:03:19,370 --> 00:03:25,370
And the trace is essentially going to pause operations, mid script and then allow you to play with

59
00:03:25,370 --> 00:03:26,990
variables to understand what's going on.

60
00:03:27,350 --> 00:03:31,640
That way, you don't have to keep adding in print functions to try to figure out what result one is,

61
00:03:31,640 --> 00:03:36,500
what Y is, what Z is, because you can imagine that these variable definitions could be way further

62
00:03:36,500 --> 00:03:40,910
up along in your code and are easily readable like we have here in such a short script.

63
00:03:41,270 --> 00:03:46,520
So what I'm going to do is I'm going to set a price on debugger before my error.

64
00:03:46,730 --> 00:03:52,640
So note that type errors will report what line the error occurs on is occurring on line seven.

65
00:03:53,180 --> 00:03:56,090
And we're going to do here is right before this result.

66
00:03:56,090 --> 00:03:57,530
Two is equal to Y plus X..

67
00:03:57,890 --> 00:03:58,870
I'll set my trace.

68
00:03:59,000 --> 00:04:02,300
So you should be setting your trace before the airline.

69
00:04:03,140 --> 00:04:05,030
So let's go ahead and set our trace by saying.

70
00:04:05,990 --> 00:04:13,980
P DBI sent underscore trays, open closed princes, and let's see what this actually does, OK?

71
00:04:14,240 --> 00:04:21,920
So you just ran that and now it says Python debugger set trace and you'll notice says X, Y, Z, result

72
00:04:21,920 --> 00:04:22,340
one.

73
00:04:22,580 --> 00:04:27,950
And so far we haven't actually printed anything out, but we do know that Result two is going to give

74
00:04:27,950 --> 00:04:28,730
us an error.

75
00:04:29,240 --> 00:04:32,990
So what does the Python debugger allow us to do through this little interactive environment?

76
00:04:33,530 --> 00:04:38,600
Well, it allows you to actually explore and call variables at this point in time.

77
00:04:39,170 --> 00:04:44,840
So I know that I'm currently at this line right before result, too, because I set the trace there

78
00:04:45,320 --> 00:04:49,520
and then I can call variables like X and I'll report back what they are.

79
00:04:50,180 --> 00:04:56,090
So basically I've pauses operation mid script and then I can begin exploring what things are so I can

80
00:04:56,120 --> 00:05:02,090
then ask, well, what's result one right now, mid script, it says it's five and then what is Y two.

81
00:05:02,240 --> 00:05:03,740
So here I could then explore.

82
00:05:04,100 --> 00:05:07,250
Well at this point in time what is why I see that it's two.

83
00:05:07,670 --> 00:05:09,160
And at this point time what is X.

84
00:05:09,440 --> 00:05:10,280
I see it's a list.

85
00:05:10,520 --> 00:05:14,840
And hopefully this can then help you figure out what variables are interacting with each other.

86
00:05:15,170 --> 00:05:17,240
And often, sometimes a variable is none.

87
00:05:17,360 --> 00:05:21,800
And you try to interact with it and it's not able to perform operations because it doesn't have an actual

88
00:05:21,800 --> 00:05:22,910
value associated with it.

89
00:05:23,360 --> 00:05:24,840
And hopefully that would then help you figure out.

90
00:05:25,040 --> 00:05:29,720
Oh, I can see here I'm trying to add an integer to a list, and that's the source of my error.

91
00:05:30,320 --> 00:05:35,750
Keep in mind that Python debugger will still call on you, the user, to try to look up the error code.

92
00:05:36,080 --> 00:05:37,130
Understand what's going on.

93
00:05:37,490 --> 00:05:43,700
Its main tool is for you to figure out in a very large Python script what variables are assigned to

94
00:05:43,700 --> 00:05:49,400
what in order to help you debug, especially in mid operation, where at a certain point you've been

95
00:05:49,400 --> 00:05:53,210
at reassigning a variable to certain operations or certain outputs of functions.

96
00:05:53,480 --> 00:05:57,500
And you may not know clearly what it is at a certain point in time in your script.

97
00:05:57,920 --> 00:06:02,060
Python debugger essentially allows you to put a pause right in the middle of your script in order to

98
00:06:02,060 --> 00:06:04,190
explore variables, which is very useful.

99
00:06:04,490 --> 00:06:09,680
Instead of having to flood your script with print function statements in order, figure out what variables

100
00:06:09,740 --> 00:06:10,430
actually are.

101
00:06:11,470 --> 00:06:16,840
OK, and then once you're ready to quit the debugger, you can just type in Q here, hit enter and it

102
00:06:16,840 --> 00:06:18,910
will report back that you quit the debugger.

103
00:06:19,120 --> 00:06:19,750
So there you go.

104
00:06:20,350 --> 00:06:25,000
Now you can definitely check out the official documentation for the debugger comes with a lot more tools.

105
00:06:25,030 --> 00:06:28,180
But me, this one of setting the trace is its main functionality.

106
00:06:28,510 --> 00:06:33,620
So if you check out the lecture notebook, we have a link here into the Python debugger documentation.

107
00:06:34,000 --> 00:06:37,360
You can see here how they're setting traces and calling modules, etc..

108
00:06:37,900 --> 00:06:41,560
So, again, set trace is what you're mainly going to be using this for.

109
00:06:42,010 --> 00:06:43,450
But there's other options here.

110
00:06:43,480 --> 00:06:45,280
You can check out in case you're interested in.

111
00:06:45,400 --> 00:06:47,410
And there's other debugger commands as well.

112
00:06:48,100 --> 00:06:49,300
OK, thanks.

113
00:06:49,570 --> 00:06:50,980
And I'll see you at the next lecture.
