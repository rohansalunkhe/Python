1
00:00:05,660 --> 00:00:06,380
Welcome, everyone.

2
00:00:06,650 --> 00:00:11,600
It's time to test multiple skill sets that you just learned in the advanced module section of the course.

3
00:00:12,080 --> 00:00:14,570
In order to do that, we've set up a puzzle for you to solve.

4
00:00:14,990 --> 00:00:19,040
So let's go ahead and do a quick overview of the exercise puzzle notebook.

5
00:00:19,520 --> 00:00:22,430
And in the next lecture, we'll go over through an example solution.

6
00:00:23,000 --> 00:00:23,210
All right.

7
00:00:23,240 --> 00:00:25,310
Here I am under advanced python modules.

8
00:00:25,670 --> 00:00:28,040
If you scroll up, you should see number to zero eight.

9
00:00:28,160 --> 00:00:31,580
Another folder called Advanced Python Module Exercise.

10
00:00:32,030 --> 00:00:37,310
And then if you open that up, you'll notice that there's a puzzle as well as the solutions.

11
00:00:37,400 --> 00:00:40,850
And then this zip file called Unzip Me for instructions.

12
00:00:41,300 --> 00:00:46,160
We bring over the actual modules, exercise, puzzle, notebook first.

13
00:00:47,340 --> 00:00:47,580
OK.

14
00:00:48,150 --> 00:00:53,550
So for this puzzle, your goal is basically to solve and try to figure out what you need to do.

15
00:00:53,880 --> 00:00:56,160
So we're actually not going to give you too much guidance.

16
00:00:56,250 --> 00:00:58,950
Instead, basically figure out what you need to do.

17
00:00:59,220 --> 00:01:03,720
And your first clue is this zip file called Unzip Me for instructions.

18
00:01:04,020 --> 00:01:05,670
So basically unzip it.

19
00:01:05,760 --> 00:01:11,100
There should be a text file there for you to open up with instructions and then read the instructions

20
00:01:11,160 --> 00:01:12,930
and see if you can figure out what you need to do.

21
00:01:13,590 --> 00:01:18,450
Now, if you get stuck on where to start or what steps you should take, I did have a link here to a

22
00:01:18,450 --> 00:01:22,770
Google document that will give you step by step what you should be considering and what you should be

23
00:01:22,770 --> 00:01:23,130
doing.

24
00:01:23,430 --> 00:01:26,760
But I highly encourage you to basically try to figure it out on your own.

25
00:01:27,300 --> 00:01:30,510
Start by trying to unzip this file and then see where that takes you.

26
00:01:30,990 --> 00:01:32,070
Again, this is a puzzle.

27
00:01:32,310 --> 00:01:34,560
So really, there's not that much guidance we've set up for you.

28
00:01:34,770 --> 00:01:37,950
But if you do need it, there is this guide, hence link that you can click on.

29
00:01:38,480 --> 00:01:38,700
OK.

30
00:01:39,150 --> 00:01:40,380
That's all I will say for now.

31
00:01:40,800 --> 00:01:45,130
If you end up getting stuck and this guide and hints don't help you, check out the next lecture where

32
00:01:45,150 --> 00:01:47,190
we go step by step through a possible solution.

33
00:01:47,640 --> 00:01:48,150
I'll see you there.
