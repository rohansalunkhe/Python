1
00:00:05,240 --> 00:00:10,250
Welcome, everyone, to this lecture on the collections module, the collections module is built in

2
00:00:10,280 --> 00:00:11,070
to Python.

3
00:00:11,250 --> 00:00:17,030
Based Python, and it implements specialized container data types that are essentially alternatives

4
00:00:17,360 --> 00:00:20,750
to pythons built in containers that are just general purpose.

5
00:00:21,140 --> 00:00:24,230
So what I mean by a container is something like a dictionary or a tuple.

6
00:00:24,560 --> 00:00:26,600
We've already learned how to use normal dictionaries.

7
00:00:26,930 --> 00:00:32,660
But Counter has specialized dictionary objects or specialized tuple like objects.

8
00:00:32,960 --> 00:00:35,720
So we're going to learn about these specialized container types.

9
00:00:36,050 --> 00:00:41,570
You may not always need these specialized versions, but in certain use cases you may find them useful.

10
00:00:41,720 --> 00:00:43,190
Let's go ahead and open up a notebook.

11
00:00:43,820 --> 00:00:44,000
All right.

12
00:00:44,030 --> 00:00:45,530
Here I am in the Jupiter notebook.

13
00:00:45,920 --> 00:00:50,380
The first object I want to show you from the collections module is the counter class.

14
00:00:51,490 --> 00:00:57,170
We will import it by saying from collections import and then capital C counter.

15
00:00:58,430 --> 00:01:04,580
Let's imagine you have a situation where you have a list and then some unique values in the list.

16
00:01:04,760 --> 00:01:07,220
But there's also repeats for these unique values.

17
00:01:07,730 --> 00:01:10,940
So I'm just gonna make a bunch of ones, a bunch of twos, a bunch of threes and so on.

18
00:01:12,230 --> 00:01:16,460
Now, let's imagine I wanted you to get a count of each unique item in this list.

19
00:01:16,850 --> 00:01:18,890
For example, I want you to count how many ones are there?

20
00:01:18,980 --> 00:01:19,810
How many twos are there?

21
00:01:19,820 --> 00:01:20,750
How many threes are there?

22
00:01:21,350 --> 00:01:23,690
Well, you currently do have the skills to do this.

23
00:01:24,050 --> 00:01:28,580
You probably have to create a for loop and then have some sort of dictionary keeping track if you've

24
00:01:28,580 --> 00:01:30,050
already seen the number or not.

25
00:01:30,170 --> 00:01:33,090
And then if you've already seen it, go ahead and add plus one to the count.

26
00:01:33,410 --> 00:01:37,010
If you haven't seen it, create a new key and then add one again, et cetera.

27
00:01:37,490 --> 00:01:39,080
So that's actually quite complicated.

28
00:01:39,320 --> 00:01:42,650
But Counter can do this for us automatically if a single call.

29
00:01:42,980 --> 00:01:45,260
It's super useful in these sort of situations.

30
00:01:46,010 --> 00:01:48,530
Simply passing my list and to counter.

31
00:01:49,520 --> 00:01:55,130
And then you get this specialized counter object that has now counted the instances for each unique

32
00:01:55,160 --> 00:01:55,970
item in that list.

33
00:01:56,300 --> 00:01:57,770
So there's five instances of one.

34
00:01:58,250 --> 00:01:59,930
There's four instances of two.

35
00:02:00,320 --> 00:02:02,690
And there's seven instances of three.

36
00:02:03,200 --> 00:02:04,850
And this works if there were strings as well.

37
00:02:05,690 --> 00:02:07,280
So imagine we have some A's there.

38
00:02:08,540 --> 00:02:09,830
And then let's say.

39
00:02:10,840 --> 00:02:11,490
Three tens.

40
00:02:12,650 --> 00:02:20,260
Going to say counter my list and I can see here accounts that there's two a strings and then three instances

41
00:02:20,320 --> 00:02:20,800
of 10.

42
00:02:21,310 --> 00:02:27,640
Notice this looks very similar to a dictionary and counters technically a dictionary subclass that essentially

43
00:02:27,640 --> 00:02:29,650
just helps count cashable objects.

44
00:02:30,040 --> 00:02:34,900
So inside of it, elements are stored as dictionary keys and the counts of the objects are stored as

45
00:02:34,900 --> 00:02:35,470
the values.

46
00:02:35,820 --> 00:02:41,110
So kind of this specialized dictionary where the keys are always the object and then the value is always

47
00:02:41,110 --> 00:02:41,650
the count.

48
00:02:42,130 --> 00:02:43,480
Now this also works of strings.

49
00:02:43,990 --> 00:02:47,080
So I could say counter and then pass on some sort of string.

50
00:02:47,260 --> 00:02:53,530
So let's say a BBB, et cetera, run that then accounts the individual letters.

51
00:02:54,250 --> 00:02:56,650
Let's imagine I wanted to count the words in a sentence.

52
00:02:56,800 --> 00:03:01,240
Well, I already know this works of Fliss, so it could split the actual sentence.

53
00:03:02,140 --> 00:03:14,290
So let's create a sentence here saying how many times does each word show up in this sentence with a

54
00:03:14,290 --> 00:03:16,000
word or something like that?

55
00:03:16,750 --> 00:03:23,080
Well, I know I can't simply split this string on the white space to get a list that looks like this,

56
00:03:23,380 --> 00:03:26,380
which means I can then call counter on that list.

57
00:03:27,040 --> 00:03:29,830
And I have a count of each word I see here.

58
00:03:29,900 --> 00:03:31,000
Words showed up twice.

59
00:03:31,390 --> 00:03:37,540
And if I wanted to, I could do something like call lowercase on everything first, then split it in

60
00:03:37,540 --> 00:03:40,750
case I wanted to check for capitalized versions.

61
00:03:43,060 --> 00:03:48,160
So finally, let's go ahead to show you some common patterns that we will encounter when using the counter

62
00:03:48,160 --> 00:03:48,580
object.

63
00:03:49,150 --> 00:03:50,230
I will say.

64
00:03:52,250 --> 00:03:55,130
Let's say letters is equal to a string.

65
00:03:55,400 --> 00:04:01,040
Go ahead and put some A's in there, some B's in there and just put a bunch of C's and a bunch of these.

66
00:04:01,550 --> 00:04:02,030
There we go.

67
00:04:03,320 --> 00:04:04,970
And I'm going to create my counter object.

68
00:04:05,030 --> 00:04:07,380
I would just call it C notice.

69
00:04:07,480 --> 00:04:11,130
I'm actually assigning counter to the result here.

70
00:04:12,500 --> 00:04:13,620
And so I check out C.

71
00:04:14,390 --> 00:04:15,560
And it tells me the counts.

72
00:04:15,600 --> 00:04:19,460
So notice there's three A's for B's, eight C's and eleven D.

73
00:04:20,030 --> 00:04:21,860
I can do C dot tab.

74
00:04:22,430 --> 00:04:26,720
You'll notice there's a bunch of methods that look very similar to methods that we already know are

75
00:04:26,720 --> 00:04:27,060
available.

76
00:04:27,140 --> 00:04:30,290
Dictionary like keys or values.

77
00:04:30,590 --> 00:04:32,720
But there's also things like most common.

78
00:04:32,950 --> 00:04:36,230
So most common will actually just return back.

79
00:04:37,370 --> 00:04:39,980
The most common here and a list as a tuple.

80
00:04:40,460 --> 00:04:43,210
So it says D is the most common with eleven instances.

81
00:04:43,610 --> 00:04:45,940
C second most common of eight b..

82
00:04:46,010 --> 00:04:48,020
Third, most common for a fourth.

83
00:04:48,020 --> 00:04:48,920
Most common with three.

84
00:04:49,250 --> 00:04:50,090
And it would keep going.

85
00:04:50,360 --> 00:04:56,180
And it's up to you to provide here a number and of how many you actually want returned.

86
00:04:56,600 --> 00:04:58,430
So let's say I wanted the two most common.

87
00:04:59,180 --> 00:05:03,830
Then it would just return DNC if I wanted the three most common, then returns.

88
00:05:03,900 --> 00:05:05,390
D.C. M.B and so on.

89
00:05:05,840 --> 00:05:10,130
And we've actually provided for you in the lecture notebook that goes with this.

90
00:05:10,350 --> 00:05:11,540
We're going to bring it in over here.

91
00:05:12,110 --> 00:05:16,220
If you scroll, there's a common patterns when using the counter object.

92
00:05:16,520 --> 00:05:21,290
It's essentially a little table here that shows you just things that are common to do, like get a list

93
00:05:21,290 --> 00:05:26,840
of total of all counts, reset accounts, lists, unique items, convert to a set and so on.

94
00:05:27,140 --> 00:05:29,660
And then the actual code you would use in order to do that.

95
00:05:30,140 --> 00:05:31,550
So let's go and try one of these out.

96
00:05:31,670 --> 00:05:34,340
Let's imagine I want to list unique elements.

97
00:05:34,700 --> 00:05:40,400
Then I have to do is take my counter object, which you notice looks kind of like a dictionary here,

98
00:05:40,760 --> 00:05:47,090
but I can actually pass this into a list and then I essentially just get a list of all the keys.

99
00:05:47,120 --> 00:05:50,600
So all the unique values, lots different things we can do with the counter object.

100
00:05:50,930 --> 00:05:55,810
Obviously, it's really designed to be this workhorse for the case where you have to count unique elements.

101
00:05:57,790 --> 00:05:58,000
All right.

102
00:05:58,030 --> 00:06:01,720
We have two more things to show you from the collections module.

103
00:06:02,050 --> 00:06:04,450
The second one I want to show you is the default dictionary.

104
00:06:05,650 --> 00:06:12,730
So we'll say from collections, import defaults, the icy tea dictionary and we're going to do here

105
00:06:12,910 --> 00:06:15,040
is show you what happens in a normal dictionary.

106
00:06:15,820 --> 00:06:18,040
And then we'll show you what happens with a default dictionary.

107
00:06:18,820 --> 00:06:19,960
I'm going to create a dictionary.

108
00:06:20,320 --> 00:06:21,070
Quite simple.

109
00:06:21,790 --> 00:06:24,940
Has the value ay or excuse me, the Kia with the value.

110
00:06:24,970 --> 00:06:25,280
Ten.

111
00:06:26,170 --> 00:06:27,460
So I check out this dictionary.

112
00:06:27,980 --> 00:06:28,780
Eight to 10.

113
00:06:28,870 --> 00:06:29,470
No problem.

114
00:06:30,340 --> 00:06:34,360
And if I call the KIA, it returns back 10 as expected.

115
00:06:34,900 --> 00:06:39,700
But in a normal python dictionary, let's imagine I call the wrong key.

116
00:06:40,120 --> 00:06:41,130
So notice wrong.

117
00:06:41,350 --> 00:06:43,330
Definitely not a key yet in this dictionary.

118
00:06:43,990 --> 00:06:46,760
If I try to send a complaint saying, hey, here.

119
00:06:47,020 --> 00:06:49,030
This doesn't exist within this dictionary.

120
00:06:49,900 --> 00:06:54,910
In certain situations, especially when you're doing something like a for loop that you want to quickly

121
00:06:54,940 --> 00:07:00,130
adding keys that are not already present in your dictionary for whatever particular reason, you can

122
00:07:00,130 --> 00:07:01,900
use a default dictionary.

123
00:07:02,320 --> 00:07:07,090
And what a default dictionary does, the way it gets its name default dictionary is it will assign a

124
00:07:07,090 --> 00:07:11,830
default value if there is an instance where a key error would have occurred.

125
00:07:12,310 --> 00:07:18,220
So essentially, if you try to ask for a key that isn't present in a default dictionary, it will assign

126
00:07:18,220 --> 00:07:19,930
it with some default value.

127
00:07:20,470 --> 00:07:22,960
Let me show you how you can create this default dictionary.

128
00:07:23,380 --> 00:07:27,910
And the way we do this is by saying whatever your dictionary is gonna be.

129
00:07:27,910 --> 00:07:28,900
So call it again.

130
00:07:29,800 --> 00:07:31,480
In this case, we call it default.

131
00:07:32,670 --> 00:07:35,130
The icy tea for default dictionary.

132
00:07:35,520 --> 00:07:38,580
And we have to choose what the actual default value will be.

133
00:07:39,000 --> 00:07:43,680
So instead of returning a key error, they'll assign it our default value and we can do this through

134
00:07:43,680 --> 00:07:45,210
the use of a lambda expression.

135
00:07:45,270 --> 00:07:49,740
So we'll simply say lambda colon and then whatever you want the default value to be.

136
00:07:50,220 --> 00:07:53,370
For instance, let's say I want all default values to be zero.

137
00:07:54,210 --> 00:07:59,910
So it still behaves like a normal dictionary under normal circumstances where I can assign some key.

138
00:07:59,940 --> 00:08:01,260
So let's say it's the correct key.

139
00:08:01,740 --> 00:08:03,180
I'll assign at the value one hundred.

140
00:08:03,650 --> 00:08:07,600
And if I call it again, it turns back 100.

141
00:08:08,130 --> 00:08:12,330
But now let's put in a key that we know is not currently present.

142
00:08:12,870 --> 00:08:15,660
So in a normal dictionary, this will result in a killer.

143
00:08:16,110 --> 00:08:20,190
But maybe you have a situation where you want your script to keep running and you want to assign some

144
00:08:20,190 --> 00:08:21,480
default value like zero.

145
00:08:22,080 --> 00:08:26,310
So when we run this notice, we don't get that error anymore.

146
00:08:26,460 --> 00:08:29,580
Instead, it's automatically assigned it this default value.

147
00:08:30,640 --> 00:08:34,650
And now when I check out the dictionary, I see it's a default dictionary.

148
00:08:35,120 --> 00:08:37,350
It reports back that it has slammed the function here.

149
00:08:37,470 --> 00:08:38,310
That's what this is.

150
00:08:38,310 --> 00:08:39,300
Or the lambda expression.

151
00:08:39,870 --> 00:08:41,390
And then we have the wrong key.

152
00:08:41,520 --> 00:08:42,000
Zero.

153
00:08:42,090 --> 00:08:42,450
Correct.

154
00:08:42,480 --> 00:08:42,990
One hundred.

155
00:08:43,560 --> 00:08:46,740
So hopefully I can see that various use cases for a default dictionary.

156
00:08:47,070 --> 00:08:49,590
Perhaps you're trying to assign values to dictionary.

157
00:08:49,710 --> 00:08:51,900
But you don't know if everyone's present or not.

158
00:08:51,990 --> 00:08:54,540
So maybe you just have a default value of zero for them.

159
00:08:54,810 --> 00:08:57,270
If you encounter them and they'll have them in the dictionary.

160
00:08:58,150 --> 00:08:58,330
OK.

161
00:08:58,420 --> 00:08:59,580
So that's the default dictionary.

162
00:08:59,940 --> 00:09:04,640
The last specialized container object that I want to show you from the collections module is called

163
00:09:04,640 --> 00:09:05,910
a named tuple.

164
00:09:06,330 --> 00:09:11,730
So similar to the way a default dictionary tries to improve on just a standard dictionary by getting

165
00:09:11,730 --> 00:09:13,560
rid of this KiOR for default value.

166
00:09:13,920 --> 00:09:21,540
The name tuple tries to expand on a normal tuple object by actually having named indices.

167
00:09:21,660 --> 00:09:23,010
So let me show you what I mean by that.

168
00:09:24,390 --> 00:09:28,650
Let's create a standard tuple, say, my tuple, and it's just 10, 20, 30.

169
00:09:29,610 --> 00:09:35,700
So this is a very small tuple, which means if I wanted to grab, let's say, 10, I know I can see

170
00:09:35,700 --> 00:09:39,390
it's an index zero and then we have 10 returned to us.

171
00:09:39,420 --> 00:09:42,420
So I just need to look up the index, pass it in.

172
00:09:42,690 --> 00:09:43,720
And then there's 10.

173
00:09:44,430 --> 00:09:49,950
Now, in certain situations, you may have a very large tuple or you may not remember which value is

174
00:09:49,950 --> 00:09:50,880
at which index.

175
00:09:51,420 --> 00:09:59,550
So the name tuple is going to have not just a numeric connection to the values, but it will also have

176
00:09:59,640 --> 00:10:02,460
essentially a named index for that value.

177
00:10:02,850 --> 00:10:07,440
So instead of just calling it by zero, we could call it by some sort of string code.

178
00:10:08,370 --> 00:10:10,500
Let me show you how we can construct a named tuple.

179
00:10:11,810 --> 00:10:13,680
It actually kind of looks a lot like a class.

180
00:10:14,920 --> 00:10:21,040
So we'll say from collections import named tuple and let's construct our named tuple, which is almost

181
00:10:21,040 --> 00:10:27,000
like constructing a new object using object oriented programming or OPIS.

182
00:10:27,460 --> 00:10:29,190
So we'll say dog note, there's no prints.

183
00:10:29,190 --> 00:10:31,510
This here is equal to named tuple.

184
00:10:32,140 --> 00:10:35,320
And there's two main parameters, which is the type name.

185
00:10:35,590 --> 00:10:35,990
What type.

186
00:10:36,010 --> 00:10:39,950
This will be reported as the field names Pastan as a list.

187
00:10:40,600 --> 00:10:44,950
So let's go ahead and say that the type is going to be a dog.

188
00:10:46,940 --> 00:10:56,630
And then a dog will have an age of breed and a name almost like attributes in a class call.

189
00:10:57,470 --> 00:11:00,320
And now I can create instances of a dog object.

190
00:11:01,130 --> 00:11:02,240
So no dog here.

191
00:11:03,230 --> 00:11:06,380
And what I'm going to do is create a dog called Samey.

192
00:11:09,850 --> 00:11:12,730
And then I'm going to say that Sam is five years old.

193
00:11:14,340 --> 00:11:14,910
The breed.

194
00:11:16,400 --> 00:11:19,610
It's a husky and then the name.

195
00:11:21,330 --> 00:11:22,070
It's just Sam.

196
00:11:23,300 --> 00:11:25,530
So we have this variable, Sammy, if we check the type of it.

197
00:11:27,990 --> 00:11:30,310
Those are reports back that it's type dog.

198
00:11:30,350 --> 00:11:32,410
It doesn't report back that it's a named tuple.

199
00:11:33,040 --> 00:11:33,430
So.

200
00:11:34,910 --> 00:11:40,700
If I take a look at samey notice, it looks kind of like a mix between an object you would afraid of

201
00:11:40,730 --> 00:11:43,640
object oriented programming and a tuple itself.

202
00:11:43,790 --> 00:11:48,110
So essentially, it's kind of like this tuple here where you had 10, 20, 30.

203
00:11:48,830 --> 00:11:53,810
But notice now there's essentially an association with each value.

204
00:11:53,840 --> 00:12:01,070
So it's age four, five Huskie for Brede name for Sam, which means I can call these by saying Sammy

205
00:12:01,720 --> 00:12:02,690
Dot H.

206
00:12:03,230 --> 00:12:05,990
As if there were just attributes on this.

207
00:12:06,420 --> 00:12:07,790
So I can call for the breed.

208
00:12:09,490 --> 00:12:11,710
And then I can call for the name.

209
00:12:12,700 --> 00:12:19,390
Now notice if I say Sammy at index zero, I get back five, which was the same thing as calling Sammy

210
00:12:19,420 --> 00:12:20,080
that age.

211
00:12:20,620 --> 00:12:25,900
So you can imagine four very large tuples or tuples where he can't quite remember what values at which

212
00:12:25,960 --> 00:12:33,220
index it might be useful to be able to access them both by calling an index position such as zero or

213
00:12:33,250 --> 00:12:36,130
by calling it as if it was an attribute asking for the age.

214
00:12:36,430 --> 00:12:36,730
All right.

215
00:12:36,820 --> 00:12:37,960
So that's it for this lecture.

216
00:12:38,050 --> 00:12:41,500
You can check out the full documentation online on the collections module.

217
00:12:41,860 --> 00:12:45,340
These aren't things you're going to use for every single Python script you ever write.

218
00:12:45,700 --> 00:12:51,070
However, there may be certain situations where these can really help you and are more of a convenience

219
00:12:51,070 --> 00:12:51,530
for you.

220
00:12:51,730 --> 00:12:56,350
So you can check out the full docs online and I will see you at the next lecture.
