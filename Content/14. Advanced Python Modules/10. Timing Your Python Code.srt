1
00:00:05,190 --> 00:00:08,010
Welcome back, everyone, to this lecture on timing your code.

2
00:00:09,220 --> 00:00:13,930
As you learn more, Python, you're inevitably going to discover multiple solutions for a single task.

3
00:00:14,380 --> 00:00:18,070
And you're probably going to find yourself trying to figure out what was the most efficient approach.

4
00:00:18,520 --> 00:00:23,110
So if you have maybe three solutions to the exact same problem, you want to choose the fastest and

5
00:00:23,110 --> 00:00:23,980
most efficient one.

6
00:00:24,490 --> 00:00:29,800
An easy way to do this is the time your code's performance in this lecture will focus on three ways

7
00:00:29,830 --> 00:00:30,490
of doing this.

8
00:00:30,910 --> 00:00:35,380
One is to just simply track the time elapse before and after calling your function.

9
00:00:35,830 --> 00:00:37,810
You'll notice, however, this has its limitations.

10
00:00:37,870 --> 00:00:40,600
If your function is very fast for a small problem.

11
00:00:41,740 --> 00:00:46,060
Second way is to use the time that module ends is the most official way of doing this.

12
00:00:46,380 --> 00:00:48,490
And time at module is actually built into Python.

13
00:00:49,150 --> 00:00:52,240
And then the third way is a special time at magic function.

14
00:00:52,600 --> 00:00:55,590
However, this magic command only works for Jupiter notebooks.

15
00:00:55,630 --> 00:00:59,650
So if you're running this on something like PI charm, you'll just have to use the time at module or

16
00:00:59,690 --> 00:01:01,110
simply track the time elapsed.

17
00:01:01,570 --> 00:01:04,240
Let's explore all three of these methods in a Jupiter notebook.

18
00:01:05,200 --> 00:01:05,360
All right.

19
00:01:05,380 --> 00:01:06,850
Here I am at my Jupiter notebook.

20
00:01:07,360 --> 00:01:10,960
In order to test timing code, we actually need to write some code.

21
00:01:11,350 --> 00:01:15,400
So what I'm going to do is I'm going to write two functions that essentially return the same thing.

22
00:01:16,090 --> 00:01:21,880
If you give them a number such as 10 or let's actually make it smaller, like three, what they're going

23
00:01:21,880 --> 00:01:27,910
to produce is a list of the string versions of those numbers all the way.

24
00:01:27,940 --> 00:01:30,310
Basically, the integers up to three.

25
00:01:30,820 --> 00:01:33,330
So produce something like this or zero one two.

26
00:01:33,640 --> 00:01:34,930
If we decide to use range.

27
00:01:35,530 --> 00:01:35,690
OK.

28
00:01:35,800 --> 00:01:40,060
So let me go ahead and start by putting a function.

29
00:01:40,300 --> 00:01:42,340
We'll call this one, let's say function one.

30
00:01:43,930 --> 00:01:52,270
So a function one takes in some number N, which is the top number, and it's going to return and we'll

31
00:01:52,270 --> 00:01:53,800
do this through list comprehension.

32
00:01:54,520 --> 00:02:02,800
So we'll say for num in range up to N, go ahead and convert that to a string.

33
00:02:04,030 --> 00:02:10,540
So if we test out function one, we pass and 10, they're gonna go starting at zero, go all the way

34
00:02:10,540 --> 00:02:11,110
up to nine.

35
00:02:11,230 --> 00:02:12,730
Notice now these are strings.

36
00:02:13,090 --> 00:02:16,720
So it's just grabbing the number in range and then converting it to a string.

37
00:02:17,140 --> 00:02:20,080
Returns it all as a list through a list comprehension.

38
00:02:20,650 --> 00:02:23,980
Now, there's many ways of actually solving this problem in alternative ways.

39
00:02:24,520 --> 00:02:27,190
We'll call it func two or function two.

40
00:02:29,030 --> 00:02:31,640
Is to use the mapping function.

41
00:02:32,450 --> 00:02:37,880
So we'll say return list map and we'll map the string function.

42
00:02:39,200 --> 00:02:40,070
Two range.

43
00:02:40,720 --> 00:02:43,220
And if I call funk to.

44
00:02:44,700 --> 00:02:47,970
And say 10, then we get back the exact same result.

45
00:02:48,090 --> 00:02:53,790
So what this is doing is you get the same values from range end and then you convert them all to a string.

46
00:02:53,880 --> 00:02:59,190
By mapping the string function that each number that results in range, you convert that whole thing

47
00:02:59,250 --> 00:03:00,960
into a list by calling list on it.

48
00:03:01,500 --> 00:03:07,170
So you'll notice func one and func two are essentially two separate solutions to the exact same problem.

49
00:03:07,560 --> 00:03:08,790
We'll return back the same result.

50
00:03:09,360 --> 00:03:11,340
Now we want to know which ones is more efficient.

51
00:03:11,880 --> 00:03:18,120
Well, one way of doing this is to time your code simply by marking the start time and then calculating

52
00:03:18,120 --> 00:03:18,720
the difference.

53
00:03:18,990 --> 00:03:23,400
And you can do this by importing the time library.

54
00:03:24,030 --> 00:03:25,760
And that is built into Python.

55
00:03:25,860 --> 00:03:26,880
Nothing to install there.

56
00:03:27,390 --> 00:03:29,040
And essentially, we're gonna have three steps.

57
00:03:29,130 --> 00:03:32,970
Which is grab the current time before we run the code.

58
00:03:35,000 --> 00:03:39,950
Then we will run the code and then grab current time.

59
00:03:40,520 --> 00:03:46,970
After running code and then just take the difference to get the elapsed time.

60
00:03:47,900 --> 00:03:49,670
So let's start with grabbing the current time.

61
00:03:49,760 --> 00:03:51,440
This is actually quite easy at the time library.

62
00:03:51,530 --> 00:03:55,700
You simply say something like start time is equal to time that time.

63
00:03:56,980 --> 00:03:58,600
This just grabs from your operating system.

64
00:03:58,660 --> 00:03:59,410
What time it is.

65
00:03:59,980 --> 00:04:00,910
Then we run the code.

66
00:04:01,150 --> 00:04:03,130
So let's say results is equal to.

67
00:04:03,790 --> 00:04:07,140
We'll call function one and let's give it a larger end.

68
00:04:07,210 --> 00:04:09,790
So we'll give it an N of one million.

69
00:04:10,720 --> 00:04:16,870
So essentially going to pass in one million here and you get a list that's a million string digits long.

70
00:04:17,410 --> 00:04:18,780
Then we calculate an end time.

71
00:04:20,320 --> 00:04:21,370
So that will be.

72
00:04:22,350 --> 00:04:26,940
Time that time after running the code and then the total elapsed time.

73
00:04:28,420 --> 00:04:33,490
It's going to be equal to the end time, minus the start time.

74
00:04:35,290 --> 00:04:37,900
And then we can print out the elapsed time.

75
00:04:39,460 --> 00:04:40,870
OK, so let's go ahead and run this.

76
00:04:41,830 --> 00:04:44,020
And the elapsed time is in seconds.

77
00:04:44,080 --> 00:04:47,800
So this took zero point one seven nine eight seconds.

78
00:04:48,250 --> 00:04:53,910
Now, let's try this for funk, too, so we can try to function two here.

79
00:04:55,060 --> 00:04:55,690
Run this.

80
00:04:55,960 --> 00:04:59,530
And it looks like function two was slightly faster at zero point one six.

81
00:05:00,040 --> 00:05:01,960
Now, there are definitely limitations here.

82
00:05:02,380 --> 00:05:06,640
For one thing, if the function is faster than a tenth of a second.

83
00:05:07,030 --> 00:05:11,190
So let's go ahead and changes to just do this for the end is equal to 10.

84
00:05:12,010 --> 00:05:16,780
You'll notice elapsed time starts getting a really hard time trying to get that precise.

85
00:05:17,260 --> 00:05:18,880
So if we say now func two is ten.

86
00:05:19,330 --> 00:05:19,870
You run that.

87
00:05:19,870 --> 00:05:23,380
Func two ran so fast, it's actually less than a tenth of a second.

88
00:05:23,680 --> 00:05:26,770
So the lapse time, it's precision is actually not good enough.

89
00:05:27,130 --> 00:05:29,110
So if we changes to both, just one.

90
00:05:31,010 --> 00:05:31,950
And you run both of these.

91
00:05:32,010 --> 00:05:33,930
They essentially tell you the elapsed time was zero.

92
00:05:34,350 --> 00:05:37,830
So four very simple pieces of code or code that runs very fast.

93
00:05:38,160 --> 00:05:41,880
The precision may not be enough to actually even show anything on elapsed time.

94
00:05:42,390 --> 00:05:46,700
And you'll notice, even if it can show up on elapsed time by running laps.

95
00:05:46,950 --> 00:05:48,330
This one still running quite fast.

96
00:05:49,080 --> 00:05:52,110
So for something like 10000, let's try 10000 here.

97
00:05:52,500 --> 00:05:54,630
I don't know if this will show we'll run this.

98
00:05:55,080 --> 00:05:59,940
So for something like this, these are both running so fast, it becomes really hard to actually compare

99
00:05:59,940 --> 00:06:04,980
their performance and whether or not there's a true difference between function to and function one.

100
00:06:05,370 --> 00:06:10,080
It does look like function two is performing faster, but it's really hard to tell from this how much

101
00:06:10,080 --> 00:06:11,400
faster it's performing.

102
00:06:11,910 --> 00:06:16,420
So what we're going to do is instead use the time IT module, which is specifically designed to time

103
00:06:16,420 --> 00:06:16,830
code.

104
00:06:17,280 --> 00:06:22,010
So again, this is a very simple method you can use for larger blocks of code that are simpler and just

105
00:06:22,020 --> 00:06:23,970
want to get some total elapsed seconds.

106
00:06:24,300 --> 00:06:28,680
But we actually want to know down to the efficiency which one is taking less time.

107
00:06:29,130 --> 00:06:31,170
It's time to move on to the time IT module.

108
00:06:31,530 --> 00:06:31,770
OK.

109
00:06:33,160 --> 00:06:39,790
So we'll import time it in order to time our code with the time IT module, you'll notice.

110
00:06:39,820 --> 00:06:42,730
We will call time at that time.

111
00:06:43,510 --> 00:06:46,900
And if we do shift tap here, there's really three main things to look at.

112
00:06:47,200 --> 00:06:49,970
One is statement or s TMT.

113
00:06:50,500 --> 00:06:51,700
Another is set up.

114
00:06:52,390 --> 00:06:57,010
And then there's no number of times you actually want to run this code because time it is going to run

115
00:06:57,100 --> 00:07:01,990
the set statement code over and over and over again to figure out just how efficient it is.

116
00:07:02,410 --> 00:07:06,160
So we need to do is get anything that need to be set up beforehand.

117
00:07:06,670 --> 00:07:09,850
The code we actually want to test and then the number of times we want to test it.

118
00:07:10,330 --> 00:07:15,520
What's a little interesting about this is that statement and setup are actually passed in at strings.

119
00:07:15,640 --> 00:07:18,730
You don't actually pass in the raw functions themselves.

120
00:07:19,030 --> 00:07:20,260
So let me show you what I mean by that.

121
00:07:21,010 --> 00:07:23,440
We're going to say statement is equal to.

122
00:07:24,100 --> 00:07:29,140
And then here I will have triple quotes in order to have a multi-line string.

123
00:07:29,700 --> 00:07:31,990
And my statement, which is the actual code.

124
00:07:32,080 --> 00:07:35,950
I want to run, is going to be function one.

125
00:07:36,970 --> 00:07:39,970
And let's go ahead and just run it for the number one hundred.

126
00:07:40,420 --> 00:07:46,900
So recall, if I tried to do this with the previous method by just typing in one hundred, it's most

127
00:07:46,900 --> 00:07:51,730
likely going to be too fast for me to actually figure out what the difference is between these.

128
00:07:52,210 --> 00:07:55,540
But with time, it it's going to run this over and over and over again.

129
00:07:55,930 --> 00:07:57,670
And tell us which one is faster.

130
00:07:58,180 --> 00:08:00,520
So my statement is function one.

131
00:08:01,000 --> 00:08:02,400
Next is the setup.

132
00:08:03,040 --> 00:08:03,760
Same thing here.

133
00:08:04,130 --> 00:08:06,010
We're going to have triple quotes.

134
00:08:06,010 --> 00:08:11,980
To have a multi-line string and setup is essentially what code needs to be run before you call statement

135
00:08:12,040 --> 00:08:12,910
over and over again.

136
00:08:13,360 --> 00:08:18,520
And statement is calling function one, which means setup should have function one defined.

137
00:08:18,940 --> 00:08:21,580
So statement is what you're going to be calling over and over again.

138
00:08:21,860 --> 00:08:25,790
Setup just gets called once in order to set everything up for the statement code.

139
00:08:26,200 --> 00:08:30,850
So for the statement code, I need a setup function one in my setup, essentially defining it.

140
00:08:31,450 --> 00:08:32,320
So we'll come up here.

141
00:08:32,860 --> 00:08:34,090
I'm going to copy this.

142
00:08:35,320 --> 00:08:40,090
And then I'm going to paste it here in my setup note that indentation is still there.

143
00:08:41,440 --> 00:08:42,340
So I have my statement.

144
00:08:42,490 --> 00:08:43,250
I have my set up.

145
00:08:43,840 --> 00:08:50,650
Now it's time to time my code by saying time at that time it passed in my statement pass in my setup

146
00:08:51,160 --> 00:08:53,140
and we'll say the number of times I want to run this.

147
00:08:53,350 --> 00:08:55,210
We'll go ahead and run it hundred thousand times.

148
00:08:55,900 --> 00:08:58,180
So one hundred one, two, three thousand times.

149
00:08:59,080 --> 00:09:01,990
We run this and this will take a little bit of time on your computer.

150
00:09:02,230 --> 00:09:05,680
Obviously, if you make this number larger, it will take more, more time.

151
00:09:06,130 --> 00:09:10,750
But we'll really want to do is just run it enough times where we can see a discernable difference between

152
00:09:10,750 --> 00:09:11,560
the function calls.

153
00:09:12,130 --> 00:09:12,320
All right.

154
00:09:12,910 --> 00:09:19,780
So I can see here that this took one point five two or one point five three ish seconds to run this

155
00:09:19,780 --> 00:09:23,020
particular statement call hundred thousand times.

156
00:09:23,290 --> 00:09:24,310
Let's do the same thing now.

157
00:09:24,670 --> 00:09:26,510
But for function, too.

158
00:09:28,090 --> 00:09:29,440
So, again, triple quotes here.

159
00:09:29,860 --> 00:09:32,860
The statement I'm going to be running is function two.

160
00:09:33,550 --> 00:09:37,120
We want to make sure that it's a fair comparison by also passing in one hundred.

161
00:09:37,210 --> 00:09:38,320
Same as the previous statement.

162
00:09:38,800 --> 00:09:41,650
In fact, let's call this statement to just so we don't get confused.

163
00:09:42,730 --> 00:09:49,930
And then we'll call setup to equal two, because we need to setup the code and statement to scroll up

164
00:09:49,930 --> 00:09:54,100
here and say the F func to go ahead and copy and paste that.

165
00:09:56,080 --> 00:09:56,560
There we go.

166
00:09:57,760 --> 00:09:58,240
Run that.

167
00:09:58,690 --> 00:10:01,630
And now we have statement to and setup to ready to go.

168
00:10:02,260 --> 00:10:03,940
We'll go ahead and copy the code here.

169
00:10:05,110 --> 00:10:07,180
Want to make sure we run the exact same number of times.

170
00:10:07,210 --> 00:10:08,530
Otherwise, it's not a fair comparison.

171
00:10:08,980 --> 00:10:13,690
And here and to change this statement to and set up to go ahead and run this.

172
00:10:15,260 --> 00:10:19,420
You'll notice that function, too, does look to be performing faster.

173
00:10:19,840 --> 00:10:23,740
And if you want to get more clarity on this, you can go ahead and just run at more times.

174
00:10:23,890 --> 00:10:29,060
So in this case, I'm going to run this for one million times and I'll do the same thing for setup,

175
00:10:29,080 --> 00:10:29,370
too.

176
00:10:29,920 --> 00:10:31,870
I'll run this one million times.

177
00:10:33,190 --> 00:10:39,280
And now we can do a really fair comparison because after running the same functions one million times,

178
00:10:39,580 --> 00:10:43,600
you should know or have a discernable difference if one is much faster than the other.

179
00:10:44,110 --> 00:10:48,130
So you'll notice on my computer and keep in mind, depending on your hardware, it may take longer,

180
00:10:48,460 --> 00:10:56,770
but it looks like function one took about 14 seconds to run one million times and function two to eleven

181
00:10:56,770 --> 00:10:58,030
point three seconds.

182
00:10:58,180 --> 00:11:03,160
So over and over again, we see that function, too, is in fact the more efficient function.

183
00:11:03,610 --> 00:11:07,840
And as you increase this number, that difference should become even more clear.

184
00:11:08,530 --> 00:11:08,830
All right.

185
00:11:09,460 --> 00:11:12,610
Finally, because time it is a built in module of Python.

186
00:11:13,030 --> 00:11:19,990
Jupiter notebook has actually a magic method, so to speak, which essentially calls time it within

187
00:11:20,020 --> 00:11:20,440
a cell.

188
00:11:21,750 --> 00:11:26,970
So the way this works is you have two percentage signs and then call time it.

189
00:11:27,420 --> 00:11:29,970
And this needs to be the first line in the cell.

190
00:11:30,990 --> 00:11:35,040
And then right below it, you call the function you want to test.

191
00:11:35,520 --> 00:11:39,870
And essentially, what's nice about this is you don't need to provide any setup because Jupiter Notebook

192
00:11:39,900 --> 00:11:44,170
will use the previous cells that you've already defined as setup.

193
00:11:44,820 --> 00:11:46,440
So now I'm going to call function one.

194
00:11:47,400 --> 00:11:48,630
Let's pass in one hundred here.

195
00:11:49,470 --> 00:11:52,140
Run it and then I'll call the same thing.

196
00:11:53,690 --> 00:11:56,460
Time it is now for function two.

197
00:11:57,470 --> 00:12:01,550
One hundred and run this and note what it reports back.

198
00:12:01,820 --> 00:12:05,210
It essentially by default will run this for one hundred thousand loops.

199
00:12:05,270 --> 00:12:09,770
So it's running this for one hundred thousand loops and then it tells you the best of three.

200
00:12:10,070 --> 00:12:13,280
It took thirteen point seven microseconds per loop.

201
00:12:13,670 --> 00:12:15,440
And then function to its best.

202
00:12:15,450 --> 00:12:18,170
The three took eleven point two microseconds pollute.

203
00:12:18,560 --> 00:12:22,460
So here again, we're confirming that function two is running a bit faster.

204
00:12:23,000 --> 00:12:28,580
OK, you can check out the documentation online for time at module in the official Python docs for more

205
00:12:28,580 --> 00:12:29,150
information.

206
00:12:29,750 --> 00:12:30,170
Thanks.

207
00:12:30,320 --> 00:12:31,460
And I'll see at the next lecture.
