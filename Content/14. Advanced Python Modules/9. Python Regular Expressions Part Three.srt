1
00:00:05,300 --> 00:00:09,860
Welcome back to Part three of the regular Expressions lecture series in part three.

2
00:00:09,890 --> 00:00:13,070
We're going to continue learning about some syntax for regular expressions.

3
00:00:13,340 --> 00:00:18,020
Let's head over to Jupiter notebook to finish off our discussion on regular expression Syntex.

4
00:00:18,350 --> 00:00:23,180
We're going to discuss just a few more useful methods you may find yourself using out in the field.

5
00:00:23,930 --> 00:00:26,330
First off, let's discuss the OR operator.

6
00:00:26,840 --> 00:00:29,900
Sometimes you're going to want to search for multiple terms.

7
00:00:30,020 --> 00:00:31,760
You can use the OR operator to do this.

8
00:00:32,600 --> 00:00:38,300
For example, if we say are either search and then the pattern you want to search for is a string.

9
00:00:38,990 --> 00:00:40,310
For instance, just the string cat.

10
00:00:41,670 --> 00:00:43,960
And we say search the actual string.

11
00:00:44,590 --> 00:00:45,850
The cat is here.

12
00:00:46,720 --> 00:00:48,640
If you're on this, we get a match for Cat.

13
00:00:49,180 --> 00:00:53,740
But if it says the dog is here, as you expect, we don't get a match.

14
00:00:54,160 --> 00:00:57,670
What if we wanted to search for cat or dog in that case?

15
00:00:57,700 --> 00:00:59,470
We can use the pipe operator.

16
00:01:00,100 --> 00:01:01,750
So the pipe operator, it looks like that.

17
00:01:02,020 --> 00:01:06,040
And that stands for or meaning search for cat or dog.

18
00:01:06,520 --> 00:01:12,160
And then we'll get a match if there's a dog there or if there's a cat there.

19
00:01:12,610 --> 00:01:13,890
So that's the or operator.

20
00:01:14,500 --> 00:01:20,360
There's also a wild card operator and the wild card acts as a placement that will match any character

21
00:01:20,360 --> 00:01:20,980
or place they're.

22
00:01:22,550 --> 00:01:26,300
So let's find all to show you an example of the wild card operator.

23
00:01:26,810 --> 00:01:30,200
First, I'm just going to search for the string 80 or at.

24
00:01:32,440 --> 00:01:35,510
And I'll search for it in the string, the cat in the hat.

25
00:01:37,350 --> 00:01:38,070
SAT there.

26
00:01:39,660 --> 00:01:44,060
So notice I have three matches of at Cat Hat and set.

27
00:01:44,610 --> 00:01:49,210
But what if I actually wanted to grab the actual letter in front of T?

28
00:01:49,920 --> 00:01:56,970
In that case, I can provide a period and that stands for a wild card, meaning any thing here, any

29
00:01:56,970 --> 00:01:59,210
wild card, they're attached to a T.

30
00:01:59,790 --> 00:02:01,920
And then when I run this I get back.

31
00:02:01,980 --> 00:02:03,930
Cat hat and set.

32
00:02:04,470 --> 00:02:07,530
So again, the period can act as a wild card for you.

33
00:02:08,430 --> 00:02:11,880
So notice how we're really only matching the first three letters.

34
00:02:11,910 --> 00:02:15,610
But if we had something longer, for instance, such as Splatt.

35
00:02:16,140 --> 00:02:18,030
So the cat in the hat will say.

36
00:02:19,140 --> 00:02:20,270
Went splat.

37
00:02:20,490 --> 00:02:22,590
What I could do is add more wild cards.

38
00:02:23,760 --> 00:02:24,870
So now I have.

39
00:02:25,850 --> 00:02:29,990
Let's say up to three wildcards here, so the cat in the hat went splat.

40
00:02:30,230 --> 00:02:32,390
So s p l 80.

41
00:02:32,750 --> 00:02:37,400
But keep in mind, these wild card characters are also going to grab other letters and other spaces.

42
00:02:37,790 --> 00:02:40,760
So if I were to run this, I get back.

43
00:02:40,910 --> 00:02:42,020
E space cat.

44
00:02:42,380 --> 00:02:43,220
E space hat.

45
00:02:43,310 --> 00:02:44,680
And then the full splat.

46
00:02:45,200 --> 00:02:50,540
And the reason I'm grabbing E space is because the wildcard characters are counting the stuff that comes

47
00:02:50,540 --> 00:02:52,320
before this first letter.

48
00:02:52,340 --> 00:02:53,900
So it's also counting this space.

49
00:02:53,960 --> 00:02:58,970
And this E as well as the space and this E for the cat and the hat.

50
00:02:59,420 --> 00:03:03,370
So keep that in mind as you use wildcard characters, the period.

51
00:03:03,770 --> 00:03:07,760
If you want more control, you'll want to use the character identifiers, as we discussed earlier.

52
00:03:08,630 --> 00:03:11,060
Again, period, essentially just a wildcard.

53
00:03:11,960 --> 00:03:12,260
All right.

54
00:03:12,800 --> 00:03:17,630
Let's finally discuss starts with and ends with so we can use this.

55
00:03:19,140 --> 00:03:20,940
This carrot symbol show in just a second.

56
00:03:20,970 --> 00:03:23,010
We'll say are, eat, find all.

57
00:03:24,360 --> 00:03:27,420
And if I want to find everything, that starts with a number.

58
00:03:28,400 --> 00:03:33,080
What I can do is indicate this carrot symbol or two power symbol.

59
00:03:33,110 --> 00:03:33,890
Let's just shift.

60
00:03:33,980 --> 00:03:36,860
Number six and then type in what I'm looking for.

61
00:03:37,310 --> 00:03:42,470
So what this indicates is that the string I'm searching through starts with a number.

62
00:03:44,160 --> 00:03:46,860
So I'll say one is a no.

63
00:03:47,870 --> 00:03:55,060
And this returns a match for one, because the entire text itself starts with the actual digit.

64
00:03:55,480 --> 00:04:00,820
Keep in mind, this is only for the entire text, not for a number randomly inside of this.

65
00:04:01,180 --> 00:04:07,990
So if I say the two is a number, I'm not going to get any matches here because it's only searching

66
00:04:07,990 --> 00:04:10,600
if the entire text itself starts off a number.

67
00:04:11,080 --> 00:04:14,760
So if I say two is a number, then I find my match for two.

68
00:04:15,520 --> 00:04:17,470
Similarly, I can say ends with.

69
00:04:17,700 --> 00:04:23,200
And the way you do that is instead of a carrot symbol, you just at the end of this pattern put a dollar

70
00:04:23,200 --> 00:04:25,000
sign and that says ends with.

71
00:04:25,480 --> 00:04:28,090
So if I say two is a number, I don't get a match.

72
00:04:28,420 --> 00:04:34,180
But if I say the number is two, then I do get a match.

73
00:04:34,330 --> 00:04:36,490
So again, dollar sign four ends with.

74
00:04:37,640 --> 00:04:41,570
And then this carrot or a power to sign for starts with.

75
00:04:42,650 --> 00:04:47,630
And then there's also ways you can use exclusion so to exclude characters.

76
00:04:47,990 --> 00:04:52,490
We again, we use this symbol, but we use it in conjunction with a set of brackets.

77
00:04:52,610 --> 00:04:57,680
So let's make a couple of cells here so we don't get confused by the earlier syntax.

78
00:04:57,740 --> 00:04:59,900
I'm going to create a phrase called.

79
00:05:00,650 --> 00:05:03,290
There are three numbers.

80
00:05:04,400 --> 00:05:05,230
Thirty four.

81
00:05:05,330 --> 00:05:07,750
Now, the number inside five.

82
00:05:08,000 --> 00:05:08,420
This.

83
00:05:10,500 --> 00:05:11,160
Sentence.

84
00:05:11,490 --> 00:05:11,640
OK.

85
00:05:11,700 --> 00:05:16,230
So here I have a phrase and it seems like just random numbers placed inside this sentence.

86
00:05:17,490 --> 00:05:23,760
What I want to do is find or get back everything that isn't a number in this sentence.

87
00:05:23,790 --> 00:05:27,450
So I want to exclude digits or exclude numbers.

88
00:05:28,020 --> 00:05:29,910
The format for that is the following.

89
00:05:30,510 --> 00:05:33,030
You'll see your pattern is equal to.

90
00:05:34,010 --> 00:05:39,730
And use inside of square Brett brackets, just like you would for a kind of python list.

91
00:05:40,520 --> 00:05:42,170
Say whatever you want to exclude.

92
00:05:42,290 --> 00:05:44,090
So in this case, I want to.

93
00:05:45,100 --> 00:05:46,690
Exclude digits.

94
00:05:47,110 --> 00:05:48,610
So look at the syntax here.

95
00:05:48,670 --> 00:05:51,040
I have square braces or square brackets.

96
00:05:51,520 --> 00:05:54,630
This symbol and then backslash lowercase D..

97
00:05:54,730 --> 00:05:57,280
So that just says exclude any digits.

98
00:05:57,310 --> 00:05:58,860
So anything that matches with this character.

99
00:05:58,870 --> 00:05:59,650
Right, that the fire.

100
00:06:01,180 --> 00:06:02,710
And then if I say find all.

101
00:06:05,070 --> 00:06:05,710
Pattern.

102
00:06:06,770 --> 00:06:07,950
And I put phrase here.

103
00:06:08,460 --> 00:06:13,020
I'm going to get back a list of every single character that was not a number.

104
00:06:13,620 --> 00:06:14,100
So I see.

105
00:06:14,120 --> 00:06:19,110
There are and then there's no number there numbers inside this sentence.

106
00:06:19,800 --> 00:06:27,330
And what we can do is if we actually want to get the words back together, I can add a plus sign to

107
00:06:27,330 --> 00:06:31,890
the end of this because remember a plus sign if we look back at our quantifiers.

108
00:06:32,070 --> 00:06:34,280
It just says occurs one or more times.

109
00:06:36,850 --> 00:06:40,270
So this is a nice way of free expression to quickly remove things.

110
00:06:40,720 --> 00:06:42,020
So let's run these lines again.

111
00:06:42,760 --> 00:06:47,350
And I can see there are excluded, essentially these groups of numbers.

112
00:06:47,680 --> 00:06:50,050
There are numbers inside this sentence.

113
00:06:50,770 --> 00:06:51,020
All right.

114
00:06:51,220 --> 00:06:53,770
So why would you use this exclusion syntax?

115
00:06:54,130 --> 00:06:57,880
This is a really common way to get rid of punctuation from a sentence.

116
00:06:59,330 --> 00:07:03,650
So we'll say test, underscore phrase is equal to.

117
00:07:04,250 --> 00:07:10,160
This is a string exclamation point, but it has punctuation.

118
00:07:10,310 --> 00:07:10,910
Period.

119
00:07:11,420 --> 00:07:12,950
How can we remove it?

120
00:07:13,430 --> 00:07:14,150
Question mark.

121
00:07:15,460 --> 00:07:18,070
Well, we can do is the fine, we'll say, Ari.

122
00:07:18,810 --> 00:07:19,000
Fine.

123
00:07:19,400 --> 00:07:19,780
All.

124
00:07:20,990 --> 00:07:24,890
And inside of this, we're going to say.

125
00:07:26,380 --> 00:07:27,490
Square brackets.

126
00:07:28,640 --> 00:07:32,400
This symbol and now I can add things I want to exclude.

127
00:07:32,870 --> 00:07:38,060
So I'm going to just put him in the same order so I can say I want to exclude an exclamation point.

128
00:07:38,450 --> 00:07:40,100
I want to exclude a period.

129
00:07:40,160 --> 00:07:42,710
And I want to exclude a question mark.

130
00:07:42,980 --> 00:07:44,390
And then I can add a plus sign.

131
00:07:44,750 --> 00:07:47,000
So I just want to exclude those wherever they occur.

132
00:07:47,480 --> 00:07:48,920
And then we'll say test.

133
00:07:49,750 --> 00:07:50,470
Phrase.

134
00:07:51,370 --> 00:07:52,440
And now I get back the list.

135
00:07:52,470 --> 00:07:53,280
This is a string.

136
00:07:53,370 --> 00:07:55,980
Notice how it's essentially being split on any punctuation.

137
00:07:56,340 --> 00:07:57,450
But it has punctuation.

138
00:07:57,480 --> 00:07:58,590
How can we remove it?

139
00:07:58,950 --> 00:08:00,990
So there's no longer any punctuation there.

140
00:08:01,890 --> 00:08:07,980
And something else we can do is add a space here so that it will end up removing the spaces.

141
00:08:09,300 --> 00:08:16,350
And then I get back a list of all the words, meaning if I say this is clean.

142
00:08:20,900 --> 00:08:24,060
Then how clean is all these words?

143
00:08:24,300 --> 00:08:25,530
And I can then join them.

144
00:08:26,370 --> 00:08:27,450
So I will say.

145
00:08:28,710 --> 00:08:32,820
Join clean and I will join them all with the space itself.

146
00:08:33,510 --> 00:08:34,200
And then I get back.

147
00:08:34,230 --> 00:08:35,960
This is a string, but it has punctuation.

148
00:08:35,970 --> 00:08:36,930
How can we remove it?

149
00:08:37,470 --> 00:08:42,420
So these are some more complex tasks you can use with regular expressions.

150
00:08:42,930 --> 00:08:46,380
And obviously you can see here how this quickly can get very hard to read.

151
00:08:46,710 --> 00:08:52,110
So always break down regular expression pattern codes in order to try to help yourself understand what's

152
00:08:52,110 --> 00:08:52,860
actually happening.

153
00:08:54,990 --> 00:08:58,860
We're going to show you two more useful things you can use for regular expressions.

154
00:08:58,980 --> 00:09:03,570
And one of them is actually these brackets are braces here, these square brackets.

155
00:09:03,900 --> 00:09:09,270
And that actually is basically just for grouping things together in the context of this carrot symbol.

156
00:09:09,330 --> 00:09:11,610
Essentially says group these things for exclusion.

157
00:09:11,970 --> 00:09:14,130
But you can also use them grouping for inclusion.

158
00:09:14,850 --> 00:09:16,590
So let's show a couple examples of that.

159
00:09:17,160 --> 00:09:20,160
We're going to create some text that says.

160
00:09:21,260 --> 00:09:22,670
Only fined.

161
00:09:24,950 --> 00:09:30,350
The hyphen Dasch words in this sentence.

162
00:09:31,220 --> 00:09:36,820
But you do not know how long dash ish.

163
00:09:36,920 --> 00:09:37,580
They are.

164
00:09:40,130 --> 00:09:45,470
OK, so we have this text and basically the main point is text is that I have hyphen Dasch words.

165
00:09:45,620 --> 00:09:47,930
And then later on else have hyphen Dasch words.

166
00:09:47,980 --> 00:09:54,350
But note here how I don't know how many letters are before and how many letters are after this dash.

167
00:09:54,740 --> 00:09:57,950
So my main task is given this piece of text.

168
00:09:58,370 --> 00:10:02,450
Go ahead and find words to have a hyphen or a dash in the middle of them.

169
00:10:02,990 --> 00:10:03,800
So how would I do that?

170
00:10:04,310 --> 00:10:12,320
Well, the general pattern idea would be a group of alphanumeric words or characters dash and then some

171
00:10:12,320 --> 00:10:14,210
other group of alphanumeric characters.

172
00:10:15,220 --> 00:10:20,840
So the way we do that is by filling out the following pattern, the pattern is going to be.

173
00:10:23,420 --> 00:10:24,190
Braces.

174
00:10:24,300 --> 00:10:28,080
And then inside these braces, I will say backslash w.

175
00:10:29,640 --> 00:10:31,260
And then I'll add a plus sign there.

176
00:10:31,770 --> 00:10:37,560
And what this indicates is just a group of backslash lowercase W..

177
00:10:37,980 --> 00:10:43,410
So remember, back from our character identifiers, lowercase WS, just an alphanumeric.

178
00:10:43,500 --> 00:10:46,020
So it's basically looking for a group of alpha numerics.

179
00:10:47,360 --> 00:10:51,350
And remember, the plus was essentially saying occurs one or more times.

180
00:10:54,400 --> 00:10:56,710
So we can check for this pattern.

181
00:10:56,950 --> 00:10:59,410
We'll say are either find all.

182
00:11:01,710 --> 00:11:08,250
And then we'll pass in the pattern and pass in the text, and it finds back any groups of Alphen America's.

183
00:11:09,480 --> 00:11:14,670
Now, if I want to find the hyphenated words, I'll say a group of alphanumeric stickers one or more

184
00:11:14,670 --> 00:11:15,240
times.

185
00:11:15,750 --> 00:11:16,470
Dash.

186
00:11:17,540 --> 00:11:21,620
And then another group of alpha numerics that occurs one or more times.

187
00:11:22,920 --> 00:11:25,230
And then I can get back the actual hyphenated words.

188
00:11:26,280 --> 00:11:29,010
Now, if we were to remove these actual braces.

189
00:11:30,250 --> 00:11:33,460
So that it looks like this, you'd get back the same results.

190
00:11:33,670 --> 00:11:39,550
However, some programmers, because this looks hard to read, they like to separate groups.

191
00:11:41,360 --> 00:11:47,630
Using this brace notation and the brace notation also allows you to kind of combine things together.

192
00:11:47,960 --> 00:11:52,160
So maybe you're looking for a word and then some digits.

193
00:11:52,160 --> 00:11:56,180
For some reason, you can use those braces to do the sort of grouping syntax.

194
00:11:56,600 --> 00:11:57,770
So just keep that in mind.

195
00:11:58,220 --> 00:12:01,850
Out in the field, you see sometimes people using braces like this.

196
00:12:02,060 --> 00:12:08,000
Often you'll see braces used in these sort of exclusion statements, but they can also be used for inclusion.

197
00:12:09,500 --> 00:12:12,230
And then you can also use parentheses for multiple options.

198
00:12:12,620 --> 00:12:14,380
So we have multiple options for matching.

199
00:12:14,390 --> 00:12:16,820
You can use princes to list out these options.

200
00:12:17,240 --> 00:12:19,610
So I'm going to copy and paste some code here.

201
00:12:20,450 --> 00:12:21,080
Break quickly.

202
00:12:22,120 --> 00:12:28,150
Just from the notebook that goes along with this video and we say text is equal to hello, would you

203
00:12:28,150 --> 00:12:29,170
like some catfish?

204
00:12:29,620 --> 00:12:31,000
Text two says hello.

205
00:12:31,150 --> 00:12:32,660
Would you like to take a catnap?

206
00:12:33,190 --> 00:12:34,540
And then text three says hello.

207
00:12:34,570 --> 00:12:36,290
Have you seen this cat, Piller?

208
00:12:36,940 --> 00:12:39,070
So notice how all of these start with cat.

209
00:12:39,190 --> 00:12:41,380
And then it says Fish Nap Pillar.

210
00:12:42,460 --> 00:12:47,800
So if you wanted to kind of find the different options where you could do, say, search.

211
00:12:49,150 --> 00:12:53,980
Cat, and then you can use parentheses to kind of grouped together multiple options.

212
00:12:54,460 --> 00:12:55,630
So we'll say fish.

213
00:12:59,860 --> 00:13:00,950
And then you could say, Klaw.

214
00:13:01,160 --> 00:13:02,300
So let's see how this works.

215
00:13:02,840 --> 00:13:03,820
We'll say text.

216
00:13:04,610 --> 00:13:06,800
So when I run, that finds cat catfish.

217
00:13:07,160 --> 00:13:10,070
So that was the first instance of that.

218
00:13:10,130 --> 00:13:12,980
And then if I say text two, it says catnap.

219
00:13:13,460 --> 00:13:14,690
And if I say text three.

220
00:13:16,070 --> 00:13:19,550
It doesn't find anything because I didn't say basically or pillar.

221
00:13:19,730 --> 00:13:23,660
So I'm going to copy this and put that in there or a pillar.

222
00:13:23,880 --> 00:13:26,570
And if I check text three, it finds the match.

223
00:13:26,960 --> 00:13:32,480
So all this is doing is allowing you to basically combine that or statement we saw earlier with other

224
00:13:32,480 --> 00:13:35,480
pieces of text and provide multiple options.

225
00:13:36,620 --> 00:13:39,290
That's the basics of regular expressions.

226
00:13:39,380 --> 00:13:44,510
And you can check out the documentation page on regular expressions in the Python official documentation.

227
00:13:44,840 --> 00:13:47,090
It's linked to you at the bottom of the notebook.

228
00:13:47,390 --> 00:13:52,070
For more information or regular expressions, again, it can definitely very quickly get in to some

229
00:13:52,070 --> 00:13:54,830
very complex looking syntax that is difficult to read.

230
00:13:55,190 --> 00:13:59,330
But always remember just the basics where the main idea is you have some sort of character, right,

231
00:13:59,330 --> 00:14:03,590
that the fire and then attached to it some sort of quantifier if necessary.
