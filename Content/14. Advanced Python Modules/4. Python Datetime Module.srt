1
00:00:05,310 --> 00:00:06,230
Welcome back, everyone.

2
00:00:06,660 --> 00:00:11,820
In this lecture, we're going to give you a brief tour of the date time module, which essentially allows

3
00:00:11,820 --> 00:00:17,190
you to create objects that have information not just on the date or the time, but also things like

4
00:00:17,190 --> 00:00:22,950
time zone and operations between date time objects, such as how many seconds have passed or how many

5
00:00:22,950 --> 00:00:23,850
days have passed.

6
00:00:24,240 --> 00:00:26,100
Let's check this all out in a Jupiter notebook.

7
00:00:26,460 --> 00:00:26,730
All right.

8
00:00:26,760 --> 00:00:28,020
To begin using date time.

9
00:00:28,240 --> 00:00:30,570
Let's first explore the time factor.

10
00:00:31,110 --> 00:00:33,630
To do that, I'm going to first say import.

11
00:00:35,090 --> 00:00:35,650
Date time.

12
00:00:36,980 --> 00:00:39,710
And then we're going to create a date time object.

13
00:00:39,770 --> 00:00:45,920
So I will say my time is equal to date time that time.

14
00:00:46,850 --> 00:00:52,400
And if you do ship tap here, you'll notice it takes a couple of arguments an hour, a minute, a second

15
00:00:52,580 --> 00:00:54,770
a microsecond, and then time zone information.

16
00:00:55,280 --> 00:00:59,510
If you do not provide certain information, it's just going to automatically fill it in.

17
00:00:59,990 --> 00:01:03,290
For example, let's only provide the hour and the minute.

18
00:01:03,890 --> 00:01:10,490
So let's go ahead and say it's 2:00 a.m. so knows it's going to work on a 24 hour clock.

19
00:01:10,970 --> 00:01:13,760
And then it's 20 minutes past 2:00 a.m..

20
00:01:15,290 --> 00:01:22,130
So now if I take a look at this, my time object and I hit Dot Tab after I've defined it, I can actually

21
00:01:22,130 --> 00:01:23,720
access things like the minute.

22
00:01:24,620 --> 00:01:29,330
So this is an attribute and I can see the minute portion is 20 minutes.

23
00:01:29,930 --> 00:01:34,970
And I can check that the our portion is two hours.

24
00:01:35,030 --> 00:01:36,950
Again, this is running on a 24 hour clock.

25
00:01:37,370 --> 00:01:45,020
So if I actually just print out my time, it's going to print out to 20 and then they'll automatically

26
00:01:45,020 --> 00:01:47,480
fill in the seconds with zero.

27
00:01:48,020 --> 00:01:51,800
Let's imagine I only defined hour two and I rerun.

28
00:01:51,800 --> 00:01:55,300
This will automatically fill in minutes as zeros hours to.

29
00:01:55,570 --> 00:01:58,040
And when I print the time, it says it's just two o'clock.

30
00:01:58,320 --> 00:02:00,160
Everything's kind of filled in with zeros.

31
00:02:01,020 --> 00:02:03,230
Now this can actually go up to microseconds.

32
00:02:03,650 --> 00:02:08,690
So you could if you wanted to give microsecond level information.

33
00:02:09,080 --> 00:02:11,420
So in this case, will automatically fill it in at zero.

34
00:02:11,810 --> 00:02:15,860
But if you didn't want that sort of information, you would just keep going through the levels.

35
00:02:15,980 --> 00:02:21,530
So our minute, second microsecond and even time zone information so you can go all the way for something

36
00:02:21,530 --> 00:02:23,120
like if you want to be very precise.

37
00:02:23,630 --> 00:02:24,350
You can do.

38
00:02:24,470 --> 00:02:26,840
Let's go ahead and do something kind of the p.m. so.

39
00:02:26,870 --> 00:02:33,200
Thirteen hundred hours, 20 minutes, one second and then 20 microseconds.

40
00:02:33,710 --> 00:02:39,170
So if you run this, you can see we get all that information back and you'll notice that it continues

41
00:02:39,170 --> 00:02:40,490
on with the microseconds.

42
00:02:40,580 --> 00:02:43,550
If you don't find microseconds, it won't bother to display them for you.

43
00:02:44,010 --> 00:02:47,060
And again, microseconds typically are using that in a more scientific setting.

44
00:02:47,750 --> 00:02:47,980
OK.

45
00:02:48,620 --> 00:02:55,070
Now, we saw that this time instance we check out what type of object my time is.

46
00:02:55,130 --> 00:02:57,290
It's a state time dot type object.

47
00:02:57,650 --> 00:02:59,660
So only holds values of time.

48
00:02:59,720 --> 00:03:01,610
It has no date associated with this.

49
00:03:02,060 --> 00:03:08,930
Well, we could do, though, is add in date information using a date object or a combined date time

50
00:03:08,930 --> 00:03:09,380
object.

51
00:03:09,860 --> 00:03:12,500
So let's begin exploring the objects themselves.

52
00:03:14,680 --> 00:03:21,930
To do this, I'll say, today is equal to will say, date time, dot date.

53
00:03:22,390 --> 00:03:26,500
And if I wanted to, here's where I could begin defining the year, month and day.

54
00:03:27,010 --> 00:03:32,320
Or I could grab information on today by using the method call today.

55
00:03:33,720 --> 00:03:37,400
And now after running that I can print out today.

56
00:03:38,080 --> 00:03:39,040
And I'll say it's 20.

57
00:03:39,580 --> 00:03:40,240
June 12th.

58
00:03:40,540 --> 00:03:46,120
So notice here, the format follows the European kind of standard, which is the year, the month,

59
00:03:46,180 --> 00:03:46,570
the date.

60
00:03:46,690 --> 00:03:51,670
And in general, most programming languages will follow this sample from kind of the largest timescale

61
00:03:51,700 --> 00:03:55,090
years to smaller and smaller months dates and so on.

62
00:03:55,600 --> 00:03:59,140
And just as you might have expect, all of these are attributes.

63
00:03:59,260 --> 00:04:01,360
So I can grab the year.

64
00:04:03,570 --> 00:04:04,170
The month.

65
00:04:06,160 --> 00:04:07,390
And the date as well.

66
00:04:07,420 --> 00:04:07,930
Or the day.

67
00:04:09,070 --> 00:04:11,620
So at 12:00, which means I can grab portions of this.

68
00:04:13,670 --> 00:04:18,360
In Python also allows you to return something called sea time formatting, which is just another way

69
00:04:18,360 --> 00:04:20,610
of formatting this along with some time information.

70
00:04:21,000 --> 00:04:28,050
So I can say, go ahead, grab today, call sea time on it, and I'll print it out in this format where

71
00:04:28,050 --> 00:04:33,570
it will give you the day and three letters the month, the date itself.

72
00:04:33,780 --> 00:04:37,980
If you added time information, which will show you how to do in just a second, it'll be there.

73
00:04:38,130 --> 00:04:39,150
And then the year itself.

74
00:04:39,210 --> 00:04:42,630
So sometimes certains databases store time in this format.

75
00:04:42,690 --> 00:04:43,560
So it might be useful.

76
00:04:45,270 --> 00:04:50,070
So let's say now you don't just want date information and you also don't want just time information,

77
00:04:50,400 --> 00:04:52,410
you actually want date time information.

78
00:04:52,440 --> 00:04:57,570
So information that combines both of these, you can say from date time import.

79
00:04:57,870 --> 00:04:59,310
And this is where it looks a little weird.

80
00:04:59,610 --> 00:05:04,340
You actually import daytime from date time because the object happens to be the same thing or called

81
00:05:04,340 --> 00:05:08,880
the same thing as the module because it combines both date information and time information.

82
00:05:09,450 --> 00:05:10,700
And if we take a look at date time.

83
00:05:11,340 --> 00:05:17,320
Notice now it takes an both year, month and day as well as everything from our minute, second microsecond.

84
00:05:17,400 --> 00:05:18,660
And time zone information.

85
00:05:18,990 --> 00:05:20,940
So all of that now is available to you.

86
00:05:21,540 --> 00:05:25,490
So I could do something like, let's see, the year twenty twenty one.

87
00:05:26,880 --> 00:05:29,070
Let's say October 3rd.

88
00:05:29,460 --> 00:05:31,410
And then it can continue on to the actual hour.

89
00:05:31,860 --> 00:05:35,400
I can say it's going to be fourteen hundred hours.

90
00:05:36,020 --> 00:05:36,840
Twenty minutes.

91
00:05:37,380 --> 00:05:38,610
And let's say one second.

92
00:05:39,330 --> 00:05:42,030
And I can see I have this date time object so I can say.

93
00:05:43,210 --> 00:05:45,370
My date time is equal to this.

94
00:05:46,900 --> 00:05:49,660
And I can call all that information or I can just print it out.

95
00:05:51,010 --> 00:05:55,240
So you run this and we see the information printed out here again, going from the largest timescale

96
00:05:55,330 --> 00:05:58,510
year down to the smallest month day.

97
00:05:58,630 --> 00:06:00,610
Our minute, second and so on.

98
00:06:01,450 --> 00:06:05,590
Now, for some reason, you made a mistake or you need to do some sort of replacement.

99
00:06:05,860 --> 00:06:09,890
Like, for instance, Akseli said twenty, twenty one and I need to still be in twenty.

100
00:06:10,420 --> 00:06:17,470
I can do that with the replace functionality so I can take my date time object call replace on it.

101
00:06:18,190 --> 00:06:24,760
And then I simply say what the attribute I want to replaces such as the year and then specify that it

102
00:06:24,760 --> 00:06:26,760
should be another year like twenty twenty.

103
00:06:27,310 --> 00:06:33,970
We run that and now it returns back the same information, but with whatever you want to replace replaced

104
00:06:34,390 --> 00:06:36,640
and what it could do is then update it.

105
00:06:36,880 --> 00:06:38,410
So this doesn't actually happen in place.

106
00:06:38,500 --> 00:06:40,390
I'd have to say my date time is now equal to this.

107
00:06:40,960 --> 00:06:45,760
And now when I prince my date time, it's supposedly fixed.

108
00:06:45,790 --> 00:06:51,190
If that's what I needed to fix so I can use a replace parameter or method call in order to do that to

109
00:06:51,190 --> 00:06:52,180
your current dates.

110
00:06:52,510 --> 00:06:58,300
Now, something that's really common to perform with date time objects or date objects is just simple

111
00:06:58,330 --> 00:06:59,050
arithmetic.

112
00:06:59,560 --> 00:07:05,560
For example, let's say someone logs in to your Web site on a certain day and then they log back out

113
00:07:05,620 --> 00:07:09,580
on another day or at a certain date time and they leave at a certain date time.

114
00:07:09,940 --> 00:07:14,350
You probably want to figure out, well, how long they spend on my Web site or how long do they spend

115
00:07:14,350 --> 00:07:14,950
logged in.

116
00:07:15,040 --> 00:07:17,950
And we can perform simple arithmetic to solve for these.

117
00:07:18,060 --> 00:07:23,620
Then you can perform arithmetic either on a date object or a date time object.

118
00:07:23,650 --> 00:07:25,480
So we still need that date information there.

119
00:07:25,990 --> 00:07:27,640
Let me show you a couple of examples.

120
00:07:27,940 --> 00:07:32,350
Let's first do it with just pure date information and dates.

121
00:07:32,440 --> 00:07:35,730
Recall if we come up, here is date time, dot date.

122
00:07:36,190 --> 00:07:42,160
Alternatively, it's the same thing as saying something like from date time import date.

123
00:07:42,430 --> 00:07:43,930
So it's up to you, which when you want to use.

124
00:07:44,320 --> 00:07:45,340
Let's create two dates.

125
00:07:46,610 --> 00:07:47,790
Going to create date one.

126
00:07:48,590 --> 00:07:51,500
And let's just say this is sometime in the future.

127
00:07:52,070 --> 00:07:59,180
Go ahead and say maybe 20, 21 and we'll make this November 3rd and date to.

128
00:08:00,590 --> 00:08:02,840
It's going to be, let's say, 20, 20.

129
00:08:04,170 --> 00:08:09,840
November 3rd, what I can do is actually perform direct arithmetic on this to figure out the difference

130
00:08:09,840 --> 00:08:13,200
between them so I can say how much time is between date one.

131
00:08:14,630 --> 00:08:18,440
And day two, by taking this one in the future and subtracting this one.

132
00:08:19,070 --> 00:08:21,210
So I read this and it returns back.

133
00:08:21,230 --> 00:08:22,580
This time Delta object.

134
00:08:22,670 --> 00:08:27,410
And as you've might have guessed, if you take the difference between two date objects, it reports

135
00:08:27,410 --> 00:08:30,320
back that difference in number of days.

136
00:08:30,650 --> 00:08:35,780
And we could do something like result is equal to date one minus date two.

137
00:08:36,290 --> 00:08:41,720
And if you check the type of this results, it's a special time Delta Object, which has its own properties

138
00:08:41,720 --> 00:08:43,880
and attributes which you could find useful sometimes.

139
00:08:43,890 --> 00:08:46,580
So you can actually just call how many days have passed.

140
00:08:46,880 --> 00:08:52,070
Run that and returns back 365 days, which makes sense because these are directly a full year apart.

141
00:08:52,580 --> 00:08:56,360
Something to keep in mind, though, is that if you're doing this for really long distances between

142
00:08:56,360 --> 00:08:59,990
years, that is to say, you may have issues with leap year.

143
00:09:00,410 --> 00:09:03,350
So it may not be exactly 365 days per year.

144
00:09:03,630 --> 00:09:06,380
Instead, you may encounter a leap year with an additional day.

145
00:09:07,160 --> 00:09:07,350
All right.

146
00:09:07,850 --> 00:09:12,350
So the other thing we can do is perform arithmetic on a date time object.

147
00:09:12,890 --> 00:09:18,260
And we can either call this with date time, that date time or as we saw here, say from date time,

148
00:09:18,320 --> 00:09:19,100
import date time.

149
00:09:20,000 --> 00:09:21,860
Let's create to date time.

150
00:09:21,860 --> 00:09:23,150
Objects will say date time.

151
00:09:23,160 --> 00:09:30,500
One is equal to date time and it's going to use the same dates.

152
00:09:32,170 --> 00:09:40,270
And then what we're going to do is say one occurs at, let's just say twenty two hundred hours.

153
00:09:41,500 --> 00:09:42,190
So let's take time.

154
00:09:42,190 --> 00:09:43,220
One recall.

155
00:09:43,440 --> 00:09:43,900
Twenty two.

156
00:09:43,920 --> 00:09:44,550
Here's ours.

157
00:09:44,590 --> 00:09:47,560
This is minutes and then date time to.

158
00:09:48,890 --> 00:09:51,230
Is equal to date time and.

159
00:09:52,450 --> 00:09:54,720
Still the same dates, they're one year apart.

160
00:09:54,810 --> 00:09:58,320
But we're also going to make them, let's say, 10 hours apart.

161
00:09:59,640 --> 00:10:00,300
So we run that.

162
00:10:00,910 --> 00:10:05,910
Now, let's go out and check the difference between daytime one minus.

163
00:10:06,950 --> 00:10:10,010
Date time to run that.

164
00:10:10,100 --> 00:10:12,320
And our returns back to points of information.

165
00:10:12,650 --> 00:10:13,460
Total days.

166
00:10:13,700 --> 00:10:14,960
You'll notice it returns back.

167
00:10:15,050 --> 00:10:15,980
Total seconds.

168
00:10:16,400 --> 00:10:17,240
So we're turning back.

169
00:10:17,570 --> 00:10:19,100
Thirty six thousand seconds.

170
00:10:19,400 --> 00:10:21,920
We know these are ten hours apart.

171
00:10:22,340 --> 00:10:24,170
So I can check on this by saying.

172
00:10:24,710 --> 00:10:26,900
Thirty six thousand divided by.

173
00:10:27,140 --> 00:10:28,010
If we say there.

174
00:10:28,550 --> 00:10:31,790
If we want to figure out maybe how many hours apart there, I could say three.

175
00:10:32,000 --> 00:10:32,870
Thirty six thousand.

176
00:10:33,200 --> 00:10:35,240
Divided by sixty to get minutes.

177
00:10:35,660 --> 00:10:37,390
Divided by 60 to get ours.

178
00:10:38,120 --> 00:10:40,280
We run that and we get back those ten hours.

179
00:10:40,910 --> 00:10:43,610
If I say this as a result, let's copy this.

180
00:10:44,210 --> 00:10:48,400
Say my difference is equal to date time one minus date time two.

181
00:10:49,190 --> 00:10:50,510
You'll notice there's attributes here.

182
00:10:50,660 --> 00:10:52,490
And I can actually grab the total seconds.

183
00:10:53,890 --> 00:10:58,100
Our seconds between them, which is thirty six thousand, if I say it, my def.

184
00:10:59,410 --> 00:11:04,620
And he total seconds is then now inclusive of not just the second time between them, but also how many

185
00:11:04,620 --> 00:11:06,420
seconds are in 365 days.

186
00:11:06,810 --> 00:11:08,580
So this is just the seconds portion.

187
00:11:08,760 --> 00:11:11,010
This is everything reported as seconds.

188
00:11:11,490 --> 00:11:14,810
That's it for this brief lecture on date time module.

189
00:11:15,300 --> 00:11:19,140
You'll often find yourself encountering this if you're ever dealing with a database.

190
00:11:19,920 --> 00:11:20,280
Thanks.

191
00:11:20,370 --> 00:11:21,450
And I'll see you at the next lecture.
