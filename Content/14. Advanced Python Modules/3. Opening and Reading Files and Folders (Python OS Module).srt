1
00:00:05,440 --> 00:00:06,480
Welcome back, everyone.

2
00:00:06,670 --> 00:00:12,400
In this lecture, we're going to be focusing on two built in modules which relate to opening and reading

3
00:00:12,430 --> 00:00:14,230
files and folders on your computer.

4
00:00:14,620 --> 00:00:18,190
And that is the Shell utilities module and the OS module.

5
00:00:18,400 --> 00:00:19,960
So shuttle or OS?

6
00:00:21,490 --> 00:00:25,300
Essentially, we already know how to open an individual file of python.

7
00:00:25,600 --> 00:00:30,610
We saw that at the very beginning of the course, but we still don't know how to do a few things, such

8
00:00:30,610 --> 00:00:35,110
as what if we have to open every file and a directory and read and write to it?

9
00:00:35,590 --> 00:00:40,570
How would we actually find those correct directories and loop through them, especially if they were

10
00:00:40,570 --> 00:00:42,460
sub folders within sub folders?

11
00:00:43,060 --> 00:00:46,720
Also, what if we actually wanted to move files around on our own computer?

12
00:00:48,220 --> 00:00:54,310
Pythons OS module and Shell Utility's module allow us to easily navigate files and directories on the

13
00:00:54,310 --> 00:00:58,300
computer and then perform actions on them, such as moving them or deleting them.

14
00:00:58,870 --> 00:01:01,630
Let's go ahead and explore these modules and get started.

15
00:01:02,140 --> 00:01:05,350
OK, here I am underneath advanced Python Modules folder.

16
00:01:05,770 --> 00:01:10,510
I wanted to note here that the lecture notebook corresponds with what we will be showing here is called

17
00:01:10,810 --> 00:01:12,910
Opening and reading files folders.

18
00:01:13,090 --> 00:01:16,570
So keep in mind, it's not called something like a W module and shell utility.

19
00:01:16,700 --> 00:01:20,920
So right now, the election book is this one opening and reading files and folders.

20
00:01:21,310 --> 00:01:26,130
And I would recommend that you follow along with a notebook in the same location as me.

21
00:01:26,200 --> 00:01:30,820
Which is under advanced python modules since we're going to be doing things like listening directory

22
00:01:30,820 --> 00:01:31,360
structure.

23
00:01:31,420 --> 00:01:35,470
And if you want to see similar results, it should be in the same location as where I am.

24
00:01:35,490 --> 00:01:39,460
So I've gone ahead and created a new untitled notebook, which is this right here.

25
00:01:40,060 --> 00:01:40,270
All right.

26
00:01:40,540 --> 00:01:44,980
So, again, if you're doing this in another directory or on your computer, keep in mind the results

27
00:01:44,980 --> 00:01:50,290
you see maybe slightly different than ours, but the actual logic should still be the same to discuss

28
00:01:50,380 --> 00:01:51,730
opening and reading files.

29
00:01:51,820 --> 00:01:54,850
Let's quickly review an understanding of file paths.

30
00:01:55,780 --> 00:02:02,680
If you're in Jupiter, you can type P WD hit, enter or shift, enter, run the cell and it will report

31
00:02:02,680 --> 00:02:08,290
back your current working directory, which is right now here under Complete Python three bootcamp and

32
00:02:08,290 --> 00:02:09,850
then advanced python modules.

33
00:02:10,270 --> 00:02:15,790
The second most important thing to note here is it actually reports back the syntax structure for writing

34
00:02:15,820 --> 00:02:18,790
a file path for whatever your operating system happens to be.

35
00:02:19,150 --> 00:02:20,770
In my case, I'm using windows.

36
00:02:21,040 --> 00:02:26,980
So it separates folders or directories with this double backslash and that sort of formatting is important

37
00:02:26,980 --> 00:02:27,460
later on.

38
00:02:27,820 --> 00:02:30,970
So keep that in mind if you're using something like Linux or Mac OS.

39
00:02:31,270 --> 00:02:35,350
It's probably going to show you a forward slash, but it should show you the correct format.

40
00:02:36,060 --> 00:02:39,940
OK, so I understand now where my notebook is located.

41
00:02:40,420 --> 00:02:43,720
What I want to do before we get started is create a practice file.

42
00:02:44,110 --> 00:02:47,890
So I actually already know how to create a practice file or any text file.

43
00:02:48,120 --> 00:02:49,240
Write to it and close it.

44
00:02:49,840 --> 00:02:56,530
What I mean by that is early on in the course we cover doing something like this where I will open up

45
00:02:56,680 --> 00:02:57,820
practice, not text file.

46
00:02:58,060 --> 00:03:01,450
And since this actually doesn't exist yet, it will actually be creating a text file.

47
00:03:01,810 --> 00:03:02,740
And I want to write to it.

48
00:03:03,340 --> 00:03:06,250
So I will have my wrote A Mode, B write plus.

49
00:03:07,200 --> 00:03:08,890
And let's go ahead and write something to this.

50
00:03:09,250 --> 00:03:10,690
This is a test string.

51
00:03:12,520 --> 00:03:13,870
And then I will close the file.

52
00:03:13,960 --> 00:03:16,210
You can do this with a with statement if you prefer.

53
00:03:17,320 --> 00:03:20,270
So if you run, this basically happens is it's going to open up.

54
00:03:20,290 --> 00:03:22,360
Practice that text file and overwrite.

55
00:03:22,720 --> 00:03:24,730
This is a test string on it and close it off.

56
00:03:25,150 --> 00:03:29,200
So now if I take a look at advanced python modules, you should be able to scroll somewhere here and

57
00:03:29,200 --> 00:03:32,320
see practice, not text file, which if you click on it and open it.

58
00:03:32,410 --> 00:03:33,700
It says this is a test string.

59
00:03:34,060 --> 00:03:34,540
Perfect.

60
00:03:34,840 --> 00:03:34,990
OK.

61
00:03:35,050 --> 00:03:37,100
So we'll be working off that text file later on.

62
00:03:37,570 --> 00:03:41,800
But let's come back to our notebook and first discuss how to use the OS module.

63
00:03:42,250 --> 00:03:47,590
So the OS module is really useful because it allows you to do things like get the current working directory

64
00:03:47,650 --> 00:03:49,510
or list all the files in a directory.

65
00:03:49,990 --> 00:03:52,900
And these commands worked across all operating systems.

66
00:03:53,710 --> 00:04:02,830
So I simply say import OS and I can say OS stop get S.W. WD Current Working Directory open and close

67
00:04:02,830 --> 00:04:06,400
princes and it reports back the current working directory.

68
00:04:06,820 --> 00:04:09,040
So paedo BD right here.

69
00:04:09,400 --> 00:04:12,100
This is kind of a special command within Jupiter.

70
00:04:12,250 --> 00:04:19,450
It also works technically at your command line if you're running something on Mac OS or a Linux based

71
00:04:19,450 --> 00:04:19,840
system.

72
00:04:20,230 --> 00:04:26,830
But this OS thought Getsy WD that will work within any Python script will report back your current working

73
00:04:26,830 --> 00:04:27,400
directory.

74
00:04:27,820 --> 00:04:31,000
And let's imagine I wanted to list items in a directory.

75
00:04:32,380 --> 00:04:35,290
If I say os dort list DSR.

76
00:04:37,570 --> 00:04:40,150
It will list everything in my current directory.

77
00:04:40,210 --> 00:04:42,010
So here I can see all the notebooks I had.

78
00:04:42,340 --> 00:04:47,410
I can see some folders, the text file, as well as the practice, not text file and my current notebook.

79
00:04:47,920 --> 00:04:52,900
Now, by default, if you just say list the IRR, it's going to list all the items in the current working

80
00:04:52,900 --> 00:04:53,410
directory.

81
00:04:53,860 --> 00:04:56,200
What I could do is list items in another directory.

82
00:04:56,650 --> 00:05:02,740
So, for example, if I wanted to, for instance, figure out what are all the files and folders I have

83
00:05:02,740 --> 00:05:04,870
under users, I can run that.

84
00:05:05,050 --> 00:05:07,540
And now it's going to list everything underneath users.

85
00:05:07,900 --> 00:05:11,400
So I can see all users default, my profile public, etc..

86
00:05:11,920 --> 00:05:15,580
So list directory essentially just takes the argument of path.

87
00:05:15,940 --> 00:05:18,220
You pass it in and it lists all the items there.

88
00:05:18,660 --> 00:05:24,820
This will be very useful if you wanted to maybe have a for loop to open up all the files in a directory

89
00:05:24,940 --> 00:05:29,820
and we'll see later on how we can do that in an even more robust manner through OS walk.

90
00:05:30,460 --> 00:05:33,340
So now we know how to get the current working directory.

91
00:05:33,520 --> 00:05:36,800
And again, this works on any operating system or any Python script.

92
00:05:37,210 --> 00:05:39,380
And we know how to list all the items in any directory.

93
00:05:41,820 --> 00:05:45,750
Now, let's go ahead and show you how you can move files around, which is something we actually don't

94
00:05:45,750 --> 00:05:46,420
know how to do yet.

95
00:05:46,990 --> 00:05:53,250
What I'm going to do is I'm going to import the Shell Utility's module, which is just a s s h.

96
00:05:53,320 --> 00:05:54,220
You t i l.

97
00:05:55,440 --> 00:05:59,910
And it can be used to move files in different directions or in different locations, I should say.

98
00:06:00,420 --> 00:06:07,050
So right now I have my practice, not text file located right here under advanced Pythonic modules.

99
00:06:07,470 --> 00:06:09,150
Let's go ahead and show you how you can move it.

100
00:06:09,990 --> 00:06:16,710
You simply call shell utilities that move and essentially just takes two main parameters, which is

101
00:06:17,040 --> 00:06:20,820
SIRC the source and DSD the destination.

102
00:06:21,360 --> 00:06:23,990
So it's very similar to the Unix M.V. Command.

103
00:06:24,360 --> 00:06:27,720
If you're comfortable with the Linux command line, that should feel familiar.

104
00:06:28,170 --> 00:06:32,070
Essentially what we're saying is take this source and move it to this destination.

105
00:06:32,800 --> 00:06:34,590
Let's go ahead and grab that practice.

106
00:06:35,950 --> 00:06:42,260
The text file and then the next thing to do is tell Python where I want to move this file to, I need

107
00:06:42,260 --> 00:06:44,840
to choose somewhere where I actually have permission to move it to.

108
00:06:45,170 --> 00:06:50,000
So if you're running this on a work computer or work network where you don't have full admin permissions,

109
00:06:50,030 --> 00:06:51,710
you may actually encounter an error here.

110
00:06:51,740 --> 00:06:53,840
So make sure you have permissions on your computer to do this.

111
00:06:54,290 --> 00:06:59,090
I know that I'll have permissions in my own user folder, so let's go ahead and do that.

112
00:06:59,510 --> 00:07:01,200
Going to copy and paste this path.

113
00:07:03,640 --> 00:07:04,390
Let's type that in.

114
00:07:05,290 --> 00:07:10,900
Move that and now basely reports back that this practice, that text file is now located underneath

115
00:07:10,900 --> 00:07:11,500
my username.

116
00:07:11,950 --> 00:07:16,120
And if I take a look back at my folder, advanced python modules and scroll down.

117
00:07:16,600 --> 00:07:19,360
Notice that practice text file is no longer there.

118
00:07:19,750 --> 00:07:24,610
And if I were to search on my computer underneath my username, I would find that practice, that text

119
00:07:24,610 --> 00:07:25,110
file there.

120
00:07:25,570 --> 00:07:29,830
It can actually confirm this by just saying OS stop list directory.

121
00:07:30,340 --> 00:07:31,600
Let's go ahead and list everything.

122
00:07:32,950 --> 00:07:33,940
That's under my username.

123
00:07:37,990 --> 00:07:38,460
Run that.

124
00:07:40,780 --> 00:07:43,620
And I should somewhere here be able to see that practice, not text file.

125
00:07:43,680 --> 00:07:48,600
So if he keeps rolling down, eventually make sure it's nothing out of order here to practice.

126
00:07:48,600 --> 00:07:49,650
Stop text file.

127
00:07:49,710 --> 00:07:50,220
Perfect.

128
00:07:50,610 --> 00:07:56,100
So I was able to take a file and programmatically move it somewhere else on my computer.

129
00:07:58,950 --> 00:08:03,040
Now, something to keep in mind is OS has a lot of functionality available on it.

130
00:08:03,430 --> 00:08:06,520
So as you get more advanced, you may find yourself wanting to do more things of it.

131
00:08:06,970 --> 00:08:12,430
You can say, oh, dot tab, and you'll notice that it has a huge list of things that can do for the

132
00:08:12,430 --> 00:08:12,790
most part.

133
00:08:12,790 --> 00:08:15,490
We're just gonna be teaching you the basics here, how to move things around.

134
00:08:15,700 --> 00:08:16,570
How to open things.

135
00:08:16,600 --> 00:08:18,550
List directories and how to remove things.

136
00:08:19,000 --> 00:08:22,030
So there's three ways to actually delete a file.

137
00:08:22,210 --> 00:08:26,260
And I talk about this within the lecture notebook, so I'm just gonna quickly bring that over.

138
00:08:27,510 --> 00:08:29,670
To discuss it, something to scroll back up here.

139
00:08:30,740 --> 00:08:32,510
And here's the deleting file section.

140
00:08:32,540 --> 00:08:36,470
So we just went over moving things and deleting files.

141
00:08:36,620 --> 00:08:40,460
You should note that the OS module provides three methods for leading files.

142
00:08:40,920 --> 00:08:46,100
There's OS, the unlink path, which is going to delete a file at the path you provide.

143
00:08:46,740 --> 00:08:51,050
There's OS Stop our MDR, which deletes an entire folder.

144
00:08:51,200 --> 00:08:53,600
However, the folder must be empty at the party provide.

145
00:08:53,990 --> 00:08:58,850
So unlink is for a single file removed directory is to remove a folder.

146
00:08:58,940 --> 00:09:00,030
Folder has to be empty first.

147
00:09:00,070 --> 00:09:02,810
So essentially the unlink everything within that folder.

148
00:09:03,140 --> 00:09:09,170
Then remove the directory and then there's the shell utility command, which is remove tree.

149
00:09:09,380 --> 00:09:12,800
And this part of the most dangerous one, which is the one I really want to talk about here.

150
00:09:13,130 --> 00:09:16,460
This will remove all files and folders contained in the path.

151
00:09:16,940 --> 00:09:21,950
This one you have to be super careful with because these methods can actually be reversed.

152
00:09:22,280 --> 00:09:27,530
So they're operating essentially at the command line, which means it's not like just clicking something

153
00:09:27,530 --> 00:09:28,700
and dragging it to the trash.

154
00:09:29,060 --> 00:09:32,080
And if you made a mistake, you can take it out of the trash again.

155
00:09:33,020 --> 00:09:38,210
Once you delete something using remove tree, it becomes extremely difficult to try to recover the file.

156
00:09:38,580 --> 00:09:40,880
You should just assume that you're not going be able to recover it.

157
00:09:41,000 --> 00:09:42,500
And it's a permanent removal.

158
00:09:42,920 --> 00:09:43,760
So keep that in mind.

159
00:09:43,770 --> 00:09:45,140
You should really be careful with this.

160
00:09:45,140 --> 00:09:50,330
Remove tree command and only use it when you're absolutely sure that you would have no problem deleting

161
00:09:50,330 --> 00:09:53,840
it, especially if there's a possibility this path could change.

162
00:09:54,410 --> 00:09:59,390
So because this is such a dangerous method, what I suggest you use if you find yourself needing to

163
00:09:59,390 --> 00:10:05,630
remove files is the send to trash module, which is actually an outside module you need to download.

164
00:10:06,090 --> 00:10:09,380
So you need to pip install, sent the trash at your command line.

165
00:10:09,410 --> 00:10:11,930
So open up your command line and type PIP install.

166
00:10:12,260 --> 00:10:15,620
Send it to trash and then you should be able to import it.

167
00:10:16,040 --> 00:10:21,350
So send the trash is safer because it will send things to the trash bin instead of a more permanent

168
00:10:21,350 --> 00:10:21,890
removal.

169
00:10:22,340 --> 00:10:22,530
OK.

170
00:10:22,940 --> 00:10:29,120
So again OS module, it can delete files, delete empty folders or delete everything within a folder,

171
00:10:29,210 --> 00:10:30,350
including all the files in it.

172
00:10:30,650 --> 00:10:34,040
But it's pretty dangerous to work with because it's a permanent removal.

173
00:10:34,310 --> 00:10:38,450
Instead of moving things to Trashman, which is why I recommend, if you do find yourself doing this

174
00:10:38,830 --> 00:10:40,160
checkout center trash first.

175
00:10:40,220 --> 00:10:43,430
So its explorer sent the trash going to come back here.

176
00:10:45,060 --> 00:10:51,570
And what I want to do is after you've done installing it, import send to trash.

177
00:10:53,610 --> 00:10:54,060
There we go.

178
00:10:54,660 --> 00:10:56,490
And let's check everything that's in my directory.

179
00:10:58,160 --> 00:11:01,040
And I currently don't have that practice file in my directory.

180
00:11:01,130 --> 00:11:02,630
So let's go ahead and get it back.

181
00:11:03,050 --> 00:11:04,700
What I'm going to do is say.

182
00:11:06,140 --> 00:11:07,100
Shell utilities.

183
00:11:08,300 --> 00:11:08,780
Grab.

184
00:11:11,510 --> 00:11:15,830
The practice, that text file, which is currently located under my username, and then I have to figure

185
00:11:15,830 --> 00:11:18,290
out where I want to move it to and I will move it to.

186
00:11:19,780 --> 00:11:25,090
My current working directory, which I can simply say os thought get current working directory, so

187
00:11:25,120 --> 00:11:27,940
that says grab that practice text file and move it back here.

188
00:11:29,650 --> 00:11:31,370
So I run that reports back.

189
00:11:31,390 --> 00:11:32,420
Now it's location.

190
00:11:33,070 --> 00:11:39,010
And if I try to list these directories again, I now see practice, not text file has been moved back

191
00:11:39,010 --> 00:11:40,000
to my current location.

192
00:11:40,510 --> 00:11:42,550
Let's go ahead and send this to the trash.

193
00:11:42,610 --> 00:11:43,330
It's actually quite easy.

194
00:11:43,370 --> 00:11:44,980
Do you simply say.

195
00:11:46,090 --> 00:11:49,630
Send to trash dot sent the trash.

196
00:11:50,290 --> 00:11:54,730
And then you just provide a full file path or file of what you want to send to the trash.

197
00:11:54,850 --> 00:12:01,450
So I will send this practice, that text file to the trash run that it's now in my trash bin.

198
00:12:01,540 --> 00:12:03,130
It hasn't been completely removed.

199
00:12:03,160 --> 00:12:05,080
I could recover it if I really needed to.

200
00:12:07,270 --> 00:12:08,890
And say, oh, not this directory.

201
00:12:10,010 --> 00:12:12,290
And I no longer see that practice text file.

202
00:12:13,130 --> 00:12:17,300
And now the last thing I want to show you in this lecture, which is probably the most useful, at least

203
00:12:17,360 --> 00:12:22,370
in my opinion, functionality of the OS module is OS dot walk.

204
00:12:22,640 --> 00:12:29,860
And if you type OS that walk and do shift tab here, it essentially takes in one parameter top.

205
00:12:30,140 --> 00:12:35,780
And it says it's a directory tree generator, which essentially what that means is for each directory

206
00:12:35,870 --> 00:12:39,410
in directory tree rooted at the top, it's going to yield a tuple.

207
00:12:39,650 --> 00:12:40,760
The directory path.

208
00:12:41,050 --> 00:12:42,380
The directory names.

209
00:12:42,530 --> 00:12:43,790
And then the file names.

210
00:12:44,050 --> 00:12:47,360
So the way I think about this is by creating a for loop.

211
00:12:47,930 --> 00:12:54,800
So it's a four folder, comma, sub folders, comma files.

212
00:12:54,920 --> 00:12:59,270
This is tuple unpacking in OS dot walk.

213
00:12:59,720 --> 00:13:03,740
And if you want, you can pass in your current working directory.

214
00:13:04,100 --> 00:13:09,290
But the reason I recommended you follow along in the same location we did is because I'm going to take

215
00:13:09,290 --> 00:13:12,290
a look at Figueiro Advanced Python modules up here.

216
00:13:12,470 --> 00:13:14,900
I created an example top level folder.

217
00:13:15,470 --> 00:13:17,270
Do you click on example top a folder?

218
00:13:17,630 --> 00:13:19,430
Notice there's two folders within it.

219
00:13:19,610 --> 00:13:21,710
Mid example one, mid example two.

220
00:13:22,130 --> 00:13:25,430
And there's an actual file within the middle made example that text.

221
00:13:25,940 --> 00:13:31,310
If you then go to mid example, one notice there's two bottom level folders, some mid-level document

222
00:13:31,310 --> 00:13:31,820
in here.

223
00:13:32,480 --> 00:13:33,770
And then there's one text.

224
00:13:35,120 --> 00:13:38,520
Bottom level two, there's bottom text to come back up.

225
00:13:38,540 --> 00:13:40,480
Levels go to mid example, too.

226
00:13:40,700 --> 00:13:41,660
And this one is empty.

227
00:13:42,230 --> 00:13:46,100
So what I want to do is actually pass any file path for this example, top level.

228
00:13:46,460 --> 00:13:51,380
And this should give you a good idea, as well as a simple comparison to be able to understand how OS

229
00:13:51,390 --> 00:13:53,540
that walk is reporting back results.

230
00:13:54,350 --> 00:13:57,230
So I'm going to come back here and I'm going to say the following.

231
00:13:57,350 --> 00:14:03,500
I'm going to say, oh, stop, walk and let's go ahead and actually grab the directory for this example,

232
00:14:03,920 --> 00:14:05,300
top level file.

233
00:14:05,750 --> 00:14:13,730
And the way to do that is let me insert a cell above this and quickly grab our current working directory.

234
00:14:15,860 --> 00:14:18,560
So I'm going to grab this guy here.

235
00:14:20,920 --> 00:14:22,190
Pasted in in effect.

236
00:14:22,260 --> 00:14:23,230
Let's do this on two lines.

237
00:14:23,260 --> 00:14:24,400
It's a little easier to read.

238
00:14:25,000 --> 00:14:28,870
So we'll say file path is equal to.

239
00:14:29,570 --> 00:14:31,640
And then we have this kind of long file path here.

240
00:14:31,720 --> 00:14:32,630
Users, et cetera.

241
00:14:33,190 --> 00:14:35,350
And I'm not going to look at advanced python modules.

242
00:14:35,440 --> 00:14:39,340
I'm going to say backslash backslash example.

243
00:14:40,030 --> 00:14:43,090
Underscore top, underscore level.

244
00:14:43,630 --> 00:14:45,850
Note my capitalization here.

245
00:14:46,470 --> 00:14:46,710
OK.

246
00:14:46,820 --> 00:14:49,270
Now let's actually put this in a separate cell.

247
00:14:51,210 --> 00:14:54,850
So there's my file path, I'm going to look directly at example, top level.

248
00:14:55,410 --> 00:15:02,070
So we'll say four folder sub folders, files and OS that walk for this particular file path if you want.

249
00:15:02,100 --> 00:15:06,990
If you're operating on a different computer and for instance, you don't have access to our specific

250
00:15:07,020 --> 00:15:12,540
advanced python modules, you can replace this with Overstock, get current working directory in order

251
00:15:12,540 --> 00:15:13,920
to explore what we're talking about here.

252
00:15:14,310 --> 00:15:19,170
But to get same results, I do go ahead and check out this example, top level folder and then inside

253
00:15:19,170 --> 00:15:21,030
of this will add in a couple of print statements.

254
00:15:21,090 --> 00:15:23,010
So we can clearly see what's going on.

255
00:15:23,700 --> 00:15:24,660
I'm going to print.

256
00:15:26,530 --> 00:15:28,270
Using, let's say, a string literals here.

257
00:15:30,630 --> 00:15:33,120
Currently looking at.

258
00:15:34,350 --> 00:15:36,240
And then I'm going to report back.

259
00:15:36,300 --> 00:15:38,460
The folder I'm currently looking at.

260
00:15:40,320 --> 00:15:46,200
I'll print a new line with backslash end and then I will print.

261
00:15:50,290 --> 00:15:56,450
The sub folders or really subdirectories are now let's report back.

262
00:15:58,160 --> 00:16:03,920
For every subfolder, we'll say, for every sub fold and sub folders.

263
00:16:05,680 --> 00:16:06,220
Prince.

264
00:16:07,620 --> 00:16:12,180
And we're going to do here is again, f string literal subfolder.

265
00:16:13,750 --> 00:16:15,190
And then we all do here is.

266
00:16:16,170 --> 00:16:18,230
Paste in that subfield.

267
00:16:18,720 --> 00:16:19,740
So it's going to report back.

268
00:16:19,770 --> 00:16:24,360
The folder and then I'm going to go through each subfolder or subdirectory within this.

269
00:16:25,060 --> 00:16:28,350
And then print a new line.

270
00:16:29,670 --> 00:16:31,110
I'll do the same thing for the files.

271
00:16:32,040 --> 00:16:32,880
We'll say Prince.

272
00:16:34,280 --> 00:16:35,240
The files are.

273
00:16:37,340 --> 00:16:39,530
And for F in files.

274
00:16:41,430 --> 00:16:45,190
I'll say print string literal here.

275
00:16:45,670 --> 00:16:46,240
File.

276
00:16:47,660 --> 00:16:50,240
Let's type in the F there to print out the file name.

277
00:16:52,290 --> 00:16:54,150
And once this is done or print out.

278
00:16:55,960 --> 00:17:02,790
A new line and just for more organization where you could do, if you want, you can add in tab spaces.

279
00:17:02,800 --> 00:17:07,420
So essentially backslash T is a tab so we can do that within days.

280
00:17:07,480 --> 00:17:12,700
So it says Crumley, looking at folder Elyssa some folders indented and Elyssa files indented.

281
00:17:13,180 --> 00:17:13,860
Go ahead and run this.

282
00:17:13,870 --> 00:17:15,220
Make sure we don't commit any typos.

283
00:17:15,910 --> 00:17:17,110
Looks like I have one minor typo.

284
00:17:17,140 --> 00:17:20,110
I actually said some folders instead of sub underscore folders.

285
00:17:20,590 --> 00:17:21,190
Let's fix that.

286
00:17:22,060 --> 00:17:22,510
And there we go.

287
00:17:22,930 --> 00:17:23,130
OK.

288
00:17:23,230 --> 00:17:25,630
So what's this actually looking at reports back.

289
00:17:25,750 --> 00:17:27,880
I'm kind of looking at this folder example.

290
00:17:27,880 --> 00:17:29,380
Top level recall.

291
00:17:29,440 --> 00:17:30,400
That's what I have here.

292
00:17:30,430 --> 00:17:31,450
Example, top level.

293
00:17:31,800 --> 00:17:33,310
So we're gonna kind of could do this comparison.

294
00:17:33,810 --> 00:17:37,660
It reports back to sub folders made example, one mid example, too.

295
00:17:38,260 --> 00:17:40,150
So if I take a look here, I see my example.

296
00:17:40,150 --> 00:17:42,610
One main example to note, these are folder directories.

297
00:17:42,940 --> 00:17:44,380
It's not just a single file.

298
00:17:45,430 --> 00:17:50,020
It says the files underneath this are file mid example, dot text.

299
00:17:50,770 --> 00:17:53,500
Now, what OS walk is going to continue to do.

300
00:17:53,980 --> 00:17:57,770
It's then going to open each of these sub folders and report back.

301
00:17:57,810 --> 00:17:59,770
Hey, now I'm looking at MIT example one.

302
00:18:00,340 --> 00:18:02,650
Here are the folders with an MIT example one.

303
00:18:03,130 --> 00:18:04,540
And here's the file within that.

304
00:18:05,260 --> 00:18:07,720
Now I'm going to look at bottom level one.

305
00:18:08,080 --> 00:18:09,370
So now it's looking at this subfolder.

306
00:18:09,910 --> 00:18:13,150
It says file one text is located in there.

307
00:18:13,660 --> 00:18:14,890
Now, I'm going to look at the other one.

308
00:18:14,920 --> 00:18:16,660
Bottom level to bottom.

309
00:18:16,660 --> 00:18:17,800
Text two is located there.

310
00:18:18,340 --> 00:18:19,380
Then we're gonna go back up.

311
00:18:19,690 --> 00:18:23,770
And our view, mid example two, there were no sub folders there and there were no files there.

312
00:18:24,220 --> 00:18:26,770
So if your call MIT example, too, was just empty.

313
00:18:27,460 --> 00:18:30,430
So we can see here that OS walk.

314
00:18:30,980 --> 00:18:37,180
What it's going to do is essentially make a tree and it's going to look at every single thing inside

315
00:18:37,270 --> 00:18:38,650
of this file path, location.

316
00:18:39,010 --> 00:18:40,150
It's going to look at the folder.

317
00:18:40,270 --> 00:18:42,460
It's going to look at the subdirectories or sub folders.

318
00:18:42,760 --> 00:18:44,230
And then it's going to look at the files.

319
00:18:44,470 --> 00:18:47,230
And hopefully what you can do then is add in logic.

320
00:18:47,260 --> 00:18:53,390
So maybe you're looking for every text file within a certain directory that starts with the year that

321
00:18:53,530 --> 00:18:55,570
twenty ten or twenty twenty or something like that.

322
00:18:55,990 --> 00:18:59,950
Well, you could use this and then have some sort of if statement to check the file names.

323
00:19:00,340 --> 00:19:06,280
And now you can very quickly and programmatically look within directories, within directories.

324
00:19:06,520 --> 00:19:08,500
So OS Walk is extremely useful.

325
00:19:08,920 --> 00:19:13,750
It takes a little bit of time to get used to this sort of kind of tuple unpacking as well as the way

326
00:19:13,780 --> 00:19:15,610
the tree directory actually works.

327
00:19:15,880 --> 00:19:23,590
But hopefully this example kind of allows you to have a relationship between what is reported back versus

328
00:19:23,650 --> 00:19:24,670
what is actually there.

329
00:19:25,000 --> 00:19:27,730
So I would highly recommend you run this on the file.

330
00:19:27,730 --> 00:19:33,520
We created example top level in order for you to make the direct comparison to understand how it's looking

331
00:19:33,520 --> 00:19:34,180
at files.

332
00:19:34,510 --> 00:19:35,590
Subdirectories.

333
00:19:35,680 --> 00:19:37,150
And the actual raw files.

334
00:19:37,720 --> 00:19:39,160
OK, you have any questions?

335
00:19:39,160 --> 00:19:40,720
Feel free to post the Q&amp;A forms.

336
00:19:41,050 --> 00:19:42,060
I'll see you at the next lecture.
