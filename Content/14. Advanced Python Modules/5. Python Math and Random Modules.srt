1
00:00:05,290 --> 00:00:06,340
Welcome back, everyone.

2
00:00:06,430 --> 00:00:09,460
In this lecture, we're going to discuss two builtin modules.

3
00:00:09,820 --> 00:00:11,470
One is the math module.

4
00:00:11,590 --> 00:00:15,340
It's essentially just a module that holds a ton of useful math functions.

5
00:00:15,640 --> 00:00:18,400
We won't go through every single one, but we'll show you how to find them.

6
00:00:18,880 --> 00:00:24,040
And then there's the random module that contains a lot of mathematical random functions, as well as

7
00:00:24,040 --> 00:00:27,700
random functions for grabbing a random item from a python list.

8
00:00:28,060 --> 00:00:31,330
We've actually already seen a little bit of random with random shuffle.

9
00:00:31,370 --> 00:00:33,340
But let's explore these in more detail.

10
00:00:33,670 --> 00:00:35,530
You can check out the links in the lecture notebook.

11
00:00:35,620 --> 00:00:40,510
If you're looking for a link to the documentation online, or you can just Google search math python

12
00:00:40,510 --> 00:00:43,630
module and you'll first link will be the official documentation.

13
00:00:44,110 --> 00:00:45,550
Let's head over to a Jupiter notebook.

14
00:00:46,150 --> 00:00:46,370
All right.

15
00:00:46,390 --> 00:00:47,740
Here we are at the Jupiter notebook.

16
00:00:48,040 --> 00:00:52,600
Let's go ahead and start off with the math module going to import math.

17
00:00:53,350 --> 00:00:57,790
And if you want to figure out what's available to you in math, you can actually just call help math.

18
00:00:58,360 --> 00:01:03,820
And I'll describe that this modules always available and essentially has a bunch of mathematical functions

19
00:01:03,820 --> 00:01:04,270
available.

20
00:01:04,870 --> 00:01:08,980
Everything from trigonometric functions like arc cosine or hyperbolic.

21
00:01:08,980 --> 00:01:10,060
Cosine or inverse.

22
00:01:10,360 --> 00:01:11,470
Hyperbolic, cosine.

23
00:01:11,830 --> 00:01:17,800
All the way to simpler things like taking the floor of a number, essentially just taking a large integer

24
00:01:17,800 --> 00:01:20,470
that is less than or equal to X. and so on.

25
00:01:20,790 --> 00:01:21,520
Murti Explorer.

26
00:01:21,550 --> 00:01:24,370
Just a couple of these functions and the rest.

27
00:01:24,400 --> 00:01:28,000
I'll leave it up to you if you ever need them to just look it up in the documentation.

28
00:01:28,270 --> 00:01:32,860
But just always keep in mind that there's a basic mathematical function that is common enough to be

29
00:01:32,860 --> 00:01:34,840
found in any mathematics textbook.

30
00:01:35,170 --> 00:01:37,660
It's probably available to you inside the math module.

31
00:01:37,690 --> 00:01:43,270
He just needs to say from math import whatever function you're looking at or just call math function.

32
00:01:44,620 --> 00:01:47,140
So let's go ahead and check this out.

33
00:01:47,740 --> 00:01:50,200
So let's see how we can round the numbers.

34
00:01:51,300 --> 00:01:56,310
If we have a value called four point three five, for instance, there's a couple of different ways

35
00:01:56,310 --> 00:01:58,590
we can do this using the math module.

36
00:01:58,710 --> 00:02:03,960
I can say floor, which essentially is going to bring this down to the integer that is less than or

37
00:02:03,990 --> 00:02:04,980
equal to the value.

38
00:02:05,520 --> 00:02:06,390
So that should be for.

39
00:02:07,620 --> 00:02:08,730
And then I can do ceiling.

40
00:02:09,060 --> 00:02:16,710
So if I said Martha SEAL for sealing, it essentially works the other way, giving you the next integer

41
00:02:16,710 --> 00:02:16,900
up.

42
00:02:18,000 --> 00:02:21,570
So essentially rounds up regardless if it's closer or not to the floor value.

43
00:02:22,050 --> 00:02:23,910
If you want essentially a true rounding.

44
00:02:24,270 --> 00:02:25,920
So you can call it round.

45
00:02:26,000 --> 00:02:28,050
It's actually built in to the python itself.

46
00:02:28,320 --> 00:02:31,170
It's not part of the math module because it's common enough to use.

47
00:02:31,680 --> 00:02:34,990
So if you just say round four point three or four point three five.

48
00:02:35,010 --> 00:02:37,200
Since it's closer to four, it will round it down.

49
00:02:37,710 --> 00:02:42,360
And if you do something like round four point five, which is kind of right in the middle, it'll go

50
00:02:42,360 --> 00:02:42,960
to four.

51
00:02:43,200 --> 00:02:45,780
But notice what happens if I say round.

52
00:02:46,760 --> 00:02:47,730
Five point five.

53
00:02:48,370 --> 00:02:49,570
It goes to six.

54
00:02:49,900 --> 00:02:50,920
So what's actually happening here?

55
00:02:50,950 --> 00:02:53,050
Why is one going down and one going up?

56
00:02:53,560 --> 00:02:58,780
It's because in general, you want to choose to always round to either all evens or all odds when you're

57
00:02:58,780 --> 00:03:02,260
starting to, for instance, create some sort of set of rules.

58
00:03:02,380 --> 00:03:07,960
And the reason for that is if you always chose to round down when it came to a point five, split something

59
00:03:07,960 --> 00:03:08,680
right in the middle.

60
00:03:09,010 --> 00:03:12,400
Then all your estimates over time would be lower than they should be.

61
00:03:13,030 --> 00:03:18,370
Or if you chose to round up, then over time, all these rounding errors would be higher than they should

62
00:03:18,370 --> 00:03:18,580
be.

63
00:03:19,000 --> 00:03:25,030
If you choose a rule based off a number being even or odd, then you start to even itself out because

64
00:03:25,030 --> 00:03:28,270
you'll round down as many times, hopefully as you round up.

65
00:03:28,510 --> 00:03:34,720
So that's why you'll see this round at point five go towards the even numbers like four and six when

66
00:03:34,720 --> 00:03:36,730
it comes to some split right down the middle.

67
00:03:37,730 --> 00:03:42,850
OK, let's show you how you can grab mathematical constants so you can call these right off of the math

68
00:03:42,850 --> 00:03:44,820
library, such as Math Pi.

69
00:03:45,610 --> 00:03:51,130
And that gives you the mathematical constant pi or if you want, you can save from math, import PI

70
00:03:51,400 --> 00:03:53,320
and then just you something like PI in your code.

71
00:03:53,890 --> 00:04:00,640
There's also e very common number and there's representations of things like infinity or not a number.

72
00:04:01,330 --> 00:04:04,840
So math, infinity or math, not a number.

73
00:04:05,650 --> 00:04:06,970
Something else I want to point out.

74
00:04:07,090 --> 00:04:12,190
If you find yourself that you're working with heavy enough mathematics that you're going to be using

75
00:04:12,190 --> 00:04:14,710
math, infinity or math, but not a number.

76
00:04:14,980 --> 00:04:17,290
You may want to check out the number PI Library.

77
00:04:17,620 --> 00:04:19,270
That's number PI.

78
00:04:20,020 --> 00:04:21,040
Check it out online.

79
00:04:21,070 --> 00:04:26,470
It's a library specifically designed for numerical processing and it's highly efficient and goes much

80
00:04:26,470 --> 00:04:28,810
deeper than Python built in math module.

81
00:04:28,990 --> 00:04:32,500
But for our use cases in this course, the math module is more than enough.

82
00:04:33,130 --> 00:04:37,210
Finally, let's go ahead and see how it could do logarithmic values with the math function.

83
00:04:38,020 --> 00:04:39,500
So here we have math E!

84
00:04:40,030 --> 00:04:41,240
So we have two points, every one.

85
00:04:41,740 --> 00:04:48,250
And if you take the log base, E of math E, it should give you one.

86
00:04:48,550 --> 00:04:49,570
So what does it actually mean?

87
00:04:49,990 --> 00:04:51,940
Essentially, it's just the.

88
00:04:53,490 --> 00:04:57,600
Based on the power of some N is equal to the variable you provide.

89
00:04:58,170 --> 00:05:01,890
So let me show you this by showing you how to create a custom log base.

90
00:05:02,640 --> 00:05:04,380
So I will call math that log.

91
00:05:04,890 --> 00:05:11,100
You'll notice it provides an X and it's up to you to provide a base return the logarithm of X to the

92
00:05:11,100 --> 00:05:11,820
given base.

93
00:05:12,330 --> 00:05:18,330
So if I say one hundred is X base ten, it returns back to.

94
00:05:18,810 --> 00:05:20,820
So what does that actually solving for it.

95
00:05:20,850 --> 00:05:21,720
Solving for.

96
00:05:22,290 --> 00:05:26,580
What number do I have to take 10 to the power of in order to get one hundred.

97
00:05:27,120 --> 00:05:32,220
So that means I'd have to do 10 to the power of two to get one hundred.

98
00:05:32,760 --> 00:05:37,290
So if you're not familiar, if logarithmic, just mathematics in general, then you probably won't be

99
00:05:37,290 --> 00:05:39,090
using this in Python anyways.

100
00:05:40,120 --> 00:05:43,150
And finally, there's trigonometric functions and other really useful one.

101
00:05:43,530 --> 00:05:47,080
You can take math sign of some number.

102
00:05:47,450 --> 00:05:51,150
It turns it back in radians if you want to turn it into the Griese.

103
00:05:51,340 --> 00:05:54,010
You simply say Matha degrees.

104
00:05:54,330 --> 00:05:56,770
It can convert something from radiance into degrees.

105
00:05:56,800 --> 00:06:04,660
For example, PI over two degrees is 90 degrees or math radians 180 degree ingredients.

106
00:06:04,730 --> 00:06:08,080
Let's make sure we spell that right is going to be equal to PI.

107
00:06:08,740 --> 00:06:13,900
Again, this is basic trigonometry and that's all it within the math module that's built into Python.

108
00:06:14,740 --> 00:06:18,080
Let's move on and discuss the random module the ran.

109
00:06:18,080 --> 00:06:23,560
The module allows us to create random numbers and we can even set a seed to produce the same random

110
00:06:23,560 --> 00:06:24,730
set every time.

111
00:06:25,300 --> 00:06:29,860
Now, an explanation of how computers attempt to generate random numbers is really beyond the scope

112
00:06:29,860 --> 00:06:35,500
of this course, because it actually involves some pretty high level mathematics on how computers use

113
00:06:35,530 --> 00:06:38,410
what are known as pseudo random number generators.

114
00:06:38,950 --> 00:06:41,260
Now, I've linked to Wikipedia articles.

115
00:06:41,290 --> 00:06:44,980
In case you are interested in this in the lecture notebook.

116
00:06:45,010 --> 00:06:47,920
So if you check out the lecture notebook, we bring it over real quick.

117
00:06:48,430 --> 00:06:51,070
When we discussed around the modules, there's two useful links here.

118
00:06:51,430 --> 00:06:53,740
One is discussing pseudo random number generators.

119
00:06:53,830 --> 00:06:55,420
It's a really interesting feel at mathematics.

120
00:06:55,960 --> 00:07:00,760
Really interesting history and how you can actually get a computer to show something that looks random

121
00:07:01,060 --> 00:07:05,590
as well as random seed, which is then how can you get the same batch of random numbers if you want

122
00:07:05,590 --> 00:07:07,480
to test the same code over and over again?

123
00:07:07,930 --> 00:07:12,760
So let's go ahead and start trying to understand a seed made bringing this back over here.

124
00:07:13,870 --> 00:07:17,130
To begin, I'm going to import random.

125
00:07:18,420 --> 00:07:22,890
And let's start by checking out the random Dot Rand I.A..

126
00:07:24,060 --> 00:07:29,280
So if we check out what this function does, it just returns a random integer in range.

127
00:07:29,380 --> 00:07:32,090
And B, and it includes both end points.

128
00:07:32,610 --> 00:07:35,040
Which means if I type in right here.

129
00:07:35,280 --> 00:07:40,170
Zero and one hundred and run, this is going to return back some random integer.

130
00:07:40,560 --> 00:07:45,270
If you ran this, it's more than likely than not that you got a different number than I did.

131
00:07:45,480 --> 00:07:48,090
It's basically a one percent chance that you got same number.

132
00:07:48,090 --> 00:07:48,390
I did.

133
00:07:48,450 --> 00:07:52,950
And every time you run this, you'll notice you start getting back a different random integer.

134
00:07:53,670 --> 00:07:58,740
It's possible, the exact same one in a row, but that's one over 100 times one over 100 chance.

135
00:07:59,190 --> 00:07:59,400
OK.

136
00:07:59,820 --> 00:08:02,010
So we know we can produce random integers.

137
00:08:02,490 --> 00:08:07,950
However, what if I'm testing a code script and I want to make adjustments to this script of code?

138
00:08:08,280 --> 00:08:10,320
But I want to test it on random numbers.

139
00:08:10,380 --> 00:08:12,150
But the same batch of random numbers.

140
00:08:12,600 --> 00:08:14,310
This is where the seed comes into play.

141
00:08:14,760 --> 00:08:20,700
Setting the seed allows us to start from a seated pseudo random number generator, which means the same

142
00:08:20,700 --> 00:08:22,530
random numbers will show up in a series.

143
00:08:23,240 --> 00:08:23,450
OK.

144
00:08:23,790 --> 00:08:29,640
When it comes to running this within Jupiter, in order to make sure this works, your seed should be

145
00:08:29,640 --> 00:08:34,020
in the same cell as whatever function you're using to generate random numbers.

146
00:08:34,950 --> 00:08:40,020
So we call random that seed and then it's up to you to choose some seed value.

147
00:08:40,400 --> 00:08:42,660
Note, this value is completely arbitrary.

148
00:08:42,690 --> 00:08:43,830
It can be whatever you want.

149
00:08:44,220 --> 00:08:48,150
Often people do one, a one just because it's easy to remember and it looks nice.

150
00:08:48,630 --> 00:08:53,310
Other times people, especially libraries, often use 40 two because of Hitchhiker's Guide to the Galaxy.

151
00:08:53,370 --> 00:08:54,240
Just the reference of that.

152
00:08:54,540 --> 00:08:57,660
But again, this value right here is completely up to you.

153
00:08:57,690 --> 00:08:58,890
It's totally arbitrary.

154
00:08:59,790 --> 00:09:00,900
So we choose that seed.

155
00:09:01,050 --> 00:09:06,780
And in the same cell, what I want you to do if you're following along with me is call Rande I.A..

156
00:09:07,790 --> 00:09:08,990
And then zero, one hundred.

157
00:09:09,560 --> 00:09:10,550
Go ahead and run this.

158
00:09:11,060 --> 00:09:14,180
And if you set this up, both of the seed, the same value I did.

159
00:09:14,600 --> 00:09:18,290
You should have gotten the same random integer if these were in the same cell.

160
00:09:18,320 --> 00:09:20,720
So notice here we get back seventy four.

161
00:09:21,410 --> 00:09:22,360
And note what's going to happen.

162
00:09:22,380 --> 00:09:23,840
I'm going to run this again one more time.

163
00:09:24,350 --> 00:09:25,300
Again I get seventy four.

164
00:09:25,370 --> 00:09:27,110
Doesn't matter how many times I run the cell.

165
00:09:27,710 --> 00:09:32,810
I always get back seventy four because the seed set and the seed essentially starts the same series

166
00:09:32,900 --> 00:09:33,890
of random integers.

167
00:09:34,310 --> 00:09:35,210
So if I copy this.

168
00:09:36,570 --> 00:09:37,860
And paste it in the next cell.

169
00:09:38,520 --> 00:09:40,650
If I run this, I will get 24.

170
00:09:41,930 --> 00:09:44,320
And if I run this one, I'll get 69.

171
00:09:45,030 --> 00:09:47,440
However, let's go ahead and reset our seed.

172
00:09:48,040 --> 00:09:51,810
Notice now all three cells keep repeating those same random numbers.

173
00:09:52,170 --> 00:09:57,390
But if I take one of these and I haven't set the seed or reset it again and I keep calling this one,

174
00:09:57,990 --> 00:10:02,310
I'll get the same series of random integers each time.

175
00:10:02,370 --> 00:10:05,280
So essentially what that means is if I were to print these out.

176
00:10:07,340 --> 00:10:09,860
Let's go ahead and copy and paste this a couple times.

177
00:10:12,470 --> 00:10:15,060
Print this out five times and we'll reset the seed.

178
00:10:17,280 --> 00:10:25,050
Within the cell, what I should expect is that this first one produces 74 and that this next one produces

179
00:10:25,100 --> 00:10:29,310
24, since I can see right here that it goes 70 for 24.

180
00:10:29,380 --> 00:10:31,560
And I believe the next one was sixty nine.

181
00:10:32,600 --> 00:10:33,170
And so on.

182
00:10:33,230 --> 00:10:35,560
So when I run this, I get back those results.

183
00:10:35,720 --> 00:10:37,660
Seventy four, twenty four and sixty nine.

184
00:10:38,000 --> 00:10:42,320
And that's because I set the seed right before calling that first random integer.

185
00:10:42,830 --> 00:10:48,770
So the thing I really want to clarify here is just because you set a seed doesn't mean that every time

186
00:10:48,770 --> 00:10:51,220
you call random integer, it always returns 74.

187
00:10:51,980 --> 00:10:59,450
Instead, you should think of it as setting a seed for any sequence of basically infinite random numbers.

188
00:11:00,200 --> 00:11:00,450
OK.

189
00:11:01,070 --> 00:11:04,940
So we already know that we can grab random integers using the Rande I.A. function.

190
00:11:05,390 --> 00:11:08,480
There's a couple of other functions I want to show you with the random library.

191
00:11:08,930 --> 00:11:15,860
The first one I want to show you is taking just a random item from a list so I can say my list is equal

192
00:11:15,860 --> 00:11:19,760
to list range zero to 20.

193
00:11:20,480 --> 00:11:24,590
So now I have a list of a bunch of integers, zero up to 19.

194
00:11:25,130 --> 00:11:28,010
And let's imagine I wanted to grab just a random item from this.

195
00:11:28,670 --> 00:11:31,220
I could say random dot choice.

196
00:11:32,250 --> 00:11:32,880
My list.

197
00:11:33,360 --> 00:11:33,930
Run this.

198
00:11:34,290 --> 00:11:36,820
And it chooses some random integer from this.

199
00:11:36,840 --> 00:11:38,250
In this case, it was 16 for me.

200
00:11:38,730 --> 00:11:41,040
Keep in mind, does not permanently affect your list.

201
00:11:41,400 --> 00:11:43,440
16 is still there within your list.

202
00:11:44,310 --> 00:11:46,650
Now, that's just for grabbing one item.

203
00:11:47,100 --> 00:11:49,170
Let's imagine you want to grab multiple items.

204
00:11:49,220 --> 00:11:52,200
Let's say I want five random numbers from this list.

205
00:11:52,680 --> 00:11:54,540
Well, there's two different ways of doing that.

206
00:11:55,080 --> 00:11:58,230
One is to allow yourself to pick the same number twice.

207
00:11:58,560 --> 00:12:04,470
So that's sampling with replacement or you sample without replacement means.

208
00:12:04,800 --> 00:12:07,950
Once you've picked out an item from this list, you can't pick it again.

209
00:12:08,880 --> 00:12:13,110
So let's show you first sample with replacement.

210
00:12:13,770 --> 00:12:19,860
So, again, that's going to be allowing same the same integer in this case to be chosen more than once.

211
00:12:20,610 --> 00:12:24,330
And the way that works is we call random dot choices.

212
00:12:24,960 --> 00:12:26,370
And there's two main parameters here.

213
00:12:26,940 --> 00:12:28,080
It's the population.

214
00:12:28,110 --> 00:12:31,050
What you're actually picking from, which is my list.

215
00:12:31,950 --> 00:12:34,390
And then K, which is how many items you want from this.

216
00:12:34,950 --> 00:12:36,390
Let's make K equal to ten.

217
00:12:36,720 --> 00:12:40,290
So that means I'm going to randomly pick a number from this list.

218
00:12:40,410 --> 00:12:44,280
I'm going to do that 10 times and I'll return a list of those numbers.

219
00:12:45,060 --> 00:12:52,290
Since I'm sampling with replacement, I should be able to see some numbers duplicated and with K equal

220
00:12:52,290 --> 00:12:55,650
to ten, it's highly likely that you picked the same number at least twice.

221
00:12:56,040 --> 00:13:00,780
So here we can see four was chosen twice as well as one, actually four chosen three times.

222
00:13:01,230 --> 00:13:03,790
OK, so let's sampling with replacements.

223
00:13:04,590 --> 00:13:08,430
Now let's imagine I want to sample without replacement.

224
00:13:09,930 --> 00:13:10,710
So what does that mean?

225
00:13:11,190 --> 00:13:15,360
It means that once I've chosen a number, I don't get to pick it again.

226
00:13:15,780 --> 00:13:19,530
And to do that, that's going to be random dot sample.

227
00:13:20,400 --> 00:13:21,390
Same parameters.

228
00:13:21,480 --> 00:13:22,830
We provide a population.

229
00:13:23,840 --> 00:13:24,530
My list.

230
00:13:24,740 --> 00:13:27,620
And if I say Kay is equal to 10, we run this.

231
00:13:27,920 --> 00:13:32,450
Notice that none of these numbers are repeated because once you picked it, you're not allowed to pick

232
00:13:32,450 --> 00:13:32,870
it again.

233
00:13:33,630 --> 00:13:33,790
Okay.

234
00:13:34,580 --> 00:13:36,470
Finally, we can show you how to shuffle a list.

235
00:13:36,800 --> 00:13:41,420
Keep in mind, this will permanently affect the list, so you don't need to actually assign the result

236
00:13:41,420 --> 00:13:41,900
to anything.

237
00:13:42,890 --> 00:13:43,850
So here's my list.

238
00:13:43,940 --> 00:13:47,240
If I wanted to shuffle it in place, I can say random dot shuffle.

239
00:13:49,340 --> 00:13:50,000
My list.

240
00:13:51,450 --> 00:13:53,430
No, I do not do any sort of reassignment.

241
00:13:53,640 --> 00:13:55,050
It just does it in place.

242
00:13:55,110 --> 00:13:57,480
So the list is permanently affected.

243
00:13:58,200 --> 00:14:02,550
And in case you're interested in probability distributions, there are a few that are available to you

244
00:14:02,580 --> 00:14:03,660
in the random library.

245
00:14:04,320 --> 00:14:06,150
So there's a uniform distribution.

246
00:14:06,750 --> 00:14:12,300
In case you're familiar with it, which is essentially just going to randomly pick a value between A

247
00:14:12,300 --> 00:14:12,800
and B..

248
00:14:12,960 --> 00:14:16,950
So let's say A is equal to zero and B is equal to one hundred.

249
00:14:17,370 --> 00:14:19,080
This is a continuous distribution.

250
00:14:19,110 --> 00:14:21,150
So that means floating point numbers are allowed.

251
00:14:21,510 --> 00:14:26,250
So you can pick any number between zero and one hundred up to a very large floating point precision.

252
00:14:26,310 --> 00:14:27,510
So it's not just integers.

253
00:14:28,140 --> 00:14:28,860
So you run this.

254
00:14:28,920 --> 00:14:31,410
And in this case, I chose zero point six five.

255
00:14:32,010 --> 00:14:37,410
And the reason that it's called uniform is because every number between zero 100 has the same likelihood

256
00:14:37,470 --> 00:14:38,190
of being chosen.

257
00:14:39,700 --> 00:14:44,260
Now, there's also a normal or Gaussian distribution that you can choose.

258
00:14:45,230 --> 00:14:48,050
So that's callable by random Gauss.

259
00:14:49,160 --> 00:14:52,460
And this takes in a mean and a standard deviation.

260
00:14:52,790 --> 00:14:54,850
If you're not familiar with that, then don't worry about it.

261
00:14:54,860 --> 00:14:58,460
You probably wouldn't be using this if you don't know what a normal distribution is.

262
00:14:58,700 --> 00:15:04,010
But in case you do know, you should be aware that it is available to you within Python's built in random

263
00:15:04,010 --> 00:15:04,490
module.

264
00:15:04,900 --> 00:15:09,920
So is it Gaussian or normal distribution centered at zero with a standard deviation of one?

265
00:15:10,760 --> 00:15:13,520
We run that and it picks just some random value from that.

266
00:15:14,180 --> 00:15:18,980
So something we want to know is if you're using Python to the point where you find yourself calling

267
00:15:19,370 --> 00:15:22,730
uniform random distributions or random normal distributions.

268
00:15:23,060 --> 00:15:25,670
You should really look into the NUM Pi library.

269
00:15:26,060 --> 00:15:28,730
We teach it in some of my data science and machine learning courses.

270
00:15:28,760 --> 00:15:32,420
You can check it out there or you can just check out the free online documentation.

271
00:15:32,750 --> 00:15:37,460
It's a really powerful library and I would highly recommend that if you're calling random that uniform

272
00:15:37,550 --> 00:15:40,850
or random gauss, you just start switching over to num pi at that point.

273
00:15:41,480 --> 00:15:42,680
OK, thanks.

274
00:15:42,800 --> 00:15:43,850
And I'll see at the next lecture.
