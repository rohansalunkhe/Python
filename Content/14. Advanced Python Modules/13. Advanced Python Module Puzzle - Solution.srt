1
00:00:05,380 --> 00:00:06,300
Welcome back, everyone.

2
00:00:06,580 --> 00:00:10,780
In this lecture, we're going to go through a solution for the Advanced Modules exercise puzzle.

3
00:00:11,140 --> 00:00:11,800
Let's get started.

4
00:00:12,700 --> 00:00:14,440
Okay, so here I am at the Jupiter notebook.

5
00:00:14,530 --> 00:00:19,240
I've gone ahead and opened up a new untitled notebook in the same location as the exercise puzzle.

6
00:00:19,330 --> 00:00:20,410
And exercise solutions.

7
00:00:20,680 --> 00:00:23,740
Since the first thing I need to do, step one is to unzip this file.

8
00:00:24,520 --> 00:00:25,300
Go ahead and do that.

9
00:00:26,200 --> 00:00:27,880
I'll use the Shell Utility Library.

10
00:00:29,060 --> 00:00:32,730
So I will import it and then we'll simply use unpack archive.

11
00:00:33,390 --> 00:00:35,580
So you should be able to say show utility.

12
00:00:37,250 --> 00:00:37,640
Unpack.

13
00:00:37,680 --> 00:00:38,310
Archive.

14
00:00:38,760 --> 00:00:43,050
And then we need to tell it what actual look, its location, the zip file is in.

15
00:00:43,440 --> 00:00:46,920
Since I'm in the same folder, I just need to essentially pass on the zip file name.

16
00:00:47,460 --> 00:00:50,270
So in this case, I can simply say, unzip me.

17
00:00:51,200 --> 00:00:51,760
Four.

18
00:00:52,090 --> 00:00:57,400
And then I can tap autocomplete this now the second thing is where we actually want to extract it,

19
00:00:57,400 --> 00:00:57,630
too.

20
00:00:58,180 --> 00:01:02,320
In my case, I'll just leave this as an empty string because I want to extract it at my current location.

21
00:01:02,950 --> 00:01:04,450
And then the format is.

22
00:01:06,090 --> 00:01:06,280
All right.

23
00:01:06,340 --> 00:01:07,120
That's step one.

24
00:01:07,170 --> 00:01:10,480
If you come over here, you should notice that there's extracted content.

25
00:01:10,660 --> 00:01:15,140
And if you open this up, you can now see we have one, two, three, four, five.

26
00:01:15,220 --> 00:01:17,080
And then the extra instructions, that text.

27
00:01:17,530 --> 00:01:22,870
Also, the quick note extracted underscored content is the default file name that shell utility will

28
00:01:22,870 --> 00:01:23,710
provide for us.

29
00:01:24,250 --> 00:01:29,170
So we want to open up instructions that text, while technically I can just click this open, but let's

30
00:01:29,170 --> 00:01:31,300
go ahead and open it with Python as a quick review.

31
00:01:32,050 --> 00:01:33,220
Lots of different ways you can do this.

32
00:01:33,700 --> 00:01:34,690
I'll use a width statement.

33
00:01:35,320 --> 00:01:36,460
So we'll say with open.

34
00:01:37,330 --> 00:01:39,910
And then inside of extracted content.

35
00:01:40,540 --> 00:01:42,760
I want to open the instructions that text file.

36
00:01:42,850 --> 00:01:44,310
I'm just using tab to autocomplete.

37
00:01:44,350 --> 00:01:46,720
There will open it as f.

38
00:01:47,960 --> 00:01:50,310
Then we'll grab the content, say, FDR.

39
00:01:50,450 --> 00:01:50,780
Read.

40
00:01:52,110 --> 00:01:54,210
And we'll go ahead and print out that content.

41
00:01:54,780 --> 00:01:56,510
I could also just say print F-stop read.

42
00:01:57,930 --> 00:02:00,720
OK, so step one, we unzip the file.

43
00:02:00,870 --> 00:02:01,590
Good job with that.

44
00:02:01,920 --> 00:02:06,450
We now see those five folders and each of those has a lot of random dot text files.

45
00:02:06,870 --> 00:02:11,430
So if I take a look at one of these, such as one notice, there's a ton of text files here.

46
00:02:11,610 --> 00:02:15,870
And if I open up one of these text files, it's essentially just a bunch of lorem ipsum text, just

47
00:02:15,870 --> 00:02:17,280
a bunch of Latin filler text.

48
00:02:17,760 --> 00:02:19,290
So what do we have to do here?

49
00:02:20,330 --> 00:02:24,350
Within one of these text files is a telephone number formatted like this.

50
00:02:24,940 --> 00:02:25,210
OK.

51
00:02:25,430 --> 00:02:31,550
So we need to use the Python OS module and regular expressions to basically iterate through all of those

52
00:02:31,550 --> 00:02:34,730
text files, open it and search for the telephone number.

53
00:02:35,240 --> 00:02:37,070
So let's go ahead to move on to step three.

54
00:02:37,140 --> 00:02:40,040
Well, I will create a regular expression to find the link.

55
00:02:40,610 --> 00:02:44,240
So this is pretty similar to what we covered in the regular expression lecture.

56
00:02:45,640 --> 00:02:46,960
So I know that the pattern.

57
00:02:47,440 --> 00:02:51,370
If I were to look up in the table, there's different ways I could write this pattern.

58
00:02:51,460 --> 00:02:53,860
But I know that it should be a digit.

59
00:02:54,580 --> 00:02:58,720
Three of them, then three more digits after the dash.

60
00:03:00,880 --> 00:03:03,310
Another dash and then four digits.

61
00:03:04,840 --> 00:03:07,060
OK, so there's our pattern.

62
00:03:07,630 --> 00:03:10,930
If you're confused on this, you can review the regular expressions lecture series.

63
00:03:11,260 --> 00:03:16,000
But basically this says three digits, a dash and others that are three digits, a dash and then a set

64
00:03:16,000 --> 00:03:16,780
of four digits.

65
00:03:17,350 --> 00:03:21,700
Now, what I would always recommend doing is before you just go ahead and use this pattern, you just

66
00:03:21,700 --> 00:03:25,270
test it on a single string to make sure it's returning what you expect.

67
00:03:25,540 --> 00:03:33,280
So I will say my test string is equal to here is a phone number and we'll say it's one, two, three.

68
00:03:33,430 --> 00:03:34,210
One, two, three.

69
00:03:34,480 --> 00:03:35,290
One, two, three, four.

70
00:03:35,870 --> 00:03:36,100
OK.

71
00:03:37,000 --> 00:03:37,990
So there's our phone number.

72
00:03:38,260 --> 00:03:40,710
Let's go ahead and make sure that our pattern can find it.

73
00:03:41,170 --> 00:03:45,130
We can use find all for this pattern versus the test string.

74
00:03:45,820 --> 00:03:46,300
Run that.

75
00:03:46,620 --> 00:03:47,980
And it looks like it found the phone number.

76
00:03:48,280 --> 00:03:48,760
Perfect.

77
00:03:49,240 --> 00:03:54,520
Next, I'm going to make this into a function and the function is going to accept a file, the text

78
00:03:54,520 --> 00:03:54,850
file.

79
00:03:55,270 --> 00:03:58,540
It's going to open it up and then search for the pattern within that file.

80
00:03:59,260 --> 00:04:04,420
So I will call this the F search it takes in the file.

81
00:04:04,540 --> 00:04:10,990
So I'm going to iterate through all those text files and then the pattern is going to be what I defined

82
00:04:10,990 --> 00:04:11,410
up here.

83
00:04:11,500 --> 00:04:14,260
So we'll go ahead and copy this.

84
00:04:15,720 --> 00:04:16,160
Paste it.

85
00:04:18,470 --> 00:04:19,950
And we'll first open up the file.

86
00:04:20,120 --> 00:04:24,420
So I'll say F is equal to open the file in this case.

87
00:04:24,440 --> 00:04:25,400
I just want to read the file.

88
00:04:25,430 --> 00:04:26,760
So we'll say mode is equal to R..

89
00:04:27,740 --> 00:04:31,090
And we'll grab the text of the file by saying F thought read.

90
00:04:31,610 --> 00:04:35,660
So now that I have this text, it should basically act like the test string I had here.

91
00:04:36,110 --> 00:04:40,100
And instead of using find all, I'll just go ahead and use search to search through this text.

92
00:04:40,640 --> 00:04:46,100
So I'll say if regular expression search.

93
00:04:47,000 --> 00:04:52,820
Pattern text, meaning it found that there that I'm going to return the results.

94
00:04:53,180 --> 00:04:55,010
So I'll say return.

95
00:04:56,450 --> 00:04:58,520
Search pattern.

96
00:04:59,450 --> 00:05:02,990
Text and technically is lots of different ways you could do this and set up your function.

97
00:05:04,160 --> 00:05:07,070
Else I'm going to just return an empty string.

98
00:05:07,700 --> 00:05:12,650
So essentially what this is going to do is it's going to open up the file, look for a pattern there.

99
00:05:13,040 --> 00:05:14,930
And then if it finds it, return it.

100
00:05:16,000 --> 00:05:19,300
So we'll go ahead and create that search function.

101
00:05:19,840 --> 00:05:25,480
Now, what I have to do is I have to walk through all the files here, essentially using the OS module,

102
00:05:25,870 --> 00:05:30,160
because I need to not just go through all the files in this folder, but I need to go through all the

103
00:05:30,160 --> 00:05:33,370
files and folders one, two, three, four and five.

104
00:05:33,880 --> 00:05:38,860
So this is too much for me to do manually, which means I will use the OS module for this.

105
00:05:39,070 --> 00:05:46,900
I will import OS to make our results a list and then I'll do the following, I will say for folder.

106
00:05:48,770 --> 00:05:49,670
Sub folders.

107
00:05:51,560 --> 00:05:58,030
Files and OS walk and I'll say, just go ahead and do this in the current directory.

108
00:05:58,050 --> 00:05:59,600
Plus extracted content.

109
00:06:00,290 --> 00:06:08,390
So say OS to get current working directory and then concatenate it with the string extracted content.

110
00:06:10,210 --> 00:06:13,930
So in case you are interested in what this looks like, I'll show you right here above it.

111
00:06:15,280 --> 00:06:16,690
So I'm going to insert this.

112
00:06:16,960 --> 00:06:22,120
And so it's basically saying my full current location plus extracted the content because recall, my

113
00:06:22,120 --> 00:06:27,010
untitled notebook is actually kind of sitting above this extracted content folder.

114
00:06:28,720 --> 00:06:31,720
So I want to go ahead and I'll provide that full file path.

115
00:06:34,280 --> 00:06:35,850
And now what do we want to do?

116
00:06:35,940 --> 00:06:37,830
We'll go ahead and search through this.

117
00:06:37,920 --> 00:06:45,630
So put the colon in there and say for F and files, essentially all the text files actually don't care

118
00:06:45,630 --> 00:06:48,540
too much about marking down sub folders or folders for now.

119
00:06:48,870 --> 00:06:51,270
I just want to search every single text file.

120
00:06:52,350 --> 00:06:54,870
Then I will say the full path.

121
00:06:56,000 --> 00:07:02,550
To this text file is equal to folder plus, in my case, two back slashes since I'm on windows.

122
00:07:02,880 --> 00:07:04,260
Maybe a forward slash for you.

123
00:07:04,590 --> 00:07:08,730
If you're on Mac OS or Linux and then the file itself.

124
00:07:08,880 --> 00:07:10,110
So why do I need to do this?

125
00:07:10,530 --> 00:07:14,520
Well, recall that I'm going to be calling this search function on each file.

126
00:07:15,030 --> 00:07:17,970
And what I want to do is make sure I don't get any file path errors.

127
00:07:18,270 --> 00:07:21,990
So in order to do that, what I'm going to do is call the folder.

128
00:07:22,290 --> 00:07:25,380
That happens to be in and then the file itself as well.

129
00:07:26,930 --> 00:07:31,400
And if you're kind of confused on this as far as what this is actually doing, you could if you want

130
00:07:31,400 --> 00:07:36,380
to print out the full path, but you'll just get a bunch of print statements of every single file in

131
00:07:36,380 --> 00:07:36,590
there.

132
00:07:37,430 --> 00:07:41,840
But I'll recommend doing that if you're still confused on what the full path actually looks like.

133
00:07:43,160 --> 00:07:44,300
And then we'll say search.

134
00:07:45,500 --> 00:07:46,100
Full path.

135
00:07:47,150 --> 00:07:47,340
All right.

136
00:07:47,480 --> 00:07:49,970
So pretty much very simple function here.

137
00:07:50,450 --> 00:07:51,500
But what are we actually doing?

138
00:07:52,070 --> 00:07:56,810
All we're doing is we're going to do a walk through the entire extracted content folder.

139
00:07:57,230 --> 00:07:59,690
Going to look at each file, grab its file path.

140
00:08:00,080 --> 00:08:04,520
And then I'm going to perform my search function, which is up here.

141
00:08:04,640 --> 00:08:10,130
Recall, this is my custom search function, not regular express and search and my search function takes

142
00:08:10,130 --> 00:08:12,950
in the file and essentially searches for the pattern in it.

143
00:08:13,370 --> 00:08:16,140
If I can find the pattern, they'll return back that telephone number.

144
00:08:16,400 --> 00:08:18,260
If not, it just returns back and then t string.

145
00:08:18,620 --> 00:08:21,530
Alternatively, I could also just say pass and not return anything.

146
00:08:22,490 --> 00:08:22,720
OK.

147
00:08:23,210 --> 00:08:26,180
So we're gonna run this should take a little bit of time.

148
00:08:27,240 --> 00:08:29,940
And then we'll say, what are my results looking like?

149
00:08:30,690 --> 00:08:34,860
So four are in results.

150
00:08:35,490 --> 00:08:39,510
In fact, if I just take a look at results, you'll let us essentially a really long list of a bunch

151
00:08:39,510 --> 00:08:43,120
of empty strings, because I'm usually just returning an empty string.

152
00:08:43,170 --> 00:08:45,420
But somewhere in here, there is a telephone number.

153
00:08:45,780 --> 00:08:48,030
So here we have the actual match object.

154
00:08:48,390 --> 00:08:53,370
So what I'm going to do is say four are in results.

155
00:08:54,780 --> 00:08:58,230
If R is not equal to an empty string.

156
00:08:59,640 --> 00:09:05,940
Go ahead and print out are in group it together, it's actually grab the whole match.

157
00:09:06,450 --> 00:09:07,530
So let's go ahead and run that.

158
00:09:08,820 --> 00:09:09,110
OK.

159
00:09:09,780 --> 00:09:10,900
And there's the telephone number.

160
00:09:11,370 --> 00:09:14,850
So if you're wondering what this phone number actually does, I would encourage you to call it if you're

161
00:09:14,850 --> 00:09:19,860
in United States, because as of this filming at least and for the past 10 years, pretty much this

162
00:09:19,860 --> 00:09:24,750
telephone number has been a hotline where you can listen to a Hall and Oates song and you can check

163
00:09:24,750 --> 00:09:26,730
out the election notebook for more information on it.

164
00:09:27,030 --> 00:09:29,370
The official number is called Colin Oates.

165
00:09:29,910 --> 00:09:35,220
OK, so other adjustments you can make to this if you wanted to, you could just say else here, pass.

166
00:09:36,300 --> 00:09:41,130
And then this case, if you were to run this again, let's go ahead and make results and a..

167
00:09:41,130 --> 00:09:41,640
List.

168
00:09:42,890 --> 00:09:43,670
Run that again.

169
00:09:44,000 --> 00:09:46,130
Now, results might be a little cleaner for you.

170
00:09:47,060 --> 00:09:48,710
So it's just gonna be a bunch of nuns.

171
00:09:48,980 --> 00:09:51,510
So there you can just search for an actual object there.

172
00:09:51,830 --> 00:09:52,580
Really up to you.

173
00:09:52,610 --> 00:09:54,050
There's tons of different ways you could do this.

174
00:09:54,350 --> 00:09:59,120
But the main thing you have to do is figure out that this was a number hidden within all those text

175
00:09:59,120 --> 00:09:59,600
documents.

176
00:09:59,660 --> 00:10:01,970
And it should just be one telephone number you return.

177
00:10:02,510 --> 00:10:07,130
I added some other digits within some other text files that were not formatted correctly.

178
00:10:07,310 --> 00:10:10,550
So you should really only get this single Colin Oates phone number.

179
00:10:11,210 --> 00:10:11,480
OK.

180
00:10:11,780 --> 00:10:12,920
I hope you enjoyed that puzzle.

181
00:10:13,340 --> 00:10:15,230
And I'll see you at the next section of the course.
