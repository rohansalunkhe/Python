1
00:00:05,520 --> 00:00:08,910
Welcome, everyone, to this section of the course on advanced python modules.

2
00:00:10,220 --> 00:00:13,790
Python has several built in modules that we haven't really fully explored yet.

3
00:00:14,300 --> 00:00:19,150
In this section, we will dive deeper into some useful built in modules that already come prepackaged

4
00:00:19,160 --> 00:00:19,760
with Python.

5
00:00:20,180 --> 00:00:21,560
We'll explore their use cases.

6
00:00:21,860 --> 00:00:26,180
And then at the end, we'll give you a fun puzzle exercise to solve all they uses various modules.

7
00:00:27,360 --> 00:00:32,400
The modules will be covering in the section, the course is the collections module, the OS module and

8
00:00:32,400 --> 00:00:33,330
the date time module.

9
00:00:33,790 --> 00:00:38,520
Mathen Random Python debugger time it and in general, how to time your code.

10
00:00:39,030 --> 00:00:42,480
We'll also cover your regular expressions and how to search for patterns in text.

11
00:00:42,840 --> 00:00:48,660
And then we'll also cover how to unzip and zip files with Python through built in modules and also modules

12
00:00:48,660 --> 00:00:49,260
you can download.

13
00:00:50,120 --> 00:00:51,840
OK, so lots of stuff to cover.

14
00:00:52,110 --> 00:00:53,700
Let's not waste any more time and get started.

15
00:00:54,060 --> 00:00:54,990
I'll see at the next lecture.
