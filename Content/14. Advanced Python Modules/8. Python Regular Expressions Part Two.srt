1
00:00:05,720 --> 00:00:06,380
Welcome back.

2
00:00:06,980 --> 00:00:12,160
Now that we've learned how to actually do a pattern search using the regular expression library, it's

3
00:00:12,170 --> 00:00:19,050
time to continue our understanding with being able to build out these actual patterns with identifier

4
00:00:19,070 --> 00:00:19,790
syntax.

5
00:00:20,000 --> 00:00:25,010
So we'll look at a table and see how we can convert the items in that table to actual patterns.

6
00:00:25,490 --> 00:00:26,960
Let me head over to the Jupiter notebook.

7
00:00:27,440 --> 00:00:33,620
We just discussed how to use regular strings for pattern searching when using regular expressions library.

8
00:00:34,070 --> 00:00:39,230
Now let's move on to the key part of regular expressions, which is a special pattern, codes you can

9
00:00:39,230 --> 00:00:41,780
use to build out your own pattern sequences.

10
00:00:42,200 --> 00:00:44,480
We'll start off by discussing character, right, that the fires.

11
00:00:44,540 --> 00:00:48,060
And here we have a table that tries to explain what each character.

12
00:00:48,060 --> 00:00:48,170
Right.

13
00:00:48,170 --> 00:00:49,310
That the fire is.

14
00:00:50,210 --> 00:00:55,640
Basically, you'll have special character identifiers that start off with the backslash code and then

15
00:00:55,670 --> 00:00:58,400
a letter indicating what character you're referencing.

16
00:00:58,910 --> 00:01:03,440
So to start off, we can look at this example, backslash D and this description is a digit.

17
00:01:03,860 --> 00:01:07,280
Essentially, you would have this as a placeholder for any digit.

18
00:01:07,820 --> 00:01:12,980
So in your example pattern code, you could have a string file, underscore backslash D, backslash

19
00:01:12,980 --> 00:01:13,300
D.

20
00:01:13,730 --> 00:01:18,650
And it would look for anything that holds that pattern file underscore and then two digits following

21
00:01:18,650 --> 00:01:19,760
it for example.

22
00:01:19,760 --> 00:01:20,430
File underscore.

23
00:01:20,490 --> 00:01:22,670
Twenty five would match this pattern code.

24
00:01:23,300 --> 00:01:27,440
Here we can also see there's alphanumeric basically indicating letters or numbers.

25
00:01:27,530 --> 00:01:29,090
So alphabet or numerical.

26
00:01:29,480 --> 00:01:30,820
And that's backslash W.

27
00:01:31,340 --> 00:01:34,070
And this is what a an example pattern code could look like.

28
00:01:34,130 --> 00:01:35,490
Backslash W dash.

29
00:01:35,540 --> 00:01:37,280
And then three backslash ws.

30
00:01:37,670 --> 00:01:43,580
And that basically says any letter or number followed by a dash and then followed by three letters or

31
00:01:43,580 --> 00:01:44,910
numbers and alphanumeric.

32
00:01:45,200 --> 00:01:47,080
Also include things like underscore.

33
00:01:47,130 --> 00:01:48,110
So keep that in mind.

34
00:01:49,170 --> 00:01:50,040
Then there's whitespace.

35
00:01:50,580 --> 00:01:55,160
So if you're particularly looking for whitespace in the pattern, you can say backslash s.

36
00:01:55,250 --> 00:01:57,440
And that indicates a placeholder for whitespace.

37
00:01:57,900 --> 00:02:03,830
So an example pattern code for that would be the letter a backslash s, the letter B backslash s letter.

38
00:02:03,840 --> 00:02:04,250
See.

39
00:02:04,580 --> 00:02:09,350
And what this does is it indicates that you're looking for A Whitespace B, whitespace C.

40
00:02:09,780 --> 00:02:11,250
So here we have an example match.

41
00:02:11,820 --> 00:02:16,290
There's also a non digit character placeholder, which is backslash capital D.

42
00:02:16,680 --> 00:02:22,890
And you can think of the pattern here as lowercase is a digit of lowercase D and then uppercase D is

43
00:02:22,890 --> 00:02:23,560
a non digit.

44
00:02:23,940 --> 00:02:28,620
So you can see how these are related to each other and backslash T, backslash D, backslash D or they're

45
00:02:28,620 --> 00:02:33,720
all upper case is going to match up with, for instance, this pattern ABC, because none of these are

46
00:02:33,720 --> 00:02:35,820
digits and they're all in a row with each other.

47
00:02:36,330 --> 00:02:38,720
You also have not alphanumeric and non space.

48
00:02:38,790 --> 00:02:41,370
Again, the capitalized version of their counterparts.

49
00:02:41,760 --> 00:02:44,580
So here you can see some various example matches.

50
00:02:45,060 --> 00:02:47,670
Let's begin by checking out that example of the telephone number.

51
00:02:48,390 --> 00:02:55,620
We will say or text variable is my phone number is four zero eight five five five one, two, three,

52
00:02:55,620 --> 00:02:56,010
four.

53
00:02:57,430 --> 00:03:02,260
And then we will say phone is equal to R e search.

54
00:03:03,250 --> 00:03:07,810
And if we actually search the phone number itself for zero eight, dash five five five, dash one,

55
00:03:07,810 --> 00:03:14,800
two, three, four, in our text, we'll see that phone is that match object saying where it found that

56
00:03:14,800 --> 00:03:15,280
span.

57
00:03:15,760 --> 00:03:18,040
However, we may not always know the actual number.

58
00:03:18,100 --> 00:03:22,240
We just know the pattern for it's three digits, a dash, three digits.

59
00:03:22,300 --> 00:03:23,290
And then a dash.

60
00:03:23,290 --> 00:03:24,370
And then four digits.

61
00:03:24,760 --> 00:03:28,510
So what we can do is replace this using the pattern character.

62
00:03:28,550 --> 00:03:28,690
Right.

63
00:03:28,720 --> 00:03:31,960
The fires we just learned about in this case, we're looking for a digit.

64
00:03:32,020 --> 00:03:33,850
So we will use Backslash De.

65
00:03:34,890 --> 00:03:39,210
So we'll see backslash D, backslash D, backslash D, then we expect the dash.

66
00:03:39,660 --> 00:03:40,710
Three more numbers.

67
00:03:41,010 --> 00:03:41,730
Another dash.

68
00:03:41,820 --> 00:03:43,520
And then finally, four numbers.

69
00:03:43,980 --> 00:03:50,400
However, recall that when working with strings, these back slashes indicated special escape characters

70
00:03:50,700 --> 00:03:56,040
such as backslash end for a new line or backslash T for a tab to tell.

71
00:03:56,040 --> 00:03:59,880
Pi found that we're using these as a pattern for regular expression.

72
00:04:00,180 --> 00:04:03,240
What we do is we add an R right in front of the string.

73
00:04:03,690 --> 00:04:09,450
So that basically tells Python that these aren't really escape slashes such as New Line or indicate

74
00:04:09,450 --> 00:04:09,930
tab.

75
00:04:10,230 --> 00:04:13,410
Instead, we're using them as a pattern for regular expressions.

76
00:04:13,710 --> 00:04:18,810
So that's why you see this are in front of the strings when working off patterns for Riggo expressions.

77
00:04:19,290 --> 00:04:22,920
So let's rerun this cell and I'll see what our phone object is.

78
00:04:23,280 --> 00:04:25,780
And you'll notice the phone object is, again, the exact same thing.

79
00:04:25,800 --> 00:04:26,400
It's a match.

80
00:04:26,700 --> 00:04:28,680
But if I were to change these numbers around.

81
00:04:28,920 --> 00:04:33,960
So I said seven seven seven seven for the text and rerun these cells.

82
00:04:34,350 --> 00:04:39,150
It would still get the match before it would not get the match because it's looking for the exact numbers.

83
00:04:39,450 --> 00:04:45,840
Here instead, we're doing a smarter approach or actually searching for the pattern itself.

84
00:04:46,080 --> 00:04:49,620
And if you ever wanted the actual number, you could say the phone object.

85
00:04:49,620 --> 00:04:50,910
Here you see the span.

86
00:04:51,450 --> 00:04:57,240
And if you call the group method we discussed earlier on it, it returns the actual piece of string

87
00:04:57,240 --> 00:04:57,870
that matched.

88
00:04:58,020 --> 00:05:01,320
So this is how you could actually grab that phone number itself.

89
00:05:02,100 --> 00:05:03,450
Now, notice what we did here.

90
00:05:03,600 --> 00:05:07,720
For every single digit in this phone number, we wrote a backslash D.

91
00:05:08,310 --> 00:05:12,630
But what if we were looking for a pattern that included 20 digits or 100 hundred digits?

92
00:05:13,020 --> 00:05:17,760
We wouldn't want to have to write backslash D 20, 50 or 100 times.

93
00:05:18,270 --> 00:05:23,880
For this reason, we can use quantifiers to indicate repetition of the same character.

94
00:05:24,840 --> 00:05:26,910
So we already know our character to the fires.

95
00:05:27,060 --> 00:05:31,800
Now let's learn about the quantifiers allowing us to not have to write these multiple times.

96
00:05:33,520 --> 00:05:38,500
So here we have the quantifiers table and basically the way the Syntex works is you stick your character

97
00:05:38,500 --> 00:05:43,810
right in the fire and then immediately after it, you stick this quantifier if you want to indicate

98
00:05:43,810 --> 00:05:44,770
a certain quantity.

99
00:05:45,400 --> 00:05:47,920
So you have various quantities you can choose from.

100
00:05:48,220 --> 00:05:50,970
There's the plus symbol indicating that the character.

101
00:05:50,970 --> 00:05:52,780
Right, that the fire occurs one or more times.

102
00:05:53,170 --> 00:05:57,150
So an example of this pattern code could be version backslash W.

103
00:05:57,190 --> 00:06:01,480
So that's alphanumeric dash backslash W alphanumeric plus.

104
00:06:01,870 --> 00:06:07,540
So what this plus indicates is that you're looking for backslash W one or more times.

105
00:06:07,660 --> 00:06:10,930
So an example match could be a dash B one.

106
00:06:10,930 --> 00:06:11,770
Underscore one.

107
00:06:12,130 --> 00:06:14,080
So all of these are alphanumeric.

108
00:06:14,200 --> 00:06:15,700
That occurred one or more times.

109
00:06:15,730 --> 00:06:18,280
Recall the underscore counts as an alphanumeric.

110
00:06:19,150 --> 00:06:21,070
Now let's imagine you wanted something to occur.

111
00:06:21,160 --> 00:06:21,940
Exactly.

112
00:06:22,060 --> 00:06:22,900
N amount of time.

113
00:06:22,930 --> 00:06:24,070
So exactly three times.

114
00:06:24,070 --> 00:06:25,570
Four times you get the idea.

115
00:06:25,960 --> 00:06:27,850
You can use curly braces in that sense.

116
00:06:28,150 --> 00:06:30,670
So you would stick on your character identifier.

117
00:06:30,850 --> 00:06:34,720
This case we're looking for a backslash capital D. indicating a non digit.

118
00:06:35,110 --> 00:06:37,240
And we want this to occur exactly three times.

119
00:06:37,720 --> 00:06:41,410
So an example match would be ABC three non digits.

120
00:06:41,860 --> 00:06:43,510
If you want a range you're looking for.

121
00:06:43,540 --> 00:06:45,250
For example, occurs two to four times.

122
00:06:45,580 --> 00:06:46,840
It's the same curly braces.

123
00:06:47,050 --> 00:06:50,020
Except now you indicate the lower limit, comma.

124
00:06:50,110 --> 00:06:50,800
The upper limit.

125
00:06:51,190 --> 00:06:56,890
So backslash lowercase d curly braces to come off for an example match could be one, two, three.

126
00:06:57,190 --> 00:07:00,070
In other example, match could be one, two or one, two, three, four.

127
00:07:00,160 --> 00:07:01,540
Because we're looking for a range here.

128
00:07:04,140 --> 00:07:09,600
Alternatively, if you're looking for three or more or some and no or more, so you don't want to set

129
00:07:09,600 --> 00:07:10,290
an upper limit.

130
00:07:10,620 --> 00:07:15,120
You can just say curly braces, your lower limit comma and leave the upper limit blank.

131
00:07:15,600 --> 00:07:18,240
So backslash lowercase W..

132
00:07:18,390 --> 00:07:21,630
Along with curly braces, three comma, no upper limit.

133
00:07:21,960 --> 00:07:24,470
Basically that just says three or more alpha numerics.

134
00:07:24,570 --> 00:07:27,390
Go ahead and match them so you can see any characters here.

135
00:07:27,390 --> 00:07:30,360
That string will match because it's three or more alpha numerics.

136
00:07:30,840 --> 00:07:36,690
And now you can see how regular expression code can get a little complicated quite quickly because you're

137
00:07:36,780 --> 00:07:39,300
adding character right in the fires and quantifiers.

138
00:07:39,330 --> 00:07:43,920
But if you break it down, you'll be able to see what the actual pattern code is asking for.

139
00:07:44,670 --> 00:07:49,980
The next quantifier we can look at is an Asterix here, and that basically indicates if it occurs zero

140
00:07:50,070 --> 00:07:51,120
or more times.

141
00:07:51,330 --> 00:07:57,230
So here we have an example pattern code A, then asterisk B, strict, and then C, Asterix.

142
00:07:57,510 --> 00:08:04,470
And what that is asking for does A occur zero or more times followed by B, occurring zero or more times,

143
00:08:04,800 --> 00:08:07,290
followed by C, occurring XX or more times.

144
00:08:07,590 --> 00:08:14,400
So there are a ton of actual patterns that can match up to, for example, triple-A, then two CS that

145
00:08:14,400 --> 00:08:18,240
indicates that A did occur three times, B occurred zero or more times.

146
00:08:18,540 --> 00:08:21,480
And then you have Z, C occurring two times.

147
00:08:21,540 --> 00:08:24,600
So a very powerful here and it's going to match a lot.

148
00:08:24,630 --> 00:08:29,400
So keep that in mind and then you have a question mark here indicating once or none.

149
00:08:29,820 --> 00:08:36,630
So here we can see plurals question mark indicating does s occur once or none.

150
00:08:36,720 --> 00:08:40,190
And so an example match could be plural or could be plurals.

151
00:08:40,260 --> 00:08:42,840
So either court occurs once or no times.

152
00:08:43,710 --> 00:08:44,020
All right.

153
00:08:44,460 --> 00:08:51,480
Let's see how we could transform our original search for a telephone number using quantifiers.

154
00:08:52,510 --> 00:08:57,250
We'll place our example phone, but now we know the actual amounts you want.

155
00:08:57,280 --> 00:09:02,650
So we're going to replace this with our character right then to fire and then saying, I want exactly

156
00:09:02,650 --> 00:09:03,490
three numbers.

157
00:09:03,940 --> 00:09:04,750
Then a dash.

158
00:09:04,990 --> 00:09:06,790
Then another three numbers.

159
00:09:08,770 --> 00:09:10,180
And then we want four numbers.

160
00:09:12,530 --> 00:09:17,900
And this is now a regular expression pattern code for a phone number with dashes.

161
00:09:18,710 --> 00:09:22,220
So we can't run this and see that if we check our phone.

162
00:09:22,850 --> 00:09:28,130
We get back the same match here because we're asking for three digits in a row, followed by a dash

163
00:09:28,220 --> 00:09:32,630
followed by three digits in a row, followed by dash, followed by four digits in a row.

164
00:09:35,150 --> 00:09:37,580
Now, let's imagine that we wanted to do two tasks.

165
00:09:37,610 --> 00:09:42,830
One was to find phone numbers, but then also to be quickly able to extract their area code.

166
00:09:42,920 --> 00:09:45,050
That is the first three digits of the phone number.

167
00:09:45,140 --> 00:09:46,590
These first three digits right here.

168
00:09:47,210 --> 00:09:52,790
Well, we can do is we can use groups for any general tasks that involves grouping together regular

169
00:09:52,790 --> 00:09:53,510
expressions.

170
00:09:53,780 --> 00:09:55,640
And that allows us to later break them down.

171
00:09:56,330 --> 00:09:58,940
So to do that, we need to learn about the compile function.

172
00:10:00,110 --> 00:10:05,150
So we'll say phone underscore pattern is equal to R e.

173
00:10:06,350 --> 00:10:07,070
Compile.

174
00:10:08,460 --> 00:10:15,500
And what compiled does is it compiles together different regular expression pattern codes, for example,

175
00:10:16,190 --> 00:10:18,020
the first pattern could we see here?

176
00:10:18,290 --> 00:10:19,820
That's just a single pattern code.

177
00:10:20,150 --> 00:10:24,590
But what we could do is think about it as three pattern codes connected by this dash.

178
00:10:25,310 --> 00:10:26,980
So that's exactly what we're going to do here.

179
00:10:26,990 --> 00:10:27,810
We will say, ah.

180
00:10:28,370 --> 00:10:34,640
And then our quotes and in parentheses, we're going to group together three pattern codes.

181
00:10:34,910 --> 00:10:38,360
So the parentheses indicate that it's a group of a pattern.

182
00:10:38,930 --> 00:10:45,710
So the first one that's going to go into first parentheses is this one right here, backslash the curly

183
00:10:45,710 --> 00:10:46,420
braces three.

184
00:10:46,850 --> 00:10:52,070
So there's our first group and then Dash and we're going to do our second group, which is actually

185
00:10:52,070 --> 00:10:52,730
the same thing.

186
00:10:53,420 --> 00:10:56,480
And then the third group is going to be.

187
00:10:57,430 --> 00:10:58,420
With the four here.

188
00:10:58,930 --> 00:11:05,650
So what this does is it takes multiple pattern codes and each pattern code is separated with parentheses

189
00:11:05,680 --> 00:11:10,660
as a group, and then it compiles them into a single expression.

190
00:11:10,720 --> 00:11:16,720
So what this is going to do is it's going to compile these into this expression that we see right here.

191
00:11:17,170 --> 00:11:22,390
But what's nice about using the compile is that it's still understands that these were three separate

192
00:11:22,510 --> 00:11:23,230
groupings.

193
00:11:23,290 --> 00:11:26,110
So you could call the groupings individually.

194
00:11:27,690 --> 00:11:31,020
So you krater a phone pattern using the compile function.

195
00:11:31,830 --> 00:11:40,220
So if I say results is equal to our E search and I search for the phone pattern in my text.

196
00:11:41,590 --> 00:11:42,970
And then I say results.

197
00:11:43,870 --> 00:11:45,290
And I group them together.

198
00:11:45,700 --> 00:11:49,360
I see here four zero eight five five five seven seven seven seven.

199
00:11:50,230 --> 00:11:52,120
Now what I can do is say results.

200
00:11:52,260 --> 00:11:52,930
Group.

201
00:11:53,890 --> 00:11:58,590
And call them by the group position and the groups were denoted by these parentheses.

202
00:11:58,660 --> 00:12:02,500
So if I want the first group, I can say group one.

203
00:12:03,160 --> 00:12:07,540
And notice here and a little bit of a differentiation from normal python code.

204
00:12:07,870 --> 00:12:09,290
We don't start indexing at zero.

205
00:12:09,290 --> 00:12:10,300
We're starting at one.

206
00:12:10,360 --> 00:12:11,850
So group ordering starts at one.

207
00:12:12,670 --> 00:12:18,160
So if I grab those results and I group and I just say group one, it only returns back.

208
00:12:18,280 --> 00:12:19,150
The first group here.

209
00:12:19,750 --> 00:12:25,800
So if I want a group to essentially that second set of digits, I could say group two and a return back

210
00:12:25,810 --> 00:12:26,610
five five five.

211
00:12:26,990 --> 00:12:29,680
And if I wanted the third group, I would say group three.

212
00:12:30,410 --> 00:12:32,860
And return back seven seven seven seven.

213
00:12:33,520 --> 00:12:38,560
And if I ask for a group that is outside of this, such as Group four, it's going to say, sorry,

214
00:12:38,590 --> 00:12:44,470
there is no such group, because here we only had three sets of parentheses first group, second group

215
00:12:44,530 --> 00:12:45,220
and third group.

216
00:12:45,970 --> 00:12:52,990
What's nice about this approach in regards to our first approach is that now we can easily extract parts

217
00:12:52,990 --> 00:12:56,830
of that information while at the same time looking for a complete match.

218
00:12:57,280 --> 00:13:00,610
So if you just call a group with no number, it groups together.

219
00:13:01,270 --> 00:13:05,650
All the patterns you had in your compile function and their returns back the whole match.

220
00:13:06,220 --> 00:13:09,250
So if you wanted to find phone numbers, you could just use this approach.

221
00:13:09,370 --> 00:13:10,510
And then group them together.

222
00:13:11,370 --> 00:13:17,410
And if you wanted to find phone numbers and their area codes, you could use group with nothing and

223
00:13:17,410 --> 00:13:18,340
then say group one.

224
00:13:18,590 --> 00:13:20,020
And that returns the area code.

225
00:13:20,170 --> 00:13:21,880
So that's why they compile function.

226
00:13:21,910 --> 00:13:24,550
Along with the group method is so powerful.

227
00:13:25,640 --> 00:13:29,930
That's it so far for quantifiers as well as to compile in group function.

228
00:13:30,410 --> 00:13:30,920
Up next.

229
00:13:30,980 --> 00:13:36,590
All we have left to talk about is some additional regular expression syntax, such as wildcard characters

230
00:13:36,650 --> 00:13:41,360
and then or pipe operators and stuff with like starts with or ends with.

231
00:13:41,450 --> 00:13:43,070
Let's quickly review what we covered here.

232
00:13:43,520 --> 00:13:48,830
Basically, we discovered that if we don't want to repeat character right, then the fires often.

233
00:13:49,390 --> 00:13:54,350
Well, we can do is use quantifiers and we have various quantifiers indicated by a special character.

234
00:13:54,710 --> 00:13:57,800
Then there's a description here and we can essentially tack them on.

235
00:13:58,280 --> 00:14:04,550
We also learned that if we want to grab subsections or subgroups of our entire pattern, we can use

236
00:14:04,550 --> 00:14:09,650
the compile function to create the actual pattern instead of just passing in the pattern as a whole

237
00:14:09,650 --> 00:14:13,010
string separating each group with parentheses.

238
00:14:13,280 --> 00:14:19,430
And then we can say group together off of the search results in order to grab everything together.

239
00:14:19,760 --> 00:14:22,370
But if we only want to grab subgroups, we can say group.

240
00:14:22,460 --> 00:14:25,580
And then that group position where group positions start at one.

241
00:14:26,270 --> 00:14:26,540
All right.

242
00:14:27,020 --> 00:14:31,400
We'll see you at the next overview where we're going to talk about some additional regular expression,

243
00:14:31,400 --> 00:14:32,060
Syntex.
