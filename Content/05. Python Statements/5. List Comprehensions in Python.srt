1
00:00:05,380 --> 00:00:09,980
Will come back before we finished off this section and head over to your assessment test.

2
00:00:09,980 --> 00:00:16,000
I want to quickly discuss a concept known as list comprehensions list comprehensions are a unique way

3
00:00:16,000 --> 00:00:18,520
of quickly creating a list with Python.

4
00:00:18,520 --> 00:00:22,990
So if you're ever in a situation where you find yourself using a for loop along with a pen statement

5
00:00:23,320 --> 00:00:28,720
that goes over and over again through a for loop to create a list list comprehensions are a good alternative

6
00:00:28,720 --> 00:00:30,000
to this method.

7
00:00:30,110 --> 00:00:31,440
What I am actually talking about.

8
00:00:31,570 --> 00:00:33,260
Let's jump to Jupiter notebook.

9
00:00:33,650 --> 00:00:41,620
OK let's imagine that we have a string called my string and we say it's hello and I want to make a list

10
00:00:41,710 --> 00:00:43,500
of every character in this.

11
00:00:43,570 --> 00:00:54,320
What you may do is start off with an empty list such as this and then say four letter in my string append

12
00:00:56,330 --> 00:01:01,310
the letter to my list and then after running this if I check out what my list looks like now.

13
00:01:02,860 --> 00:01:03,680
I get back.

14
00:01:03,690 --> 00:01:05,230
H o.

15
00:01:06,090 --> 00:01:08,700
So this is something that's really common for beginners to do.

16
00:01:08,700 --> 00:01:14,370
They create an empty list and then they iterate through some other iterable and then they append whatever

17
00:01:14,370 --> 00:01:17,460
object that is or whatever element that is to their list.

18
00:01:17,460 --> 00:01:22,680
There's actually a more efficient way to do this in Python and by efficient I really mean taking up

19
00:01:22,680 --> 00:01:25,980
less space in code as far as the actual computation.

20
00:01:25,980 --> 00:01:26,880
It's about the same.

21
00:01:26,910 --> 00:01:31,010
So don't worry about saving computational time using this method.

22
00:01:31,050 --> 00:01:34,500
If you find this method too confusing you can always go back to the for loop.

23
00:01:34,540 --> 00:01:38,850
But let me show you how you could do this exact same thing on a single line.

24
00:01:38,850 --> 00:01:45,000
What you do is you essentially break down this entire four loops I'm going to copy this again here and

25
00:01:45,000 --> 00:01:48,420
then slowly break it down to be in the format of a list comprehension.

26
00:01:48,690 --> 00:01:54,840
So first off all of this code and logic here in some format is actually going to go inside these braces

27
00:01:55,260 --> 00:01:56,630
and we basically reorganize this.

28
00:01:56,640 --> 00:02:09,380
We say four letter in my string we're going to delete all this and then put letter right here.

29
00:02:09,380 --> 00:02:19,890
So now we have letter four letter in my string cut pasted in here run that and now we have the exact

30
00:02:19,890 --> 00:02:21,520
same thing.

31
00:02:22,500 --> 00:02:24,150
So what's actually happening here.

32
00:02:24,210 --> 00:02:29,700
The logic is essentially a flattened out for loop because we're going to assume that the only action

33
00:02:29,700 --> 00:02:34,960
being taken in this for loop is just appending whatever elements you want to your same list.

34
00:02:34,980 --> 00:02:41,550
What list comprehension does is it just takes that element for elements in some other other iterable

35
00:02:41,550 --> 00:02:43,680
object such as another list.

36
00:02:43,710 --> 00:02:53,890
So let's show you another example we can say my list is equal to open and close square brackets x for

37
00:02:53,890 --> 00:03:01,210
x in word and then if I check up my list I get back w o r d.

38
00:03:01,360 --> 00:03:02,420
And just like a for loop.

39
00:03:02,440 --> 00:03:03,720
This is a temporary variable named.

40
00:03:03,730 --> 00:03:05,060
You can call whatever you want.

41
00:03:05,200 --> 00:03:08,760
So we can call it just GWV.

42
00:03:08,800 --> 00:03:12,380
And as long as it matches here let's make it work to.

43
00:03:12,410 --> 00:03:14,770
We can see if it's actually working or two.

44
00:03:15,000 --> 00:03:22,200
So again it's just elements four elements in and then some sort of string list or other iterable object.

45
00:03:22,350 --> 00:03:28,880
So often what don't see something like this my list is to open and close square braces.

46
00:03:29,110 --> 00:03:36,500
And then what I'm going to do here say x for x in range 0 to 11.

47
00:03:36,530 --> 00:03:38,960
So now my list are those actual numbers.

48
00:03:38,980 --> 00:03:43,670
So instead of saying Excellent going to do it say numb because that's a little clearer to me.

49
00:03:44,800 --> 00:03:46,660
So how does flatten that for loop.

50
00:03:46,660 --> 00:03:49,020
Now you may be wondering well is this all I can do.

51
00:03:49,240 --> 00:03:51,130
And that's actually not all it can do.

52
00:03:51,130 --> 00:03:56,920
In fact you can begin to perform operations on this first variable name.

53
00:03:56,980 --> 00:04:02,950
So if I wanted to grab the square of every number in that range what I could do is perform the operation

54
00:04:02,950 --> 00:04:03,480
here.

55
00:04:03,620 --> 00:04:07,720
So I'm saying number squared for now in range 0 to 11.

56
00:04:07,810 --> 00:04:11,610
And then when I rerun this now I get back to square two versions of that.

57
00:04:11,920 --> 00:04:18,800
So what this is doing is essentially flattening out this for loop right here where this letter or this

58
00:04:18,800 --> 00:04:23,300
number whatever happens to be is basically as if you had done squared here.

59
00:04:23,300 --> 00:04:26,000
That's what we're doing when we say squared right here.

60
00:04:26,270 --> 00:04:29,420
So you're appending whatever this happens to be.

61
00:04:29,420 --> 00:04:33,020
Now you can also add an if statements into this.

62
00:04:33,020 --> 00:04:39,230
So let's say we only wanted to grab the numbers from this one I could do is say my list is equal to

63
00:04:39,710 --> 00:04:49,090
open and close square brackets and then say x for x in range 0 through 11 and then after this is where

64
00:04:49,090 --> 00:04:54,990
you add in the if statement you can say if x mod 2 is equal to zero.

65
00:04:54,990 --> 00:04:57,350
So remember that's the check if something's even.

66
00:04:57,440 --> 00:05:04,300
So I'm saying x for x in this range of numbers but only if when you say Mattu of that X it's equal to

67
00:05:04,300 --> 00:05:04,970
zero.

68
00:05:05,880 --> 00:05:09,930
And now when I checked the result to my list it's only that even numbers and it could also do something

69
00:05:09,930 --> 00:05:15,540
like this next to the power of two here and now I only have the square of the even numbers.

70
00:05:17,300 --> 00:05:21,070
Now you can do some more complex things like more complex arithmetic here.

71
00:05:21,080 --> 00:05:23,390
So I'm going to show you one last example.

72
00:05:23,390 --> 00:05:27,180
Let's imagine we have a list of temperatures in Celsius.

73
00:05:27,230 --> 00:05:31,740
So 0 10 20 let's say thirty four point five.

74
00:05:32,360 --> 00:05:34,130
And then I wanted them in Fahrenheit.

75
00:05:34,400 --> 00:05:35,750
So let's do that.

76
00:05:36,090 --> 00:05:37,490
Illest called Fahrenheit.

77
00:05:37,670 --> 00:05:39,140
I will set that equal to.

78
00:05:39,410 --> 00:05:48,310
And then there's some sort of formula for the conversion and the formula is 9:0 or five times the temperature

79
00:05:48,610 --> 00:05:51,720
in Celsius plus 32.

80
00:05:51,970 --> 00:05:55,910
So here we have quite a complex arithmetic arrangement.

81
00:05:55,930 --> 00:06:03,500
But then I always say for temp in Celsius and then when I run that and I see Fahrenheit again I see

82
00:06:03,500 --> 00:06:10,400
back the temperatures in Fahrenheit so zero degrees Fahrenheit is 32 or Scuse me or the Celsius is 32

83
00:06:10,400 --> 00:06:11,910
degrees Fahrenheit.

84
00:06:12,380 --> 00:06:14,230
So I'm going to break this down as a for loop.

85
00:06:14,240 --> 00:06:15,760
Just so we can see the connection.

86
00:06:15,800 --> 00:06:21,770
This is the exact same thing as me doing the following as me saying Fahrenheit set that equal to empty

87
00:06:21,770 --> 00:06:35,950
list and then say for for four for temp in Celsius Fahrenheit append and then just doing this sort of

88
00:06:35,950 --> 00:06:36,570
thing right here.

89
00:06:36,780 --> 00:06:40,930
Scrappiness and putting it in there.

90
00:06:41,500 --> 00:06:43,280
So this is the exact same thing.

91
00:06:43,300 --> 00:06:46,170
And if I check out Fahrenheit I get back the same result.

92
00:06:46,360 --> 00:06:50,850
So what I would suggest is you basically freeze this video right here and see if you can build out the

93
00:06:50,860 --> 00:06:56,170
comparisons between this flattened version for miscomprehension and this version for the for loop because

94
00:06:56,170 --> 00:07:01,270
all we're really doing here is flattening out and getting rid of the suspend functionality because it's

95
00:07:01,270 --> 00:07:02,880
essentially implied by default.

96
00:07:03,010 --> 00:07:05,300
When you put it all in this list comprehension.

97
00:07:05,740 --> 00:07:09,890
Now often I'm asked by students as they review their list comprehension.

98
00:07:10,060 --> 00:07:14,500
Hey am I able to do if else statements inside of a list comprehension.

99
00:07:14,530 --> 00:07:16,750
And the answer to that is yes you can.

100
00:07:16,750 --> 00:07:18,870
But the order is going to be a little different.

101
00:07:18,880 --> 00:07:24,910
And before I show you this I want to emphasize that above all else readability and reproducibility in

102
00:07:24,910 --> 00:07:27,920
your Python code should be the most important thing.

103
00:07:28,000 --> 00:07:33,450
Not trying to do slick one liners and list comprehensions can kind of fool you into thinking you're

104
00:07:33,460 --> 00:07:38,380
becoming a really good programmer when in fact your writing just kind of really one liners that are

105
00:07:38,470 --> 00:07:42,270
really ugly and it's hard to read when you come back to them a month later.

106
00:07:42,280 --> 00:07:43,260
So keep that in mind.

107
00:07:43,360 --> 00:07:48,070
I am going to show you right now how to do it but I am myself typically don't do this because it's really

108
00:07:48,070 --> 00:07:49,080
hard to read.

109
00:07:49,120 --> 00:07:55,760
Once you come back to it let me show you how to use an if and else statement inside of a list comprehension.

110
00:07:55,760 --> 00:08:02,440
So the way we do this is let's say results are going to set that equal to and the order is a little

111
00:08:02,440 --> 00:08:11,310
reversed here you say X if and then you have your statement so we'll say X if X Mattu is equal to zero.

112
00:08:11,380 --> 00:08:21,160
And then we have our statement else do odd then we say for x in and then we can say range 0 to 11.

113
00:08:21,180 --> 00:08:24,210
So this is kind of weird because now the order has been totally reversed.

114
00:08:24,300 --> 00:08:25,980
Different than what I said up here.

115
00:08:26,160 --> 00:08:30,090
So that's why I also recommend that you kind of take the easy when you're doing if else statements and

116
00:08:30,090 --> 00:08:35,550
a list comprehension because it can quickly become unreadable or confusing because the orders are different

117
00:08:36,030 --> 00:08:38,820
but when you run this and we check out our results.

118
00:08:38,820 --> 00:08:45,110
Now I have a list where I have the even numbers or basically the numbers where you made 2 0 and then

119
00:08:45,130 --> 00:08:45,960
string odd.

120
00:08:46,080 --> 00:08:47,150
If that's not the case.

121
00:08:47,190 --> 00:08:50,250
So that's you can use if else in a list comprehension.

122
00:08:50,250 --> 00:08:55,890
Notice how the order is now different than just a simple IF statement inside of a list comprehension.

123
00:08:55,890 --> 00:09:00,180
Also keep in mind that a month later when you're coming back this is suddenly going to take a little

124
00:09:00,180 --> 00:09:02,490
longer to read and understand what you doing here.

125
00:09:02,490 --> 00:09:04,230
So be kind to your future self.

126
00:09:05,490 --> 00:09:10,460
One last note I want to show is you can also do nested loops in the list comprehension.

127
00:09:10,620 --> 00:09:15,540
This is going to be another topic where I would say Take it easy at first because it can become quite

128
00:09:15,540 --> 00:09:16,250
confusing.

129
00:09:16,440 --> 00:09:19,920
But let me show you what I mean by a nested loop.

130
00:09:19,920 --> 00:09:21,520
We haven't actually seen a nested loop yet.

131
00:09:21,570 --> 00:09:23,050
So we'll introduce one first.

132
00:09:23,370 --> 00:09:32,770
Let's imagine I have my list as an empty list there and I say for x in and let's say two four six and

133
00:09:32,770 --> 00:09:41,810
then inside of this I want to make another for loop will say for y in one let's say 100 200 300 I'm

134
00:09:41,810 --> 00:09:47,430
going to take my list and I'm going to append X times Y.

135
00:09:47,480 --> 00:09:53,550
So I run that and then outside of those four loops I print out my list and I get back two hundred 400

136
00:09:53,560 --> 00:09:57,080
600 etc. all the way to the end so what is actually doing.

137
00:09:57,080 --> 00:10:02,720
Well it's going to take to multiply by each of these numbers that it's going to take four multiply by

138
00:10:02,720 --> 00:10:05,780
each of those numbers resulting in this and then it's going to take six.

139
00:10:05,780 --> 00:10:09,100
Multiply it but each of those numbers and then result in that.

140
00:10:09,110 --> 00:10:12,800
So let me choose a couple of numbers here that will make it even more obvious what's happening.

141
00:10:13,030 --> 00:10:19,040
And let's say one 10 and then we'll make this last one a thousand.

142
00:10:19,040 --> 00:10:26,110
So when I run this code you can see here to 22000 for forty four thousand six sixty six six thousand.

143
00:10:26,120 --> 00:10:29,850
Now there is a way to do this with list comprehension again.

144
00:10:29,870 --> 00:10:33,800
You're going to sacrifice readability here but it is possible the way you would do this is the following

145
00:10:33,800 --> 00:10:34,570
manner.

146
00:10:34,690 --> 00:10:37,810
You would say my list is equal to.

147
00:10:38,060 --> 00:10:44,720
And then we say X times Y for x in and then you pass in that list.

148
00:10:44,720 --> 00:10:54,410
So in this case we're going to say two for six and then we'll say four y in and then we'll say one 10

149
00:10:54,880 --> 00:10:56,260
1000.

150
00:10:56,920 --> 00:11:01,060
And when we check out my list we get back the same results.

151
00:11:01,070 --> 00:11:05,120
Now again definitely be careful with this because this is really hard to read especially when you're

152
00:11:05,120 --> 00:11:06,220
coming back to it later.

153
00:11:06,230 --> 00:11:10,480
So it's possible but I would suggest always try to do readability first.

154
00:11:10,820 --> 00:11:11,380
OK.

155
00:11:11,480 --> 00:11:13,730
That's the basics of list comprehension.

156
00:11:13,730 --> 00:11:18,050
We'll play a round of them a little bit and some of your exercises and projects but it will definitely

157
00:11:18,050 --> 00:11:23,930
take awhile before it comes before this becomes part of your normal flow in programming of Python.

158
00:11:23,930 --> 00:11:26,230
All right thanks everyone and I'll see at the next lecture.
