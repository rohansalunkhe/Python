1
00:00:05,650 --> 00:00:06,770
Welcome back everyone.

2
00:00:06,850 --> 00:00:10,400
Let's finish up the section by having your statements assessment test.

3
00:00:10,420 --> 00:00:14,230
I'm going to quickly go over the notebook in this lecture and then in the very next lecture it will

4
00:00:14,230 --> 00:00:15,730
go over the solutions.

5
00:00:15,730 --> 00:00:17,160
Let's head over to the notebook.

6
00:00:17,500 --> 00:00:17,710
All right.

7
00:00:17,710 --> 00:00:19,990
Here I am at the hub for this course.

8
00:00:19,990 --> 00:00:24,850
And in order to find the note book that I'm referring to it's going to be under Python statements and

9
00:00:24,850 --> 00:00:27,300
then come down to statements assessment tests.

10
00:00:27,340 --> 00:00:31,120
There's also solutions no book which we're going to go over in the very next lecture.

11
00:00:31,120 --> 00:00:35,040
Click on statements assessment test and then let me quickly go over the questions.

12
00:00:35,080 --> 00:00:37,790
So for this one we want you to use a for statement.

13
00:00:37,960 --> 00:00:39,870
The Split method off a string.

14
00:00:39,910 --> 00:00:41,190
See if you remember that one.

15
00:00:41,340 --> 00:00:46,390
And an if statement to create a statement that's going to print out the words the start of an s.

16
00:00:46,840 --> 00:00:53,290
So here I give you a string and I want you to use a for loop along with split if that's going to be

17
00:00:53,290 --> 00:01:00,230
able to print so only the words in the stream that start s then I want you to use range to print out

18
00:01:00,290 --> 00:01:03,320
all the even numbers from 0 to 10.

19
00:01:03,620 --> 00:01:07,910
Then I want you to use less comprehension to create a list of all numbers between 1 and 50 that are

20
00:01:07,910 --> 00:01:12,380
divisible by three and that's to be evenly divisible by three.

21
00:01:12,380 --> 00:01:14,000
So let me just clarify that.

22
00:01:14,210 --> 00:01:18,720
And then I want you to go through the string below and if the length word is even prints even this one

23
00:01:18,730 --> 00:01:21,870
is a little more complex than what we've seen in these earlier questions.

24
00:01:21,890 --> 00:01:26,960
So we're going to do is use what you know about Python statements maybe a for loop here to go through

25
00:01:26,960 --> 00:01:29,150
the string and every single word.

26
00:01:29,150 --> 00:01:33,740
So remember you going have to use them for split on the whitespace and then if the length of the word

27
00:01:33,830 --> 00:01:41,210
is even print out even next I want you to write a program that prints the integers from 1 to 100.

28
00:01:41,410 --> 00:01:48,610
But for multiples of 3 printout is it sort of the number for multiples of 5 print out buzz and for numbers

29
00:01:48,610 --> 00:01:50,670
which are multiples of both three and a five.

30
00:01:50,670 --> 00:01:52,230
Print out fibbers.

31
00:01:52,270 --> 00:01:57,280
So this is a very common question and it's kind of used as a base filter for some interviewers when

32
00:01:57,280 --> 00:01:59,920
they want to know this is person even really know how to program.

33
00:02:00,070 --> 00:02:03,080
So this is a really famous question called fiss buzz.

34
00:02:03,370 --> 00:02:07,930
And then finally I want you to lose a useless comprehension to create a list of the first letters of

35
00:02:07,930 --> 00:02:09,580
every word and string below.

36
00:02:09,580 --> 00:02:14,230
So again we have a string below useless comprehension to create a list of the first letters of every

37
00:02:14,230 --> 00:02:15,490
single word.

38
00:02:15,520 --> 00:02:16,110
OK.

39
00:02:16,270 --> 00:02:20,890
Best of luck and definitely a big hint here is utilizing that split you're going to need to use that

40
00:02:21,190 --> 00:02:24,310
for a lot of these test questions.

41
00:02:24,310 --> 00:02:26,610
We'll see at the next lecture where we go over the solutions.
