1
00:00:05,410 --> 00:00:06,430
Welcome back everyone.

2
00:00:06,490 --> 00:00:10,080
And this lecture we're going to discuss for loop's.

3
00:00:10,290 --> 00:00:15,600
Now many objects in Python are iterable meaning that we can iterate over every element in the object

4
00:00:16,050 --> 00:00:21,150
such as every element in a list or every character in a string and we can use for loops to execute a

5
00:00:21,150 --> 00:00:23,170
block of code for every iteration.

6
00:00:23,430 --> 00:00:27,570
So I want to focus a little bit on the term iterable because you hear it a lot especially in Python

7
00:00:27,570 --> 00:00:31,310
documentation and it's kind of a strange term if you never heard it before.

8
00:00:31,350 --> 00:00:37,350
So all this term means is that you can iterate over the object and that means you can performs an action

9
00:00:37,380 --> 00:00:39,990
for every thing in the object.

10
00:00:39,990 --> 00:00:46,200
So for example for every character in a string you can iterate through that string and then do something

11
00:00:46,200 --> 00:00:46,960
for every character.

12
00:00:46,980 --> 00:00:51,360
Often maybe you want to print out every single letter in that string or every single character in that

13
00:00:51,360 --> 00:00:52,030
string.

14
00:00:52,050 --> 00:00:54,380
So that means the string isn't iterable object.

15
00:00:54,390 --> 00:00:56,040
You can work through it.

16
00:00:56,070 --> 00:00:58,390
You can also iterate over every item in a list.

17
00:00:58,410 --> 00:01:02,850
That means the list is iterable and then you can iterate over every key in a dictionary.

18
00:01:02,880 --> 00:01:06,140
So the dictionary itself can also be iterated over.

19
00:01:06,540 --> 00:01:12,810
So kind of a little funny term there but just think of it as a way of going through something.

20
00:01:12,900 --> 00:01:17,380
So let's go over the example of the syntax of a for loop.

21
00:01:17,410 --> 00:01:19,830
So this is the syntax of a for loop break here.

22
00:01:19,840 --> 00:01:21,930
Notice that we have a list defined.

23
00:01:21,970 --> 00:01:26,380
And then we have a couple of keywords and then we're performing some action so let's focus on each aspect

24
00:01:26,380 --> 00:01:28,390
of this syntax.

25
00:01:28,390 --> 00:01:32,950
We start off with the statement and this is just an assignment we're saying my iterable we're choosing

26
00:01:32,950 --> 00:01:33,970
that as a variable name.

27
00:01:33,970 --> 00:01:38,530
This could be my list my items my dogs whatever you want is equal to.

28
00:01:38,560 --> 00:01:39,900
In this case we're choosing a list.

29
00:01:39,910 --> 00:01:43,540
We're going to see other examples of other bowls in just a second.

30
00:01:43,750 --> 00:01:45,300
So he makes some sort of assignments.

31
00:01:45,310 --> 00:01:51,190
We have a variable name there and then we're going say four to that key word for item name.

32
00:01:51,190 --> 00:01:53,220
So this is a variable name that you can choose.

33
00:01:53,380 --> 00:01:58,500
That's going to be a bit of a placeholder for every single item in your iterable object.

34
00:01:58,540 --> 00:02:05,050
In this case the item name actually represents the numbers in that list and we say in so another key

35
00:02:05,050 --> 00:02:08,870
word there and then the variable name that you chose in this case my iterable.

36
00:02:08,920 --> 00:02:15,580
So we're saying for every item or item name in this list of 1 to 3 then we have a colon and then we

37
00:02:15,580 --> 00:02:18,790
have some whitespace and then we perform or execute some block of code.

38
00:02:18,790 --> 00:02:23,210
In this case we're saying print item name but you don't have to actually use the item name.

39
00:02:23,230 --> 00:02:26,610
You could just print it hello three times so we're going to show more examples of that.

40
00:02:26,800 --> 00:02:29,040
But that's the basic syntax for for a loop.

41
00:02:29,050 --> 00:02:33,500
So let's actually explore these concepts in a Jupiter notebook and try to build a deeper understanding.

42
00:02:33,670 --> 00:02:37,150
So we're going to perform for loops for a bunch of different objects in Python.

43
00:02:37,330 --> 00:02:41,630
And I think by the end of this lecture you're going to have a fair understanding of the basic idea of

44
00:02:41,650 --> 00:02:43,580
for loop and how to work with them.

45
00:02:43,750 --> 00:02:46,430
And this is really where you begin to level up your skills.

46
00:02:46,450 --> 00:02:51,760
So before we've basically just been learning about data types objects we haven't really been able to

47
00:02:51,760 --> 00:02:53,140
construct anything yet.

48
00:02:53,320 --> 00:02:57,880
Once you start learning about control flow and for loops and later on while loops and then after that

49
00:02:57,880 --> 00:03:03,670
functions etc. This is where we really start to be able to construct logical programs.

50
00:03:03,700 --> 00:03:03,960
OK.

51
00:03:03,970 --> 00:03:05,050
So it's an exciting leap.

52
00:03:05,140 --> 00:03:07,900
Let's head to Jupiter notebook and get started.

53
00:03:07,900 --> 00:03:10,960
All right let's begin with a simple example.

54
00:03:10,960 --> 00:03:13,020
I'm going to create a list called my list.

55
00:03:13,120 --> 00:03:18,800
And we're going to set the sequel through the numbers one through ten will say one two three four five

56
00:03:18,800 --> 00:03:20,820
six seven eight nine.

57
00:03:20,920 --> 00:03:24,160
And then finally 10 and it's a very quick note.

58
00:03:24,370 --> 00:03:26,740
You should not use list as a variable name here.

59
00:03:26,740 --> 00:03:29,830
Notice that it's being highlighted through syntax highlighting.

60
00:03:29,830 --> 00:03:35,160
That means list is a built in keywords so avoid using just list by itself as a variable name.

61
00:03:35,230 --> 00:03:40,480
If for some reason you accidentally or do that go ahead and delete this and then just hit kernel restart

62
00:03:40,930 --> 00:03:45,320
and then restart this and that will reset all your variable names where you will need to do is then

63
00:03:45,340 --> 00:03:47,340
rerun themselves if you already have stuff defined.

64
00:03:47,350 --> 00:03:49,090
So I just need to rewind that.

65
00:03:49,090 --> 00:03:50,450
Notice how now it's back at 1.

66
00:03:50,500 --> 00:03:54,730
So I've restarted everything OK so I have this list 1 through 10.

67
00:03:54,730 --> 00:03:59,590
Now I'm going to create a for loop to iterate for every item in this list.

68
00:03:59,590 --> 00:04:05,230
So I start with keyword 4 and then I get to choose a variable name that's going to represent the elements

69
00:04:05,290 --> 00:04:06,950
inside of this list.

70
00:04:07,700 --> 00:04:17,210
So unsafe for some in my list that iterable colon I hit enter and noticed now have an indented block

71
00:04:17,210 --> 00:04:18,070
of code there.

72
00:04:18,320 --> 00:04:20,940
So now I can interact with those actual elements.

73
00:04:20,960 --> 00:04:21,760
So let's print them out.

74
00:04:21,770 --> 00:04:23,660
I'm going say for now them in my list.

75
00:04:23,660 --> 00:04:28,310
Prints numb and then when I run this I see one two three four five six seven eight nine 10.

76
00:04:28,310 --> 00:04:33,620
So it's actually printing out this number for every number in that list something I want to make really

77
00:04:33,620 --> 00:04:38,780
clear though is that this particular variable name that you chose to represent the elements inside that

78
00:04:38,780 --> 00:04:39,310
list.

79
00:04:39,380 --> 00:04:40,550
It can be whatever you want.

80
00:04:40,790 --> 00:04:45,770
So here I chose not because these are numbers but I'm going to just make it really obvious by choosing

81
00:04:45,830 --> 00:04:53,750
Jeli and then saying for Jeli in my list print Jeli run that again and you get back the exact same results.

82
00:04:53,750 --> 00:04:59,930
So again this variable name can be whatever you want this iterable can't be has to be predefined but

83
00:04:59,990 --> 00:05:05,060
this variable name that's going to represent the actual objects inside the iterable that is whatever

84
00:05:05,090 --> 00:05:05,890
you want.

85
00:05:06,670 --> 00:05:11,020
Now I want to show the point that we technically don't even need to use this variable name.

86
00:05:11,050 --> 00:05:16,480
We can still execute some block of code for every single item in the list even if the code itself is

87
00:05:16,510 --> 00:05:17,800
unrelated to the item.

88
00:05:17,860 --> 00:05:19,810
So I can print hello

89
00:05:23,560 --> 00:05:25,760
for every item in that list and we get back.

90
00:05:25,780 --> 00:05:26,510
Hello.

91
00:05:26,710 --> 00:05:28,260
One through 10 times.

92
00:05:28,300 --> 00:05:31,510
And again we can call this whatever we want.

93
00:05:31,750 --> 00:05:34,680
Run that and we get back Hello 10 times.

94
00:05:34,690 --> 00:05:38,920
So keep in mind when choosing these variable names you have full flexibility so you want to choose a

95
00:05:38,920 --> 00:05:44,010
variable name that is somehow related to the object inside the iterable.

96
00:05:44,010 --> 00:05:48,800
Now let's combine our knowledge for loops with a little bit of what we just discussed in control flow.

97
00:05:48,810 --> 00:05:52,800
Let's see if we can print out only that even numbers in this list.

98
00:05:52,800 --> 00:05:59,580
So to do that we're going to do is start off by saying for now I'm in my list and then I'm going to

99
00:05:59,580 --> 00:06:02,060
add in some control flow to somehow check the numbers.

100
00:06:02,070 --> 00:06:04,930
Even so it's a check for even here.

101
00:06:04,950 --> 00:06:06,440
This is just a comment.

102
00:06:06,750 --> 00:06:08,210
And then what I will do is the following.

103
00:06:08,210 --> 00:06:17,650
I'll say if numb maade to is equal to zero colon print the number.

104
00:06:17,700 --> 00:06:23,970
So what I'm doing here is I'm saying OK for every number in that list if the numb maade to say Remember

105
00:06:24,060 --> 00:06:27,000
that's the remainder when you divide by 2 is equal to zero.

106
00:06:27,000 --> 00:06:30,730
So if there's an even number and you divide it by two and the remainder is zero.

107
00:06:30,840 --> 00:06:33,170
Then you can go ahead and print that number.

108
00:06:33,180 --> 00:06:34,870
So then we run this.

109
00:06:34,980 --> 00:06:36,550
Here we have all the even numbers.

110
00:06:36,570 --> 00:06:38,320
Two four six eight 10.

111
00:06:38,520 --> 00:06:45,460
And it can add even more control flow by attaching an else statement to this if statement and then say

112
00:06:45,460 --> 00:06:54,250
Prince odd number and we can even do some string literal formatting so I can say if number

113
00:06:58,440 --> 00:07:03,510
right there run that and it says odd number one to number three four.

114
00:07:03,510 --> 00:07:04,830
Number five six.

115
00:07:05,090 --> 00:07:10,770
So this is some good work here as far as combining four loops with the control flow from before.

116
00:07:10,850 --> 00:07:13,410
Key thing to note here is the in the notation.

117
00:07:13,460 --> 00:07:17,840
So luckily Python is very clean because of the use case of indentation.

118
00:07:17,900 --> 00:07:21,440
So everything at this indentation level is inside of this for loop.

119
00:07:21,440 --> 00:07:27,050
So this entire block of code is going to execute every single number in that list and then we can see

120
00:07:27,050 --> 00:07:31,100
that this print is only going to execute when this if statement is true.

121
00:07:31,160 --> 00:07:34,170
And this else in the notation is lined up with this.

122
00:07:34,190 --> 00:07:41,150
If so we say if this case printed some else and then we print out the number and again using some string

123
00:07:41,150 --> 00:07:44,230
literal here to nicely print out that number itself.

124
00:07:45,270 --> 00:07:46,490
All right let's move along.

125
00:07:46,500 --> 00:07:51,960
So another common idea for a for loop is to keep some sort of running tally during multiple loops and

126
00:07:51,960 --> 00:07:56,070
later I'm going to show you the enumerate function which kind of does this free automatically right

127
00:07:56,070 --> 00:08:00,830
now and I'm going to do is say list some is equal to zero.

128
00:08:01,040 --> 00:08:04,520
So let's try to get the sum of every number in that list.

129
00:08:04,520 --> 00:08:15,120
So I will say for some in my list I will say the list sum is equal to the current list.

130
00:08:15,120 --> 00:08:23,450
Some plus the number and then I'm going to outside of this for loop.

131
00:08:23,780 --> 00:08:26,730
Print out the list some.

132
00:08:26,750 --> 00:08:28,880
So when I run this I get back 55.

133
00:08:28,880 --> 00:08:33,250
So this is a good example of indentation and the use case.

134
00:08:33,260 --> 00:08:35,830
So right now I have this list some.

135
00:08:35,870 --> 00:08:38,520
So I'm saying my initial sum is zero.

136
00:08:38,660 --> 00:08:44,990
Then for every single number in that list I take my old value from my sum and I add on the current number

137
00:08:45,080 --> 00:08:48,020
and then I reassign that value to list some.

138
00:08:48,020 --> 00:08:52,870
So I do that over and over again for every number in that list and then at the end of this for loop

139
00:08:52,980 --> 00:08:57,860
1 is once it's done completing those the indentation level here I print out list some.

140
00:08:57,920 --> 00:09:03,580
Let's see what would happen if I were to press tab here and have this print statement inside of this

141
00:09:03,590 --> 00:09:04,630
for loop.

142
00:09:04,640 --> 00:09:08,840
Now when I run this I get back of the running tally of this sum.

143
00:09:08,930 --> 00:09:10,570
So it starts off at zero.

144
00:09:10,640 --> 00:09:14,440
And then once it has a first run inside of the for loop it gets printed out.

145
00:09:14,480 --> 00:09:16,430
So then it has zero plus one.

146
00:09:16,580 --> 00:09:17,480
Well that's equal to one.

147
00:09:17,480 --> 00:09:20,200
So print out one then we have one plus two.

148
00:09:20,270 --> 00:09:21,290
Well that's three.

149
00:09:21,290 --> 00:09:24,050
So three three plus three is six.

150
00:09:24,050 --> 00:09:27,190
Six was forced ten ten plus five 15 and so on.

151
00:09:27,200 --> 00:09:30,840
So now we get the running tally and it's only until the very end that we see a 55.

152
00:09:30,960 --> 00:09:35,990
If we only want to see the last result then I just have the Sprint outside of that for a loop.

153
00:09:36,050 --> 00:09:38,990
So that's why in the station all white space is so important.

154
00:09:39,830 --> 00:09:42,120
So we just saw how to use four loops with lists.

155
00:09:42,140 --> 00:09:43,440
But what about strings.

156
00:09:43,490 --> 00:09:44,930
Remember strings are a sequence.

157
00:09:44,930 --> 00:09:50,030
That means we can iterate through them and we'll be accessing each character in that string.

158
00:09:50,090 --> 00:10:01,200
So we'll say four letter in and let's create a string will say my string is equal to Hello World than

159
00:10:01,200 --> 00:10:08,600
four letter in my string colon print out that letter and I run this here.

160
00:10:08,620 --> 00:10:13,890
C H E L L O space they'll be O R L D.

161
00:10:13,920 --> 00:10:17,310
So that's the way we can actually iterate through a string itself.

162
00:10:17,310 --> 00:10:20,860
And technically you don't need to actually assign a variable name to the string.

163
00:10:20,900 --> 00:10:25,830
You could do it just straight up put the string here.

164
00:10:26,250 --> 00:10:31,290
So you could say four letter in Hello World print letter and we get back the exact same results.

165
00:10:31,380 --> 00:10:32,860
And I really want to drive this point home.

166
00:10:32,880 --> 00:10:35,850
You can call this whatever you want to this letter variable name.

167
00:10:35,850 --> 00:10:38,750
You can say GHC H.

168
00:10:38,760 --> 00:10:45,180
And as long as it's the same thing inside of this and you run this you'll get back the same result.

169
00:10:45,240 --> 00:10:49,320
And the other thing I want to stress is that you technically don't even need to use this variable.

170
00:10:49,560 --> 00:10:53,880
So a lot of times what people do as they get more advanced in Python for some reason they want to iterate

171
00:10:53,880 --> 00:10:55,640
something a certain amount of times.

172
00:10:55,650 --> 00:10:57,390
So imagine I wanted to Prince

173
00:11:00,170 --> 00:11:03,570
cool for as many times as your character is here.

174
00:11:03,590 --> 00:11:03,980
So I see.

175
00:11:03,980 --> 00:11:04,370
Cuckoo.

176
00:11:04,370 --> 00:11:04,700
Cuckoo.

177
00:11:04,700 --> 00:11:05,620
Cool.

178
00:11:05,690 --> 00:11:09,800
What they do is instead of assigning this variable name they just use an underscore.

179
00:11:09,800 --> 00:11:14,840
And that's really common syntax for when you don't intend to actually use that variable name as you

180
00:11:14,840 --> 00:11:15,890
iterate through.

181
00:11:15,920 --> 00:11:17,930
So they just use a simple underscore there.

182
00:11:18,050 --> 00:11:19,790
That way it's a little more readable.

183
00:11:20,180 --> 00:11:22,570
OK so that's iterating through a for loop.

184
00:11:22,610 --> 00:11:25,820
You can also use the same iteration for each tuple.

185
00:11:25,820 --> 00:11:36,790
So I can say Toop is equal to one to three then for item and TUPE print out the item run that and it

186
00:11:36,800 --> 00:11:38,980
get back 1 to 3.

187
00:11:38,990 --> 00:11:43,670
Now I want to mention tuple packing so tuples have a little bit of a special quality when it comes to

188
00:11:43,670 --> 00:11:44,360
for loops.

189
00:11:44,390 --> 00:11:50,560
So iterating through a sequence that contains itself self tuples the item can be used with tuple and

190
00:11:50,570 --> 00:11:51,350
packing.

191
00:11:51,350 --> 00:11:53,700
So let me show you what I mean by this.

192
00:11:53,780 --> 00:12:00,120
I want to create a new list called my list and I would set this equal to a list of tuples.

193
00:12:00,140 --> 00:12:04,610
So here you have square brackets and I'm going to make some tuple pairs.

194
00:12:04,700 --> 00:12:15,600
We'll say one two comma then three four comma five six comma seven eight.

195
00:12:15,620 --> 00:12:17,030
So notice what you have here.

196
00:12:17,030 --> 00:12:18,040
I have a list.

197
00:12:18,080 --> 00:12:22,170
And then there's tuple pairs as items in that list.

198
00:12:22,190 --> 00:12:29,750
So if I check out the length of my list I have four items in it where every single item is a tuple pair.

199
00:12:29,810 --> 00:12:32,440
So let's print out those items themselves.

200
00:12:32,480 --> 00:12:35,540
For item in my list.

201
00:12:35,540 --> 00:12:36,890
Print out the item.

202
00:12:37,220 --> 00:12:39,210
And when I run this actually see the tuples themselves.

203
00:12:39,230 --> 00:12:41,980
One two three four five six seven eight.

204
00:12:42,290 --> 00:12:47,480
This sort of data structure is extremely common in Python and later on we're going to see that a lot

205
00:12:47,480 --> 00:12:53,420
of built in Python functions actually take advantage of this built in structure and return back tuples

206
00:12:53,570 --> 00:12:54,950
inside of a list.

207
00:12:54,950 --> 00:12:57,710
And the reason for that is something called tuple and packing.

208
00:12:57,710 --> 00:13:01,740
Right now we had a print out every single item inside this tuple.

209
00:13:02,240 --> 00:13:04,150
But what it could do is the following.

210
00:13:04,430 --> 00:13:08,140
I could say let me delete the print statement.

211
00:13:08,370 --> 00:13:09,090
I could do the following.

212
00:13:09,090 --> 00:13:16,770
I could say for and make kind of a temporary variable name that looks like a tuple here.

213
00:13:16,770 --> 00:13:25,870
So for a B in my list then I can print the variable A and then print the variable b.

214
00:13:26,040 --> 00:13:30,490
So what happens now I'm saying 1 2 3 4 5 6 7 8.

215
00:13:30,540 --> 00:13:31,750
So it's actually going on here.

216
00:13:31,770 --> 00:13:37,740
Well this is called tuple unpacking where you kind of duplicate the structure of the items.

217
00:13:37,860 --> 00:13:41,700
In this case tuples inside of this sequence and then unpack them.

218
00:13:41,700 --> 00:13:44,330
So now I have access to the individual items.

219
00:13:44,370 --> 00:13:50,550
So if I only want to print out the first item in those tuples So I run this I get back 1 3 5 7 because

220
00:13:50,550 --> 00:13:52,030
not only printing a.

221
00:13:52,160 --> 00:13:53,810
So this is known as tuple in packing.

222
00:13:53,970 --> 00:13:56,600
And technically you actually don't need these princes here.

223
00:13:56,730 --> 00:14:00,340
So it's a lot more common to see it done this way with no parentheses.

224
00:14:00,540 --> 00:14:03,850
So I say for a B in my list prints a.

225
00:14:03,960 --> 00:14:08,220
We'll see what happens if we print B we should get back Aldo's even number so that second half that

226
00:14:08,220 --> 00:14:10,470
2 4 6 8.

227
00:14:10,470 --> 00:14:16,750
So again this is tuple in packing and you're going to be using it a lot as you start working with more

228
00:14:16,770 --> 00:14:22,960
built in Python functions because a lot of things will return kind of pairs and sequences in this manner.

229
00:14:22,960 --> 00:14:26,890
We have a list of tuples and want to show you one more example.

230
00:14:27,690 --> 00:14:38,050
Let's say my list is equal to a list one to three one to three while it's some different numbers say

231
00:14:38,950 --> 00:14:45,200
five six seven and we'll do one more with a nine and 10.

232
00:14:45,200 --> 00:14:47,850
I get the number four here but no big deal.

233
00:14:47,940 --> 00:14:55,510
Say for item in my list print item and there are my three tuples.

234
00:14:55,580 --> 00:14:57,180
But I can do tuple unpacking here.

235
00:14:57,230 --> 00:15:04,270
The way I can do that is make up three variable names separate them by commas so I can say A B C.

236
00:15:04,370 --> 00:15:05,610
Notice I don't have these princes.

237
00:15:05,630 --> 00:15:08,430
And then I can print out whichever one of these I want.

238
00:15:08,510 --> 00:15:14,720
For example if I just want to print out 2 6 and 9 those middle numbers I would just say B and there

239
00:15:14,720 --> 00:15:16,750
we go we have access to be just the middle numbers.

240
00:15:16,750 --> 00:15:21,890
So this is really useful when you maybe you want to reorganize these structures into some other manner

241
00:15:22,160 --> 00:15:25,980
or maybe you only want to deal with the first item in these tuple pairs.

242
00:15:26,330 --> 00:15:28,490
OK so that's tuple in packing.

243
00:15:28,550 --> 00:15:33,830
And now let's finish off this discussion about four loops by discussing how to iterate through a dictionary

244
00:15:34,760 --> 00:15:40,890
so that we're going to create a dictionary called the going to have K-1 is a key value and will have

245
00:15:40,890 --> 00:15:45,480
the number one is that value will say K-2 here.

246
00:15:47,160 --> 00:15:55,050
Call and 2 and then we'll say K3 colon three and then I go into four.

247
00:15:55,080 --> 00:16:00,260
Item in the print the item.

248
00:16:00,280 --> 00:16:02,020
So this is something that's kind of interesting.

249
00:16:02,020 --> 00:16:06,860
Notice that by default when you iterate through a dictionary you only iterate through the Keys.

250
00:16:06,880 --> 00:16:13,960
So K-1 K-2 and K3 if you walk we want to iterate through the items themselves.

251
00:16:13,980 --> 00:16:21,440
What you need to do is call the items here and now when you run that you get back these two pairs K-1

252
00:16:21,580 --> 00:16:26,760
one to two three or three which means we can use the same tuple and packing technique.

253
00:16:26,770 --> 00:16:27,960
We just discussed.

254
00:16:27,970 --> 00:16:34,270
So instead of item what it can do is say key comma value and let's say I only want to print out the

255
00:16:34,270 --> 00:16:38,670
values then I can say for key value in the items.

256
00:16:38,670 --> 00:16:44,530
Print out the value and I have 1 2 and 3 again taking advantage of tuple and packing here because the

257
00:16:44,550 --> 00:16:50,520
the items returns back something in this sort of format and more likely it's the sort of forment we

258
00:16:50,520 --> 00:16:51,800
have a key and a value.

259
00:16:51,810 --> 00:16:56,440
So we're using the same tuple packing that we just discussed now with D items.

260
00:16:56,460 --> 00:16:59,910
So by default when you iterate through a dictionary it's going to be the keys.

261
00:16:59,910 --> 00:17:04,030
If you want to iterate through the items you can use tuple unpacking packing to grab of keys and values.

262
00:17:04,200 --> 00:17:12,370
If you only want the values themselves you could do is say Diderot's and then call values for this.

263
00:17:12,700 --> 00:17:16,020
Run that and only print out the values one two three.

264
00:17:16,030 --> 00:17:20,310
The last thing I want to note here is that dictionaries are technically unordered.

265
00:17:20,320 --> 00:17:21,760
This is a very small dictionary.

266
00:17:21,760 --> 00:17:25,700
So it looks like it's everything's going in order 1 2 3 K-1 K-2 K3.

267
00:17:25,840 --> 00:17:29,970
But if you have a very large dictionary there's no guarantee that things are going to be sorted or in

268
00:17:29,980 --> 00:17:31,070
any sort of order.

269
00:17:31,150 --> 00:17:33,460
So keep that in mind when you iterate through a dictionary.

270
00:17:33,560 --> 00:17:37,380
There's no guarantee that you're going to get things back in the order you put them in.

271
00:17:37,390 --> 00:17:37,840
OK.

272
00:17:38,050 --> 00:17:42,970
So now we've learned about how to use for loops with tuples lists strings and dictionaries.

273
00:17:42,970 --> 00:17:44,670
It's going to be a really important tool for us.

274
00:17:44,670 --> 00:17:48,940
So make sure you understood it well the above examples specifically make sure you really understand

275
00:17:49,000 --> 00:17:53,110
the whitespace so probably the most prudent thing to get out of this lecture is the fact to use this

276
00:17:53,110 --> 00:17:59,950
for keyword and temporary variables you want to say then the keyword in some sort of iterable object

277
00:18:00,040 --> 00:18:02,760
either values a list or a string itself.

278
00:18:02,800 --> 00:18:08,620
As we saw a here colon and then in this indented block of code that's going to execute every single

279
00:18:08,620 --> 00:18:11,560
time for every single iterable in that list.

280
00:18:11,560 --> 00:18:12,310
Thanks everyone.

281
00:18:12,310 --> 00:18:15,630
And if you have any questions feel free to post them the Q&amp;A forums.

282
00:18:15,670 --> 00:18:16,690
I'll see you at the next lecture.
