1
00:00:05,700 --> 00:00:09,990
Welcome back in this lecture we're going to go over the solutions for the test shown in the previous

2
00:00:09,990 --> 00:00:10,850
lecture.

3
00:00:10,860 --> 00:00:13,010
Let's get started by opening up a Jupiter notebook.

4
00:00:13,170 --> 00:00:14,010
OK let's get started.

5
00:00:14,010 --> 00:00:17,370
First statements assessment test solutions the first question.

6
00:00:17,380 --> 00:00:22,440
Wanted us to create a statement that will print out words that start with an S from the string right

7
00:00:22,470 --> 00:00:23,400
here.

8
00:00:23,400 --> 00:00:25,130
So let's run this Celso.

9
00:00:25,140 --> 00:00:27,030
Now we have our string defined.

10
00:00:27,120 --> 00:00:30,630
So there's our string and we can use the split statement.

11
00:00:30,870 --> 00:00:36,260
If you recall from the strings lecture to create a list and that's split by default splits on the whitespace.

12
00:00:36,270 --> 00:00:39,020
So now we have all the words right here.

13
00:00:39,120 --> 00:00:40,990
So let's use a for loop to iterate through this.

14
00:00:41,010 --> 00:00:49,140
I can say for word in s.t. split and if I just print out the word I get back the words themselves but

15
00:00:49,140 --> 00:00:53,740
what I want to do is create some sort of if statement that checks if the word starts of an s.

16
00:00:53,760 --> 00:00:54,760
So how do I do that.

17
00:00:54,990 --> 00:00:59,390
Well I can actually grab the first letter of the word using indexing because these are all strings.

18
00:00:59,580 --> 00:01:02,750
I can say word of zero index zero.

19
00:01:02,880 --> 00:01:04,020
That's the first letter.

20
00:01:04,410 --> 00:01:14,230
So I can say if I were to index 0 the first letter is equal to the string s print word and then we go

21
00:01:14,230 --> 00:01:17,440
we have Star s and sentence.

22
00:01:17,710 --> 00:01:23,050
Something else I wanted to note here is let's imagine that there was a word here that started with a

23
00:01:23,050 --> 00:01:24,620
capital S..

24
00:01:24,670 --> 00:01:28,180
So if I say Sam prints only the words surface.

25
00:01:28,450 --> 00:01:34,540
If I run this this actually no longer works because it's missing out this capital S. what it could do

26
00:01:34,780 --> 00:01:37,760
is force this letter to be lowercase.

27
00:01:37,780 --> 00:01:41,330
When I do the comparison in order to compare it to this lowercase s.

28
00:01:41,350 --> 00:01:47,610
So there's two ways I could do this I could say if Word 0 1 it's lowercase is equal to lower case s

29
00:01:48,160 --> 00:01:50,140
and then I do grab Sam right here.

30
00:01:50,140 --> 00:01:51,670
So that's one way I can do that.

31
00:01:51,670 --> 00:01:55,800
The other way I could do that is just compare both a lowercase s and a capitalist.

32
00:01:55,840 --> 00:02:04,810
I could say if word is equal to Esraa here so if this first letter is equal to lowercase s or words

33
00:02:04,850 --> 00:02:11,510
0 is equal to capital S and then that's the exact same thing.

34
00:02:11,510 --> 00:02:17,060
Typically we're going to see from a more experienced Python programmer is the core of lower instead

35
00:02:17,060 --> 00:02:21,070
of or because it's less code and it looks a little cleaner.

36
00:02:21,110 --> 00:02:25,280
So that's a way you could deal with capital S's if they happen to be in that string to keep it simple.

37
00:02:25,280 --> 00:02:28,050
We didn't have that but just keep that in mind.

38
00:02:28,610 --> 00:02:32,900
Up next we wanted you to use range to print out all the even numbers from 0 to 10.

39
00:02:32,900 --> 00:02:34,600
So that's pretty straightforward.

40
00:02:34,790 --> 00:02:36,390
And there's lots of ways you could done this.

41
00:02:36,440 --> 00:02:43,150
You could have just set list range 0 11:02 and then we have all the numbers.

42
00:02:43,250 --> 00:02:54,160
But if we actually wanted you to print them you could have said for now in range 0 11 12 print out and

43
00:02:54,170 --> 00:02:55,440
we would have accepted that as well.

44
00:02:55,430 --> 00:02:56,780
So either one of these fine.

45
00:02:56,840 --> 00:03:02,960
Main thing I want you to realize is that range you'd have to say up to 11 if you want to go to 10 and

46
00:03:02,960 --> 00:03:06,350
then we have to have two sets that size.

47
00:03:06,450 --> 00:03:10,950
Next we want you to use a list comprehension to create a list of all numbers between 1 and 50 that are

48
00:03:10,950 --> 00:03:12,630
divisible by three.

49
00:03:12,630 --> 00:03:21,300
So the way we can do that is the following will say x for x in range and we'll go from one up to 51

50
00:03:21,300 --> 00:03:22,550
because we want to go up to 15.

51
00:03:22,560 --> 00:03:34,020
So we'll go up to about including 51 and then we can say if x mod 3 is equal to zero.

52
00:03:34,120 --> 00:03:36,860
So we run that and we get back these results.

53
00:03:37,150 --> 00:03:42,040
And this was really similar to other examples we saw when we were printing out all the even numbers

54
00:03:42,160 --> 00:03:48,090
here with just checking the visible by three instead evenly divisible That is next question was to go

55
00:03:48,090 --> 00:03:49,090
through the string below.

56
00:03:49,140 --> 00:03:54,270
And if the length of the word is even print even so let's do that it's going to be very similar to the

57
00:03:54,270 --> 00:03:54,950
first question.

58
00:03:54,990 --> 00:03:58,870
I'll say for word and s t split.

59
00:03:59,010 --> 00:04:03,420
Except now that if statement is going to check the length of the word we're going to say if the length

60
00:04:03,420 --> 00:04:13,940
of the word when divided by two has a remainder of 0 I will print out the word and or I will print out

61
00:04:13,940 --> 00:04:14,310
even.

62
00:04:14,320 --> 00:04:20,000
Doesn't really matter what you do here you can say print is events or just using some string concatenation

63
00:04:20,000 --> 00:04:25,640
there so only even that is even with even exit etc..

64
00:04:25,640 --> 00:04:27,360
So there you have it.

65
00:04:27,620 --> 00:04:32,660
The next question is a really common question called fizz buzz and that's where we have to write a program

66
00:04:32,660 --> 00:04:37,780
that prints out the integers from 1 to 100 except we're going to replace integers with words when they're

67
00:04:37,790 --> 00:04:42,540
multiples of three multiples of five or are multiples above 3 and 5.

68
00:04:42,770 --> 00:04:44,250
And there's a bit of a trick to this one.

69
00:04:44,360 --> 00:04:47,600
But first let's figure out how to actually print the numbers.

70
00:04:48,600 --> 00:04:55,910
We're going to say for now in range 1 to 101 because that actually gives us back all the way to 100

71
00:04:56,480 --> 00:05:01,070
and the first thing we need to check is multiples of both 3 and 5.

72
00:05:01,100 --> 00:05:05,810
Often what beginners will do is we'll make the mistake of checking for three first then checking for

73
00:05:05,810 --> 00:05:08,420
five and then checking for three and five.

74
00:05:08,600 --> 00:05:15,150
But the problem with that is multiples of both three and five are technically also a multiple of three.

75
00:05:15,320 --> 00:05:18,980
So you actually end up printing out things we should be printing out fizz buzz.

76
00:05:19,150 --> 00:05:24,320
So the very first thing you check for logically is three and five with Hizbollah's.

77
00:05:24,350 --> 00:05:35,640
So we will say if numb mod 3 is equal to zero and numb Mada 5 is equal to zero.

78
00:05:37,030 --> 00:05:40,830
We will printout fizz buzz.

79
00:05:40,970 --> 00:05:53,160
Then we can check for the other cases so we can say LLF numb my three is equal to zero print out is.

80
00:05:53,400 --> 00:05:55,590
And then we could say Elif numb.

81
00:05:55,680 --> 00:05:58,870
Five is equal to zero.

82
00:05:58,980 --> 00:06:06,930
Printout buzz else remember we want to print the integers so we'll just print out the integer print

83
00:06:06,930 --> 00:06:08,480
the number itself.

84
00:06:08,490 --> 00:06:11,490
So when you do this you get back 1 to is four.

85
00:06:11,490 --> 00:06:16,460
Buzz is and if you keep going all the way to 15 finally you get fizz buzz.

86
00:06:16,470 --> 00:06:20,570
So the main trick here is to check for three and five first.

87
00:06:20,580 --> 00:06:24,900
That way you don't actually trigger one of these accident.

88
00:06:25,620 --> 00:06:29,550
Then moving on to the next question you wanted to use less comprehension to create a list of the first

89
00:06:29,550 --> 00:06:35,010
letters of every word in the string below and to do this it's going to be a bit similar to what we did

90
00:06:35,010 --> 00:06:41,890
before we say word for word and Esti split.

91
00:06:42,200 --> 00:06:44,140
But I just want to grab the actual first letter.

92
00:06:44,150 --> 00:06:49,230
So if I were to run this it would be bringing back all the words I just want the first letter.

93
00:06:49,250 --> 00:06:51,660
So here I index for zero.

94
00:06:51,950 --> 00:06:56,460
And when I run that I get back the first letter for every word in the string.

95
00:06:56,870 --> 00:07:00,330
Ok that's it takes some time to review these.

96
00:07:00,410 --> 00:07:04,160
A lot of these are kind of tricky and you have to wrap your head around them at first but hopefully

97
00:07:04,160 --> 00:07:09,350
with this solution or review you kind of start getting the idea of applying and mixing the ideas that

98
00:07:09,350 --> 00:07:12,840
we've just talked about together to solve real problems.

99
00:07:12,860 --> 00:07:16,970
Coming up next we're going to go to the next section where we're going to begin understanding things

100
00:07:16,970 --> 00:07:18,170
like functions.

101
00:07:18,260 --> 00:07:18,840
I'll see you there.
