1
00:00:05,360 --> 00:00:06,320
Welcome back everyone.

2
00:00:06,350 --> 00:00:12,620
Let's not take some time to talk about wild loops while loops will continue to execute a block of code

3
00:00:12,770 --> 00:00:14,850
while some condition remains true.

4
00:00:15,140 --> 00:00:18,910
So for example we try to relate this to a sort of real life scenario.

5
00:00:19,250 --> 00:00:24,440
While my pool is not fool maybe I want a program to keep filling my pool of water or if I'm designing

6
00:00:24,440 --> 00:00:28,990
a robot some code while my dogs are still hungry keep feeding my dogs.

7
00:00:29,040 --> 00:00:32,770
So let's explore the syntax of a WHILE loop section quite simple.

8
00:00:32,780 --> 00:00:37,550
We're going to say a while and then some sort of blink condition so we'll usually have some sort of

9
00:00:37,550 --> 00:00:42,910
comparison operation check and a lot of times it's going to be while some variable is true.

10
00:00:42,920 --> 00:00:48,510
Or as equal equals true then colon and a block of code and that block of code in this case represented

11
00:00:48,620 --> 00:00:52,620
by do something is going to continue to execute while the condition is true.

12
00:00:53,780 --> 00:00:57,520
Now we can also combine a with statement with an else statement if we want.

13
00:00:57,650 --> 00:00:59,980
So it can say while some boolean condition is true.

14
00:01:00,050 --> 00:01:01,640
Do something else.

15
00:01:01,790 --> 00:01:05,870
Meaning once that condition is no longer true or since the beginning it was no longer true.

16
00:01:05,960 --> 00:01:08,000
We do something different.

17
00:01:08,050 --> 00:01:10,600
Let's explore these concepts with some examples.

18
00:01:10,760 --> 00:01:12,750
Let's hop to it in a note book.

19
00:01:12,760 --> 00:01:16,010
All right so let's start off by building a very basic while loop.

20
00:01:16,020 --> 00:01:17,110
I'm going to create variable.

21
00:01:17,110 --> 00:01:25,440
Say x is equal to zero and then say while X is less than let's say less than five.

22
00:01:25,510 --> 00:01:27,310
So we have a whole lot of printing.

23
00:01:27,340 --> 00:01:31,270
We're going to print the current value of x or we'll say print the current value of x.

24
00:01:31,270 --> 00:01:32,550
Lots of different ways we can do this.

25
00:01:32,560 --> 00:01:35,320
We'll just do it quickly with an F string literal.

26
00:01:35,320 --> 00:01:43,030
The current value of x is an inside curly braces will pass and x.

27
00:01:43,030 --> 00:01:44,560
Now let's imagine what's going to happen here.

28
00:01:44,560 --> 00:01:49,420
Don't run this Sayliyah will say x is equal to zero while X is less than 5.

29
00:01:49,420 --> 00:01:54,490
I'm going to print out the current value of x so if I were just to run this basically I would say the

30
00:01:54,490 --> 00:01:59,950
current value of x is zero and that would happen over and over and over again because X isn't changing.

31
00:01:59,950 --> 00:02:07,210
So let's update x during this while loop will say x is equal to x plus 1.

32
00:02:07,210 --> 00:02:09,080
So now let's run this and see what happens.

33
00:02:10,340 --> 00:02:10,880
I get back.

34
00:02:10,880 --> 00:02:14,950
The current value of x is 0 than 1 than 2 than 3 than 4.

35
00:02:15,120 --> 00:02:20,790
And on that last one when it's equal to 4 we then say OK X is going to be equal to 4 plus 1 which makes

36
00:02:20,880 --> 00:02:23,700
X equal to 5 and 5 is not less than 5.

37
00:02:23,700 --> 00:02:24,620
It's equal to it.

38
00:02:24,630 --> 00:02:28,140
So then the while loop stops running because it's only going to run.

39
00:02:28,200 --> 00:02:34,440
While this condition is true now if you accidentally have some sort of condition that never becomes

40
00:02:34,440 --> 00:02:35,080
false.

41
00:02:35,190 --> 00:02:39,530
Maybe you forgot this last line of code here and it just ran over and over and over again.

42
00:02:39,780 --> 00:02:44,160
So what happens is you get infinite while loop and it's a common mistake for beginners to make.

43
00:02:44,160 --> 00:02:48,720
I know if I made it myself a bunch of times when I was learning Python what happens in the notebook

44
00:02:48,810 --> 00:02:54,630
is you get a little star here in the clue that the cell is still continually running in order to kind

45
00:02:54,630 --> 00:03:00,300
of reset things just come over here its kernel and either interrupt and if that doesn't work then hit

46
00:03:00,360 --> 00:03:05,610
restart and restart your kernel sometimes interrupt won't work because basically the command to interrupt

47
00:03:05,670 --> 00:03:10,500
won't get there because Python's using all your computing to kind of continue this while loop.

48
00:03:10,710 --> 00:03:14,490
So keep that in mind if you ever get an infinite while loop just come over here to a kernel and then

49
00:03:14,490 --> 00:03:17,180
hit restart and you'll have to read the finds themselves again.

50
00:03:17,190 --> 00:03:20,890
But that's the basics of avoiding the error.

51
00:03:20,900 --> 00:03:23,960
OK so we have the while loop by itself and a quick note.

52
00:03:24,110 --> 00:03:27,150
This statement of x is equal to x plus 1.

53
00:03:27,170 --> 00:03:33,030
We can also write that in the following manner we can say X plus equals 1.

54
00:03:33,110 --> 00:03:35,900
So this line and this line are the same thing.

55
00:03:35,900 --> 00:03:37,790
This is just a more compact way of writing it.

56
00:03:37,970 --> 00:03:41,110
So if you have a reading someone else's code and they wrote this instead.

57
00:03:41,210 --> 00:03:43,780
That's kind of this sleeker way of writing that same code.

58
00:03:43,790 --> 00:03:47,870
Let me write one this again because they know to get back the same values.

59
00:03:48,230 --> 00:03:52,180
Let's attach the else statement to this while loop so we can say.

60
00:03:52,190 --> 00:03:54,780
Else colon.

61
00:03:55,280 --> 00:03:56,830
And I'm going to print.

62
00:03:57,140 --> 00:04:05,140
X is not less than 5 you know we run this let's see what happens you get current values so all those

63
00:04:05,140 --> 00:04:06,250
other print statements.

64
00:04:06,400 --> 00:04:08,860
And then I see X is not less than five.

65
00:04:08,920 --> 00:04:13,970
So once this while condition is not true because this else is lined up with it we went ahead and printed

66
00:04:13,990 --> 00:04:15,670
X is not less five.

67
00:04:15,760 --> 00:04:22,220
Now as quick know if right off the bat X was greater than five so let's make ex-cult 50.

68
00:04:22,260 --> 00:04:27,200
And I run this then the while loop never executes because this condition is never true.

69
00:04:27,260 --> 00:04:30,540
Then we have the else condition execute anyways.

70
00:04:30,640 --> 00:04:31,700
That doesn't mean I need it.

71
00:04:31,750 --> 00:04:34,700
So if I did not have that there and I ran this just nothing would happen.

72
00:04:34,720 --> 00:04:36,690
Because this isn't true.

73
00:04:36,690 --> 00:04:36,970
All right.

74
00:04:36,970 --> 00:04:41,350
Finally I want to introduce three key words that are really useful with loops.

75
00:04:41,410 --> 00:04:45,670
You're not going to be using them all the time but sometimes you'll need their use cases.

76
00:04:45,670 --> 00:04:47,130
And that's the break keyword.

77
00:04:47,170 --> 00:04:52,920
The continue keyword and the past keyword break is going to break out of the current closest inclosing

78
00:04:52,960 --> 00:04:54,240
loop continue.

79
00:04:54,310 --> 00:04:59,140
Goes to the top of the close in closing loop and then pass does nothing at all.

80
00:04:59,590 --> 00:05:01,570
So let's start a little bit backwards here.

81
00:05:01,570 --> 00:05:06,220
Go with pass and we can show this actually if a for loop might be a little simpler here.

82
00:05:06,250 --> 00:05:17,290
We're going to say for let's make a quick list will say x is equal to 1 to 3 and then say for item X

83
00:05:17,380 --> 00:05:22,070
colon and let's say we didn't quite know yet what we wanted to do here.

84
00:05:22,090 --> 00:05:24,160
We essentially want some filler.

85
00:05:24,250 --> 00:05:27,100
If I just have a comment here and I try running this.

86
00:05:27,130 --> 00:05:30,660
So when you attempt to run this you actually get an error syntax error.

87
00:05:30,700 --> 00:05:39,820
Unexpected end of file while parsing and what that means is Python because it uses indentation and whitespace

88
00:05:40,000 --> 00:05:41,850
as a crucial part of its code.

89
00:05:41,890 --> 00:05:46,390
It expects you to basically have something here even a comment isn't enough to fill it.

90
00:05:46,510 --> 00:05:52,900
So often what you can do is have the past keyword and the past just says do nothing at all and a lot

91
00:05:52,900 --> 00:05:59,500
of times programmers keep it as a placeholder to kind of avoid a syntax error because you can imagine

92
00:05:59,590 --> 00:06:03,730
implementing a larger piece of code and I know eventually I want to come up and fill out this for loop

93
00:06:03,730 --> 00:06:04,290
right now.

94
00:06:04,360 --> 00:06:05,620
I don't want to deal with it.

95
00:06:05,920 --> 00:06:10,620
Let's say I wanted to print and of my scripts over here.

96
00:06:10,880 --> 00:06:15,830
Now when I run this I don't get that syntax error and I'm still able to print out my script if I just

97
00:06:15,830 --> 00:06:17,390
have the comments.

98
00:06:17,390 --> 00:06:21,160
I get an error because it's expecting something to happen after this colon.

99
00:06:21,200 --> 00:06:25,550
So in order to avoid that you can use the pass keyword and you're going to see the past keyword use

100
00:06:25,550 --> 00:06:31,580
the lot when we start building out functions and methods ourselves because often when you're programming

101
00:06:31,640 --> 00:06:35,020
you'll create a little holder for a function but you don't want to test it right away.

102
00:06:35,150 --> 00:06:40,160
So you use the past keyword as something to put in place there so you avoid syntax errors and you're

103
00:06:40,160 --> 00:06:44,540
also safe because you know a pass doesn't do anything it just says well I'll pass this and I'm not going

104
00:06:44,540 --> 00:06:45,590
to do anything.

105
00:06:45,800 --> 00:06:50,080
All right so that's the past cure does nothing at all continue.

106
00:06:50,090 --> 00:06:52,550
Goes to the top of the closest inclosing loop.

107
00:06:52,550 --> 00:06:54,120
Show you an example of that.

108
00:06:54,280 --> 00:07:07,140
Intellectually as for loops again for this we're going to say my string is equal to Sammy so let's have

109
00:07:07,140 --> 00:07:07,790
that there.

110
00:07:07,900 --> 00:07:16,070
I will say four letter in my string print out the letter.

111
00:07:16,130 --> 00:07:18,240
So here we see Samie printed out.

112
00:07:18,260 --> 00:07:23,090
Now what I'm going to do is let's imagine that I actually don't want to print the letter A for whatever

113
00:07:23,090 --> 00:07:25,190
reason I could do the following.

114
00:07:25,190 --> 00:07:36,150
If the letter is equal to lowercase a colon continue and I want to run this I'm not actually printing

115
00:07:36,150 --> 00:07:37,040
out the letter.

116
00:07:37,110 --> 00:07:39,460
So what's happening here will continue.

117
00:07:39,690 --> 00:07:45,380
Basically tells this program to go back to the top of the closest in closing loop.

118
00:07:45,690 --> 00:07:47,400
So here's continue.

119
00:07:47,400 --> 00:07:51,330
And we have a loop up here four letter in my string.

120
00:07:51,480 --> 00:07:57,690
So if that letter is equal to a we continue meaning go back to the top of the closest in closing loop.

121
00:07:57,960 --> 00:08:02,940
Note that we are specifically saying loop were not saying some sort of statement here.

122
00:08:02,940 --> 00:08:04,840
So thats where semantics can come into play.

123
00:08:05,040 --> 00:08:11,620
But what this is saying is if the letter is a go ahead and continue and go back to the loop so that

124
00:08:11,740 --> 00:08:16,000
you can use the continue statement if you're a beginner in Python programming it's definitely not something

125
00:08:16,000 --> 00:08:18,980
that comes up a lot and you're beginning code.

126
00:08:19,000 --> 00:08:22,650
It's going to happen that more naturally you'll start using it much later on.

127
00:08:22,780 --> 00:08:27,820
So be aware of it but don't worry about use cases right now you won't really be using it that often.

128
00:08:27,880 --> 00:08:32,130
And then finally break breaks out of the current closest closing loop.

129
00:08:32,260 --> 00:08:37,360
So let's see what happens if we replace continue here with break instead.

130
00:08:37,390 --> 00:08:41,860
Now when we run this what happens is instead of going back to the top of the closest inclosing loop

131
00:08:42,190 --> 00:08:47,740
it breaks out of the current closest inclosing loop meaning it just breaks this loop and stops this

132
00:08:47,740 --> 00:08:48,350
loop.

133
00:08:48,730 --> 00:08:50,450
If that letter is equal to a.

134
00:08:50,530 --> 00:08:53,840
So that's actually really useful especially with wild statements.

135
00:08:53,920 --> 00:08:56,810
So let's see an example of using break for a while statement.

136
00:08:56,840 --> 00:08:59,300
Zoom in here just so we can see it clearly.

137
00:08:59,430 --> 00:09:07,440
I'm going to do the same thing as before we'll say x is equal to 5 and then we'll say while nexilis

138
00:09:07,440 --> 00:09:08,230
make it equal to zero.

139
00:09:08,230 --> 00:09:20,310
Here just as before while X is less than five print outs X and then we'll say x is equal to x plus 1.

140
00:09:20,670 --> 00:09:23,510
So when we run that as expected I get 0 1 2 3 4.

141
00:09:23,640 --> 00:09:30,600
But let's imagine I want to break this while loop if X was ever to then I can do the same thing here.

142
00:09:30,600 --> 00:09:38,840
I say if x is equal to to any moment go ahead and break out of this while loop.

143
00:09:38,850 --> 00:09:42,280
Now when we run this we only get back 0 and 1.

144
00:09:42,750 --> 00:09:43,390
OK.

145
00:09:43,530 --> 00:09:48,120
So that's the basics of this break continue and pass keyword.

146
00:09:48,150 --> 00:09:50,180
So what we covered in this lecture was the while loop.

147
00:09:50,190 --> 00:09:53,730
So while some condition is true execute this block of code.

148
00:09:53,760 --> 00:09:58,590
Be careful here because if you accidentally say while and this condition is constantly true you may

149
00:09:58,590 --> 00:10:03,240
have to restart your kernel if you're running a Python script and you end up getting the error that

150
00:10:03,240 --> 00:10:06,030
the while loop is just never closing.

151
00:10:06,030 --> 00:10:08,780
You should try Control-C that typically interrupts.

152
00:10:08,790 --> 00:10:14,120
So you can even see here interrupt says control seats that go to the kernel.

153
00:10:14,350 --> 00:10:18,670
All right then we have the break continue the past few words again not going to be using these right

154
00:10:18,670 --> 00:10:22,820
off the bat especially things like continue but will probably be using break a little more.

155
00:10:22,840 --> 00:10:26,500
And then once we get used to functions we'll be using pass a little more and then much later on we'll

156
00:10:26,500 --> 00:10:27,610
be using continue.

157
00:10:27,610 --> 00:10:32,160
But you should be aware of these keywords now that you understand before loops and while loops.

158
00:10:32,500 --> 00:10:34,840
OK thanks everyone and I'll see you at the next lecture.
