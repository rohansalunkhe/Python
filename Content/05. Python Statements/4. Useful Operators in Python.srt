1
00:00:05,540 --> 00:00:07,400
Welcome back everyone in this lecture.

2
00:00:07,400 --> 00:00:11,990
We're going to discuss a few built in functions and operator key words in Python that didn't really

3
00:00:11,990 --> 00:00:14,190
fit well into any of the previous categories.

4
00:00:14,300 --> 00:00:18,950
However this is really important and they're going to really enhance your ability to program in Python.

5
00:00:19,130 --> 00:00:21,940
Let's open up a super notebook and quickly run through these.

6
00:00:22,280 --> 00:00:26,240
Let's begin by talking about the range function in Python.

7
00:00:26,240 --> 00:00:33,630
Notice that often we were creating lists that were just a series of integers as 1 2 3 or 1 3 4 5 et

8
00:00:33,640 --> 00:00:37,580
cetera all the way up to 10 because it's such a common task.

9
00:00:37,580 --> 00:00:40,120
Python actually has a built in operator for this.

10
00:00:40,190 --> 00:00:41,990
And there's two ways you can use it.

11
00:00:42,050 --> 00:00:44,120
One is you can't iterate through it.

12
00:00:44,120 --> 00:00:47,100
So I can say something like For numb and.

13
00:00:47,210 --> 00:00:51,350
And then we use the keyword range and it takes a couple of parameters.

14
00:00:51,350 --> 00:00:57,320
You can do shift tab here to see the parameters it takes but basically as a start stop and optional

15
00:00:57,320 --> 00:00:58,380
step size.

16
00:00:58,400 --> 00:01:00,780
So start is technically also optional.

17
00:01:00,800 --> 00:01:02,660
Let's show you how we can actually use these.

18
00:01:02,930 --> 00:01:10,410
So if we just indicate a single number such as 10 and we say Prince numb in range 10 what it's going

19
00:01:10,410 --> 00:01:16,000
to do is it's going to generate the numbers from 0 all the way up to but not including 10.

20
00:01:16,110 --> 00:01:20,970
So that all the way up to but not including That's very similar to that slicing syntax we saw earlier

21
00:01:20,970 --> 00:01:22,070
in the course.

22
00:01:22,080 --> 00:01:27,810
So for now I'm in range 10 percent number in here we see 0 through 9.

23
00:01:27,810 --> 00:01:32,610
Now let's say we want to specify a starting index position for the integers.

24
00:01:32,610 --> 00:01:36,790
We could say start three go all the way up to but not including 10.

25
00:01:36,990 --> 00:01:38,880
And then we run this we get three.

26
00:01:39,030 --> 00:01:41,990
And then all the way up to but not including 10.

27
00:01:42,000 --> 00:01:43,730
So that's the range operator.

28
00:01:43,920 --> 00:01:47,480
And in addition to all this we can actually have a step size as well.

29
00:01:47,790 --> 00:01:53,400
So we can add a third argument for step size and that gives us the even numbers so that's taking jumps

30
00:01:53,460 --> 00:01:57,380
and steps 2 from zero all the way up to but not including 10.

31
00:01:57,390 --> 00:02:01,080
So if we wanted to include 10 at one of those numbers we'd have to put an 11 there.

32
00:02:01,290 --> 00:02:04,180
And then we get back zero to four six eight 10.

33
00:02:04,230 --> 00:02:07,410
So that's how you can use the range function for iteration.

34
00:02:07,600 --> 00:02:13,800
But you'll notice if I copy and paste this into a cell and ask for it to be returned to me I just get

35
00:02:13,800 --> 00:02:17,160
back this range operator and it's because it's a generator.

36
00:02:17,160 --> 00:02:20,080
So actually I want the actual list of numbers.

37
00:02:20,280 --> 00:02:26,970
What I can do is cast it to a list and then I get back the list of numbers later on the course we're

38
00:02:26,970 --> 00:02:32,400
going to discuss generators and a lot more detail but basically a generator is a special type of function

39
00:02:32,490 --> 00:02:36,050
that will generate information instead of saving it all to memory.

40
00:02:36,060 --> 00:02:41,250
So this is a more efficient way of generating these numbers instead of having a giant list stored in

41
00:02:41,250 --> 00:02:46,250
memory and we'll discuss that and a lot more detail in the generators section of the course.

42
00:02:46,810 --> 00:02:51,910
Let's move on to discussing another really useful function called enumerate and to show you the use

43
00:02:51,910 --> 00:02:53,660
case and motivation behind numerate.

44
00:02:53,680 --> 00:02:55,880
Let's do an example.

45
00:02:56,080 --> 00:03:00,090
I'm going to set a variable called index count equal to zero.

46
00:03:00,190 --> 00:03:01,030
Let me zoom in here.

47
00:03:01,030 --> 00:03:10,660
One level of index count is equal to zero and then four letter and the string ABC the I'm going to do

48
00:03:10,660 --> 00:03:11,770
some printing here.

49
00:03:11,920 --> 00:03:20,270
I'm going to say index the letter is curly braces.

50
00:03:20,310 --> 00:03:25,670
Then I'll use that format here to say Index counts and then the letters.

51
00:03:25,680 --> 00:03:27,160
Well OK.

52
00:03:27,170 --> 00:03:30,490
So what I'm doing here is index count.

53
00:03:30,500 --> 00:03:41,020
The letter is blank and then what I will do is say Index counts plus equals 1.

54
00:03:41,040 --> 00:03:47,820
So when I run this it says back the following information at index 0 the letters in X-1 letters B and

55
00:03:47,820 --> 00:03:50,580
so on all the way until the end.

56
00:03:50,580 --> 00:03:56,190
Now this is such a common operation that we're going to do is use the enumerate function to replicate

57
00:03:56,220 --> 00:03:56,630
this.

58
00:03:56,640 --> 00:04:01,620
And the reason this is very common is because a lot of times you will want to have some sort of counter

59
00:04:01,890 --> 00:04:07,170
for how many times have we gone through this for loop or what index position you are at in this particular

60
00:04:07,170 --> 00:04:08,020
string.

61
00:04:08,040 --> 00:04:14,830
And the reason for that is because often people like to do something like this they like to let's define

62
00:04:14,950 --> 00:04:17,950
word as a b c d e

63
00:04:20,830 --> 00:04:33,160
and then say four letter in a word prints word that index count and then we say Index counts plus equals

64
00:04:33,220 --> 00:04:33,910
1.

65
00:04:34,240 --> 00:04:38,480
So it's actually happening in this loop while we're doing the following.

66
00:04:38,530 --> 00:04:44,650
We have an index count starts at 0 and we have a word of string ABC the then for every letter in that

67
00:04:44,650 --> 00:04:44,980
word.

68
00:04:44,980 --> 00:04:51,680
So for every character here what we end up doing is reprints the index value location at that word.

69
00:04:51,700 --> 00:04:53,530
And that's the same thing as printing the letter.

70
00:04:53,680 --> 00:05:00,150
But oftentimes all we're doing is using this sort of syntax of a list later on in our code.

71
00:05:00,250 --> 00:05:05,760
But North do this when he's have some sort of running index count so when I run this I get back ABC

72
00:05:05,760 --> 00:05:13,750
the because this sort of operation is really common later on what Python did is they built in the enumerate

73
00:05:13,750 --> 00:05:16,510
function.

74
00:05:16,580 --> 00:05:19,490
So let's see what happens when we use enumerate.

75
00:05:19,490 --> 00:05:27,900
So I went into Dilley inducts counts here and I'll say for item in enumerate word prints out the item

76
00:05:28,790 --> 00:05:35,660
and now want to run this I get back Notice tuples So 0 with a one with B to A C.

77
00:05:35,880 --> 00:05:41,020
So this is basically doing that index count for us automatically in the form of tuples.

78
00:05:41,130 --> 00:05:44,300
And as we know we have tuple and packing from the for loop lecture.

79
00:05:44,310 --> 00:05:52,790
So I could say the following I could say Index comma letter and then I could print out the index and

80
00:05:52,790 --> 00:05:58,840
then print the letter separately and then print out a new line using an escape sequence character.

81
00:05:59,360 --> 00:06:04,210
So now I say 0 a 1 B to C etc and so on.

82
00:06:04,520 --> 00:06:11,390
So this is the enumerate function and it can take in any iterable object and what it does is it returns

83
00:06:11,390 --> 00:06:18,770
back some sort of index counter and then the object itself or the elements at that particular iteration.

84
00:06:18,770 --> 00:06:24,290
The next one I want to show you is the Zipp function which is kind of almost like an opposite of enumerate.

85
00:06:24,680 --> 00:06:28,130
What it does is it zips together to lists.

86
00:06:28,160 --> 00:06:37,210
So I'll say my list one is equal to one two three four five and I'm going to make my list to equal to

87
00:06:37,930 --> 00:06:43,070
a b c the actually is just to ABC once you three.

88
00:06:43,080 --> 00:06:45,030
That way they match up.

89
00:06:45,330 --> 00:06:52,700
And then what I will do here is I will zip them together using the zip function.

90
00:06:52,710 --> 00:06:57,630
Now if I just runs if I don't get back anything it just says hey you have this generator waiting for

91
00:06:57,630 --> 00:07:01,570
you at this location in your memory.

92
00:07:01,860 --> 00:07:03,850
If we actually iterate through it.

93
00:07:03,960 --> 00:07:11,140
So if I say for item in zip my list and then I print out the item.

94
00:07:11,360 --> 00:07:14,010
Notice back we get back this list of tuples.

95
00:07:14,080 --> 00:07:19,000
So previously in that for loop lecture I mentioned that tuple and packing is going to be a really useful

96
00:07:19,000 --> 00:07:23,430
thing because a lot of built in functions that Python return back a list of tuples.

97
00:07:23,430 --> 00:07:25,180
And this is one such function.

98
00:07:25,210 --> 00:07:31,660
The Zipp function will just like a zipper on a jacket zipped together these two lists and pair up these

99
00:07:31,750 --> 00:07:36,480
items so that they match together and you can actually do this with more than just Toolis.

100
00:07:36,580 --> 00:07:38,720
So I could say Maylis 3.

101
00:07:38,890 --> 00:07:46,490
And let's add the sequel to some digits here will say 100 200 300 capacity.

102
00:07:46,510 --> 00:07:49,660
This is another thing Maylis 3.

103
00:07:49,840 --> 00:07:55,090
And now I can see the output is these two balls of three items.

104
00:07:55,090 --> 00:07:58,250
A common question is what happens if these lists are uneven.

105
00:07:58,420 --> 00:07:59,600
Well we can check that.

106
00:07:59,770 --> 00:08:02,700
Let's add in a bunch more elements to the first list.

107
00:08:02,740 --> 00:08:04,600
So we're going to say four or five six here.

108
00:08:04,600 --> 00:08:07,720
Now when we run this we actually get back the same results.

109
00:08:07,720 --> 00:08:13,930
So keep in mind Zipp is only going to be able to go and zip together as far as the list which is the

110
00:08:13,930 --> 00:08:14,740
shortest.

111
00:08:14,830 --> 00:08:16,170
So it won't give an error.

112
00:08:16,210 --> 00:08:21,890
I'll just ignore everything else that's extra you see here that we iterated through the zip.

113
00:08:21,910 --> 00:08:25,830
But if you actually just want to get the list itself you can just cast it using a list.

114
00:08:26,050 --> 00:08:31,740
So we can say list of Zipp and then we'll put these lists together.

115
00:08:32,560 --> 00:08:35,510
And there you go one a to b three C..

116
00:08:35,530 --> 00:08:35,740
All right.

117
00:08:35,740 --> 00:08:37,120
That's the Zipp operator.

118
00:08:37,120 --> 00:08:43,060
Very useful for zipping together to lists if you want to later use them together using tuple and packing.

119
00:08:43,060 --> 00:08:49,320
So as I mentioned you could unpack these so I could say a column become a C and maybe just print out

120
00:08:49,740 --> 00:08:55,120
B here and I only get back the letters now want to show you the in operator.

121
00:08:55,150 --> 00:09:00,220
We've already seen that in keyword in the for loop but we can use it to quickly check if an object is

122
00:09:00,280 --> 00:09:02,400
in a list.

123
00:09:02,530 --> 00:09:08,610
I can say is X in the list 1 to 3 and 0 return back a boolean true or false.

124
00:09:08,650 --> 00:09:10,630
Is it in 1 to 3.

125
00:09:10,660 --> 00:09:13,300
I could also use it for a list of letters.

126
00:09:13,300 --> 00:09:17,790
So for example let's not say X Y Z.

127
00:09:19,650 --> 00:09:22,760
An hour returns true because x is in that list.

128
00:09:22,770 --> 00:09:26,230
So this is a really nice way to quickly check if something is in a list.

129
00:09:26,400 --> 00:09:31,580
So Naledge changes to two in 1 to 3 and it get back true the keyword.

130
00:09:31,620 --> 00:09:35,210
It also works on other iterable object types and strings as well.

131
00:09:35,250 --> 00:09:36,960
You can say is a in

132
00:09:39,670 --> 00:09:43,620
O world and it returns back true.

133
00:09:43,630 --> 00:09:52,150
This also works in dictionaries as well for their keys so I could say my key and then check if it's

134
00:09:52,150 --> 00:09:52,840
in a dictionary.

135
00:09:52,840 --> 00:09:56,160
Let's quickly create one that has that key Mikey.

136
00:09:56,230 --> 00:09:57,370
And let's give it some value.

137
00:09:57,370 --> 00:10:03,220
Three four five and it returns back true because my key is in this dictionary if you want to check if

138
00:10:03,220 --> 00:10:08,050
it's an items or if it's in the values then you could just call that itself.

139
00:10:08,050 --> 00:10:11,240
So to show you what I mean by that let's get back to the story.

140
00:10:13,720 --> 00:10:18,270
And then it could say hey is three four or five in D.

141
00:10:20,270 --> 00:10:21,400
Values.

142
00:10:21,940 --> 00:10:25,040
Now returns back true if I asked for keys.

143
00:10:25,240 --> 00:10:27,070
It returns back false.

144
00:10:27,170 --> 00:10:29,270
So that's the in keyword operator.

145
00:10:29,360 --> 00:10:30,870
Very useful for quickly checking.

146
00:10:30,890 --> 00:10:33,980
I like to use it a lot when working of strings.

147
00:10:33,980 --> 00:10:37,910
A few more than I want to show you is some mathematical functions minimax.

148
00:10:37,940 --> 00:10:40,020
And then the random library.

149
00:10:40,070 --> 00:10:47,660
So if you ever have a string of numbers let's quickly create one my list 10 20 30 40 and let's go jump

150
00:10:47,660 --> 00:10:48,670
to 100.

151
00:10:48,920 --> 00:10:54,510
If I ever wanted to know the minimum value there there's a built in min function that it could pass

152
00:10:54,510 --> 00:10:56,910
in and airport spec the minimum of the list.

153
00:10:56,910 --> 00:11:02,010
And there's also a max function there reports back the maximum value of a list.

154
00:11:02,100 --> 00:11:07,020
So keep that in mind because often it's going to be very tempting to use min and max as keywords but

155
00:11:07,020 --> 00:11:08,930
they're already built in functions in Python.

156
00:11:08,940 --> 00:11:13,620
So should not use those and that's denoted by the fact that they show you syntax highlighting their

157
00:11:14,520 --> 00:11:14,810
OK.

158
00:11:14,810 --> 00:11:16,900
Let's quickly discuss the random library.

159
00:11:17,150 --> 00:11:21,590
So Python actually comes of its built in random library and there is a ton of functions included in

160
00:11:21,590 --> 00:11:22,460
this library.

161
00:11:22,700 --> 00:11:28,310
And this is actually going to be a first example of importing a library of important functions from

162
00:11:28,310 --> 00:11:28,840
a library.

163
00:11:28,850 --> 00:11:37,710
What you can do is say from random imports and if you hit tab here you should be able to see a big drop

164
00:11:37,710 --> 00:11:39,140
down list of options.

165
00:11:39,320 --> 00:11:44,590
And a lot of these are the functions that are available to you inside this particular builtin library.

166
00:11:44,600 --> 00:11:47,720
So the one we're going to show you right now is shuffle.

167
00:11:47,720 --> 00:11:52,490
So what this is saying is hey from that random library that's built into Python.

168
00:11:52,490 --> 00:11:54,940
Import the shuffle function.

169
00:11:55,040 --> 00:11:59,590
And later on in this course we'll discuss how to download other libraries from the Internet.

170
00:11:59,870 --> 00:12:01,350
Right now Rannells built in.

171
00:12:01,550 --> 00:12:07,870
So now we have this shuffle function and what it does is it randomly shuffles around any sort of list.

172
00:12:07,880 --> 00:12:09,540
So let's create another list.

173
00:12:10,810 --> 00:12:15,570
And let's make in an order so three four five six seven eight nine ten.

174
00:12:16,260 --> 00:12:23,850
And if we shuffle this list and then call the list again it's now shuffled or scrambled.

175
00:12:23,850 --> 00:12:28,020
And if we run the shuffle again and notice here I'm going to run that one more time.

176
00:12:28,260 --> 00:12:29,890
It's going to reshuffle the list.

177
00:12:29,940 --> 00:12:32,570
You should also note that it's actually not returning anything.

178
00:12:32,580 --> 00:12:40,970
So if I asked for this random list as equal to shuffle that's not going to work because it's not returning

179
00:12:40,970 --> 00:12:41,510
anything.

180
00:12:41,510 --> 00:12:43,890
So it's going to just say hey there's nothing there.

181
00:12:43,890 --> 00:12:48,130
And if you check out the type of this it again says none type.

182
00:12:48,170 --> 00:12:53,300
So this is an in-place function meaning it operates in place on that list.

183
00:12:53,300 --> 00:12:55,690
So for check on my list now still scrambled.

184
00:12:56,120 --> 00:12:56,580
OK.

185
00:12:56,720 --> 00:12:58,420
So that's my list.

186
00:12:58,640 --> 00:13:01,510
And that's the shuffle function for shuffling a list.

187
00:13:01,520 --> 00:13:04,720
There's also a really nice function for grabbing a random integer.

188
00:13:05,210 --> 00:13:13,540
So we can say from random import Rande I N T and that allows us to quickly grab a random integer.

189
00:13:13,540 --> 00:13:21,770
So I will say Rand I.A. and what we do here is we pasan a lower range any upper range.

190
00:13:21,770 --> 00:13:28,390
So I can say Rand Diante from 0 to 100 and it returns back some random integer in a fight.

191
00:13:28,400 --> 00:13:34,170
Do this again from 0 to 100 in other random integer and most likely your numbers will be different than

192
00:13:34,170 --> 00:13:34,700
mine.

193
00:13:34,740 --> 00:13:41,790
Because again it's random and because it is returning something I can save it so I can say my name is

194
00:13:41,790 --> 00:13:45,960
equal to Ranz I t zero to 10.

195
00:13:46,220 --> 00:13:48,920
And then I can later on use that number.

196
00:13:48,950 --> 00:13:54,690
Finally let's show you how to accept user input and it's actually quite easy with the input function.

197
00:13:55,160 --> 00:14:00,020
So we use the special keyword input function and then we have the text that wants to show up when we

198
00:14:00,020 --> 00:14:02,020
actually ask the user for input.

199
00:14:02,240 --> 00:14:10,720
So say hey enter a number here call and space and when you run this now pythons asking you to enter

200
00:14:10,720 --> 00:14:11,180
a number.

201
00:14:11,250 --> 00:14:14,820
So we'll go ahead and enter one will say 50 and there we go.

202
00:14:14,820 --> 00:14:20,420
Now it has 50 typically where you're going to do is say this is some sort of result.

203
00:14:20,700 --> 00:14:24,450
So you'll say hey whatever variable you want is equal to input.

204
00:14:24,690 --> 00:14:26,340
And this phrase you can say whatever you want.

205
00:14:26,340 --> 00:14:29,290
So we could say what is your name.

206
00:14:29,460 --> 00:14:31,750
We run this and then it says What's your name.

207
00:14:31,750 --> 00:14:32,900
I'll type in Jose.

208
00:14:33,030 --> 00:14:37,960
And then later on your code you can do is refer to result when you get back.

209
00:14:38,160 --> 00:14:39,870
Jose as a string.

210
00:14:39,870 --> 00:14:41,580
So that's the basics of input.

211
00:14:41,610 --> 00:14:46,620
The thing you got to watch out for is that input always accepts anything that's passed into it as a

212
00:14:46,620 --> 00:14:47,550
string.

213
00:14:47,550 --> 00:14:49,590
So let's go back to that number example.

214
00:14:50,040 --> 00:14:53,110
So we'll ask the user Hey what's your favorite number.

215
00:14:55,460 --> 00:15:01,450
When we run that if I pass and 30 and I hit enter there and then I ask for the result.

216
00:15:01,490 --> 00:15:04,460
It's actually the string version 130.

217
00:15:04,550 --> 00:15:08,710
So if I say type of results here it says hey it's a string.

218
00:15:08,720 --> 00:15:15,680
So in order to actually cast this or really transform it into an other data type what I need to do is

219
00:15:15,680 --> 00:15:23,090
say either floats of results to get back 30 point zero or if I still wanted to be an integer I can say

220
00:15:23,090 --> 00:15:29,150
integer result and it returns back 30 and now it's an actual number instead of a string because you

221
00:15:29,150 --> 00:15:30,050
just say results.

222
00:15:30,200 --> 00:15:32,240
Those here still has those quotes.

223
00:15:32,240 --> 00:15:37,250
So keep that in mind IMPA always accepts things as a string because later on you may want to be using

224
00:15:37,250 --> 00:15:39,510
input for grabbing an index position.

225
00:15:39,650 --> 00:15:45,440
So you will most likely have to cast it and you can do this all in one line by saying wrap this in I.A.

226
00:15:46,580 --> 00:15:48,110
and then we will get a favorite number here.

227
00:15:48,110 --> 00:15:49,290
So I say 20.

228
00:15:49,860 --> 00:15:54,200
And now when I check the type of my result it's an integer.

229
00:15:54,200 --> 00:15:54,400
OK.

230
00:15:54,410 --> 00:15:57,040
So that's the basics of the input function.

231
00:15:57,380 --> 00:16:02,330
And those are some other useful operators that we'll be using especially later on in your first milestone

232
00:16:02,330 --> 00:16:03,440
project.

233
00:16:03,440 --> 00:16:07,350
You can check out the notebook for more examples and I will see you at the next lecture.

234
00:16:07,520 --> 00:16:08,430
Thanks everyone.
