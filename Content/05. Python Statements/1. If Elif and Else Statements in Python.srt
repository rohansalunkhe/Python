1
00:00:05,630 --> 00:00:07,580
Welcome back everyone in this lecture.

2
00:00:07,580 --> 00:00:08,960
We're going to discuss if.

3
00:00:08,960 --> 00:00:09,500
Elif.

4
00:00:09,500 --> 00:00:09,780
Else.

5
00:00:09,800 --> 00:00:10,730
Statements.

6
00:00:12,300 --> 00:00:17,490
So as I mentioned in this lecture we're going to discuss control flow in general and control flow basically

7
00:00:17,490 --> 00:00:22,000
allows us to use logic to execute code only when we want to.

8
00:00:22,170 --> 00:00:27,330
So often you have a larger piece of code and you only want certain code to be executed when a particular

9
00:00:27,330 --> 00:00:28,930
condition has been met.

10
00:00:28,950 --> 00:00:34,740
For example let's imagine that I'm trying to program a robot to feed my dogs that I could say if my

11
00:00:34,740 --> 00:00:35,890
dog is hungry.

12
00:00:36,000 --> 00:00:40,530
So that's the condition my dog being hungry then I'll have the robot feed the dog.

13
00:00:40,710 --> 00:00:45,860
They'll have the actual code execute or perform some action.

14
00:00:46,080 --> 00:00:50,530
So in order to control this flow of logic we use some keywords and the keywords we're going to be introducing

15
00:00:50,530 --> 00:00:52,060
in this lecture are if.

16
00:00:52,060 --> 00:00:52,600
Elif.

17
00:00:52,630 --> 00:00:53,290
Else.

18
00:00:53,590 --> 00:00:58,400
So let's see the syntax for these three keywords in order to understand the syntax.

19
00:00:58,490 --> 00:01:04,490
We have to understand that control flow syntax in Python makes use of colons and indentation otherwise

20
00:01:04,490 --> 00:01:05,700
known as whitespace.

21
00:01:05,930 --> 00:01:09,400
And this indentation system is absolutely crucial to Python.

22
00:01:09,640 --> 00:01:12,620
And it's really what sets it apart from other programming language.

23
00:01:12,620 --> 00:01:18,920
This use of whitespace and in the notation allows Python code to be easily readable and very quick to

24
00:01:18,920 --> 00:01:21,700
prototype.

25
00:01:21,740 --> 00:01:25,150
So here's the syntax of a basic IF statement we're going to say.

26
00:01:25,190 --> 00:01:30,980
If so that's a keyword some condition so some condition is usually some sort of comparison operation

27
00:01:30,980 --> 00:01:33,320
that we just saw in the previous section of the course.

28
00:01:33,320 --> 00:01:39,920
So that could be something like If hungry is equal to true colon and then notice that that blue line

29
00:01:40,340 --> 00:01:43,110
is indented further than the if statement.

30
00:01:43,340 --> 00:01:48,680
So that says anything along that indentation is going to be executed if that condition happens to be

31
00:01:48,680 --> 00:01:49,960
true.

32
00:01:50,050 --> 00:01:53,490
Now on top of an IF statement we can add an else to this.

33
00:01:53,590 --> 00:01:55,880
So let's say that condition doesn't happen to be true.

34
00:01:55,900 --> 00:01:58,460
We can have another block of code execute.

35
00:01:58,600 --> 00:02:05,170
So in this logic we say if some condition happens to be true we execute some code else meaning that

36
00:02:05,170 --> 00:02:06,690
condition that it has to be true.

37
00:02:06,760 --> 00:02:07,680
We do something else.

38
00:02:07,720 --> 00:02:10,940
And notice how the else doesn't have a condition attached to it.

39
00:02:10,950 --> 00:02:15,710
It only actually executes if the condition is above that it happens to be true.

40
00:02:15,730 --> 00:02:20,280
You should also notice that the if else in the notation lies are lined up with each other.

41
00:02:22,460 --> 00:02:27,610
If you want to check for multiple conditions before that else statement executes you can have an elf

42
00:02:27,620 --> 00:02:33,830
statement or E L I F statement and basically you say if some condition that executes some code Elif

43
00:02:33,830 --> 00:02:38,190
some other condition do something different and you can have as many of these LFA as you want.

44
00:02:38,210 --> 00:02:43,380
And then finally all the way at the end you can have an else statement do something else OK.

45
00:02:43,390 --> 00:02:48,760
Let's explore all these concepts by actually coding out some examples in a Jupiter notebook to begin

46
00:02:48,760 --> 00:02:49,240
all of this.

47
00:02:49,240 --> 00:02:54,520
We're going to start with the simplest example we can do just a single line of an IF statement with

48
00:02:54,520 --> 00:03:01,260
just a boolean condition we're saying if true colon and note what happens when I hit enter.

49
00:03:01,270 --> 00:03:03,940
I have this indentation automatically done for me.

50
00:03:04,070 --> 00:03:09,640
And if using any text editor and you have defined the file as a PI script already you should see this

51
00:03:09,640 --> 00:03:12,910
in the notation occur automatically for you as well.

52
00:03:13,380 --> 00:03:17,600
Then we're going to say Prince it's true.

53
00:03:18,100 --> 00:03:22,610
So then we're going to run this and we see if true print it's true.

54
00:03:22,630 --> 00:03:27,610
So notice we're saying if some condition is true print it's true.

55
00:03:27,640 --> 00:03:30,310
Now typically you won't have just a boolean like this.

56
00:03:30,310 --> 00:03:35,680
Otherwise you'll always be printing that instead what you may have is something like a comparison operation.

57
00:03:35,680 --> 00:03:40,120
So we'll say if three is greater than to print it's true.

58
00:03:40,360 --> 00:03:42,200
We run that and we get back it's true.

59
00:03:42,580 --> 00:03:49,830
And then to make this even more realistic we'll say hungry set a variable there will say Hungry is equal

60
00:03:49,830 --> 00:03:59,780
to true and then I'll say if hungry Prince feed me.

61
00:03:59,950 --> 00:04:03,390
And if you run that we see that we get feed me.

62
00:04:03,580 --> 00:04:06,260
We can also then get hungry to false.

63
00:04:06,280 --> 00:04:10,930
Now if I run this code again notice I don't get back anything else.

64
00:04:11,080 --> 00:04:16,930
So I have some condition and it happened to be false meaning this block of code that execute what I

65
00:04:16,930 --> 00:04:20,080
could do is add in an else statement to execute.

66
00:04:20,080 --> 00:04:22,120
If this condition doesn't happen to be true.

67
00:04:22,300 --> 00:04:28,550
So he hit enter again and then we hit backspace in order to lineup up or else block with the if statement.

68
00:04:28,570 --> 00:04:34,120
So if we want this else to be lined up with this if they need to be at the same indentation in our code

69
00:04:34,550 --> 00:04:38,890
in a lot of times when you're working if any text editor will kind of automatically line things up for

70
00:04:38,890 --> 00:04:39,070
you.

71
00:04:39,070 --> 00:04:40,310
So keep that in mind.

72
00:04:40,750 --> 00:04:43,340
So else does it have any other conditions attached to it.

73
00:04:43,360 --> 00:04:44,830
Because we're only going to execute.

74
00:04:44,830 --> 00:04:48,050
Else if none of the conditions above happened to be true.

75
00:04:48,550 --> 00:04:53,150
So right now we're saying if you're hungry print's feed me otherwise.

76
00:04:53,190 --> 00:04:56,170
Prince I'm not hungry.

77
00:04:57,370 --> 00:05:00,440
I'll run this right now because Hungary was equal to false.

78
00:05:00,580 --> 00:05:01,300
We're getting back.

79
00:05:01,300 --> 00:05:02,650
I'm not hungry.

80
00:05:02,650 --> 00:05:06,780
If we change hungry to be true we get back.

81
00:05:06,790 --> 00:05:07,820
Feed me.

82
00:05:07,930 --> 00:05:13,160
What's also important to notice is that I'm just passing right here hungry by itself as a bullion.

83
00:05:13,300 --> 00:05:15,760
I don't actually need to do something like this.

84
00:05:15,850 --> 00:05:20,230
Check that Hungary is equal to true because hungry by itself is already a boolean.

85
00:05:20,290 --> 00:05:23,200
And we'll explore that example later on in more detail.

86
00:05:23,590 --> 00:05:28,150
So if hungry print's feed me else Prince I'm not hungry.

87
00:05:28,180 --> 00:05:28,440
OK.

88
00:05:28,450 --> 00:05:32,410
Now let's discuss multiple branches using if Ellefson else.

89
00:05:33,490 --> 00:05:35,470
So let's look at another example.

90
00:05:35,530 --> 00:05:39,570
I'm going to say EHLO see which stands for location.

91
00:05:40,480 --> 00:05:42,680
And I'm going to set that equal to bank.

92
00:05:42,850 --> 00:05:43,820
So have a location.

93
00:05:43,820 --> 00:05:55,080
It's equal to bank and I'm going to say if my location is equal to an auto shop I will for it.

94
00:05:55,120 --> 00:05:56,640
Cars are cool.

95
00:05:59,810 --> 00:06:04,150
Else I'll Prince.

96
00:06:04,270 --> 00:06:08,180
I do not know much.

97
00:06:08,320 --> 00:06:14,890
So when I run this it says I do not know much because the location was bank and we have location equal

98
00:06:14,890 --> 00:06:16,030
to our shop print cards.

99
00:06:16,020 --> 00:06:16,590
Cool.

100
00:06:16,600 --> 00:06:17,650
So that's an excuse.

101
00:06:17,830 --> 00:06:19,070
So then we have else printing.

102
00:06:19,120 --> 00:06:25,060
I don't know much what we can do is check for other conditions using elif.

103
00:06:25,160 --> 00:06:32,880
So let's pass in another condition here we'll say if the location is equal to the bank.

104
00:06:33,090 --> 00:06:36,760
Then Prince money is cool.

105
00:06:37,670 --> 00:06:41,060
As I'm sure everyone at the bank says and then we run this when we get back.

106
00:06:41,060 --> 00:06:42,030
Money is cool.

107
00:06:42,290 --> 00:06:48,680
So here we can stack on as many conditions using an if statement so we can add and more lives for more

108
00:06:48,680 --> 00:06:49,310
conditions.

109
00:06:49,310 --> 00:07:01,790
We can say Alosi is equal to store Colin Prince Welcome to the store we run that and we still get back.

110
00:07:01,790 --> 00:07:02,570
Money is cool.

111
00:07:02,600 --> 00:07:06,730
But as soon as we start changes condition let's change it to auto shop.

112
00:07:06,740 --> 00:07:07,310
We run that.

113
00:07:07,310 --> 00:07:07,760
We get back.

114
00:07:07,760 --> 00:07:08,950
Cars are cool.

115
00:07:09,080 --> 00:07:12,710
If we change it to store we get back.

116
00:07:12,710 --> 00:07:13,840
Welcome to the store.

117
00:07:13,910 --> 00:07:17,870
And if we change it to something else that's not in any of these conditions then we'll have the else

118
00:07:17,870 --> 00:07:18,950
block could x.

119
00:07:19,730 --> 00:07:21,270
So let's see what that looks like.

120
00:07:21,710 --> 00:07:27,150
Let's say we're going to some game and it says I do not know much.

121
00:07:27,380 --> 00:07:31,030
Perfect.

122
00:07:31,040 --> 00:07:31,310
All right.

123
00:07:31,310 --> 00:07:34,410
Just to drive this point home of indentation and white.

124
00:07:34,430 --> 00:07:37,100
We're going to do one last very simple example.

125
00:07:37,310 --> 00:07:39,100
Pretty much exactly the same as the last one.

126
00:07:39,230 --> 00:07:49,280
Let's define a name we'll say Sammy and I will say if this person's name is equal to let's say Frankie

127
00:07:50,550 --> 00:08:02,870
will print out Hello Frankie then we'll say well if we have some other name if elif name is equal to

128
00:08:02,870 --> 00:08:12,230
Sammy print out Hello Sammy and untypically for your L's condition it's going to be something where

129
00:08:12,500 --> 00:08:14,560
none of the other conditions were met.

130
00:08:14,600 --> 00:08:17,260
So good thing to do here would be ask the person what their really.

131
00:08:17,300 --> 00:08:18,900
What is your name.

132
00:08:18,980 --> 00:08:23,130
And later on in another lecture we'll actually learn how to get input from the user.

133
00:08:23,280 --> 00:08:25,550
But for now let's focus on a couple of things here.

134
00:08:25,550 --> 00:08:27,790
Note the indentation and know how.

135
00:08:27,800 --> 00:08:34,610
If Elif and else are all lined up for each other and their respective blocks are all indented and then

136
00:08:34,610 --> 00:08:38,240
we also have this colon at the end of these conditions.

137
00:08:38,240 --> 00:08:45,370
So now when I read this we should expect to see Hello Sammy and if we change this to Franki and run

138
00:08:45,370 --> 00:08:50,930
it again I get back Villefranche if it changes Tony and it's not there because they like it back.

139
00:08:50,950 --> 00:08:52,380
What is your name.

140
00:08:52,420 --> 00:08:53,650
All right that's the basics.

141
00:08:53,670 --> 00:08:54,480
If elephant.

142
00:08:54,480 --> 00:08:58,450
Else they seem pretty simple and hopefully they're pretty straight forward to you.

143
00:08:58,450 --> 00:09:03,790
Later on we're going to use them to create really nice large pieces of code that can execute more complex

144
00:09:03,790 --> 00:09:04,670
tasks.

145
00:09:04,690 --> 00:09:05,800
We'll see you at the next lecture.
