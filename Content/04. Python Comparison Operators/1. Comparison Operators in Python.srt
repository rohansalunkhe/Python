1
00:00:05,470 --> 00:00:07,290
Welcome back everyone in this lecture.

2
00:00:07,300 --> 00:00:12,190
We're going to formally discuss Python comparison operators and we actually mentioned these at the very

3
00:00:12,190 --> 00:00:14,130
end of your previous assessment test.

4
00:00:14,290 --> 00:00:15,640
Hopefully were pretty straightforward.

5
00:00:15,640 --> 00:00:17,020
But in case a little confusing.

6
00:00:17,050 --> 00:00:21,940
Now we're going to formally dive into them and show you how you can return back boolean values using

7
00:00:21,940 --> 00:00:23,020
comparisons.

8
00:00:23,020 --> 00:00:25,720
Let's open up a Jupiter notebook and get straight to it.

9
00:00:25,720 --> 00:00:31,030
All right so here I am a notebook and before we get started I want to briefly mention that in the comparison

10
00:00:31,030 --> 00:00:35,770
operators notebook that goes along with this lecture it's underneath the comparison operators section

11
00:00:36,330 --> 00:00:40,930
there is actually a table of comparison operators same table from the previous assessment test that

12
00:00:41,170 --> 00:00:44,670
has the operator the description and then an example of it.

13
00:00:44,680 --> 00:00:46,630
So let's get started.

14
00:00:46,630 --> 00:00:49,120
Basically going to walk through all of them should be pretty straightforward.

15
00:00:49,150 --> 00:00:50,610
Little quick lecture here.

16
00:00:50,800 --> 00:00:53,050
Often you're going to want to check for equality.

17
00:00:53,050 --> 00:00:56,000
So to do that it's just double equal signs.

18
00:00:56,020 --> 00:00:59,700
So two equals two run that then that's true.

19
00:00:59,750 --> 00:01:04,630
If we check it out on something else for example is two equal to 1 we get back false.

20
00:01:04,640 --> 00:01:08,440
And the reason we have two equal signs is because we don't want to use a single equal sign.

21
00:01:08,450 --> 00:01:12,860
Otherwise Python is going to think you're trying to assign a variable there and you can use this not

22
00:01:12,860 --> 00:01:15,190
just for numbers but basically any objects.

23
00:01:15,260 --> 00:01:24,100
So I can check is the string Hello equal to by one that says false.

24
00:01:24,110 --> 00:01:28,920
And notice when you compare and seeing when you're comparing strings capitalization counts.

25
00:01:28,920 --> 00:01:35,900
So you can't do something like this and that returns false because one is lowercase b and one's uppercase

26
00:01:35,900 --> 00:01:36,650
B.

27
00:01:36,760 --> 00:01:39,670
You should also be aware that Python is going to consider types.

28
00:01:39,670 --> 00:01:45,880
So for example the string two is not going to be equal to the number two are the integer 2 for floating

29
00:01:45,880 --> 00:01:48,220
points as long as they hold the same value.

30
00:01:48,340 --> 00:01:49,040
They will be true.

31
00:01:49,060 --> 00:01:52,110
So 2.0 is going to be the same as two.

32
00:01:52,450 --> 00:01:55,090
So that's equality for inequality.

33
00:01:55,240 --> 00:01:57,820
You can use the exclamation point equal sign.

34
00:01:57,910 --> 00:02:02,380
So if you're going say is three not equal to three.

35
00:02:02,800 --> 00:02:07,750
Well that's false because three is equal to three unless shown an example of where it's going to return

36
00:02:07,750 --> 00:02:08,070
true.

37
00:02:08,080 --> 00:02:12,230
So as for not equal to five that returns true.

38
00:02:12,250 --> 00:02:17,040
So we have equality we have non equality or not equal or greater then.

39
00:02:17,050 --> 00:02:22,360
So I can say is greater than 1 that's true is one greater than two.

40
00:02:22,630 --> 00:02:23,780
That's false.

41
00:02:23,890 --> 00:02:24,970
Then we have less then.

42
00:02:24,970 --> 00:02:28,450
So basically the reverse is to or is one less than two.

43
00:02:28,510 --> 00:02:31,560
That's true is to less than five.

44
00:02:31,570 --> 00:02:32,890
That's also true.

45
00:02:33,280 --> 00:02:37,210
And then we have greater than or equal to and less than or equal to.

46
00:02:37,250 --> 00:02:44,030
So I can say is to greater than or equal to the number two in this case is true because we're equal

47
00:02:44,030 --> 00:02:46,640
to each other and then we can do a similar thing.

48
00:02:46,670 --> 00:02:51,710
We can say is for Lessner equal to for example 1 and that's false.

49
00:02:51,710 --> 00:02:55,540
And you can see a bunch of other comparisons right here in this notebook.

50
00:02:55,550 --> 00:03:01,490
Pretty straightforward stuff a lot of times we're really going to be using is the equality but coming

51
00:03:01,490 --> 00:03:05,660
up next we want to discuss is if you want to make more than one comparison at a time.

52
00:03:05,660 --> 00:03:11,460
So so far we've just been comparing two numbers or two objects so two into one and 0.

53
00:03:11,570 --> 00:03:15,220
But often you're going to want to do multiple comparisons on the same line.

54
00:03:15,350 --> 00:03:21,050
And to do that we're going to use something called logical operators which will allow us to chain together

55
00:03:21,050 --> 00:03:22,430
comparison operators.

56
00:03:22,550 --> 00:03:25,250
So we'll discuss that in the very next lecture or see either.
