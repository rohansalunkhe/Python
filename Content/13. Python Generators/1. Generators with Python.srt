1
00:00:05,600 --> 00:00:07,490
Welcome back everyone in this lecture.

2
00:00:07,490 --> 00:00:10,530
We're going to be discussing generator's.

3
00:00:10,600 --> 00:00:17,050
We've already learned how to create functions using the DPF keyword and the return statement generator

4
00:00:17,050 --> 00:00:22,120
functions allow us to write a function that can send back a value and then later resume to pick up where

5
00:00:22,120 --> 00:00:24,330
it left off.

6
00:00:24,340 --> 00:00:27,790
So as I mentioned that type of function is a generator in Python.

7
00:00:27,790 --> 00:00:34,330
The basic idea is that generators allow us to generate a sequence of values over time instead of having

8
00:00:34,330 --> 00:00:37,570
to create an entire sequence and hold it in memory.

9
00:00:37,570 --> 00:00:44,540
The main difference in syntax when you're programming is the use of a yield keyword statement.

10
00:00:44,630 --> 00:00:49,430
So when a generator function is compiled they become an object that supports some sort of iteration

11
00:00:49,430 --> 00:00:50,540
protocol.

12
00:00:50,540 --> 00:00:56,230
That means when they're actually called in your code they don't return a value and then exit instead.

13
00:00:56,240 --> 00:01:00,110
Generator functions will automatically suspend and resume their executions.

14
00:01:00,120 --> 00:01:03,710
They are around the last point of value generation.

15
00:01:03,710 --> 00:01:08,260
The advantage here is that instead of having to compute an entire series of values upfront and hold

16
00:01:08,260 --> 00:01:13,390
it in memory the generator computes one value and waits until the next value is called for.

17
00:01:13,430 --> 00:01:17,740
So you can imagine if you wanted to get all the numbers between 1 and 1 million.

18
00:01:17,990 --> 00:01:19,240
You have two options here.

19
00:01:19,370 --> 00:01:24,930
You can either start generating values 1 2 3 4 and then feed them that way.

20
00:01:24,950 --> 00:01:26,840
For example in a for loop.

21
00:01:27,010 --> 00:01:32,360
Or you would create a giant list of numbers 1 through a million and then slowly pick off those numbers

22
00:01:32,420 --> 00:01:33,200
from memory.

23
00:01:35,170 --> 00:01:37,540
So this is what the range function is actually doing.

24
00:01:37,540 --> 00:01:39,770
The first way it's generating numbers.

25
00:01:39,820 --> 00:01:43,430
So instead of producing that giant list from 1 to 1 million in memory.

26
00:01:43,450 --> 00:01:47,890
Since that's kind of wasteful if you're using an a for loop instead what it does is it just keeps track

27
00:01:47,890 --> 00:01:53,440
of the last number you asked for the step size which by default is one to provide a steady flow of numbers

28
00:01:53,530 --> 00:01:59,170
so generating the numbers over time instead of creating a giant list in memory.

29
00:01:59,170 --> 00:02:03,580
Now if the user didn't need that list would they have to do is transform the generator to a list with

30
00:02:03,580 --> 00:02:08,230
list range from 0 to 10 or zero to a million or whatever their start and stop values are.

31
00:02:08,530 --> 00:02:10,460
So that's why when we're working a range.

32
00:02:10,480 --> 00:02:15,220
If we actually want to take you the list of numbers we have to cast to a list because range itself is

33
00:02:15,220 --> 00:02:18,520
a generator and it's just remembering the last number it sent out.

34
00:02:18,640 --> 00:02:21,220
And then the step size to generate the new number.

35
00:02:21,220 --> 00:02:25,770
That way it doesn't have to store this huge list in memory and it makes a lot more efficient.

36
00:02:26,110 --> 00:02:29,010
So let's explore now how to create our own generators.

37
00:02:29,020 --> 00:02:33,370
We're going to open up a Jupiter notebook in order to get that better understanding of generators.

38
00:02:33,370 --> 00:02:34,880
We're going to create some.

39
00:02:35,050 --> 00:02:42,900
First I would create a normal function and this function is going to be called create cubes.

40
00:02:43,120 --> 00:02:50,140
And what this function does is it's going to create a list of cubes from 0 up to and whatever number

41
00:02:50,140 --> 00:02:52,000
the user asked for.

42
00:02:52,000 --> 00:03:04,210
So in this case we'll say result is equal to a list and then we will say for x in range n take results

43
00:03:04,930 --> 00:03:08,360
and the pen X to the power of three.

44
00:03:08,380 --> 00:03:11,740
It's cube and then at the end we're going to return.

45
00:03:11,740 --> 00:03:13,680
Result.

46
00:03:13,700 --> 00:03:21,210
So if I want to create cubes up to 10 I run this and I get back this list.

47
00:03:21,260 --> 00:03:24,930
So notice what's happening when we actually work with a normal function.

48
00:03:25,130 --> 00:03:29,740
We have to create an empty list and then we go for every number from zero.

49
00:03:29,750 --> 00:03:34,210
Up to that value we append the cubed value to this result.

50
00:03:34,250 --> 00:03:37,950
So we're keeping this entire list in memory.

51
00:03:37,950 --> 00:03:43,670
Now that may be useful if you actually want the list but sometimes maybe you just want to say for x

52
00:03:43,760 --> 00:03:48,940
in create cubes print x.

53
00:03:49,150 --> 00:03:51,070
Now notice what happens when we print them.

54
00:03:51,070 --> 00:03:54,820
We actually really only needed one value at a time to print them.

55
00:03:54,820 --> 00:03:57,810
We didn't need the whole list stored in memory.

56
00:03:57,820 --> 00:04:03,160
In fact we just need the previous value and then whatever the formula is to get to the next value in

57
00:04:03,160 --> 00:04:06,140
order to generate all these cubes.

58
00:04:06,190 --> 00:04:11,350
So instead of actually creating this giant list in memory it would be better if we just yielded the

59
00:04:11,350 --> 00:04:12,920
actual cube numbers.

60
00:04:13,000 --> 00:04:14,410
So that's what we're going to do now.

61
00:04:15,810 --> 00:04:17,730
So let's go back to this create cubes function.

62
00:04:17,730 --> 00:04:19,420
I'll zoom in here.

63
00:04:19,470 --> 00:04:22,980
I don't need the list anymore because I'm not going to store any of this in memory.

64
00:04:22,980 --> 00:04:28,410
Instead well do a save for x in range and instead of returning the result.

65
00:04:28,410 --> 00:04:36,030
At the very end I'm just going to call yield and this is a keyword lips y.

66
00:04:36,180 --> 00:04:38,130
I e l d.

67
00:04:38,230 --> 00:04:43,350
I'm going to yield that cube x value and then if I run create cubes.

68
00:04:43,350 --> 00:04:47,630
Now I get back the same results it says for X and create cubes.

69
00:04:47,700 --> 00:04:53,910
Print x and then getting back the exact same thing except now create cubes is way more memory efficient.

70
00:04:53,910 --> 00:04:59,610
The first way if I had passed then a really big number here would have had to create the entire list

71
00:04:59,610 --> 00:05:04,800
in memory of the cube numbers from 0 to 10000 and then from there if we wanted to iterate through it

72
00:05:04,890 --> 00:05:07,370
we would have had that list in memory.

73
00:05:07,500 --> 00:05:12,610
But now I don't have this list in memory instead of just yielding the values as they come.

74
00:05:12,780 --> 00:05:14,040
So then create cubes.

75
00:05:14,040 --> 00:05:18,510
Here is a generator generating those values as you need them.

76
00:05:18,510 --> 00:05:19,320
Now keep in mind.

77
00:05:19,390 --> 00:05:21,160
Let's turn this back to 10.

78
00:05:21,300 --> 00:05:26,290
If I were just to call create cubes by itself I no longer see that list.

79
00:05:26,370 --> 00:05:26,930
I just see.

80
00:05:26,940 --> 00:05:32,130
Hey you have a generator object here at this location in memory and you need to iterate through it.

81
00:05:32,250 --> 00:05:37,830
If you actually want the list of numbers if you do end up just wanting the actual list itself you could

82
00:05:37,830 --> 00:05:41,170
cast it to a list and then get back to the list.

83
00:05:41,520 --> 00:05:47,980
But create cubes itself has the option of just yielding it to be more memory efficient.

84
00:05:48,040 --> 00:05:53,940
So let's now create another example which calculates a Fibonacci sequence and a Fibonacci sequence.

85
00:05:53,940 --> 00:06:01,390
All it is is if you have a number a followed by a number B then B is defined as the sum of the last

86
00:06:01,390 --> 00:06:02,700
two numbers.

87
00:06:02,740 --> 00:06:05,530
So let's actually show you what I mean by that.

88
00:06:05,620 --> 00:06:13,590
We're going to say the F generate fib for generate Fibonacci that's going to generate if you actually

89
00:06:13,590 --> 00:06:17,990
sequence up to an so we can start with equals 1.

90
00:06:18,100 --> 00:06:24,580
Because one other Fibonacci sequences start with equal zero and because 1 it's really up to you and

91
00:06:24,580 --> 00:06:37,010
we'll say for I in range and I'm going to yield a and then in order to actually recalculate and B for

92
00:06:37,010 --> 00:06:39,350
the Fibonacci sequence I can do the following.

93
00:06:39,590 --> 00:06:47,640
I can say do a little tuple matching here a b is going to equal to be a plus b.

94
00:06:47,780 --> 00:06:52,850
And this allows me to avoid any issues with trying to reassign a and b all they're still being played

95
00:06:52,850 --> 00:06:53,740
around with.

96
00:06:54,110 --> 00:06:55,800
So let's walk through what's happening here.

97
00:06:56,000 --> 00:07:02,780
The generation of the Fibonacci sequence of equal 1 equal 1 for Iron Range and we yield a and then we're

98
00:07:02,780 --> 00:07:10,760
going to reset a to be equal to B and then B is going to beat to the sum of that previous A-plus B.

99
00:07:10,760 --> 00:07:19,310
So in this case the first round we have 1 1 and then we have 1 2 and then from there we have 2 3 then

100
00:07:19,310 --> 00:07:30,460
3 5 5 8 13:21 we'll be able to see this when we run this we'll say for number in generate the notchy

101
00:07:30,540 --> 00:07:39,870
up to 10 percent the number and we were in that we get to see this Fibonacci Sequence occur.

102
00:07:39,880 --> 00:07:44,290
So what if this was a normal function just to kind of show you the difference between yielding something

103
00:07:44,680 --> 00:07:46,240
and creating a normal function.

104
00:07:46,330 --> 00:07:49,100
In that case we would have to store everything.

105
00:07:49,100 --> 00:07:52,140
So we would have the same output as some empty list.

106
00:07:52,300 --> 00:07:59,110
And instead of yielding everything we'd have to say output a pand A.

107
00:07:59,430 --> 00:08:03,630
And then after we did that entire four loop I would end up returning the output.

108
00:08:03,870 --> 00:08:06,610
And if I run this it looks the same as the results.

109
00:08:06,720 --> 00:08:12,210
But this is way less memory fashion because I'm holding everything in the list in memory instead of

110
00:08:12,210 --> 00:08:14,630
just yielding them as I need them.

111
00:08:14,640 --> 00:08:19,980
So when you want to iterate through some sort of generation sequence it becomes a lot better to use

112
00:08:19,980 --> 00:08:23,520
this yield keyword instead of storing thing in a list.

113
00:08:23,520 --> 00:08:29,340
Especially if you expect that number to be really large and if you're creating a huge list.

114
00:08:29,490 --> 00:08:35,520
So the key to fully understanding generators is the next function and the entire function.

115
00:08:35,520 --> 00:08:37,430
So let's explore those concepts now.

116
00:08:38,250 --> 00:08:40,360
Going to create a couple of new empty cells here.

117
00:08:40,770 --> 00:08:42,680
Well first start off with the next function.

118
00:08:42,990 --> 00:08:52,170
So to do that I'm going to create a very simple generator and all this generator does is it says for

119
00:08:52,860 --> 00:08:54,500
x in range 3.

120
00:08:54,600 --> 00:08:56,570
So I've already predefine the range.

121
00:08:56,580 --> 00:08:58,330
We won't have these or press that in.

122
00:08:58,620 --> 00:09:03,340
I will yield X so what does that actually mean.

123
00:09:03,340 --> 00:09:12,380
It means if I say for x in simple gen France actually let's say numbers so we don't get confused by

124
00:09:12,380 --> 00:09:18,850
the x in there we say for a number in simple gen print number I get back 0 1 2.

125
00:09:18,870 --> 00:09:27,020
So let's now assign G equal to a new instance of simple gen.

126
00:09:27,450 --> 00:09:30,520
So here I'm actually calling simple gen here.

127
00:09:32,260 --> 00:09:34,620
And let's take a look at G.

128
00:09:34,740 --> 00:09:36,410
Is this generator object.

129
00:09:36,640 --> 00:09:42,970
And I can ask for the next G and let's print out the results here.

130
00:09:44,310 --> 00:09:46,420
And I get back zero.

131
00:09:46,480 --> 00:09:53,910
So then if I ask for the next one after that I get back 1 and this is what the generator object is doing

132
00:09:53,910 --> 00:09:56,640
internally when you call that yield keyword.

133
00:09:56,640 --> 00:10:01,490
It's remembering what the previous one was and then returning the next value given later formula.

134
00:10:01,500 --> 00:10:02,280
It's following.

135
00:10:02,370 --> 00:10:09,990
It's not holding everything in memory so that we can print out the Next G when we get back to and then

136
00:10:09,990 --> 00:10:14,520
eventually you're going to see because we only went up to three.

137
00:10:14,580 --> 00:10:20,670
It'll say stop iteration because after yielding all the values next calls a stop iteration error.

138
00:10:20,940 --> 00:10:25,780
And what this error does is it informs us that all the values have been yielded.

139
00:10:25,800 --> 00:10:30,780
Now you may be wondering why we don't get this error while using a for loop nets because a for loop

140
00:10:30,870 --> 00:10:34,220
automatically catches this error and stops calling next.

141
00:10:34,230 --> 00:10:38,940
So this is another interesting thing to think about when you're actually calling this for loop on this

142
00:10:38,940 --> 00:10:39,720
generator.

143
00:10:39,750 --> 00:10:44,340
What the for loop is doing is it saying hey give me the next thing hey give me the next thing hey give

144
00:10:44,340 --> 00:10:45,310
me the next thing.

145
00:10:45,570 --> 00:10:51,560
So that's what this 4 is doing internally on this generator object.

146
00:10:51,580 --> 00:11:02,500
Finally I want to show you the entire function so I T E R and the function basically allows us to automatically

147
00:11:02,500 --> 00:11:06,220
iterate through a normal object that you may not expect.

148
00:11:06,220 --> 00:11:13,000
So I'm actually going to first say s is equal to hello and I already know I can iterate through the

149
00:11:13,000 --> 00:11:20,110
string so I can say for letter and s print the letter and I get back.

150
00:11:20,150 --> 00:11:22,010
H E L L O.

151
00:11:22,510 --> 00:11:28,310
Now unfortunately that doesn't mean the string itself is going to be able to iterate with the next function.

152
00:11:28,310 --> 00:11:34,890
In fact if I say next on s they'll say hey this string object is an iterator.

153
00:11:34,970 --> 00:11:40,190
So what this actually means is that the string object does support iteration because we went through

154
00:11:40,190 --> 00:11:45,620
a for loop on it but we cannot directly iterate over it just like we did if a generator using the next

155
00:11:45,620 --> 00:11:51,570
function in order to do that to basically turn this string into a generator.

156
00:11:52,040 --> 00:11:58,790
So we can iterate over we're going to say let's underscore it her and then call the function on it.

157
00:11:58,790 --> 00:12:07,790
And then now we can call next on the version of it to get back h and then we callback next on the next

158
00:12:07,790 --> 00:12:08,820
one to get back.

159
00:12:08,960 --> 00:12:10,190
And so on.

160
00:12:10,190 --> 00:12:14,990
So now you know you know how to convert objects that are iterable into iterators themselves.

161
00:12:15,170 --> 00:12:20,270
And I know there's a lot of kind of strange semantics here that iterators iterable and generators but

162
00:12:20,270 --> 00:12:24,680
the main takeaway from this lecture really has to do with that yield keyword.

163
00:12:24,680 --> 00:12:28,580
So if you scroll all the way back up if you really are going to take one thing from this lecture it's

164
00:12:28,580 --> 00:12:31,940
really how to create your own generators with yield.

165
00:12:32,030 --> 00:12:36,590
Usually when you're working of code you're not going to be actually using that next function that often

166
00:12:36,830 --> 00:12:39,140
you're not going to be using that function that often.

167
00:12:39,260 --> 00:12:42,880
Really those are being called behind the scenes for you already where you will be using.

168
00:12:42,890 --> 00:12:47,050
Is this yield keyword when you eventually find yourself creating your own generators.

169
00:12:47,060 --> 00:12:51,950
You can check out the notebook which has two links at the bottom for really useful Stack Overflow answers

170
00:12:52,280 --> 00:12:55,000
explaining some more aspects of generators.

171
00:12:55,160 --> 00:12:59,120
Coming up next we're going to just quickly check your knowledge to make sure that what we discussed

172
00:12:59,120 --> 00:13:01,310
about the yield keyword stuck with you.

173
00:13:01,310 --> 00:13:04,700
So we'll see you at the next lecture where we go over some generator questions.

174
00:13:04,760 --> 00:13:05,350
We'll see if there.
