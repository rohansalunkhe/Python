1
00:00:05,550 --> 00:00:10,080
Welcome back in this lecture we're going to go over the solutions for the iterators and generators homework

2
00:00:10,710 --> 00:00:15,090
because the solutions themselves didn't require that many lines of code we'll just browse through solutions

3
00:00:15,100 --> 00:00:17,350
notebook and explain the solutions there.

4
00:00:17,610 --> 00:00:19,670
Let's get started OK.

5
00:00:19,690 --> 00:00:22,220
Here I am underneath the python generators folder.

6
00:00:22,210 --> 00:00:25,370
There's the iterators generated homework solution file.

7
00:00:25,780 --> 00:00:27,020
Let's go over problem one.

8
00:00:27,040 --> 00:00:30,900
Whereas you create a generator that generates the squares of numbers up to some end.

9
00:00:31,150 --> 00:00:35,140
We actually did a very similar example of this in the lecture for generators.

10
00:00:35,170 --> 00:00:41,380
All you have to do is say for I in range n and then you want it to yield the squared of I.

11
00:00:41,590 --> 00:00:46,480
And then you could then create a generator and print out those square numbers.

12
00:00:46,510 --> 00:00:52,360
Problem 2 was a very similar problem except we wanted to create a generator for random numbers between

13
00:00:52,360 --> 00:00:54,320
some low and high number.

14
00:00:54,460 --> 00:00:59,380
And in this case what you're doing is instead of yielding the square of the number we do is you take

15
00:00:59,380 --> 00:01:06,050
advantage of this random Rantz integer function and then yield some random dot ran it and then passen

16
00:01:06,070 --> 00:01:11,830
low and high because remember this function itself is going to generate a number and then we're going

17
00:01:11,830 --> 00:01:16,690
to do than yield it in order to create a generator out of this entire function.

18
00:01:17,750 --> 00:01:20,290
And here we have the generating random numbers.

19
00:01:21,840 --> 00:01:25,700
Problem 3 was to use the functions convert the string below into an iterator.

20
00:01:25,740 --> 00:01:29,210
We actually did the exact same thing in the lecture for this.

21
00:01:29,220 --> 00:01:33,720
But all you have to do is say hello and then pass on to the entire function and that's it.

22
00:01:33,720 --> 00:01:41,160
You can then call next on S and it's gone from being an iterable to an iterator problem forced to explain

23
00:01:41,160 --> 00:01:45,090
a use case for a generator using a yield statement where you would not want to use the normal function

24
00:01:45,480 --> 00:01:49,860
and basically is the thing I've been saying all along during the lecture is that the output has a potential

25
00:01:49,860 --> 00:01:53,370
taking up a large amount of memory and you only intend to iterate through it.

26
00:01:53,370 --> 00:01:58,390
You don't want the entire thing at once then that makes sense to use a generator and then the extra

27
00:01:58,410 --> 00:02:01,740
credit was can you explain what a comp is bhalo.

28
00:02:02,020 --> 00:02:07,300
So we actually didn't cover this in the lecture but if you google generator comprehension this is exactly

29
00:02:07,300 --> 00:02:07,870
what this is.

30
00:02:07,900 --> 00:02:12,820
It's like a list comprehension but instead of actually creating the list in memory it's going to generate

31
00:02:12,820 --> 00:02:13,080
it.

32
00:02:13,090 --> 00:02:19,630
So all you need to do here is switch out the square brackets for princes and you turn your list comprehension

33
00:02:19,750 --> 00:02:24,720
into a generator so that you can iterate the generator and not hold all of this in memory.

34
00:02:25,200 --> 00:02:25,590
OK.

35
00:02:25,630 --> 00:02:29,440
Hope those useful to you have any questions feel free to post the county forms.

36
00:02:29,440 --> 00:02:30,460
We'll see you at the next lecture.
