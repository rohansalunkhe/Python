1
00:00:05,630 --> 00:00:10,880
Now that you know about the yield keyword and how generators work in general what we want to do is test

2
00:00:10,880 --> 00:00:12,860
your knowledge with some homework questions.

3
00:00:12,890 --> 00:00:14,030
Let's jump to the notebook.

4
00:00:14,030 --> 00:00:17,600
Go over the questions and in the next lecture we'll go over the solutions.

5
00:00:18,140 --> 00:00:22,640
OK if you go to the Python generators folder you should be able to find the iterators and generators

6
00:00:22,640 --> 00:00:23,610
homework in a book.

7
00:00:23,670 --> 00:00:26,510
There's a solutions notebook that goes along with this.

8
00:00:26,630 --> 00:00:31,050
And the first problem is create a generator that generates the squares of numbers up to some number.

9
00:00:31,070 --> 00:00:37,020
And we did a really similar example of this in the actual lecture and then problem too is to create

10
00:00:37,020 --> 00:00:41,730
that generator that yields N random numbers between a low and high number.

11
00:00:41,750 --> 00:00:43,010
So those are the inputs.

12
00:00:43,010 --> 00:00:47,180
You have a low number and a high number and you want and random numbers.

13
00:00:47,210 --> 00:00:53,430
Remember they're generated using the yield keyword and then problem is to use the function to convert

14
00:00:53,460 --> 00:00:55,470
the string below into an iterator.

15
00:00:55,470 --> 00:01:01,300
We pretty much actually did this exact same thing in the lecture before this and then problem for this

16
00:01:01,300 --> 00:01:03,510
is just something to kind of think about.

17
00:01:03,640 --> 00:01:07,450
And it's mainly to explain the use case for a generator using a yield statement where you would not

18
00:01:07,450 --> 00:01:09,720
want to use a normal function with a return statement.

19
00:01:09,820 --> 00:01:14,900
And this is kind of the same thing O.S. over and over again in regards to memory management.

20
00:01:15,020 --> 00:01:20,300
And then we hear we have a quick extra credit problem where can you see if you can explain what's actually

21
00:01:20,300 --> 00:01:24,900
happening here with this Jenna Komp variable.

22
00:01:24,950 --> 00:01:28,850
So we've never really covered this in the lecture because it's a pretty advanced topic but you might

23
00:01:28,850 --> 00:01:33,440
be able to figure it out if you google generator comprehension and more or less that's actually the

24
00:01:33,440 --> 00:01:34,240
answer itself.

25
00:01:34,460 --> 00:01:36,450
But maybe you can explore this a little more.

26
00:01:36,790 --> 00:01:37,170
OK.

27
00:01:37,250 --> 00:01:39,310
We'll see at the next lecture where we go over the solutions.
