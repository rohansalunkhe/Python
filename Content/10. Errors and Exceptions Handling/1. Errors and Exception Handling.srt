1
00:00:05,280 --> 00:00:11,250
Welcome back in this lecture we're going to be discussing errors and exception handling errors are bound

2
00:00:11,250 --> 00:00:12,680
to happen in your code.

3
00:00:12,720 --> 00:00:17,730
Nobody's perfect and errors are definitely going to arise especially when someone else ends up using

4
00:00:17,730 --> 00:00:20,110
your code in an unexpected way.

5
00:00:20,130 --> 00:00:25,320
We can use error handling in an attempt to plan for possible errors that may arise in our code in the

6
00:00:25,320 --> 00:00:26,280
future.

7
00:00:27,060 --> 00:00:33,330
For example maybe in some part of your code you try to open up the file so later on someone else is

8
00:00:33,330 --> 00:00:38,430
using your code and the user may try to actually write to the file when it was actually only opened

9
00:00:38,490 --> 00:00:44,040
with the mode are for reading currently if there's any sort of error inside our programs the entire

10
00:00:44,040 --> 00:00:47,540
scripts are going to stop and we get back some sort of error statement.

11
00:00:47,610 --> 00:00:52,770
We can actually use error handling to let script continue if the other code and report the error.

12
00:00:52,770 --> 00:00:57,880
So even if there's an error the code will report the error and continue on.

13
00:00:58,060 --> 00:01:02,000
Now there's three key words for this process of exception handling.

14
00:01:02,110 --> 00:01:03,300
And it's the try except.

15
00:01:03,310 --> 00:01:08,400
And finally keywords and these are all going to have a block of code associated with them.

16
00:01:08,420 --> 00:01:13,900
So under the try block of code that's the block of code that's going to be attempted and this may or

17
00:01:13,900 --> 00:01:15,240
may not lead to an error.

18
00:01:15,490 --> 00:01:21,760
So once you try the actual try block of code if there is an error you have the accept block and this

19
00:01:21,760 --> 00:01:26,090
is a block code that will execute in case there's any error inside of that try block.

20
00:01:26,260 --> 00:01:31,210
Then you have a finally block of code which is a final block of code which is always executed whether

21
00:01:31,210 --> 00:01:33,980
or not there is an exception or error occurring.

22
00:01:34,240 --> 00:01:38,110
Let's jump to the notebook and see some examples how to use these blocks.

23
00:01:38,110 --> 00:01:38,610
OK.

24
00:01:38,710 --> 00:01:43,420
Let's start off actually viewing an example of some error that may occur.

25
00:01:43,420 --> 00:01:51,430
Let's imagine that we have a very simple function some add function takes in and one and two and then

26
00:01:51,430 --> 00:01:58,060
what it does it just prints out the sum of them and one plus and two you may be thinking no problem

27
00:01:58,110 --> 00:01:58,420
here.

28
00:01:58,440 --> 00:02:02,990
I'm just going to say add 10 and 20 and you get back 30.

29
00:02:03,150 --> 00:02:05,830
But let's imagine you have the following situation.

30
00:02:06,120 --> 00:02:08,170
You have some variable number one.

31
00:02:08,400 --> 00:02:09,740
You set that equal to 10.

32
00:02:09,960 --> 00:02:16,930
Then you have a bunch of code and later on to get the second number you ask for it as an input and let's

33
00:02:16,950 --> 00:02:20,950
say number two is equal to the input of.

34
00:02:21,000 --> 00:02:21,840
Please.

35
00:02:22,840 --> 00:02:24,730
Provide a number.

36
00:02:25,930 --> 00:02:28,330
So it asks us please provide a number.

37
00:02:28,330 --> 00:02:37,430
We say 20 and then what we end up doing is we say well let me call the function again on number 1 and

38
00:02:37,430 --> 00:02:38,670
number two.

39
00:02:38,960 --> 00:02:44,090
Hopefully you realize that this is already going to give you an error because notice recall that when

40
00:02:44,090 --> 00:02:47,420
you call the input function 20 here is actually a string.

41
00:02:47,510 --> 00:02:50,250
So when I run this it says hey you have a type error.

42
00:02:50,270 --> 00:02:55,040
So notice here that there is an actual specific exception that Python is trying to tell you about type

43
00:02:55,040 --> 00:03:01,280
error and that says unsupported operation type as four plus integer and string.

44
00:03:01,330 --> 00:03:06,620
So basically telling you hey I can't add an integer and a string together I can add two integers together

45
00:03:06,860 --> 00:03:11,330
or I can concatenate two strings together but I can't work with both these data types.

46
00:03:11,480 --> 00:03:16,070
And this is an example of the type of error you may get while running your code.

47
00:03:16,160 --> 00:03:20,090
And unfortunately let's say I wanted to have a print statement.

48
00:03:20,090 --> 00:03:22,660
So let's say something happened.

49
00:03:26,070 --> 00:03:29,590
And I run this this print statement is never actually going to occur.

50
00:03:29,650 --> 00:03:31,140
What's going to happen is I get an error.

51
00:03:31,180 --> 00:03:32,570
And that's the end of my script.

52
00:03:32,590 --> 00:03:35,420
Doesn't matter if I have hundreds of lines pass this block of code.

53
00:03:35,650 --> 00:03:40,090
I will still get this type error and nothing else after it is going to get executed.

54
00:03:40,090 --> 00:03:46,360
The idea behind the try and accept and finally statements is that even if it ever happens we're going

55
00:03:46,360 --> 00:03:49,250
to try to do that block of code.

56
00:03:49,360 --> 00:03:52,840
And even if there's an error we can continue with some more code.

57
00:03:53,460 --> 00:03:55,730
So how will we actually do this.

58
00:03:55,770 --> 00:03:58,360
Well we have to try keyword colon.

59
00:03:58,620 --> 00:04:00,120
And then we have the block of code.

60
00:04:00,120 --> 00:04:02,460
So this is the code that you want to try.

61
00:04:03,030 --> 00:04:10,100
So you want to attempt this code however it may have an error.

62
00:04:10,180 --> 00:04:16,500
So here I'm going to say result is equal to 10 plus 10 whoops ago.

63
00:04:16,520 --> 00:04:18,770
So we know this is not going to actually induce an error.

64
00:04:19,160 --> 00:04:23,700
And then what I'm going to do here is I'm going to have my except block of code.

65
00:04:23,960 --> 00:04:29,990
So my except block of code is what I want to have happen if there is an error and this is really common.

66
00:04:30,050 --> 00:04:34,770
When you're building out larger libraries that other people are going to use you can have except block

67
00:04:34,800 --> 00:04:39,110
so you can print out a more specific message to the user.

68
00:04:39,350 --> 00:04:48,550
So maybe you can say hey it looks like you aren't adding correctly.

69
00:04:48,750 --> 00:04:51,040
So I'm going to run this and this.

70
00:04:51,080 --> 00:04:53,200
We actually don't induce air.

71
00:04:53,280 --> 00:05:00,050
Instead I can call results here with no problem as 20 which makes sense because Templeton has no problems.

72
00:05:00,060 --> 00:05:02,620
Let's now actually create that air.

73
00:05:02,730 --> 00:05:07,240
I'm going to try to add the integer 10 to the string 10 here.

74
00:05:07,260 --> 00:05:11,900
Now when I run this it says hey it looks like you aren't adding correctly.

75
00:05:11,940 --> 00:05:13,360
Now notice what happened here.

76
00:05:13,470 --> 00:05:18,570
The rest of the block of code was still able to execute with the accept statement my whole program wasn't

77
00:05:18,570 --> 00:05:22,010
shut down just because I had some sort of error like before.

78
00:05:22,230 --> 00:05:29,580
Instead I tried this block of code and then I said except and in general here I can say except if there's

79
00:05:29,640 --> 00:05:33,790
any type of error do this code right here.

80
00:05:33,800 --> 00:05:39,620
Now let's imagine that I want a block of code to execute if there is no exception.

81
00:05:39,620 --> 00:05:40,970
I could do something like this.

82
00:05:41,000 --> 00:05:41,470
I can say.

83
00:05:41,480 --> 00:05:53,090
Else Prince results and above that I can say Prince ad went well.

84
00:05:53,190 --> 00:05:57,780
So right now when I run this it just says hey it looks like you aren't coding correctly.

85
00:05:57,900 --> 00:05:59,280
So this is known as a try.

86
00:05:59,310 --> 00:06:00,150
Except.

87
00:06:00,150 --> 00:06:01,430
Else statement.

88
00:06:01,650 --> 00:06:04,650
And what we have here is the block of code we're going to try.

89
00:06:04,650 --> 00:06:05,900
And then we have two options.

90
00:06:05,940 --> 00:06:09,860
If there is an error in that try block we run the except code else.

91
00:06:09,900 --> 00:06:11,910
If there is no error there we run this.

92
00:06:11,910 --> 00:06:12,650
Else code.

93
00:06:12,780 --> 00:06:16,830
So let's fix this and have it not produce an error anymore.

94
00:06:17,130 --> 00:06:18,210
We run this.

95
00:06:18,300 --> 00:06:21,360
We try this block of code since it was successful.

96
00:06:21,390 --> 00:06:23,600
We don't execute this except block of code.

97
00:06:23,610 --> 00:06:28,100
Instead we jump straight to else and we say ad went well and we have 20.

98
00:06:28,160 --> 00:06:28,760
OK.

99
00:06:29,010 --> 00:06:29,790
So let's try.

100
00:06:29,790 --> 00:06:30,270
Except.

101
00:06:30,270 --> 00:06:36,020
Else we can now actually use the finally key word to have a block of code.

102
00:06:36,060 --> 00:06:37,250
Always executing.

103
00:06:37,530 --> 00:06:39,470
Regardless if there's an error.

104
00:06:39,570 --> 00:06:47,050
So the show this I'm going to build off of an example where we actually try to write some files.

105
00:06:47,190 --> 00:06:49,660
So let's explore this concept of try except.

106
00:06:49,680 --> 00:06:56,970
And finally by working on a new example and in this example we're going to try to open up a file so

107
00:06:56,970 --> 00:07:01,100
we'll have some test file and we're going to start by opening it in W mode.

108
00:07:01,310 --> 00:07:06,080
That's right mode which allows us to either open the file and write to it or create the file if it doesn't

109
00:07:06,080 --> 00:07:07,880
already exist.

110
00:07:08,180 --> 00:07:16,840
And then I will say F that right and I'm going to write a test line to this file and then I will say

111
00:07:16,920 --> 00:07:23,330
except and here I can either say except colon and that's going to Except for any errors.

112
00:07:23,470 --> 00:07:30,760
But there's actually many types of errors I can specifically except for for example I can try to only

113
00:07:30,760 --> 00:07:32,540
except for this type error.

114
00:07:32,590 --> 00:07:34,270
So what happens is noticeable.

115
00:07:34,260 --> 00:07:36,400
We had this specific type error.

116
00:07:36,490 --> 00:07:42,010
What I could do is say hey only if there is a type error print out.

117
00:07:42,010 --> 00:07:45,280
There was a type error.

118
00:07:46,770 --> 00:07:52,120
Then I could try another error that's actually associated with opening and writing files.

119
00:07:52,290 --> 00:07:57,660
And if you go to the documentation you can come to errors and exceptions and this explains syntax errors

120
00:07:57,690 --> 00:07:59,290
and exceptions in general.

121
00:07:59,400 --> 00:08:04,560
But you'll notice there's a link here it's a built in exceptions which jumps to Section 5 here of the

122
00:08:04,560 --> 00:08:09,360
documentation and this is actually a list of all the different types of exceptions and errors that may

123
00:08:09,360 --> 00:08:10,250
happen.

124
00:08:10,260 --> 00:08:16,470
So if you keep calling down there is an assertion error there's and the file error import errors index

125
00:08:16,530 --> 00:08:16,950
errors.

126
00:08:16,950 --> 00:08:22,320
Some of these you may have seen before a lot of what we haven't seen there is OS errors recursion errors

127
00:08:22,350 --> 00:08:24,470
overflows all different kinds of errors.

128
00:08:24,550 --> 00:08:26,940
You can specifically try to catch.

129
00:08:26,940 --> 00:08:32,160
So in our case the error that's going to occur when you're opening and writing to files you don't have

130
00:08:32,160 --> 00:08:34,660
permissions to is an OS.

131
00:08:34,660 --> 00:08:35,080
Err.

132
00:08:38,540 --> 00:08:49,740
And we're going to print pay you have an OS air you can also say I own air depending on what version

133
00:08:49,740 --> 00:08:58,380
of python you have and then we're going to add in a finally block and this finally block is always going

134
00:08:58,380 --> 00:09:00,190
to execute no matter what.

135
00:09:00,270 --> 00:09:07,610
So say I always run going to run this right now and right now we just get back.

136
00:09:07,640 --> 00:09:08,860
I always run.

137
00:09:09,110 --> 00:09:10,560
So what's actually happening here.

138
00:09:10,700 --> 00:09:16,790
Well we open up the test file we call right on it so that all works meaning I don't have this except

139
00:09:16,790 --> 00:09:19,230
block running and I don't have this except block running.

140
00:09:19,250 --> 00:09:20,990
So then I just say finally I print.

141
00:09:20,990 --> 00:09:22,570
I always run.

142
00:09:22,700 --> 00:09:28,240
Now it's actually induce an error by opening it with our So that's read only permission.

143
00:09:28,340 --> 00:09:32,600
So we should get an error on this line when we try to write to the file.

144
00:09:32,660 --> 00:09:35,960
Now we shouldn't get a type error because that's not actually the error we're getting.

145
00:09:35,960 --> 00:09:40,940
Instead we should get an OS error because that's the error that's related to trying to write to a file

146
00:09:41,310 --> 00:09:43,130
that you only have read permissions for.

147
00:09:43,370 --> 00:09:45,190
So if I run this I get back.

148
00:09:45,200 --> 00:09:47,120
Hey you have an OS error.

149
00:09:47,360 --> 00:09:53,110
And unlike last time where we had this except else we're only one of these blocks is going to run with

150
00:09:53,120 --> 00:09:58,130
the finally statement the except block will run and then the finally block will run.

151
00:09:58,130 --> 00:10:03,050
And this is how you can line up a bunch of excepts to have specific things print's for specific errors

152
00:10:03,330 --> 00:10:08,630
where you could do is let's say that you have type error here and you didn't want to memorize all the

153
00:10:08,630 --> 00:10:12,220
other types if you just say except at the end of this.

154
00:10:12,320 --> 00:10:14,510
This is going to be all other exceptions.

155
00:10:14,540 --> 00:10:19,460
So all other exceptions and every run again.

156
00:10:19,500 --> 00:10:23,820
And it says all other exceptions because it didn't detect the type error or any other errors that we

157
00:10:23,830 --> 00:10:24,570
defined.

158
00:10:24,570 --> 00:10:28,000
So if you just say except colon you'll print out that error.

159
00:10:28,480 --> 00:10:29,290
OK.

160
00:10:29,310 --> 00:10:33,540
So keep in mind as you're beginning in Python you probably won't memorize all the error types and most

161
00:10:33,540 --> 00:10:36,940
likely your blocks of code are just going to look like this.

162
00:10:36,960 --> 00:10:37,580
Try.

163
00:10:37,680 --> 00:10:38,600
Except.

164
00:10:38,610 --> 00:10:39,930
And finally.

165
00:10:39,930 --> 00:10:45,270
So just to reiterate the try block of code is the block of code you're going to try to attempt except

166
00:10:45,360 --> 00:10:50,410
block of code is going to occur when there is an error in your try statement.

167
00:10:50,700 --> 00:10:55,610
And then you have the option of putting a finally block here that will always run whether or not there

168
00:10:55,610 --> 00:11:01,340
is an error or you can have an ELSE block code that's only going to X-2 when there's not an error.

169
00:11:02,160 --> 00:11:05,420
OK so let's focus on this one more time here.

170
00:11:05,450 --> 00:11:11,730
Right now there is no there is going to be no error because they will say they'll be there.

171
00:11:12,410 --> 00:11:13,780
If I run this I get back.

172
00:11:13,790 --> 00:11:14,630
I always run.

173
00:11:14,630 --> 00:11:15,530
So there's no error.

174
00:11:15,530 --> 00:11:18,050
Finally block still runs.

175
00:11:18,050 --> 00:11:19,310
We induce an error.

176
00:11:19,670 --> 00:11:23,960
We say all other exceptions because I'm just saying and generally except here it does whatever.

177
00:11:23,960 --> 00:11:27,100
This block of code says finally it does this block of code.

178
00:11:27,140 --> 00:11:32,680
So this finally block regardless if there's an error it's always going to run.

179
00:11:32,710 --> 00:11:39,130
So let's finish off our discussion here by showing you how to use the tricks that finally blocks inside

180
00:11:39,130 --> 00:11:43,990
of a function that tries to get a specific type of input from a user.

181
00:11:43,990 --> 00:11:46,330
So we're going to do here is the following.

182
00:11:47,570 --> 00:11:53,380
We'll say DPF ask for I a..

183
00:11:53,420 --> 00:11:57,970
So what this function does is it's going to attempt to ask the user for an integer.

184
00:11:58,550 --> 00:12:00,620
So I'll say the following.

185
00:12:00,620 --> 00:12:08,570
Try to result is equal to integer of the input please.

186
00:12:09,730 --> 00:12:20,540
Provide number and then if we get an error here and going to say except and I'm going to print out whoops

187
00:12:21,170 --> 00:12:23,730
that is not a number.

188
00:12:25,050 --> 00:12:34,790
And then I'll say finally print and of tri except finally.

189
00:12:35,300 --> 00:12:39,960
So I run this and we're going to iterate on this function just a little bit but I'm going to call this

190
00:12:39,960 --> 00:12:41,010
function right now.

191
00:12:41,400 --> 00:12:42,630
Do shift enter.

192
00:12:42,690 --> 00:12:44,630
It asks me to please provide a number.

193
00:12:44,880 --> 00:12:51,000
Let's give it one 20 and I just get and the try except finally it makes sense that whoops that is not

194
00:12:51,000 --> 00:12:57,170
a number that imprint because we were successfully able to cast or transform 20 into an integer.

195
00:12:57,510 --> 00:13:00,050
But what happens if I run the cell again.

196
00:13:00,380 --> 00:13:03,150
And now I provide a word.

197
00:13:03,210 --> 00:13:07,350
Now I know I definitely can't transform the strings word into an integer.

198
00:13:07,350 --> 00:13:10,700
So when I run this it says whoops that is not a number.

199
00:13:10,770 --> 00:13:11,840
Now we get an end of try.

200
00:13:11,840 --> 00:13:13,550
Except finally.

201
00:13:13,770 --> 00:13:19,740
So let's see how we can now use a while loop to continually be doing this try block of code.

202
00:13:19,950 --> 00:13:23,460
And then except if there's an error there.

203
00:13:23,460 --> 00:13:28,740
OK so let's see if we can adjust this function so that instead of just ending after you provide the

204
00:13:28,740 --> 00:13:33,990
wrong word we're going to put it inside of a while loop that keeps going over and over again until you

205
00:13:33,990 --> 00:13:37,950
actually have a block of code with no exception.

206
00:13:38,010 --> 00:13:43,560
So I'm going to do it while true and you should always be careful in doing while.

207
00:13:43,570 --> 00:13:47,750
True because it basically means you need to use a break statement somewhere to break out of this while

208
00:13:47,750 --> 00:13:48,650
loop.

209
00:13:48,650 --> 00:13:54,220
I'm going to indent all of this highlighted all hit tab here and I'll say wild true.

210
00:13:54,770 --> 00:14:01,520
Get this result as the input then I say except whoops that is not a number.

211
00:14:01,790 --> 00:14:06,700
And then I'm going to say continue to make this a little more readable.

212
00:14:06,710 --> 00:14:14,720
As far as the logic what's happening now let's say there is not a error then I'm going to say.

213
00:14:14,720 --> 00:14:17,720
Else you can actually combine Elswyth finally here.

214
00:14:17,750 --> 00:14:19,980
So we're going to try this block of code.

215
00:14:20,390 --> 00:14:23,230
If there's an error I'm going to say whoops that's not a number.

216
00:14:23,240 --> 00:14:27,200
Continue on else meaning there is no exception.

217
00:14:27,410 --> 00:14:29,490
That means the user did provide an integer.

218
00:14:29,510 --> 00:14:32,240
So I will say yes.

219
00:14:32,540 --> 00:14:33,940
Thank you.

220
00:14:33,940 --> 00:14:35,860
Now I need to break out of this while loop.

221
00:14:35,870 --> 00:14:42,320
So I will say here break which remember the break statement goes up to it's inclosing loop either while

222
00:14:42,320 --> 00:14:44,300
loop for loop and then ends it.

223
00:14:44,300 --> 00:14:46,340
So end this while loop.

224
00:14:46,670 --> 00:14:50,080
Then finally we'll say and a try except finally.

225
00:14:50,390 --> 00:14:56,210
OK so we just put these four blocks of code inside our while true and we're going to break once we say

226
00:14:56,250 --> 00:15:02,220
yes thank you and we'll have a Finally statement that says print and the try except finally and we can

227
00:15:02,220 --> 00:15:08,360
just make it very clear that Prince I will always run at the end.

228
00:15:08,450 --> 00:15:13,670
So regardless there's an error or not this finally block of code is always going to run.

229
00:15:14,120 --> 00:15:16,970
So we're going to run this ask for an integer.

230
00:15:17,300 --> 00:15:20,740
Let's provide a correct integer we'll say 20 hit enter.

231
00:15:20,750 --> 00:15:21,470
It says yes.

232
00:15:21,470 --> 00:15:22,000
Thank you.

233
00:15:22,040 --> 00:15:25,020
And the try except finally I will always run at the end.

234
00:15:25,640 --> 00:15:26,390
Let's provide it.

235
00:15:26,510 --> 00:15:30,550
A string will say q hit enter and it says whoops.

236
00:15:30,620 --> 00:15:32,700
That is not a number and the try.

237
00:15:32,710 --> 00:15:34,580
Finally I will always run at the end.

238
00:15:34,580 --> 00:15:39,410
Except we have while true here and this break hasn't actually executed yet.

239
00:15:40,330 --> 00:15:43,680
So we have another chance to provide a number will say.

240
00:15:43,690 --> 00:15:45,190
Q It says Whoops again.

241
00:15:45,360 --> 00:15:49,750
And you can keep providing it over and over the wrong things and it's going to keep asking you for input

242
00:15:49,840 --> 00:15:54,310
until you finally provide something that being kept that can actually be cast as an integer.

243
00:15:54,310 --> 00:15:55,730
You say 90 and it says yes.

244
00:15:55,750 --> 00:15:56,210
Thank you.

245
00:15:56,230 --> 00:15:59,030
And the try except Finally I will always right at the end.

246
00:15:59,050 --> 00:16:05,230
So what's happening here we're saying while true try this block of code if there is an exception do

247
00:16:05,230 --> 00:16:09,430
something and then continue else meaning there is no exception.

248
00:16:09,430 --> 00:16:11,840
You can print some sort of statement and then break.

249
00:16:11,890 --> 00:16:16,980
And typically you're only going to want to break out of this while true when there is no exception.

250
00:16:16,990 --> 00:16:21,140
Then finally if for some reason you wanted something to continue.

251
00:16:21,310 --> 00:16:26,570
So a good thing here could be like I'm going to ask you again.

252
00:16:27,930 --> 00:16:29,110
New line.

253
00:16:29,180 --> 00:16:32,540
So that's more realistic example of why you would say finally.

254
00:16:32,540 --> 00:16:35,380
So if I run this again they'll say please provide the number.

255
00:16:35,390 --> 00:16:36,790
We give it the wrong answer.

256
00:16:36,830 --> 00:16:38,150
It says that is our number.

257
00:16:38,150 --> 00:16:42,190
I'm going to ask you again please provide a number and then you can provide it the correct number.

258
00:16:42,230 --> 00:16:43,060
And it says yes.

259
00:16:43,070 --> 00:16:43,820
Thank you.

260
00:16:43,820 --> 00:16:46,390
Unfortunately it's going to say I'm going to ask you again.

261
00:16:46,490 --> 00:16:49,160
So that's why a lot of times people don't like to mix up else.

262
00:16:49,190 --> 00:16:53,780
And finally statements but there's the option for you in case you want it where you could do instead

263
00:16:53,820 --> 00:16:58,970
of just get rid of that finally statement and when you're code like this and you'll say please provide

264
00:16:58,970 --> 00:16:59,870
my number.

265
00:16:59,870 --> 00:17:01,880
Q Well that is not a number.

266
00:17:01,930 --> 00:17:06,230
You can keep doing this over and over again until you actually provide the number and says Yes I think

267
00:17:06,230 --> 00:17:11,650
you it's up to you whether you want to mix Elsin finally blocks but you can if you want to.

268
00:17:11,660 --> 00:17:12,020
All right.

269
00:17:12,020 --> 00:17:13,220
That's the basics of try.

270
00:17:13,220 --> 00:17:13,730
Except.

271
00:17:13,730 --> 00:17:14,440
Else.

272
00:17:14,480 --> 00:17:18,380
Coming up next we're going to give you an assignment to test your new skills on this.

273
00:17:18,380 --> 00:17:19,010
We'll see if they're.
