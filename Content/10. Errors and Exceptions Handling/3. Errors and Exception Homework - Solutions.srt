1
00:00:05,250 --> 00:00:10,050
Welcome back everyone and this lecture we're going to go now over the solutions for the errors and exceptions

2
00:00:10,170 --> 00:00:11,540
homework problems.

3
00:00:11,580 --> 00:00:13,810
Let's open up DUPERE notebook and get started.

4
00:00:14,110 --> 00:00:14,390
OK.

5
00:00:14,390 --> 00:00:15,920
Taking a look at the first problem.

6
00:00:15,930 --> 00:00:19,590
It was to handle the exception thrown by the code here below.

7
00:00:19,710 --> 00:00:23,410
So let's copy this and put it in a new cell.

8
00:00:23,550 --> 00:00:29,490
If I just run this by itself we get that type error because we're trying to square a string which doesn't

9
00:00:29,490 --> 00:00:30,470
really make sense.

10
00:00:30,470 --> 00:00:34,970
So what we need to do is put this whole block of code inside a try block.

11
00:00:35,490 --> 00:00:41,010
And this is where the notation is really going to matter because now we need to indent everything in

12
00:00:41,010 --> 00:00:41,950
order for alignment.

13
00:00:42,030 --> 00:00:48,120
So we have this tri block right here and then we can make an accept call and it's up to you whether

14
00:00:48,120 --> 00:00:57,920
you want to say except and catch that specific type error and then print out type error or whatever

15
00:00:57,920 --> 00:00:58,780
you want.

16
00:00:58,820 --> 00:01:04,040
So if I run this type error watch out or if you don't want to catch the specific error just any error

17
00:01:04,460 --> 00:01:07,830
you don't actually need to say that you can just say except and you get the same thing.

18
00:01:07,880 --> 00:01:09,350
In this case it want to be a taper.

19
00:01:09,350 --> 00:01:11,670
You could just say something like general error.

20
00:01:11,900 --> 00:01:12,530
Watch out.

21
00:01:13,380 --> 00:01:18,100
Main idea being that you need to put that piece of code inside that tray block have an accept line up

22
00:01:18,100 --> 00:01:20,430
with it and then say something.

23
00:01:20,950 --> 00:01:22,630
Let's move on to Problem 2.

24
00:01:22,690 --> 00:01:25,000
This was to handle the exception thrown by the code below.

25
00:01:25,030 --> 00:01:33,010
Using trying to set blocks then finally block prints all done going to copy this put it back here.

26
00:01:34,180 --> 00:01:36,310
And if I just run this let's confirm that we get an error.

27
00:01:36,310 --> 00:01:38,210
Looks like we're getting an 0 division error.

28
00:01:38,350 --> 00:01:42,000
So let's put this all inside of a try block.

29
00:01:42,560 --> 00:01:47,280
We will say try an indent all of this in the try block.

30
00:01:47,280 --> 00:01:49,480
You could also just highlight it all then hit tab.

31
00:01:50,010 --> 00:01:55,200
Then I'm going to say except and again I can either say except 0 division air or just except by itself

32
00:01:55,950 --> 00:01:59,000
we're going to print out error.

33
00:01:59,790 --> 00:02:05,800
And then finally we'll print out all done Hoeffel is pretty straightforward.

34
00:02:07,110 --> 00:02:08,070
So we say error.

35
00:02:08,160 --> 00:02:08,880
All done.

36
00:02:09,000 --> 00:02:13,320
And the last one of the most complicated of them was to write a function that asks for an integer and

37
00:02:13,320 --> 00:02:14,610
prints out the square of it.

38
00:02:14,730 --> 00:02:16,430
Use a while loop if a try except.

39
00:02:16,470 --> 00:02:19,850
Else block to account for the incorrect inputs.

40
00:02:19,890 --> 00:02:23,550
So we're going to copy this with something very similar in the lecture.

41
00:02:23,550 --> 00:02:32,070
So what are we going to do here is say while true and it's up to you if you want to say while true or

42
00:02:32,070 --> 00:02:42,720
while some condition so I'm going to say while true try to say and is equal to the integer input of

43
00:02:43,500 --> 00:02:47,100
a number.

44
00:02:47,130 --> 00:02:51,160
Now if we have some sort of exception and we're going to assume that the exception occurs when you try

45
00:02:51,160 --> 00:02:56,010
to transform that into an integer we'll print out.

46
00:02:56,180 --> 00:02:57,740
Please try again.

47
00:02:58,160 --> 00:02:59,660
And then let's print out a new line.

48
00:03:01,120 --> 00:03:08,870
And we'll say continue now else that means there's no exception what we can do is say break optional.

49
00:03:09,010 --> 00:03:13,680
You can finally in there but remember the finally is going to occur every time.

50
00:03:13,680 --> 00:03:17,040
So let's run this and make sure this works.

51
00:03:17,050 --> 00:03:26,590
So we're going to say ask shift enter to run that and turn number let's say we entered 10 and it looks

52
00:03:26,590 --> 00:03:30,840
like we forgot to actually print out the squared which was what was asking us if we come back here and

53
00:03:30,850 --> 00:03:38,660
say think your number squared is so let's do that and this is going to be outside of that while statement.

54
00:03:38,670 --> 00:03:42,590
So once this entire while it is then running we should have and ready to go.

55
00:03:42,780 --> 00:03:53,140
So we're going to print and to the power of two and we can say above that print your number squared

56
00:03:53,650 --> 00:03:55,810
is there we go.

57
00:03:56,200 --> 00:03:57,450
OK so now let's try this again.

58
00:03:57,480 --> 00:03:59,120
We actually get the results.

59
00:03:59,120 --> 00:04:00,230
We'll ask this again.

60
00:04:00,250 --> 00:04:02,280
Enter number we'll say 10.

61
00:04:02,350 --> 00:04:04,020
Enter number squared 100.

62
00:04:04,030 --> 00:04:04,950
So that's working.

63
00:04:05,140 --> 00:04:12,730
Let's not try to do something wrong or say a word and it says Please try again we'll say W.E. Please

64
00:04:12,730 --> 00:04:13,480
try again.

65
00:04:13,500 --> 00:04:14,810
Let's enter to now.

66
00:04:14,980 --> 00:04:16,990
And the number squared is four.

67
00:04:16,990 --> 00:04:17,630
OK.

68
00:04:17,800 --> 00:04:19,270
So that's how you can use a while.

69
00:04:19,270 --> 00:04:23,720
True so that while true is dependent on this break keyword.

70
00:04:23,980 --> 00:04:34,950
Well you could do instead say while something like waiting set waiting to be equal to true.

71
00:04:34,960 --> 00:04:39,730
So this is saying like we're waiting for a correct response you can call this variable whatever you

72
00:04:39,730 --> 00:04:40,860
want.

73
00:04:43,550 --> 00:04:46,210
So I'm waiting for a quick response and then try this.

74
00:04:46,370 --> 00:04:52,360
I'm going to have the exception else if there's no exception then I can set waiting equal to false.

75
00:04:52,370 --> 00:04:55,140
I'm no longer waiting for the correct response.

76
00:04:55,160 --> 00:04:59,290
So that's the same thing except we're not using a break statement so we can try this again.

77
00:04:59,330 --> 00:05:03,990
Make sure that tactic also works let's send through the wrong thing.

78
00:05:04,020 --> 00:05:07,530
Please try again and for the right thing and it works for.

79
00:05:07,650 --> 00:05:08,210
OK.

80
00:05:08,460 --> 00:05:11,390
So that's it for errors and exception handling.

81
00:05:11,640 --> 00:05:14,550
If you have any questions feel free to post the Q&amp;A forms.

82
00:05:14,610 --> 00:05:15,690
I'll see you at the next lecture.
