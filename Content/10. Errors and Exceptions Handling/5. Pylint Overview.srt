1
00:00:05,460 --> 00:00:06,620
Welcome back everyone.

2
00:00:06,630 --> 00:00:10,970
Now that we understand how to handle errors and exceptions in Python the try except.

3
00:00:11,010 --> 00:00:12,600
Finally blocks of code.

4
00:00:12,630 --> 00:00:14,950
Let's move on to discussing unit testing.

5
00:00:16,210 --> 00:00:21,250
As you begin to expand to larger multi-file projects or you begin to work with a team that's bigger

6
00:00:21,250 --> 00:00:24,760
than just yourself it becomes really important to have tests in place.

7
00:00:24,880 --> 00:00:29,710
This way as you or your teammates make changes or update your code you can run your test files to make

8
00:00:29,710 --> 00:00:32,250
sure previous code still runs as expected.

9
00:00:33,490 --> 00:00:36,240
There are several testing tools and we're going to focus on two.

10
00:00:36,240 --> 00:00:40,270
There's other ones mentioned in the notebook that goes along with this lecture but we're going to focus

11
00:00:40,270 --> 00:00:41,170
on pylons.

12
00:00:41,260 --> 00:00:44,980
And this is a library that in general looks at your code and reports back.

13
00:00:44,980 --> 00:00:46,150
Possible issues.

14
00:00:46,150 --> 00:00:47,500
Maybe you have styling issues.

15
00:00:47,500 --> 00:00:52,270
Or maybe you have some invalid code and will report back issues of your code.

16
00:00:52,270 --> 00:00:54,100
Then we have the unit test library.

17
00:00:54,100 --> 00:00:58,780
This is a builtin library in python that's going to allow you to test your own programs and then check

18
00:00:58,780 --> 00:01:01,830
if you're getting the desired outputs.

19
00:01:01,860 --> 00:01:06,080
We're going to begin by showing you how to use pilot to check your code for possible errors and styling.

20
00:01:06,090 --> 00:01:08,040
And if you're wondering what I mean by styling.

21
00:01:08,190 --> 00:01:11,590
Python has a set of style convention rules known as pep A.

22
00:01:11,640 --> 00:01:16,200
I will show you those in the documentation and then afterwards we're going to explore how to test our

23
00:01:16,200 --> 00:01:20,430
code was the builtin unit test library for this lecture.

24
00:01:20,430 --> 00:01:24,780
We're going to be creating up scripts and subline because it's a more realistic example of how you'll

25
00:01:24,780 --> 00:01:27,240
actually be using these programs.

26
00:01:27,240 --> 00:01:29,770
You can still use the associated notebook for code.

27
00:01:29,850 --> 00:01:33,910
It has a bunch of right file magic Jupiter commands to create the thought postscripts.

28
00:01:34,010 --> 00:01:39,890
So if you're really intent on sticking to Jupiter the Juber note book does work for this lecture but

29
00:01:40,130 --> 00:01:44,480
in the actual filming of this we're going to use the text editor because it's a little more realistic.

30
00:01:44,520 --> 00:01:46,150
OK let's get started.

31
00:01:46,530 --> 00:01:50,010
OK the first thing we need to do is install the pilot's library.

32
00:01:50,070 --> 00:01:52,530
So let's go ahead and do that at our command line.

33
00:01:52,530 --> 00:01:57,420
So if you're a command line or your terminal if you are in Mac OS or Linux or you need to do is straight

34
00:01:57,420 --> 00:01:58,330
into command line.

35
00:01:58,350 --> 00:02:00,990
Don't call Python before this is just a pit.

36
00:02:01,510 --> 00:02:04,850
Install silent enter.

37
00:02:04,950 --> 00:02:07,400
So that's why L and T.

38
00:02:07,410 --> 00:02:09,110
Right now I already have it ready to go.

39
00:02:09,150 --> 00:02:11,210
Because it came with my distribution of Anaconda.

40
00:02:11,340 --> 00:02:15,900
That may be the case for you but make sure you run this line just to check the have it already.

41
00:02:15,960 --> 00:02:19,560
Once you have that ready to go let's go back to our text editor.

42
00:02:19,560 --> 00:02:24,210
All right so here I am at the text editor and I'm going to create a very simple dot py file so let me

43
00:02:24,210 --> 00:02:34,340
save file save and I'm going to save this to my desktop and I will save this as a simple one pie just

44
00:02:34,380 --> 00:02:35,880
a very simple file you can save.

45
00:02:35,880 --> 00:02:39,340
Or ever you want as long as you're able to call at the command line.

46
00:02:39,600 --> 00:02:44,130
If you're unfamiliar with how to actually run scripts like this make sure you go back and visit the

47
00:02:44,130 --> 00:02:47,920
lecturer at the beginning of the course called Running Python code.

48
00:02:47,940 --> 00:02:54,300
Ok so I will say AES equal to 1 and notice have syntax highlighting that means it's registered it's

49
00:02:54,300 --> 00:02:55,470
a dot by file.

50
00:02:55,720 --> 00:02:57,310
Then I will say be equal to two.

51
00:02:57,340 --> 00:03:03,060
Notice my lower case is here then I will say print a and then I will say print.

52
00:03:03,110 --> 00:03:05,050
And I'm going to make a mistake here on purpose.

53
00:03:05,060 --> 00:03:09,650
I'm going to say print capital-T even supply of Texas telling me hey I think you mean lower case but

54
00:03:09,860 --> 00:03:11,370
we're going to ignore that for now.

55
00:03:11,420 --> 00:03:15,800
So it's just a very simple script but there's definitely a mistake in it right here.

56
00:03:15,800 --> 00:03:20,570
So as you can imagine with four lines it's pretty easy to catch this mistake but if you have a script

57
00:03:20,570 --> 00:03:25,640
of hundreds of lines it's going to be much harder to do control s or command this to save this and once

58
00:03:25,640 --> 00:03:27,720
you have it saved come to your command line.

59
00:03:29,500 --> 00:03:32,190
And then we're going to do here is say pilot.

60
00:03:32,450 --> 00:03:34,390
So I'm not saying Python.

61
00:03:34,390 --> 00:03:38,450
I will say pylint and then say simple one pie.

62
00:03:38,710 --> 00:03:43,930
Remember they have to be at the same location as you up high script and you can view the command line

63
00:03:43,930 --> 00:03:47,130
lecture to get an idea of how to move around that your command line.

64
00:03:47,410 --> 00:03:50,060
But let's enter here and see what happens.

65
00:03:50,510 --> 00:03:50,870
OK.

66
00:03:50,890 --> 00:03:56,410
So we actually get back a bit of a report here and what pilot does is it issues what is essentially

67
00:03:56,500 --> 00:03:59,620
automated report grading your code.

68
00:03:59,790 --> 00:04:05,130
So please scroll way back up here it says no config file found using default configuration so you can

69
00:04:05,130 --> 00:04:09,400
set more advanced configurations and we have final new line missing.

70
00:04:09,420 --> 00:04:10,710
So we have some issues.

71
00:04:10,710 --> 00:04:12,160
And these are styling issues.

72
00:04:12,210 --> 00:04:15,610
We have an invalid constant name but most importantly that is here.

73
00:04:15,630 --> 00:04:17,910
This is an actual error so has an e here.

74
00:04:18,120 --> 00:04:22,120
And these are the ones you really want to watch out for an undefined variable B.

75
00:04:22,320 --> 00:04:27,130
So later on it gives you statistics as far as what they're using function's method classes.

76
00:04:27,330 --> 00:04:30,990
So for some reason you want to figure out how many classes do I have.

77
00:04:31,110 --> 00:04:32,680
How many functions are having this code.

78
00:04:32,730 --> 00:04:33,870
Are they documented.

79
00:04:33,870 --> 00:04:36,990
Do they have a stylized name that's inappropriate.

80
00:04:36,990 --> 00:04:42,110
For example maybe you accidentally capitalized a function so this would be giving statistics on that

81
00:04:43,200 --> 00:04:48,160
then it gives us raw metrics such as how much code are there how many docstring are there comments.

82
00:04:48,180 --> 00:04:49,440
How much is empty.

83
00:04:49,470 --> 00:04:54,030
So if you're working for other people and you the man hey every other line should have a comment on

84
00:04:54,030 --> 00:04:54,270
it.

85
00:04:54,300 --> 00:04:58,180
You could use this to quickly check now whether or not you need that many comments is up to you.

86
00:04:58,380 --> 00:05:02,670
Hopefully you can see in more of a management position how these sort of reports would be really helpful

87
00:05:02,910 --> 00:05:05,890
either to give to your manager or to receive as a manager.

88
00:05:06,390 --> 00:05:07,520
Do we have duplication.

89
00:05:07,590 --> 00:05:09,080
How many lines have been duplicated.

90
00:05:09,120 --> 00:05:11,540
Right now we have zero because it's such a simple file.

91
00:05:11,550 --> 00:05:13,220
Then we see messages by category.

92
00:05:13,260 --> 00:05:15,150
So if there's errors warnings.

93
00:05:15,150 --> 00:05:17,680
So here we are actually getting an error.

94
00:05:17,910 --> 00:05:22,400
So notice they ran the code and got an error and then we have some more information about messages.

95
00:05:22,560 --> 00:05:26,690
And these are what kind of messages actually rise up when you run that pilot report.

96
00:05:26,740 --> 00:05:31,050
And here we're getting a bunch of messages we have undefined variables there's missing thok strings

97
00:05:31,110 --> 00:05:34,420
because it's just that simple file with a huge mistake in it.

98
00:05:34,440 --> 00:05:37,790
So globally valuation at the very end it's going to re-evaluate your code.

99
00:05:37,790 --> 00:05:42,110
And here we're getting a horrible evaluation which is next to twelve point five out of 10.

100
00:05:42,180 --> 00:05:47,060
So perfect score is 10 minutes and here we're doing absolutely horribly with negative twelve point five.

101
00:05:47,250 --> 00:05:49,910
So pilot it's listing some styling issues.

102
00:05:49,920 --> 00:05:54,030
It would like to see an extra line at the end modules and function definitions should have the script

103
00:05:54,030 --> 00:05:58,650
that doc strings a single characters are also poor choice for variable names so it's kind of complaining

104
00:05:58,650 --> 00:05:59,850
about that too.

105
00:05:59,890 --> 00:06:03,460
And most importantly out of all of this if found the error in the program.

106
00:06:03,540 --> 00:06:08,280
So let's try to clean this all up and see what kind of score we can get.

107
00:06:08,310 --> 00:06:13,700
And I should note here it's pretty difficult to try to aim for a perfect 10 out of 10.

108
00:06:13,890 --> 00:06:17,740
So don't be stressed if you're not getting a tentative 10 on your own code.

109
00:06:17,940 --> 00:06:19,990
Sometimes you get something and a 10.

110
00:06:20,010 --> 00:06:22,580
It has to be very a machine readable and sort of human readable.

111
00:06:22,620 --> 00:06:26,280
And you always want to balance that if someone else views or code they're going to be able to read it

112
00:06:26,280 --> 00:06:27,170
easily.

113
00:06:27,190 --> 00:06:29,280
However we can definitely make some improvements here.

114
00:06:29,340 --> 00:06:30,860
So let's see what we can.

115
00:06:31,250 --> 00:06:32,540
OK so back to the script.

116
00:06:32,550 --> 00:06:34,620
Let's see if we can improve this whole thing.

117
00:06:34,680 --> 00:06:38,730
Often when you're dealing with the high script you're going to want to have a multi-line comment at

118
00:06:38,730 --> 00:06:42,480
the top just so other developers can come in and understand what's going on.

119
00:06:42,730 --> 00:06:49,570
So here which is going to say a very simple script.

120
00:06:50,140 --> 00:06:55,990
So that should help us score some points with pilots and then we're actually going to create a function

121
00:06:55,990 --> 00:06:59,790
that physicians will say my phunk won't take any parameters.

122
00:06:59,860 --> 00:07:05,380
And again we should give our functions a docstring and a zoom out just a little bit here so we can see

123
00:07:05,380 --> 00:07:06,420
the whole picture.

124
00:07:06,700 --> 00:07:16,340
And here we're going to say a simple function and then let's actually create two new variable names

125
00:07:16,350 --> 00:07:17,880
first is equal to 1.

126
00:07:18,030 --> 00:07:19,500
And second is equal to 2.

127
00:07:19,650 --> 00:07:25,950
Because usually you don't want to use just a single letter as a variable name and they'll print out

128
00:07:25,980 --> 00:07:32,910
first and then we're going to do is we're going to print out seconds here.

129
00:07:34,350 --> 00:07:38,770
And at the end of this we're going to actually execute the my function.

130
00:07:39,060 --> 00:07:40,410
So I'll say my funk.

131
00:07:40,530 --> 00:07:42,860
Open close Princie execute at the bottom.

132
00:07:42,960 --> 00:07:46,470
So notice here that the indentation I'm going to say this.

133
00:07:46,530 --> 00:07:50,590
And let's run this again in pilot and see if we able to improve our score.

134
00:07:50,700 --> 00:07:52,210
Again most likely won't get a.

135
00:07:52,260 --> 00:07:57,990
And we should see a big jump from negative 12 all right I'm back here at my desk top.

136
00:07:57,990 --> 00:08:00,320
First let's just make sure that our script actually runs.

137
00:08:00,360 --> 00:08:06,030
We'll call simple or Python simple one that pi should be able to tab out of complete that and we get

138
00:08:06,030 --> 00:08:06,900
back one to two.

139
00:08:06,900 --> 00:08:07,840
Looks like it's running.

140
00:08:07,860 --> 00:08:17,780
Let's now check the score will say pilots a simple one that PI enter and now we get back 0 out of 10.

141
00:08:17,860 --> 00:08:22,540
So it looks like we still have some issues but we're no longer getting negative twelve point five out

142
00:08:22,540 --> 00:08:23,900
of 10.

143
00:08:23,900 --> 00:08:24,390
All right.

144
00:08:24,410 --> 00:08:29,780
Now you may be looking at this and saying that it looks like a lot of our problems are with mixed indentation.

145
00:08:29,810 --> 00:08:34,630
And if you scroll up here you'll realize that it's telling you about all these warnings.

146
00:08:34,640 --> 00:08:39,950
It's found in the notation with tabs instead of spaces due to styling issues especially different text

147
00:08:39,970 --> 00:08:40,410
editors.

148
00:08:40,420 --> 00:08:45,160
So let's say your colleague opens this up and pie term and you're opening up sublime someone else is

149
00:08:45,230 --> 00:08:45,860
opening up.

150
00:08:45,900 --> 00:08:52,100
Adam you typically when you're working for other people want to use spaces instead of tabs.

151
00:08:52,100 --> 00:08:56,910
Now again this is very much just the styling choice the code will still run fine as we just saw they

152
00:08:56,930 --> 00:08:58,700
had no problem issuing that code.

153
00:08:58,710 --> 00:09:05,240
The pilot is going to complain about this issue and you can't check the documentation and say don't

154
00:09:05,240 --> 00:09:07,560
worry about this particular warning.

155
00:09:07,580 --> 00:09:12,320
Usually you do want to have it warn about mixed in the station but you don't want to have it warn about

156
00:09:12,320 --> 00:09:14,430
pure tabs or pure spaces.

157
00:09:14,640 --> 00:09:18,130
So in order to fix this issue you can go back to your script.

158
00:09:18,140 --> 00:09:19,190
Let's do that now.

159
00:09:20,830 --> 00:09:25,540
So here I am at my function and you'll notice that when I was defining my function all I did was hit

160
00:09:25,570 --> 00:09:28,480
enter and then that basically auto tabs for me.

161
00:09:28,480 --> 00:09:32,770
So if I hit delete or backspace right now it will go back one tab.

162
00:09:32,770 --> 00:09:38,440
So to fix that all we can do is make sure everything is defined as spaces.

163
00:09:38,470 --> 00:09:45,050
So delete this right here and then do one two three four spaces and if we do that for every line.

164
00:09:45,100 --> 00:09:47,440
I've actually already done that for you.

165
00:09:47,500 --> 00:09:49,550
I've actually already done that myself on the script.

166
00:09:49,570 --> 00:09:51,790
We've done that for you in the Jupiter note book as well.

167
00:09:51,970 --> 00:09:55,430
But now he should be able to improve on that so I'm going to save this.

168
00:09:55,570 --> 00:10:00,130
And if you ever have a doubt are these tabs or spaces in some blind text editor you should be able to

169
00:10:00,130 --> 00:10:00,870
highlight.

170
00:10:01,150 --> 00:10:08,110
And here is probably won't show up on your screencast but I have I can see four very faint dots indicating

171
00:10:08,140 --> 00:10:09,310
each space.

172
00:10:09,310 --> 00:10:11,680
And if you just have a tab you won't see that.

173
00:10:11,680 --> 00:10:13,350
So you'll have to see that on your own screen.

174
00:10:13,360 --> 00:10:14,730
But again just highlighting this.

175
00:10:14,800 --> 00:10:16,250
You can see the spaces.

176
00:10:16,450 --> 00:10:21,410
And if you have just enter you'll end up seeing a tab here just one straight line.

177
00:10:21,640 --> 00:10:24,790
So pylons will complain when you mix those together.

178
00:10:24,940 --> 00:10:27,280
So let's save this and go back to our command line.

179
00:10:28,400 --> 00:10:31,740
And I'm going to rerun pylons on my symbol on that postscript.

180
00:10:31,820 --> 00:10:34,340
Now that it only has white spaces I'll hit enter.

181
00:10:36,300 --> 00:10:38,250
And it looks like now we're getting much better.

182
00:10:38,250 --> 00:10:42,120
We're getting eight point three three over our previous run.

183
00:10:42,160 --> 00:10:45,270
And you make up six point six seven after fixing that.

184
00:10:45,400 --> 00:10:48,750
I've run this a few times trying to deal that whitespace issue.

185
00:10:48,970 --> 00:10:54,130
And this is most likely probably about as best as we're going to get without having to put in a bunch

186
00:10:54,130 --> 00:10:55,470
of extra comments in code.

187
00:10:56,740 --> 00:10:57,170
OK.

188
00:10:57,190 --> 00:11:02,080
That's really all we need to know about pylons for our use cases and in general we are just coding by

189
00:11:02,080 --> 00:11:02,820
yourself.

190
00:11:02,890 --> 00:11:07,390
You won't be using something like this that often this is really more for when you're working if other

191
00:11:07,390 --> 00:11:12,970
people are working with really large programs and you have kind of strict methods for yourself to make

192
00:11:12,970 --> 00:11:16,270
sure everything is up to some sort of style convention.

193
00:11:16,270 --> 00:11:21,760
Again it's not really useful for just a rating to come back to a single user because you just saw the

194
00:11:21,760 --> 00:11:22,960
entire bit of code.

195
00:11:23,290 --> 00:11:28,360
OK so that's pilots coming up next we're going to learn about unit test which allows you to write your

196
00:11:28,390 --> 00:11:29,650
own test programs.

197
00:11:29,650 --> 00:11:31,940
Notice here that we're basically running a bunch of tests.

198
00:11:32,020 --> 00:11:34,040
Would be nice if we could write our own.

199
00:11:34,240 --> 00:11:35,780
We'll see you at the next lecture.
