1
00:00:05,420 --> 00:00:06,500
Welcome back everyone.

2
00:00:06,620 --> 00:00:09,480
Now that we understand how to work with errors and exception handling.

3
00:00:09,530 --> 00:00:12,780
Let's get some practice of a quick homework set of three problems.

4
00:00:12,950 --> 00:00:15,010
Let's go over in the notebooks so you can attempt to yourself.

5
00:00:15,030 --> 00:00:17,540
And then the very next lecture will go over the solutions.

6
00:00:17,780 --> 00:00:22,760
OK so you can find this notebook under errors and exception handling a second notebook errors and exceptions

7
00:00:22,760 --> 00:00:25,390
homework the very next note book is The solutions.

8
00:00:25,400 --> 00:00:27,240
So there's three problems in here.

9
00:00:27,290 --> 00:00:30,280
The first one is to handle the exception thrown by the code below.

10
00:00:30,290 --> 00:00:37,220
By using try and except blocks so here we're saying for I in this list of strings are trying to square

11
00:00:37,220 --> 00:00:39,570
the actual strings doesn't really make sense.

12
00:00:39,740 --> 00:00:44,110
So try to use a try except block to handle this somehow.

13
00:00:44,150 --> 00:00:48,590
That's a pretty open statement so you can just print something like an error occurred and then maybe

14
00:00:48,590 --> 00:00:50,540
have something else happen.

15
00:00:50,750 --> 00:00:56,330
Basically just don't have this type error pop up problem too is to handle the exception thrown by the

16
00:00:56,330 --> 00:00:57,050
code below.

17
00:00:57,200 --> 00:01:00,990
But using trying except blocks as well and then use a finally block to print.

18
00:01:01,010 --> 00:01:02,090
All done.

19
00:01:02,090 --> 00:01:04,330
So right now we have a zero division error.

20
00:01:04,340 --> 00:01:06,490
So we're trying to say 5 divided by zero.

21
00:01:06,500 --> 00:01:11,730
Go ahead and catch this exception and then have a finally block execute them problem.

22
00:01:11,730 --> 00:01:16,280
Three is kind of similar to what we discuss towards the end of the previous lecture and it's to write

23
00:01:16,280 --> 00:01:21,130
a function that asks for an integer and prints the square of it onto these a while loop with a try.

24
00:01:21,170 --> 00:01:21,650
Except.

25
00:01:21,650 --> 00:01:25,100
Else block to account for incorrect inputs.

26
00:01:25,130 --> 00:01:25,740
OK.

27
00:01:25,940 --> 00:01:26,580
Best of luck.

28
00:01:26,600 --> 00:01:29,150
And we're going to go over the solutions in the very next lecture.
