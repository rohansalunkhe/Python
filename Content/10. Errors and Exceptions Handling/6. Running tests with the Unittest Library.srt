1
00:00:05,170 --> 00:00:06,940
Welcome back everyone in this lecture.

2
00:00:06,940 --> 00:00:11,890
We're actually going to go over the unit test library so unit test allows you to write your own test

3
00:00:11,890 --> 00:00:12,570
program.

4
00:00:12,610 --> 00:00:18,280
And the goal is to set a specific set of data to your program analyze the returned results and then

5
00:00:18,370 --> 00:00:21,490
see if it actually gives you the expected result.

6
00:00:21,490 --> 00:00:23,440
So let's actually create two scripts here.

7
00:00:23,470 --> 00:00:28,780
One is going to be a simple script that capitalizes text and then the other script is going to be the

8
00:00:28,780 --> 00:00:33,880
actual test script for this and you're going to need to review object oriented programming to understand

9
00:00:33,880 --> 00:00:37,630
how to use unit tests because you do build out a test class.

10
00:00:37,630 --> 00:00:38,490
Let's get started.

11
00:00:38,700 --> 00:00:40,750
OK here I am at Sublime Text Editor.

12
00:00:40,780 --> 00:00:46,990
I'm going to create two scripts will say new file save as an on my desktop.

13
00:00:47,050 --> 00:00:50,780
I will save the first one as cap that pie.

14
00:00:50,980 --> 00:00:56,200
So this is the script that's actually going to capitalized given text and then the next script will

15
00:00:56,200 --> 00:01:05,870
say file new file file save as and I'm going to save this and technically you can call your test script

16
00:01:05,900 --> 00:01:07,060
whatever you want.

17
00:01:07,290 --> 00:01:11,670
Well we'll call this one test underscore kept up by Save that.

18
00:01:11,700 --> 00:01:14,340
And those are both saved right now in the same location.

19
00:01:14,390 --> 00:01:17,630
So our cap PI file will be very simple.

20
00:01:17,780 --> 00:01:26,730
We'll just have a single function inside of it and this function takes in some text and then what it

21
00:01:26,860 --> 00:01:32,630
is it's going to return Tex dot and we're going to capitalize the text.

22
00:01:32,680 --> 00:01:39,450
So if you don't recall capitalize method on a string just capitalizes single the first single letter.

23
00:01:39,460 --> 00:01:41,510
It doesn't actually uppercase everything.

24
00:01:41,590 --> 00:01:45,780
So you can check that yourself quickly at the command line but that's what the capitalized method does.

25
00:01:45,790 --> 00:01:49,430
We expect text to be some sort of string.

26
00:01:49,440 --> 00:02:01,190
So basically here we're going to input a string output to capitalize string and by capitalized we just

27
00:02:01,190 --> 00:02:05,080
mean capitalize that first letter want to say this.

28
00:02:05,180 --> 00:02:11,510
So cancel save that go to test underscore cap and this is where we actually use the unit test.

29
00:02:11,510 --> 00:02:16,850
So when writing test functions it's best to go from simple to complex as each function is going to be

30
00:02:16,850 --> 00:02:17,770
run an order.

31
00:02:17,810 --> 00:02:23,700
So I usually want to test simple things and then later on test the more complicated things the first

32
00:02:23,700 --> 00:02:30,830
thing is to do is import unit test and then you're going to need to import any of the files you want.

33
00:02:30,870 --> 00:02:36,260
And because these files are both safe at the desktop application I'm going to say import cap.

34
00:02:36,270 --> 00:02:37,530
So that actually imports.

35
00:02:37,530 --> 00:02:39,010
My other thought postscript.

36
00:02:39,010 --> 00:02:42,390
This kept up PI so import a unit test.

37
00:02:42,390 --> 00:02:43,450
That's a built in function.

38
00:02:43,460 --> 00:02:45,330
And let's make sure I spell that right.

39
00:02:45,330 --> 00:02:46,250
It has to tease.

40
00:02:46,260 --> 00:02:48,080
So watch out for that unit test.

41
00:02:48,090 --> 00:02:50,070
Two words in order import cap.

42
00:02:50,100 --> 00:02:56,330
That's the cap file we just created and then we're going to create a class and we'll call this class

43
00:02:56,420 --> 00:03:04,400
Test cap and we're going to inherit the test case class that comes with unit test.

44
00:03:04,400 --> 00:03:09,050
So we say unit test dot test case.

45
00:03:09,050 --> 00:03:15,540
So if this syntax strange to you you can review the inheritance lectures in Object-Oriented Programming.

46
00:03:15,860 --> 00:03:20,540
But here we're just going to have a list of methods and these methods are basically going to be run

47
00:03:21,350 --> 00:03:23,420
when we test the actual script.

48
00:03:23,430 --> 00:03:35,710
So I'll say if test one word self and this is just going to be set text equal to the simple word Python

49
00:03:37,430 --> 00:03:46,230
and the result is going to be we'll say Cap we'll call the cap text function.

50
00:03:46,230 --> 00:03:52,250
So what I'm doing here is importing cap and then from kept up by importing this cap text function.

51
00:03:52,290 --> 00:03:56,560
So I'm saying cap the cap text function and I'll pass in the text.

52
00:03:56,580 --> 00:04:02,210
So that's lowercase Python and I would expect my result to be equal to the following.

53
00:04:02,340 --> 00:04:08,670
We say self dots and we call asserts equal.

54
00:04:08,750 --> 00:04:13,580
We pass and results and then we pass in Python with a capital P.

55
00:04:13,940 --> 00:04:15,970
And this is the general structure of a test.

56
00:04:16,070 --> 00:04:18,980
And when you look at it it's actually really quite simple.

57
00:04:19,070 --> 00:04:26,120
All you do is you create your class to be called at the end you inherit from unit test the test case

58
00:04:27,020 --> 00:04:29,540
and then you have a function to be called.

59
00:04:29,540 --> 00:04:33,590
In this case we call it test 1 and usually you number them like Test 1 Test 2.

60
00:04:33,620 --> 00:04:37,430
Or you maybe have names that refer to what they're actually doing.

61
00:04:37,430 --> 00:04:41,690
So here we could have test one word and then the next one could be testing multiple words.

62
00:04:42,020 --> 00:04:44,110
So here we have Texas to Python.

63
00:04:44,330 --> 00:04:47,690
And then you call whatever functions you want to test off your script.

64
00:04:47,750 --> 00:04:52,280
So that way if someone comes in and they start editing kept the PI and you want to make sure that this

65
00:04:52,280 --> 00:04:57,860
function always returns the same thing you now have this test that will assert that.

66
00:04:57,860 --> 00:05:05,120
So you get the results of something passed into that function and then you say self assert equal and

67
00:05:05,120 --> 00:05:12,530
you're saying I need that when this lower case Python gets passed into the text function that it always

68
00:05:12,830 --> 00:05:16,410
returns back the capitalized version of Python.

69
00:05:16,700 --> 00:05:22,360
And then we can add in another test so we can say DPF and we can call this test something different.

70
00:05:22,370 --> 00:05:26,400
You can either number them or you can find them as far as what they're testing.

71
00:05:26,640 --> 00:05:33,740
So maybe we want to test multiple words well say self and then we'll add in some text here.

72
00:05:35,150 --> 00:05:38,970
And set that equal to let's have it be two words Monty Python.

73
00:05:39,680 --> 00:05:42,340
And that's basically the same thing here we say result.

74
00:05:44,650 --> 00:05:45,930
So copy and paste.

75
00:05:46,060 --> 00:05:52,360
So I want my result to be passing in Monty Python to text and then I will say self-taught are equal

76
00:05:52,360 --> 00:05:53,860
it's copy that line as well.

77
00:05:54,680 --> 00:05:59,900
Onto the new line except in this case when you call capitalise on the string what it should do is capitalize

78
00:05:59,900 --> 00:06:01,970
the first letter of each of these words.

79
00:06:02,030 --> 00:06:11,790
So I should expect to see Monty Python and at the end of all this we can say if you underscore underscore

80
00:06:12,860 --> 00:06:20,160
name is equal to main We will call the main function here.

81
00:06:20,170 --> 00:06:24,960
Unit test the main So save this.

82
00:06:25,000 --> 00:06:28,080
And let's actually now run our test Cup.

83
00:06:28,180 --> 00:06:34,390
All right so what we're going to do here is actually run our test code we'll say Python test underscore

84
00:06:34,510 --> 00:06:38,670
cap and you can auto tap complete that will enter in right now.

85
00:06:38,680 --> 00:06:40,320
It looks like we're actually getting a failure.

86
00:06:40,330 --> 00:06:42,480
So you get this failure report which is kind of nice.

87
00:06:42,480 --> 00:06:44,670
It this fail test multiple words.

88
00:06:44,730 --> 00:06:50,670
It says Monty Python knows here how the P is being lowercase does not equal Monty Python.

89
00:06:51,010 --> 00:06:53,170
So now we're scratching our heads and thinking well what's the issue.

90
00:06:53,170 --> 00:06:56,030
Is it my test or is it my actual file.

91
00:06:56,160 --> 00:06:57,750
And that depends on the situation.

92
00:06:57,760 --> 00:07:04,900
But typically you're going to say hey I need to adjust my main script so that I make sure my testing

93
00:07:04,960 --> 00:07:05,570
works.

94
00:07:05,590 --> 00:07:08,720
And a lot of times larger companies you'll have someone in the queue.

95
00:07:08,740 --> 00:07:14,260
The apartment quality assurance actually writing these tests to make sure that your scripts work as

96
00:07:14,260 --> 00:07:15,940
they expect them to.

97
00:07:16,050 --> 00:07:21,970
And if we think about this the actual issue is as we previously mentioned capitalize all it does is

98
00:07:21,970 --> 00:07:25,250
it capitalizes the very first letter of a string.

99
00:07:25,360 --> 00:07:30,370
That means that if we take a look at Monte Python and you may have noticed it as I was writing that

100
00:07:30,910 --> 00:07:35,980
this is actually not going to happen when we call capitalize and there's actually a different method

101
00:07:35,980 --> 00:07:36,730
for this.

102
00:07:36,730 --> 00:07:42,190
If I want both these letters to be capitalized then what I need to do is instead of calling capitalize

103
00:07:42,520 --> 00:07:48,970
there's actually a method called Title and that will then capitalize every single word instead of just

104
00:07:48,970 --> 00:07:51,730
the first letter of the string.

105
00:07:51,730 --> 00:07:57,370
So now let's save those changes using Title and see if we pass this test.

106
00:07:57,370 --> 00:07:58,870
So we'll come back to the command line.

107
00:08:02,640 --> 00:08:07,800
And I want I'm going to do is run the same thing as I did before Python test underscore cap we hit enter

108
00:08:07,830 --> 00:08:10,530
and it says ran two tests and everything OK.

109
00:08:10,680 --> 00:08:12,760
That means you passed all your tests.

110
00:08:12,840 --> 00:08:13,240
OK.

111
00:08:13,290 --> 00:08:16,190
That's basically all you need to know about unit testing.

112
00:08:16,200 --> 00:08:18,230
So let's go back to a very quick overview.

113
00:08:18,420 --> 00:08:20,160
But the actual idea is very simple.

114
00:08:21,720 --> 00:08:26,820
So you have your scripts and this could be lots of scripts could be a single script and you call functions

115
00:08:26,850 --> 00:08:29,380
or classes whatever you want.

116
00:08:29,790 --> 00:08:34,290
And then inside of your testing script you're going to have is you're going to import you know test

117
00:08:34,650 --> 00:08:39,360
import any of the scripts you've been working on create a class for testing you're going to inherit

118
00:08:39,420 --> 00:08:45,090
from unit test test case and then you get to have methods to test for any situations you can think of.

119
00:08:45,090 --> 00:08:49,950
So you'll set whatever variables you need and then at the end that they somehow you're going to call

120
00:08:49,950 --> 00:08:55,490
functions or classes passing in something you get a result and then you just assert equal.

121
00:08:55,740 --> 00:08:59,610
So you say hey is the result I'm getting from my script.

122
00:08:59,610 --> 00:09:01,000
The expected result.

123
00:09:01,200 --> 00:09:02,270
And that's all you have to do.

124
00:09:02,370 --> 00:09:05,940
So just be wary of this self-taught assert equal statement.

125
00:09:05,940 --> 00:09:11,420
Notice how it's casting self so that it's actually calling it from that test case inheritance.

126
00:09:11,460 --> 00:09:11,860
All right.

127
00:09:11,910 --> 00:09:14,390
That's it for talking about unit tests.

128
00:09:14,400 --> 00:09:15,630
Actually quite straightforward.

129
00:09:15,690 --> 00:09:20,610
Hopefully if you have any questions feel free to post the Q&amp;A forums but only take a look at the documentation

130
00:09:20,640 --> 00:09:21,860
for unit test.

131
00:09:21,870 --> 00:09:26,700
There's a lot more that it can do but a lot of times it's just starting out all you really need to do

132
00:09:26,910 --> 00:09:30,900
is create these simple assert equal statements in your class.

133
00:09:30,900 --> 00:09:32,550
Thanks and we'll see you at the next lecture.
