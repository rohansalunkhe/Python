1
00:00:05,640 --> 00:00:09,450
Welcome back, everyone, to the main series of lectures in this section.

2
00:00:09,490 --> 00:00:11,100
The course for functions.

3
00:00:12,940 --> 00:00:18,700
Creating clean, repeatable code is a key part of becoming an effective programmer and functions in

4
00:00:18,700 --> 00:00:19,270
Python.

5
00:00:19,360 --> 00:00:25,270
Allow us to create blocks of code that can be easily executed many times without needing to constantly

6
00:00:25,270 --> 00:00:27,070
rewrite that entire block of code.

7
00:00:27,730 --> 00:00:33,430
Currently, if we wanted to have a piece of code run twice or call it multiple times, we would have

8
00:00:33,430 --> 00:00:38,710
to copy and paste all the lines in that particular block of code and then call it again.

9
00:00:38,800 --> 00:00:40,660
So we'd have to rewrite that code.

10
00:00:41,020 --> 00:00:44,260
Functions will essentially allow us to do this all within one line.

11
00:00:46,410 --> 00:00:51,090
So keep in mind that functions are going to be a huge leap forward in your capabilities as a Python

12
00:00:51,090 --> 00:00:51,690
programmer.

13
00:00:52,320 --> 00:00:57,750
That also means that the problems you are now able to solve after learning functions can be a lot harder.

14
00:00:59,450 --> 00:01:03,140
So it's really important to get practice combining everything you've learned so far.

15
00:01:03,470 --> 00:01:09,410
Things like control, flow, loops, logic, et cetera, with functions in order to become an effective

16
00:01:09,410 --> 00:01:10,010
programmer.

17
00:01:11,790 --> 00:01:17,580
Now, I've had the experience of teaching lots of students Python at this point in time, and I often

18
00:01:17,580 --> 00:01:23,010
see students that once they start learning functions, they sometimes get discouraged or frustrated

19
00:01:23,310 --> 00:01:27,720
because the level of problems they are able to solve with functions tends to be a steep incline.

20
00:01:28,230 --> 00:01:29,790
I want you to not worry about that.

21
00:01:29,910 --> 00:01:32,220
It's completely normal and very common.

22
00:01:32,520 --> 00:01:34,180
And we're going to do our best to guide you.

23
00:01:34,200 --> 00:01:37,470
Step by step and make the introduction to functions.

24
00:01:37,530 --> 00:01:39,570
A very gradual journey.

25
00:01:40,080 --> 00:01:42,600
So keep in mind that you should be patient for yourself.

26
00:01:42,930 --> 00:01:47,040
We're learning about functions because it is going to be a huge leap forward in your skill set.

27
00:01:47,430 --> 00:01:51,060
And the best way to learn functions is through practice, practice, practice.

28
00:01:52,490 --> 00:01:58,220
OK, so, again, just to kind of visualize what I'm trying to say here is right now the difficulty

29
00:01:58,220 --> 00:02:00,350
of problems you can solve is pretty simple.

30
00:02:00,440 --> 00:02:05,520
And throughout the course, as we progressed in Python and learn things about basic data types, we

31
00:02:05,530 --> 00:02:07,640
were able to solve some simple problems.

32
00:02:07,730 --> 00:02:12,320
And then we learn about things like loops and logic and we're progressing in Python.

33
00:02:12,410 --> 00:02:15,740
And the difficulty of problems we can solve kind of goes along linearly.

34
00:02:16,310 --> 00:02:20,810
However, once you learn about functions, you're going to see a steep incline in the difficulty of

35
00:02:20,810 --> 00:02:24,020
problems that you can theoretically solve with functions.

36
00:02:24,110 --> 00:02:29,000
So I don't want you to get discouraged here when you're gonna make a huge leap in a very short amount

37
00:02:29,000 --> 00:02:30,590
of time by learning about functions.

38
00:02:30,920 --> 00:02:34,670
It's really just about taking on a lot of practice problems with functions.

39
00:02:36,020 --> 00:02:40,190
So my rules about learning about functions of Python is to be patient with yourself.

40
00:02:41,030 --> 00:02:42,800
Take your time to practice the material.

41
00:02:43,100 --> 00:02:47,180
You'll notice that this section of the course actually has more practice problems than almost any other.

42
00:02:48,170 --> 00:02:52,850
And to start getting excited about your new skills and start thinking about personal projects, because

43
00:02:52,850 --> 00:02:58,850
once you learn about functions, it's really going to open a door into allowing you to start connecting

44
00:02:58,850 --> 00:03:01,850
Python to what you actually want to accomplish in the real world.

45
00:03:03,100 --> 00:03:03,320
OK.

46
00:03:03,980 --> 00:03:07,370
In the next lecture, we'll begin to learn how to create functions with Python.

47
00:03:07,670 --> 00:03:08,210
I'll see you there.
