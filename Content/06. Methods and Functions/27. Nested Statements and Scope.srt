1
00:00:05,370 --> 00:00:07,230
Welcome back everyone in this lecture.

2
00:00:07,230 --> 00:00:10,170
We're going to talk about nested statements and scope.

3
00:00:10,170 --> 00:00:14,310
We've got over already how to write our own functions and we've even gotten a little bit of practice

4
00:00:14,310 --> 00:00:15,130
with them.

5
00:00:15,150 --> 00:00:20,160
However it's important to understand how Python deals with the variable names you assign when you create

6
00:00:20,220 --> 00:00:21,460
a variable name in Python.

7
00:00:21,480 --> 00:00:27,270
That name is stored in what's called the namespace and variable names also have a scope and the scope

8
00:00:27,360 --> 00:00:31,400
determines the visibility of that variable name to other parts of your code.

9
00:00:31,650 --> 00:00:36,450
So if you've ever wondered what you've gotten some errors that say something like this variable is not

10
00:00:36,450 --> 00:00:41,820
defined We're going to explore why that happens and how to fix those errors in the future.

11
00:00:41,820 --> 00:00:42,790
Let's get started.

12
00:00:42,970 --> 00:00:43,200
OK.

13
00:00:43,200 --> 00:00:47,160
The first thing we're going to do is with a little thought experiment and then we're going to explore

14
00:00:47,160 --> 00:00:48,450
the results.

15
00:00:48,450 --> 00:00:54,670
Right now I'm going to say x is equal to 25 and below that I will create a function.

16
00:00:55,250 --> 00:00:57,610
It's going to be called the printer.

17
00:00:57,660 --> 00:01:00,930
Make sure you don't call it print otherwise it will overwrite the print function.

18
00:01:01,080 --> 00:01:09,510
But all this does is it sets X to 50 and then it returns x.

19
00:01:09,520 --> 00:01:16,030
So let's run this in and I want to ask you what do you expect to happen when I say Prince X.

20
00:01:16,090 --> 00:01:19,940
Do you expect to see 25 or 50.

21
00:01:19,980 --> 00:01:22,420
So when I run this I get back 25.

22
00:01:22,770 --> 00:01:25,980
And if you had the intuition that it's going to be 25 that's great.

23
00:01:26,040 --> 00:01:28,890
And if you had the intuition that it's going to be 50 Don't worry.

24
00:01:28,890 --> 00:01:31,210
That's exactly what this lecture is here to explain.

25
00:01:32,190 --> 00:01:38,620
What I want to show you is if I say print printer open close parentheses.

26
00:01:38,630 --> 00:01:42,420
So I'm actually running the result a printer and returning the X here.

27
00:01:42,710 --> 00:01:44,700
Then I get back 50.

28
00:01:44,960 --> 00:01:50,030
So when I print out X I get 25 and when I print out the result a printer I get 50.

29
00:01:50,120 --> 00:01:51,320
So it is quite interesting.

30
00:01:51,350 --> 00:01:56,390
But the question we want to answer is how does Python actually know which exercises I'm referring to

31
00:01:56,390 --> 00:01:57,500
in my code.

32
00:01:57,500 --> 00:02:02,030
Am I referring to this exercycle 25 or am I referring to this extra 50.

33
00:02:02,060 --> 00:02:07,910
And why doesn't this re-assignment seem to affect the assignment appear when I later on asked for print

34
00:02:08,000 --> 00:02:14,750
X and this is because the idea of scope allows Python to understand have a set of rules to decide which

35
00:02:14,750 --> 00:02:21,840
variables you're referencing in your code and these rules are a l e G-B rural format.

36
00:02:21,890 --> 00:02:27,170
So EJB stands for local inclosing function local's global and built in.

37
00:02:27,170 --> 00:02:29,330
So let's take a look at this.

38
00:02:29,360 --> 00:02:36,350
So this is the L E G-B rule and this is essentially the order that Python is going to look for variables

39
00:02:36,380 --> 00:02:40,630
in and it starts off by checking out the local namespace.

40
00:02:40,640 --> 00:02:46,490
And these are names that are assigned in any way within a function such as F or inside a land the call

41
00:02:46,790 --> 00:02:48,980
and they're not declared global in that function.

42
00:02:48,980 --> 00:02:53,560
And later on we'll see what we mean by declared global that use a global keyword.

43
00:02:53,570 --> 00:02:59,630
So first Python is going to say hey if you call that variable is it in my local namespace then it's

44
00:02:59,630 --> 00:03:02,210
going to check for inclosing function locals.

45
00:03:02,240 --> 00:03:07,110
And these are names in the local scope of any and all inclosing functions from inner to outer.

46
00:03:07,160 --> 00:03:11,380
So you can actually have functions inside of functions which you're going to see in just a second.

47
00:03:11,780 --> 00:03:15,910
Then it looks for the variable if it still can't find it in the global namespace.

48
00:03:15,950 --> 00:03:21,800
And these are names assigned at the top level of a module file or declared global in ADF within the

49
00:03:21,800 --> 00:03:22,600
file.

50
00:03:22,850 --> 00:03:25,330
Then it looks in the built in namespace.

51
00:03:25,340 --> 00:03:31,350
So these are names that are pre-assigned in the built in names module things like Open Range list as

52
00:03:31,360 --> 00:03:38,330
TR syntax errors et cetera built in keywords in Python that you see have those special syntax highlighting

53
00:03:38,330 --> 00:03:39,250
rules to them.

54
00:03:39,560 --> 00:03:46,400
Let's explore what each of these looks like local inclosing functional locals global and built in.

55
00:03:46,970 --> 00:03:49,210
We'll start off an example of local.

56
00:03:49,280 --> 00:03:56,450
So an example of local would be something like inside a the function if we were to say numb colon numb

57
00:03:56,530 --> 00:03:57,890
to the power of two.

58
00:03:58,030 --> 00:04:00,440
This is an example of a local variable.

59
00:04:00,460 --> 00:04:05,990
So numb right here is local to this lambda expression.

60
00:04:06,050 --> 00:04:09,320
Let's now see an example of inclosing function locals.

61
00:04:09,400 --> 00:04:15,200
So we'll go ahead and just comment this out in order to show you an example of inclosing function local's

62
00:04:15,200 --> 00:04:18,970
we first need to create a function and then a function inside of it.

63
00:04:19,010 --> 00:04:27,680
So I'm going to say name at the very top of this notice in the notation and say this is a global string

64
00:04:29,990 --> 00:04:39,110
that will create a function called greets it takes no arguments and inside of this I say name is equal

65
00:04:39,110 --> 00:04:40,110
to Sammy.

66
00:04:40,310 --> 00:04:42,310
So I'm overwriting that name variable here.

67
00:04:44,590 --> 00:04:54,220
And that inside of this function I'm going to say hello and I'm going to say Hey Prince Hello concatenated

68
00:04:54,220 --> 00:04:58,850
with name and then I'm going to call hello.

69
00:04:59,570 --> 00:05:01,690
And then I'm going to call greet.

70
00:05:01,880 --> 00:05:02,180
All right.

71
00:05:02,190 --> 00:05:05,560
There's a lot going on here especially since there's a function instead of function.

72
00:05:05,720 --> 00:05:08,190
So let's break this one down here.

73
00:05:08,210 --> 00:05:15,050
I have a function called greets and what it does inside of it it sets the variable name equal to Samie.

74
00:05:15,050 --> 00:05:19,280
Then inside of the group functions and notice how all of this right here that I have have highlighted

75
00:05:19,280 --> 00:05:25,700
is inside accretes because the indentation we have DPF than the hello function and inside the hello

76
00:05:25,700 --> 00:05:27,130
function I ask hey.

77
00:05:27,260 --> 00:05:34,010
Print out Hello name and right now we have name defined twice appear as a global and then over here

78
00:05:34,130 --> 00:05:41,180
as an enclosing function local and after we define Hello inside of greets I'm actually going to call

79
00:05:41,750 --> 00:05:42,970
and execute Hello.

80
00:05:43,280 --> 00:05:45,380
And then I will call greet.

81
00:05:45,400 --> 00:05:47,630
So let's see what happens when we run this.

82
00:05:47,630 --> 00:05:48,590
We get back.

83
00:05:48,590 --> 00:05:49,690
Hello Sammy.

84
00:05:49,760 --> 00:05:52,000
When I actually execute greet.

85
00:05:52,010 --> 00:05:53,500
So what's actually happening here.

86
00:05:53,780 --> 00:06:00,860
Well when I execute greets in effect we can put this in a new cell so that's a little more clear what's

87
00:06:00,860 --> 00:06:01,360
happening.

88
00:06:01,370 --> 00:06:06,020
So greed is kind of separate that greet call is separate from the definition of greed.

89
00:06:06,620 --> 00:06:10,860
When I call greed what happens is I'm calling and executing the function.

90
00:06:10,910 --> 00:06:15,050
It assigns name equal to Samie and then it defines this function.

91
00:06:15,050 --> 00:06:15,650
Hello.

92
00:06:15,890 --> 00:06:17,040
And then it calls the function.

93
00:06:17,050 --> 00:06:17,520
Hello.

94
00:06:17,600 --> 00:06:22,170
So when you call greet internally it's it's created this variable name.

95
00:06:22,340 --> 00:06:23,270
It's defined in this function.

96
00:06:23,270 --> 00:06:23,710
Hello.

97
00:06:23,870 --> 00:06:25,100
And then it's executing this function.

98
00:06:25,100 --> 00:06:26,250
Hello.

99
00:06:26,460 --> 00:06:29,030
This is going to follow this.

100
00:06:29,100 --> 00:06:33,420
EJB rule when we want to figure out well what name are you referring to.

101
00:06:33,420 --> 00:06:36,020
So first off we look in the local namespace.

102
00:06:36,180 --> 00:06:41,740
So the local namespace as we just discussed is within a function.

103
00:06:41,870 --> 00:06:47,340
So here in the function DPF Hello it's going to say hey did I define name anywhere.

104
00:06:47,420 --> 00:06:49,700
If it can't find it then it goes to the next level.

105
00:06:49,910 --> 00:06:56,390
So after searching local we go to inclosing function locals so we have this inclosing function because

106
00:06:56,390 --> 00:06:59,100
this function is inside of this function.

107
00:06:59,270 --> 00:07:01,790
And then here we do find that name has been defined.

108
00:07:01,970 --> 00:07:03,050
It's equal to Samie.

109
00:07:03,080 --> 00:07:04,480
So that's the one we choose.

110
00:07:04,610 --> 00:07:09,670
When we finally execute Hello here and that's why you see Hello Sammy.

111
00:07:09,740 --> 00:07:10,780
Now let's see what happens.

112
00:07:10,820 --> 00:07:19,340
If I were to comment this out so I've commented out Samie and if I run this again and I run that again

113
00:07:19,670 --> 00:07:20,270
now I see.

114
00:07:20,270 --> 00:07:20,850
Hello.

115
00:07:20,900 --> 00:07:22,300
This is a global strain.

116
00:07:22,520 --> 00:07:23,510
So what's happening now.

117
00:07:23,510 --> 00:07:25,130
Well we're going through the same rules.

118
00:07:25,130 --> 00:07:26,510
We first look at local.

119
00:07:26,690 --> 00:07:27,530
So when we execute.

120
00:07:27,530 --> 00:07:31,730
Hello here this hello function says well I've got to figure out what variable name is.

121
00:07:31,760 --> 00:07:33,680
I don't see it defined locally.

122
00:07:33,920 --> 00:07:39,020
And now since this is commented I don't see it defined in the inclosing function.

123
00:07:39,020 --> 00:07:44,940
So then I'm going to go to the next step which is to look globally and there it is in the global level.

124
00:07:44,960 --> 00:07:50,360
This is a global string and Global's defined by basically having no indentation it's all the way to

125
00:07:50,360 --> 00:07:54,130
the left either in your script or in your Jupiter notebook cell.

126
00:07:54,320 --> 00:07:58,130
And because it found that in the global namespace it now says hello.

127
00:07:58,160 --> 00:07:59,880
This is a global string.

128
00:08:00,320 --> 00:08:05,940
Let's see what happens if we uncomment this and we say name equal Sammy.

129
00:08:06,110 --> 00:08:07,950
So I want to kind of label these right now.

130
00:08:08,060 --> 00:08:10,340
This is the global.

131
00:08:10,540 --> 00:08:15,770
This one right here is inclosing and let's make a local one.

132
00:08:16,380 --> 00:08:27,230
So a local is going to be name is equal to I'm a local and now when we rerun this to redefine this function

133
00:08:27,740 --> 00:08:30,170
and I call greet it gets back.

134
00:08:30,320 --> 00:08:32,070
Hello I'm a local.

135
00:08:32,150 --> 00:08:38,120
Because when you execute hello that executes this function right here which says hey Python to figure

136
00:08:38,120 --> 00:08:40,270
out what does his name variable assigned to.

137
00:08:40,550 --> 00:08:44,640
And it immediately finds in the local namespace where here name I a local.

138
00:08:44,660 --> 00:08:46,030
So that's what it uses.

139
00:08:46,520 --> 00:08:50,900
As you can comment these out there look in the next level meaning the inclosing functions.

140
00:08:50,990 --> 00:08:56,360
If you comment that went out it will then look in the global namespace level and the only level above

141
00:08:56,360 --> 00:09:02,130
Global is the built in function level which would be something like a built in function as length.

142
00:09:02,180 --> 00:09:06,830
So you should always be careful not to overwrite these built in function names and they're always indicated

143
00:09:06,830 --> 00:09:08,600
to you with syntax highlighting.

144
00:09:08,630 --> 00:09:14,480
And if you're not sure if you're able to call help on it and you see some stuff pop out then it's definitely

145
00:09:14,500 --> 00:09:16,970
built in because it already has that information.

146
00:09:16,970 --> 00:09:22,590
Finally to close off this discussion we're going to do is look back at our original thought experiment.

147
00:09:22,700 --> 00:09:28,510
This assignment of x 25 at the global level re-assigning at the local level returning the local signs

148
00:09:28,510 --> 00:09:33,350
it but still being able to print out both when we wanted to we could print out the global one or we

149
00:09:33,350 --> 00:09:36,040
could return that local one and print that as well.

150
00:09:36,080 --> 00:09:40,750
So we're going to discuss local variables as well as the global keyword.

151
00:09:40,830 --> 00:09:46,440
So we're going to show you an example of that thought experiment again will say x is equal to 50 and

152
00:09:46,470 --> 00:09:58,140
I'm going to create a function that takes an X and it's going to print using string notation x is x

153
00:09:58,140 --> 00:10:00,300
right here.

154
00:10:00,330 --> 00:10:07,520
So let's run that and let's see what happens when we call function X it says X is 50.

155
00:10:07,650 --> 00:10:08,850
Perfect.

156
00:10:08,850 --> 00:10:14,630
Now also inside this function I'm going to reassign X locally.

157
00:10:14,700 --> 00:10:27,000
So this is a local reassignments and then I'm going to print I just locally

158
00:10:29,970 --> 00:10:39,800
changed lips changed x to and then we'll insert X right there and make sure your X variables lowercase

159
00:10:39,800 --> 00:10:40,100
here.

160
00:10:40,150 --> 00:10:44,780
If it was lowercase there now we rerun the cell to read the fine that function.

161
00:10:44,910 --> 00:10:49,860
And now I see X's 50 I just locally changed x to be 200.

162
00:10:50,050 --> 00:10:57,440
Now outside of this what happens if I print X will I get back X's 50.

163
00:10:57,550 --> 00:11:00,730
Even though inside of this function I re-assign that to be 200.

164
00:11:01,000 --> 00:11:07,480
And this is because this re-assignment is only happening in the local space inside of this function.

165
00:11:07,510 --> 00:11:10,290
Its not going to affect anything at a higher scope.

166
00:11:10,510 --> 00:11:12,160
So this is what scope really is.

167
00:11:12,160 --> 00:11:17,890
Its the fact that when you declare variables inside of a function definition such as what we've done

168
00:11:17,890 --> 00:11:24,070
here those variable names only have a scope local to this function.

169
00:11:24,100 --> 00:11:29,770
So that's the scope of this variable x right here when we assignment when we assign it inside of this

170
00:11:29,770 --> 00:11:30,640
function.

171
00:11:30,730 --> 00:11:36,310
We have an X here floating around in the global space but this local re-assignment scope won't be able

172
00:11:36,310 --> 00:11:44,620
to extend outside to another level which is why after all we're still able to print out x here as 50.

173
00:11:44,650 --> 00:11:49,900
So let's imagine you're in a situation where for whatever reason you actually did want to grab this

174
00:11:49,900 --> 00:11:52,760
global x and reassign it to be 200.

175
00:11:52,810 --> 00:11:54,070
How could you do that.

176
00:11:54,540 --> 00:11:58,360
Well in that case we will actually accept X as a parameter.

177
00:11:58,370 --> 00:12:02,110
Instead we're going to do inside or function at the very first line.

178
00:12:02,410 --> 00:12:10,030
We're going to the declare the global keyword here and then say X and what this does is instead of passing

179
00:12:10,030 --> 00:12:13,100
an X we're going to declare a global x.

180
00:12:13,390 --> 00:12:20,590
And that tells Python hey I want to go to the namespace jumped to the global level and grab the x variable

181
00:12:20,590 --> 00:12:26,370
there and then what I want you to do is anything that happens inside the scope of this assignment.

182
00:12:26,590 --> 00:12:28,930
It's going to affect that global x.

183
00:12:29,080 --> 00:12:35,050
So we're going to grab that Global X report what it is we're going to do a local reassignments except

184
00:12:35,050 --> 00:12:44,380
now it's on a global variable meaning it will have an effect to this guy up here 50 and there we're

185
00:12:44,380 --> 00:12:50,790
going to print I just locally changed global x to some other value.

186
00:12:51,350 --> 00:12:56,450
OK so let's run this again and I'm going to instead of having this be 200 I'm going to make it super

187
00:12:56,450 --> 00:12:59,420
obvious that we're changing it by saying new value.

188
00:12:59,450 --> 00:13:01,370
So X used to be a number 50.

189
00:13:01,570 --> 00:13:05,820
I'm going to want to call this function reached to the global namespace grab that x.

190
00:13:05,900 --> 00:13:07,280
Report back what it is.

191
00:13:07,280 --> 00:13:10,060
Change it to the string new value and display.

192
00:13:10,080 --> 00:13:12,260
Hey I just locally change global x.

193
00:13:12,350 --> 00:13:18,610
So let's run this and I'm going to create a couple of more cells here using alt enter.

194
00:13:18,630 --> 00:13:20,350
Just we get kind of a clean slate.

195
00:13:20,470 --> 00:13:21,650
So I just ran this.

196
00:13:21,910 --> 00:13:23,820
And let's see what happens when I actually call it.

197
00:13:23,830 --> 00:13:25,630
So right now I just define the function.

198
00:13:25,630 --> 00:13:27,920
I haven't actually executed it yet.

199
00:13:28,150 --> 00:13:33,010
So when I right now say print X I still have 50 there.

200
00:13:33,010 --> 00:13:39,140
Let's see what happens when I execute the function it says hey X is 50 as we just saw.

201
00:13:39,210 --> 00:13:44,950
And now it's reporting back I just locally changed the global x to the string new value.

202
00:13:45,070 --> 00:13:47,660
And now let's see what happens when I print out x.

203
00:13:47,850 --> 00:13:49,800
It says new value.

204
00:13:49,800 --> 00:13:54,810
So using the global keyword you're able to reach out into that global namespace and then your local

205
00:13:54,810 --> 00:13:58,030
assignments do affect that global variable.

206
00:13:58,080 --> 00:14:03,390
Now because of the power of this global key we're to do that in general especially if you're a beginner

207
00:14:03,660 --> 00:14:07,850
you should avoid using that global keyword unless absolutely necessary.

208
00:14:07,890 --> 00:14:14,610
Instead what you should be doing is if you want to grab that global variable and then affect it you

209
00:14:14,610 --> 00:14:24,700
should take it in as a parameter do the reassignments and then return the reassignments as an object

210
00:14:24,700 --> 00:14:25,690
itself.

211
00:14:25,690 --> 00:14:28,530
So let me show you why you would want to do that.

212
00:14:28,630 --> 00:14:37,690
So now that I ran that if I print to X X is still 50 and then I'm going to say phunk of X and then I

213
00:14:37,690 --> 00:14:44,430
will say X is now equal to phunk of x so X is 50 I just locally changed and I should've deleted this

214
00:14:44,420 --> 00:14:46,230
is no longer that global variable.

215
00:14:46,530 --> 00:14:47,940
But technically.

216
00:14:47,940 --> 00:14:53,570
Now when I call X it's been reassigned to the string new value because I returned X and I re-assign

217
00:14:53,670 --> 00:14:56,330
the global level X using that function.

218
00:14:56,460 --> 00:15:01,540
And in general this is a much better path to take than using that global keyword.

219
00:15:01,560 --> 00:15:07,950
So even though this combined with this is the same thing as the global keyword It's much cleaner and

220
00:15:07,950 --> 00:15:14,220
much safer because you have to know that you want this re-assignment to take place somewhere else in

221
00:15:14,220 --> 00:15:15,030
your code.

222
00:15:15,030 --> 00:15:20,310
It's quite dangerous to have the reassignments take place of the global keyword because as you use larger

223
00:15:20,310 --> 00:15:24,930
and larger scripts and you have separate scripts interacting with each other you're me accidentally

224
00:15:24,960 --> 00:15:28,540
overwrite global keyword inside of a function without ever knowing it.

225
00:15:28,550 --> 00:15:30,390
And that makes it a lot harder to debug.

226
00:15:30,390 --> 00:15:34,950
It's a lot easier to debug something like this because there's a clear re-assignment happening here

227
00:15:35,250 --> 00:15:40,330
where we specifically grab that Global X and said Hey now make that equal to the output.

228
00:15:40,330 --> 00:15:41,640
What are this function is.

229
00:15:41,640 --> 00:15:45,960
And later on when you have your first milestone one project that's the pathway we're going to recommend

230
00:15:45,960 --> 00:15:50,590
taking for dealing with certain objects in that global namespace.

231
00:15:50,950 --> 00:15:51,660
OK.

232
00:15:51,750 --> 00:15:56,910
Definitely a more complex topic but hopefully it allows you to build an intuition of nested statements

233
00:15:57,180 --> 00:15:59,760
and more importantly the scope of variables.

234
00:15:59,760 --> 00:16:02,110
Thanks everyone and we'll see you at the next lecture.
