1
00:00:05,600 --> 00:00:07,660
Welcome back everyone in this lecture.

2
00:00:07,670 --> 00:00:12,980
We're going to be discussing two functional parameters that you'll definitely be encountering as you

3
00:00:12,980 --> 00:00:14,370
look at other people's code.

4
00:00:14,390 --> 00:00:20,360
And as you begin to look at open source code on the Internet those are star arcs and double star quarks

5
00:00:20,420 --> 00:00:23,650
and they stand for arguments and keyword arguments.

6
00:00:23,750 --> 00:00:29,300
Eventually if you work with Python functions long enough you're going to want a way to accept an arbitrary

7
00:00:29,300 --> 00:00:35,960
number of arguments and keyword arguments without having to pre-defined a bunch of parameters in your

8
00:00:35,960 --> 00:00:36,910
function calls.

9
00:00:37,070 --> 00:00:41,420
So let's show you what I'm talking about by jumping to DUPERE notebook.

10
00:00:41,440 --> 00:00:43,570
Let's begin by doing the following.

11
00:00:43,570 --> 00:00:51,630
I'm going to create a function DPF and will call it my func and what this function is going to do is

12
00:00:51,700 --> 00:00:54,310
it's going to accept two numbers.

13
00:00:54,450 --> 00:01:00,290
Number a number be and will put a little comma here.

14
00:01:00,470 --> 00:01:04,780
Returns 5 percent.

15
00:01:06,050 --> 00:01:11,500
Of the sum of B so let's go ahead and do that.

16
00:01:11,540 --> 00:01:17,620
We're going to say some and we need to pass this as a tuple or a list here and B.

17
00:01:17,640 --> 00:01:23,880
So it takes the sum of everything in a tuple A and B and then multiply it by 0.05.

18
00:01:24,370 --> 00:01:29,800
So then when I call my func and lets pass two numbers let's say 40 and 60.

19
00:01:30,100 --> 00:01:34,950
Well 40 plus 60 is 100 and 5 percent of 100 is five.

20
00:01:34,960 --> 00:01:36,360
All right great.

21
00:01:36,370 --> 00:01:38,100
Now the question arises.

22
00:01:38,140 --> 00:01:42,670
What if I had more numbers or more parameters to pasan.

23
00:01:42,670 --> 00:01:48,120
Notice right now that A and B These are examples of what are called positional arguments.

24
00:01:48,160 --> 00:01:54,220
That is to say that right now 40 is assigned to a because it was in the first position or the first

25
00:01:54,250 --> 00:02:00,200
argument and that 60 is assigned to B because it was in the second position or second argument.

26
00:02:00,520 --> 00:02:05,980
I notice that when we want to work with multiple positional arguments then the sum function we had to

27
00:02:05,980 --> 00:02:08,820
pass them in as a tuple.

28
00:02:08,830 --> 00:02:13,400
The question I was saying was what if we want to work with more than two numbers.

29
00:02:13,600 --> 00:02:19,570
Well one thing we could do is assign a lot of parameters so I could say well just in case the person

30
00:02:19,570 --> 00:02:23,500
passes in c c there an I'll set it default to 0.

31
00:02:23,500 --> 00:02:25,710
Just in case they pass it in that way.

32
00:02:25,720 --> 00:02:27,580
Just in case they don't pass it in it's zero.

33
00:02:27,640 --> 00:02:30,740
It won't affect the sum but they can always overwrite it.

34
00:02:30,790 --> 00:02:32,150
And what if they all four numbers.

35
00:02:32,150 --> 00:02:37,450
Well then I'll say is equal to zero as well and all have the equal to zero.

36
00:02:37,460 --> 00:02:38,030
Whoops.

37
00:02:38,080 --> 00:02:39,090
D here.

38
00:02:39,400 --> 00:02:40,260
By default.

39
00:02:40,300 --> 00:02:46,730
And if I want another number I can say e equal to zero here and will say he is here as well.

40
00:02:47,150 --> 00:02:51,330
Now I still get back the same results but I can always pass in more parameters here.

41
00:02:51,360 --> 00:02:56,150
Soon after I pass an extra hundred I get back 10 as 5 percent of 200.

42
00:02:56,380 --> 00:03:03,910
And if I want 5 percent of let's say 300 then I end up getting 15 here and I can keep adding below this

43
00:03:03,970 --> 00:03:11,630
I'll eventually eliminate up to E because if I keep adding more numbers eventually I'll say type error.

44
00:03:11,680 --> 00:03:17,440
Hey it only takes from 2 to 5 positional arguments but six were given and this is where we can use that

45
00:03:17,440 --> 00:03:18,580
star args.

46
00:03:18,640 --> 00:03:23,840
It allows us to set this to take an arbitrary number of arguments.

47
00:03:24,160 --> 00:03:30,160
So when a function parameter starts with that star or asterisk what are we going to do here.

48
00:03:30,200 --> 00:03:35,750
S. The F my phunk star args.

49
00:03:35,770 --> 00:03:44,170
What this allows me to do is treat this as a tuple of parameters are coming in and then I'm going say

50
00:03:44,200 --> 00:03:51,410
return some of arcs and then multiply that by 0.05.

51
00:03:52,000 --> 00:03:57,640
And now I can pass in as many arguments as I want and by default what Python is going to do is it's

52
00:03:57,640 --> 00:04:02,450
going to take all the parameters are passed in and set them to be inside a tuple.

53
00:04:02,740 --> 00:04:07,600
So we're going to pass and let's say 40 and 60 and we get back 5.

54
00:04:07,630 --> 00:04:13,660
If I add in another 100 there I get back 10 and I can keep adding in as many numbers as I want to here

55
00:04:14,170 --> 00:04:15,200
with no problems.

56
00:04:15,220 --> 00:04:20,050
And that's basically the whole point of this star args and I want to just really bring the point home

57
00:04:20,440 --> 00:04:27,950
that if I just print out what args actually looks like to this function it just looks like a tuple here.

58
00:04:28,000 --> 00:04:30,850
So all this does when it starts with this started term.

59
00:04:30,850 --> 00:04:33,880
It says OK whatever this parameter is.

60
00:04:34,000 --> 00:04:39,220
The user can pass in as many as they want and it's going to be treated as a tuple inside its function

61
00:04:39,480 --> 00:04:41,490
and that's really useful for a lot of things.

62
00:04:41,500 --> 00:04:47,080
You can loop through it or iterate through it or sum it together or do any sort of aggregation function

63
00:04:47,470 --> 00:04:54,730
on whatever this tuple that is being passed then you should also notice that by convention we use args.

64
00:04:54,730 --> 00:04:59,780
But this technically is indicated by the fact that there is that star term there.

65
00:04:59,790 --> 00:05:03,190
Args can be any other keyword you want.

66
00:05:03,190 --> 00:05:09,590
For example if I say Star spam and I say print spam it has the exact same effect.

67
00:05:09,790 --> 00:05:10,200
Args.

68
00:05:10,210 --> 00:05:12,650
That word itself is an arbitrary choice.

69
00:05:12,790 --> 00:05:18,770
As long as it's followed by this asterisk or star now by convention you should always use args.

70
00:05:18,790 --> 00:05:22,590
Otherwise someone else is going to be reading your code and scratching your head thinking why is this

71
00:05:22,600 --> 00:05:23,670
person to spam.

72
00:05:23,860 --> 00:05:29,150
So by convention by pipit styling you should always use star with args.

73
00:05:29,530 --> 00:05:33,110
OK so that's arguments and an arbitrary number of arguments.

74
00:05:33,160 --> 00:05:34,540
So you just ask in now.

75
00:05:34,690 --> 00:05:37,700
And you get to have them as a tuple inside your function.

76
00:05:37,840 --> 00:05:38,810
So you could do something like.

77
00:05:38,800 --> 00:05:44,150
For item in args.

78
00:05:44,490 --> 00:05:48,180
Print item and you get back 40 60 100.

79
00:05:48,390 --> 00:05:53,070
So this allows you again pass in as many arguments as you want and you get the tuple of arguments to

80
00:05:53,070 --> 00:05:55,500
play around with inside your function.

81
00:05:55,500 --> 00:06:01,230
Similarly Python offers a way to handle an arbitrary number of key word arguments.

82
00:06:01,320 --> 00:06:08,460
So instead of creating a tuple of values what happens is we use star star quarks which is keyword arguments

83
00:06:08,520 --> 00:06:11,470
and that builds a dictionary of key value pairs.

84
00:06:11,790 --> 00:06:16,670
So let's bill a function that utilizes that we're going to say f my func.

85
00:06:17,310 --> 00:06:30,920
Star Star quarks and we will say if let's make a function if fruit is in keyword arguments that I'm

86
00:06:30,940 --> 00:06:39,180
going to print something like my fruit of choice is.

87
00:06:39,380 --> 00:06:48,230
And then here is where we're going to use the format an adult format call is keywords arguments we're

88
00:06:48,230 --> 00:06:51,010
going to treat it as a dictionary it's coming in.

89
00:06:51,200 --> 00:06:52,700
So just like args here.

90
00:06:52,700 --> 00:06:57,900
So Star Ark's returns back a tuple what star star keyword arguments does.

91
00:06:57,920 --> 00:07:05,750
It returns back a dictionary meaning I can grab this passen fruits here.

92
00:07:06,940 --> 00:07:09,370
And let me zoom out one level so we can see the whole thing.

93
00:07:10,460 --> 00:07:11,370
And then I'm going to say.

94
00:07:11,370 --> 00:07:15,730
Else Prince.

95
00:07:15,840 --> 00:07:17,860
I did not find the need for here

96
00:07:21,180 --> 00:07:21,470
OK.

97
00:07:21,480 --> 00:07:23,050
So what does this actually mean.

98
00:07:23,320 --> 00:07:24,360
Well it means the following.

99
00:07:24,360 --> 00:07:25,990
I can call my funk now.

100
00:07:26,970 --> 00:07:31,190
And passen fruit equal to Apple.

101
00:07:31,380 --> 00:07:38,040
And it turns out that it says my fruit of choice is Apple and I could also pass some other keywords.

102
00:07:38,220 --> 00:07:46,500
Maybe I want to say my favorite veggie is equal to lettuce.

103
00:07:47,100 --> 00:07:49,280
And Python is no longer complaining.

104
00:07:49,340 --> 00:07:52,780
And that's the power of these arbitrary keyword arguments.

105
00:07:52,850 --> 00:08:00,410
What keyword arguments now is is if we print it out at the very beginning it's just the dictionary we

106
00:08:00,410 --> 00:08:05,240
see here that fruit is now attached to Apple vegi is now attached to lettuce and it allows you to call

107
00:08:05,240 --> 00:08:07,190
it any way you want.

108
00:08:07,280 --> 00:08:15,110
So just like args appear return back a tuple keyword arguments return back a dictionary that you can

109
00:08:15,110 --> 00:08:17,270
then play around with inside your function.

110
00:08:17,270 --> 00:08:22,100
So now the user is going to be able to define any parameters you want and they are going to be able

111
00:08:22,100 --> 00:08:25,870
to call them and use them later on inside of your function.

112
00:08:25,990 --> 00:08:27,100
And again.

113
00:08:27,230 --> 00:08:28,670
Kw.

114
00:08:28,820 --> 00:08:31,280
That is technically an arbitrary choice.

115
00:08:31,280 --> 00:08:34,980
What is really indicating this to be a dictionary.

116
00:08:35,000 --> 00:08:40,170
Once it's inside is the use of two asterisks here or two stars.

117
00:08:40,190 --> 00:08:43,680
So again I'm going to change this is something that's to make that really obvious.

118
00:08:43,880 --> 00:08:51,020
I could say Star Star jelly and let's delete all this and just say print jelly.

119
00:08:51,040 --> 00:08:53,530
Notice I get back the same results here.

120
00:08:53,580 --> 00:09:01,020
So by convention you should be using choirs or keyword arguments here but keep in mind what's what's

121
00:09:01,040 --> 00:09:05,510
really in the can a python that is special is these two stars in front of it.

122
00:09:06,390 --> 00:09:13,450
And now to end their discussion of arguments and keyword arguments is we can actually use these in combination.

123
00:09:13,590 --> 00:09:15,470
So we'll show you a simple example of that.

124
00:09:15,600 --> 00:09:20,400
You can check out the note book for another example of this but we're going to accept both arguments

125
00:09:20,490 --> 00:09:24,100
at the beginning and keyword arguments at the beginning.

126
00:09:26,650 --> 00:09:29,180
And I will say the following.

127
00:09:29,280 --> 00:09:33,870
We're going to print out.

128
00:09:33,980 --> 00:09:35,840
I would like

129
00:09:38,590 --> 00:09:52,160
Blink blink and I'm going to say what formats and we're going to grab arcs of 0 and then we'll say quarks

130
00:09:52,270 --> 00:09:55,410
of and we'll expect it to be some sort of food item there.

131
00:09:57,190 --> 00:10:03,360
So then I can call my funk and I can pass in as many numbers here as I want.

132
00:10:03,360 --> 00:10:09,350
So 10 20 30 and those will now be accepted inside of the tuple.

133
00:10:09,600 --> 00:10:15,420
And then after that I can pass in as many keyword arguments as I want so I could say something like

134
00:10:15,870 --> 00:10:19,870
fruit is equal to orange.

135
00:10:20,100 --> 00:10:27,500
Food is equal to eggs and it may be animal equal to dog.

136
00:10:27,930 --> 00:10:31,400
And then when I run this what happens is it takes in the arcs.

137
00:10:31,710 --> 00:10:36,450
So let's print these out so we can really see what's going on here and takes in the keyword arguments

138
00:10:36,450 --> 00:10:37,470
as well.

139
00:10:39,090 --> 00:10:40,710
Your arguments here.

140
00:10:40,930 --> 00:10:45,070
It takes in those arguments takes some a tuple so you can do whatever you want with them.

141
00:10:45,280 --> 00:10:48,950
It takes in the keyword arguments sets them as a dictionary and you can use whatever you want with them

142
00:10:49,360 --> 00:10:55,640
and then you can kind of look for things here and then print out whatever you want.

143
00:10:55,640 --> 00:11:00,080
And again this allows you to take in an arbitrary number of arguments that you don't need to define

144
00:11:00,110 --> 00:11:00,960
beforehand.

145
00:11:01,190 --> 00:11:05,570
And this is going to be especially useful when you begin to use outside libraries.

146
00:11:05,570 --> 00:11:10,970
So right now at your current stage you're not going to find a whole lot of use cases for args and keyword

147
00:11:11,000 --> 00:11:16,370
arguments but later on they're going to become incredibly powerful tools for you to leverage in your

148
00:11:16,370 --> 00:11:17,330
own code.

149
00:11:17,550 --> 00:11:22,750
But right now you should just keep in mind that you have a way to accept an arbitrary number of arguments.

150
00:11:22,850 --> 00:11:28,130
The only final point I'd like to make here is that because we said args first and keyword arguments

151
00:11:28,580 --> 00:11:29,930
it has to go in that order.

152
00:11:29,960 --> 00:11:35,930
We have to have that list or a tuple of arguments and then after that we have to have the keyword arguments

153
00:11:35,960 --> 00:11:41,300
where we're not able to do is after this define an argument without a keyword.

154
00:11:41,300 --> 00:11:46,400
Otherwise Python is going to complain it's going to say hey you have a position argument which is following

155
00:11:46,430 --> 00:11:47,720
a keyword argument.

156
00:11:47,770 --> 00:11:49,990
It has to be in the same order you prescribe.

157
00:11:50,000 --> 00:11:54,320
Which was all the arguments first then all the keyword arguments.

158
00:11:54,320 --> 00:11:54,950
All right.

159
00:11:54,950 --> 00:11:57,410
That's the basics of args and quarks.

160
00:11:57,440 --> 00:11:58,810
I hope you found that helpful.

161
00:11:58,880 --> 00:12:00,320
We'll see you at the next lecture.
