1
00:00:05,230 --> 00:00:07,180
Welcome back everyone in this lecture.

2
00:00:07,180 --> 00:00:12,790
We're going to be discussing lambda expressions map and filter lammed expressions are way to quickly

3
00:00:12,790 --> 00:00:15,220
create what are known as anonymous functions.

4
00:00:15,220 --> 00:00:19,660
Basically just one time use functions that you don't even really name you just use them one time and

5
00:00:19,660 --> 00:00:21,380
then never reference them again.

6
00:00:21,550 --> 00:00:26,200
To understand why this would be useful and in what context you'd use this you first need to talk about

7
00:00:26,200 --> 00:00:31,320
the map function that's built into Python as well as the filter function that's built into Python.

8
00:00:31,360 --> 00:00:36,670
So let's open up a you know but talk about MAP talk about filter and then we'll discuss lambda expressions.

9
00:00:36,670 --> 00:00:37,650
Let's get started.

10
00:00:37,870 --> 00:00:40,120
OK so we're back here at our Jupiter notebook.

11
00:00:40,120 --> 00:00:43,350
The first thing we're going to do is check out the map function.

12
00:00:43,480 --> 00:00:48,220
You can see here right when I type map I'm getting some syntax highlighting so it's green and that indicates

13
00:00:48,220 --> 00:00:50,000
to me that this is the built in function.

14
00:00:50,140 --> 00:00:53,710
And as we know we can shift tab to check out the documentation string.

15
00:00:53,790 --> 00:00:59,080
Now you can see here it expects a funk and that's short code for function and then some sort of iterable

16
00:00:59,080 --> 00:00:59,770
object.

17
00:00:59,830 --> 00:01:02,890
You can expand this will explain a little more about what it does.

18
00:01:02,950 --> 00:01:07,690
But the best way to actually realize what it does is to just show you since we know it's expecting a

19
00:01:07,690 --> 00:01:14,520
function Let's quickly create a function we'll use the keyword and we're going to create a simple function

20
00:01:14,670 --> 00:01:17,230
that just takes in a number and returns it square.

21
00:01:18,900 --> 00:01:25,560
So we will define a function called Square takes in a number and it returns that number to the power

22
00:01:25,590 --> 00:01:27,810
to where the square of that number.

23
00:01:27,810 --> 00:01:36,930
Now let's imagine I have a list of numbers and the numbers are 1 2 3 1 2 3 4 5 and we don't want to

24
00:01:36,930 --> 00:01:43,380
do is I want to apply the square function to every single number in my number list.

25
00:01:43,380 --> 00:01:48,640
But what we can do that is use a for loop but that may take a while and it's a lot of code.

26
00:01:48,930 --> 00:01:54,210
Instead what I want to do is take advantage of that map function so the map function what you end up

27
00:01:54,210 --> 00:02:03,620
doing is you pass in the function that you want to map to every single element or item in this list.

28
00:02:04,020 --> 00:02:06,510
So then we can say minims.

29
00:02:06,600 --> 00:02:08,750
Now notice what happens when I just run it like this.

30
00:02:08,790 --> 00:02:15,940
I get back this kind of cryptic statement which says hey you have a map at this location on your computer

31
00:02:15,970 --> 00:02:17,700
is the memory location.

32
00:02:17,700 --> 00:02:20,100
This by itself isn't super useful.

33
00:02:20,100 --> 00:02:31,650
What we need to do is actually iterate through this so I can do now is say for item in map print item

34
00:02:32,120 --> 00:02:38,280
and what this does is it generates applying this square function to every single item in this list.

35
00:02:38,280 --> 00:02:43,800
So one to the power of two is one and two the power to four and three square it is nine Foursquare to

36
00:02:43,800 --> 00:02:47,370
16 five squares 25 and so on.

37
00:02:47,370 --> 00:02:52,350
So that's one way to quickly iterate through this in other way you may want uses is if you actually

38
00:02:52,350 --> 00:03:00,540
want the list back you can call the built in list function on your map and then pasand square and my

39
00:03:00,620 --> 00:03:02,060
is here.

40
00:03:02,130 --> 00:03:04,120
And then when you run this you get back to the actual list.

41
00:03:04,160 --> 00:03:12,490
1 4 9 16 25 and these functions can actually be a lot more complex than just this simple square function.

42
00:03:12,520 --> 00:03:19,800
So it's really a function that works of strings will call this function splicer and it takes a string

43
00:03:20,070 --> 00:03:26,520
say it takes in my string and what it's going to do is it's going to say if the length of my string

44
00:03:27,350 --> 00:03:29,960
2 is equal to zero.

45
00:03:30,240 --> 00:03:34,530
Well that means it has an even number of characters in that string because the remainder after dividing

46
00:03:34,530 --> 00:03:36,260
by two is equal to zero.

47
00:03:36,690 --> 00:03:42,660
And in that case we're going to return the word even or the string even.

48
00:03:42,660 --> 00:03:49,320
Else we're actually just going to return the first character of that string will say my string 0.

49
00:03:49,320 --> 00:03:54,750
So this is a little more complex of a function than we saw up here but it's going to work just the same

50
00:03:54,750 --> 00:03:55,240
way.

51
00:03:55,440 --> 00:04:01,040
When we map it so it is create a list of names we'll say and the

52
00:04:03,630 --> 00:04:14,190
Yves and let's say Sally and then we're going to after we call list map our splicer function to our

53
00:04:14,190 --> 00:04:15,090
names.

54
00:04:15,450 --> 00:04:21,270
We run this and now we get back even for names that have an even number of letters in them and we only

55
00:04:21,270 --> 00:04:26,610
get back first letters for names that have an odd number of letters to them and they can see the flexibility

56
00:04:26,620 --> 00:04:32,340
you can pretty much do any function you want as long as it's going to successfully taken the argument

57
00:04:32,370 --> 00:04:38,020
or parameter that you pass with this list or with any iterable object.

58
00:04:38,040 --> 00:04:40,240
So that's how you can use the map function.

59
00:04:40,380 --> 00:04:42,210
Something that I want to make really clear here.

60
00:04:42,240 --> 00:04:43,790
When you're using the map function.

61
00:04:43,860 --> 00:04:50,100
Notice how I'm passing and square and how I'm passing in splicer I'm actually not calling them to execute

62
00:04:50,340 --> 00:04:56,710
inside of this map because map by itself is later on going to execute them.

63
00:04:56,730 --> 00:04:59,020
So that means when you pass in the function here it's a map.

64
00:04:59,130 --> 00:05:01,940
You do not add in the open and close parentheses.

65
00:05:01,980 --> 00:05:05,760
Instead you just passen the function itself as an argument.

66
00:05:05,850 --> 00:05:08,000
And that's a really important detail to note here.

67
00:05:08,020 --> 00:05:11,690
Otherwise if you do this you'll end up getting some sort of type error.

68
00:05:11,700 --> 00:05:17,010
It's going to say hey you're missing one or positional argument or it's going to say something like

69
00:05:17,370 --> 00:05:21,110
hey we're not able to apply splicer calls to names.

70
00:05:21,150 --> 00:05:24,270
And that's because we just want to passen the function itself.

71
00:05:24,270 --> 00:05:26,240
We don't want to execute the function will it.

72
00:05:26,250 --> 00:05:28,530
Map do that for us.

73
00:05:28,530 --> 00:05:29,970
That's the map function.

74
00:05:29,970 --> 00:05:35,370
And now to learn about the filter function the filter function returns an iterator yielding those items

75
00:05:35,370 --> 00:05:39,650
of the iterable for which when you pass in the item into the function it's true.

76
00:05:39,990 --> 00:05:44,610
And that just means you need to filter by a function that returns either true or false.

77
00:05:44,610 --> 00:05:47,370
Let's create a function that returns either true or false.

78
00:05:49,100 --> 00:05:55,880
Well create a function call check even and it takes in a number and all this function as it returns

79
00:05:55,880 --> 00:05:56,180
back.

80
00:05:56,180 --> 00:06:01,050
Whether or not this is an even number so we will say as we've seen many times before.

81
00:06:01,130 --> 00:06:05,010
Return them Mattu equal to zero.

82
00:06:05,330 --> 00:06:10,790
Because when you divide this number by 2 the remainder if it's zero that that means that it's even if

83
00:06:10,790 --> 00:06:12,250
not they will return false.

84
00:06:12,250 --> 00:06:17,170
And it's an odd number that I'm going to create a list of my numbers.

85
00:06:18,190 --> 00:06:22,730
Now will it be one two three four five six.

86
00:06:22,740 --> 00:06:23,630
Now let's imagine.

87
00:06:23,670 --> 00:06:29,370
I only want to grab even numbers from this list or I only want to grab numbers that pass some sort of

88
00:06:29,370 --> 00:06:36,190
condition based off a function that I previously find what I could do is use the filter function and

89
00:06:36,230 --> 00:06:43,350
it's very similar to what we just saw with map but what map is map applied this function to every element

90
00:06:43,800 --> 00:06:45,940
in that list filter instead.

91
00:06:45,960 --> 00:06:47,070
What it's going to do.

92
00:06:47,190 --> 00:06:50,620
It's going to filter based off this function's condition.

93
00:06:50,820 --> 00:06:55,620
So this function when you use the filter has to return a boolean has to return either true or false.

94
00:06:55,680 --> 00:07:02,260
And then we're going to do here is say apply check even to my norms.

95
00:07:02,620 --> 00:07:07,890
And again just like last time if you run this by itself it's going to say hey you have a filter object.

96
00:07:07,900 --> 00:07:14,050
At this point in your memory instead you want to do is either transform it to a list so that we get

97
00:07:14,050 --> 00:07:22,000
back only the even numbers here or we can iterate through it so we can say for n in filter apply check

98
00:07:22,030 --> 00:07:29,130
even to my name's printout and we see here we've printed out 2 4 6.

99
00:07:29,190 --> 00:07:35,100
So you can either transform it to a list or iterate through the results on that the results that this

100
00:07:35,100 --> 00:07:36,850
filter function is generating.

101
00:07:36,990 --> 00:07:41,670
Now that we've learned about the map function and the filter function we can learn about lambda expressions

102
00:07:42,030 --> 00:07:45,150
and see in context why they're really useful.

103
00:07:45,180 --> 00:07:50,850
So we're going to do is show you my favorite way of explaining the lambda expression and that's by converting

104
00:07:50,880 --> 00:07:57,600
a function step by step into a land expression and then we'll actually explain more formally what the

105
00:07:57,600 --> 00:07:58,520
expression is.

106
00:07:58,530 --> 00:08:01,180
So I'm going to take that square function we did earlier.

107
00:08:02,960 --> 00:08:04,860
Or at least a really similar function to it.

108
00:08:08,930 --> 00:08:10,020
And define it here.

109
00:08:10,050 --> 00:08:13,230
So notice how I have the square now.

110
00:08:13,460 --> 00:08:18,250
And I say the result is equal to numb to the power of two return result.

111
00:08:18,250 --> 00:08:21,940
Now if I call square let's make sure I spell that right.

112
00:08:22,150 --> 00:08:29,320
If I call Square on a number I get back to square one I'm goin to do now is slowly step by step.

113
00:08:29,320 --> 00:08:31,500
Turn this into an expression.

114
00:08:31,840 --> 00:08:34,130
So first off let's see how we can simplify this.

115
00:08:34,150 --> 00:08:38,860
Right now I'm assigning result equal to number the power of two and then returning that result.

116
00:08:38,860 --> 00:08:45,720
What it could do instead is just get rid of this assignment and return them to the power of two.

117
00:08:45,860 --> 00:08:48,330
And if I run this it's the exact same thing.

118
00:08:48,910 --> 00:08:56,340
And but I could also do is actually write this all on one line and typically you wouldn't do this because

119
00:08:56,340 --> 00:09:01,560
it's bad styling and not by convention but Python will actually complain here and it will let you run

120
00:09:01,560 --> 00:09:04,070
this all on one line.

121
00:09:04,080 --> 00:09:07,540
Now we're almost in the form of a land expression.

122
00:09:07,620 --> 00:09:11,750
In fact we can easily convert this into the expression through the following manner.

123
00:09:12,100 --> 00:09:17,920
Eelam expression is also known as an anonymous function and the reason for that is because it's some

124
00:09:17,940 --> 00:09:23,370
functionality that you intent only use one time because of that we don't actually give it a name nor

125
00:09:23,370 --> 00:09:24,950
do we use the keyword.

126
00:09:25,080 --> 00:09:29,890
So instead of a name or the DFA keyword we replace that with the keyword lambda.

127
00:09:29,970 --> 00:09:34,460
So I'm going to get rid of this and just say lambda here.

128
00:09:36,370 --> 00:09:45,550
And then we say lambda numb colon and the return keyword is basically we get rid of that because it's

129
00:09:45,550 --> 00:09:49,180
assumed that we're going to return whatever's on the other side of this colon.

130
00:09:49,480 --> 00:09:50,190
And there we have it.

131
00:09:50,200 --> 00:09:57,350
This is now lambda expression and it could if it wanted to assign it to square.

132
00:09:57,820 --> 00:09:58,510
Run that.

133
00:09:58,810 --> 00:10:00,730
And then I see Square still working.

134
00:10:00,730 --> 00:10:06,970
So let me make this to another number we see fives the power of two is 25.

135
00:10:06,970 --> 00:10:10,650
Now I just mentioned that lambdas are anonymous functions and you typically don't name them.

136
00:10:10,660 --> 00:10:16,020
So a lot of times you're not really going to be doing the squares equal to some land expression.

137
00:10:16,020 --> 00:10:21,630
Instead you're going to be using it in conjunction with other functions such as map and filter.

138
00:10:21,630 --> 00:10:29,660
So look up here and we see that earlier when I was using map I was passing in this square function and

139
00:10:29,870 --> 00:10:32,210
applying it to every item in the list.

140
00:10:32,210 --> 00:10:37,460
My name's now that required me to actually define square Fula here.

141
00:10:37,460 --> 00:10:40,970
But what if I only intend to use this one time.

142
00:10:40,970 --> 00:10:47,240
Do I want to take up space by defining this function here and then having to call it here what I could

143
00:10:47,240 --> 00:10:52,510
do instead is just call Alynda expression and this is where land expressions really begin to shine.

144
00:10:52,520 --> 00:10:53,980
And this is why you would use them.

145
00:10:54,440 --> 00:10:59,660
I could just say map Lamda numb colon.

146
00:10:59,670 --> 00:11:01,400
Number two the power of two.

147
00:11:01,870 --> 00:11:08,770
And then apply that to a list of numbers such as my numbers and then cast that to a list and we get

148
00:11:08,770 --> 00:11:13,660
back the same thing here every number in the my numbers list has now been squared.

149
00:11:13,720 --> 00:11:18,280
Now that we've seen how we can use lambda with the map function Let's quickly check how we can use the

150
00:11:18,280 --> 00:11:23,760
filter function so up here we saw that we were filtering using this check even function.

151
00:11:23,770 --> 00:11:27,010
So let's start by converting this check even into an expression

152
00:11:30,390 --> 00:11:31,250
we'll come over here.

153
00:11:31,260 --> 00:11:34,800
We'll remove the name and the DPF and replace that all with the lamp.

154
00:11:34,800 --> 00:11:36,120
The keyword.

155
00:11:36,490 --> 00:11:41,030
Remove that print sees and then get rid of return here and put it all in one line.

156
00:11:41,220 --> 00:11:47,400
And now we have Ahlam expression then we're going to pass this filter along with the last numbers we

157
00:11:47,400 --> 00:11:50,740
want to filter and to get the results.

158
00:11:50,910 --> 00:11:57,890
We'll pass this into list and then we run that back we'll get back to 4:6 only the even numbers inland

159
00:11:57,960 --> 00:12:00,320
expressions can be used for a variety of things.

160
00:12:00,480 --> 00:12:04,260
Let's imagine that you want to grab the first character of a string.

161
00:12:04,260 --> 00:12:06,300
So let's create a list here.

162
00:12:07,480 --> 00:12:09,970
Well we actually have the names list.

163
00:12:09,970 --> 00:12:12,520
So we have Andy Eve Sally and we only want to grab.

164
00:12:12,790 --> 00:12:24,120
Let's say these first letters what I could do is map lambda and I can take the incoming name and I want

165
00:12:24,120 --> 00:12:30,900
to return back only the first character index zero of names.

166
00:12:31,080 --> 00:12:32,580
And you can call this whatever you want.

167
00:12:32,580 --> 00:12:39,150
Doesn't have to be name it could be X here or any other variable just like for any normal function and

168
00:12:39,150 --> 00:12:41,880
list passes to a list so we can see the results.

169
00:12:42,030 --> 00:12:43,810
And there we go a s.

170
00:12:43,830 --> 00:12:46,840
Now let's imagine I wanted to reverse the actual names.

171
00:12:46,980 --> 00:12:52,350
What I could do is say colon colon negative one and then that will reverse the names just a slicing

172
00:12:52,350 --> 00:12:56,660
before you can even passen multiple arguments into lambda expression.

173
00:12:56,670 --> 00:13:02,910
However keep in mind that not every function that we've seen up here for example this check even or

174
00:13:02,910 --> 00:13:05,110
splicer or square.

175
00:13:05,160 --> 00:13:09,440
Every single complex function is going to be directly translated to lambda expression.

176
00:13:09,630 --> 00:13:13,980
So you should really only use lambda expressions when you can still easily read it if you're to come

177
00:13:13,980 --> 00:13:15,440
back to your code later.

178
00:13:15,510 --> 00:13:19,890
And as you get more and more experience of Python you'll be able to quickly realize what a lambda expression

179
00:13:19,950 --> 00:13:20,900
is doing.

180
00:13:21,000 --> 00:13:23,590
But in the beginning it's going to be a bit tricky at first.

181
00:13:23,730 --> 00:13:28,740
So if you ever struggle of trying to convert a normal function to the land expression just tried using

182
00:13:28,740 --> 00:13:30,390
the normal function instead.

183
00:13:30,780 --> 00:13:31,150
All right.

184
00:13:31,200 --> 00:13:34,080
That's the basics of map filter lambda expressions.

185
00:13:34,080 --> 00:13:35,520
We'll see you at the next lecture.
