1
00:00:05,530 --> 00:00:07,230
Welcome back everyone in this lecture.

2
00:00:07,240 --> 00:00:11,910
We're going to be discussing some function practice problems learning about Python.

3
00:00:11,920 --> 00:00:16,090
Functions is going to increase your general Python skills exponentially.

4
00:00:16,120 --> 00:00:21,100
However this also means that the difficulties of problems the canal solve is also going to increase

5
00:00:21,100 --> 00:00:22,150
drastically.

6
00:00:22,180 --> 00:00:26,730
So this is where as a beginner in Python you're going to make a big leap in your abilities.

7
00:00:27,770 --> 00:00:32,580
So in order to get comfortable to sleep we're going to give you some practice problems that apply to

8
00:00:32,640 --> 00:00:36,820
get some practice in converting general problem statements into Python code.

9
00:00:36,900 --> 00:00:42,240
Previously a lot of your assessment test exercises have specifically told you what to do either to use

10
00:00:42,240 --> 00:00:46,590
a for loop or to use list comprehension or are you some sort of if statement.

11
00:00:46,590 --> 00:00:50,880
Now we're just going to give you a general problem to solve and then you're going to have to decide

12
00:00:50,970 --> 00:00:52,380
what's the best way to solve it.

13
00:00:52,440 --> 00:00:55,650
And a lot of times there's multiple ways of solving this exercise.

14
00:00:55,680 --> 00:00:59,610
So we're going to go through a series of function practice exercises and then after this lecture we're

15
00:00:59,610 --> 00:01:01,980
going to go through the solutions.

16
00:01:02,220 --> 00:01:04,080
Now there are two options for this material.

17
00:01:04,140 --> 00:01:08,760
You can either try out the exercises yourself and then go through the solutions or you can treat the

18
00:01:08,760 --> 00:01:12,070
solutions as a CO-2 long lecture for more guided practice.

19
00:01:12,270 --> 00:01:16,500
So if you're still feeling a little bit uneasy of Python functions you can go ahead and jump to the

20
00:01:16,500 --> 00:01:20,000
next lecture and just code along with us for the solutions.

21
00:01:20,160 --> 00:01:24,720
Later on in the same section of the course we're going to have more function practice problems in case

22
00:01:24,720 --> 00:01:26,470
you want to try out even more.

23
00:01:26,700 --> 00:01:30,900
OK so let's go to the notebook to show you a quick overview of what we expect.

24
00:01:30,900 --> 00:01:35,850
All right so here at the page for all the notebooks come over to methods and functions.

25
00:01:35,850 --> 00:01:40,500
Click on this folder and you'll see the methods notebook and the functions notebook that relate to the

26
00:01:40,500 --> 00:01:45,270
lectures we just went over and then we have functioned practice exercises as well as function practice

27
00:01:45,330 --> 00:01:49,800
exercise solutions which are going to go over in the next lecture.

28
00:01:49,800 --> 00:01:53,610
They also notice that there's functions methods and homework and homework solutions.

29
00:01:53,610 --> 00:01:56,850
Those are going to be for later on for additional practice.

30
00:01:56,880 --> 00:02:00,500
Right now let's check out the function practice exercises.

31
00:02:00,540 --> 00:02:06,790
So if we look on that note book and open it we get back this and these problems are arranged in increase

32
00:02:06,790 --> 00:02:11,460
in difficulty so we started some warm up problems and these can be solved using some basic comparison

33
00:02:11,460 --> 00:02:13,570
operators and some basic methods.

34
00:02:13,690 --> 00:02:18,420
And then we have these level 1 and these may involve If then conditional statements and some other simple

35
00:02:18,420 --> 00:02:23,490
method calls and then Level 2 may require iterating over sequences usually of some kind of loop like

36
00:02:23,490 --> 00:02:24,810
a WHILE loop or for a loop.

37
00:02:24,990 --> 00:02:29,360
And then we have challenging problems and these takes some creativity solves so they're much harder.

38
00:02:29,430 --> 00:02:33,450
And you can kind of treat these as bonus questions so we don't expect you to solve these challenging

39
00:02:33,450 --> 00:02:35,670
ones right away.

40
00:02:35,670 --> 00:02:41,670
So you notice throughout the questions basically there's the section and some explanation as a word

41
00:02:41,670 --> 00:02:45,720
problem or the question and then an example of what we expect it to look like.

42
00:02:45,960 --> 00:02:48,710
So let me show you this example here it is lesser of two evens.

43
00:02:48,720 --> 00:02:54,540
We want you to write a function that returns the lesser of two given numbers if both numbers are even

44
00:02:54,890 --> 00:02:58,420
the returns the greater one if one or both numbers are odd.

45
00:02:58,440 --> 00:03:00,930
So we can see here both numbers or even two and four.

46
00:03:00,930 --> 00:03:03,270
Even so returns the lesser one.

47
00:03:03,330 --> 00:03:06,540
However here not both numbers or even two in five.

48
00:03:06,540 --> 00:03:08,050
So it returns Degrader 1.

49
00:03:08,250 --> 00:03:09,780
So now I have to think of a couple of things.

50
00:03:09,780 --> 00:03:13,260
How do we convert these word problems into Python code.

51
00:03:13,290 --> 00:03:15,620
How do we check if something is even or odd.

52
00:03:15,630 --> 00:03:19,560
And we've already talked about the maade operator with the percent sign that check things are even or

53
00:03:19,560 --> 00:03:24,700
odd and you want to learn about well how do I convert the sort of if statements into actual Python if

54
00:03:24,700 --> 00:03:25,830
statements.

55
00:03:25,830 --> 00:03:31,900
So that's the idea we want you to convert these word problems into solutions using Python functions.

56
00:03:32,100 --> 00:03:37,410
And we have examples here of what the function calls going to look like what you should pass in an example

57
00:03:37,410 --> 00:03:39,690
of what you should see backout either true or false.

58
00:03:39,780 --> 00:03:45,060
In this case for this animal crackers problem and we've also went ahead and filled out some cells for

59
00:03:45,060 --> 00:03:46,920
you to code in.

60
00:03:47,340 --> 00:03:50,790
So that was the warm up problems that we have the level 1 problems they'll go through.

61
00:03:51,030 --> 00:03:53,640
And then we have the level two problems as well.

62
00:03:53,730 --> 00:03:57,990
And then all the way down towards the end we have the challenging problems in kind of treat these as

63
00:03:57,990 --> 00:03:58,440
a bonus.

64
00:03:58,440 --> 00:04:02,790
You don't have to go through them yet but try your best on all these problems.

65
00:04:02,790 --> 00:04:06,900
Now I want to make it very clear that we understand you're still beginning.

66
00:04:06,900 --> 00:04:09,540
And it's basically your first time using Python functions.

67
00:04:09,570 --> 00:04:14,610
So if you're feeling a little uneasy if even these warm up problems are really hard for you go ahead

68
00:04:14,700 --> 00:04:18,200
and jump to the solutions lecture and watch me code through these problems.

69
00:04:18,200 --> 00:04:23,270
It's really going to help you a lot and don't feel like you're losing out on your chance to test yourself

70
00:04:23,310 --> 00:04:27,780
because later on if you come back to this we have functions and methods homework which is going to be

71
00:04:27,780 --> 00:04:32,160
even more function practice for you later on in the course.

72
00:04:32,160 --> 00:04:36,750
So we can go through these functions practice exercises as CO-2 along if you want to so feel free to

73
00:04:36,750 --> 00:04:38,330
jump ahead to solutions.

74
00:04:38,340 --> 00:04:42,510
Treat it as a code along because you're going to have more practice and your homework later on.

75
00:04:42,690 --> 00:04:44,940
But if you want that extra challenge you're feeling up to it.

76
00:04:44,970 --> 00:04:48,750
I would really recommend you download this notebook try your best to go through it and then see how

77
00:04:48,750 --> 00:04:50,650
you did against the solutions.

78
00:04:50,940 --> 00:04:51,270
OK.

79
00:04:51,270 --> 00:04:52,300
We'll see you in the next lecture.

80
00:04:52,350 --> 00:04:54,570
We actually begin the code along through the solutions.
