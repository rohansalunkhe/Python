1
00:00:05,260 --> 00:00:09,880
Welcome back, everyone, to this lecture on the death keyword, which allows us to create a function

2
00:00:09,880 --> 00:00:10,480
of python.

3
00:00:12,110 --> 00:00:17,570
Creating a function in Python actually requires a very specific syntax, including the aforementioned

4
00:00:17,570 --> 00:00:18,290
def keyword.

5
00:00:18,800 --> 00:00:21,710
It also requires correct indentation and proper structure.

6
00:00:22,220 --> 00:00:25,490
Let's begin getting an overview of the Python function structure.

7
00:00:25,700 --> 00:00:26,420
Line by line.

8
00:00:27,560 --> 00:00:32,960
So here's the first line of an example function, and it all starts off with this def keyword essentially

9
00:00:32,960 --> 00:00:34,940
standing for the definition of the function.

10
00:00:35,420 --> 00:00:39,140
And this keyword just tells Python, Hey, what I'm about to write is a function.

11
00:00:39,560 --> 00:00:44,930
So you have D.F. and then you have the name of the function and you decide on the function name.

12
00:00:45,430 --> 00:00:48,050
She noticed this so-called snake casing.

13
00:00:48,980 --> 00:00:53,180
So in general, by convention, Python uses snake casing for the name of the function.

14
00:00:53,570 --> 00:00:58,310
And snake casing just means that you're going to use all lowercase letters, which underscores between

15
00:00:58,310 --> 00:00:58,880
the words.

16
00:00:59,330 --> 00:01:00,530
And this is by convention.

17
00:01:00,560 --> 00:01:05,420
So this means that in general, if you're looking at someone else's python script, you can easily tell

18
00:01:05,450 --> 00:01:08,240
what's a function because of the use of snake casing.

19
00:01:08,840 --> 00:01:14,120
Later on, we'll see that when we learn about object oriented programming, class calls use camel casing.

20
00:01:14,440 --> 00:01:19,790
But for right now in general, by convention, you should use snake casing, all lowercase with underscores

21
00:01:19,790 --> 00:01:20,570
between the words.

22
00:01:20,930 --> 00:01:22,850
It will still work if you don't follow snake casing.

23
00:01:23,090 --> 00:01:27,920
But in order for your code to be readable by other python developers, you should use snake casing.

24
00:01:28,160 --> 00:01:31,670
When naming functions, then you'll notice we have parentheses here.

25
00:01:32,060 --> 00:01:33,530
So the princes come at the end.

26
00:01:33,800 --> 00:01:38,990
And later on we'll see how we can passan arguments or parameters into the function where arguments or

27
00:01:38,990 --> 00:01:44,060
parameters are essentially just words saying take this variable and pass it into the function in order

28
00:01:44,060 --> 00:01:45,800
to work with it within the function.

29
00:01:46,750 --> 00:01:52,150
And then finally, you'll notice a colon, which indicates an upcoming indented block, everything indented

30
00:01:52,240 --> 00:01:53,830
is then inside the function.

31
00:01:55,330 --> 00:02:00,040
So after that comes the optional multi-line string to describe a function.

32
00:02:00,430 --> 00:02:05,860
This is called a dock string and you specify a dock string with triple quotes, which essentially allows

33
00:02:05,860 --> 00:02:07,260
you to write a multi-line comments.

34
00:02:07,810 --> 00:02:09,580
And this code does not get executed.

35
00:02:09,640 --> 00:02:14,050
It's just comment code, essentially, for someone else who's reading this function or for you at a

36
00:02:14,050 --> 00:02:14,560
later date.

37
00:02:14,830 --> 00:02:17,050
That will explain what the function is supposed to do.

38
00:02:18,970 --> 00:02:23,020
And now you start noting that everything inside the function has indented.

39
00:02:25,140 --> 00:02:29,220
And then you're going to write the code that you want to be executed every time you call the function.

40
00:02:29,580 --> 00:02:31,650
So, for example, this is a very simple function.

41
00:02:31,740 --> 00:02:33,240
All it's going to do is print.

42
00:02:33,270 --> 00:02:33,630
Hello.

43
00:02:35,220 --> 00:02:40,530
So if you were to call this function, you would simply say, name a function and this case open close

44
00:02:40,530 --> 00:02:41,100
parentheses.

45
00:02:41,490 --> 00:02:46,200
So the functions executed and then it takes whatever code is inside the function and runs it.

46
00:02:46,380 --> 00:02:47,590
In this case, it will just print.

47
00:02:47,640 --> 00:02:48,030
Hello.

48
00:02:49,770 --> 00:02:51,220
So there's your resulting output.

49
00:02:52,900 --> 00:02:58,450
Now functions can begin to get more complex, such as actually accepting a parameter or argument such

50
00:02:58,450 --> 00:02:59,380
as name here.

51
00:02:59,770 --> 00:03:03,520
So name is just essentially a made up variable that I decided to use.

52
00:03:03,760 --> 00:03:06,010
You can call it whatever you want here, but we'll call it name.

53
00:03:06,340 --> 00:03:10,690
And we're going to do is use this as an argument to be passed by the user.

54
00:03:11,410 --> 00:03:12,490
So notice what happens here.

55
00:03:13,030 --> 00:03:16,510
Here I have my function and it accepts this parameter name.

56
00:03:16,840 --> 00:03:18,370
And what it's going to do is say print.

57
00:03:18,400 --> 00:03:18,820
Hello.

58
00:03:18,850 --> 00:03:19,510
Plus name.

59
00:03:19,870 --> 00:03:21,910
So that essentially means that name should be a string.

60
00:03:22,240 --> 00:03:24,460
And I'm going to concatenate it with hello.

61
00:03:24,940 --> 00:03:28,800
So that means when I call my function here, it says Name a function Hosie.

62
00:03:29,290 --> 00:03:30,610
And then I'll say Hello Hosie.

63
00:03:31,120 --> 00:03:36,250
Which means now I can pass on any string I want in to name a function and it should say hello.

64
00:03:36,340 --> 00:03:37,270
And then that string.

65
00:03:38,930 --> 00:03:44,120
Now, typically, however, we're going to use the return key word to send back the results of the function

66
00:03:44,240 --> 00:03:45,470
instead of just printing it out.

67
00:03:46,160 --> 00:03:51,260
So far, if we take a look at our examples here, we've just been using prints inside of the function

68
00:03:51,260 --> 00:03:55,130
calls like print tallow or print hello, plus name in general.

69
00:03:55,160 --> 00:03:59,390
You're very rarely just going to have a print inside of a function because you might as well just call

70
00:03:59,390 --> 00:04:00,890
prints outside of the function.

71
00:04:01,310 --> 00:04:06,650
Instead, you're going to use what is known as the return keyword and return allows us to assign the

72
00:04:06,680 --> 00:04:08,930
output of the function to a new variable.

73
00:04:10,100 --> 00:04:13,730
Now we're gonna have a much deeper discussion on the return keyword later on in the notebook.

74
00:04:14,030 --> 00:04:14,870
So keep that in mind.

75
00:04:15,230 --> 00:04:19,850
A really common question is what's the difference between print versus return inside of function?

76
00:04:20,210 --> 00:04:23,870
We're gonna clarify that when we actually start coding out these functions within the notebook.

77
00:04:25,380 --> 00:04:27,930
But here, let's just look through a very simple example.

78
00:04:28,620 --> 00:04:33,810
I've created a function called add function and it takes in two arguments or two parameters, number

79
00:04:33,810 --> 00:04:35,040
one and number two.

80
00:04:35,340 --> 00:04:38,220
And keep in mind that you can call these variables whatever you want.

81
00:04:38,550 --> 00:04:40,380
So it's very flexible in the naming scheme.

82
00:04:41,010 --> 00:04:45,440
And then it's going to return the result of num one plus num two.

83
00:04:46,320 --> 00:04:50,760
You should also notice here that return doesn't have any parentheses attached to it, unlike a print

84
00:04:50,760 --> 00:04:51,240
function.

85
00:04:51,780 --> 00:04:56,520
And so return then allows you to save the result to a variable which print does not.

86
00:04:57,180 --> 00:05:02,550
So that means when I call add function one comma two, I'm saying number one is one.

87
00:05:02,850 --> 00:05:03,930
Number two is two.

88
00:05:04,290 --> 00:05:08,040
And so it's going to inside of the function do one plus two which is three.

89
00:05:08,460 --> 00:05:09,810
And it's using return.

90
00:05:10,110 --> 00:05:12,690
I can now save that result to a variable.

91
00:05:13,110 --> 00:05:18,720
So I can save result is equal to whatever add function one comma two returns to me.

92
00:05:19,200 --> 00:05:24,150
So in that case, if I were to later on print the result or use it somewhere else in my code, it's

93
00:05:24,150 --> 00:05:25,170
the number three.

94
00:05:26,770 --> 00:05:29,190
So most functions, as I mentioned, will use return.

95
00:05:29,620 --> 00:05:31,480
Rarely will a function only print.

96
00:05:31,600 --> 00:05:35,200
And we'll show you examples of both use cases in the Jupiter notebook.

97
00:05:36,350 --> 00:05:38,900
OK, let's start creating functions with Python.

98
00:05:39,200 --> 00:05:41,720
I'll see you at the next lecture where we're going to jump straight to a notebook.
