1
00:00:05,840 --> 00:00:07,580
Welcome back everyone in this lecture.

2
00:00:07,580 --> 00:00:11,630
We're going to go over the solutions for the practice problems that were under level 1.

3
00:00:11,630 --> 00:00:14,930
Let's open up that you brought a notebook and continue OK.

4
00:00:14,930 --> 00:00:19,670
The first problem was this old MacDonald problem where we wanted you to write a function that capitalizes

5
00:00:19,970 --> 00:00:24,310
the first four letters of a name and there's two different ways we're going to show you how to do this.

6
00:00:24,320 --> 00:00:27,920
One is if the capitalized method and then the other is the upper method.

7
00:00:27,920 --> 00:00:31,850
We haven't really shown you the capitalized method yet except we added a note here that basically what

8
00:00:31,850 --> 00:00:37,570
capitalized does is it grabs an entire string in other cases the first letter in that string.

9
00:00:37,580 --> 00:00:42,830
So let's do first with the upper and then we'll show you how to do with capitalize.

10
00:00:42,880 --> 00:00:45,180
First we want to grab the first letter.

11
00:00:45,320 --> 00:00:50,890
So I'm going to say first letter is equal to name zero.

12
00:00:51,320 --> 00:00:53,600
And then I want to grab the characters in between.

13
00:00:53,600 --> 00:00:57,590
So we'll call these in-between characters.

14
00:00:57,690 --> 00:01:06,740
And those are going to go from name index one all the way up to let's say Index three and then we want

15
00:01:06,740 --> 00:01:09,070
to grab the fourth letter.

16
00:01:09,380 --> 00:01:10,780
So say fourth letter

17
00:01:13,540 --> 00:01:17,120
is name index three.

18
00:01:17,210 --> 00:01:25,590
And then finally the rest is going to be name from index 4 onwards.

19
00:01:26,000 --> 00:01:34,690
So let's see if this works we're going to just return first letter with in-between with the fourth letter

20
00:01:35,980 --> 00:01:37,510
plus rest.

21
00:01:38,200 --> 00:01:40,420
So let's see if we actually get the same result there.

22
00:01:40,690 --> 00:01:40,980
OK.

23
00:01:40,990 --> 00:01:42,230
So good we get back.

24
00:01:42,230 --> 00:01:48,010
McDonald now want to do is uppercase the first letter and then I want to uppercase the fourth letter

25
00:01:49,790 --> 00:01:50,630
and then we run this.

26
00:01:50,630 --> 00:01:51,290
We get back.

27
00:01:51,290 --> 00:01:57,170
McDONALD So this is how you could do this with uppercase using just indexing and slicing you separated

28
00:01:57,200 --> 00:02:00,780
out uppercase what you need and then concatenate it back together again.

29
00:02:01,040 --> 00:02:05,930
But you can also do this with the capitalized method and the capitalized method is going to like to

30
00:02:05,930 --> 00:02:08,030
do fewer slices.

31
00:02:08,030 --> 00:02:12,070
So what we could do is kind of separated out into two halves.

32
00:02:12,290 --> 00:02:21,300
So say first half of the name is going to be from name from the beginning all the way to index 3.

33
00:02:21,310 --> 00:02:25,680
So it's going to be basically the Mack part and the rest of it.

34
00:02:25,680 --> 00:02:26,480
That second half.

35
00:02:26,500 --> 00:02:34,190
And it's not really an even half I should say probably second part is going to be from three all the

36
00:02:34,190 --> 00:02:40,820
way to the end and then all we could do is the following A can say first half and instead of calling

37
00:02:40,850 --> 00:02:50,010
upper I'm going to call capitalize capitalize and then I will concatenate that with the second half

38
00:02:52,780 --> 00:02:56,830
and capitalize that in our we run this.

39
00:02:56,860 --> 00:02:58,360
We get back the same results.

40
00:02:58,430 --> 00:03:01,480
Say you could use the capitalized method instead of the upper method.

41
00:03:01,600 --> 00:03:06,470
And basically what that does is it grabs two strings this lowercase Mack and this lowercase Donald and

42
00:03:06,550 --> 00:03:12,330
upper cases the first letter or character in those substrings and then concatenate them together.

43
00:03:12,340 --> 00:03:15,820
So it's the old MacDonald shown two different ways.

44
00:03:15,820 --> 00:03:21,570
Next we have Master Yoda where we're given a sentence we want to return a sentence with the words reversed.

45
00:03:21,630 --> 00:03:26,840
So in order to do this we need to do a couple of things first to actually get a list of the words.

46
00:03:26,890 --> 00:03:27,970
So we're already quite familiar with that.

47
00:03:27,970 --> 00:03:33,370
Now we can say wordlist is equal to text dot split.

48
00:03:33,400 --> 00:03:36,460
And now I need to somehow reverse this list.

49
00:03:36,520 --> 00:03:38,890
And we actually know various methods of doing that.

50
00:03:39,340 --> 00:03:40,950
So let me show you what I mean.

51
00:03:41,050 --> 00:03:43,930
I'm going to say text.

52
00:03:43,930 --> 00:03:50,800
Split is the word list and then I'm going to slice the wordless insteps from all the way from the beginning

53
00:03:50,830 --> 00:03:53,120
to the end in negative one.

54
00:03:53,140 --> 00:03:57,600
And that's actually going to be the reversed wordlist.

55
00:03:57,820 --> 00:03:58,200
Whoops.

56
00:03:58,230 --> 00:04:01,650
Let's make sure we don't use a keyword there.

57
00:04:01,740 --> 00:04:08,460
Reverse wordlist and let's return the reverse wordlist and see what happens.

58
00:04:08,460 --> 00:04:11,120
So notice here I get back home am I.

59
00:04:11,130 --> 00:04:13,290
And then with Master Yoda I get back.

60
00:04:13,290 --> 00:04:14,880
Ready are we.

61
00:04:14,880 --> 00:04:21,480
Now we do want an actual string instead of the list itself in order to join this list.

62
00:04:21,480 --> 00:04:24,460
We can actually use a method called join a string.

63
00:04:24,480 --> 00:04:28,500
So let me create a couple of cells here the way you can explain how the join method works.

64
00:04:28,530 --> 00:04:37,930
In case you haven't seen it before let's imagine to have a list here of a string A B and C..

65
00:04:38,020 --> 00:04:44,480
I don't want to join this together so I'll call this my list.

66
00:04:44,660 --> 00:04:47,040
Well you end up doing as you create a string.

67
00:04:48,100 --> 00:04:52,610
You're going to have a string of toothaches and then offer the string you say join.

68
00:04:53,140 --> 00:04:55,000
And then you passen my list.

69
00:04:55,000 --> 00:05:01,150
And what this does is it takes the string you have here and it's going to use it to concatenate as a

70
00:05:01,150 --> 00:05:02,150
fill in between.

71
00:05:02,170 --> 00:05:05,360
Every item in the list.

72
00:05:05,380 --> 00:05:14,320
So if I just have a space here it puts a space between A B and C if I have maybe a bunch of O's then

73
00:05:14,330 --> 00:05:18,980
it's going to put a bunch of rows in between A B and C..

74
00:05:19,010 --> 00:05:23,810
So a lot of times when you want to join things together such as words on a list you can either have

75
00:05:23,810 --> 00:05:29,720
it as an empty string just a blank string to single quotes right next to each other ABC or you can have

76
00:05:29,720 --> 00:05:34,090
a space here and it joins to get the other a space b c.

77
00:05:34,310 --> 00:05:38,060
So in order to actually get this back as a string sentence what it could do is fine.

78
00:05:38,190 --> 00:05:45,860
Instead of just saying reverse wordlist I could say single quote space single quote join the reverse

79
00:05:45,860 --> 00:05:46,970
word list.

80
00:05:47,340 --> 00:05:50,180
And now when I run this I get back the actual string itself.

81
00:05:50,180 --> 00:05:51,170
Home am I.

82
00:05:51,260 --> 00:05:52,970
And then ready are we.

83
00:05:52,970 --> 00:05:54,920
So that's the introduction to the join method.

84
00:05:54,920 --> 00:05:56,330
In case you haven't seen it before.

85
00:05:57,860 --> 00:06:04,640
Finally we're going to discuss the almost there and that was given an integer n return true and is within

86
00:06:04,640 --> 00:06:08,110
1 10 of either 100 or 200.

87
00:06:08,120 --> 00:06:11,400
So the way we can do that is by taking note of abs.

88
00:06:11,510 --> 00:06:15,110
So abs returns the absolute value of a number.

89
00:06:15,110 --> 00:06:17,810
So we're going to return and check for the following.

90
00:06:17,840 --> 00:06:27,600
We want to check if 100 minus and the absolute value of that is less than or equal to 10.

91
00:06:27,740 --> 00:06:31,230
So we want to check that but we also want to check another situation.

92
00:06:31,230 --> 00:06:37,160
So we're going to raptus princes to kind of state that's one thing we're checking or want to check if

93
00:06:37,160 --> 00:06:45,400
the absolute value between 200 and an is less than or equal to 10.

94
00:06:45,600 --> 00:06:47,680
And then finally we can return this.

95
00:06:47,730 --> 00:06:52,540
So we're just checking hey is 100 minus number the absolute value of that.

96
00:06:52,560 --> 00:06:56,220
Basically the difference between that number and 100 is that less equal to 10.

97
00:06:56,370 --> 00:06:59,740
Or is the difference between that number and 200 Lessner equal to 10.

98
00:06:59,850 --> 00:07:00,820
And we run this.

99
00:07:00,890 --> 00:07:02,520
We get back the correct results.

100
00:07:02,520 --> 00:07:03,850
So this is false.

101
00:07:03,850 --> 00:07:06,500
This was true and then this one is also true.

102
00:07:06,940 --> 00:07:07,440
OK.

103
00:07:07,560 --> 00:07:08,640
That's the basics.

104
00:07:08,730 --> 00:07:16,380
Level 1 introduce a couple of things here mainly the joint method and the absolute value function in

105
00:07:16,380 --> 00:07:17,180
the very next lecture.

106
00:07:17,180 --> 00:07:20,010
We're going to go over the level two problems we'll see.
