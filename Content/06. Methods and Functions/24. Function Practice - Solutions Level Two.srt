1
00:00:05,360 --> 00:00:09,680
Welcome back in this lecture we're going to go over the solutions for the level two problems and the

2
00:00:09,680 --> 00:00:11,570
function practice problems notebook.

3
00:00:11,690 --> 00:00:12,700
Let's get started.

4
00:00:12,980 --> 00:00:14,540
All right so here are two problems.

5
00:00:14,540 --> 00:00:20,990
The first problem we had was 5:33 which was given a list of integers return true if this list or array

6
00:00:21,320 --> 00:00:23,690
contains a three next two or three somewhere.

7
00:00:23,720 --> 00:00:26,310
So here we can see three and three are together.

8
00:00:26,420 --> 00:00:30,130
So we say true here the threes aren't connected in any way so we say false.

9
00:00:30,140 --> 00:00:32,000
And again here threes aren't connected together.

10
00:00:32,010 --> 00:00:33,060
So we say false.

11
00:00:33,080 --> 00:00:37,250
So basically at two threes in a row we know to return true.

12
00:00:37,250 --> 00:00:39,820
So do this or do the following logic.

13
00:00:40,280 --> 00:00:49,270
We'll say for I in range from 0 and we actually don't want to go all the way to the end of numbers.

14
00:00:49,360 --> 00:00:55,530
We want to go to the length from the minus one because we want to go up to the second to last digit.

15
00:00:55,550 --> 00:01:01,220
So we can check the very next digit if we went up to the last digit there would be no next digit to

16
00:01:01,220 --> 00:01:10,860
check which is why we're going to say for iron range from 0 to length of numbs minus 1.

17
00:01:10,870 --> 00:01:12,240
So this part of the trick already.

18
00:01:12,670 --> 00:01:32,490
And we can say if numbs of eye is equal to 3 and numbs up plus 1 is equal to 3 then return true.

19
00:01:32,580 --> 00:01:36,100
Otherwise we're going to do is return false.

20
00:01:36,360 --> 00:01:37,880
So and that is what I'm doing here.

21
00:01:37,980 --> 00:01:43,920
I'm going to run through a for loop that goes through every single index position in this list up to

22
00:01:44,070 --> 00:01:47,470
the second to last one and then we're going to do.

23
00:01:47,790 --> 00:01:51,750
Then we're going to do here is check that number and the number right next to it.

24
00:01:51,840 --> 00:01:53,240
So number of.

25
00:01:53,510 --> 00:01:56,190
And the number next to is number of I plus 1.

26
00:01:56,190 --> 00:02:00,840
And if they're both three then we're going to return true if we go through this entire four loop and

27
00:02:00,840 --> 00:02:02,650
we never execute this return statement.

28
00:02:02,730 --> 00:02:06,070
Then we know there are no threes together and will return false.

29
00:02:06,090 --> 00:02:08,270
So let's run this and check it out.

30
00:02:08,310 --> 00:02:11,700
We're going to copy this paste here.

31
00:02:11,820 --> 00:02:12,870
Run that and we get back.

32
00:02:12,870 --> 00:02:13,290
True.

33
00:02:13,320 --> 00:02:14,430
Asharq one of the false ones.

34
00:02:14,430 --> 00:02:16,100
See if it also works.

35
00:02:16,120 --> 00:02:16,920
Put that in.

36
00:02:16,980 --> 00:02:18,500
Run it and we get back false.

37
00:02:18,600 --> 00:02:19,520
Perfect.

38
00:02:19,530 --> 00:02:28,650
Now the other alternative we could do that it doesn't look as nice as this statement is to say if numbs

39
00:02:29,390 --> 00:02:38,430
of I sliced to I plus two is equal to the list 3 three.

40
00:02:38,510 --> 00:02:40,690
This is actually the exact same thing as before.

41
00:02:40,700 --> 00:02:43,760
It just is in my opinion a little less readable.

42
00:02:43,810 --> 00:02:49,110
That is technically a little sleeker because you're just doing one slice and one comparison instead

43
00:02:49,130 --> 00:02:50,130
of two comparisons.

44
00:02:50,300 --> 00:02:56,190
So all this is doing is grabbing slices at a time and comparing them to this sublist three three.

45
00:02:56,210 --> 00:02:56,410
All right.

46
00:02:56,420 --> 00:03:02,020
Let's move on to this problem called Paper Doll.

47
00:03:02,030 --> 00:03:07,760
So for this paper doll problem we wanted to say given a string returned string where for every character

48
00:03:07,760 --> 00:03:10,080
in the original There are now three characters.

49
00:03:10,100 --> 00:03:15,400
So essentially go along and create three of these characters so three H's three E's three L's.

50
00:03:15,410 --> 00:03:18,290
Another set of three L's and then three goes.

51
00:03:18,450 --> 00:03:23,410
And this is actually not so bad once you figure out the trick is to start off an empty string and then

52
00:03:23,410 --> 00:03:25,490
to keep concatenating that string.

53
00:03:25,530 --> 00:03:30,050
So I'm going to say result starts off as an empty string.

54
00:03:31,560 --> 00:03:37,650
And then for every character in that text that I passen I'm going to grab my current result so that

55
00:03:37,650 --> 00:03:42,600
current string and I'm going to add on that character times three.

56
00:03:42,600 --> 00:03:46,390
The other thing it could do is say character plus character plus character.

57
00:03:46,410 --> 00:03:50,580
So times three or three characters concatenated together that's the same thing.

58
00:03:51,180 --> 00:03:55,780
And then at the end of this for loop outside of it I'll just say return result.

59
00:03:55,820 --> 00:03:56,980
And that's actually all you had to do.

60
00:03:57,060 --> 00:04:02,500
So the main trick here is realizing that you can continually concatenate to string.

61
00:04:02,660 --> 00:04:09,020
The next question was called Black Jack which is basically using the blackjack card game as a base for

62
00:04:09,020 --> 00:04:13,700
the given problem statement we were given three integers between 1 and 11.

63
00:04:13,730 --> 00:04:17,930
And if there's some is less than or equal to 21 we just return there some.

64
00:04:17,930 --> 00:04:24,460
However if there are some exceeds 21 and there's an 11 we get a chance to reduce the total sum by 10.

65
00:04:24,800 --> 00:04:30,180
And then finally if that some even after adjustment exceeds 21 we return bust.

66
00:04:30,260 --> 00:04:31,370
So let's check this out.

67
00:04:31,430 --> 00:04:36,670
The first single do is just actually check if there is less than or equal to 21.

68
00:04:36,840 --> 00:04:41,990
And we can do this by saying if some of ABC

69
00:04:44,570 --> 00:04:52,180
is less than or equal to 21 return back that some say some of ABC.

70
00:04:52,550 --> 00:04:55,310
So some is a nice built in function you can use.

71
00:04:55,310 --> 00:04:59,640
Alternatively you could just send a plus b pussy.

72
00:04:59,750 --> 00:05:06,290
So that's the same thing as passing in this list here of a BNC.

73
00:05:06,320 --> 00:05:09,500
So we'll be using some keyword because it's a little more readable.

74
00:05:10,940 --> 00:05:11,140
All right.

75
00:05:11,140 --> 00:05:16,340
The next thing we're going to check is the second case which is where we actually have an 11 there.

76
00:05:16,460 --> 00:05:23,330
So remember there is an 11 we get to reduce the total sum by 10 and then we actually check if it exceeds

77
00:05:23,330 --> 00:05:23,910
or not.

78
00:05:24,140 --> 00:05:30,790
So something you can do is say the following elif.

79
00:05:30,940 --> 00:05:37,830
There's an 11 in the list A B C.

80
00:05:38,030 --> 00:05:50,510
So if there's an 11 in there and the some of these three put together is less or equal to 31 because

81
00:05:50,510 --> 00:05:54,530
remember we got to reduce the total sum by 10 when there's an 11 there.

82
00:05:54,530 --> 00:06:00,600
So basically we get to check it against thirty one this time then we get to return the sum.

83
00:06:00,600 --> 00:06:08,750
So and then we can return the sum of this ABC except we're going to subtract now 10 from this.

84
00:06:08,780 --> 00:06:16,010
So this less than or equal to 31 basically all we're doing is now instead of checking less than 21 because

85
00:06:16,160 --> 00:06:18,910
beforehand we're able to reduce the sum by 10.

86
00:06:18,920 --> 00:06:21,600
That means we get to check against 31.

87
00:06:21,710 --> 00:06:29,520
So the other way to doing this is saying some of ABC minus 10 is less or equal to 21.

88
00:06:29,540 --> 00:06:34,250
So those are the same things and it's up to you which logic you prefer whether you prefer to take the

89
00:06:34,250 --> 00:06:41,000
sum then subtract 10 and then check if it's less under 21 or if you just like to do the check against

90
00:06:41,120 --> 00:06:44,690
31 so that's technically the same thing there.

91
00:06:44,720 --> 00:06:47,750
Finally if that does happen to be the case then this person has busted.

92
00:06:47,770 --> 00:06:50,240
So we return bust.

93
00:06:50,420 --> 00:06:51,500
Now we run this.

94
00:06:51,590 --> 00:06:57,160
We get back 18 bust and then we run that over time we get back 19.

95
00:06:57,630 --> 00:07:01,000
Okay let's go to the next problem which is Summer of 69.

96
00:07:01,160 --> 00:07:06,750
We wanted you to return to some of the numbers in the array except ignore sections of numbers starting

97
00:07:06,750 --> 00:07:09,280
with a 6 and extending to the next 9.

98
00:07:09,320 --> 00:07:14,040
So every 6 will be followed by at least 1 9 and we return to 0 4.

99
00:07:14,060 --> 00:07:16,340
No numbers.

100
00:07:16,380 --> 00:07:17,670
So what does that actually mean.

101
00:07:17,700 --> 00:07:19,780
Well we can see here three different samples here.

102
00:07:19,800 --> 00:07:22,550
We have 1 3 5 7 6 or 9 there.

103
00:07:22,590 --> 00:07:26,520
So return back to some of them which is one plus three plus 5 which is 9.

104
00:07:26,730 --> 00:07:28,750
Here we get back 4 5.

105
00:07:28,920 --> 00:07:31,080
And then we have a 6 7 8 9.

106
00:07:31,260 --> 00:07:34,610
So 6 through 9 here are going to be ignored.

107
00:07:34,680 --> 00:07:36,050
So that's the summer of 69.

108
00:07:36,060 --> 00:07:38,890
We're ignoring it because let's say nothing done there.

109
00:07:38,940 --> 00:07:42,080
So then we just add four plus five and we get back 9.

110
00:07:42,150 --> 00:07:45,000
Here we get to 1 and then we have a 6 and a 9.

111
00:07:45,000 --> 00:07:51,070
So these two numbers plus anything in between gets ignored and then we see a lot of in there and they

112
00:07:51,100 --> 00:07:56,090
go to 14 and we also noted that every six will be followed by at least 1 9.

113
00:07:56,220 --> 00:08:00,180
When you're given in this list you don't need to worry about some sort of weird edge case where you

114
00:08:00,180 --> 00:08:03,890
just have a 6 and then you're waiting for a nine that never comes.

115
00:08:03,890 --> 00:08:05,470
OK so let's get started on this.

116
00:08:05,670 --> 00:08:11,130
The way we're going to do this is start running total we'll say total is equal to zero.

117
00:08:11,540 --> 00:08:17,910
And I'm also going to create a boolean value called AD and and I said that the true first and I will

118
00:08:17,910 --> 00:08:25,320
say for numb in my array or in my list while ad is on.

119
00:08:25,320 --> 00:08:26,550
So while I'm still adding.

120
00:08:26,560 --> 00:08:33,930
Well that's true and we start off half true if the current number is not equal to 6 then I'm going to

121
00:08:33,960 --> 00:08:37,280
take my total and I will add to it.

122
00:08:38,410 --> 00:08:40,540
And then I will break out of here.

123
00:08:42,770 --> 00:08:49,680
Else what I'm going to do is add equal to false and then lined up with this while loop.

124
00:08:49,680 --> 00:09:03,890
What I'll do is set up while not add the other way to say add is not equal to true if no is not equal

125
00:09:03,890 --> 00:09:06,860
to 9 that I'm going to break.

126
00:09:08,350 --> 00:09:18,940
Else I'm going to add that back to true and then break here and then finally after all of this outside

127
00:09:18,940 --> 00:09:20,880
of this for loop I'll return the total.

128
00:09:20,890 --> 00:09:24,130
So let's run this and then we'll break down the explanation.

129
00:09:24,130 --> 00:09:24,330
OK.

130
00:09:24,340 --> 00:09:26,780
So looks like we got nine here that looks good.

131
00:09:26,950 --> 00:09:28,690
We got nine here.

132
00:09:28,690 --> 00:09:29,290
That looks good.

133
00:09:29,290 --> 00:09:30,210
Four and Five together.

134
00:09:30,220 --> 00:09:31,150
Ignoring all this.

135
00:09:31,300 --> 00:09:32,210
And some are sixty nine.

136
00:09:32,210 --> 00:09:36,310
Here we just get two plus one 11 which is 14.

137
00:09:36,400 --> 00:09:38,880
So it's actually happening here in our logic.

138
00:09:39,060 --> 00:09:40,090
We have a total which is zero.

139
00:09:40,090 --> 00:09:41,080
That makes sense.

140
00:09:41,080 --> 00:09:44,440
And we have this boolean condition called ADD which is equal to true.

141
00:09:44,650 --> 00:09:48,970
And basically by default I'm going to assume that I'm going to keep adding all the numbers in this list

142
00:09:48,970 --> 00:09:49,910
or in the survey.

143
00:09:50,200 --> 00:09:55,480
So then I going through the array and I'm saying OK for every number in the array if my ADD statement

144
00:09:55,480 --> 00:09:58,720
is true then I'll do the following check.

145
00:09:58,960 --> 00:10:05,380
If the number of those that happen to be 6 I will say Take the total as the number in there and then

146
00:10:05,470 --> 00:10:08,020
I'm going to break out of that loop.

147
00:10:08,260 --> 00:10:13,090
So I'm going to break out of this inclosing loop which is the while loop which brings me back then to

148
00:10:13,240 --> 00:10:18,790
the for loop and I continue along then I like to say else.

149
00:10:19,110 --> 00:10:22,180
So let's say the number was 6 so this does happen to be true.

150
00:10:22,350 --> 00:10:25,080
Then I say add equal to false.

151
00:10:25,080 --> 00:10:28,290
So in that case now I'm just waiting for the nine.

152
00:10:28,410 --> 00:10:30,750
So I'm right now in the summer of 69 and I have to keep going.

153
00:10:30,750 --> 00:10:31,730
Waiting for the nine.

154
00:10:31,980 --> 00:10:39,370
So I'll say it while not add if numb is not equal to 9 then I will break out of this while not add or

155
00:10:39,630 --> 00:10:42,320
else I will say add is equal to true.

156
00:10:42,450 --> 00:10:44,180
And then it will break.

157
00:10:44,270 --> 00:10:44,840
OK.

158
00:10:44,940 --> 00:10:49,170
So this one's definitely a little tricky because we're using breaks here and we have a nested loops

159
00:10:49,170 --> 00:10:53,160
situation we have a for loop with two while loops nested in there.

160
00:10:53,190 --> 00:10:58,470
So the key thing to notice here is that this break is only connected to the While loop it's in and these

161
00:10:58,470 --> 00:11:03,090
breaks are only connected to the wire loops there in which will bring you back to this for a loop so

162
00:11:03,090 --> 00:11:04,810
you can continue on the numbers.

163
00:11:04,920 --> 00:11:07,290
I would say this is definitely probably the hardest problem.

164
00:11:07,320 --> 00:11:13,560
I would say in this entire challenge as far as the ones that aren't specifically challenging problems

165
00:11:13,850 --> 00:11:19,280
but even I think that this summer 69 is probably even harder than the first challenging problem.

166
00:11:19,280 --> 00:11:19,600
OK.

167
00:11:19,620 --> 00:11:23,670
And the very next lecture will finish off our discussion by going through these challenging problems

168
00:11:23,970 --> 00:11:24,970
and their solutions.

169
00:11:24,990 --> 00:11:25,530
We'll see other.
