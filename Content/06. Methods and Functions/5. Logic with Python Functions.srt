1
00:00:05,420 --> 00:00:08,900
Welcome back, everyone, to this lecture on using functions with logic.

2
00:00:09,290 --> 00:00:12,230
We already know a lot of logic statements within Python.

3
00:00:12,500 --> 00:00:16,850
Things like for loops and while loops, if statements and L statements and so on.

4
00:00:17,360 --> 00:00:21,890
So we're going to do is to begin combine that knowledge with the ability of a function.

5
00:00:22,190 --> 00:00:23,030
So let's get started.

6
00:00:23,480 --> 00:00:27,380
We're going to start off with a very simple function that just checks whether or not a single number

7
00:00:27,380 --> 00:00:27,860
is even.

8
00:00:28,220 --> 00:00:30,020
And we'll expand the complexity from there.

9
00:00:30,500 --> 00:00:32,000
Let me head back to the Jupiter notebook.

10
00:00:32,540 --> 00:00:32,740
All right.

11
00:00:32,750 --> 00:00:34,280
Here I am in the Jupiter notebook.

12
00:00:34,670 --> 00:00:41,090
I want to do a quick recall about the model operator, which essentially returns the remainder after

13
00:00:41,090 --> 00:00:48,530
a division, which means if the mod operator is end up equals to zero, then the number is evenly divisible

14
00:00:48,770 --> 00:00:51,050
by whatever the model operation was.

15
00:00:51,500 --> 00:00:59,870
So to give an example of this, if I were to say to Mod two and run this, then the result is zero because

16
00:00:59,870 --> 00:01:02,480
two divided by two is one remaindered zero.

17
00:01:03,050 --> 00:01:09,590
If I were to say three mod two, then three divided by two, two goes at once into three.

18
00:01:09,680 --> 00:01:11,030
And then there's a remainder of one.

19
00:01:11,210 --> 00:01:16,100
So recall that this model operator just returns back the remainder after division.

20
00:01:16,670 --> 00:01:22,950
So for example, if I were to say forty one mod forty tickets a second to figure this out.

21
00:01:23,000 --> 00:01:26,810
Basically it should be remainder one since 40 you can go into forty one.

22
00:01:27,140 --> 00:01:29,330
One full time with a remainder of one.

23
00:01:29,840 --> 00:01:33,080
OK, so how can we use this to check if a number is even.

24
00:01:33,620 --> 00:01:40,460
Well if a number is even such as 20 then I know that that number mod two should be equal to zero.

25
00:01:40,910 --> 00:01:46,370
So essentially any number I put in here, if I provide the model operator and it ends up being equal

26
00:01:46,370 --> 00:01:51,290
to zero, then I know it's even if not, then it's not even.

27
00:01:51,920 --> 00:01:53,870
So for example, I could do a check like this.

28
00:01:54,260 --> 00:02:01,650
I could say 20 Matou and check for equality against zero, because I know if the result of this Mod

29
00:02:01,650 --> 00:02:08,060
two is equal to zero, then I have true it isn't even number otherwise, which is twenty one odd number

30
00:02:08,690 --> 00:02:12,200
and I do mod to set equal to zero then that's false.

31
00:02:12,680 --> 00:02:19,070
OK, so let's construct this into a function now where we can place any number here instead of having

32
00:02:19,070 --> 00:02:21,050
to manually type in 20 and 21.

33
00:02:22,480 --> 00:02:24,010
We start off with the key word, the F.

34
00:02:24,780 --> 00:02:26,260
Call your function whatever you want.

35
00:02:26,410 --> 00:02:27,700
I will call mine, even check.

36
00:02:28,030 --> 00:02:29,320
Notice the snake casing here.

37
00:02:30,040 --> 00:02:31,750
We have the passan in arguments.

38
00:02:32,050 --> 00:02:33,160
I will call the argument.

39
00:02:33,580 --> 00:02:34,020
No.

40
00:02:35,070 --> 00:02:37,060
This is where you have flexibility and what you want to call it.

41
00:02:37,930 --> 00:02:39,670
And then what you need to do is inside of this.

42
00:02:40,970 --> 00:02:43,520
I need to say the number Matou.

43
00:02:44,580 --> 00:02:46,050
Equals equals zero.

44
00:02:46,860 --> 00:02:53,740
And so what this is going to do is if I say something like result is equal to no more two equals equal

45
00:02:53,740 --> 00:02:54,120
zero.

46
00:02:54,840 --> 00:02:56,390
I could return the results.

47
00:02:57,000 --> 00:02:59,820
Run this and then I can have even check.

48
00:03:00,990 --> 00:03:01,710
And so even check.

49
00:03:01,740 --> 00:03:05,610
Twenty twenty is true because this is true.

50
00:03:05,640 --> 00:03:06,720
So the result is true.

51
00:03:07,290 --> 00:03:10,850
And then I can say even check on twenty one returns.

52
00:03:10,860 --> 00:03:11,160
False.

53
00:03:11,190 --> 00:03:14,010
Twenty one is not even now for beginners.

54
00:03:14,070 --> 00:03:17,340
It's really common to separate things out into multiple lines like this.

55
00:03:17,670 --> 00:03:21,700
But keep in mind, I don't actually need to save this as a result parameter.

56
00:03:22,110 --> 00:03:24,000
Instead I can directly return.

57
00:03:24,570 --> 00:03:26,580
No more two is equal to zero.

58
00:03:27,330 --> 00:03:30,690
So if I base that in, this is actually the exact same thing.

59
00:03:31,380 --> 00:03:33,900
So if I run this again, I get the same results.

60
00:03:34,260 --> 00:03:39,360
So again, it's really common for beginners to kind of duplicate lines when really you can just return

61
00:03:39,390 --> 00:03:41,400
something that is the bully check.

62
00:03:42,210 --> 00:03:48,330
So we have a function that can check if a single number is even and then it returns back boolean true

63
00:03:48,330 --> 00:03:50,130
or false, whether or not that's even.

64
00:03:50,790 --> 00:03:53,700
Let's go ahead and check inside of a list.

65
00:03:55,400 --> 00:04:01,280
So what I'm going to do here is I'm going to start off a new function and this function is going to

66
00:04:02,090 --> 00:04:09,760
return true if any number is even inside iList.

67
00:04:11,080 --> 00:04:11,320
OK.

68
00:04:11,600 --> 00:04:16,610
So, again, there just has to be a single even number inside of a list and the list can be as long

69
00:04:16,610 --> 00:04:18,320
as you want and it's going to return.

70
00:04:18,320 --> 00:04:20,030
True, if that happens to be the case.

71
00:04:20,510 --> 00:04:22,910
Otherwise, it will not return anything.

72
00:04:24,170 --> 00:04:28,910
So I'll say D.F. and I will call this now check even list.

73
00:04:29,630 --> 00:04:33,500
It takes in a parameter and others parameter is going to be assumed to be a list.

74
00:04:34,430 --> 00:04:35,840
So we'll say no list.

75
00:04:37,580 --> 00:04:40,290
And what I need to do is I need to loop through each number.

76
00:04:40,320 --> 00:04:43,650
So we're going to start adding in logic that we already learned about like a for loop.

77
00:04:44,130 --> 00:04:45,150
So I can say the following.

78
00:04:45,900 --> 00:04:52,260
For every number in the NUM list and recall number, this is going to be passed in by the user.

79
00:04:53,070 --> 00:04:58,110
I'm going to have another call in here and notice how now we have different layers of indentation.

80
00:04:58,590 --> 00:05:00,960
Have this first one, which is everything inside the function.

81
00:05:01,380 --> 00:05:05,010
And I'm going to have this second one, which is everything inside this for loop.

82
00:05:05,640 --> 00:05:08,580
So for number in NUM list.

83
00:05:09,060 --> 00:05:12,120
First, let me check if I get a so-called hit on an even number.

84
00:05:13,020 --> 00:05:17,350
So if that number, Montu, is equal to zero.

85
00:05:17,940 --> 00:05:23,250
Well, I'm technically already done since I'm just checking if any number is within a list.

86
00:05:23,580 --> 00:05:27,240
Then the very first time that I hit an even number, I can pretty much return.

87
00:05:27,240 --> 00:05:27,510
True.

88
00:05:28,320 --> 00:05:31,360
So I will say, oops, forgot my call in there if.

89
00:05:31,730 --> 00:05:35,100
No number Matou is equal to zero, I'll return.

90
00:05:36,880 --> 00:05:37,240
True.

91
00:05:38,020 --> 00:05:44,310
So note here that I'm going to go through every number on the list and the very first time I get no

92
00:05:44,350 --> 00:05:45,910
more two is equal to zero.

93
00:05:46,360 --> 00:05:47,860
Then I go ahead and return.

94
00:05:47,860 --> 00:05:48,160
True.

95
00:05:49,330 --> 00:05:56,140
And what I'm going to do is say else and I'm going to use the pass keyword and pass just means don't

96
00:05:56,140 --> 00:05:56,920
do anything.

97
00:05:58,910 --> 00:06:00,950
So let's go ahead and look at the logic here.

98
00:06:01,130 --> 00:06:06,020
I pacien this list of numbers, I start going through every number in the number list.

99
00:06:06,590 --> 00:06:10,880
If I get an even number by saying, if true, then I go ahead and return.

100
00:06:11,030 --> 00:06:12,950
True here else.

101
00:06:13,040 --> 00:06:14,600
So let's say this is an odd number.

102
00:06:14,930 --> 00:06:15,830
I'm just going to pass.

103
00:06:15,890 --> 00:06:17,060
I'm not going to do anything.

104
00:06:17,540 --> 00:06:20,480
So notice here, return is actually in the middle of the function.

105
00:06:20,570 --> 00:06:22,160
It doesn't always have to go at the end.

106
00:06:22,550 --> 00:06:26,930
And in fact, we'll show you in just a second how you can have multiple returns inside of a function.

107
00:06:27,860 --> 00:06:33,200
So let's first check that this actually works when they say check even list, and let's pass on a list

108
00:06:33,380 --> 00:06:34,880
that has all odd numbers.

109
00:06:36,020 --> 00:06:37,640
Notice, nothing is returned.

110
00:06:39,530 --> 00:06:42,320
Now, let's say check even list that has.

111
00:06:43,710 --> 00:06:47,400
To even numbers in it and it returns back, true, there isn't even number there.

112
00:06:48,150 --> 00:06:51,120
And let's do a couple of cases just to make sure this is working.

113
00:06:51,570 --> 00:06:55,650
Let's go ahead and have the first number be even it returns.

114
00:06:55,650 --> 00:06:56,040
True.

115
00:06:56,850 --> 00:06:59,970
And let's go ahead and have the last number be even.

116
00:07:01,950 --> 00:07:03,120
And that also returns true.

117
00:07:03,580 --> 00:07:04,950
OK, so it looks like it's working.

118
00:07:05,370 --> 00:07:08,760
So what it's doing is just looping through these numbers until it finds an even one.

119
00:07:08,850 --> 00:07:10,880
And then it just breaks out of the function and returns.

120
00:07:10,890 --> 00:07:11,160
True.

121
00:07:11,670 --> 00:07:16,800
And in fact, you can think of this return statement as essentially going to break off the entire function

122
00:07:17,130 --> 00:07:18,030
and end the function.

123
00:07:18,030 --> 00:07:19,740
Call once you call, return.

124
00:07:19,800 --> 00:07:20,190
That's it.

125
00:07:20,220 --> 00:07:21,000
The functions over.

126
00:07:21,030 --> 00:07:22,290
It's not being run anymore.

127
00:07:22,620 --> 00:07:25,680
So currently, our function doesn't return anything.

128
00:07:26,070 --> 00:07:30,570
If there is no even number in the list, let's say we actually wanted to return false.

129
00:07:31,080 --> 00:07:32,040
How do we fix this?

130
00:07:32,550 --> 00:07:36,540
A super common mistake that a beginner would make is they would simply change.

131
00:07:36,600 --> 00:07:39,150
Else here to return false.

132
00:07:39,330 --> 00:07:41,130
However, this is wrong.

133
00:07:41,610 --> 00:07:42,600
Why is this wrong?

134
00:07:43,110 --> 00:07:47,520
Well, I just mentioned you can think of return as essentially breaking out of this function.

135
00:07:48,000 --> 00:07:51,890
So why is it wrong to return false here when it was correct to put.

136
00:07:51,930 --> 00:07:52,770
Return true.

137
00:07:53,400 --> 00:07:58,710
Often beginners make the mistake that all the return statements should have the same level of indentation.

138
00:07:58,950 --> 00:08:02,250
But that's actually not true because look what's going to happen here.

139
00:08:02,670 --> 00:08:08,220
We're gonna say for number and number list and then the very first time that a number is not even,

140
00:08:08,520 --> 00:08:09,420
we'll say, okay.

141
00:08:09,600 --> 00:08:11,310
In other words, just return false.

142
00:08:11,640 --> 00:08:17,640
Essentially saying that this if else statement is only going to be able to check one number because

143
00:08:17,640 --> 00:08:20,430
both conditions will immediately return something.

144
00:08:21,000 --> 00:08:25,860
So if I was actually run this, let's go ahead and run this on.

145
00:08:26,220 --> 00:08:27,340
Let's say one three five.

146
00:08:27,960 --> 00:08:28,840
So that return false.

147
00:08:28,860 --> 00:08:29,370
Correctly.

148
00:08:29,640 --> 00:08:31,260
But now it's put in a two there.

149
00:08:31,590 --> 00:08:32,190
It should return.

150
00:08:32,190 --> 00:08:32,580
True.

151
00:08:32,970 --> 00:08:38,940
But notice it returns false because it's essentially only checking the first number since there is a

152
00:08:38,940 --> 00:08:42,360
return call on either condition that the first number has.

153
00:08:42,780 --> 00:08:45,600
So that means this return should actually not be here.

154
00:08:46,230 --> 00:08:47,580
In fact, why should it be?

155
00:08:47,880 --> 00:08:53,220
Well, I want to make sure I've already checked every single number and whether or not it's even by

156
00:08:53,220 --> 00:08:55,110
the time I return, false.

157
00:08:55,320 --> 00:09:02,040
So the return false should actually be an indentation with the for loop here.

158
00:09:02,520 --> 00:09:05,930
So what this says is go ahead and complete the entire for loop.

159
00:09:06,330 --> 00:09:11,040
If this is completed and we never broke out of the function with this return statement, then go ahead

160
00:09:11,250 --> 00:09:11,940
and then return.

161
00:09:12,000 --> 00:09:12,540
False.

162
00:09:13,110 --> 00:09:15,600
So let's go ahead and run this and see if that works.

163
00:09:15,750 --> 00:09:18,480
So there is one, two, five returns.

164
00:09:18,480 --> 00:09:18,900
True.

165
00:09:19,500 --> 00:09:23,640
If I change this to one three five now, it correctly returns false.

166
00:09:24,240 --> 00:09:29,490
Again, a super common mistake beginners make is they think all the returns have to be indented together.

167
00:09:29,850 --> 00:09:33,300
But we can see we really need to focus on the logic of what's happening here.

168
00:09:33,660 --> 00:09:38,580
We want to loop through all the numbers first, and we can use these pass statements as placeholders

169
00:09:38,820 --> 00:09:40,710
to essentially say don't do anything.

170
00:09:40,980 --> 00:09:46,590
And they'll be very useful when you have a function that needs or requires multiple return statements.

171
00:09:48,510 --> 00:09:51,180
Let's expand this one more level of complexity.

172
00:09:51,600 --> 00:09:55,620
Let's imagine that we actually want to return all the even numbers in the list.

173
00:09:55,710 --> 00:09:57,750
So I don't just want to say true or false.

174
00:09:57,840 --> 00:10:00,690
Is there an even number actually want to return them?

175
00:10:01,320 --> 00:10:03,900
So let's go ahead and start adjusting our check even list.

176
00:10:04,110 --> 00:10:10,830
So, again, what we want to do here is I want to return all the even numbers in the list.

177
00:10:11,190 --> 00:10:14,610
And if there are no even numbers, we'll go ahead and just return an empty list.

178
00:10:15,450 --> 00:10:16,470
So what we need to do here?

179
00:10:16,980 --> 00:10:18,960
Well, this code may come in handy.

180
00:10:18,990 --> 00:10:20,160
So I'll keep it for now.

181
00:10:20,610 --> 00:10:22,260
But what I'm going to do is start off.

182
00:10:23,650 --> 00:10:28,780
With an empty place, holder list will say even numbers is equal to an empty list.

183
00:10:28,870 --> 00:10:34,960
And it's very common at the top of function call if you intend on returning something, if you don't

184
00:10:34,960 --> 00:10:39,400
have it defined as an argument that you define it up here as some sort of place holder.

185
00:10:40,000 --> 00:10:44,470
So often you have place holder variables at the top of a function.

186
00:10:44,560 --> 00:10:49,240
So here, even numbers is essentially a place holder for what I eventually will return.

187
00:10:49,930 --> 00:10:53,370
And so I'm not going to be returning true or false anymore.

188
00:10:54,160 --> 00:10:55,480
What do actually want to return?

189
00:10:55,750 --> 00:10:57,520
I want to return the list of even numbers.

190
00:10:58,030 --> 00:11:01,660
So what I'm going to do here is I'll still go for every number in the number list.

191
00:11:02,170 --> 00:11:06,970
If I do happen to have an even number, I will append it to my even numbers list.

192
00:11:07,270 --> 00:11:10,000
I'll say even numbers that append.

193
00:11:11,260 --> 00:11:13,030
And it's called number here.

194
00:11:13,240 --> 00:11:14,410
So I'll copy and paste.

195
00:11:15,330 --> 00:11:15,730
No.

196
00:11:16,480 --> 00:11:18,490
OK, so again, I'm going through a number list.

197
00:11:18,610 --> 00:11:21,610
If I attach an even number, I will append it to my list.

198
00:11:22,240 --> 00:11:24,010
Otherwise, I don't want to do anything.

199
00:11:24,100 --> 00:11:29,290
If it's not even and then just as before, I want to make sure we've looped through every single number

200
00:11:29,290 --> 00:11:32,560
and the number lists before I return, even numbers.

201
00:11:33,070 --> 00:11:35,560
So I'll come back here and say Return.

202
00:11:37,660 --> 00:11:38,440
Even numbers.

203
00:11:39,400 --> 00:11:44,710
So, again, the return here is lined up with a for loop because I want to go through the entire logic

204
00:11:44,710 --> 00:11:45,490
of the for loop.

205
00:11:46,060 --> 00:11:47,710
And only when there's an even number.

206
00:11:47,920 --> 00:11:48,820
Do I do something.

207
00:11:49,060 --> 00:11:51,370
And then I return the even numbers list.

208
00:11:52,090 --> 00:11:55,540
Let's go ahead and check this out and delete a couple of these.

209
00:11:57,050 --> 00:11:58,710
So it's say check even list.

210
00:11:59,920 --> 00:12:04,000
If I say one, two, three, four, five, I run that it returns back to in four.

211
00:12:04,420 --> 00:12:05,350
Looks like it's working.

212
00:12:05,840 --> 00:12:08,290
I say check even list one three five.

213
00:12:08,440 --> 00:12:09,370
All odd numbers.

214
00:12:09,760 --> 00:12:11,140
I get back an empty list.

215
00:12:11,800 --> 00:12:13,180
OK, that's it for this lecture.

216
00:12:13,480 --> 00:12:17,920
Coming up next, we're going to discuss about returning tuples for packing with functions.

217
00:12:18,070 --> 00:12:18,670
I'll see you there.
