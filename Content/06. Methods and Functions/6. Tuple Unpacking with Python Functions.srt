1
00:00:05,290 --> 00:00:06,250
Welcome back, everyone.

2
00:00:06,370 --> 00:00:11,590
In this lecture, we're going to discuss functions and returning multiple items from a function with

3
00:00:11,590 --> 00:00:12,640
tuple unpacking.

4
00:00:12,940 --> 00:00:13,570
Let's get started.

5
00:00:14,320 --> 00:00:19,870
Now, recall that we've previously seen we can loop through a list of tuples and impact the values within

6
00:00:19,870 --> 00:00:20,110
them.

7
00:00:20,620 --> 00:00:22,660
For example, let me create.

8
00:00:23,910 --> 00:00:26,520
A list of tuple called struck prices.

9
00:00:26,610 --> 00:00:31,800
And what I'm going to do here is the first item will be a ticker and then that stock's price.

10
00:00:31,830 --> 00:00:33,620
So we'll say Apple is at 200.

11
00:00:34,080 --> 00:00:36,030
And I'm just completely making up these numbers.

12
00:00:36,510 --> 00:00:37,620
Then we'll say Google.

13
00:00:39,870 --> 00:00:41,970
Is at, say, four hundred dollars.

14
00:00:43,350 --> 00:00:45,240
And then finally, let's say Microsoft.

15
00:00:46,860 --> 00:00:47,650
One hundred dollars.

16
00:00:48,870 --> 00:00:49,090
OK.

17
00:00:49,670 --> 00:00:55,610
So we already know that I can actually say for item in stock prices.

18
00:00:57,010 --> 00:00:59,470
Print item, and it prints out the tuples.

19
00:00:59,770 --> 00:01:05,230
I also know that I can do tuple unpacking, which means I can actually individually grab the items from

20
00:01:05,230 --> 00:01:14,800
these tuples so I can say for price or actually for ticker and price and stock prices.

21
00:01:15,800 --> 00:01:16,220
Prints.

22
00:01:16,410 --> 00:01:17,490
Let's say just a ticker.

23
00:01:18,630 --> 00:01:19,170
Run that.

24
00:01:19,320 --> 00:01:24,300
And what it's doing is it's unpacking ticker price separately so I can do different things with them,

25
00:01:24,330 --> 00:01:25,080
such as ticker.

26
00:01:25,220 --> 00:01:29,520
Or maybe I can say print price plus.

27
00:01:30,510 --> 00:01:33,660
Let's say zero point one times price.

28
00:01:33,710 --> 00:01:35,940
So I want to see what a 10 percent increase would look like.

29
00:01:36,720 --> 00:01:39,990
So I can run that and I can see a 10 percent increase in the prices.

30
00:01:40,440 --> 00:01:42,480
OK, so that's tuple unpacking.

31
00:01:43,050 --> 00:01:47,310
Now we've seen how we can do it with a for loop, but we can also do it with a function.

32
00:01:47,490 --> 00:01:53,220
So a function can actually return things as tuples and then we can unpack the result from the function.

33
00:01:54,000 --> 00:01:57,960
So let's go ahead and do kind of a more complicated example here.

34
00:01:58,950 --> 00:02:03,890
I'm going to say work hours and this is going to be a list of tuples that has an employee name.

35
00:02:04,470 --> 00:02:05,220
Let's say Abie.

36
00:02:06,300 --> 00:02:08,880
And then the hours they happened to work that particular month.

37
00:02:09,480 --> 00:02:11,010
So we have Abbie at one hundred hours.

38
00:02:11,580 --> 00:02:16,740
Let's go ahead and have Billy at four hundred hours, just kind of making these up.

39
00:02:17,130 --> 00:02:20,490
And then let's have Cassie make sure it's a string.

40
00:02:22,170 --> 00:02:25,850
Eight hundred hours, so kind of extreme values here, but they're totally made up.

41
00:02:26,760 --> 00:02:29,690
OK, so we have these work hours over some period of time.

42
00:02:30,080 --> 00:02:34,880
And now what we want to do is we want to see who's the employee of the month or the employee of the

43
00:02:34,880 --> 00:02:35,180
year.

44
00:02:35,720 --> 00:02:39,800
And the way we judge that is by the employee that has worked the most hours.

45
00:02:40,250 --> 00:02:45,020
And we want to return not just the employee name, but also the amount of hours worked.

46
00:02:45,290 --> 00:02:53,780
So the function that can actually then go through these tuples and then see the name and the corresponding

47
00:02:53,810 --> 00:02:57,140
hours and keep track of the name that has the most hours.

48
00:02:57,740 --> 00:02:58,730
So how can we do this?

49
00:02:59,560 --> 00:03:06,110
I'll create a function called Employee Check and it accepts this list of tuples work hours.

50
00:03:07,310 --> 00:03:11,990
And then, as I've stated before, typically we're going to have place holder variables when it comes

51
00:03:11,990 --> 00:03:12,650
to logic.

52
00:03:12,770 --> 00:03:14,090
At the very top of the function.

53
00:03:14,600 --> 00:03:16,250
So I'm gonna have to place Holder variables.

54
00:03:16,340 --> 00:03:21,080
I'm going to have a current's max and I going to set it equal to zero.

55
00:03:21,950 --> 00:03:25,890
And then I'm going to have an employee, let's say, of the month.

56
00:03:25,910 --> 00:03:28,560
In this case, we'll say employee of month.

57
00:03:29,980 --> 00:03:36,140
And that's equal to an empty string because at the end, I intend to return a tuple.

58
00:03:36,770 --> 00:03:41,060
So I intend to return a tuple that says Employee of the Month.

59
00:03:42,350 --> 00:03:45,880
Along with the current max, which is the current max hours.

60
00:03:46,310 --> 00:03:50,150
So why do I have current max equal to zero and what is current Max going to do?

61
00:03:50,720 --> 00:03:56,120
Well, what current Max is going to do is it's going to start off at zero and then it's going to compare

62
00:03:56,660 --> 00:03:59,900
each employees hours to the current max hours.

63
00:04:00,200 --> 00:04:06,650
And if they beat the current max hours, then I will reset the current max to their value and I'll reset

64
00:04:06,650 --> 00:04:08,820
the employee of the month to this string.

65
00:04:09,620 --> 00:04:11,360
So we have to do a for loop to see this.

66
00:04:12,260 --> 00:04:13,040
We'll say four.

67
00:04:13,280 --> 00:04:19,580
And here we'll do some tuple unpacking for employee common hours and work hours.

68
00:04:19,640 --> 00:04:21,830
Which recall is this list of tuples?

69
00:04:22,880 --> 00:04:25,340
What I'm going to do is I'm first going to check.

70
00:04:27,150 --> 00:04:34,530
If the hours that they've worked is greater than my current Max, then I'm going to reset my current

71
00:04:34,530 --> 00:04:35,100
Max.

72
00:04:36,370 --> 00:04:42,540
Equal to the hours that that particular employee worked and then the employee of the month.

73
00:04:44,180 --> 00:04:45,470
It's going to be equal to.

74
00:04:46,620 --> 00:04:47,910
The employee in that tuple.

75
00:04:48,900 --> 00:04:50,010
So what does that actually mean?

76
00:04:50,340 --> 00:04:55,410
Well, recall that my current Mac starts off at zero, so he immediately the very first person I see

77
00:04:55,680 --> 00:04:57,870
has beat supposedly zero hours.

78
00:04:58,080 --> 00:05:01,590
So that means Abby starts off as employee of the month at my current max.

79
00:05:01,620 --> 00:05:04,020
Hours to beat is one hundred hours.

80
00:05:05,230 --> 00:05:05,830
Otherwise.

81
00:05:06,970 --> 00:05:07,870
I'll simply pass.

82
00:05:08,950 --> 00:05:11,300
So let's go ahead and see what happens when we reach Billy.

83
00:05:11,770 --> 00:05:14,410
We see Billy, we extract Billy's, ours is 400.

84
00:05:14,830 --> 00:05:16,810
Is that greater than the current max hours?

85
00:05:16,840 --> 00:05:20,130
Well, in this case it is, because Abbie only worked 100 zijn.

86
00:05:20,260 --> 00:05:24,440
I reset the current max, essentially the target to beat as 400.

87
00:05:24,910 --> 00:05:28,900
And then I set Billy as the employee of the month and I keep going throughout this.

88
00:05:29,230 --> 00:05:34,030
So then Cassie is going to end up being Billy's 400 hours and then she'll be the employee of the month.

89
00:05:34,660 --> 00:05:37,810
And what I'm going to do is return this then as a tuple.

90
00:05:38,680 --> 00:05:40,240
So again, here's all the code.

91
00:05:41,140 --> 00:05:41,860
Go out.

92
00:05:41,890 --> 00:05:45,610
One level of zoom so we can see this all going to run this.

93
00:05:45,700 --> 00:05:47,470
And let's run our employee check.

94
00:05:48,430 --> 00:05:51,670
And I'm going to run it on my current existing work hours.

95
00:05:52,150 --> 00:05:53,580
That's the variable we first assign.

96
00:05:54,160 --> 00:05:56,230
And as we suspected, Cassie one.

97
00:05:56,450 --> 00:05:57,820
So let's make sure this is working.

98
00:05:58,030 --> 00:06:00,870
Let's go ahead and say Billy worked 4000 hours.

99
00:06:01,660 --> 00:06:02,650
So we run this again.

100
00:06:03,310 --> 00:06:04,150
Employee check.

101
00:06:04,600 --> 00:06:05,060
Run that.

102
00:06:05,470 --> 00:06:07,230
And I see Billy 4000.

103
00:06:07,840 --> 00:06:12,250
So, again, what it's doing here is it's just tracking who work the max hours and essentially.

104
00:06:12,430 --> 00:06:13,360
Who do we have to beat?

105
00:06:13,840 --> 00:06:19,150
And eventually it's gonna happen is we'll find out who worked the max hours because no one will start

106
00:06:19,150 --> 00:06:22,990
beating this equation right here of ours greater than the current Max.

107
00:06:23,020 --> 00:06:28,120
Which means we never ended up resetting current Max and we never end up overwriting that employee of

108
00:06:28,120 --> 00:06:28,570
the month.

109
00:06:28,990 --> 00:06:34,050
So essentially, once Billy comes in and he hits 4000, Kasey doesn't end up beating him with a hundred.

110
00:06:34,360 --> 00:06:38,020
So Billy stays as the assigned variable employee of the month.

111
00:06:38,680 --> 00:06:41,830
Now, we just saw this return back, this tuple.

112
00:06:42,430 --> 00:06:44,410
So all we can do is two things.

113
00:06:44,470 --> 00:06:47,980
I can either say result is equal to employee check.

114
00:06:48,900 --> 00:06:55,240
And if I take a look at result, it says Billy 4000 or just as before with the for loop where I said

115
00:06:55,360 --> 00:07:00,850
ticker comma price, I can do the same thing if a function returns back this tuple.

116
00:07:01,270 --> 00:07:03,010
So it's really common to see something like this.

117
00:07:03,040 --> 00:07:05,140
I can say name karma.

118
00:07:05,830 --> 00:07:09,310
Ours is equal to employee check.

119
00:07:10,840 --> 00:07:11,620
Work hours.

120
00:07:12,490 --> 00:07:18,940
Run that and note what happened, unlike before, where results if we see it here is the tuple idea,

121
00:07:18,940 --> 00:07:19,850
tuple unpacking.

122
00:07:20,080 --> 00:07:21,390
And now I have the name Billy.

123
00:07:22,150 --> 00:07:29,470
And ours as separate variables so I can actually perform tuple unpacking with a function call.

124
00:07:30,190 --> 00:07:35,560
Now, a really common error is when you're using someone else's function or a function from a different

125
00:07:35,560 --> 00:07:37,150
library that you're not too familiar with.

126
00:07:37,510 --> 00:07:42,010
Maybe we think that, oh, this function also gives the employees location.

127
00:07:42,250 --> 00:07:43,930
So let's see the result here.

128
00:07:44,050 --> 00:07:46,150
Let's say name, comma, hours, comma, location.

129
00:07:46,450 --> 00:07:48,970
Now, I know for a fact it doesn't report back to location.

130
00:07:49,600 --> 00:07:51,610
If you run this, you get an error.

131
00:07:51,910 --> 00:07:55,570
And there's a very common error where it says not enough values to unpack.

132
00:07:56,050 --> 00:07:59,680
And very common error to get when you're dealing with functions that you don't, 100 percent know what

133
00:07:59,680 --> 00:08:00,220
they return.

134
00:08:00,760 --> 00:08:07,400
So you notice here it actually tells you, hey, expected three, got to expect it.

135
00:08:07,450 --> 00:08:09,970
Three is basically what you expected.

136
00:08:10,150 --> 00:08:13,150
You expect the three values, but you only got two.

137
00:08:13,630 --> 00:08:15,580
So how do you actually deal with this sort of error?

138
00:08:15,970 --> 00:08:22,540
The easiest way is, is just assign everything to a single item and then begin exploring the item.

139
00:08:22,630 --> 00:08:26,290
And then you can see, oh, I only have those two values and it looks like it was a name.

140
00:08:26,650 --> 00:08:29,080
And the hours, I don't actually have a third location item.

141
00:08:30,160 --> 00:08:30,390
All right.

142
00:08:30,460 --> 00:08:32,750
That's it for returning tuples, for packing.

143
00:08:33,220 --> 00:08:37,720
Again, you can review this when we discussed for loops as far as tuple and packing within a for loop.

144
00:08:38,050 --> 00:08:41,950
And you can kind of review this function, call in the lecture notebook.

145
00:08:42,400 --> 00:08:44,600
If you have any questions, feel free to post to community forums.

146
00:08:44,950 --> 00:08:50,650
We'll finish off this lecture series by just creating a very simple function program that essentially

147
00:08:50,650 --> 00:08:52,780
shows interactions between functions.

148
00:08:53,140 --> 00:08:54,100
I'll see you at the next lecture.
