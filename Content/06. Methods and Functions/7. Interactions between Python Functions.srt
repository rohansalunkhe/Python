1
00:00:05,320 --> 00:00:08,860
Welcome back, everyone, to this lecture on interactions between functions.

2
00:00:09,970 --> 00:00:15,340
Typically, a Python script or Python notebook contains several functions interacting with each other.

3
00:00:15,700 --> 00:00:19,750
We take the results of one function and often use that as input to another function.

4
00:00:20,320 --> 00:00:24,670
What we're going to do is we're going to create a few functions to attempt to mimic the carnival guessing

5
00:00:24,670 --> 00:00:24,970
game.

6
00:00:25,000 --> 00:00:25,900
Three cup Monty.

7
00:00:26,210 --> 00:00:30,940
And in case you're not familiar with this, it's basically you start off with three cups and underneath

8
00:00:30,940 --> 00:00:32,740
one of them is hit in a red ball.

9
00:00:33,430 --> 00:00:38,980
And then we're going to do is you shuffle these cups around, have the user guess where the red ball

10
00:00:38,980 --> 00:00:42,030
is, and then they're either right or wrong.

11
00:00:43,350 --> 00:00:46,230
So our very simple game will actually show the cups her ball.

12
00:00:46,350 --> 00:00:50,010
Instead, we're going to mimic this with the effect of a python list.

13
00:00:50,160 --> 00:00:52,980
So it's going to be a python list with two empty strings.

14
00:00:53,100 --> 00:00:57,330
And then one string that has just a capital O, which will be our ball.

15
00:00:57,930 --> 00:01:00,810
And our simple version will also not show the shuffling to the user.

16
00:01:01,140 --> 00:01:03,360
So the guest for the user will just be completely random.

17
00:01:03,900 --> 00:01:09,390
Our main focus here is to show you how multiple functions can end up interacting with each other and

18
00:01:09,390 --> 00:01:10,740
returning objects to each other.

19
00:01:11,310 --> 00:01:12,450
OK, let's get started.

20
00:01:12,840 --> 00:01:13,040
All right.

21
00:01:13,050 --> 00:01:15,420
Here we are in the Jupiter notebook to begin.

22
00:01:15,510 --> 00:01:19,170
I just want to show you how you can shuffle a list at random in Python.

23
00:01:19,340 --> 00:01:19,930
In Python.

24
00:01:20,310 --> 00:01:22,950
Actually comes in a built in random module.

25
00:01:23,220 --> 00:01:25,500
And we covered this module in more detail later on.

26
00:01:25,800 --> 00:01:30,600
But for now, I'll show you just the shuffle function that can come from the random module and we'll

27
00:01:30,600 --> 00:01:31,560
show you how to import it.

28
00:01:31,880 --> 00:01:34,710
And we'll talk a lot more about imports later on in the course.

29
00:01:35,340 --> 00:01:38,490
But for this case, let's go ahead and make an example list.

30
00:01:39,150 --> 00:01:43,560
Let's say it has seven items in it and I want to randomly shuffle the list.

31
00:01:43,860 --> 00:01:52,650
Well, I can say from random import shuffle and the shuffle function will actually shuffle a list in

32
00:01:52,650 --> 00:01:55,560
place, meaning it doesn't actually return anything.

33
00:01:55,920 --> 00:01:58,020
It just grabs that current list and then shuffles it.

34
00:01:59,010 --> 00:02:00,420
So we can say shuffle.

35
00:02:01,870 --> 00:02:08,210
The example list, and then when you call example again, you'll notice that it's been shuffled.

36
00:02:08,600 --> 00:02:15,170
So we're going to be using this built in shuffle from the random module inside of Python within one

37
00:02:15,170 --> 00:02:15,950
of our functions.

38
00:02:16,520 --> 00:02:18,500
So let's start by creating our simple game.

39
00:02:19,010 --> 00:02:25,820
First, I'm going to create kind of our own version of shuffle, which actually returns the example

40
00:02:25,820 --> 00:02:26,210
list.

41
00:02:26,630 --> 00:02:29,570
Notice shuffle example here, it doesn't actually return anything.

42
00:02:30,020 --> 00:02:38,030
For example, if I were to try to say result is equal to shuffle example and then call results later

43
00:02:38,030 --> 00:02:42,350
on, it's actually happening all in place within the example list.

44
00:02:42,560 --> 00:02:45,560
So nothing is return, which means I can't actually do this.

45
00:02:45,860 --> 00:02:48,740
So what I need to do is create my own function for this.

46
00:02:49,580 --> 00:02:49,970
So.

47
00:02:51,510 --> 00:02:57,270
Say, D.F. Shuffle, and we can't call it shuffle, because that's the same name as the function space

48
00:02:57,270 --> 00:02:58,620
and we can't share that namespace.

49
00:02:59,040 --> 00:03:03,600
So we'll say D.F. Shuffle list, which is going to take in my list.

50
00:03:04,760 --> 00:03:10,340
It will shuffle my list and then it will also return my list.

51
00:03:10,670 --> 00:03:15,680
So adding just a little bit of functionality on top of the built-In shuffle, since the built in shuffle

52
00:03:15,740 --> 00:03:16,670
won't return anything.

53
00:03:16,700 --> 00:03:17,750
It just doesn't in place.

54
00:03:18,050 --> 00:03:21,740
And I actually want to be able to return it and use it as a result here.

55
00:03:23,240 --> 00:03:24,290
So let's see if this works now.

56
00:03:24,380 --> 00:03:27,990
I'll say result is equal to shuffle list.

57
00:03:28,610 --> 00:03:30,050
Let's pass in our example.

58
00:03:31,010 --> 00:03:33,320
We check out our result and it's shuffled.

59
00:03:33,530 --> 00:03:37,100
And each time I run this, you'll notice it kind of shuffles it in a different way.

60
00:03:37,820 --> 00:03:38,300
Perfect.

61
00:03:38,690 --> 00:03:40,820
So we have a function that can shuffle this list.

62
00:03:41,090 --> 00:03:43,880
So what is our actual game list going to look like?

63
00:03:44,630 --> 00:03:52,250
Well, as I noted previously, our game list is just going to be three strings with one of them being

64
00:03:52,850 --> 00:03:57,440
essentially an capital O as our red ball and the other two are empty strings.

65
00:03:57,620 --> 00:04:01,280
So what the user is going to try to guess is where is this hidden capital?

66
00:04:01,310 --> 00:04:01,550
Oh.

67
00:04:03,240 --> 00:04:06,330
So eventually we're going to be doing is calling shuffle list.

68
00:04:07,630 --> 00:04:08,380
On my list.

69
00:04:08,800 --> 00:04:12,490
And then it will shuffle this around and the O may or may not be in the same spot.

70
00:04:13,300 --> 00:04:16,060
Next, we need to create a function that can take in a player.

71
00:04:16,100 --> 00:04:16,990
Guess so.

72
00:04:17,140 --> 00:04:19,090
The player after the list is being shuffled.

73
00:04:19,120 --> 00:04:20,530
Is going to guess the location.

74
00:04:20,920 --> 00:04:23,590
And we won't display where the list is before we shuffle.

75
00:04:25,010 --> 00:04:31,400
So what I want to show you is a function called player guests, and this takes in no arguments.

76
00:04:31,790 --> 00:04:38,480
Instead, we start off with a placeholder of the user's guests and the user can guess essentially one

77
00:04:38,480 --> 00:04:39,950
of three index positions.

78
00:04:40,370 --> 00:04:44,300
They can guess it's either index zero, index one or index two.

79
00:04:44,900 --> 00:04:49,040
So we start off with an empty guess and then we'll say.

80
00:04:50,030 --> 00:04:52,730
Guess is equal to input function.

81
00:04:53,770 --> 00:04:55,040
And we'll say pick a number.

82
00:04:56,240 --> 00:04:57,630
Zero one.

83
00:04:59,530 --> 00:05:06,490
Or two now, in order to make sure that the guests is actually correct, we could add in some extra

84
00:05:06,490 --> 00:05:07,060
code here.

85
00:05:07,480 --> 00:05:11,950
Basically, as we mentioned before, we can use a while loop to make sure that the input is correct.

86
00:05:12,520 --> 00:05:14,350
So I could say while the guests.

87
00:05:15,820 --> 00:05:16,900
Not in.

88
00:05:18,240 --> 00:05:21,260
Zero one two.

89
00:05:22,010 --> 00:05:28,680
Colin, go ahead and keep asking for this input and then once that's finalized, go ahead and return

90
00:05:28,740 --> 00:05:30,700
to integer version of guests.

91
00:05:31,140 --> 00:05:37,050
You may be wondering why are these strings and why are you casting it as an integer at the end?

92
00:05:37,440 --> 00:05:41,850
Well, that's because input returns and always returns a string.

93
00:05:42,390 --> 00:05:48,060
So even if you type in the integer zero one or two, it's going to return it as the form of a string,

94
00:05:48,450 --> 00:05:51,300
which is why we're checking the string zero one and two.

95
00:05:51,660 --> 00:05:57,690
But I'm returning it as an integer since I will use it to directly index off this list.

96
00:05:58,380 --> 00:05:59,300
Let's go ahead and run this.

97
00:05:59,370 --> 00:06:00,480
You can see what it looks like.

98
00:06:01,560 --> 00:06:04,500
I call player guess open close parentheses.

99
00:06:05,160 --> 00:06:05,700
Run this.

100
00:06:05,760 --> 00:06:07,890
It says pick a number zero or one or two.

101
00:06:08,340 --> 00:06:12,930
If I provide an incorrect value because of that, while Loop will keep asking me over and over again

102
00:06:13,290 --> 00:06:15,960
until I get either zero one or two.

103
00:06:16,020 --> 00:06:18,930
So if I pass in one, that's perfect.

104
00:06:19,020 --> 00:06:21,330
And then I get the result, one which I can save.

105
00:06:21,450 --> 00:06:22,200
So I could say.

106
00:06:23,490 --> 00:06:26,730
My index is equal to player guess.

107
00:06:27,660 --> 00:06:28,440
Run this again.

108
00:06:28,710 --> 00:06:34,560
And if I pass in to this time, you'll notice that my index is now too perfect.

109
00:06:34,920 --> 00:06:41,280
So we just need one more function, which is to essentially combine this list has been shuffled with

110
00:06:41,280 --> 00:06:42,190
the players guess.

111
00:06:42,420 --> 00:06:44,790
So I just need to check that my guess was correct or not.

112
00:06:46,850 --> 00:06:47,990
So we'll say check.

113
00:06:49,690 --> 00:06:50,250
Yes.

114
00:06:51,160 --> 00:06:53,890
And this is where the functions begin to interact with each other.

115
00:06:54,260 --> 00:06:59,820
Because I will pass in my shuffled list and I will pass in the users guests.

116
00:07:00,790 --> 00:07:08,070
So that means I should have called player guests and my shuffle list function before calling this check

117
00:07:08,070 --> 00:07:08,830
guests function.

118
00:07:09,490 --> 00:07:13,840
And so what we're going to do at the end of this notebook is write a little script that essentially

119
00:07:13,840 --> 00:07:15,880
calls the functions in the correct order.

120
00:07:16,630 --> 00:07:18,260
So we'll say the F check.

121
00:07:18,340 --> 00:07:18,940
Guess.

122
00:07:19,670 --> 00:07:30,430
And we'll say if my list at my guests index position is equal to a capital O, then I'll simply just

123
00:07:30,430 --> 00:07:30,970
print.

124
00:07:32,250 --> 00:07:32,860
Correct.

125
00:07:34,550 --> 00:07:36,840
Or else I will print.

126
00:07:38,770 --> 00:07:39,590
Wrong guess.

127
00:07:39,820 --> 00:07:44,800
And let's go ahead and tell the user where actually was by printing out that list.

128
00:07:45,100 --> 00:07:49,330
No, in this case, I don't actually care about returning anything because this will essentially be

129
00:07:49,330 --> 00:07:51,130
the last line in my little script.

130
00:07:52,540 --> 00:07:58,000
So I have my functions defined and now it's time to set up a little bit of logic as a final script.

131
00:07:58,330 --> 00:08:02,060
So if you're dealing with this and a thought py file, it's very common to have a bunch of function

132
00:08:02,110 --> 00:08:06,400
definitions at the top and then at the bottom you have the actual logic to be called here.

133
00:08:06,910 --> 00:08:10,180
So what you need to do is set up my initial list.

134
00:08:11,110 --> 00:08:13,440
Then I need to shuffle that list.

135
00:08:14,770 --> 00:08:16,870
Then have the user guess the location.

136
00:08:17,910 --> 00:08:22,040
And then I will check against the list with the user guest.

137
00:08:22,710 --> 00:08:23,580
So how do we do this?

138
00:08:24,060 --> 00:08:27,870
We'll start off with my list, which is an empty string.

139
00:08:28,680 --> 00:08:30,650
Oh, and another empty string.

140
00:08:30,660 --> 00:08:32,040
So the O starts off in the middle.

141
00:08:32,700 --> 00:08:38,040
And then what I'm going to do is I will have a mixed up version of this list and I will call.

142
00:08:39,280 --> 00:08:43,450
Equals shuffle list on my list.

143
00:08:43,570 --> 00:08:44,380
That's step two.

144
00:08:45,480 --> 00:08:48,250
And this is where I'm also going to grab the user guess.

145
00:08:49,160 --> 00:08:52,690
So say guess is equal to the result of player guess.

146
00:08:53,700 --> 00:08:55,470
And then I'm going to check the guests.

147
00:08:55,860 --> 00:08:56,580
So we'll say check.

148
00:08:56,620 --> 00:08:57,180
Guess.

149
00:08:58,940 --> 00:09:02,000
And I will pass on the mixed up list along.

150
00:09:02,060 --> 00:09:02,840
These are guests.

151
00:09:03,200 --> 00:09:08,450
So this is really where the functions start interact with each other because I will shuffle and get

152
00:09:08,450 --> 00:09:10,940
a mixed list and then I will get the player guests.

153
00:09:11,150 --> 00:09:15,230
And this third function takes the results of those two other functions.

154
00:09:15,620 --> 00:09:16,970
And this is what's going to apply to.

155
00:09:16,970 --> 00:09:17,290
Right.

156
00:09:17,450 --> 00:09:19,040
Much more complex code.

157
00:09:19,400 --> 00:09:25,040
The fact they can have separate functions, doing separate parts and then interact with each other later

158
00:09:25,040 --> 00:09:26,510
on within another function.

159
00:09:26,960 --> 00:09:33,590
So you can see here that I'm actually able to write more complex code because I've been able to compartmentalize

160
00:09:33,680 --> 00:09:38,420
a lot of these functions and a lot of this complexity to just certain function calls.

161
00:09:38,720 --> 00:09:40,490
So this is really easy to read.

162
00:09:40,640 --> 00:09:45,170
And then if I ever have a question on what player guesses or what shuffle list is, I can review by

163
00:09:45,170 --> 00:09:47,840
going back up and checking my function definitions.

164
00:09:48,170 --> 00:09:52,640
So I can be in creating really complex code that is still actually quite readable.

165
00:09:53,180 --> 00:09:55,010
So let's run this and see how it works.

166
00:09:56,100 --> 00:09:57,150
So it says pick a number.

167
00:09:57,240 --> 00:09:58,350
Zero, one or two.

168
00:09:58,740 --> 00:10:01,020
Let me just input a wrong number to make sure it's still working.

169
00:10:01,550 --> 00:10:02,050
I hit eight.

170
00:10:02,290 --> 00:10:03,240
It asks me again.

171
00:10:03,420 --> 00:10:04,440
Looks like that's working.

172
00:10:04,860 --> 00:10:07,510
Let's just guess location index position zero.

173
00:10:07,560 --> 00:10:10,350
So if I get lucky, I have one in three chance of getting it right.

174
00:10:11,190 --> 00:10:12,510
And it was a wrong guess.

175
00:10:12,690 --> 00:10:19,440
So notice here, I said zero zero is empty string and it's currently lying in index two, which also

176
00:10:19,440 --> 00:10:25,320
lets me know that the shuffle list is working because my initial list had the oh, index position one.

177
00:10:26,860 --> 00:10:28,120
All right, that's it.

178
00:10:28,510 --> 00:10:33,010
Go ahead and check out the lecture notebook in case you have any more questions on this.

179
00:10:33,070 --> 00:10:35,920
It's kind of setup in a step by step manner there.

180
00:10:36,220 --> 00:10:38,890
Or you can post the Q&amp;A forums if you have any questions.

181
00:10:39,340 --> 00:10:43,600
If you're trying to follow along, but you're getting errors, makes you try running our notebook first

182
00:10:43,600 --> 00:10:45,400
to make sure you didn't commit a simple typo.

183
00:10:46,030 --> 00:10:46,420
Thanks.

184
00:10:46,610 --> 00:10:47,680
And I'll see you at the next lecture.
