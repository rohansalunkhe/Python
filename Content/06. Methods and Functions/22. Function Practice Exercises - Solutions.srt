1
00:00:05,210 --> 00:00:10,140
In this lecture we're going to catalogue through solutions for the previous function practice problems.

2
00:00:10,140 --> 00:00:11,230
Let's get started.

3
00:00:11,580 --> 00:00:16,980
OK let's begin with this warm up section and this one was called lesser of two evils you want to write

4
00:00:16,980 --> 00:00:22,380
a function that returns the lesser of two given numbers if both numbers are even but returns the greater

5
00:00:22,470 --> 00:00:24,200
if one or both numbers are odd.

6
00:00:24,570 --> 00:00:29,510
So here we have our function and it takes a number a in a number be the first thing you want to do is

7
00:00:29,520 --> 00:00:33,210
probably checked if the numbers are both even.

8
00:00:33,430 --> 00:00:38,900
And that way we have an if statement so we'll say if a mod 2 is equal to zero.

9
00:00:38,920 --> 00:00:44,550
So that is to say when you divide a by two is to have a remainder of 0 indicating that's even.

10
00:00:44,890 --> 00:00:49,850
And the same for B B MA 2 is equal to zero.

11
00:00:50,900 --> 00:00:52,990
So if this statement happens to be true.

12
00:00:53,000 --> 00:00:56,290
So if this condition is true that means we have both numbers or even.

13
00:00:56,510 --> 00:01:00,850
So I'm going to say here both numbers are even.

14
00:01:00,950 --> 00:01:06,230
And in this case when both numbers are even we want to return the lesser of the two numbers.

15
00:01:06,230 --> 00:01:07,880
So we'll say the following.

16
00:01:07,970 --> 00:01:13,010
We're going to say if a 2 is equal to zero and B 2 is equal to zero.

17
00:01:13,190 --> 00:01:14,680
Let's check the greater number.

18
00:01:14,870 --> 00:01:28,410
If A is less than B we're going to say result is equal to A else.

19
00:01:28,400 --> 00:01:35,310
Result is equal to be and offer this we're going to do is say.

20
00:01:35,310 --> 00:01:36,810
Else.

21
00:01:36,830 --> 00:01:43,110
So now one or both numbers are odd.

22
00:01:44,720 --> 00:01:46,200
And in this case we want to return the greater.

23
00:01:46,200 --> 00:01:47,850
So we're doing the exact opposite here.

24
00:01:47,900 --> 00:01:59,370
If A is greater than B say the results is a else the result is be.

25
00:01:59,600 --> 00:02:04,600
And then all the way at the end here we can return the result.

26
00:02:04,610 --> 00:02:07,080
Notice the indentation here.

27
00:02:07,090 --> 00:02:12,670
All right so this is one way of solving this problem just using a bunch of IF and L statements so let's

28
00:02:12,670 --> 00:02:15,610
run this and confirm that it's actually working for us.

29
00:02:15,610 --> 00:02:16,960
So a lesser of two evils.

30
00:02:17,140 --> 00:02:18,600
Looks like it's returning to here.

31
00:02:18,640 --> 00:02:21,820
Lesser of two evils if one of them has an odd number it's returning five.

32
00:02:21,910 --> 00:02:25,930
Let's make them both on just to check in returns 7.

33
00:02:25,930 --> 00:02:26,510
Great.

34
00:02:26,530 --> 00:02:29,350
So this is a viable solution for this problem.

35
00:02:29,380 --> 00:02:32,330
However we can actually clean up this code a lot more.

36
00:02:32,350 --> 00:02:38,720
In fact there is a way we can run this chunk of code automatically and that's using the min function.

37
00:02:38,740 --> 00:02:43,840
Previously we saw how the min function can actually return the minimum value of a list.

38
00:02:44,050 --> 00:02:49,270
But if we take a look at what happens with minimum function when we pass on to numbers such as 10 and

39
00:02:49,270 --> 00:02:54,710
20 it actually returns back the minimum of the two numbers that you're comparing.

40
00:02:55,080 --> 00:02:59,180
And it works with just two numbers if you want more than that you'll have to pass on a list.

41
00:02:59,640 --> 00:03:01,850
But it also works the same with the Max function.

42
00:03:02,090 --> 00:03:07,830
So then it returns the max either 20 here when you pass and two numbers in which case all I need to

43
00:03:07,830 --> 00:03:09,000
do is the following.

44
00:03:09,030 --> 00:03:18,540
I can say results here is equal to the max of a combi and I can then say here result is equal to the

45
00:03:18,550 --> 00:03:21,490
min of AB.

46
00:03:21,640 --> 00:03:24,160
So we be able to clean this up even further.

47
00:03:24,280 --> 00:03:27,600
And if I run this let's check to make sure we get the same results here.

48
00:03:27,850 --> 00:03:31,210
So I get back to and they get back 7 when I rerun those cells.

49
00:03:31,210 --> 00:03:32,220
Perfect.

50
00:03:32,260 --> 00:03:34,680
And in fact we can take this one step further.

51
00:03:34,750 --> 00:03:39,910
We can actually have multiple return statements inside here because once you say return and it gets

52
00:03:39,970 --> 00:03:42,340
executed the function will stop.

53
00:03:42,340 --> 00:03:47,140
So instead of saving these results and then waiting until the very end to return your result we could

54
00:03:47,140 --> 00:03:57,580
do is just say return men a B over here and then say return Max AB over here and delete the last one.

55
00:03:57,940 --> 00:04:01,440
So then what's going to happen here is your function's going to go along.

56
00:04:01,570 --> 00:04:06,540
And it says hey if both the numbers are even go ahead and just go straight to returning the minimum

57
00:04:06,550 --> 00:04:12,180
value between a and b else then go straight to returning the maximum value of a and b.

58
00:04:12,430 --> 00:04:15,350
And this is probably the most efficient way to do this problem.

59
00:04:15,370 --> 00:04:21,520
We could make it a little sleeker maybe try to put it all in one line but this right here is a better

60
00:04:21,520 --> 00:04:24,180
solution than what we saw at the very first run.

61
00:04:24,310 --> 00:04:26,460
And this is what you're going to see in the notebook.

62
00:04:26,920 --> 00:04:30,940
All right the next question in the warmup section was this question called Animal Crackers where you

63
00:04:30,940 --> 00:04:37,330
wanted to write a function that takes in a two word string and then returns true if both words begin

64
00:04:37,330 --> 00:04:38,590
with the same letter.

65
00:04:38,590 --> 00:04:41,450
So here I can see level headed and llama both begin with L.

66
00:04:41,500 --> 00:04:46,790
So we return TRUE crazy kangaroo begin with different letters so we return false.

67
00:04:46,840 --> 00:04:48,390
So let's build this out.

68
00:04:48,800 --> 00:04:53,290
We're going to do a similar thing as last time we'll build out all the logic and then see if we can

69
00:04:53,620 --> 00:04:57,300
make it a little sleeker by using some keywords.

70
00:04:57,310 --> 00:05:04,030
So the first thing I want to do is actually grab the two words so I can create a variable called wordlist

71
00:05:05,200 --> 00:05:10,500
and set equal to the incoming string and to grab the two words I can use split.

72
00:05:10,510 --> 00:05:13,380
And now I should have a list of the actual two words.

73
00:05:13,390 --> 00:05:17,280
So let's just print out word lists so we can confirm this.

74
00:05:17,290 --> 00:05:22,060
So if I run this now I see that I'm getting back a list of two words and what I want to do is check

75
00:05:22,530 --> 00:05:27,770
is the very first character and this word matching to the very first character in this word.

76
00:05:27,880 --> 00:05:32,540
So I could say that my first word is equal to word list at zero.

77
00:05:33,280 --> 00:05:36,220
And my second word is equal to wordlist.

78
00:05:36,250 --> 00:05:37,680
Next one.

79
00:05:37,900 --> 00:05:42,750
And then I want to return is first zero.

80
00:05:43,000 --> 00:05:49,420
Meaning is that first letter of the first word equal to second 0.

81
00:05:49,420 --> 00:05:53,460
So is this first letter equal to the first letter of second.

82
00:05:53,470 --> 00:05:59,650
So now we run this I get back true and I get back false here and we can take out this print statement

83
00:05:59,650 --> 00:06:03,720
now to see just the boolean results.

84
00:06:03,730 --> 00:06:06,060
Now we can actually edit this a little bit.

85
00:06:06,100 --> 00:06:13,240
In fact notice here how I'm essentially making two index calls zero and then another zero here and then

86
00:06:13,240 --> 00:06:14,980
one and another zero here.

87
00:06:14,980 --> 00:06:20,770
So what it could do is not even bother with this assignment and it's just grab orderless itself and

88
00:06:20,770 --> 00:06:22,130
replace it for first here.

89
00:06:23,250 --> 00:06:26,940
And then I could grab word list one here and replace it for a second there.

90
00:06:28,760 --> 00:06:33,730
And now this is the exact same thing except it's now using a double index call.

91
00:06:33,730 --> 00:06:36,760
So when I run this again I get back the same results.

92
00:06:36,910 --> 00:06:42,630
Something to notice here that I'm not actually checking is just in case these two letters.

93
00:06:42,820 --> 00:06:46,840
Right now they're both capitalized and we're assuming they're both capitalized but maybe one of them

94
00:06:46,930 --> 00:06:48,980
is lowercase and I'll show you what I mean by that.

95
00:06:49,150 --> 00:06:53,980
Let's imagine we have crazy cat with a lowercase see if I run this.

96
00:06:53,980 --> 00:06:55,370
It's going to return false.

97
00:06:55,390 --> 00:06:58,100
Even though we are starting with the same letter.

98
00:06:58,240 --> 00:07:02,720
In that case what it could do is lowercase my words split lowercase my text.

99
00:07:02,740 --> 00:07:11,290
Excuse me before the split so I could say text that lower and then after that call the split second

100
00:07:11,290 --> 00:07:12,610
to do that in a new line.

101
00:07:12,690 --> 00:07:17,150
But Python actually allows you to kind of tack these on one right after the other.

102
00:07:17,190 --> 00:07:21,670
So I'm going to do lower and then do that split on this Snell's run this again.

103
00:07:22,200 --> 00:07:27,210
And now you can see that it confirms true because it's making everything lower case before it actually

104
00:07:27,210 --> 00:07:31,190
does the split and we can see that by printing out that word list.

105
00:07:31,290 --> 00:07:36,750
And if I run this again on crazy cat I notice here crazy has now been lower case and it would all work

106
00:07:36,750 --> 00:07:43,450
the same way we used upper because then it would have just uppercase the cat there.

107
00:07:43,750 --> 00:07:47,170
So it would have confirmed capital C capital C..

108
00:07:47,230 --> 00:07:50,280
All right so that's the basics of the animal crackers problem.

109
00:07:50,320 --> 00:07:50,680
All right.

110
00:07:50,710 --> 00:07:55,190
Our last warm up problem is little more math based and it's called makes 20.

111
00:07:55,330 --> 00:08:01,030
And we wanted to know that given two integers return true if the sum of the integers is 20.

112
00:08:01,250 --> 00:08:04,260
Or if one of the integers themselves is 20.

113
00:08:04,330 --> 00:08:05,320
If neither of these are true.

114
00:08:05,320 --> 00:08:07,210
Go ahead and return false.

115
00:08:07,240 --> 00:08:09,930
So let's check through these three cases.

116
00:08:10,760 --> 00:08:12,810
First we need to check the sum 20.

117
00:08:13,140 --> 00:08:24,640
So you can say if and 1 plus and 2 is equal to 20 return true and then we can elevate cases for the

118
00:08:24,640 --> 00:08:31,130
other ones we can say well if and one is equal to 20 return true.

119
00:08:32,620 --> 00:08:41,310
Elif and 2 physical to 20 return true and if none of these happen to be true then you can just return

120
00:08:41,910 --> 00:08:43,000
false.

121
00:08:43,020 --> 00:08:46,080
So when I run this code and I check it it looks like it's working.

122
00:08:46,080 --> 00:08:47,340
So two or three is false.

123
00:08:47,370 --> 00:08:49,050
20 in 10 is true.

124
00:08:49,050 --> 00:08:53,390
And if you do one that sums up to 20 10 and 10 that also returns true.

125
00:08:53,630 --> 00:08:59,610
But as a previously mentioned throughout the course if you're already checking if some boolean statement

126
00:08:59,700 --> 00:09:05,340
return that actual boolean then what we could do is put all of this on a single line.

127
00:09:05,790 --> 00:09:07,260
So how do we actually do that.

128
00:09:07,260 --> 00:09:09,560
Well no this is just a bunch of boolean checks here.

129
00:09:09,750 --> 00:09:14,600
So we can use these comparison operators along with a logical operators such as or.

130
00:09:14,760 --> 00:09:28,610
So all we actually need to do in one line is say return and 1 plus and 2 equal to 20 or and one equal

131
00:09:28,610 --> 00:09:33,310
to 20 or into equal to 20.

132
00:09:33,320 --> 00:09:37,270
So let's check out how this is the exact same thing as all this code that we just wrote here.

133
00:09:37,490 --> 00:09:42,890
We're saying Check out this bullying condition is true or this one happens to be true or this one happens

134
00:09:42,890 --> 00:09:43,710
to be true.

135
00:09:43,790 --> 00:09:46,750
If none of these happened to be true then we return false.

136
00:09:46,760 --> 00:09:50,960
So this one single line of code is doing the same thing as all of this.

137
00:09:51,200 --> 00:09:56,210
And we can check it out by running this and we get back the exact same results.

138
00:09:56,210 --> 00:09:58,400
All right that's it for the warm up problems.

139
00:09:58,460 --> 00:10:01,720
Coming up next we're going to go over at level one problems.

140
00:10:01,730 --> 00:10:02,390
We'll see if they're.
