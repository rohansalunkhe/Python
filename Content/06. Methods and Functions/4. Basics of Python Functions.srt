1
00:00:05,350 --> 00:00:06,300
Welcome back, everyone.

2
00:00:06,700 --> 00:00:10,840
In this lecture, we'll jump to Jupiter notebook and start creating basic functions in Python.

3
00:00:11,170 --> 00:00:11,800
Let's get started.

4
00:00:12,190 --> 00:00:12,390
OK.

5
00:00:12,460 --> 00:00:14,560
Here I am at a fresh Jupiter notebook.

6
00:00:14,890 --> 00:00:19,180
As a quick note, the lecture notebook that goes along with this is called Functions.

7
00:00:19,300 --> 00:00:23,440
And throughout this series of lectures, we talk about things like prints versus return, adding in

8
00:00:23,440 --> 00:00:28,870
logic inside a function tuple unpacking, etc. That's all within this singular function's notebook.

9
00:00:28,990 --> 00:00:32,440
So it's a longer notebook that's gonna have several video lectures attached to it.

10
00:00:33,050 --> 00:00:34,420
OK, let's get started.

11
00:00:34,930 --> 00:00:38,570
As you mentioned, you have first have to start off of the D.F. keyword.

12
00:00:38,740 --> 00:00:43,540
Notice that it starts highlighting the special syntax behind it and then we decide on the name of our

13
00:00:43,540 --> 00:00:44,110
function.

14
00:00:44,500 --> 00:00:47,500
For example, let's have a function called Say Hello.

15
00:00:48,100 --> 00:00:50,740
Then we have open and close parentheses and the colon.

16
00:00:51,280 --> 00:00:55,600
And then when you hit enter, you'll notice an automatic indentation, which happens after you click

17
00:00:55,630 --> 00:00:57,370
a colon in Python.

18
00:00:57,490 --> 00:01:01,660
And basically what that means is anything inside this indented block is going to be called when you

19
00:01:01,690 --> 00:01:02,710
execute the function.

20
00:01:03,280 --> 00:01:04,810
For example, if I say prints.

21
00:01:06,520 --> 00:01:06,940
Hello.

22
00:01:08,180 --> 00:01:09,550
Run that then.

23
00:01:09,800 --> 00:01:13,370
If I later on somewhere else, am I code or script call, say hello.

24
00:01:13,850 --> 00:01:19,190
Open and close parentheses, then it's going to execute the block of code inside this function.

25
00:01:19,640 --> 00:01:22,880
That means that if I add more complex code like a bunch of print statements.

26
00:01:23,180 --> 00:01:25,940
Hello, how are you.

27
00:01:26,070 --> 00:01:32,150
For imagine for a second that I wanted this all in three lines for some reason that I no longer need

28
00:01:32,150 --> 00:01:33,920
to write this print statements three times.

29
00:01:34,010 --> 00:01:36,080
Instead I just need to call say hello.

30
00:01:36,110 --> 00:01:36,710
One time.

31
00:01:37,220 --> 00:01:39,760
Now note here the difference between calling.

32
00:01:39,770 --> 00:01:40,540
Say hello.

33
00:01:41,930 --> 00:01:44,390
With princes versus without princes.

34
00:01:44,570 --> 00:01:48,860
If you call it without parentheses, then it's actually not going to execute the function.

35
00:01:49,160 --> 00:01:52,790
Python will just report back that say hello is a function.

36
00:01:53,330 --> 00:01:57,920
Keep that in mind that when we're calling functions in order to execute them, we have to add on this

37
00:01:57,970 --> 00:01:59,090
open and close parentheses.

38
00:01:59,210 --> 00:02:03,200
So if you see something like this, it means you're not actually calling the function you need to add

39
00:02:03,200 --> 00:02:04,060
on the parentheses.

40
00:02:04,520 --> 00:02:04,800
Okay.

41
00:02:05,330 --> 00:02:10,640
So the main point of a function is to execute the block of code, especially when you plan on repeating

42
00:02:10,640 --> 00:02:11,450
that block of code.

43
00:02:13,830 --> 00:02:19,050
And realistically speaking, we're probably going to take some input parameters or input arguments into

44
00:02:19,050 --> 00:02:19,770
our function.

45
00:02:20,280 --> 00:02:21,690
So let's go ahead and continue.

46
00:02:22,500 --> 00:02:23,410
I'm going to overwrite.

47
00:02:23,460 --> 00:02:24,120
Say hello.

48
00:02:26,010 --> 00:02:33,030
And then I'm going to say, let's go ahead and call this name that it takes in some name parameter and

49
00:02:33,030 --> 00:02:36,450
then it's going to print out using enough string literal here.

50
00:02:37,230 --> 00:02:37,740
Hello.

51
00:02:39,230 --> 00:02:39,590
Name.

52
00:02:39,950 --> 00:02:40,760
So what is this doing?

53
00:02:41,210 --> 00:02:46,790
Well, it takes in some argument called that name, and then it's going to insert it right here so that

54
00:02:46,790 --> 00:02:48,020
I will report back.

55
00:02:48,050 --> 00:02:48,310
Hello.

56
00:02:48,320 --> 00:02:54,110
Name, which means if I call, say hello and I pass on the string.

57
00:02:54,270 --> 00:02:56,280
Hosain, it says hello, Hosie.

58
00:02:56,840 --> 00:02:57,950
A couple things to note here.

59
00:02:58,430 --> 00:03:01,010
Technically, I can call this variable whatever I want.

60
00:03:01,250 --> 00:03:06,110
So let's go ahead and show that I could call this jelly and then this would have to match up here.

61
00:03:06,440 --> 00:03:09,530
Jelly, run that and it looks exactly the same.

62
00:03:09,650 --> 00:03:13,610
So typically we should be doing is just providing variables that make sense to you.

63
00:03:13,880 --> 00:03:15,770
In this case, name makes a lot of sense.

64
00:03:16,700 --> 00:03:21,750
The other thing to note here is let's imagine I forgot to passan an actual value.

65
00:03:21,860 --> 00:03:23,090
So I just said say hello.

66
00:03:23,810 --> 00:03:25,760
Well, in this case, Python is going to complain.

67
00:03:26,270 --> 00:03:32,600
And the way to read this error is it will say it's missing one required positional argument called name,

68
00:03:33,080 --> 00:03:39,080
which is essentially trying to tell you, hey, we need to know what name is as an argument in order

69
00:03:39,080 --> 00:03:41,120
to actually execute this block of code.

70
00:03:41,870 --> 00:03:43,160
So how can you fix this?

71
00:03:43,280 --> 00:03:46,370
Well, one way is just to provide the value, as mentioned.

72
00:03:46,850 --> 00:03:49,430
Another way is to provide what's known as a default value.

73
00:03:49,600 --> 00:03:55,430
And you do this with an equal sign up here and you can provide some default value.

74
00:03:55,670 --> 00:03:58,070
So what this means is if name is not provided.

75
00:03:58,430 --> 00:04:00,290
Then we'll go ahead and use this default.

76
00:04:00,380 --> 00:04:03,980
And in this case, we'll just say equals and whatever you want the default string to be.

77
00:04:04,070 --> 00:04:06,230
In this case, I literally just put it as default.

78
00:04:06,680 --> 00:04:11,360
So make sure you keep running these cells in order to overwrite your old function code.

79
00:04:11,930 --> 00:04:14,260
And then let's call, say, Hosie or say hello, who's.

80
00:04:14,660 --> 00:04:15,530
It says hello.

81
00:04:16,050 --> 00:04:18,050
And now what we're gonna do is leave it blank.

82
00:04:18,410 --> 00:04:20,480
Previously, when we left it blank, we got an error.

83
00:04:20,840 --> 00:04:26,270
But now that I reran this function definition and run this cell again, it says hello default.

84
00:04:26,660 --> 00:04:28,280
So no longer produces an error.

85
00:04:28,490 --> 00:04:32,120
Instead, it will reach in and find the default value for that function.

86
00:04:32,510 --> 00:04:32,780
All right.

87
00:04:32,840 --> 00:04:38,450
Now, as we noted in the previous lectures, typically when it comes to a function call, you're not

88
00:04:38,450 --> 00:04:40,190
actually just going to print stuff out.

89
00:04:40,460 --> 00:04:43,430
Instead, you're going to use the return keyword.

90
00:04:43,880 --> 00:04:45,170
So let's see an example of that.

91
00:04:46,550 --> 00:04:51,830
I'm going to create a function called D.F. Adnam, and it takes in two numbers.

92
00:04:52,490 --> 00:04:53,150
Number one.

93
00:04:53,660 --> 00:04:54,350
And number two.

94
00:04:55,470 --> 00:04:56,790
And it's going to return there.

95
00:04:56,840 --> 00:04:58,140
Some return.

96
00:04:58,160 --> 00:04:58,760
Number one.

97
00:04:58,880 --> 00:05:00,500
Plus number two.

98
00:05:00,950 --> 00:05:05,900
And one of the most common questions I get from beginning students is what's the difference between

99
00:05:05,900 --> 00:05:09,590
saying prints in a function versus return in a function?

100
00:05:10,010 --> 00:05:15,410
And the main difference is that return, instead of just printing out the results, is actually going

101
00:05:15,410 --> 00:05:17,810
to allow you to save them as a variable.

102
00:05:18,410 --> 00:05:23,530
So we'll say Adnam here and in Jupiter notebook.

103
00:05:23,780 --> 00:05:29,720
If I would just say something like 10 20 adnam, notice that I see the result anyways.

104
00:05:29,860 --> 00:05:30,220
30.

105
00:05:30,710 --> 00:05:35,750
But because I'm using return, I'm actually able to assign it to a variable.

106
00:05:36,740 --> 00:05:39,260
So I can assign it to a variable instead of seeing the result.

107
00:05:39,650 --> 00:05:43,010
And then call later on somewhere in my code that result.

108
00:05:43,430 --> 00:05:46,010
So let's actually see this between two functions.

109
00:05:47,210 --> 00:05:51,290
We will call one print result that takes in two parameters.

110
00:05:51,540 --> 00:05:53,480
Let's go ahead and call them A and B this time.

111
00:05:54,320 --> 00:05:55,910
And this one just prints out the some.

112
00:05:57,020 --> 00:05:57,800
A plus B.

113
00:05:59,590 --> 00:06:02,500
We'll create a very similar function called return result.

114
00:06:06,140 --> 00:06:12,140
And this one is going to return A plus B this way we can actually see the true difference between them.

115
00:06:12,860 --> 00:06:16,040
So let's first start off by running this print result function.

116
00:06:16,850 --> 00:06:21,590
So I will say prints result I passan 10 and 20.

117
00:06:21,650 --> 00:06:22,870
So Temple's 20 is 30.

118
00:06:23,420 --> 00:06:25,610
And when you run this notice, it prints out 30.

119
00:06:26,090 --> 00:06:31,340
You should also notice that unlike before, there is no output cell showing you that variables being

120
00:06:31,340 --> 00:06:31,770
return.

121
00:06:32,030 --> 00:06:34,310
Instead, it's just printing out the result.

122
00:06:34,970 --> 00:06:39,680
So let's imagine that I wanted to actually save the result 30 to some result variable.

123
00:06:40,310 --> 00:06:43,610
What I cannot do since I'm just using print is the following.

124
00:06:44,390 --> 00:06:47,780
If I say result is equal to print result.

125
00:06:48,200 --> 00:06:50,430
Then Python is just going to print this out.

126
00:06:50,780 --> 00:06:53,390
And it's not actually returning anything to this result.

127
00:06:53,750 --> 00:07:01,280
In fact, if I check result and try to run it, I don't get anything back because the type is none type.

128
00:07:01,490 --> 00:07:05,600
Because again, prints result is not actually returning anything to be saved.

129
00:07:05,960 --> 00:07:11,270
It's simply printing and output and then closing off the function in order to actually do this sort

130
00:07:11,270 --> 00:07:16,430
of thing where I want to save the variable, I would need to return A plus B.

131
00:07:16,850 --> 00:07:18,440
So let's see the return result function.

132
00:07:19,400 --> 00:07:20,600
I say return result.

133
00:07:22,190 --> 00:07:23,090
Ten, common 20.

134
00:07:25,240 --> 00:07:26,640
AC 30 is the output.

135
00:07:27,000 --> 00:07:32,040
Note that Jupiter lets you know something's being returned by showing you this output cell, just signifying

136
00:07:32,040 --> 00:07:33,270
that it's not being printed.

137
00:07:33,360 --> 00:07:34,140
It's being returned.

138
00:07:34,710 --> 00:07:39,270
And now what I can also do as a result is equal to return result.

139
00:07:40,580 --> 00:07:43,820
And now, if I call result, it's actually the number 30.

140
00:07:44,610 --> 00:07:50,190
So, again, just having prints here is not going to allow you to actually grab the result.

141
00:07:50,790 --> 00:07:54,750
Now, that doesn't mean you can't use prints and return together if for some reason.

142
00:07:54,810 --> 00:07:59,820
Keep in mind, is not super common to do this, that you want to see both the printed result and be

143
00:07:59,820 --> 00:08:00,690
able to return it.

144
00:08:01,170 --> 00:08:02,730
Then you could just do something like this.

145
00:08:03,720 --> 00:08:12,180
You could say D.F., call it my funk takes an A plus B, it will go ahead and inform the user by printing

146
00:08:12,180 --> 00:08:16,350
out A plus B and it will return A plus B.

147
00:08:16,800 --> 00:08:18,950
Now, this is not too common to do this.

148
00:08:18,960 --> 00:08:21,150
You usually either just return or just print.

149
00:08:21,270 --> 00:08:22,500
Most common, just return.

150
00:08:22,770 --> 00:08:27,420
But if for some reason you find yourself saying in my large scrip, I want to be able to kind of view

151
00:08:27,420 --> 00:08:28,770
the results as they are coming in.

152
00:08:29,130 --> 00:08:30,180
Then you could print.

153
00:08:30,360 --> 00:08:34,590
And then also return, which kind of allows you to get the best of both worlds, so to speak.

154
00:08:34,710 --> 00:08:35,880
So I could say result.

155
00:08:37,300 --> 00:08:37,990
My function.

156
00:08:38,920 --> 00:08:39,580
10 and 20.

157
00:08:40,390 --> 00:08:43,240
Run this notice that this print gets executed.

158
00:08:43,270 --> 00:08:45,190
But then this return also gets executed.

159
00:08:45,850 --> 00:08:47,890
So now result is equal to 30.

160
00:08:48,340 --> 00:08:52,960
Again, not super common to do this, but when you're debugging sometimes as a beginner, it is useful

161
00:08:52,960 --> 00:08:57,940
to have these print statements within a function so you can see what's going on internally.

162
00:08:59,780 --> 00:09:04,460
Now, the last thing I want to point out in this lecture is the fact that we were just saying A plus

163
00:09:04,460 --> 00:09:05,840
B in the function.

164
00:09:06,200 --> 00:09:09,350
But keep in mind, we were actually checking what the data types were.

165
00:09:10,070 --> 00:09:16,490
So if I called a function some underscore numbers, it takes a number one.

166
00:09:16,940 --> 00:09:18,620
Number two, just as before.

167
00:09:19,210 --> 00:09:24,500
And it returns number one, plus them to nowhere in this.

168
00:09:24,560 --> 00:09:26,850
Am I actually checking to make sure these are numbers.

169
00:09:27,140 --> 00:09:32,180
And if you're coming from another programming language that is statically typed, that is to say, you

170
00:09:32,180 --> 00:09:37,250
have to do make sure and define the data type of a variable before you call a variable.

171
00:09:37,640 --> 00:09:39,050
Then you may not be used to this.

172
00:09:39,200 --> 00:09:44,780
Python is dynamically typed, which means I don't need to specify beforehand what data type I expect.

173
00:09:44,870 --> 00:09:48,560
Number one and number two to be that allows you to program a lot faster.

174
00:09:48,800 --> 00:09:51,230
But it also leaves you open to possible bugs.

175
00:09:51,680 --> 00:09:52,550
Let me show you what I mean.

176
00:09:53,600 --> 00:09:59,470
If I say some numbers and I say something like 10 and 20, I get the result.

177
00:09:59,480 --> 00:10:00,600
I expect 30.

178
00:10:01,100 --> 00:10:02,840
But that can get dangerous.

179
00:10:03,410 --> 00:10:08,600
When I say something like some numbers, 10 as a string and 20 as a string.

180
00:10:09,170 --> 00:10:14,570
Since these are strings, the ADD sign will actually concatenate these together, meaning I get the

181
00:10:14,570 --> 00:10:19,700
result 10 20, which almost looks like a thousand and twenty, which is not what I was expecting here.

182
00:10:20,150 --> 00:10:21,560
And we were taking user input.

183
00:10:22,010 --> 00:10:23,360
It will show up as a string.

184
00:10:23,870 --> 00:10:28,400
So recall from the Useful Operators lecture that the input function returns a string.

185
00:10:29,000 --> 00:10:34,190
So that means if you find yourself creating a function that another user will be interacting with.

186
00:10:34,520 --> 00:10:39,110
You should probably be making sure to check the type of the data before returning the result.

187
00:10:39,380 --> 00:10:42,980
And we're actually going to have a much deeper discussion on this when we start heading towards your

188
00:10:42,980 --> 00:10:44,060
milestone project.

189
00:10:44,420 --> 00:10:45,800
So keep that in mind for now.

190
00:10:46,160 --> 00:10:50,660
And later on, we'll show you different ways of having to deal with this and making sure the right data

191
00:10:50,660 --> 00:10:52,430
type is being returned by a user.

192
00:10:52,850 --> 00:10:54,080
So keep this in mind for now.

193
00:10:54,470 --> 00:10:57,420
Later on, we'll show you various ways of potentially addressing it.

194
00:10:58,500 --> 00:10:59,940
OK, that's it for now.

195
00:11:00,060 --> 00:11:03,840
Coming up next, we'll show you how to add in logic to internal function operations.

196
00:11:04,110 --> 00:11:04,640
I'll see you there.
