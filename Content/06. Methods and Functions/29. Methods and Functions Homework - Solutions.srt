1
00:00:05,290 --> 00:00:08,950
Welcome back, everyone, to this lecture where we're going to be going through the solutions for the

2
00:00:08,950 --> 00:00:11,260
methods and functions, homework exercises.

3
00:00:11,710 --> 00:00:12,410
Let's get started.

4
00:00:12,860 --> 00:00:15,940
OK, here I am at the Functions and Methods Homework Notebook.

5
00:00:16,420 --> 00:00:21,250
The first task we had to do was write a function that computes the volume of a sphere given its radius.

6
00:00:21,640 --> 00:00:26,680
So this is a really common task you may have to do if you work in the STEM field or science or technology

7
00:00:27,040 --> 00:00:31,720
where you have to transform essentially a mathematical equation into actual code.

8
00:00:32,140 --> 00:00:33,580
And this one's a pretty simple example.

9
00:00:33,650 --> 00:00:34,720
So let's go ahead and work through it.

10
00:00:35,320 --> 00:00:37,240
Well, we first do is take the four thirds.

11
00:00:37,420 --> 00:00:43,030
So I'm going to imprint's these here do four thirds to make sure the order of operations ends up being

12
00:00:43,030 --> 00:00:43,510
correct.

13
00:00:43,930 --> 00:00:45,670
Then I'm going to multiply it by PI.

14
00:00:46,560 --> 00:00:51,790
Pi can actually be imported from the math module that we'll learn about later with Python, or you can

15
00:00:51,790 --> 00:00:54,190
just estimate it with three point one four.

16
00:00:54,580 --> 00:00:58,960
So if you wanted to, you could say import or say from math, import PI.

17
00:00:59,290 --> 00:01:04,630
But in this case we'll just say three point one four and then we'll also take the radius and we need

18
00:01:04,630 --> 00:01:05,170
a cubit.

19
00:01:05,320 --> 00:01:09,070
So for exponents, that's going to be two abstracts there.

20
00:01:09,640 --> 00:01:15,880
And we could just say something like result is equal to this and then return result.

21
00:01:16,030 --> 00:01:17,650
But this is actually duplicating a step.

22
00:01:17,670 --> 00:01:20,470
We don't need we don't actually need to save this as a variable.

23
00:01:20,560 --> 00:01:24,040
Instead, we can simply just say, return that.

24
00:01:24,600 --> 00:01:26,260
Please go ahead and check to make sure this worked.

25
00:01:26,800 --> 00:01:28,930
I run this and I get back the correct results.

26
00:01:29,620 --> 00:01:29,770
OK.

27
00:01:29,830 --> 00:01:30,940
Let's move on to the next one.

28
00:01:31,630 --> 00:01:37,210
Next one was to write a function that checks whether a number is in a given range, inclusive of high

29
00:01:37,300 --> 00:01:37,900
and low.

30
00:01:38,380 --> 00:01:40,210
So lots of different ways you can do this.

31
00:01:40,240 --> 00:01:44,710
The main thing I wanted you to be aware of was the fact that you're probably enough to use the range

32
00:01:44,710 --> 00:01:47,950
function and it's inclusive of high and low.

33
00:01:48,250 --> 00:01:51,640
And if you recall, the range function is actually exclusive of high.

34
00:01:52,150 --> 00:01:53,260
So if I take a look at this.

35
00:01:53,800 --> 00:01:58,510
For example, if I say four item in range.

36
00:02:00,320 --> 00:02:01,200
025.

37
00:02:03,910 --> 00:02:04,660
Prince item.

38
00:02:05,140 --> 00:02:08,860
Notice it goes up to bed, not including the last number here, five.

39
00:02:09,280 --> 00:02:14,830
So this is actually inclusive of the first lower bound and exclusive of the upper bound.

40
00:02:15,220 --> 00:02:20,050
So, I mean, if I'm going to use range and I want inclusive of high and low, I may have to do something

41
00:02:20,050 --> 00:02:21,370
like plus one here.

42
00:02:21,470 --> 00:02:22,960
And we'll see that in just a second.

43
00:02:23,560 --> 00:02:25,330
So let's go ahead and check this out.

44
00:02:25,990 --> 00:02:31,870
I'm going to make use of the in operator keyword and recall the in operator is a really useful operator.

45
00:02:31,870 --> 00:02:36,190
We actually cover in these four operators lecture, which just checks for the presence of something

46
00:02:36,190 --> 00:02:36,890
in an iterable.

47
00:02:37,300 --> 00:02:44,230
So I could say five in range, zero to ten and it's going to be true.

48
00:02:44,710 --> 00:02:50,980
Now, if I said in range zero to five, that's going to be false because recall that five here when

49
00:02:50,980 --> 00:02:54,370
I'm using range is going to be exclusive.

50
00:02:54,850 --> 00:03:01,240
So if I'm going to use a check like this, five in range or num in range, low and high, when I'm going

51
00:03:01,240 --> 00:03:04,070
to up, say, high plus one in order to be inclusive of both.

52
00:03:04,510 --> 00:03:05,560
So let's see what that looks like.

53
00:03:06,280 --> 00:03:08,040
I can say the check.

54
00:03:08,080 --> 00:03:08,560
NUM.

55
00:03:10,110 --> 00:03:14,160
In range, and then we're going to do here is say.

56
00:03:15,520 --> 00:03:18,760
Low come a high plus one.

57
00:03:19,480 --> 00:03:24,910
Now, keep in mind, if I just wanted to return the bullion as shown here, then I simply return this.

58
00:03:24,940 --> 00:03:28,750
I'll say return num in range.

59
00:03:28,850 --> 00:03:29,250
Low.

60
00:03:29,350 --> 00:03:34,570
High plus one, because I'm checking if this number is within the range between low and high inclusive.

61
00:03:34,840 --> 00:03:36,160
That's why I added the plus one there.

62
00:03:36,610 --> 00:03:37,720
So if I run this.

63
00:03:38,820 --> 00:03:43,650
Then I have the bullying check ready to go, if I want some sort of print statement, then I can simply

64
00:03:43,650 --> 00:03:44,970
just use an if statement.

65
00:03:45,000 --> 00:03:47,130
I'll say if this happens to be true.

66
00:03:47,730 --> 00:03:53,220
Go ahead and make some sort of print functionality here so I can say something like, we use F string

67
00:03:53,220 --> 00:03:53,940
literals here.

68
00:03:54,900 --> 00:03:57,150
Let's say num lips.

69
00:03:58,880 --> 00:04:01,580
Is in range.

70
00:04:02,620 --> 00:04:07,030
Of low and high, then I'll say else.

71
00:04:08,200 --> 00:04:08,680
Prince.

72
00:04:10,170 --> 00:04:10,860
Not in range.

73
00:04:10,920 --> 00:04:16,200
And you can obviously write whatever you want here, but we run this and we can see five is in range

74
00:04:16,200 --> 00:04:17,070
of low and high.

75
00:04:17,220 --> 00:04:22,530
And if you want, you can keep replacing these with actual low and actual high.

76
00:04:23,860 --> 00:04:26,620
So you run that and says five is in range of two and seven.

77
00:04:26,740 --> 00:04:30,700
And this is just F string literal printing, we cover that in the print formatting lecture.

78
00:04:31,450 --> 00:04:32,530
All right, moving along.

79
00:04:33,910 --> 00:04:39,160
Next, we wanted to write a python function that accepts a string and calculates the number of uppercase

80
00:04:39,160 --> 00:04:41,110
letters and lowercase letters.

81
00:04:41,530 --> 00:04:44,170
And this is where we're going to use a for loop within the function.

82
00:04:44,710 --> 00:04:48,640
So I can also make use of a dictionary in order to keep count.

83
00:04:49,330 --> 00:04:53,410
So if you feel ambitious, you can also explore the collections module, which is something we cover

84
00:04:53,470 --> 00:04:54,640
later on in the course.

85
00:04:54,970 --> 00:04:58,630
I'll go ahead and just show you how to solve this, given what we already know so far.

86
00:04:59,470 --> 00:05:05,110
So the first thing to do is figure out something to keep track of the number of uppercase and lowercase

87
00:05:05,110 --> 00:05:11,860
letters so I can have some sort of counter such as to integers that start at zero and then start counting

88
00:05:11,860 --> 00:05:12,100
them.

89
00:05:12,490 --> 00:05:14,290
Or I can use something like a dictionary.

90
00:05:14,680 --> 00:05:18,790
So I'll first show you with two separate variables and then I'll show you the dictionary method.

91
00:05:19,870 --> 00:05:20,920
So let's go ahead and say.

92
00:05:22,320 --> 00:05:29,630
Lower case starts off at zero and then uppercase starts off at zero and one, are you going to do is

93
00:05:29,630 --> 00:05:30,050
the following.

94
00:05:30,080 --> 00:05:34,700
I'll simply say Hurby character in my string s.

95
00:05:36,300 --> 00:05:40,080
If and this is where the is upper and is lower comes into play.

96
00:05:40,110 --> 00:05:44,940
These essentially return of bullying, if it's upper case or lower case, I'll say if the character.

97
00:05:46,780 --> 00:05:48,010
Is upper case.

98
00:05:49,400 --> 00:05:54,050
Then go ahead and add one to my lower vs. assuming my upper case count flips.

99
00:05:55,590 --> 00:06:01,530
So we'll say upper case plus equals one, which is same as saying upper case is equal to upper case

100
00:06:01,530 --> 00:06:02,070
plus one.

101
00:06:03,060 --> 00:06:04,470
And then I'll have another check here.

102
00:06:05,040 --> 00:06:08,280
I'll say L.F. character is lower.

103
00:06:09,900 --> 00:06:11,520
Then go ahead and say.

104
00:06:13,070 --> 00:06:13,610
Case.

105
00:06:14,880 --> 00:06:16,170
Plus equals one.

106
00:06:17,190 --> 00:06:21,540
And then finally, if I say else, so, for example, I could have punctuation.

107
00:06:22,110 --> 00:06:24,810
If we take a look at the result of that, let me add in a cell here.

108
00:06:25,350 --> 00:06:30,930
If we have the string question mark and I say is upper, it's going to say false.

109
00:06:31,170 --> 00:06:36,750
But I also say is lower than it's also false because technically a question mark isn't really upper

110
00:06:36,750 --> 00:06:41,670
case or lower case, which means there's gonna be a third option here where neither of these happens

111
00:06:41,670 --> 00:06:42,180
to be true.

112
00:06:42,780 --> 00:06:44,970
And if that happens to be the case, I will simply pass.

113
00:06:45,240 --> 00:06:50,340
So I'm adding once the count, if it's upper case or adding one to the count of slower case.

114
00:06:50,490 --> 00:06:54,950
If I happen to have punctuation there, I'll simply pass once this entire for loop is done.

115
00:06:55,140 --> 00:06:57,360
I can print out the results as shown here.

116
00:06:58,020 --> 00:07:01,950
So we want to make sure that we align this with the four loops or for Loop executes first.

117
00:07:02,610 --> 00:07:04,770
And I will go ahead and print out the original string.

118
00:07:05,610 --> 00:07:06,270
So we'll say.

119
00:07:08,130 --> 00:07:13,350
Original string, and then it was just s print out.

120
00:07:15,270 --> 00:07:16,330
Lower case count.

121
00:07:18,200 --> 00:07:20,750
That's going to be lower case.

122
00:07:20,900 --> 00:07:22,400
Make sure we have an F string there.

123
00:07:23,450 --> 00:07:25,400
And then finally print out upper case count.

124
00:07:27,760 --> 00:07:29,900
Uppercase Count Ulgen, just be uppercase.

125
00:07:31,940 --> 00:07:32,720
So we run this.

126
00:07:32,810 --> 00:07:33,830
Let's check if it works.

127
00:07:34,850 --> 00:07:37,160
And looks like we got the same results actually flip this.

128
00:07:37,160 --> 00:07:39,650
So Silver case 33, uppercase four.

129
00:07:39,980 --> 00:07:40,940
But that was the same as here.

130
00:07:40,990 --> 00:07:42,820
Upper case for lower case 33.

131
00:07:43,280 --> 00:07:45,050
So it looks like this is working correctly.

132
00:07:45,560 --> 00:07:49,850
Now, I mention you could also do this of a dictionary if you're dealing with lots of things to keep

133
00:07:49,850 --> 00:07:50,450
track of.

134
00:07:50,720 --> 00:07:51,550
It might be helpful.

135
00:07:51,550 --> 00:07:56,450
Then instead of all these being separate variables, you just transform this into a dictionary instead.

136
00:07:56,870 --> 00:07:58,850
So what you mean by that is setting up a dictionary.

137
00:07:59,540 --> 00:08:03,000
So we'll call it Dictionary D that happens to have key values.

138
00:08:03,030 --> 00:08:03,860
Wolesi Upper.

139
00:08:05,140 --> 00:08:07,450
Zero lower.

140
00:08:09,040 --> 00:08:09,520
Zero.

141
00:08:09,670 --> 00:08:14,800
And then instead of having lower case no case here, we simply take the counts at that key.

142
00:08:14,830 --> 00:08:16,750
So we'll say the upper.

143
00:08:18,770 --> 00:08:19,550
And these lower.

144
00:08:19,700 --> 00:08:24,140
And when you're start to get things that are more complex or larger, it becomes really nice just to

145
00:08:24,140 --> 00:08:27,350
have this single object you can reference with these key calls.

146
00:08:27,800 --> 00:08:32,630
And then we simply need to swap this out lowercase an uppercase for the upper and the lower.

147
00:08:32,970 --> 00:08:35,030
Make sure you keep an eye on what kind of quotes you're using.

148
00:08:35,090 --> 00:08:36,200
Otherwise, you get an error.

149
00:08:36,800 --> 00:08:40,460
So since I'm using single quotes on the outside, I'll use double quotes here on the inside.

150
00:08:40,580 --> 00:08:42,710
So this is going to be lower.

151
00:08:43,430 --> 00:08:45,980
And then in here we'll say this is.

152
00:08:47,080 --> 00:08:47,470
Upper.

153
00:08:49,100 --> 00:08:49,520
There we go.

154
00:08:49,610 --> 00:08:53,480
So we go ahead and run this and we get back the exact same results up to you.

155
00:08:53,510 --> 00:08:54,560
Both methods are correct.

156
00:08:54,860 --> 00:08:56,870
I personally prefer the sort of dictionary method.

157
00:08:57,470 --> 00:08:57,650
OK.

158
00:08:57,740 --> 00:08:58,940
Let's go ahead and continue.

159
00:09:01,960 --> 00:09:07,750
Next task was to write a python function that takes a list and returns a new list of the unique elements

160
00:09:07,840 --> 00:09:09,010
of the first list.

161
00:09:09,490 --> 00:09:11,770
So basically we just want to get rid of the repeats.

162
00:09:11,950 --> 00:09:15,040
Now, there's one very quick way to do this, which is to simply.

163
00:09:16,370 --> 00:09:16,970
Return.

164
00:09:18,080 --> 00:09:23,720
If you say set list and run this, that returns the set, which means I can then easily transform that

165
00:09:24,320 --> 00:09:27,140
into a list by casting it into a list.

166
00:09:27,680 --> 00:09:28,490
And there you have it.

167
00:09:28,550 --> 00:09:31,250
This probably the simplest and most Pythonic way of doing this.

168
00:09:31,610 --> 00:09:36,920
However, I do want to show you kind of a more logical and manual way of doing this just so you can

169
00:09:36,920 --> 00:09:40,700
get used to using for loops and if statements within a function.

170
00:09:41,330 --> 00:09:46,520
So another way we could have done this is to basically have a placeholder list that then we're going

171
00:09:46,520 --> 00:09:49,160
to append items that we haven't seen before.

172
00:09:49,940 --> 00:09:50,600
So we'll say.

173
00:09:51,890 --> 00:09:52,360
For.

174
00:09:55,460 --> 00:09:57,410
Number in that list.

175
00:10:00,140 --> 00:10:02,450
If that number is not.

176
00:10:03,810 --> 00:10:07,670
I don't need to say is not in X.

177
00:10:08,900 --> 00:10:10,460
So, in fact, let's just call X.

178
00:10:13,340 --> 00:10:14,390
Scene numbers.

179
00:10:14,840 --> 00:10:18,710
So if that number is not in the numbers I've already seen.

180
00:10:20,730 --> 00:10:22,260
Then I will simply say.

181
00:10:25,280 --> 00:10:26,210
CNN numbers.

182
00:10:27,330 --> 00:10:28,530
Append the number.

183
00:10:29,380 --> 00:10:30,440
I don't want this for loop.

184
00:10:31,990 --> 00:10:32,620
Is completed.

185
00:10:33,640 --> 00:10:35,230
I will simply return the list.

186
00:10:36,430 --> 00:10:37,090
Seen numbers.

187
00:10:37,120 --> 00:10:39,550
And if I run that, I get back the exact same results.

188
00:10:40,000 --> 00:10:42,790
So essentially what I'm doing here is I have this list.

189
00:10:43,030 --> 00:10:45,250
It's numbers I've already seen starts off as empty.

190
00:10:45,700 --> 00:10:48,530
And then for every number in my original samples.

191
00:10:48,700 --> 00:10:49,990
So for one, one.

192
00:10:49,990 --> 00:10:50,920
One, two, two.

193
00:10:51,040 --> 00:10:51,610
And so on.

194
00:10:52,120 --> 00:10:58,150
If that number is not already present inside this unique list, I am building this see numbers.

195
00:10:58,480 --> 00:10:59,950
Then I'll go ahead and append it.

196
00:11:00,490 --> 00:11:04,540
And then once this entire four loop is done running, I'll go ahead and return.

197
00:11:04,630 --> 00:11:07,240
Seen numbers, computationally speaking.

198
00:11:07,300 --> 00:11:10,060
This is not nearly as efficient as the previous way we saw.

199
00:11:10,570 --> 00:11:13,000
So keep that in mind now.

200
00:11:13,630 --> 00:11:17,620
Next is to write a python function to multiply all the numbers in a list.

201
00:11:18,190 --> 00:11:24,340
Later on, we'll actually learn about things like mapping, filtering and reducing, which is which

202
00:11:24,340 --> 00:11:27,280
essentially are built in functions in Python that allow you to do this quite easily.

203
00:11:27,580 --> 00:11:29,470
But for now, we'll go ahead and do this manually.

204
00:11:30,010 --> 00:11:31,090
So what do I need to do?

205
00:11:31,420 --> 00:11:33,700
It's actually a really similar problem to what we had here.

206
00:11:34,060 --> 00:11:36,370
We're going to start off some sort of base case.

207
00:11:36,430 --> 00:11:40,630
Use a for loop to adjust it and then return that final thing.

208
00:11:40,690 --> 00:11:45,310
So what we're gonna do here is start off with total equal to one.

209
00:11:45,550 --> 00:11:47,410
And the reason for that being is one times.

210
00:11:47,470 --> 00:11:50,670
Anything is going to be the resulting number.

211
00:11:50,800 --> 00:11:53,560
So what I mean by that is one times N is equal to N.

212
00:11:53,950 --> 00:11:55,120
So one times one hundred is equal.

213
00:11:55,120 --> 00:11:56,560
One hundred, one times one hundred is equal.

214
00:11:56,560 --> 00:11:57,520
Two hundred and so on.

215
00:11:58,060 --> 00:12:02,890
So then all you need to do is for every number or every num.

216
00:12:04,310 --> 00:12:08,670
In numbers, take my total and then.

217
00:12:10,380 --> 00:12:14,760
Multiply it by the current number after that return, the total.

218
00:12:15,240 --> 00:12:19,530
So, again, because we're using multiplication, I'm to start off the total is equal to one.

219
00:12:20,130 --> 00:12:25,530
If the ask was to add all of these, then I would've started with zero.

220
00:12:26,340 --> 00:12:28,040
So I'll say four num numbers.

221
00:12:28,440 --> 00:12:30,210
The total is equal to the current total.

222
00:12:30,450 --> 00:12:31,200
Times that number.

223
00:12:31,890 --> 00:12:34,850
So if I run this notice, I get back same results.

224
00:12:35,220 --> 00:12:36,960
And if I want, I can keep adding to this.

225
00:12:36,990 --> 00:12:39,890
So if I had a 10 year notice, I get negative to 40.

226
00:12:40,290 --> 00:12:43,830
So there's going to adjust to however long my list happens to be.

227
00:12:44,640 --> 00:12:49,770
Next, we wanted to write a python function that checks whether a word or phrase is a palindrome or

228
00:12:49,770 --> 00:12:50,190
not.

229
00:12:50,880 --> 00:12:51,990
So how can we do this?

230
00:12:52,050 --> 00:12:53,580
We essentially need to do two things.

231
00:12:53,710 --> 00:12:54,010
One.

232
00:12:54,510 --> 00:12:57,330
The first one is remove the spaces from the string.

233
00:12:59,080 --> 00:13:01,540
And that's because when it comes to a phrase palindrome.

234
00:13:01,930 --> 00:13:07,330
Notice this space will technically ruin it if we were to immediately reverse this, because we would

235
00:13:07,330 --> 00:13:07,570
get.

236
00:13:07,660 --> 00:13:10,180
And you are space and then everything else.

237
00:13:10,480 --> 00:13:13,060
When going forwards, there's technically no space after.

238
00:13:13,060 --> 00:13:13,690
And you are.

239
00:13:14,080 --> 00:13:18,820
So we should do is remove that space and then check.

240
00:13:20,240 --> 00:13:24,620
If string is equals, that's double equals there.

241
00:13:24,650 --> 00:13:25,700
So check for equality.

242
00:13:26,030 --> 00:13:29,870
If the string is equal to the reversed version of the string.

243
00:13:31,010 --> 00:13:32,270
OK, so can we do this?

244
00:13:32,390 --> 00:13:36,530
Well, to remove spaces in the string, we hinted that you should check out the replace method.

245
00:13:37,100 --> 00:13:38,180
So really easy to use here.

246
00:13:38,240 --> 00:13:41,960
I'm simply going to say this is equal to s stop replace.

247
00:13:42,380 --> 00:13:44,570
And I'm going to replace all spaces.

248
00:13:44,660 --> 00:13:46,850
So that's quotes with a space there.

249
00:13:47,810 --> 00:13:50,900
With an empty string, so essentially no space.

250
00:13:51,320 --> 00:13:53,930
So notice here, there's a space on this first one.

251
00:13:53,990 --> 00:13:57,860
So says replace all spaces with nothing and empty string.

252
00:13:58,420 --> 00:14:00,770
And I want to show you what the effects of just doing that are.

253
00:14:01,280 --> 00:14:07,790
So if I return s here and I run this, notice it, remove the space from nurses, run.

254
00:14:08,510 --> 00:14:11,680
Next step is to check if that string this one nurse is running out.

255
00:14:11,690 --> 00:14:14,450
The space is equal to the reverse version of the string.

256
00:14:15,050 --> 00:14:16,790
So lots different ways you can do this.

257
00:14:16,850 --> 00:14:21,320
But if you Google search how to reverse a string in Python, there's kind of a really famous method,

258
00:14:21,860 --> 00:14:24,410
which is to do the following check.

259
00:14:24,510 --> 00:14:26,100
S is equal to.

260
00:14:26,240 --> 00:14:30,110
And then the reverse is going to be s with sliced notation.

261
00:14:30,440 --> 00:14:30,820
Colon.

262
00:14:30,830 --> 00:14:31,330
Colon.

263
00:14:31,700 --> 00:14:32,690
Negative one.

264
00:14:33,230 --> 00:14:34,430
So what is that actually doing.

265
00:14:35,950 --> 00:14:42,100
S colon, colon, negative one here is saying starting all the way from the beginning, going all the

266
00:14:42,100 --> 00:14:42,790
way to the end.

267
00:14:43,450 --> 00:14:49,000
Grabbing the string and a step size of negative one recall, we can grab the string in various step

268
00:14:49,000 --> 00:14:49,510
sizes.

269
00:14:49,600 --> 00:14:54,640
So if we grab in step size of two, that means you take a step size from the beginning of two.

270
00:14:54,730 --> 00:14:57,400
So it'd be every other letter in this string.

271
00:14:57,880 --> 00:15:02,620
If I do a step size of negative one, that means start all the way at the end.

272
00:15:02,770 --> 00:15:09,700
And then work backwards so I can simply return the results of this check.

273
00:15:11,580 --> 00:15:14,560
And when I run this, I get back.

274
00:15:14,770 --> 00:15:15,610
True or false?

275
00:15:15,700 --> 00:15:17,800
So now this works with a full phrase.

276
00:15:18,250 --> 00:15:23,920
And regardless of where this spaces, so fact, I can kind of put spaces wherever this will remove all

277
00:15:23,920 --> 00:15:24,250
of them.

278
00:15:24,340 --> 00:15:26,710
And this will check if it's actually palindrome.

279
00:15:27,340 --> 00:15:28,530
So note that still returns.

280
00:15:28,550 --> 00:15:28,780
True.

281
00:15:30,260 --> 00:15:35,810
And then finally, the hard one here was to write a python function to check whether a string is a pan

282
00:15:35,840 --> 00:15:36,650
gram or not.

283
00:15:37,190 --> 00:15:40,640
And you should also assume the string past end does not have any punctuation.

284
00:15:41,300 --> 00:15:45,080
So pan grams are words or sentences containing every letter of the alphabet.

285
00:15:45,200 --> 00:15:46,910
At least one time.

286
00:15:47,360 --> 00:15:48,620
So that's kind of a key word here.

287
00:15:48,680 --> 00:15:49,970
At least one time.

288
00:15:50,240 --> 00:15:53,510
Which means I only need to check if that letter occurs just once.

289
00:15:54,380 --> 00:15:54,640
Okay.

290
00:15:54,800 --> 00:15:58,640
Now, there's lots of different ways you can do this as long as you got the same results as us.

291
00:15:59,060 --> 00:16:00,830
Don't worry if you did it a different way.

292
00:16:01,070 --> 00:16:07,220
But basically, you should get true for the quick brown fox jumps over the lazy dog and falls for a

293
00:16:07,220 --> 00:16:09,200
sentence that doesn't have all letters of the alphabet.

294
00:16:09,710 --> 00:16:13,040
I want to show you how you can do this with a simple set comparison.

295
00:16:13,670 --> 00:16:17,930
So we'll come back here and we'll plan out what this function is going to do.

296
00:16:19,270 --> 00:16:23,520
The way we're gonna need to do this and keep in mind is lots of different ways you can do this is I'm

297
00:16:23,530 --> 00:16:27,760
first going to create a set of the entire alphabet.

298
00:16:29,320 --> 00:16:29,680
Then.

299
00:16:30,700 --> 00:16:35,350
I'm going to remove any spaces from the input string.

300
00:16:37,640 --> 00:16:40,280
Then I'm going to convert the input string.

301
00:16:42,700 --> 00:16:43,760
Into all lowercase.

302
00:16:45,220 --> 00:16:48,850
Because what I'm using for the comparison is a lowercase strings.

303
00:16:51,780 --> 00:17:00,240
And then what I'm going to do is I'm going to grab all unique letters from the string because recall,

304
00:17:00,300 --> 00:17:02,790
I only care that they show up at least once.

305
00:17:02,820 --> 00:17:08,850
Which means if I have two B's or two A's, I only really care about their uniqueness that H just happened

306
00:17:08,850 --> 00:17:10,410
to show up at least one time.

307
00:17:10,890 --> 00:17:13,080
So a nice way to do this would be with a set.

308
00:17:14,510 --> 00:17:18,440
And since I'm going to have a set of the alphabet and a set of all the unique letters and lowercase,

309
00:17:18,860 --> 00:17:25,760
then I just compare my alphabet set to check if it's equal to my string set from the input.

310
00:17:26,630 --> 00:17:29,840
So let's check this out to create a set of the alphabet.

311
00:17:30,080 --> 00:17:31,520
I will call this Alpha set.

312
00:17:32,720 --> 00:17:34,700
And say, set alphabet.

313
00:17:36,770 --> 00:17:39,950
Next, to remove any space from the input stream, we actually already know how to do that.

314
00:17:40,490 --> 00:17:45,260
It simply call replace all spaces with nothing.

315
00:17:46,070 --> 00:17:47,840
Then we're going to convert them all into lowercase.

316
00:17:48,110 --> 00:17:53,270
So that's SDR one thought or is equal to SDR, one that lower.

317
00:17:54,910 --> 00:17:58,030
And then we'll say SDR one is equal to the set.

318
00:17:59,120 --> 00:17:59,970
Of US Tier one.

319
00:18:01,010 --> 00:18:07,340
And then finally, we're going to return the check, the tier one is equal to offset.

320
00:18:08,320 --> 00:18:11,630
So to happen here, I create a set of the alphabet with the Senate.

321
00:18:12,350 --> 00:18:17,410
Remove any spaces, lowercase everything, then converted it into a set.

322
00:18:17,860 --> 00:18:21,420
And now I'm comparing my offset along with my string set.

323
00:18:21,940 --> 00:18:24,180
So now when I run this, I get back.

324
00:18:24,190 --> 00:18:24,540
True.

325
00:18:25,060 --> 00:18:30,460
And if any of these steps happens to confuse you, a really nice way to visualize things is with print

326
00:18:30,460 --> 00:18:31,000
statements.

327
00:18:31,090 --> 00:18:36,940
So if you maybe aren't clear where Alpha set is, you can simply print it out when you call the function.

328
00:18:37,720 --> 00:18:41,830
And when you run this, you'll get to see that entire alpha set along with what is being returned.

329
00:18:42,190 --> 00:18:43,810
So you can use that at any point in time.

330
00:18:44,280 --> 00:18:47,290
So let's say you're confused what step you're at right after this.

331
00:18:47,320 --> 00:18:54,760
You'll simply say print as tier one as a really great way to learn as a kind of beginner.

332
00:18:54,850 --> 00:18:59,080
So now we can see at this point in time, everything's lower case and there are no more spaces.

333
00:18:59,380 --> 00:19:03,220
So I highly encourage you as you're learning to use the sort of print statements to help you kind of

334
00:19:03,220 --> 00:19:06,550
debug or understand what's going on in the larger functions.

335
00:19:07,180 --> 00:19:07,450
All right.

336
00:19:08,020 --> 00:19:08,440
That's it.

337
00:19:08,740 --> 00:19:09,280
Great job.

338
00:19:09,400 --> 00:19:10,390
And I'll see you at the next lecture.
