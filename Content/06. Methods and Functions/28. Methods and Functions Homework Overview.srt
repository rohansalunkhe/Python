1
00:00:05,250 --> 00:00:06,210
Welcome back, everyone.

2
00:00:06,480 --> 00:00:10,770
We're going to continue to test your skills with methods and functions and what we've labeled a homework

3
00:00:10,770 --> 00:00:15,330
assignment, but really can do it whenever it's a notebook, methods and functions, homework.

4
00:00:15,600 --> 00:00:18,540
So let's go ahead and do a quick overview of the notebook in this lecture.

5
00:00:18,800 --> 00:00:21,600
And in the next lecture, we'll go through some example solutions.

6
00:00:21,930 --> 00:00:23,160
Let me hop over to that notebook.

7
00:00:23,470 --> 00:00:24,210
OK, here I am.

8
00:00:24,240 --> 00:00:26,930
And the functions and methods homework notebook.

9
00:00:27,420 --> 00:00:30,720
So this is zero eight dash functions and methods homework.

10
00:00:31,140 --> 00:00:34,770
And so basically we wanted to do is answer the following questions and they're pretty much all in the

11
00:00:34,770 --> 00:00:35,850
form of a function.

12
00:00:36,360 --> 00:00:41,220
Sue knows this first one here says Write a function that computes the volume of a sphere given its radius.

13
00:00:41,610 --> 00:00:46,950
So in case you don't know that formula, it's four over three times PI times are cubed.

14
00:00:47,340 --> 00:00:52,590
And what we've done here is essentially we have these placeholder functions with just a simple pass

15
00:00:52,590 --> 00:00:53,010
statement.

16
00:00:53,370 --> 00:00:55,680
And it's your job to fill out this function.

17
00:00:56,130 --> 00:01:01,320
And then you should be able to check it with the second cell and make sure you get a corresponding correct

18
00:01:01,320 --> 00:01:01,770
value.

19
00:01:02,190 --> 00:01:03,840
So then go ahead and fill out here.

20
00:01:03,900 --> 00:01:08,070
This function called volle or volume, whatever you want to call it, and then run it to check.

21
00:01:08,550 --> 00:01:08,790
OK.

22
00:01:09,570 --> 00:01:14,520
Next one is to write a function that checks whether a number is in a given range, inclusive of high

23
00:01:14,550 --> 00:01:15,060
and low.

24
00:01:15,420 --> 00:01:17,340
So you should include that high and low number.

25
00:01:17,820 --> 00:01:21,210
So for example, we're gonna take in the number low and high.

26
00:01:21,750 --> 00:01:26,820
And what we should be able to do is check if five is in the given range between two and seven.

27
00:01:27,420 --> 00:01:30,770
And however you want to return, the results is OK with me.

28
00:01:31,080 --> 00:01:36,240
If you want, you can print out a statement saying five is in the range or you can print out something

29
00:01:36,240 --> 00:01:38,160
like 10 is outside the range.

30
00:01:38,700 --> 00:01:40,950
Alternatively, you can just return a boolean.

31
00:01:41,370 --> 00:01:42,240
So it can return.

32
00:01:42,240 --> 00:01:43,140
True or false.

33
00:01:43,530 --> 00:01:45,120
See if you can write it both ways.

34
00:01:45,150 --> 00:01:48,660
One that returns this print statement and one that returns the boolean.

35
00:01:50,190 --> 00:01:55,410
Next, we want you to write a python function that accepts a string and calculates the number of uppercase

36
00:01:55,410 --> 00:01:57,060
letters in lowercase letters.

37
00:01:57,570 --> 00:02:01,410
So an example string could be something like, hello, Mr. Rogers, how are you?

38
00:02:01,410 --> 00:02:02,370
This fine Tuesday.

39
00:02:02,850 --> 00:02:04,380
So we have hello.

40
00:02:04,410 --> 00:02:06,570
Capitalized and capitalized here.

41
00:02:06,600 --> 00:02:08,870
Rogers capitalized and then t capitalized.

42
00:02:09,330 --> 00:02:15,300
And we want you to do is essentially go through this string and then count the number of uppercase letters

43
00:02:15,300 --> 00:02:15,900
and print it out.

44
00:02:16,320 --> 00:02:18,270
And the number of lowercase letters and prints it out.

45
00:02:18,750 --> 00:02:23,850
Now there's two methods that will be very useful for this, which is the is upper method and the is

46
00:02:23,910 --> 00:02:24,600
lower method.

47
00:02:24,630 --> 00:02:30,000
You can call this on a string or a single letter and check if it's upper case or lower case.

48
00:02:30,490 --> 00:02:32,700
I'll let you to figure out the best way to run that.

49
00:02:33,330 --> 00:02:36,220
And again, here it is, the function with a placeholder.

50
00:02:36,630 --> 00:02:39,870
And when you run this, it should put out an output, something similar to this.

51
00:02:40,550 --> 00:02:40,770
OK.

52
00:02:41,960 --> 00:02:46,880
Next, we want you to write a python function that takes in a list and returns a new list with unique

53
00:02:46,940 --> 00:02:48,530
elements of the first list.

54
00:02:49,070 --> 00:02:50,870
So, for example, here we have the list.

55
00:02:51,350 --> 00:02:53,990
Repeated ones, twos, threes, and then a four and a five.

56
00:02:54,440 --> 00:02:56,350
So we just want the unique elements.

57
00:02:56,380 --> 00:02:58,250
So one, two, three, four, five.

58
00:02:58,700 --> 00:03:04,730
And if you recall from our previous lectures on data types, there's a specific data type that could

59
00:03:04,730 --> 00:03:10,280
be helpful for you in order to grab the unique elements from an era iterable object like a list.

60
00:03:10,400 --> 00:03:12,320
So I'll kind of leave that to you to figure out.

61
00:03:12,500 --> 00:03:15,500
But essentially, the output should look something like this, just the unique elements.

62
00:03:15,830 --> 00:03:17,780
And note, we want to in a list form.

63
00:03:19,450 --> 00:03:23,020
After that, we want you to write a python function to multiply all the numbers in the list.

64
00:03:23,410 --> 00:03:24,760
So you have a sample list.

65
00:03:24,940 --> 00:03:25,780
One, two, three.

66
00:03:25,810 --> 00:03:26,920
And then here, negative four.

67
00:03:27,400 --> 00:03:31,720
And essentially, what should be happening here is the output will be one times, two times, three

68
00:03:31,720 --> 00:03:32,560
times negative four.

69
00:03:32,680 --> 00:03:36,580
So in this case, one times two is two times three is six times negative.

70
00:03:36,580 --> 00:03:37,470
Four is negative.

71
00:03:37,630 --> 00:03:37,990
Twenty four.

72
00:03:38,470 --> 00:03:42,820
And this should be able to work for a list regardless of how long the list is.

73
00:03:43,090 --> 00:03:46,570
So this should work for a list of two items, up to a million items.

74
00:03:46,870 --> 00:03:49,060
So make sure your code is robust in that sense.

75
00:03:49,510 --> 00:03:54,610
The next question is to write a python function that checks whether a word or phrase is a palindrome

76
00:03:54,670 --> 00:03:55,060
or not.

77
00:03:55,270 --> 00:03:59,320
And in case you're not familiar with what a palindrome is, it's a word, phrase or sequence that reads

78
00:03:59,320 --> 00:04:01,180
the same way backwards or forwards.

79
00:04:01,480 --> 00:04:05,140
So single words can be something like madam or kayak or race car.

80
00:04:05,410 --> 00:04:09,930
If you read those backwards, it's the same thing or can be a phrase like nurses run.

81
00:04:10,480 --> 00:04:15,130
And as a quick kintz, when you're dealing with the phrases, you may want to check out the replace

82
00:04:15,190 --> 00:04:18,400
method in a string to help out with dealing with spaces.

83
00:04:18,880 --> 00:04:23,290
You can also Google search how to reverse a string in Python, because essentially we're trying to do

84
00:04:23,380 --> 00:04:25,840
is compare a string against it's reverse.

85
00:04:25,990 --> 00:04:27,820
So some things to keep in mind.

86
00:04:29,820 --> 00:04:34,380
And then finally, this one's a little hard, but we want you to write a python function to check whether

87
00:04:34,380 --> 00:04:36,600
a string is a pan graham or not.

88
00:04:36,960 --> 00:04:40,980
And it's gonna be a couple of things you'll have to check out, including the string module, which

89
00:04:41,010 --> 00:04:43,650
isn't exactly something we covered in the previous lecture.

90
00:04:44,040 --> 00:04:46,620
So here we want you to expand your capabilities.

91
00:04:46,980 --> 00:04:52,890
Essentially being able to do a Google search or a stack overflow search to compare or grab the full

92
00:04:52,890 --> 00:04:54,750
alphabet in Python.

93
00:04:54,840 --> 00:04:59,550
And we gave kind of a link here for you to check out, which is the string that A.

94
00:04:59,550 --> 00:04:59,780
S.

95
00:05:00,080 --> 00:05:01,740
I underscore lowercase.

96
00:05:02,040 --> 00:05:04,380
So see if you can Google search and figure out what this is doing.

97
00:05:05,190 --> 00:05:07,130
But essentially, what we want you to do is a pan.

98
00:05:07,140 --> 00:05:12,540
Graham is a word or sentence containing every letter of the alphabet at least once.

99
00:05:12,960 --> 00:05:18,570
So, for example, here we can see the quick brown fox jumps over the lazy dog.

100
00:05:19,270 --> 00:05:21,710
And again, notice here we said a hint.

101
00:05:21,750 --> 00:05:22,920
Look at the string module.

102
00:05:23,340 --> 00:05:24,750
This string that A.

103
00:05:24,870 --> 00:05:27,130
S see, I underscore lowercase.

104
00:05:27,510 --> 00:05:33,480
If you check it out and we kind of showed you here what it does, it actually is a string of the entire

105
00:05:33,480 --> 00:05:35,130
alphabet in lowercase.

106
00:05:35,730 --> 00:05:39,720
One last point I want to make here is for the sake of simplicity.

107
00:05:40,110 --> 00:05:42,870
Go ahead and assume there's no punctuation.

108
00:05:43,200 --> 00:05:44,670
Pastan to the string here.

109
00:05:45,060 --> 00:05:48,030
So, again, don't worry about the edge case of punctuation.

110
00:05:48,390 --> 00:05:53,490
Assume that the string can have both uppercase and lowercase letters, but no punctuation.

111
00:05:54,390 --> 00:05:55,500
All right, thanks.

112
00:05:55,740 --> 00:05:58,530
And we'll see you at the next lecture where we'll go over the solutions.
