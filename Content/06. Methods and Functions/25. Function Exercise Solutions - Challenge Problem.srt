1
00:00:05,460 --> 00:00:07,430
Welcome back everyone in this lecture.

2
00:00:07,440 --> 00:00:11,890
We're going to finish off by going over the challenging problems and we're going to start off with Spike

3
00:00:11,890 --> 00:00:12,660
again.

4
00:00:12,810 --> 00:00:18,450
Now Spike at first seems similar to the get 33 except in this case we want to write a function that

5
00:00:18,450 --> 00:00:24,380
takes on a list of integers and returns true if it contains Double-O 7 in order and actually mean in

6
00:00:24,430 --> 00:00:27,520
consecutive order and we show that by the second example here.

7
00:00:27,750 --> 00:00:32,070
So this first example you can see here has double 0 7 or 0 0 7 in order.

8
00:00:32,070 --> 00:00:33,580
So it returns true.

9
00:00:33,600 --> 00:00:39,660
Now you'll also notice on the second one has two zeros showing up before 7 which also means true.

10
00:00:39,900 --> 00:00:42,120
So this double A7 doesn't have to be consecutive.

11
00:00:42,120 --> 00:00:44,870
It can also work like this 0 0 7.

12
00:00:44,940 --> 00:00:48,350
And here you can see we have 7 0 0 so that returns false.

13
00:00:48,780 --> 00:00:55,500
So let's see how we can actually approach this problem we'll say spy game function and it takes in some

14
00:00:55,500 --> 00:00:57,310
lesson numbers in them.

15
00:00:57,330 --> 00:01:01,470
So right off the bat we know we're going to have to go through each of these numbers and do some sort

16
00:01:01,470 --> 00:01:02,710
of check.

17
00:01:02,730 --> 00:01:07,920
So something that might be good to say for numb numbs.

18
00:01:08,130 --> 00:01:11,970
And right now we can say pass just a little placeholder when we think about this.

19
00:01:12,060 --> 00:01:16,430
I want to check if two zeros show up before seven.

20
00:01:16,440 --> 00:01:23,950
So there's lots of ways we could do this but one way is to set up a code list in our code list it's

21
00:01:23,950 --> 00:01:30,600
going to be 0 0 7 and then some extra string SOLs have X there.

22
00:01:30,600 --> 00:01:36,630
And what we're going to do is we're going to go through every number in this list and if we hit the

23
00:01:36,630 --> 00:01:40,970
first item in the list we'll go ahead and just pop it off.

24
00:01:41,400 --> 00:01:51,550
So what I mean by this is if I say Fortnum and numbers if my current number is equal to the first item

25
00:01:51,640 --> 00:01:57,920
in code what I will say is equal to the first item in code so code at index 0.

26
00:01:57,940 --> 00:02:04,490
I'm going to take that code list and pop off that first item.

27
00:02:04,510 --> 00:02:06,030
So what does this actually mean.

28
00:02:06,040 --> 00:02:11,140
It means as I'm iterating through this list I'm checking the number and saying hey is this number equal

29
00:02:11,140 --> 00:02:16,300
to the first number in this code list so you can imagine for the first case eventually hit the zero

30
00:02:16,630 --> 00:02:19,180
and I say yes I have a match 0 and 0.

31
00:02:19,180 --> 00:02:23,360
So what I do is I pop off the index at 0.

32
00:02:23,410 --> 00:02:25,040
That means I pop off the 0.

33
00:02:25,210 --> 00:02:27,430
So then the next iteration of code looks like this.

34
00:02:27,430 --> 00:02:35,420
We have 0 7 and then some string X. And then if I get another match then I pop off again.

35
00:02:35,530 --> 00:02:44,860
So I will get seven string x and then if I pop off one more time meaning I matched 007 in consecutive

36
00:02:44,860 --> 00:02:50,150
order then I'll just have a list of length 1.

37
00:02:50,200 --> 00:02:55,750
So notice here we have a length is equal to 1 and you could also just check if length is empty or something

38
00:02:55,750 --> 00:02:57,650
like that but will do length to 1.

39
00:02:57,670 --> 00:03:01,130
Just so it's easier to understand.

40
00:03:01,190 --> 00:03:09,130
So then here I'll just return is the length of my code after running that for a loop equal to 1.

41
00:03:09,140 --> 00:03:13,250
So let's walk through this one more time because the code is a little weird at first but if we break

42
00:03:13,250 --> 00:03:15,110
down the logic it'll make sense.

43
00:03:15,110 --> 00:03:21,770
I have this coded list and it has 0 0 7 and some extra string and I'm going to go through every number

44
00:03:21,770 --> 00:03:22,950
in this list.

45
00:03:22,950 --> 00:03:27,580
Now I'm going to check is that number equal to code index 0.

46
00:03:27,860 --> 00:03:32,230
If that's the case then I'm going to pop off the first item in code.

47
00:03:32,240 --> 00:03:34,010
So it's first now start at zero.

48
00:03:34,010 --> 00:03:36,750
That means no code is equal to 0 7 x.

49
00:03:36,770 --> 00:03:41,410
So I keep doing it for a loop and I check hey you know I have this number called code at zero that's

50
00:03:41,500 --> 00:03:42,300
this zero.

51
00:03:42,300 --> 00:03:44,420
Now if that's true then I pop it off.

52
00:03:44,450 --> 00:03:47,150
And now my coatless looks like this 7 x.

53
00:03:47,150 --> 00:03:52,310
So I keep going around until I see that 7 and I say hey if seven is equal to that first item and I could

54
00:03:52,310 --> 00:03:55,400
list the seven right here and pop off that 7.

55
00:03:55,460 --> 00:04:00,860
And if I'm able to go through this entire four loop and I finally since it's a list of integers I have

56
00:04:00,860 --> 00:04:03,710
a string here and it's going to be a list of length 1.

57
00:04:03,920 --> 00:04:11,270
Then if length the code is equal to one that I know I had 007 or double A7 in order so we can check

58
00:04:11,270 --> 00:04:12,840
this out by running this.

59
00:04:12,980 --> 00:04:14,920
Let's grab some of these examples.

60
00:04:14,990 --> 00:04:17,120
Run them we get back.

61
00:04:17,120 --> 00:04:18,140
True.

62
00:04:18,380 --> 00:04:19,810
Let's run the second one.

63
00:04:19,820 --> 00:04:26,350
We should also get back through and let's run the third one which should bring back false

64
00:04:29,350 --> 00:04:30,180
OK.

65
00:04:30,190 --> 00:04:34,360
So again this was a little more challenging because we had to use an outside data structure and use

66
00:04:34,360 --> 00:04:38,970
logic in order to figure out how best to find those consecutive numbers.

67
00:04:38,980 --> 00:04:41,230
There's tons of different ways you could have approached this one.

68
00:04:41,230 --> 00:04:43,900
Feel free to share other methods in the cuniform.

69
00:04:44,260 --> 00:04:45,520
OK the next question was.

70
00:04:45,520 --> 00:04:50,500
Count primes which was to write a function that returns the number of prime numbers that exists up to

71
00:04:50,560 --> 00:04:55,240
and including a given number and by conventional will treat 0 and 1 as not prime.

72
00:04:55,240 --> 00:04:57,080
So how do we actually approach this.

73
00:04:57,330 --> 00:05:04,820
Well we're going start off by saying the math count primes and we pass in some number.

74
00:05:04,920 --> 00:05:07,420
So that's going to be the integer itself.

75
00:05:07,430 --> 00:05:12,030
Now since we're treating 0 and 1 is not prime we might as we'll have a quick check for them.

76
00:05:12,080 --> 00:05:16,560
So I won't say if that number that the person is put in is less than two.

77
00:05:16,610 --> 00:05:19,520
Meaning it's zero or 1 or it's a negative number.

78
00:05:19,520 --> 00:05:21,190
We'll just return 0.

79
00:05:21,440 --> 00:05:32,860
So this is our check for 0 or 1 and put now if we've not returned yet after this check then we know

80
00:05:32,860 --> 00:05:41,140
we're dealing with a number that's two or greater now to itself is a prime number and I want to make

81
00:05:41,140 --> 00:05:43,360
a list to keep track of prime numbers.

82
00:05:43,390 --> 00:05:44,820
So here's what I'm going to do.

83
00:05:44,860 --> 00:05:48,580
And this is kind of a clever trick which is what makes this challenging.

84
00:05:48,580 --> 00:05:52,430
I will start off my list called primes with the number two in it.

85
00:05:53,460 --> 00:05:58,890
And then I'm going to set a variable x equal to 3 and when I'm going to do.

86
00:05:59,100 --> 00:06:07,590
Isn't going to keep adding on to X until I hit the input number and we'll check if X ends up being prime

87
00:06:07,590 --> 00:06:08,060
or not.

88
00:06:08,070 --> 00:06:09,970
So that showed I mean by that.

89
00:06:10,050 --> 00:06:15,510
So we already know that if we've gone past that if statement failing then or dealing with two or greater

90
00:06:16,530 --> 00:06:17,730
and we have here a list.

91
00:06:17,730 --> 00:06:19,490
Let me zoom in just a little bit here.

92
00:06:19,560 --> 00:06:22,370
We have an A-list to store our prime numbers.

93
00:06:25,360 --> 00:06:33,490
And then we have some sort of will say exit some sort of counter counter that's going up to the input

94
00:06:36,440 --> 00:06:50,180
and we'll say while X is less or equal to the input number I'll say for y in range from 3 all the way

95
00:06:50,180 --> 00:06:54,890
up to that current number x and then we'll take step size of two.

96
00:06:54,890 --> 00:07:00,350
The reason we're taking step sizes of two here is because even numbers beyond two we already know those

97
00:07:00,350 --> 00:07:04,610
will not have a chance of being prime because they're even numbers are all going to be evenly divisible

98
00:07:04,610 --> 00:07:05,390
by two.

99
00:07:05,690 --> 00:07:09,050
So we're doing here is we have X which is this counter.

100
00:07:09,350 --> 00:07:12,220
And then we're saying OK kind of a subproblem here.

101
00:07:12,230 --> 00:07:25,280
We need to check if X itself is prime So X is going through every number up to the input number.

102
00:07:25,670 --> 00:07:30,370
And then here we need to check if X is prime.

103
00:07:30,440 --> 00:07:39,670
So save for y in range from 3 to X and even steps because we only want to check on them or is there

104
00:07:41,200 --> 00:07:46,950
if x mod Y is equal to zero.

105
00:07:48,710 --> 00:07:53,360
Then we know we don't have an even number there or excuse me we know we don't have a prime number there

106
00:07:53,870 --> 00:08:02,230
so we're going to do is add to X kind of add to our account and we'll break out of this for loop.

107
00:08:02,240 --> 00:08:03,100
So this for loop.

108
00:08:03,110 --> 00:08:10,370
All it does is this checks if X is prime because it's going through every number in a range 3 up to

109
00:08:10,370 --> 00:08:13,920
x or x is going then through every number up to the input number.

110
00:08:13,970 --> 00:08:15,740
So this is checking if X is prime.

111
00:08:15,740 --> 00:08:22,550
We say if that x number is ever evenly divisible by one of these numbers and its range then we know

112
00:08:22,550 --> 00:08:28,400
it's not prime and we're going to add a count to kind of hop ahead past that even number and then we'll

113
00:08:28,400 --> 00:08:31,690
break out of this for loop because we're done checking if X is prime.

114
00:08:31,910 --> 00:08:38,180
However if we're able to successfully go through that entire four loop and we don't actually end up

115
00:08:38,720 --> 00:08:42,480
breaking out of it then we have a prime number somewhere.

116
00:08:42,590 --> 00:08:47,310
So we want to do is take that prime number and append that to our primes list.

117
00:08:47,510 --> 00:08:53,540
And this is what makes the problem challenging is that you can use a for Else statement and this is

118
00:08:53,540 --> 00:08:55,240
something that's unique to Python.

119
00:08:55,250 --> 00:08:59,690
Now my ELSE is actually line up of my four instead of with my IF here.

120
00:08:59,690 --> 00:09:02,550
That's because I have a break inside of my 4.

121
00:09:02,570 --> 00:09:10,130
So what else is saying here is if I went through this entire four loop and I never broke then this statement

122
00:09:10,220 --> 00:09:11,290
is going to execute.

123
00:09:11,420 --> 00:09:16,040
And again something unique to Python before else we haven't talked about it before which is what kind

124
00:09:16,040 --> 00:09:19,260
of makes this a challenge bonus problem.

125
00:09:19,340 --> 00:09:20,940
So that happens to be the case.

126
00:09:21,050 --> 00:09:23,770
Meaning we checked and we never broke out of this.

127
00:09:23,780 --> 00:09:25,540
So X actually is prime.

128
00:09:25,670 --> 00:09:32,150
We're going to take our primes list and we'll append the x number to it and then we jump ahead.

129
00:09:33,170 --> 00:09:39,260
Pass that even numbers will say X is not equal to x plus equal to 2.

130
00:09:39,350 --> 00:09:45,260
And after this whole thing is done we're going to print that list of primes and then we'll return back

131
00:09:45,260 --> 00:09:50,660
what the questions actually asking which is the length of how many prime numbers there are.

132
00:09:50,690 --> 00:09:52,270
So they quickly go through the logic here.

133
00:09:52,280 --> 00:09:58,390
One more time we first check for zero on one input by saying if number is less than 2 returns 0 then

134
00:09:58,450 --> 00:10:03,670
say No I'm dealing for number of two or greater I'm going start off my count of my prime numbers with

135
00:10:03,680 --> 00:10:06,420
the number two in there so I can start off with three.

136
00:10:06,500 --> 00:10:12,830
And the bonus of starting off with three means I can use a step size of two and that's indicated here

137
00:10:12,830 --> 00:10:17,440
from range 3 x 2 as well as when I'm adding two to my count of X..

138
00:10:17,630 --> 00:10:23,390
If I end up breaking out of one of these four loops or the trick or the else statement because then

139
00:10:23,390 --> 00:10:28,910
I'm skipping along all those even numbers which is why we have plus equals to here and where we're taking

140
00:10:28,910 --> 00:10:35,220
a step of range to their And then all we're doing is we're going through X all the way up to that number

141
00:10:35,470 --> 00:10:37,110
going to check of the X's prime.

142
00:10:37,260 --> 00:10:40,620
And if it's not if it is prime meaning we never broke out of this.

143
00:10:40,650 --> 00:10:45,210
We're going to append that to the prime list and that add two to our count and keep going through this

144
00:10:45,210 --> 00:10:46,590
while loop.

145
00:10:46,590 --> 00:10:52,220
Now we're going to put all our primes and return the length of primes so we run this and we say count

146
00:10:53,350 --> 00:10:54,990
is call count prime.

147
00:10:54,990 --> 00:10:56,500
So you're on 100.

148
00:10:56,580 --> 00:11:00,910
Get back that list of primes and we get back the number is 25.

149
00:11:00,920 --> 00:11:05,780
Now there's a clever trick you can add to this and the fact is that you actually don't need to check

150
00:11:05,930 --> 00:11:10,240
every single number up to x year instead.

151
00:11:10,340 --> 00:11:14,330
Recall that you're actually already storing a list of primes here.

152
00:11:14,630 --> 00:11:22,490
So you could say is for y in and instead of saying range 3 x 2 you could say for y in primes.

153
00:11:22,640 --> 00:11:27,010
So that makes use of those prime numbers you're collecting as you go along.

154
00:11:27,020 --> 00:11:33,110
So not saying for y and prime numbers check with X maade one of these prime numbers is equal to zero.

155
00:11:33,620 --> 00:11:35,900
And that's how we can do X is prime.

156
00:11:35,900 --> 00:11:37,240
Check there.

157
00:11:37,280 --> 00:11:40,390
Keep in mind this has to do more math I believe than coding.

158
00:11:40,400 --> 00:11:43,460
So if this is a struggle for you don't worry about it too much.

159
00:11:43,460 --> 00:11:49,910
This is more of a challenge math problem than a challenge program problem I would say especially since

160
00:11:49,910 --> 00:11:50,980
this is a challenge problem.

161
00:11:50,980 --> 00:11:51,950
It's a bonus.

162
00:11:51,950 --> 00:11:56,150
And you're still not quite at the skill level where you can tackle two problems at the same time.

163
00:11:56,150 --> 00:12:00,110
One is a really hard programming problem and the other one is a really hard math problem.

164
00:12:00,110 --> 00:12:03,870
So this is combining both of those which is what makes it really challenging.

165
00:12:03,890 --> 00:12:10,330
If however you really like these sort of problems I would check out is the oilor project that's E U

166
00:12:10,580 --> 00:12:16,310
L E R project that it has a ton of these types of math problems that can in general only be solved with

167
00:12:16,310 --> 00:12:16,820
coding.

168
00:12:16,830 --> 00:12:20,420
So we check out Web site if you happen to really enjoy this one.

169
00:12:20,420 --> 00:12:24,460
If you are totally lost in this one because of this heavy math don't worry about it.

170
00:12:24,470 --> 00:12:26,590
That's why it's a challenge problem.

171
00:12:26,600 --> 00:12:27,160
OK.

172
00:12:27,410 --> 00:12:30,300
So that's it for these two challenge problems.

173
00:12:30,320 --> 00:12:32,370
We also have a just for fun problem.

174
00:12:32,390 --> 00:12:36,590
It wasn't really a real it was just printing out functions to takes in a single letter.

175
00:12:36,590 --> 00:12:40,190
So I'll bring that over from the notebook itself.

176
00:12:40,190 --> 00:12:44,960
So this is just for fun and it's basically where you had to kind of invent a dictionary that would print

177
00:12:44,960 --> 00:12:46,130
out these letters.

178
00:12:46,130 --> 00:12:50,720
And because of that we were only able to say ABC the e here as examples.

179
00:12:50,720 --> 00:12:56,810
Everything else you'd have to kind of manually print out but it's just an idea of how you could print

180
00:12:56,870 --> 00:13:00,500
out actual graphics using Python code.

181
00:13:00,710 --> 00:13:04,290
And we'll get an idea later on using your first mouse project.

182
00:13:04,430 --> 00:13:07,620
So you can have graphics that you can interact with a user.

183
00:13:07,670 --> 00:13:09,440
They'll be for your tic tac toe board.

184
00:13:09,440 --> 00:13:11,430
They'll be printing later on.

185
00:13:11,430 --> 00:13:11,780
OK.

186
00:13:11,810 --> 00:13:15,440
If you have any questions feel free to post that Q&amp;A forums but we'll see at the next lecture.
