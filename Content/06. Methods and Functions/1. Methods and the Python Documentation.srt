1
00:00:05,380 --> 00:00:07,780
Welcome back everyone in this section of the course.

2
00:00:07,780 --> 00:00:11,630
We're going to be discussing methods and in particular functions.

3
00:00:11,650 --> 00:00:17,690
Let's start by briefly going into the details of methods in this lecture so we've actually already seen

4
00:00:17,690 --> 00:00:19,170
a few examples of methods.

5
00:00:19,280 --> 00:00:23,630
We're learning about object and data structure types in Python and methods are essentially functions

6
00:00:23,630 --> 00:00:28,100
that are built into objects and later on in the course we're going to learn how to create our own objects

7
00:00:28,130 --> 00:00:31,630
and methods using object oriented programming in classes.

8
00:00:31,670 --> 00:00:36,290
Right now I want to explore it in a bit more detail how to find methods and how to get information about

9
00:00:36,290 --> 00:00:36,710
them.

10
00:00:36,770 --> 00:00:41,350
For built in objects in python in the future we'll explore creating methods.

11
00:00:41,390 --> 00:00:46,070
So let's open up a Jupiter notebook and quickly discuss methods and built in objects.

12
00:00:46,070 --> 00:00:51,860
All right so the first thing I'm going to do is create a simple list and say my list is equal to 1 2

13
00:00:51,860 --> 00:00:53,030
3.

14
00:00:53,030 --> 00:00:56,350
Now we've already seen before a couple of methods are useful for atlast.

15
00:00:56,450 --> 00:01:02,280
So you've seen the append method where we say Dot a pen and we can append a new item to a list.

16
00:01:02,390 --> 00:01:05,450
So then when I see my list here I get back one two three four.

17
00:01:05,450 --> 00:01:08,830
We've also explored the pop method awfullest pop.

18
00:01:09,140 --> 00:01:13,080
And if I just hit empty there it pops off the last item in the list.

19
00:01:13,130 --> 00:01:19,190
Now if it check up my list it no longer has that last item number for what I want to show you now is

20
00:01:19,250 --> 00:01:24,020
a couple of quick ways to get the list of methods in Python for various builtin objects.

21
00:01:24,050 --> 00:01:29,090
There's too many methods really to go through them all of them in this course otherwise we have hours

22
00:01:29,090 --> 00:01:33,560
and hours of video of things that you could just explore yourself and all these methods you don't use

23
00:01:33,560 --> 00:01:34,380
that often.

24
00:01:34,460 --> 00:01:38,090
So instead we're going to show you how you can discover methods on your own.

25
00:01:38,090 --> 00:01:44,270
The first thing you can do is enjoy a notebook once you find the object dot and then hit tab to see

26
00:01:44,270 --> 00:01:49,640
a list of all the attributes and methods available for you on that object.

27
00:01:49,640 --> 00:01:53,840
Now the other thing you can do as well is actually called the help function.

28
00:01:53,840 --> 00:01:59,120
For example I'm going to hit tab here and let's explore a function or a method excuse me that we haven't

29
00:01:59,120 --> 00:02:00,610
actually explored before.

30
00:02:00,620 --> 00:02:05,870
For instance we haven't actually explored this insert method right here so I'm going to call inserts

31
00:02:07,340 --> 00:02:09,360
and there's two different ways to get help on this.

32
00:02:09,380 --> 00:02:14,000
I can do shift tab in the Jupiter note book right after this method call and it will tell me what it

33
00:02:14,000 --> 00:02:14,680
is.

34
00:02:14,690 --> 00:02:18,290
So it's going to say Insert Object before index.

35
00:02:18,290 --> 00:02:22,670
If you're not using the Jupiter notebook and you can't do that shift plus tab call where you could do

36
00:02:22,730 --> 00:02:29,550
is call the built in help function so if I run the built in help function and pass this method returns

37
00:02:29,560 --> 00:02:31,290
back that same docstring.

38
00:02:31,350 --> 00:02:37,390
So if you're using pi scripts in PI charm or in the text editor you can use this help function and run

39
00:02:37,390 --> 00:02:40,930
this in order to get back the actual documentation on the object.

40
00:02:40,930 --> 00:02:44,820
Your third option is to actually view the Python documentation.

41
00:02:45,010 --> 00:02:46,230
So let's quickly jump to that.

42
00:02:46,480 --> 00:02:52,670
OK so here at the Python documentation it says dox that Python the orc's a d o c estopped Python dot

43
00:02:52,720 --> 00:02:53,380
org.

44
00:02:53,470 --> 00:02:57,210
And in this case we're looking at the documentation for 3.6 in the future.

45
00:02:57,220 --> 00:03:03,340
You may be looking at the documentation for 3.7 or 3.8 But you'll notice here that the first thing we

46
00:03:03,340 --> 00:03:06,660
see is a what's new in Python 3 point X..

47
00:03:06,790 --> 00:03:10,980
So you can click on this and it will take you to this link where it just is almost like a little blog

48
00:03:10,990 --> 00:03:16,180
post of the different improvements and things have changed in the newest version of Python.

49
00:03:16,240 --> 00:03:20,630
And as a beginner in Python there's going to be a lot here that you don't really understand yet.

50
00:03:20,880 --> 00:03:24,940
But as you get more and more comfortable Python you'll notice that there's a lot of cool things as you

51
00:03:24,940 --> 00:03:27,810
begin to kind of update to the latest versions of Python.

52
00:03:27,820 --> 00:03:33,130
So here we can see that three point six there's this really cool secrets module that allows you to generate

53
00:03:33,130 --> 00:03:35,740
secure random numbers for managing secrets.

54
00:03:35,740 --> 00:03:40,870
Previously we saw how there's random number generation but the secrets module is actually cryptographically

55
00:03:40,870 --> 00:03:44,020
secure a generation of random numbers.

56
00:03:44,020 --> 00:03:46,110
So that's an extra thing they've added 3.6.

57
00:03:46,180 --> 00:03:49,940
And you can explore everything they've added on that link on the main documentation page.

58
00:03:49,960 --> 00:03:51,370
What's new in Python.

59
00:03:52,080 --> 00:03:56,310
Then there's a tutorial link which kind of shows you the very basics of Python that may be helpful to

60
00:03:56,310 --> 00:03:58,640
you if you want another reference for that.

61
00:03:58,650 --> 00:04:03,600
Then there's the library reference and a couple of other links as well as ethnic cues.

62
00:04:03,600 --> 00:04:06,830
So there's F-keys frequently asked questions and you can check them out here.

63
00:04:06,930 --> 00:04:11,550
General Python a fake news program The fake news design and history and then a couple of more interesting

64
00:04:11,550 --> 00:04:14,240
things like using graphical interfaces a fake news.

65
00:04:14,280 --> 00:04:16,050
So I would explore those.

66
00:04:16,050 --> 00:04:20,280
If you're more interested in that but the main thing we're here to talk about is this library reference

67
00:04:20,520 --> 00:04:22,650
and says keep this under your pillow and for good reason.

68
00:04:22,830 --> 00:04:24,720
So we click on this library reference.

69
00:04:24,720 --> 00:04:29,650
This is basically all the information about all the classes objects and modules in Python.

70
00:04:29,820 --> 00:04:33,530
And a lot of times when you do a google search maybe you find the stack overflow link.

71
00:04:33,570 --> 00:04:38,180
A lot of times in those links people link back to the standard documentation here.

72
00:04:38,310 --> 00:04:42,930
And as you scroll down you'll notice it basically discusses the official documentation of everything

73
00:04:42,930 --> 00:04:47,370
we cover in this course so we can see here it starts off with the built in types that we discussed earlier

74
00:04:47,640 --> 00:04:53,550
such as boolean types numeric types sequence types like list tuple range and then it continues on to

75
00:04:53,550 --> 00:04:58,320
talk about exceptions text processing services and later on to talk some more advanced things that we're

76
00:04:58,320 --> 00:05:03,600
going to cover later on like collections that talks about the different modules such as the math module

77
00:05:03,780 --> 00:05:08,550
functional programming module file and directory access and you can scroll down you can see all this

78
00:05:08,550 --> 00:05:12,760
useful information about all these builtin modules so there's a ton of stuff here.

79
00:05:12,900 --> 00:05:18,690
And this is kind of the official documentation for a lot of these functions and modules.

80
00:05:18,690 --> 00:05:24,480
One thing to keep in mind is that this is not the most readable place to come for answers.

81
00:05:24,510 --> 00:05:29,400
Usually I want to get a stack overflow where someone has at a kind of a nice explanation but this is

82
00:05:29,400 --> 00:05:34,230
the official documentation and it is pretty good as far as documentation goes for official programming

83
00:05:34,230 --> 00:05:35,090
languages.

84
00:05:35,100 --> 00:05:39,320
So let's imagine we wanted to go back to that example of using a list object where we could search for

85
00:05:39,340 --> 00:05:42,160
here and we'll find that here sequence types list.

86
00:05:42,210 --> 00:05:47,100
So there's various sequent types you click here for list and then it says the common sequence operations

87
00:05:47,320 --> 00:05:51,750
and you notice that it actually talks about a lot of the operations we discussed such as X and and it

88
00:05:51,750 --> 00:05:54,630
gives you a result so kind of explaining what's happening here.

89
00:05:54,750 --> 00:06:00,980
The min max length indexing and slicing it's also all explained here kind of in a more official way.

90
00:06:01,120 --> 00:06:06,450
And if you scroll back up we can click here a list to go to official list class and it will take us

91
00:06:06,540 --> 00:06:12,330
and explain the various methods so what sort does when you call it on a list and it does that for the

92
00:06:12,330 --> 00:06:13,420
rest of the object types.

93
00:06:13,440 --> 00:06:17,660
And you can all see lists implements all of the common and mutable sequence operations.

94
00:06:17,710 --> 00:06:20,420
If you want to know more about those operations click here in common.

95
00:06:20,430 --> 00:06:27,120
Ill take you back to discuss those operations such as index and count so this or you can find a lot

96
00:06:27,120 --> 00:06:32,400
more detailed information about the various methods available on objects.

97
00:06:32,400 --> 00:06:36,960
As you're a beginner in Python you won't be visiting this page that often but as you get more and more

98
00:06:36,960 --> 00:06:42,740
advance you'll find that a lot of the answers are searching for or really only found in the documentation.

99
00:06:43,110 --> 00:06:47,910
But as you're beginning I would recommend that you use this help function to quickly find doc strings

100
00:06:48,240 --> 00:06:54,460
and as a reminder you can shift tab to quickly get this documentation string on methods and functions.

101
00:06:54,480 --> 00:06:56,420
OK that's the very basics of methods.

102
00:06:56,550 --> 00:06:59,930
Hopefully it was all pretty familiar since we've already discussed methods in the past.

103
00:06:59,940 --> 00:07:03,290
Coming up next we're going to talk about his functions we'll see there.
