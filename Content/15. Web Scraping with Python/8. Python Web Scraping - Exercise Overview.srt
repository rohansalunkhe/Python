1
00:00:05,250 --> 00:00:06,240
Welcome back, everyone.

2
00:00:06,360 --> 00:00:12,510
It's now time to test your new skill set with Web scraping, with some exercises in this lecture, we're

3
00:00:12,510 --> 00:00:16,350
just going to give a brief overview of the exercise questions and what they're asking for.

4
00:00:16,740 --> 00:00:20,550
And then in the next series of lectures, we'll go ahead and work through some examples, solutions.

5
00:00:21,150 --> 00:00:21,750
Let's get started.

6
00:00:22,020 --> 00:00:22,190
OK.

7
00:00:22,260 --> 00:00:25,470
So here I am on there, the web scraping exercise notebook.

8
00:00:25,500 --> 00:00:27,030
It's called the Web scraping exercise.

9
00:00:27,330 --> 00:00:29,910
And essentially, we just have a list of tasks for you to complete.

10
00:00:30,360 --> 00:00:32,850
So first thing to do is import any libraries you think you'll need.

11
00:00:33,330 --> 00:00:37,980
And then the second thing is to use the request library and beautiful soup to connect to, quote, start

12
00:00:37,980 --> 00:00:41,190
to scrape dot com ticket, the HST e-mail text from the home page.

13
00:00:41,550 --> 00:00:44,520
So if you click on that link, it'll take you this quotes to scrape.

14
00:00:44,880 --> 00:00:47,220
It was essentially the other to scrape site.

15
00:00:47,280 --> 00:00:54,210
So we looked at books scraping that site and the previous lectures and now we can see that we have some

16
00:00:54,210 --> 00:00:54,900
quotes this great.

17
00:00:54,990 --> 00:00:57,330
So essentially the site a couple of things to know here.

18
00:00:57,330 --> 00:00:58,410
There's quotes, the scrape.

19
00:00:58,770 --> 00:01:04,470
There's a quote the person who said the quote and then tags for each quote, such as Change, Deep Thoughts,

20
00:01:04,510 --> 00:01:04,970
etc..

21
00:01:05,400 --> 00:01:09,610
And then there's the top 10 tags over here on the right hand side across the entire site.

22
00:01:10,290 --> 00:01:13,050
So you can scroll down here and see that you can click next.

23
00:01:13,800 --> 00:01:17,010
And that brings you to the next list of quotes by some authors.

24
00:01:17,490 --> 00:01:21,360
And you can also click on about and that takes you to an about description.

25
00:01:21,390 --> 00:01:25,140
For those authors who click quotes, that's great, but takes you back to the home page.

26
00:01:25,500 --> 00:01:29,710
And if you click on one of these tags, which is inspirational, it shows you and it quote with the

27
00:01:29,710 --> 00:01:30,850
text inspirational.

28
00:01:31,110 --> 00:01:36,840
And then you can always sit next to keep going for whatever pages of those tags for inspirational are.

29
00:01:37,980 --> 00:01:40,890
Coming back to the exercise, this is what it should look like.

30
00:01:40,920 --> 00:01:42,630
Essentially, we just want you to grab that text.

31
00:01:42,900 --> 00:01:43,920
This is just from the home page.

32
00:01:43,940 --> 00:01:44,670
That first page.

33
00:01:45,150 --> 00:01:48,060
Then we want you to get the names of all the authors on the first page.

34
00:01:48,510 --> 00:01:49,590
So a quick little hands here.

35
00:01:49,620 --> 00:01:51,960
Notice that I'm using a set, not a list.

36
00:01:52,740 --> 00:01:54,420
Next is to create a list.

37
00:01:54,510 --> 00:01:57,690
This is specifically a python list of all the quotes on the first page.

38
00:01:58,050 --> 00:02:00,090
So I want you to create a list that looks something like this.

39
00:02:00,930 --> 00:02:06,030
Next, we want you to inspect the site and then use beautiful soup to extract the top 10 tags from the

40
00:02:06,030 --> 00:02:06,870
requests text.

41
00:02:06,900 --> 00:02:11,670
So essentially, what we want you to do is if you notice on the home page, we have these top 10 tags.

42
00:02:12,000 --> 00:02:15,660
We want to use Python to figure out how to grab what these 10 words are.

43
00:02:16,140 --> 00:02:20,610
So eventually you should get something that it can either be a list or a string or just print it out.

44
00:02:20,670 --> 00:02:22,530
But we want those actual tags.

45
00:02:23,710 --> 00:02:27,550
After that, we have the final task and this final task is kind of a larger one.

46
00:02:27,670 --> 00:02:33,130
But basically you'll notice that there's more than one page and the subsequent pages look something

47
00:02:33,130 --> 00:02:33,550
like this.

48
00:02:33,580 --> 00:02:35,320
Let me zoom in so you can read this as well.

49
00:02:36,010 --> 00:02:40,240
They have quotes to scrape, dot com slash and then page slash the page number.

50
00:02:40,870 --> 00:02:45,280
So we want you to use what you already know about for loops, string activation and everything you've

51
00:02:45,280 --> 00:02:48,790
learned through the course to go ahead and loop through all the pages.

52
00:02:48,970 --> 00:02:50,680
And we want you to get the unique authors.

53
00:02:50,950 --> 00:02:55,180
But now on the entire Web site, the first question was to get the unique authors on the home page.

54
00:02:55,510 --> 00:02:58,720
But now create a set of the unique authors on the entire Web site.

55
00:02:59,380 --> 00:03:01,960
Now, keep in mind, there's lots of ways to achieve this.

56
00:03:02,260 --> 00:03:08,740
But we want you to do is trying to make your code robust enough that it would work, regardless of knowing

57
00:03:08,770 --> 00:03:10,540
how many pages there were beforehand.

58
00:03:11,050 --> 00:03:15,430
Now, as a hint, we want you to know that there's actually only 10 pages on this Web site.

59
00:03:15,730 --> 00:03:21,040
So if you come to quote's to scrape forward, slash 10 and then try to hit next.

60
00:03:21,060 --> 00:03:22,180
There's no next button there.

61
00:03:22,750 --> 00:03:28,180
And then if you try to go to a page that's larger than that, such as slash one hundred, you'll notice

62
00:03:28,180 --> 00:03:29,800
they'll say no quotes found.

63
00:03:29,950 --> 00:03:32,980
So that's kind of a big hint on how you can try to solve that problem.

64
00:03:33,460 --> 00:03:38,320
So if you want, you can just use a for loop that goes from page one to ten, but really try to challenge

65
00:03:38,320 --> 00:03:43,750
yourself because in a typical situation, you won't know how many pages of quotes there are.

66
00:03:43,870 --> 00:03:47,320
So see if you can figure out how to make your code robust enough in order.

67
00:03:47,320 --> 00:03:51,610
It's for it to work, regardless if you knew how many pages there were.

68
00:03:52,150 --> 00:03:56,590
Last thing I keep mine is there's many, many different types of solutions for a lot of these things.

69
00:03:56,920 --> 00:04:02,650
So don't feel that you need to check your code is viable if you figure it out differently than the solutions

70
00:04:02,650 --> 00:04:03,070
notebook.

71
00:04:03,190 --> 00:04:05,980
As long as you get the same results, then you should be good to go.

72
00:04:06,070 --> 00:04:08,320
So lots of different ways to solve all these problems.

73
00:04:08,890 --> 00:04:09,100
All right.

74
00:04:09,160 --> 00:04:09,760
Best of luck.

75
00:04:09,820 --> 00:04:11,110
We'll see if the solutions lecture.
