1
00:00:05,620 --> 00:00:06,610
Welcome back, everyone.

2
00:00:07,000 --> 00:00:11,800
As you recall, previously, we figured out that we were most interested in selecting this class called

3
00:00:11,800 --> 00:00:12,910
product pot.

4
00:00:13,420 --> 00:00:19,570
So really what I need to do is figure out what python code is going to be necessary to grab a title

5
00:00:20,110 --> 00:00:24,640
if the actual class is associated with a two star rating.

6
00:00:25,000 --> 00:00:28,360
Because recall, our goal is get the title of every book with a two star rating.

7
00:00:28,840 --> 00:00:32,030
And now I essentially have a list of this product pod.

8
00:00:32,500 --> 00:00:35,290
So let's go ahead and set this up as a list.

9
00:00:35,710 --> 00:00:40,210
So we'll say products is equal to suit that select product pod.

10
00:00:40,840 --> 00:00:44,230
And from this point on, there's kind of a million different ways you could do this.

11
00:00:44,260 --> 00:00:47,290
So this is just one example of how you could do this.

12
00:00:47,860 --> 00:00:52,510
What I'm going to do next is say products zero.

13
00:00:52,570 --> 00:00:57,280
And let's just set this up as an example, because if we can get this working, for one example, then

14
00:00:57,280 --> 00:00:59,350
we should be able to continue exploring this.

15
00:01:00,580 --> 00:01:02,680
So here we have this example object.

16
00:01:03,100 --> 00:01:06,490
So I take a look at this example, has an article class.

17
00:01:06,790 --> 00:01:09,190
Looks like it has this class star rating three.

18
00:01:09,490 --> 00:01:11,260
And recall, the one we're going to be looking for.

19
00:01:11,290 --> 00:01:12,400
Is star rating, too.

20
00:01:12,880 --> 00:01:17,980
So the two things I need to know is this particular class call, whether or not it's star rating two

21
00:01:18,040 --> 00:01:18,850
or something else.

22
00:01:19,210 --> 00:01:24,700
And the second thing I need to know is this title, which the title is going to be here, A Light in

23
00:01:24,700 --> 00:01:26,080
the Attic for that first book.

24
00:01:26,590 --> 00:01:28,570
Let's begin by trying to answer the question.

25
00:01:28,660 --> 00:01:30,550
Is this rated two stars or not?

26
00:01:31,240 --> 00:01:32,740
There's two different ways you can do this.

27
00:01:32,830 --> 00:01:34,150
There's actually probably more than two ways.

28
00:01:34,180 --> 00:01:35,830
But I'll show you two in this lecture.

29
00:01:36,190 --> 00:01:39,790
One is kind of a quick and dirty way, which may not always work.

30
00:01:39,820 --> 00:01:40,930
Depending on the situation.

31
00:01:41,290 --> 00:01:46,300
And the second one is using beautiful soups, select call to actually find this particular class call.

32
00:01:46,720 --> 00:01:48,640
So let me show you the quick and dirty way first.

33
00:01:48,970 --> 00:01:51,550
So we have this example has all the information here.

34
00:01:51,850 --> 00:01:55,720
What it could do is convert this to a string.

35
00:01:56,990 --> 00:02:01,370
Which then makes this into kind of squashed down string with Python.

36
00:02:01,850 --> 00:02:05,930
And you'll notice there's this class called classes equal to star rating three.

37
00:02:06,590 --> 00:02:09,950
So something I could do is because this is a string, I can simply just check.

38
00:02:11,130 --> 00:02:13,020
Is star rating.

39
00:02:14,350 --> 00:02:19,180
Two in string example I run, that happens to be false.

40
00:02:19,480 --> 00:02:21,640
Now, recall, this first book was rated three stars.

41
00:02:21,670 --> 00:02:25,240
So let's confirm that a star rating, three in string example.

42
00:02:25,600 --> 00:02:26,590
And that happens to be true.

43
00:02:27,130 --> 00:02:32,320
So this kind of really quick and dirty way to check if something is present in that particular example.

44
00:02:32,860 --> 00:02:38,290
So this is one way of figuring out whether or not something is rated to stars or something else because

45
00:02:38,290 --> 00:02:39,940
it will just simply return a boolean.

46
00:02:40,420 --> 00:02:43,810
So that's kind of the quick and dirty way, depending on your Web site and your application.

47
00:02:44,080 --> 00:02:48,580
And what question you're actually trying to answer, this sort of method may not be available to you.

48
00:02:48,910 --> 00:02:55,210
So now it's kind of go through a more official tactic, which is specifically trying to find what this

49
00:02:55,210 --> 00:02:57,070
particular classes is checking.

50
00:02:57,160 --> 00:02:58,350
Is this class present?

51
00:02:58,780 --> 00:03:02,730
So I know that the class I'm looking for is called star rating, too.

52
00:03:03,430 --> 00:03:10,480
So what I can do since this example is that specialized, beautiful, super tag object, I can say example

53
00:03:11,290 --> 00:03:14,890
select and now I can select classes from this.

54
00:03:15,310 --> 00:03:17,350
So I could select that star rating class.

55
00:03:18,040 --> 00:03:19,210
It's also a star rating.

56
00:03:21,100 --> 00:03:21,580
Three.

57
00:03:22,480 --> 00:03:23,080
Run this.

58
00:03:23,320 --> 00:03:26,710
And notice here that there's a space because of that.

59
00:03:26,830 --> 00:03:28,720
I need to actually fill that in with a dot.

60
00:03:30,040 --> 00:03:31,930
And now I should see the correct result.

61
00:03:32,170 --> 00:03:36,730
So keep in mind, if you're selecting a class call, there happens to be a space in it.

62
00:03:37,180 --> 00:03:40,960
You should be using this dot in order to fill out that space.

63
00:03:41,830 --> 00:03:46,590
So now here I can see that there is something return for that particular class element.

64
00:03:47,080 --> 00:03:55,240
If I were to try to search for something that was incorrect, such as star rating to run that, then

65
00:03:55,240 --> 00:03:56,800
I get back an empty list.

66
00:03:57,340 --> 00:04:04,930
So something I could do is just check whether or not I get an empty list when I ask to select star rating

67
00:04:05,140 --> 00:04:09,010
to from that particular item or product.

68
00:04:09,550 --> 00:04:11,320
So again, two different ways I could do this.

69
00:04:11,410 --> 00:04:12,920
One is kind of the quick and dirty string.

70
00:04:12,940 --> 00:04:13,900
Check, check.

71
00:04:13,930 --> 00:04:18,400
If the star that's rating to string is within the string version of example.

72
00:04:18,880 --> 00:04:24,280
The second version is using beautiful soup where I can actually check, pay, select whatever particular

73
00:04:24,280 --> 00:04:26,980
class I'm looking for and check to see if it's present.

74
00:04:27,370 --> 00:04:34,510
So then I can just simply do something like check if an empty list is equal to this, because I know

75
00:04:34,510 --> 00:04:37,540
if it's going to be empty, then it's not rated to stars.

76
00:04:38,170 --> 00:04:45,070
Okay, so now that we have those systems in place, I know how to check if this particular product is

77
00:04:45,070 --> 00:04:45,880
rated to stars.

78
00:04:46,750 --> 00:04:50,830
Next, what I need to do is actually figure out how to grab the title of the book.

79
00:04:51,220 --> 00:04:52,810
So let's imagine that this returns.

80
00:04:52,810 --> 00:04:53,140
True.

81
00:04:53,140 --> 00:04:57,580
And I'm ready to actually select for something that's two stars, depending on how I ran this.

82
00:04:57,880 --> 00:04:58,870
I need to grab the title.

83
00:04:59,710 --> 00:05:03,310
So let's run the example again and look for where the actual title is.

84
00:05:03,790 --> 00:05:05,860
The title may be present in multiple parts.

85
00:05:05,980 --> 00:05:10,330
You'll notice here that there's an alternative link inside this image that is just the title of the

86
00:05:10,330 --> 00:05:10,660
book.

87
00:05:11,030 --> 00:05:12,640
But let's look for something more official.

88
00:05:13,060 --> 00:05:18,310
It looks like the official posting of the title is actually here in title is equal to a Light in the

89
00:05:18,310 --> 00:05:18,670
Attic.

90
00:05:19,210 --> 00:05:21,430
And that is held within this tag.

91
00:05:21,490 --> 00:05:28,570
So what I'm looking for here are these open enclosing tag notes to try to find where this actual information

92
00:05:28,570 --> 00:05:28,840
is.

93
00:05:29,290 --> 00:05:35,110
So looks like I have a light in the attic as a title part of this, a element which is essentially a

94
00:05:35,110 --> 00:05:36,100
linking element.

95
00:05:36,820 --> 00:05:38,380
So what I can do is say example.

96
00:05:39,840 --> 00:05:40,500
Select.

97
00:05:41,660 --> 00:05:49,070
Eh, and here I get back these references, and the one I'm looking for is actually the second link

98
00:05:49,070 --> 00:05:49,610
reference.

99
00:05:50,060 --> 00:05:52,070
This first one is the image of the book.

100
00:05:52,520 --> 00:05:54,740
The second one looks like it's the title.

101
00:05:54,830 --> 00:05:56,630
And if we go back to the product page here.

102
00:05:57,020 --> 00:06:04,070
Notice that both this image and there's a link here will actually lead me to the book page.

103
00:06:04,310 --> 00:06:06,020
So that's essentially why there's two links.

104
00:06:06,350 --> 00:06:08,720
One is that image and one is the title of the book.

105
00:06:09,350 --> 00:06:13,310
Notice that the title parameter is the full title of the book.

106
00:06:13,670 --> 00:06:16,280
The text that's actually shown looks to be cut off.

107
00:06:16,370 --> 00:06:18,140
So I don't want to operate on this text.

108
00:06:18,500 --> 00:06:21,050
Instead, I want to operate on this title parameter.

109
00:06:21,740 --> 00:06:29,390
So that means for each product I'm going to grab the second link index is one because at index zero,

110
00:06:29,420 --> 00:06:33,380
that's the actual image index one is going to be the title of the book.

111
00:06:33,980 --> 00:06:36,950
And I'm not looking for the text in between these tags.

112
00:06:37,310 --> 00:06:39,530
Instead, I'm looking for the title.

113
00:06:39,950 --> 00:06:44,180
And as we previously mentioned, I can grab this if we take a look at this type of object.

114
00:06:45,310 --> 00:06:50,950
It's still that specialized B.S. for element tagged, which means I can grab those parameters as if

115
00:06:50,950 --> 00:06:52,030
it was a dictionary call.

116
00:06:53,180 --> 00:06:54,350
So I can ask for a title.

117
00:06:55,490 --> 00:06:57,650
And here I get a light in the attic.

118
00:06:58,370 --> 00:07:01,340
So let's just go ahead and kind of mark this down as our notes.

119
00:07:01,670 --> 00:07:10,160
So we already know that we can check if something is two stars, either through a string, call that

120
00:07:10,220 --> 00:07:15,740
in something or through example, select whatever that reading happens to be.

121
00:07:17,210 --> 00:07:20,240
And I know I can use exampled select.

122
00:07:21,280 --> 00:07:21,640
A.

123
00:07:23,150 --> 00:07:24,470
Grabbed the second item.

124
00:07:25,560 --> 00:07:28,020
And then grab title from that tag.

125
00:07:29,360 --> 00:07:35,730
To grab the book title, so I have the two checks that I need in order to grab a book title if it's

126
00:07:35,730 --> 00:07:36,390
two stars.

127
00:07:37,020 --> 00:07:43,140
Now, all I need to do is give this a shot by combining these ideas that we've talked about with a for

128
00:07:43,140 --> 00:07:49,650
loop to loop through all the pages by inserting this format four pages, one through 50.

129
00:07:49,860 --> 00:07:50,970
So let's set that loop up.

130
00:07:51,240 --> 00:07:52,050
Shouldn't be too hard.

131
00:07:52,740 --> 00:07:56,820
We're gonna do here is, say, my two star titles.

132
00:07:59,710 --> 00:08:01,430
Is equal to an empty list to begin with.

133
00:08:02,640 --> 00:08:10,560
And then for N in range, one up tube and on including 50 one, because I want pages one through 50.

134
00:08:12,730 --> 00:08:16,990
My scrape, Yoro, is going to be equal to that base, you, Earl.

135
00:08:18,450 --> 00:08:21,000
And then I'm going to insert that particular number.

136
00:08:22,340 --> 00:08:24,050
I'm going to make a request.

137
00:08:24,320 --> 00:08:28,280
So we'll say requests that get that scrape you are El.

138
00:08:30,230 --> 00:08:33,180
They will say soup is equal to B.S. for.

139
00:08:35,570 --> 00:08:36,230
Beautiful soup.

140
00:08:36,530 --> 00:08:37,380
Make sure I spell that right.

141
00:08:39,220 --> 00:08:40,300
Passing the text.

142
00:08:43,050 --> 00:08:45,850
And then use Alex Amelle as my engine there.

143
00:08:47,690 --> 00:08:51,690
And then recall, my list of books is going to be off that page.

144
00:08:52,950 --> 00:08:53,700
Select.

145
00:08:55,300 --> 00:08:55,920
Product.

146
00:08:56,060 --> 00:08:57,010
Underscore pod.

147
00:08:57,530 --> 00:09:02,330
So recall this books is the same as that list of products I had here.

148
00:09:02,990 --> 00:09:04,160
So I'm just calling it books.

149
00:09:04,220 --> 00:09:05,010
Inside this loop.

150
00:09:05,480 --> 00:09:07,640
So this books contains all those products.

151
00:09:07,760 --> 00:09:11,150
And now I need to filter out through those books.

152
00:09:11,330 --> 00:09:14,630
So here's a list of books, which is those element tags for all the books.

153
00:09:15,260 --> 00:09:20,510
And I want to say for book in books, I need a check for two things.

154
00:09:20,750 --> 00:09:21,470
I want to check.

155
00:09:23,210 --> 00:09:29,030
If it's two stars, different ways I can do this, but kind of the more official way it would be.

156
00:09:30,070 --> 00:09:33,310
If I grab that book and attempt to select.

157
00:09:34,700 --> 00:09:35,990
The star rating class.

158
00:09:37,480 --> 00:09:39,090
Start reading, too.

159
00:09:40,360 --> 00:09:43,390
And if that returns an empty list, meaning the length is one.

160
00:09:44,490 --> 00:09:46,980
Then or excuse me, the length is zero.

161
00:09:47,010 --> 00:09:49,950
If it's an empty list, then I know star rating two is not there.

162
00:09:52,260 --> 00:09:53,990
So let me explain this one more time here.

163
00:09:54,230 --> 00:10:00,800
What I'm doing is for every book that's returned inside, souped up, select that product pod, I'm

164
00:10:00,800 --> 00:10:05,900
going to attempt to select star rating to as we did kind of up here.

165
00:10:05,980 --> 00:10:07,640
Here we're selecting star rating three.

166
00:10:08,270 --> 00:10:11,900
And then if I were to select star rating two, I get back an empty list.

167
00:10:12,290 --> 00:10:18,320
So if the list is not empty, four star rating two, that means I do have a two star rating book.

168
00:10:18,950 --> 00:10:23,780
So what I'm going to do is check the length of that list and make sure it's not empty.

169
00:10:24,140 --> 00:10:25,640
That's one way I could have done this.

170
00:10:26,180 --> 00:10:28,220
The alternative way was to do that string check.

171
00:10:28,280 --> 00:10:29,900
I could have said if.

172
00:10:31,210 --> 00:10:33,010
Star reading to.

173
00:10:34,360 --> 00:10:34,750
In.

174
00:10:36,340 --> 00:10:38,830
The string version of that particular book.

175
00:10:38,890 --> 00:10:40,570
So that's the same as that string example.

176
00:10:41,050 --> 00:10:42,520
So this is one way to do the check.

177
00:10:42,760 --> 00:10:45,580
This is another way to do the check, depending on your situation.

178
00:10:46,420 --> 00:10:48,490
You may find one better than the other.

179
00:10:49,140 --> 00:10:49,360
OK.

180
00:10:49,510 --> 00:10:55,030
So if the length of that selection is not equal to zero, then boom.

181
00:10:55,090 --> 00:11:00,610
We have a two star rated book, which means the next thing I need to do is actually grab that book title.

182
00:11:01,210 --> 00:11:02,530
And we already know how to do this.

183
00:11:02,590 --> 00:11:04,540
We'll say book title is equal to.

184
00:11:05,110 --> 00:11:08,830
And recall, we did this through this example that select a one title.

185
00:11:09,460 --> 00:11:14,860
The only thing I need to do is when I copy and paste this code is this example is now called book.

186
00:11:15,670 --> 00:11:18,070
So we're gonna replace that with book.

187
00:11:19,090 --> 00:11:22,270
And then our two star titles.

188
00:11:23,620 --> 00:11:25,660
I will append that book title.

189
00:11:26,710 --> 00:11:27,220
There we go.

190
00:11:27,640 --> 00:11:31,230
Let's go ahead and run this and we get some sort of error.

191
00:11:31,300 --> 00:11:35,480
Go ahead and make sure and copy and paste our code or run our unedited notebook first.

192
00:11:35,980 --> 00:11:41,020
If you get some sort of no response error, they should just double check your Internet connection and

193
00:11:41,020 --> 00:11:43,120
make sure you don't have a firewall blocking you.

194
00:11:43,540 --> 00:11:45,460
This site is designed to be scraped.

195
00:11:45,520 --> 00:11:49,510
So it's highly unlikely that the site itself is going to be blocking you.

196
00:11:49,870 --> 00:11:52,660
Other sites, if you try to run this sort of thing, may block you.

197
00:11:52,720 --> 00:11:54,580
If you're making too many repeated requests.

198
00:11:54,580 --> 00:11:55,510
So keep that in mind.

199
00:11:56,320 --> 00:11:56,590
All right.

200
00:11:56,920 --> 00:11:58,210
So that finished running.

201
00:11:58,300 --> 00:12:00,850
Go ahead and just do a very brief overview of everything we did.

202
00:12:01,240 --> 00:12:06,460
We went ahead and said four N in range, one through fifty one, because I know there's 50 pages.

203
00:12:06,880 --> 00:12:09,190
I'm going to scrape that particular page.

204
00:12:09,850 --> 00:12:10,870
Make a request.

205
00:12:11,440 --> 00:12:12,520
Turn it into a soup.

206
00:12:13,060 --> 00:12:17,320
Look for all the product pods or essentially the books in that soup.

207
00:12:17,830 --> 00:12:20,970
And then for each of those books, I'm going to check if it's related to stars.

208
00:12:21,340 --> 00:12:23,080
By checking to see if something's actually return.

209
00:12:23,110 --> 00:12:26,800
When I ask for book select star rating to it happens to be true.

210
00:12:27,100 --> 00:12:29,680
I grabbed that book title and I pendent to my list.

211
00:12:30,520 --> 00:12:36,580
And so we're going to say to start titles, we run this and now we have a list of all the two starter

212
00:12:37,240 --> 00:12:39,520
books across all the pages.

213
00:12:40,180 --> 00:12:40,510
All right.

214
00:12:41,020 --> 00:12:45,850
So you should now have the tools necessary to scrape kind of any major Web site that interests you.

215
00:12:46,300 --> 00:12:50,110
And keep in mind, the more complex the Web site, the harder it will be to scrape.

216
00:12:50,380 --> 00:12:51,850
You should always ask for permission.

217
00:12:52,360 --> 00:12:56,650
You should also keep in mind that not every Web site lends itself this easily to Web scraping.

218
00:12:56,680 --> 00:13:03,340
Lots of sites nowadays proactively attempt to block people scraping, especially if they feel that the

219
00:13:03,340 --> 00:13:06,280
information on the Web site is proprietary in some way.

220
00:13:07,180 --> 00:13:07,420
OK.

221
00:13:07,960 --> 00:13:12,520
So with this new skill set or we're going to do is set you loose on some exercises.

222
00:13:12,880 --> 00:13:15,640
Coming up next, we'll have a brief overview of those exercises.

223
00:13:16,000 --> 00:13:18,610
And after that, we'll cut through some example solutions.

224
00:13:19,080 --> 00:13:19,430
Thanks.

225
00:13:19,540 --> 00:13:20,590
And I'll see at the next lecture.
