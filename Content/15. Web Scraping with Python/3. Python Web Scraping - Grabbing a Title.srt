1
00:00:05,430 --> 00:00:06,350
Welcome back, everyone.

2
00:00:06,690 --> 00:00:09,490
In this lecture, we're finally going start Web scraping off Python.

3
00:00:09,840 --> 00:00:14,310
And we'll begin by showing you how you can grab a page title from any Web site.

4
00:00:14,490 --> 00:00:17,460
So let's go ahead and get started by opening up a Jupiter notebook.

5
00:00:18,170 --> 00:00:20,070
OK, here I am at the Jupiter notebook.

6
00:00:20,310 --> 00:00:25,830
Before we begin, I want to again point out that everything and all the code we show here is inside

7
00:00:25,890 --> 00:00:29,760
the notebook that goes along with this lecture, which called Guide to Web Scraping.

8
00:00:29,850 --> 00:00:34,410
And this notebook is going to be used for most of the lectures throughout this section, of course.

9
00:00:34,770 --> 00:00:39,630
So I want to show you here that there's a lot of information that's accessible to you inside this guide

10
00:00:39,630 --> 00:00:40,860
to Web Scraping Notebook.

11
00:00:41,190 --> 00:00:46,500
We go and break down each time, all documents, a little bit of CSX Web scraping guidelines as well

12
00:00:46,500 --> 00:00:47,310
as what to install.

13
00:00:47,610 --> 00:00:51,330
And so we're gonna start off this example, Task Zero, which is grabbing the title of a page.

14
00:00:51,610 --> 00:00:55,680
And we're going to be using example dot com for this, which is just a very simple example, domain

15
00:00:55,680 --> 00:00:56,130
page.

16
00:00:56,670 --> 00:00:56,880
All right.

17
00:00:57,330 --> 00:00:58,710
So let's go ahead and get started.

18
00:00:59,280 --> 00:01:03,000
First, you'll notice that we have this example domain page unexampled dot com.

19
00:01:03,360 --> 00:01:08,520
The page title is going to be the text that shows up inside of this tab.

20
00:01:08,910 --> 00:01:14,460
So, for example, dot com, that example domain for the Wikipedia article that we saw about Jonas Salk.

21
00:01:14,490 --> 00:01:16,380
That title is Jonas Salk.

22
00:01:16,410 --> 00:01:17,490
Dash Wikipedia.

23
00:01:18,120 --> 00:01:22,080
So let's go ahead and show you the process in order to do some web scraping.

24
00:01:22,170 --> 00:01:24,420
The first thing you have to do is import.

25
00:01:25,830 --> 00:01:26,380
Requests.

26
00:01:26,470 --> 00:01:27,340
So that's step one.

27
00:01:28,330 --> 00:01:32,440
And then what we need to do is need to get a Web page through a request.

28
00:01:32,620 --> 00:01:37,120
So we say something like result is equal to.

29
00:01:38,120 --> 00:01:38,830
Requests.

30
00:01:38,940 --> 00:01:45,790
And then we call the get method off requests and we simply passin the actual YORO.

31
00:01:45,930 --> 00:01:49,740
So in this case, for example, domain, it's HTP.

32
00:01:49,760 --> 00:01:50,560
It's actually not secure.

33
00:01:50,610 --> 00:01:51,530
It's not HTP.

34
00:01:51,530 --> 00:01:51,760
Yes.

35
00:01:51,900 --> 00:02:00,390
But you're going to say HTP colon forward slash, forward slash to be the BW dot example dot com.

36
00:02:00,480 --> 00:02:07,170
So this is where you would put in the euro and make sure to include either HTP or HTP s depending on

37
00:02:07,200 --> 00:02:08,670
what the website actually has.

38
00:02:08,760 --> 00:02:11,790
So most sites are actually now HTP s.

39
00:02:11,850 --> 00:02:17,400
But keep in mind for some very simple sites, they actually may just be HTP, which essentially means

40
00:02:17,400 --> 00:02:18,750
whether or not they're secured.

41
00:02:20,760 --> 00:02:26,100
OK, so if you get any error on that, you want to double check your firewall and make sure it's not

42
00:02:26,100 --> 00:02:31,710
blocking Python from reaching out to the Internet and grabbing information now that we have this result.

43
00:02:31,800 --> 00:02:34,230
Let's go ahead and check the type.

44
00:02:35,220 --> 00:02:38,520
So we have this request that models and it's a response.

45
00:02:38,640 --> 00:02:44,340
So essentially what happened here is the request library that we downloaded goes and gets a response

46
00:02:44,340 --> 00:02:51,120
from W-W, that example dot com, and we can actually then call result dot text and it's an attribute.

47
00:02:51,750 --> 00:02:55,920
So if I run this, notice that it's actually this H.T. mail documents.

48
00:02:56,340 --> 00:03:02,760
And if I take a look at example domain and hit view page source, it's essentially this information

49
00:03:02,760 --> 00:03:05,880
here just stored as a giant python string.

50
00:03:06,420 --> 00:03:11,250
And this is nice because anything I can do if a python string, I can now do this large text.

51
00:03:11,820 --> 00:03:16,140
However, this is actually just a string in order to pass through this.

52
00:03:16,170 --> 00:03:18,180
What we need to do is use beautiful soup.

53
00:03:18,630 --> 00:03:23,940
The beautiful soup library, which we installed with B.S. for, is actually going to allow us to then

54
00:03:23,970 --> 00:03:27,190
grab and easily obtain information from this.

55
00:03:27,210 --> 00:03:30,030
Due to I.D. or class calls or HCM attacks.

56
00:03:30,630 --> 00:03:34,950
So right now we just have this giant string and we're going to convert it into a soup.

57
00:03:35,100 --> 00:03:40,320
So you can imagine that a soup, you have ingredients that go into a soup and then beautiful soup basically

58
00:03:40,320 --> 00:03:43,080
thinks of an e-mail document as kind of this giant soup.

59
00:03:43,380 --> 00:03:46,590
They can then grab information from like you could grab ingredients from.

60
00:03:46,920 --> 00:03:48,270
So go ahead.

61
00:03:48,360 --> 00:03:54,640
And now import B.S. for the next step is to actually create the soup variable.

62
00:03:54,690 --> 00:03:57,880
So we'll say soup is equal to B.S. for DOT.

63
00:03:59,190 --> 00:04:00,090
Beautiful soup.

64
00:04:01,140 --> 00:04:02,670
And we're gonna pass on two things here.

65
00:04:03,450 --> 00:04:05,410
We're going to pass in this texturing.

66
00:04:06,120 --> 00:04:11,830
And then finally, a string code for what engine to use to pass through this H tamale text.

67
00:04:12,060 --> 00:04:16,890
And for this, it's l x m l noticed I am passing this in as a string.

68
00:04:16,980 --> 00:04:18,330
And these are ls not ones.

69
00:04:18,660 --> 00:04:23,850
And that's why we actually had to pip install El SML because beautiful soup uses that on the back end

70
00:04:24,150 --> 00:04:31,260
to essentially go through this H tamale document and then figure out what is a C SS class.

71
00:04:31,350 --> 00:04:32,900
What is a C SS I.D..

72
00:04:33,210 --> 00:04:36,370
Which are the different H tamale elements and tags et cetera.

73
00:04:36,930 --> 00:04:46,170
So if I run this and now call soup, you'll notice that soup has essentially made this really easy to

74
00:04:46,170 --> 00:04:46,530
read.

75
00:04:46,800 --> 00:04:52,590
And now it looks exactly the same as the source code we saw in this example dot com.

76
00:04:53,010 --> 00:04:58,350
So essentially we went from kind of this rostering to this soup object.

77
00:04:58,410 --> 00:05:04,200
And now the soup object is smart enough to be able to grab things based off their tags or elements.

78
00:05:04,590 --> 00:05:08,130
So here we can see there is this title example domain.

79
00:05:09,000 --> 00:05:11,850
So now what I want to do is grab that from the soup.

80
00:05:12,000 --> 00:05:14,850
And the way I can do that is with soup that select.

81
00:05:16,280 --> 00:05:24,360
So we can say soup dot select, and this is where you can actually grab things from this H html document.

82
00:05:24,720 --> 00:05:30,770
And for this very first simple example where we're gonna do show you how you can grab just raw H.

83
00:05:30,770 --> 00:05:31,590
Tamao elements.

84
00:05:31,620 --> 00:05:32,820
So we're not focused on anything.

85
00:05:32,820 --> 00:05:33,460
We'll see assess.

86
00:05:33,770 --> 00:05:35,840
Basically, we're just looking for these tags.

87
00:05:35,840 --> 00:05:42,500
So things like H1 div body things happen to be in between these sort of less than or greater than symbols.

88
00:05:42,890 --> 00:05:45,680
So we have this H1 tag, this paragraph tag, et cetera.

89
00:05:46,310 --> 00:05:50,540
So we're going to do is we simply say as a string here, passan, one of the tag names.

90
00:05:50,960 --> 00:05:55,250
So the tag we were interested in was up here, which was the title tag.

91
00:05:56,030 --> 00:05:59,660
So notice there is an opening title and then a closing title.

92
00:05:59,840 --> 00:06:01,460
And in between is what we're really looking for.

93
00:06:01,940 --> 00:06:07,490
So we say soup dot select and we can passan that each time element tag.

94
00:06:08,510 --> 00:06:11,300
And notice it brings back this example domain.

95
00:06:11,360 --> 00:06:16,310
So basically looks through this document and it's smart enough to figure out, OK, where are these

96
00:06:16,310 --> 00:06:16,610
titled.

97
00:06:18,280 --> 00:06:24,440
Notice that by default it actually returns a list because technically there could be more than one tag

98
00:06:24,470 --> 00:06:28,640
or element on this page, especially for really complicated pages.

99
00:06:29,090 --> 00:06:31,500
So let's imagine that I wanted to grab paragraphs.

100
00:06:32,090 --> 00:06:39,140
I could say instead of Title P, which is the paragraph tag run that now I get a list of all the paragraph

101
00:06:39,140 --> 00:06:40,280
tags on that page.

102
00:06:40,670 --> 00:06:42,020
So here's one paragraph.

103
00:06:42,080 --> 00:06:42,920
And then there's a comma.

104
00:06:43,160 --> 00:06:44,510
And here's another paragraph.

105
00:06:44,990 --> 00:06:48,560
So this is what you do for just selecting simple each team.

106
00:06:48,710 --> 00:06:51,980
Elements as you just pass in as a string the name of the element.

107
00:06:52,010 --> 00:06:55,100
So you could do div body asml p.

108
00:06:55,250 --> 00:06:56,640
Let's go ahead and do H1 here.

109
00:06:57,800 --> 00:06:59,690
Run that and I can see example domain.

110
00:06:59,900 --> 00:07:05,390
So, again, the thing keep mind is when you call this, it does include those tags, so that opening

111
00:07:05,450 --> 00:07:06,110
and closing.

112
00:07:06,500 --> 00:07:13,220
And it also automatically reports back a list, even if there's just a single element that is return

113
00:07:13,220 --> 00:07:13,880
of that result.

114
00:07:14,360 --> 00:07:17,390
So because of that, you're probably next wondering let's go back to title here.

115
00:07:18,430 --> 00:07:23,000
How you can grab just the actual text, because if I'm searching for a title, I probably don't want

116
00:07:23,000 --> 00:07:25,520
to actually return this opening and closing title tag.

117
00:07:25,820 --> 00:07:27,320
I probably just want the actual text.

118
00:07:27,380 --> 00:07:30,410
Well, in this case, I need to grab the first item off of this.

119
00:07:30,500 --> 00:07:32,930
So let's go ahead and grab that first item.

120
00:07:33,820 --> 00:07:35,210
And now I have that string.

121
00:07:35,360 --> 00:07:38,300
So this indexing describes the first item off the list.

122
00:07:38,690 --> 00:07:41,060
And once I have that, I can use the get.

123
00:07:42,870 --> 00:07:45,890
Text method, which just returns back the stream.

124
00:07:46,470 --> 00:07:52,530
So let's imagine I wanted to just grab this string here of this domain is for an illustrative example,

125
00:07:52,570 --> 00:07:54,780
etc., then I would say souped up.

126
00:07:56,170 --> 00:07:59,230
Select title lips, actually, paragraph.

127
00:07:59,260 --> 00:08:02,770
So P and let's go ahead and return this back of some object.

128
00:08:02,800 --> 00:08:04,960
So we'll say site.

129
00:08:06,910 --> 00:08:08,920
Paragraph's is equal to suit.

130
00:08:08,990 --> 00:08:09,820
That's like pee.

131
00:08:10,240 --> 00:08:14,260
So, again, the string code matches up with some sort of H team L tag here.

132
00:08:14,920 --> 00:08:19,480
And now if I take a look at, say, paragraphs, as expected, it's a list.

133
00:08:19,810 --> 00:08:24,220
So if I want the very first item in that list, I can say go ahead and index at zero.

134
00:08:25,030 --> 00:08:26,020
That gets me back.

135
00:08:26,140 --> 00:08:27,280
This paragraph tag.

136
00:08:27,640 --> 00:08:29,650
And if you notice here, I check the type of this.

137
00:08:30,820 --> 00:08:31,840
This is not a string.

138
00:08:31,990 --> 00:08:37,240
It's actually a specialized, beautiful soup object, which is why I can do something like call.

139
00:08:37,570 --> 00:08:38,680
Don't get taxed on it.

140
00:08:39,010 --> 00:08:41,230
So keep in mind, this is a string.

141
00:08:41,440 --> 00:08:47,590
It's a special, beautiful soup object, which then means I can do this, get text and it returns back

142
00:08:47,620 --> 00:08:48,490
this as a string.

143
00:08:49,270 --> 00:08:49,530
OK.

144
00:08:50,170 --> 00:08:52,540
So we learned a couple of important things here.

145
00:08:52,630 --> 00:08:54,370
And let me just go over the steps briefly.

146
00:08:54,550 --> 00:08:57,010
The first thing you do is we import requests.

147
00:08:57,400 --> 00:09:00,160
We say requests that get passed on your euro.

148
00:09:00,370 --> 00:09:02,870
You may need to double check if it's HTP or HTP.

149
00:09:02,880 --> 00:09:03,250
Yes.

150
00:09:03,640 --> 00:09:06,640
You get back this response which has this text attribute.

151
00:09:07,480 --> 00:09:08,080
You go ahead.

152
00:09:08,170 --> 00:09:15,220
And after you import B.S. for you pass in that result, that text along with elec smells the code of

153
00:09:15,220 --> 00:09:20,500
what to use to decipher this into beautiful soup, which then creates this easy to use soup object.

154
00:09:20,860 --> 00:09:25,360
And the main thing we do at this soup object is we just select elements off of it.

155
00:09:25,900 --> 00:09:31,900
And so a big part of this is understanding how to use this select code and what pass on here as the

156
00:09:31,900 --> 00:09:35,800
string, because the rest of this pretty much stays the same for most other Web sites.

157
00:09:36,160 --> 00:09:40,230
Pretty much for all the Web sites we're gonna be working with for import requests, say requests, stop

158
00:09:40,240 --> 00:09:40,600
get.

159
00:09:41,110 --> 00:09:42,880
And then we create the soup object.

160
00:09:43,030 --> 00:09:48,070
The tricky part here is figure out what do you pass in into the select string.

161
00:09:48,490 --> 00:09:50,740
So this is the kind of where the work happens.

162
00:09:50,920 --> 00:09:55,090
As far as Web scrapings concern everything else before this pretty much stays the same.

163
00:09:55,450 --> 00:09:57,910
It's figuring out the different string codes you can pass in here.

164
00:09:58,240 --> 00:10:02,980
And that's where it becomes really helpful to understand each team attacks and CSF calls.

165
00:10:03,520 --> 00:10:06,850
So to expand on this, we're gonna do is in the next example.

166
00:10:07,150 --> 00:10:10,480
Coming up, we're going to show you how to grab all the elements of a class.

167
00:10:11,160 --> 00:10:11,540
Thanks.

168
00:10:11,740 --> 00:10:12,820
And I'll see at the next lecture.
