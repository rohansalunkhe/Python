1
00:00:05,310 --> 00:00:09,840
Welcome back, everyone, to this example project where we're going to be working multiple pages and

2
00:00:09,900 --> 00:00:10,380
items.

3
00:00:10,800 --> 00:00:15,420
Keep in mind we're still working with the same lecture notebook guide to web scraping as we did in the

4
00:00:15,420 --> 00:00:16,470
previous lectures.

5
00:00:18,070 --> 00:00:23,110
Basically, we've already seen how to grab elements just one at a time, such as a couple of images

6
00:00:23,170 --> 00:00:25,030
off a single Wikipedia article.

7
00:00:25,450 --> 00:00:30,910
But realistically, we want to be able to grab multiple elements and most likely across multiple pages

8
00:00:30,910 --> 00:00:31,530
of a Web site.

9
00:00:31,960 --> 00:00:36,910
And this is where we can combine our prior python knowledge with the Web scraping libraries to create

10
00:00:36,910 --> 00:00:41,380
really powerful scripts that are heavily customized to whatever task we're trying to achieve.

11
00:00:42,790 --> 00:00:48,610
We're going to be using a Web site specifically designed to practice Web scraping called w w w dot to

12
00:00:48,610 --> 00:00:49,750
scrape dot com.

13
00:00:50,320 --> 00:00:53,440
We're going to be practicing grabbing elements across multiple pages.

14
00:00:53,830 --> 00:00:55,090
Let's go ahead and get started.

15
00:00:55,360 --> 00:00:56,980
I'm going to go back to Jupiter notebook.

16
00:00:57,280 --> 00:00:59,170
OK, here I am at Jupiter Notebook.

17
00:00:59,590 --> 00:01:02,680
The Web site we're working with is called to scrape dot com.

18
00:01:03,010 --> 00:01:06,650
It's a Web site specifically designed for people to practice web scraping on.

19
00:01:06,670 --> 00:01:08,790
So we don't need to worry about asking for permissions.

20
00:01:09,220 --> 00:01:10,840
And so there's two different Web sites.

21
00:01:10,930 --> 00:01:12,100
One is called books.

22
00:01:12,130 --> 00:01:13,270
And then one is called quotes.

23
00:01:13,630 --> 00:01:17,590
Quotes is what you're going to be working with for your project later on your exercises.

24
00:01:17,890 --> 00:01:20,230
But we're going to practice with this book's Web site.

25
00:01:20,650 --> 00:01:24,320
It's a fictional bookstore and it's available at books to scrape PACOM.

26
00:01:24,680 --> 00:01:28,630
You can also just click on the image and it will take you directly to books to scrape dot com.

27
00:01:29,170 --> 00:01:32,380
And basically what this is, it's kind of like a fake bookstore.

28
00:01:32,380 --> 00:01:34,960
Web site has a list of books here.

29
00:01:34,960 --> 00:01:36,520
We can add of things to our basket.

30
00:01:37,180 --> 00:01:38,260
There's prices here.

31
00:01:38,380 --> 00:01:42,210
And then if we click on a book, it'll take us to the information for price.

32
00:01:42,250 --> 00:01:43,210
How much is in stock?

33
00:01:43,510 --> 00:01:46,960
The star rating and then says warning's the demo Web site.

34
00:01:47,260 --> 00:01:52,240
Basically, everything's kind of randomly assign, has a product description, product information and

35
00:01:52,240 --> 00:01:52,690
so on.

36
00:01:53,200 --> 00:01:55,690
So there's lots of different tasks you can play around with here.

37
00:01:56,020 --> 00:02:01,570
And I would encourage you after you work with us on this multiple page scheme that you kind of pick

38
00:02:01,570 --> 00:02:04,450
your own task and see if you can practice with your new skills.

39
00:02:04,870 --> 00:02:11,590
But what we're going to do is let's try to get the title of every book that has a two star rating.

40
00:02:12,190 --> 00:02:14,380
So I'm going to write down our goal here in The Notebook.

41
00:02:14,860 --> 00:02:17,260
So our goal, get the title.

42
00:02:18,660 --> 00:02:22,710
Of every book with a two star rating.

43
00:02:23,070 --> 00:02:27,270
So that means we're gonna have to be doing a lot of inspection on this Web site to figure out what particular

44
00:02:27,270 --> 00:02:28,620
class calls to use.

45
00:02:29,460 --> 00:02:36,000
Now, the first thing I'm going to do here is import requests and import B.S. for since that's what

46
00:02:36,000 --> 00:02:37,200
we're going to be working with.

47
00:02:37,800 --> 00:02:40,650
And the second thing I have to realize is if I scroll down here.

48
00:02:41,010 --> 00:02:44,030
Notice it says 1000 results showing one to 20.

49
00:02:44,610 --> 00:02:50,940
So that means if I scroll down, that this is actually page one of fifty, which means I hit next.

50
00:02:51,660 --> 00:02:53,960
And notice there's another page to this catalog.

51
00:02:54,450 --> 00:03:00,030
So what I need to figure out is how can I use Python and possibly some sort of looping system in order

52
00:03:00,030 --> 00:03:06,570
to keep going through each of these pages and keep doing the web scraping necessary because it's not

53
00:03:06,600 --> 00:03:10,860
all just existing on one page, as we previously saw with Wikipedia articles.

54
00:03:11,370 --> 00:03:16,200
So the first thing to do is figure out what sort of you RL procedure is happening.

55
00:03:16,470 --> 00:03:23,160
When I go from one page to the next and if I actually copy and paste the you are out so I can see what

56
00:03:23,160 --> 00:03:25,320
it looks like in a pace that as a string here.

57
00:03:25,800 --> 00:03:33,540
Notice that it's books to scrape, dot com slash catalog slash page dash and then the number inserted

58
00:03:33,540 --> 00:03:33,840
there.

59
00:03:34,410 --> 00:03:39,660
So that means it looks like a good idea would be to loop and insert a string version of a number.

60
00:03:40,170 --> 00:03:43,480
And in this case, I know it goes all the way to 50.

61
00:03:43,980 --> 00:03:48,180
Later on in your exercises, you'll have to think of how to use a while loop for when you don't know

62
00:03:48,240 --> 00:03:49,500
the last page number.

63
00:03:50,490 --> 00:03:50,650
OK.

64
00:03:51,240 --> 00:03:56,610
So we already know that something we have to do is we're gonna have to go throughout these pages and

65
00:03:56,610 --> 00:03:58,090
possibly fill in that number.

66
00:03:58,220 --> 00:04:01,940
So note here, it looks like page three follows that same pattern.

67
00:04:03,400 --> 00:04:04,370
So page three.

68
00:04:04,390 --> 00:04:05,500
Page two, et cetera.

69
00:04:06,010 --> 00:04:07,840
One thing we have to check is the home page.

70
00:04:07,930 --> 00:04:10,630
So let's go ahead and make sure that this works with page one.

71
00:04:11,110 --> 00:04:15,880
So going to come back to the home page here and then going to fill out that three or four one.

72
00:04:16,610 --> 00:04:19,330
And that, in fact, does bring us back to the first results.

73
00:04:19,390 --> 00:04:20,170
One through 20.

74
00:04:20,680 --> 00:04:21,820
So it looks like we're good there.

75
00:04:22,570 --> 00:04:28,270
Which means I just need to figure out how I can make this flexible so I can fill in this number with

76
00:04:28,480 --> 00:04:29,080
a string.

77
00:04:29,740 --> 00:04:32,350
Well, something I can do is just use the DOT format method.

78
00:04:32,920 --> 00:04:35,320
Lots of different ways you could solve for this.

79
00:04:35,830 --> 00:04:38,320
But what I'm going to do is I'm just gonna have a base.

80
00:04:38,320 --> 00:04:45,180
You are all going to copy and paste this string and then instead of a number, I'm going to fill this

81
00:04:45,180 --> 00:04:46,660
in with curly braces.

82
00:04:47,270 --> 00:04:54,400
And what's nice about this is then I can say base you URL, dot format and then whatever I want to insert

83
00:04:54,550 --> 00:04:56,110
into that format will show up.

84
00:04:56,140 --> 00:04:58,050
So I can say it's go to page 20.

85
00:04:58,750 --> 00:05:01,690
I run that and it will automatically fill in 20.

86
00:05:02,200 --> 00:05:09,210
So that means if I'm looping something such as a numbers, I can say page number is equal to page twelve.

87
00:05:09,700 --> 00:05:10,900
Then I can just passin.

88
00:05:13,510 --> 00:05:14,410
That page, no.

89
00:05:15,500 --> 00:05:16,850
And it will work.

90
00:05:17,420 --> 00:05:17,630
OK.

91
00:05:17,690 --> 00:05:19,720
So, again, we have this base, you, Earl.

92
00:05:20,120 --> 00:05:26,360
And now I figure out a way to conveniently go to any particular page or as you can imagine, I could

93
00:05:26,360 --> 00:05:30,160
loop over some range of pages since I know it goes from pages one to 50.

94
00:05:31,040 --> 00:05:31,320
OK.

95
00:05:31,820 --> 00:05:37,730
So we figured out that problem of this base, your URL, and then how to continue grabbing elements

96
00:05:37,730 --> 00:05:38,060
from it.

97
00:05:38,780 --> 00:05:44,270
Next thing I need to do is figure out how to scrape every page in that catalog Usenet loop and then

98
00:05:44,270 --> 00:05:47,960
figure out what tag and class represents the star rating.

99
00:05:48,530 --> 00:05:52,610
So we'll come over here to products and provide.

100
00:05:52,610 --> 00:05:56,570
The easiest way to do this is begin inspecting these actual stars.

101
00:05:56,850 --> 00:05:59,120
So it's maybe a little tricky because these could be images.

102
00:05:59,480 --> 00:06:00,410
Let's go in and check this out.

103
00:06:01,400 --> 00:06:04,580
I'm going to inspect these icon stars.

104
00:06:04,730 --> 00:06:10,670
And notice here on the inspect, it looks like I have icon, star, icon, star, icon, star, icon,

105
00:06:10,670 --> 00:06:11,660
star, icon, star.

106
00:06:11,990 --> 00:06:14,000
So everything is rated out of five stars.

107
00:06:14,300 --> 00:06:19,490
So I have this little list here of five class icon stars.

108
00:06:19,850 --> 00:06:26,690
And if I take a look at another one going to inspect this second star that it looks to appear the exact

109
00:06:26,690 --> 00:06:27,350
same list.

110
00:06:27,530 --> 00:06:32,000
So what I can't do is somehow figure out if a star is kind of filled in or not.

111
00:06:32,730 --> 00:06:33,650
That's a little trickier.

112
00:06:33,980 --> 00:06:38,510
But what's nice is there's a class above this which actually actually gives the rating.

113
00:06:38,540 --> 00:06:40,430
So this is star rating one class.

114
00:06:40,850 --> 00:06:44,750
And if I take a look at this three star book, inspect that again.

115
00:06:44,840 --> 00:06:46,490
This is star rating three.

116
00:06:47,120 --> 00:06:51,050
And recall, our goal is to grab every book title of a star rating of two.

117
00:06:51,620 --> 00:06:52,700
So scroll down here.

118
00:06:54,020 --> 00:06:58,490
And see a two star rated book, Starving Hearts, and going to inspect this.

119
00:06:59,360 --> 00:07:04,330
And it looks like the class I'm looking for is called Star Dash Reading, too.

120
00:07:04,880 --> 00:07:05,570
So this is great.

121
00:07:05,630 --> 00:07:10,130
I already now know what the class call I'm looking for is.

122
00:07:11,810 --> 00:07:17,570
The other thing to keep in mind is that I'm not just looking for the two star reading, I'm looking

123
00:07:17,570 --> 00:07:24,640
for the associated title, and it looks like all of this is contained within a product class.

124
00:07:24,660 --> 00:07:30,560
So if I highlight product underscore pod, then I have this little PA that's holding all this information

125
00:07:30,560 --> 00:07:32,060
for this particular book.

126
00:07:32,360 --> 00:07:36,980
Looks like it's holding the price, whether it's in stock, the image of the book as well as the title.

127
00:07:37,490 --> 00:07:43,760
So probably what I want to do is just grab this entire little product pod for everything on that Web

128
00:07:43,760 --> 00:07:47,720
page and then filter out based off this star rating to class.

129
00:07:48,290 --> 00:07:54,230
So let's begin by grabbing everything on just the first page that contains this product, underscore

130
00:07:54,260 --> 00:07:55,370
pod class.

131
00:07:55,820 --> 00:08:01,520
Now that we know what class we're looking for, let's go ahead and create a request and grab that particular

132
00:08:01,520 --> 00:08:02,660
class from the soup.

133
00:08:03,860 --> 00:08:04,640
We'll come back here.

134
00:08:05,290 --> 00:08:06,820
I'm going to say, ah, yes.

135
00:08:07,070 --> 00:08:09,470
Is equal to requests.

136
00:08:10,600 --> 00:08:11,430
Don't get.

137
00:08:12,470 --> 00:08:14,110
And let's just do it from the first page.

138
00:08:14,140 --> 00:08:15,490
So we'll say based Yoro.

139
00:08:17,170 --> 00:08:21,190
And I'm going to format in one that can be an integer or a string.

140
00:08:21,280 --> 00:08:21,940
It's the work.

141
00:08:23,360 --> 00:08:24,250
There's a request.

142
00:08:24,290 --> 00:08:26,440
Let's turn it into a soup objects.

143
00:08:26,540 --> 00:08:29,380
We'll say B.S. for beautiful soup.

144
00:08:30,210 --> 00:08:32,700
Pass on the text from that request and then.

145
00:08:34,010 --> 00:08:36,320
The engine we used to decode this.

146
00:08:37,170 --> 00:08:40,460
And if we take a look at our soup, looks like everything's there.

147
00:08:40,970 --> 00:08:42,200
And let's select.

148
00:08:43,380 --> 00:08:46,080
That class call, which if we take a look.

149
00:08:46,170 --> 00:08:46,500
It was.

150
00:08:46,680 --> 00:08:48,730
Products underscore pod.

151
00:08:49,320 --> 00:08:50,730
And the stock, the notes that it said.

152
00:08:50,760 --> 00:08:52,590
Class equals product pod.

153
00:08:53,430 --> 00:08:54,300
We run this.

154
00:08:54,930 --> 00:08:57,000
And let's take a look at the length of this.

155
00:08:58,490 --> 00:09:02,830
So going to say length of soup, that select product pod, and it equals 20.

156
00:09:03,380 --> 00:09:05,480
Now we have to ask ourselves, does that make sense?

157
00:09:05,720 --> 00:09:10,340
Well, if you take a look back at the Web site, it said it's showing results one through 20, which

158
00:09:10,340 --> 00:09:15,770
means it does confirm that most likely each of those product pods went with one of those books.

159
00:09:16,070 --> 00:09:20,660
You have a little bit of checking just to make sure, but we're pretty much on target that each of those

160
00:09:20,660 --> 00:09:23,300
little product pods has information.

161
00:09:23,810 --> 00:09:29,450
So what we're going to do and part two of this lecture series is continue right where we left off and

162
00:09:29,450 --> 00:09:36,320
finish this up by adding in Python code that allows us to explore this soup further, as well as then

163
00:09:36,410 --> 00:09:42,950
loop through different pages on that Web site to create a list of all the titles that are to Starbucks.

164
00:09:43,440 --> 00:09:43,780
Thanks.

165
00:09:43,970 --> 00:09:45,020
And I'll see at the next lecture.
