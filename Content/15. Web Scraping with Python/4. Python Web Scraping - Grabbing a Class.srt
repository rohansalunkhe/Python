1
00:00:05,300 --> 00:00:06,190
Welcome back, everyone.

2
00:00:06,410 --> 00:00:11,030
In this lecture, we're going to show you an example of grabbing all the elements associated with a

3
00:00:11,030 --> 00:00:13,310
class inside of a Web site.

4
00:00:14,940 --> 00:00:20,130
As we previously mentioned, a big part of Web scraping off the beautiful soup library in Python is

5
00:00:20,130 --> 00:00:26,280
figuring out what string syntax to pass in to the soup, that select method, because pretty much everything

6
00:00:26,280 --> 00:00:30,560
before that soup, that select method is going to be the same regardless of the website.

7
00:00:30,990 --> 00:00:32,640
It's figuring out that string syntax.

8
00:00:32,670 --> 00:00:33,600
That can be the tricky part.

9
00:00:34,110 --> 00:00:38,540
So I want to do is go through a table of some common examples and these make a lot of sense.

10
00:00:38,550 --> 00:00:44,010
If you already know the CSX syntax, since that's where this string syntax is derived from.

11
00:00:45,310 --> 00:00:49,230
So this table on the left hand side shows you an example, syntax.

12
00:00:49,420 --> 00:00:54,700
And then on the right hand side basically describes what the matching result would be throughout that

13
00:00:54,700 --> 00:00:55,740
soup documents.

14
00:00:55,930 --> 00:00:58,810
And this table is present inside the lecture notebook.

15
00:00:59,710 --> 00:01:01,450
So I just want to briefly go over these.

16
00:01:01,930 --> 00:01:06,030
The first row is going to grab every element with a certain tag.

17
00:01:06,400 --> 00:01:10,270
And we saw that in their previous example when we grabbed, for instance, everything of a paragraph

18
00:01:10,270 --> 00:01:13,510
tag that is the P tag or everything with a title tag.

19
00:01:14,050 --> 00:01:19,720
Now we want to grab everything or a particular I.D. so we can do that by saying soup that select.

20
00:01:19,810 --> 00:01:24,100
And then we use a hashtag symbol right in front of whatever the element idea is.

21
00:01:24,280 --> 00:01:29,980
So in the HDMI document, you'll see something like I.D. is equal to then some sort of string I.D. code.

22
00:01:30,310 --> 00:01:33,270
And you can use that with the hash tag soup that select.

23
00:01:33,490 --> 00:01:35,560
And it will grab that I.D. element.

24
00:01:36,340 --> 00:01:39,990
Similarly, for a class, we did the same thing except instead of a hash tag.

25
00:01:40,090 --> 00:01:40,870
It's a period.

26
00:01:41,320 --> 00:01:46,900
And this follows along with CSX syntax, if you're familiar with filling out a CSX documents.

27
00:01:47,590 --> 00:01:52,180
And if you want to get a little more complex and look for elements within elements, there's a couple

28
00:01:52,180 --> 00:01:53,200
different ways to do this.

29
00:01:53,560 --> 00:01:59,650
We can say soup that select something like div span and there's a space in between those two element

30
00:01:59,650 --> 00:02:00,070
names.

31
00:02:00,370 --> 00:02:06,510
And what that does is basically it grabs any elements named span within a div element.

32
00:02:06,880 --> 00:02:09,220
And you can switch those out for any element names.

33
00:02:09,610 --> 00:02:15,580
So essentially it's saying find any elements named on that second string within that first string,

34
00:02:16,470 --> 00:02:21,910
if you want it to be even more precise for directly within something, you can use the greater than

35
00:02:21,970 --> 00:02:22,480
symbol.

36
00:02:22,780 --> 00:02:28,030
And basically what this would return back are any elements named span directly within a div element

37
00:02:28,090 --> 00:02:29,860
with absolutely nothing in between.

38
00:02:30,390 --> 00:02:30,620
OK.

39
00:02:31,120 --> 00:02:34,720
So all of these string codes are derived from CSX syntax.

40
00:02:35,020 --> 00:02:39,820
So I would highly encourage you if you're having trouble, if these two just learn a little bit of CSX.

41
00:02:39,880 --> 00:02:43,420
And again, we have a little bit of guide for you in our notebook.

42
00:02:43,750 --> 00:02:46,930
But we can also do is show you an example of using these.

43
00:02:47,020 --> 00:02:47,890
So let's go in and do that.

44
00:02:48,190 --> 00:02:53,200
Let's show you how to grab all the elements of a particular class from a real Web page with Python and

45
00:02:53,200 --> 00:02:53,770
beautiful suit.

46
00:02:54,350 --> 00:02:55,490
Head back to that same notebook.

47
00:02:55,540 --> 00:02:56,620
We were working in last time.

48
00:02:57,760 --> 00:02:57,950
All right.

49
00:02:57,970 --> 00:02:59,680
Here I am at the Jupiter notebook.

50
00:03:00,170 --> 00:03:05,710
What I'm going to do is head over to Wikipedia page or Wikipedia article, and you can choose any major

51
00:03:05,710 --> 00:03:09,490
Wikipedia article that has a table of contents for this particular example.

52
00:03:10,030 --> 00:03:13,090
We're going to do here is we're gonna check out grasshoppers Wikipedia article.

53
00:03:13,150 --> 00:03:16,480
And in case you don't know, Grace Hopper is a famous computer scientist.

54
00:03:17,020 --> 00:03:22,660
And what we're gonna do is let's imagine for some reason I want to grab all the strings in this table

55
00:03:22,660 --> 00:03:23,440
of contents.

56
00:03:23,890 --> 00:03:25,600
So there's a couple of different ways we can do this.

57
00:03:25,960 --> 00:03:31,810
But the main way we want to understand is the ability to inspect a particular elements on a page and

58
00:03:31,810 --> 00:03:33,730
then grab an associated class.

59
00:03:34,180 --> 00:03:38,300
Now, typically, there's more than one class or I.D. or system of calls.

60
00:03:38,320 --> 00:03:42,670
You can do in order to grab information off of a Web page.

61
00:03:42,850 --> 00:03:48,190
But let's go ahead and just get you practicing with a generalized method, which is once you figure

62
00:03:48,190 --> 00:03:49,450
out what you're interested in grabbing.

63
00:03:49,870 --> 00:03:50,470
Go ahead and write.

64
00:03:50,470 --> 00:03:52,900
Click on one of these and then hit inspect.

65
00:03:53,410 --> 00:03:54,130
So essentially, right.

66
00:03:54,130 --> 00:04:00,460
Click on what you actually want to grab and you'll notice that if we take a look at the elements that

67
00:04:00,460 --> 00:04:03,250
are returned, it looks like we have a couple of classes here.

68
00:04:03,640 --> 00:04:06,460
We have something that says classes equal to TLC.

69
00:04:06,460 --> 00:04:06,940
No.

70
00:04:07,290 --> 00:04:10,250
Another one that says class is equal to TLC text.

71
00:04:10,690 --> 00:04:12,280
And we can be an expanding on this.

72
00:04:12,340 --> 00:04:18,250
And as we go through these elements, you'll noted that they're highlighted in the actual Web page.

73
00:04:18,340 --> 00:04:21,580
So it's basically telling you what everything is associated with.

74
00:04:21,730 --> 00:04:28,570
So, for example, if I expand on this list, I see class TLC level one, some sort of H reft to retirement.

75
00:04:28,600 --> 00:04:31,450
Essentially, it will bring you down to the retirement section of that page.

76
00:04:31,870 --> 00:04:38,260
I can keep expanding and I see the actual text that I want here called retirement and I see it's associated

77
00:04:38,260 --> 00:04:40,150
with the class T o c text.

78
00:04:40,430 --> 00:04:44,940
And that makes a lot of sense because it stands for most likely table of contents text.

79
00:04:45,400 --> 00:04:52,210
So let's go ahead and see if we can use this class call t o c text in order to grab all the strings

80
00:04:52,330 --> 00:04:53,920
that are in the table of contents.

81
00:04:54,460 --> 00:04:56,830
So let's begin by heading over back to our Jupiter notebook.

82
00:04:57,550 --> 00:04:59,200
And let's create a new request.

83
00:04:59,380 --> 00:05:01,030
So I'm going to say, ah, yes.

84
00:05:01,540 --> 00:05:04,000
Is equal to requests.

85
00:05:05,390 --> 00:05:08,900
Don't get and then I'm going to pass on the full YORO.

86
00:05:09,300 --> 00:05:09,560
Go ahead.

87
00:05:09,630 --> 00:05:09,960
Just.

88
00:05:10,970 --> 00:05:12,080
Copy and paste this here.

89
00:05:13,800 --> 00:05:16,830
And there's the full year Earl, I need to include this HDTV piece.

90
00:05:17,550 --> 00:05:19,590
So we'll go ahead and make that request again.

91
00:05:19,620 --> 00:05:23,640
Make sure to have any firewall or any sort of Internet issue that's blocking this request.

92
00:05:24,510 --> 00:05:27,370
And then we make a soup out of the text from that request.

93
00:05:27,480 --> 00:05:31,470
So we'll say B.S. four is equal to beautiful soup.

94
00:05:33,430 --> 00:05:34,030
Ah, yes.

95
00:05:34,090 --> 00:05:35,090
Result thought text.

96
00:05:35,400 --> 00:05:36,390
And then we tell.

97
00:05:36,420 --> 00:05:40,140
Beautiful suit to use l x m l in order to decipher it.

98
00:05:41,010 --> 00:05:42,840
We run that and now we have our soup.

99
00:05:42,960 --> 00:05:45,570
So if we check out the soup, it's gonna be quite large.

100
00:05:45,600 --> 00:05:47,400
Because there's a lot of stuff in that Wikipedia article.

101
00:05:47,670 --> 00:05:50,940
But we can see here basically grab the entire contents from that page.

102
00:05:51,450 --> 00:05:55,800
OK, so now it comes time to figure out what to select from this.

103
00:05:56,010 --> 00:06:01,980
So obviously this may take some experimentation, but we already saw when we were inspecting elements

104
00:06:02,310 --> 00:06:05,190
that this TOEIC text class looks like a good contender.

105
00:06:05,730 --> 00:06:08,220
So what I want to do is a soup thought select.

106
00:06:09,330 --> 00:06:11,200
And then passing the appropriate string code.

107
00:06:11,540 --> 00:06:13,340
And because we're working class equals.

108
00:06:13,360 --> 00:06:15,310
That means the first one should be a period.

109
00:06:15,370 --> 00:06:15,940
Or DOT.

110
00:06:16,390 --> 00:06:18,520
And then I'm going to pass in the class name.

111
00:06:18,970 --> 00:06:21,760
So it says classes equal to t o c text.

112
00:06:21,880 --> 00:06:22,570
So let's pass that.

113
00:06:22,570 --> 00:06:24,250
N t o c.

114
00:06:25,680 --> 00:06:26,250
Text.

115
00:06:27,270 --> 00:06:34,770
I run that and here I can see all the spans that have that particular class called TOEIC text, so starts

116
00:06:34,770 --> 00:06:38,270
off with early life and education, career or two, et cetera.

117
00:06:38,880 --> 00:06:44,520
So it looks like we're actually able to use that class call in order to grab all the information or

118
00:06:44,520 --> 00:06:46,620
the strings off this table of contents.

119
00:06:47,000 --> 00:06:52,200
And you can do that for essentially anything that's linked appropriately with a class call on this Web

120
00:06:52,200 --> 00:06:52,680
page.

121
00:06:53,100 --> 00:06:59,370
So going to come back here and let's imagine that I actually just want this text, not the span in class

122
00:06:59,370 --> 00:06:59,700
call.

123
00:07:00,180 --> 00:07:05,220
So recall, we can actually then loop through this and grab the text attribute from these items.

124
00:07:05,790 --> 00:07:09,420
So, for example, that very first item here, if we check the type.

125
00:07:11,630 --> 00:07:14,830
Recall, it's a specialized, beautiful soup element tag.

126
00:07:15,020 --> 00:07:16,910
It's not just a normal python string.

127
00:07:18,700 --> 00:07:25,000
So if I say my first item is equal to soup that select at zero.

128
00:07:26,690 --> 00:07:28,340
Then I can see first item here.

129
00:07:28,610 --> 00:07:32,550
And after I've go ahead and define it, I can say that tab.

130
00:07:33,130 --> 00:07:35,960
And I can see all the various attributes and method of calls here.

131
00:07:36,010 --> 00:07:39,260
And pretty much the one we're most interested in is just grabbing the text.

132
00:07:39,290 --> 00:07:44,980
So that's a text attribute that's available to us, which just returns the text in between those spang

133
00:07:44,990 --> 00:07:50,990
calls early life and education, which means if I wanted to print out all those strings, I could say

134
00:07:51,860 --> 00:07:54,530
for item in soup that select.

135
00:07:55,570 --> 00:07:57,220
And they will say t, o, c.

136
00:07:58,760 --> 00:07:59,300
Text.

137
00:08:00,350 --> 00:08:00,700
Prints.

138
00:08:01,920 --> 00:08:02,850
Item, dot text.

139
00:08:03,600 --> 00:08:06,120
We run that and then we get to see the results.

140
00:08:06,600 --> 00:08:08,040
All right, that's it for this lecture.

141
00:08:08,070 --> 00:08:10,710
Coming up next, we're going to keep continuing to examples.

142
00:08:11,070 --> 00:08:14,820
I'm going to show you how to grab an image from a Web site and then save it to your computer.

143
00:08:15,150 --> 00:08:16,080
I'll see you at the next lecture.
