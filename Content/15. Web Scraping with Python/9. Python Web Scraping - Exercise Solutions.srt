1
00:00:05,320 --> 00:00:06,210
Welcome back, everyone.

2
00:00:06,490 --> 00:00:11,110
In this lecture, we're going to work through some examples, solutions for the Web scraping exercises.

3
00:00:11,380 --> 00:00:12,070
Let's get started.

4
00:00:12,640 --> 00:00:14,050
OK, so first things first.

5
00:00:14,050 --> 00:00:18,220
We wanted to import any libraries we think we needed, and hopefully you're able to figure out that

6
00:00:18,370 --> 00:00:25,390
it's going to be the libraries we are working with, which is import requests and import B.S. for next.

7
00:00:25,390 --> 00:00:29,960
We want to use the requests library and beautiful soup to connect to quotes to scrape.

8
00:00:30,000 --> 00:00:30,540
Dot com.

9
00:00:31,030 --> 00:00:32,910
I'll go ahead and open that in a new tab just in case.

10
00:00:32,910 --> 00:00:34,120
I need to reference it later.

11
00:00:34,570 --> 00:00:40,660
But you should be able to then just make a request by saying something like results or rez is equal

12
00:00:40,660 --> 00:00:42,230
to requests.

13
00:00:42,330 --> 00:00:43,060
That gets.

14
00:00:43,750 --> 00:00:46,710
And then we'll say, Passan, that you are all lips.

15
00:00:47,170 --> 00:00:48,640
Make sure we actually copy this first.

16
00:00:49,150 --> 00:00:55,450
So I'm going to just copy this and then paste in as a string.

17
00:00:57,330 --> 00:00:57,980
So we run that.

18
00:00:58,070 --> 00:01:01,550
It makes the request and we can then view the text.

19
00:01:02,390 --> 00:01:06,310
And this is essentially the same document that we had here below.

20
00:01:08,300 --> 00:01:08,550
All right.

21
00:01:09,350 --> 00:01:13,370
Now, the first real task is to get the names of all the authors on the first page.

22
00:01:13,910 --> 00:01:15,920
So as I mention in the overview.

23
00:01:16,040 --> 00:01:18,410
Notice that this is actually a set, not a list.

24
00:01:18,800 --> 00:01:23,840
And that's because if you look at to scraped dot com, there could actually be repeat authors.

25
00:01:24,230 --> 00:01:29,030
So that means we don't actually just want to go through and grab every author's name and just add it

26
00:01:29,030 --> 00:01:32,120
to a list, because Albert Einstein is here twice.

27
00:01:32,150 --> 00:01:33,920
So we don't want to list Albert Einstein twice.

28
00:01:34,460 --> 00:01:40,940
That way I can take advantage of a set in order to not even worry about adding duplicates to a set because

29
00:01:40,940 --> 00:01:42,410
a set is just unique items.

30
00:01:42,920 --> 00:01:49,160
So what I'm going to do here is I'm first going to create a soup using B.S. for.

31
00:01:50,140 --> 00:01:50,980
Beautiful soup.

32
00:01:52,040 --> 00:01:57,140
Pacien rest text and then L x M.L..

33
00:01:58,690 --> 00:01:59,990
And now I have this soup.

34
00:02:00,740 --> 00:02:06,380
And what I need to do is figure out from the soup what is the correct class call or tag call in order

35
00:02:06,380 --> 00:02:07,490
to grab those authors.

36
00:02:07,850 --> 00:02:13,190
So one way to do it is by scrolling through the soup and trying to figure out if you see Albert Einstein.

37
00:02:13,250 --> 00:02:14,630
What class call he has.

38
00:02:15,040 --> 00:02:17,930
And you'll notice here that there's actually a connection here, classical to author.

39
00:02:18,290 --> 00:02:20,720
So that may be a good place to start in Lotus, Jake.

40
00:02:20,960 --> 00:02:23,250
J.K. Rowling as well, classes author.

41
00:02:23,660 --> 00:02:26,180
Or you come to quotes the scrape and then.

42
00:02:26,180 --> 00:02:26,390
Right.

43
00:02:26,390 --> 00:02:29,350
Click on this name here, inspect.

44
00:02:29,690 --> 00:02:31,520
And then you can expect this yourself.

45
00:02:31,970 --> 00:02:35,000
And it looks like this does actually have class equal to author.

46
00:02:35,450 --> 00:02:37,280
Let's go ahead and use that and see if it works.

47
00:02:37,370 --> 00:02:40,090
We'll say souped up select.

48
00:02:40,490 --> 00:02:42,860
And since it's a class, we'll say dot.

49
00:02:43,980 --> 00:02:46,710
Author, go ahead and run that.

50
00:02:47,290 --> 00:02:48,150
And it looks like it's working.

51
00:02:48,180 --> 00:02:52,010
We have Albert Einstein, J.K. Rowling, Albert Einstein, Marilyn Monroe, etc..

52
00:02:52,710 --> 00:02:54,390
Now, I just want the unique authors.

53
00:02:54,480 --> 00:02:59,700
So what I'm going to do is in order to get this set list or excuse me, this set, what I want to do,

54
00:02:59,730 --> 00:03:03,420
say authors, is equal to an empty set.

55
00:03:04,230 --> 00:03:07,380
And then I'm going to say for name in soup.

56
00:03:08,350 --> 00:03:11,470
That select author.

57
00:03:13,370 --> 00:03:17,060
Go ahead and say, authors add.

58
00:03:18,120 --> 00:03:20,640
And the name is this entire thing here.

59
00:03:21,000 --> 00:03:23,850
What I really just want is the text in between the text.

60
00:03:24,060 --> 00:03:28,720
So we saw in the lectures that I can grab that with name that text.

61
00:03:29,670 --> 00:03:34,260
So I run that and then I can get back the authors that are on the first page.

62
00:03:35,340 --> 00:03:39,120
Next task is to create a list of all the quotes on the first page.

63
00:03:39,690 --> 00:03:42,240
So some early, we already have our soup created.

64
00:03:42,600 --> 00:03:48,660
What I need to do is figure out what the actual class or tag call is going to come back to the Web site,

65
00:03:49,500 --> 00:03:49,890
going to.

66
00:03:49,890 --> 00:03:50,130
Right.

67
00:03:50,130 --> 00:03:56,010
Click on these quotes, hit, inspect, and you'll notice that it looks like the class call is text.

68
00:03:56,460 --> 00:03:58,830
So maybe this is the class call we can use.

69
00:03:59,100 --> 00:04:00,690
Often you have to experiment.

70
00:04:01,200 --> 00:04:03,300
So let's go ahead and try it and see if it works for us.

71
00:04:03,390 --> 00:04:05,520
We'll say soupe thought.

72
00:04:06,790 --> 00:04:07,480
Select.

73
00:04:08,710 --> 00:04:11,770
And we'll say Dot, because it's class call, dot text.

74
00:04:12,220 --> 00:04:15,700
We run that and it looks like these are all the quotes on the page.

75
00:04:15,750 --> 00:04:16,570
So we have a day of thought.

76
00:04:16,580 --> 00:04:19,210
Sunshine is like, you know, night at the bottom.

77
00:04:19,300 --> 00:04:20,500
Funny quote by Steve Martin.

78
00:04:20,980 --> 00:04:25,210
And then the first quote, The world as we have created it and we see here at the world is we've created

79
00:04:25,210 --> 00:04:25,300
it.

80
00:04:25,590 --> 00:04:26,380
OK, perfect.

81
00:04:26,740 --> 00:04:29,200
So that means I can simply do sort of same loop.

82
00:04:29,740 --> 00:04:30,610
So we'll do the following.

83
00:04:31,210 --> 00:04:33,400
We'll create a list for the quotes.

84
00:04:34,980 --> 00:04:37,340
So quotes is equal to an empty list.

85
00:04:38,000 --> 00:04:47,930
And then for name and soup, that select text or sees me for quote, it's gotten changed that for quote

86
00:04:48,020 --> 00:04:53,720
in supercut, select text, say quotes and I'm going to appends and this is now a list.

87
00:04:54,500 --> 00:04:55,640
The actual text there.

88
00:04:56,690 --> 00:04:59,840
And now when I call quotes, I get back a list of all the quotes.

89
00:05:00,140 --> 00:05:00,650
Perfect.

90
00:05:01,900 --> 00:05:02,160
OK.

91
00:05:02,390 --> 00:05:07,250
Next, we want to inspect the site and use beautiful soup to extract those top 10 tags.

92
00:05:07,670 --> 00:05:08,990
Lots different ways you can do this.

93
00:05:09,310 --> 00:05:16,880
Main way, though, is that we need to figure out what the actual tag or class call for those top 10

94
00:05:16,880 --> 00:05:17,480
taxes.

95
00:05:17,540 --> 00:05:18,320
So I'm going to right.

96
00:05:18,320 --> 00:05:24,140
Click on one of these hit inspect and you'll notice that there's this class tag.

97
00:05:24,710 --> 00:05:30,860
But if you search a little deeper, you'll notice that these tags, if I inspect them as well, also

98
00:05:30,860 --> 00:05:32,450
have that class tag.

99
00:05:32,960 --> 00:05:39,860
So what I need to do is figure out what class the top 10 tags have that these tags do not.

100
00:05:39,950 --> 00:05:42,440
So if you just say tag, you get every tag on this page.

101
00:05:42,560 --> 00:05:43,730
That's what make this a little tricky.

102
00:05:44,330 --> 00:05:49,940
So I'm going to inspect this again and see if I can find something that is a little more detailed and

103
00:05:49,940 --> 00:05:51,090
you'll notice that appear.

104
00:05:51,110 --> 00:05:54,140
We have a span which is tag that item.

105
00:05:54,710 --> 00:06:00,530
And if I were to inspect the original tags again, I actually don't see tag that item anywhere.

106
00:06:01,050 --> 00:06:07,580
So let's go ahead and give tag that item, that class call a try since that looks to be only appearing

107
00:06:08,060 --> 00:06:09,440
in the top 10 tags.

108
00:06:10,040 --> 00:06:16,700
So hop back over to my Web scraping notebook and I'll say soupe select.

109
00:06:18,380 --> 00:06:21,930
And I don't just want to use 10 because all the tags on the page had that.

110
00:06:22,380 --> 00:06:24,090
But instead I'll use tag item.

111
00:06:25,410 --> 00:06:25,890
Run that.

112
00:06:26,370 --> 00:06:28,440
And it looks like it's ten of them.

113
00:06:28,560 --> 00:06:29,850
So let's go in and check the length.

114
00:06:31,740 --> 00:06:33,900
And it's ten, which probably means we're matching up.

115
00:06:34,140 --> 00:06:41,070
And if you were to compare the words, we get back the whatever friendship, friends, truth, et cetera,

116
00:06:41,340 --> 00:06:42,740
those are the top 10 tags.

117
00:06:42,900 --> 00:06:45,310
Love, inspirational, friendship, friends, etc..

118
00:06:45,960 --> 00:06:51,180
So what I need to do is just figure out either you can append this to a list or you can just print them

119
00:06:51,180 --> 00:06:51,390
out.

120
00:06:51,510 --> 00:06:52,200
Does it really matter?

121
00:06:52,980 --> 00:06:59,250
So we'll say for item in soup dot select tag item.

122
00:07:00,150 --> 00:07:06,330
Go ahead and say print item that text and you run that and you get back these results if you want it,

123
00:07:06,350 --> 00:07:07,470
because save this to Allison.

124
00:07:07,470 --> 00:07:08,490
Just printed out the list.

125
00:07:08,970 --> 00:07:11,550
Main thing you have to figure out is that it just wasn't tag.

126
00:07:11,580 --> 00:07:14,160
It was tag that item as the main trick.

127
00:07:14,610 --> 00:07:14,820
OK.

128
00:07:16,230 --> 00:07:17,070
So we have those.

129
00:07:17,580 --> 00:07:22,980
And finally, your task on the last one is notice there's more than one page, so there's subsequent

130
00:07:22,980 --> 00:07:23,460
pages.

131
00:07:23,940 --> 00:07:28,530
And what you need to do is use what you know about four loops or while loops and string concatenation

132
00:07:28,890 --> 00:07:32,180
to loop through all this, to get all the unique authors on the Web site.

133
00:07:32,700 --> 00:07:37,530
So similar to what we did up here, grabbing all these authors, except I need to do it for all the

134
00:07:37,530 --> 00:07:38,070
pages.

135
00:07:38,550 --> 00:07:44,880
And we want you to do is try to make it robust enough so that it would work regardless of how many pages

136
00:07:44,880 --> 00:07:45,240
there were.

137
00:07:45,820 --> 00:07:51,450
It's all for show you how to do it using a for loop where you knew there was ten pages, because that's

138
00:07:51,450 --> 00:07:52,610
kind of the easier way to do it.

139
00:07:52,710 --> 00:07:54,810
And that's more similar to what we did in the exercises.

140
00:07:55,260 --> 00:07:56,190
So I'll show you that first.

141
00:07:56,220 --> 00:07:58,710
Keep in mind many different ways you can figure this out.

142
00:07:59,730 --> 00:08:03,210
I'm going to just create a base you are able to work with.

143
00:08:03,270 --> 00:08:04,830
So also you URLs equal to.

144
00:08:05,430 --> 00:08:07,110
And then let's go ahead and grab this here.

145
00:08:07,710 --> 00:08:09,100
I'll say it's equal to H.

146
00:08:09,100 --> 00:08:11,650
TDP quotes to scrape dot coms.

147
00:08:11,670 --> 00:08:12,420
Last page.

148
00:08:12,480 --> 00:08:13,080
Copy that.

149
00:08:13,890 --> 00:08:16,350
I'm going to stick that into a string.

150
00:08:17,360 --> 00:08:18,790
And so there's our base, you, Earl.

151
00:08:19,670 --> 00:08:22,340
And what we can do now, say y'all plus.

152
00:08:23,630 --> 00:08:28,550
And then concatenate it with kind of any number we want and then we'll concatenate it at the end of

153
00:08:28,550 --> 00:08:34,040
that number if you want, you can also use that format or really any sort of string interpolation to

154
00:08:34,430 --> 00:08:35,480
stick a number in there.

155
00:08:36,260 --> 00:08:42,050
So now we're going to do is simply say authors is equal to set.

156
00:08:43,470 --> 00:08:52,650
And then for page in range, and I know there's 10 pages will say the page, you are all.

157
00:08:53,730 --> 00:08:59,460
Is equal to your URL, plus the string version of that particular page.

158
00:08:59,550 --> 00:09:01,440
I can't just add an integer to a string.

159
00:09:01,530 --> 00:09:04,920
I have to convert it first to a string in order to perform that concatenation.

160
00:09:05,670 --> 00:09:07,410
Then I'll make the request for that page.

161
00:09:08,370 --> 00:09:09,450
So we'll say requests.

162
00:09:10,790 --> 00:09:12,760
Get the information off that page, Yoro.

163
00:09:14,390 --> 00:09:17,480
And then we'll say soup is equal to B.S. for.

164
00:09:19,080 --> 00:09:19,950
Beautiful soup.

165
00:09:22,470 --> 00:09:23,250
Rest text.

166
00:09:24,410 --> 00:09:30,030
Alex, I'm l just as we have before, and then I'm actually just going to copy and paste what we had

167
00:09:30,090 --> 00:09:32,260
up here for Name and Soup.

168
00:09:32,310 --> 00:09:38,490
That's like author authors, the ad name text, because we're doing the exact same thing, except now

169
00:09:38,490 --> 00:09:40,890
we're just doing it for ten pages.

170
00:09:41,100 --> 00:09:42,570
So this was just for the home page.

171
00:09:42,900 --> 00:09:45,150
We're gonna set this all up for all ten pages.

172
00:09:45,660 --> 00:09:46,800
We'll go ahead and run this.

173
00:09:48,670 --> 00:09:52,570
And then once it's done running, we'll check on what authors looks like.

174
00:09:54,100 --> 00:09:57,670
And keep in mind, depending on your Internet connection, that may take a little bit of time to go

175
00:09:57,670 --> 00:09:58,570
through 10 pages.

176
00:09:58,960 --> 00:10:01,210
But notice here now I have all the authors.

177
00:10:01,840 --> 00:10:04,160
So I'm Alan Monroe, Mother Teresa, etc..

178
00:10:04,630 --> 00:10:06,760
So these are all the authors on all 10 pages.

179
00:10:07,120 --> 00:10:11,770
This is kind of the easy solution because this assumes that, you know how many pages there are.

180
00:10:12,190 --> 00:10:16,990
Realistically, however, you're probably not going to know ahead of time how many pages there are.

181
00:10:17,170 --> 00:10:23,770
So you have to figure out is what happens once you reach a page that doesn't have any quotes on it.

182
00:10:24,260 --> 00:10:26,860
And an easy way to do that is to come back to the Web site.

183
00:10:27,430 --> 00:10:32,920
And just if you keep getting next, request a page that you know is outside the range.

184
00:10:32,950 --> 00:10:38,800
So just, for instance, fill in a bunch of nines here and you know, there's not going to be nine billion

185
00:10:38,800 --> 00:10:39,490
quotes here.

186
00:10:40,120 --> 00:10:45,940
And you can see that there's no quotes found when it hits a page that is outside its range.

187
00:10:46,420 --> 00:10:48,220
So that's a very interesting fact to know.

188
00:10:48,700 --> 00:10:55,150
Well, we could do is just keep looping through pages over and over again until we hit something that

189
00:10:55,150 --> 00:10:57,520
says no quotes found in the text.

190
00:10:59,270 --> 00:11:04,370
So what I mean by that is this go ahead and make a couple new cells to work with here.

191
00:11:05,360 --> 00:11:06,380
I'm going to say.

192
00:11:07,930 --> 00:11:12,400
My page, your URL is equal to that, your URL plus string version.

193
00:11:13,120 --> 00:11:15,280
And let me just put in just a bunch of nines there.

194
00:11:16,790 --> 00:11:20,970
So I know this page shouldn't have any quotes on it to make a request on it.

195
00:11:23,740 --> 00:11:24,730
OSA requests.

196
00:11:30,050 --> 00:11:30,980
The page you URL.

197
00:11:34,050 --> 00:11:35,860
Let's convert it into a soup.

198
00:11:37,550 --> 00:11:38,480
B.S. for.

199
00:11:39,890 --> 00:11:40,820
Beautiful soup.

200
00:11:42,350 --> 00:11:46,300
And then we'll pass in rest text and Alex M.L..

201
00:11:48,610 --> 00:11:51,870
And now if we check out that soup, this is for a page you are.

202
00:11:52,350 --> 00:11:54,250
That shouldn't have any sort of quote.

203
00:11:54,510 --> 00:11:56,730
So there's not nine billion quotes or whatever.

204
00:11:57,120 --> 00:11:58,080
You scroll down here.

205
00:11:58,260 --> 00:12:01,170
And notice we have no quotes found.

206
00:12:01,800 --> 00:12:04,920
So what I can do is simply the following.

207
00:12:05,520 --> 00:12:07,350
I know that result.

208
00:12:07,390 --> 00:12:10,460
That text is essentially a giant strain.

209
00:12:11,130 --> 00:12:18,690
And so if there's no quotes on that particular page, I can simply do a string check of.

210
00:12:19,750 --> 00:12:26,470
No quotes found exclamation point, which is the message shown once there's no more quotes left.

211
00:12:27,370 --> 00:12:29,530
So if I run this, I get back.

212
00:12:29,550 --> 00:12:29,880
True.

213
00:12:30,430 --> 00:12:33,250
And now I have a boolean condition I can check for.

214
00:12:33,580 --> 00:12:35,320
To keep running my loop.

215
00:12:35,950 --> 00:12:37,900
So let's go ahead and set up a loop.

216
00:12:38,410 --> 00:12:39,220
We're gonna do the following.

217
00:12:39,250 --> 00:12:41,100
We'll save page.

218
00:12:41,110 --> 00:12:41,920
Still valid.

219
00:12:42,730 --> 00:12:43,690
Is equal to true.

220
00:12:44,260 --> 00:12:48,460
And we'll change this to false depending on the result of no quotes found.

221
00:12:50,470 --> 00:12:52,750
We'll say Authors' is equal to an empty set.

222
00:12:53,130 --> 00:12:56,860
So reset it and we'll start on page equals one.

223
00:12:58,240 --> 00:13:05,190
And we'll say while Paige still valid, we're gonna keep doing what we did before.

224
00:13:06,570 --> 00:13:13,140
So we will reset the page, your URL is equal to your URL plus string.

225
00:13:14,430 --> 00:13:15,230
Of page.

226
00:13:16,140 --> 00:13:19,020
So this is going to concatenate that new page.

227
00:13:20,240 --> 00:13:26,600
And then what I'm going to do is at the end of this, while you say page is equal to page plus one.

228
00:13:27,170 --> 00:13:30,210
So it's important to keep basically going up that page counter.

229
00:13:30,260 --> 00:13:33,980
So we start off at one get that you are all going to do some more code here.

230
00:13:34,310 --> 00:13:38,570
The next page is to do some code again, three, etc..

231
00:13:39,260 --> 00:13:41,300
Next, we'll go ahead and obtain the requests.

232
00:13:42,260 --> 00:13:45,770
And so we'll say requests don't get.

233
00:13:46,940 --> 00:13:47,700
Page your URL.

234
00:13:48,650 --> 00:13:51,870
Now, you may think the next step is to start turning this into a soup.

235
00:13:52,160 --> 00:13:53,570
And checking if the authors are there.

236
00:13:54,020 --> 00:13:59,840
But keep in mind, we have to check if this is a valid page because we don't know yet if we hit.

237
00:13:59,990 --> 00:14:00,980
No quotes found.

238
00:14:01,610 --> 00:14:03,680
So I'll have a simple if statement here.

239
00:14:04,130 --> 00:14:07,510
I'll say if no quotes found.

240
00:14:10,260 --> 00:14:11,550
In Rezko text.

241
00:14:12,810 --> 00:14:15,150
Go ahead and just break out of this.

242
00:14:15,450 --> 00:14:19,530
Or I could say page Dobelle is equal to false, depending on how you actually want to break out of this

243
00:14:19,530 --> 00:14:19,950
while loop.

244
00:14:20,340 --> 00:14:25,500
But essentially what I'm saying here is we're gonna reach a page number that's high enough where the

245
00:14:25,740 --> 00:14:29,080
string no quotes found will be in the resulting texts.

246
00:14:29,190 --> 00:14:32,160
And if that happens, break out of this while because we're done.

247
00:14:32,730 --> 00:14:34,980
If that does not happen, then we'll just continue.

248
00:14:35,520 --> 00:14:39,240
We'll say soupe is equal to B.S. for.

249
00:14:40,290 --> 00:14:42,840
Beautiful soup, just as we did before.

250
00:14:42,900 --> 00:14:48,420
Text Alex MRL and then we'll run the exact same code as before.

251
00:14:48,630 --> 00:14:54,420
For the authors, which was this little snippet of four name and super select author authors don't add.

252
00:14:55,140 --> 00:14:57,660
So we go ahead and paste that here.

253
00:14:58,020 --> 00:15:00,360
And then once that's done, we go to the next page.

254
00:15:01,020 --> 00:15:05,090
Keep in mind lots different ways you could have done this, but this is just one valid way.

255
00:15:05,640 --> 00:15:12,270
So I'm going to run this and make sure that when I call authors, it matches the authors I had here

256
00:15:12,630 --> 00:15:13,980
on the manual for a loop.

257
00:15:14,670 --> 00:15:17,530
So we'll come here, run authors again.

258
00:15:18,000 --> 00:15:19,860
And recall, we reset authors.

259
00:15:20,070 --> 00:15:24,110
So the fact that it has all the authors here means that this loop worked, OK?

260
00:15:24,960 --> 00:15:26,550
So that's it for the exercises.

261
00:15:26,820 --> 00:15:32,580
Hopefully you're able to figure out how to code web scraping applications so that they follow along

262
00:15:32,580 --> 00:15:38,670
more like this second part where they're more robust to unknown conditions of a Web site versus the

263
00:15:38,670 --> 00:15:42,990
first one, which basically requires you to figure out how many pages they're worth to begin with.

264
00:15:43,350 --> 00:15:46,740
Which, for a very large Web site, may be difficult to impossible.

265
00:15:47,130 --> 00:15:49,910
So instead, see if you can always figure out a work around.

266
00:15:50,430 --> 00:15:56,010
Because when it comes to web scraping, the more robust the better, especially since sites can change

267
00:15:56,010 --> 00:15:56,670
quite often.

268
00:15:57,390 --> 00:15:58,740
OK, thanks.

269
00:15:58,960 --> 00:16:00,090
And I'll see you at the next one.
