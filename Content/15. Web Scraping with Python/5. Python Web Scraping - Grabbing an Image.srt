1
00:00:05,360 --> 00:00:06,710
Welcome, everyone, to this lecture.

2
00:00:06,800 --> 00:00:09,170
We're going to show you how to grab an image from a Web site.

3
00:00:09,560 --> 00:00:10,700
Again, using Python.

4
00:00:11,970 --> 00:00:17,010
So now that we understand how to grab text information based on tags and element names, I want to explore

5
00:00:17,010 --> 00:00:18,750
how to grab images from a Web site.

6
00:00:19,410 --> 00:00:23,720
Images on a Web site typically have their own your own link ending in something like that, J.

7
00:00:23,730 --> 00:00:25,110
Peg or Dot PMG.

8
00:00:26,450 --> 00:00:32,420
Beautiful soup can then scan a page and locate the specialized HD timal image tags that essentially

9
00:00:32,420 --> 00:00:35,180
the notes where that you are all is being linked to.

10
00:00:35,570 --> 00:00:40,580
And then beautiful soup can grab this list of your URLs, essentially grabbing all the images on a Web

11
00:00:40,580 --> 00:00:41,030
page.

12
00:00:41,480 --> 00:00:45,320
Then we can download those your URLs as images and write them to the computer.

13
00:00:46,040 --> 00:00:51,020
Keep in mind that you should know that always check copyright permission before downloading and using

14
00:00:51,020 --> 00:00:52,160
an image from a Web site.

15
00:00:52,640 --> 00:00:56,660
We're going to go ahead and use images from Wikipedia, which are open source.

16
00:00:56,990 --> 00:01:01,220
So let's go ahead and open up our notebook and check out a Wikipedia article.

17
00:01:01,620 --> 00:01:05,810
OK, here I am back at the notebook we're using previously for this example.

18
00:01:05,900 --> 00:01:11,900
I'm going to be using the article on Wikipedia for Deep Blue, which is a very famous chess playing

19
00:01:11,900 --> 00:01:12,500
computer.

20
00:01:13,130 --> 00:01:18,500
And we're going to do it, see if we can grab the images off of this particular Web page.

21
00:01:18,530 --> 00:01:22,070
So notice that there's actually two images here that are in the article.

22
00:01:22,460 --> 00:01:24,920
One, is this a picture of deep blue?

23
00:01:25,700 --> 00:01:29,890
And then one is of Kasparov playing in nineteen eighty five.

24
00:01:30,080 --> 00:01:34,250
And those look to be the main images on this particular page.

25
00:01:34,520 --> 00:01:38,180
So first we need to do is make a request to this page.

26
00:01:38,540 --> 00:01:39,950
Turn it into a soup.

27
00:01:40,040 --> 00:01:46,220
And then using our skills with the Chrome browser, inspect these actual images and see what we should

28
00:01:46,220 --> 00:01:49,670
be looking for as far as either a class name or an element tag.

29
00:01:50,090 --> 00:01:55,160
And keep in mind, there's usually more than one way to web scrape the same information off a page.

30
00:01:55,670 --> 00:02:00,320
The last thing I want to note here before we get started is there is actually a Wikipedia article that

31
00:02:00,320 --> 00:02:04,130
describes different things about images on Wikipedia.

32
00:02:04,580 --> 00:02:08,720
And most images used on Wikipedia are actually part of Wikimedia Commons.

33
00:02:09,170 --> 00:02:14,600
And keep in mind that even if you do see a picture on Wikipedia, there is a chance that it could be

34
00:02:14,600 --> 00:02:16,070
copyrighted in some way.

35
00:02:16,550 --> 00:02:17,900
So you should always be careful of that.

36
00:02:18,710 --> 00:02:22,760
And you should keep mind that basically here we're just going to download these images and I don't plan

37
00:02:22,760 --> 00:02:24,380
on reselling these images in any way.

38
00:02:24,710 --> 00:02:26,930
So it should be OK from a copyright standpoint.

39
00:02:27,360 --> 00:02:31,340
But keep in mind, if you're gonna be doing something like Delany's images and then selling them as

40
00:02:31,340 --> 00:02:35,870
your own, you should be extremely careful because you may be violating some sort of copyright so you

41
00:02:35,870 --> 00:02:39,170
can go ahead and just Google search 10 things you may not know about images on Wikipedia.

42
00:02:39,500 --> 00:02:43,310
And it describes a lot of things that are important to understand about images on the Web site.

43
00:02:43,750 --> 00:02:43,960
OK.

44
00:02:44,510 --> 00:02:45,680
So we're going to do here.

45
00:02:45,800 --> 00:02:48,290
It's going to come back to this Jupiter notebook.

46
00:02:48,710 --> 00:02:50,420
And let's make a fresh request.

47
00:02:50,600 --> 00:02:52,460
We'll say Arias is equal to.

48
00:02:53,760 --> 00:02:55,710
Requests don't get.

49
00:02:56,490 --> 00:03:01,680
And then I'm going to pass in the you are all for this deep blue chest computer.

50
00:03:02,010 --> 00:03:04,830
You can just copy and paste from the election book if you want.

51
00:03:05,880 --> 00:03:12,930
And then let's go ahead and make a soup out of it, say soup equal to B.S. for that beautiful soup.

52
00:03:14,690 --> 00:03:19,280
Passing the resulting text there and then use Alex Amelle as the library.

53
00:03:20,780 --> 00:03:21,890
And then if you check out Sue.

54
00:03:22,900 --> 00:03:25,090
There is r h html document.

55
00:03:25,630 --> 00:03:31,840
OK, so what we need to do is figure out what we're actually going to use for this soup that select.

56
00:03:32,560 --> 00:03:34,690
And typically, there's more than one option.

57
00:03:35,230 --> 00:03:38,290
If we take a look at these images, what I'm going to do is right.

58
00:03:38,290 --> 00:03:40,150
Click and inspect them.

59
00:03:40,990 --> 00:03:43,300
And I can see there's a bunch of different information here.

60
00:03:43,750 --> 00:03:50,590
One is that I notice there's an I M.G. image tag and notice here that it's color differently than other

61
00:03:50,590 --> 00:03:54,210
things like classes and I.D. because it's an H.

62
00:03:54,220 --> 00:03:58,560
Timal element, just like a div is or just like paragraph is with P.

63
00:03:58,720 --> 00:04:01,380
So what it could do is try to just grab everything with I M.G..

64
00:04:02,350 --> 00:04:03,220
So I come back here.

65
00:04:03,430 --> 00:04:09,940
I could just say I M.G. and I just passant I am G because it's an H Timal element tag, it's not a particular

66
00:04:09,940 --> 00:04:13,420
class or I.D. so I don't need to worry about hashtags or periods.

67
00:04:14,060 --> 00:04:14,250
Okay.

68
00:04:14,320 --> 00:04:21,160
So you can see here I get back this list of everything that has this image tag associated with it.

69
00:04:21,670 --> 00:04:25,420
Now keep in mind, this could be more than the actual images within the article.

70
00:04:25,900 --> 00:04:28,720
So you'll notice that within this information.

71
00:04:28,750 --> 00:04:32,590
Let's go ahead and grab the first one so we can explore it easily.

72
00:04:33,430 --> 00:04:38,440
There's this ult, which basically means if the image can't be found, go ahead and show an alternative.

73
00:04:38,950 --> 00:04:42,340
The main thing we're looking for when we're dealing with images is this right here.

74
00:04:42,760 --> 00:04:47,740
S RC C, which essentially tells this tag what is the source of the image?

75
00:04:48,130 --> 00:04:51,130
So each image on a Web site is going to have its unique.

76
00:04:51,130 --> 00:04:56,620
You are all they can kind of tell because they'll typically end in something like PMG or JPEG or some

77
00:04:56,620 --> 00:04:57,550
sort of image file.

78
00:04:58,060 --> 00:05:00,810
So here I can see this symbol, support vote, et cetera.

79
00:05:00,990 --> 00:05:06,160
So what I'm going to do is I can simply copy the string here.

80
00:05:06,280 --> 00:05:12,220
That's source, not with the quotes, just everything in between those quotes and a copy this and I'm

81
00:05:12,220 --> 00:05:15,490
going to paste it into my browser.

82
00:05:16,660 --> 00:05:18,890
And when I paste into my browser, it kind of looks empty.

83
00:05:18,920 --> 00:05:22,310
But if you'll notice at the very center, there's this green plus sign.

84
00:05:22,850 --> 00:05:29,150
And that's because on the actual Wikipedia article, there's more images than just this image of Deep

85
00:05:29,150 --> 00:05:32,240
Blue and of Kasparov playing chess.

86
00:05:32,510 --> 00:05:36,200
In fact, there's an image here for different articles on chess programming.

87
00:05:36,500 --> 00:05:38,630
There's an image here for the Wikipedia logo.

88
00:05:38,660 --> 00:05:40,400
There's the little immature for this person.

89
00:05:40,730 --> 00:05:42,350
And there's an image here for this plus sign.

90
00:05:42,380 --> 00:05:46,700
So there actually could be images all over this page that are outside of the article.

91
00:05:46,760 --> 00:05:48,410
There's a little settings tool here.

92
00:05:48,740 --> 00:05:49,910
There's a little star cetera.

93
00:05:50,900 --> 00:05:55,130
So just choosing all images may actually not be specific enough.

94
00:05:55,340 --> 00:05:56,360
It's definitely a good start.

95
00:05:56,630 --> 00:06:02,420
If you're having trouble finding the particular classes or I.D. calls on a page, because for sure,

96
00:06:02,750 --> 00:06:06,170
these two images are going to be with an image tags.

97
00:06:06,530 --> 00:06:09,800
But let's imagine I just want images that are within the article.

98
00:06:09,980 --> 00:06:11,750
I don't want every image on the page.

99
00:06:12,170 --> 00:06:17,090
So off to be a little more careful in my inspection and I will simply inspect.

100
00:06:18,150 --> 00:06:24,630
The image that's within the article and see if I can hone in on maybe a class or I.D., and if you start

101
00:06:24,630 --> 00:06:28,560
looking closely, you'll notice that within this image there is actually a class.

102
00:06:28,800 --> 00:06:31,170
It says class is Thum image.

103
00:06:31,710 --> 00:06:36,690
So what I'm going to do is see if I can just select everything with a thumb image and if that's the

104
00:06:36,750 --> 00:06:42,600
right class call for images that are within the article and part of the article, not just an image

105
00:06:42,600 --> 00:06:43,290
on the page.

106
00:06:44,040 --> 00:06:51,760
So instead of selecting all images, I'm going to try to select that class call of dot thumb image.

107
00:06:52,170 --> 00:06:55,530
And again, I'm using this dot here because it's a class.

108
00:06:56,670 --> 00:07:02,550
I run this and it looks like I have two links, which is already looking good for us because I know

109
00:07:02,580 --> 00:07:05,730
there was really only two images within the article itself.

110
00:07:06,330 --> 00:07:11,350
So let's confirm this by just grabbing what's under source, as I see.

111
00:07:11,850 --> 00:07:16,590
Going to copy this and let's paste it into our browser.

112
00:07:17,520 --> 00:07:20,430
And we can see here that it worked for the first one.

113
00:07:20,790 --> 00:07:22,110
And let's confirm the second one.

114
00:07:22,680 --> 00:07:24,330
So we're gonna get around the second source here.

115
00:07:27,070 --> 00:07:27,760
Copy this.

116
00:07:29,740 --> 00:07:30,290
Paste in.

117
00:07:31,330 --> 00:07:34,030
And it looks like that's also the correct corresponding image.

118
00:07:34,660 --> 00:07:40,810
So it looks like the right call here was to be specific enough to a class instead of just grabbing everything

119
00:07:40,810 --> 00:07:41,740
with an image tag.

120
00:07:42,280 --> 00:07:46,600
This is something you'll have to experiment with, depending on what website you're working with as

121
00:07:46,600 --> 00:07:47,250
well as we are.

122
00:07:47,260 --> 00:07:48,120
Final goals are.

123
00:07:48,540 --> 00:07:53,440
But let's imagine my goals were just to grab those two images that were within the article.

124
00:07:53,950 --> 00:07:54,430
Perfect.

125
00:07:54,520 --> 00:08:01,360
So now that I have that one, I'm going to do is check out the image information and see if I can download

126
00:08:01,360 --> 00:08:01,480
it.

127
00:08:01,960 --> 00:08:06,970
So let's go ahead and grab the image of the computer, which was the first one.

128
00:08:07,000 --> 00:08:11,440
So I'll say computer is equal to super select.

129
00:08:12,590 --> 00:08:18,230
When I say some image and let's just grab that first image.

130
00:08:20,150 --> 00:08:21,260
So there's the computer.

131
00:08:24,430 --> 00:08:26,020
And we have this information here.

132
00:08:26,290 --> 00:08:32,170
And what I want to do is actually just want to grab a particular portion of this notice you have something

133
00:08:32,170 --> 00:08:38,110
like class equals data file with equals, source set equals source height equals.

134
00:08:38,470 --> 00:08:42,970
If you're looking for just a particular aspect or parameter call here we can do is almost treat this

135
00:08:43,030 --> 00:08:46,510
as a dictionary because recall computer is not a string.

136
00:08:47,170 --> 00:08:49,000
It's a specialized tag object.

137
00:08:49,650 --> 00:08:56,080
And because it's a tag object, it means you can actually do a call like a dictionary for any particular

138
00:08:56,080 --> 00:08:57,280
part of this tag.

139
00:08:57,460 --> 00:09:01,030
So if I said class, it would return back Thum image.

140
00:09:01,630 --> 00:09:07,240
And that means if I said source, which is what I'm actually interested in, it returns back this string.

141
00:09:07,780 --> 00:09:10,570
So this string is what I actually want to download.

142
00:09:11,110 --> 00:09:17,920
And one real quick way we could just link this to a notebook is if I copy this string, select the cell

143
00:09:18,400 --> 00:09:21,700
and say cell cell type.

144
00:09:23,680 --> 00:09:24,180
Markdown.

145
00:09:24,850 --> 00:09:30,880
What I could do is actually paste this within my own image tag because markdown actually supports these

146
00:09:30,880 --> 00:09:31,510
image hacks.

147
00:09:31,930 --> 00:09:39,330
So I could say I M.G. source equal to and then pacien as a string.

148
00:09:40,030 --> 00:09:40,720
That source.

149
00:09:44,950 --> 00:09:48,310
So here what I'm saying is image, source equal to.

150
00:09:48,730 --> 00:09:52,120
And then this giant string and then one I'm going to do is close it off.

151
00:09:53,020 --> 00:09:53,590
Hit enter.

152
00:09:54,370 --> 00:10:00,190
What this mark down cell actually does is it grabs that euro and then displays it inside the notebook.

153
00:10:00,700 --> 00:10:04,870
So if that's something you want to do within your own notebooks, it's kind of a fun little trick.

154
00:10:05,020 --> 00:10:09,310
And again, the tricks for that were to take cell cell type.

155
00:10:09,520 --> 00:10:11,140
Turn it into markdown.

156
00:10:11,770 --> 00:10:16,660
And this essentially supports just basic H came out where we have this image tag source and then the

157
00:10:16,690 --> 00:10:17,160
JPEG.

158
00:10:17,800 --> 00:10:22,870
Keep in mind, though, if this link ever changes, then the notebook will disappear or essentially

159
00:10:22,930 --> 00:10:23,800
make something broken.

160
00:10:23,860 --> 00:10:26,830
So let's say they change it or you accidently spell this wrong.

161
00:10:27,040 --> 00:10:28,640
I was gonna say Deep J.

162
00:10:28,650 --> 00:10:29,920
Peg, run this.

163
00:10:29,980 --> 00:10:31,420
You'll get this little broken image sign.

164
00:10:32,000 --> 00:10:34,150
OK, so I'm gonna go ahead and delete that cell.

165
00:10:35,920 --> 00:10:39,850
Now, because we don't want to be dependent on this Web site, let's imagine I want to download this

166
00:10:40,150 --> 00:10:42,400
maybe for a school presentation or something like that.

167
00:10:42,850 --> 00:10:47,160
What I can do is make a new request specifically on this, Yoro.

168
00:10:47,830 --> 00:10:49,060
So let's go ahead and do that.

169
00:10:49,290 --> 00:10:50,530
What I'm going to say is.

170
00:10:51,810 --> 00:10:54,090
Image link is equal to.

171
00:10:56,360 --> 00:10:57,980
Requests don't get.

172
00:10:59,350 --> 00:11:01,570
And now I'm just going to pass in.

173
00:11:02,620 --> 00:11:03,760
This link itself.

174
00:11:04,710 --> 00:11:05,850
So copy and paste that.

175
00:11:07,360 --> 00:11:15,670
And what I need to do is in front of this ad, htp, yes, so say H.T., CPS, Colon and then the phone

176
00:11:15,670 --> 00:11:16,030
link.

177
00:11:16,450 --> 00:11:22,840
So essentially I'm making a request on this specific link, which is the link if we put this in our

178
00:11:22,840 --> 00:11:24,820
your URL for the computer.

179
00:11:24,940 --> 00:11:26,410
So if I were a copy this here.

180
00:11:28,080 --> 00:11:28,670
And paste it.

181
00:11:29,920 --> 00:11:34,690
I can see that this is what that code is going to make a request for, just this particular image.

182
00:11:35,890 --> 00:11:37,030
So we go ahead and run this.

183
00:11:38,960 --> 00:11:42,020
And now I can say image link.

184
00:11:43,730 --> 00:11:45,760
And this is going to have a content attribute.

185
00:11:46,850 --> 00:11:51,410
And what this is, it's the raw content of the actual image.

186
00:11:51,500 --> 00:11:52,970
So this is a binary file.

187
00:11:53,480 --> 00:11:57,050
And this is the computers way of representing internally.

188
00:11:57,350 --> 00:12:00,920
What this image actually looks like is actually kind of not readable by human.

189
00:12:01,220 --> 00:12:02,390
There's a binary file.

190
00:12:02,750 --> 00:12:08,000
However, Python is smart enough to be able to read and write this file and save the image onto your

191
00:12:08,000 --> 00:12:08,510
computer.

192
00:12:09,140 --> 00:12:14,150
And we do this by opening a new file and then writing to it and then closing to it.

193
00:12:15,080 --> 00:12:19,490
So recall that this image linked to content has the information I need.

194
00:12:19,850 --> 00:12:23,450
And I want to save this onto my computer in the form of an image.

195
00:12:23,990 --> 00:12:28,580
And the way we do that is I say something like F is equal to open.

196
00:12:29,210 --> 00:12:34,940
Go ahead and choose your file name so I can say my computer image.

197
00:12:36,140 --> 00:12:36,950
That Jay Pegg.

198
00:12:37,370 --> 00:12:41,960
Typically, you're gonna want to make sure that this ending matches the ending of your YORO, which

199
00:12:41,960 --> 00:12:44,000
was J-P and then.

200
00:12:45,030 --> 00:12:49,650
For the writing or reading permission to recall the actual mode we're gonna be doing.

201
00:12:50,190 --> 00:12:55,280
It's going to be W B, which means write binary.

202
00:12:55,770 --> 00:13:01,560
So the notes binary writing, because that's not a typical string or text file.

203
00:13:01,890 --> 00:13:05,100
It's actually a binary representation of that image.

204
00:13:05,580 --> 00:13:11,520
So we're going to open up this new JPEG image, which I just created through the open come in.

205
00:13:11,910 --> 00:13:17,760
So recall, this essentially makes this JPEG file for us and then we're going to do is say F but.

206
00:13:17,760 --> 00:13:18,180
Right.

207
00:13:20,150 --> 00:13:20,780
Image.

208
00:13:21,930 --> 00:13:24,390
Link content.

209
00:13:25,470 --> 00:13:29,100
So now I went ahead and wrote that to this Chapei that we just created.

210
00:13:29,580 --> 00:13:30,690
And then I'm going to close.

211
00:13:33,550 --> 00:13:34,030
There we go.

212
00:13:34,450 --> 00:13:38,320
And so now you should be able to see locally this JPEG file.

213
00:13:38,770 --> 00:13:40,240
So if I hit a file open.

214
00:13:41,760 --> 00:13:42,570
Scroll down here.

215
00:13:42,600 --> 00:13:45,630
I was working with this untitled notebook.

216
00:13:45,930 --> 00:13:48,580
Notice now I have this my computer image, that JPEG.

217
00:13:49,050 --> 00:13:52,230
And this is going to save wherever your notebook happens to be located.

218
00:13:52,590 --> 00:13:56,850
If you want to save this somewhere else, then simply provide the full file path of where you want to

219
00:13:56,850 --> 00:13:57,240
save it.

220
00:13:57,630 --> 00:14:03,030
So you could do something like see users, forward, slash, etc..

221
00:14:03,070 --> 00:14:07,530
So depending on what computer and os, you can save this technically anywhere in your computer.

222
00:14:07,650 --> 00:14:10,590
If you just say J Peg, you'll save it to the exact same location.

223
00:14:10,830 --> 00:14:13,810
Your notebook is in which you can always confirm with PWI.

224
00:14:14,730 --> 00:14:16,350
OK, so I may delete that.

225
00:14:16,860 --> 00:14:18,570
So we have this my computer image.

226
00:14:18,630 --> 00:14:26,640
If I click this open notice, it downloaded the actual image and now we have it written on our computer.

227
00:14:27,480 --> 00:14:27,750
OK.

228
00:14:28,320 --> 00:14:31,350
So that's an example of downloading images.

229
00:14:31,380 --> 00:14:34,950
So let me just do a quick overview of everything we did here, because it was quite a bit.

230
00:14:35,610 --> 00:14:38,820
We went ahead and made a request on a Web site.

231
00:14:39,300 --> 00:14:40,380
Turned that into a soup.

232
00:14:40,890 --> 00:14:44,910
And then we had to select a particular tag or class call.

233
00:14:45,240 --> 00:14:48,330
If you just do image tags, they'll grab all the images on the page.

234
00:14:48,690 --> 00:14:50,370
If you're looking for something more specific.

235
00:14:50,550 --> 00:14:56,280
See if you can find a class call or an I.D. call to further specify your results.

236
00:14:56,820 --> 00:15:02,010
After that, we can go ahead and confirm the source by just grabbing this and pasting it into our browser.

237
00:15:02,040 --> 00:15:06,240
Confirm that's the image we want once we know the actual your URL for the image we want.

238
00:15:06,780 --> 00:15:09,450
We go ahead and make a request on that image.

239
00:15:10,260 --> 00:15:10,780
You URL.

240
00:15:11,340 --> 00:15:12,300
Then we can't open it.

241
00:15:12,810 --> 00:15:14,010
Write it and close it.

242
00:15:14,490 --> 00:15:17,400
And the thing to keep in mind by opening it is that it should be right.

243
00:15:17,430 --> 00:15:19,560
Binary or WB mode.

244
00:15:20,490 --> 00:15:20,730
All right.

245
00:15:21,240 --> 00:15:25,620
Up next, we're going to show you an example project which is working with multiple pages and items.

246
00:15:25,650 --> 00:15:27,660
So a little bit more of a realistic task.

247
00:15:28,230 --> 00:15:28,610
Thanks.

248
00:15:28,770 --> 00:15:29,730
And I'll see at the next lecture.
