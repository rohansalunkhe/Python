1
00:00:05,260 --> 00:00:07,630
Welcome, everyone, to this section on Web scraping.

2
00:00:09,110 --> 00:00:14,510
Web scraping is a general term for techniques involving automating the gathering of data from a Web

3
00:00:14,510 --> 00:00:14,810
site.

4
00:00:15,470 --> 00:00:20,330
As you can imagine, there's often Web sites that have information that you want to use for some other

5
00:00:20,330 --> 00:00:20,930
project.

6
00:00:21,290 --> 00:00:26,690
However, actually manually going in and copying and pasting the information yourself would just take

7
00:00:26,690 --> 00:00:28,730
too long to be realistically possible.

8
00:00:29,210 --> 00:00:33,890
So in this section, we're going to learn how to use Python and a couple of Python libraries to conduct

9
00:00:33,890 --> 00:00:38,700
web scraping tasks such as downloading images or information off of a Web site.

10
00:00:40,560 --> 00:00:45,270
In order to web script of Python, where we first need to understand are the basic concepts of how a

11
00:00:45,270 --> 00:00:46,290
Web site works.

12
00:00:46,890 --> 00:00:52,560
So if you've already taken a course on just basic Web development, you may actually already know a

13
00:00:52,560 --> 00:00:56,520
lot of the information we're going to show here in order to web script of Python.

14
00:00:56,670 --> 00:01:02,460
What we really need to understand are how the front end of a Web site delivers information to your browser.

15
00:01:02,820 --> 00:01:05,280
And that includes basically three major topics.

16
00:01:05,610 --> 00:01:08,310
H.T., Amelle, CSX and JavaScript.

17
00:01:08,730 --> 00:01:14,790
Now, you don't need to technically know each time LCROSS and JavaScript in depth in order to actually

18
00:01:14,790 --> 00:01:15,650
scrape a website.

19
00:01:16,140 --> 00:01:21,420
You just need to know enough to be able to understand what your browser is showing you.

20
00:01:21,840 --> 00:01:28,440
So let's first, just very briefly have an overview of how a Web site connects to the Internet and then

21
00:01:28,530 --> 00:01:30,570
how your browser displays information to you.

22
00:01:31,170 --> 00:01:35,330
So when a browser loads a Web site, the user gets to see what is known as the front end of the Web

23
00:01:35,330 --> 00:01:35,610
site.

24
00:01:36,000 --> 00:01:39,540
Let's imagine that you wanted to read an article on Wikipedia.

25
00:01:40,170 --> 00:01:40,800
So what you do?

26
00:01:41,160 --> 00:01:42,240
You open up your computer.

27
00:01:42,750 --> 00:01:43,800
You connect to the Internet.

28
00:01:44,610 --> 00:01:47,160
You open up your browser and then your browser.

29
00:01:47,220 --> 00:01:49,950
You're going to pass into your URL to your Wikipedia article.

30
00:01:50,280 --> 00:01:55,680
And then Wikipedia returns back some information to your browser and your browser displays it in a format

31
00:01:55,770 --> 00:01:56,640
that you can read.

32
00:01:56,730 --> 00:01:58,410
So this is a human readable format.

33
00:01:58,860 --> 00:02:02,880
But really what Wikipedia is returning is code such as an H.

34
00:02:02,880 --> 00:02:07,050
HTML document, which stands for hypertext markup language.

35
00:02:07,500 --> 00:02:13,350
And this is special code that your browser is able to read and understand in order to display something

36
00:02:13,350 --> 00:02:15,120
that looks nice for a human to read.

37
00:02:16,520 --> 00:02:23,270
So what we're going to be doing instead is being able to read this HCM, L and C assess and JavaScript

38
00:02:23,270 --> 00:02:26,480
code that Wikipedia is sending back to us.

39
00:02:26,600 --> 00:02:31,190
And we're going to use a Python Web scraping program to directly grab what we want.

40
00:02:31,310 --> 00:02:35,210
Maybe we want to actually grab the Wikipedia logo off of this.

41
00:02:35,570 --> 00:02:37,100
So we would be able to search this H.

42
00:02:37,110 --> 00:02:43,640
T Amelle document and find where that logos images such as a JPEG or Panji file, and then use that

43
00:02:43,640 --> 00:02:44,870
for some other project.

44
00:02:46,640 --> 00:02:50,390
Or maybe you want to grab some information off this e-mail document.

45
00:02:50,720 --> 00:02:52,430
Python will be able to do that as well.

46
00:02:52,580 --> 00:02:55,580
So can grab images and information off this H t mail document.

47
00:02:57,410 --> 00:03:00,740
And convert it into another python object, such as a python list.

48
00:03:02,080 --> 00:03:06,820
OK, so there's a couple of things we need to understand in order to perform web scraping effectively.

49
00:03:07,510 --> 00:03:09,280
The first one is actually not technical.

50
00:03:09,310 --> 00:03:12,010
It's the rules of Web scraping that you should abide by.

51
00:03:12,670 --> 00:03:14,620
The second one also not really technical.

52
00:03:14,620 --> 00:03:16,300
It's the limitations of web scraping.

53
00:03:16,600 --> 00:03:20,230
So we want you to have realistic expectations coming into this section.

54
00:03:20,710 --> 00:03:22,690
And then the third one is more technical.

55
00:03:22,810 --> 00:03:26,560
It's just an understanding of basic HP, Amelle and CSX.

56
00:03:26,920 --> 00:03:31,690
So, again, you don't need to become an expert in each team LCROSS in order to perform web scraping.

57
00:03:32,020 --> 00:03:33,580
You just need to know enough about them.

58
00:03:33,610 --> 00:03:38,050
They are able to actually find what you're looking for in the H team L and C assess code.

59
00:03:38,720 --> 00:03:40,240
OK, so let's get started.

60
00:03:41,020 --> 00:03:42,550
First, the rules of web scraping.

61
00:03:43,000 --> 00:03:45,940
You should always try to get permission before scraping.

62
00:03:46,450 --> 00:03:51,940
If you make too many scraping attempts or requests, then it's possible that your IP address could get

63
00:03:51,940 --> 00:03:56,440
blocked, which means you won't even be able to visit that Web site in a normal browser.

64
00:03:57,160 --> 00:04:01,900
Now, if you're dealing with a Web site that gets millions of visitors, I feel like Wikipedia and you're

65
00:04:01,900 --> 00:04:06,550
just going to scrape it a few times to get some information, then really that's no problem because

66
00:04:06,550 --> 00:04:09,010
that Web site deals with much, much higher traffic.

67
00:04:09,400 --> 00:04:13,670
But keep in mind that some sites do automatically block scraping software.

68
00:04:14,110 --> 00:04:20,680
So you should always make sure and check the permissions or rules or guideline page for whatever particular

69
00:04:20,680 --> 00:04:23,380
Web site you're going to be attempting to scrape.

70
00:04:23,590 --> 00:04:26,200
So always trying to get permission before scraping.

71
00:04:26,710 --> 00:04:31,600
And you should also check the laws of whatever country you're operating in to see if it's legal to web

72
00:04:31,600 --> 00:04:31,990
scrape.

73
00:04:32,560 --> 00:04:33,940
Typically, it's OK.

74
00:04:34,000 --> 00:04:39,010
But again, you should always consult the Web site rules in order to make sure you're OK.

75
00:04:40,910 --> 00:04:46,560
The second thing I want to talk about are the limitations of web scraping and in general, every Web

76
00:04:46,560 --> 00:04:51,620
site is unique, which means, unfortunately, every Web scraping script is unique.

77
00:04:52,070 --> 00:04:58,700
So you can't really just take one Web scraping script in Python and just easily apply it to any other

78
00:04:58,700 --> 00:05:03,590
Web site in the world, since every Web site's HD mail code is going to be unique to that Web site.

79
00:05:04,010 --> 00:05:08,990
More than likely, you're going to have to adjust your script in order to fit other Web sites.

80
00:05:09,410 --> 00:05:15,140
And keep in mind that any slight change or update to a Web site may completely break your Web scraping

81
00:05:15,200 --> 00:05:15,770
script.

82
00:05:16,160 --> 00:05:21,830
So the scripts in general are pretty static and they're not gonna be able to adjust to changes in the

83
00:05:21,830 --> 00:05:22,340
Web site.

84
00:05:22,790 --> 00:05:26,210
So this is something that is a little annoying about Web scraping.

85
00:05:26,540 --> 00:05:31,430
And it's the fact that if you plan to have kind of a long term Web scraping project, then it's more

86
00:05:31,430 --> 00:05:35,060
than likely you're going to have to make adjustments to that script over time.

87
00:05:35,630 --> 00:05:42,140
But our goal in this section of the course is to be able to generalize the skill set of using Python

88
00:05:42,170 --> 00:05:47,180
to perform web scraping so you can take the general ideas and then apply them to Web sites instead of

89
00:05:47,180 --> 00:05:50,420
having to try to copy some cookie cutter code.

90
00:05:50,600 --> 00:05:56,180
So really, what we want to do is become experts in looking up information and generalizing the process

91
00:05:56,240 --> 00:05:59,270
of web scraping so you can apply it to unique situations.

92
00:06:01,040 --> 00:06:05,100
OK, now let's talk about the main front end components of a Web site.

93
00:06:05,540 --> 00:06:10,430
If you're already familiar of H, Team L and CSX and even a little bit of JavaScript, you should actually

94
00:06:10,430 --> 00:06:12,890
be OK for the rest of this and you can skip forward.

95
00:06:12,980 --> 00:06:16,970
But if Python is your first language, then you may not be familiar with these concepts.

96
00:06:17,150 --> 00:06:22,160
So here are the actual three main components of the front end of a Web site.

97
00:06:22,550 --> 00:06:27,440
Essentially, these are the three things that are being passed into your browser and then your browser

98
00:06:27,440 --> 00:06:30,260
uses this to show you something that's human readable.

99
00:06:30,680 --> 00:06:37,430
And Python is basically going to grab these raw documents or this raw code itself and then extract information

100
00:06:37,430 --> 00:06:38,210
that it wants from it.

101
00:06:40,150 --> 00:06:44,140
So when viewing the Web site, that browser is not going to show you all that source code.

102
00:06:44,350 --> 00:06:49,600
Instead, it shows you the HDL and some success in job script that the Web site sends to your browser

103
00:06:49,660 --> 00:06:51,580
and your browser processes that information.

104
00:06:53,580 --> 00:07:00,230
Each mail is used to create the basic structure and content of a Web page, so each T.M. actually contains

105
00:07:00,230 --> 00:07:05,720
the information that's shown on the Web site, such as the words written in a paragraph on the Web site.

106
00:07:06,320 --> 00:07:12,200
CSX, which stands for cascading style sheets, is used for the design and style of a Web page.

107
00:07:12,560 --> 00:07:18,740
So based off the female elements, they'll be connected to some sort of style, such as font size or

108
00:07:18,740 --> 00:07:20,670
color or positioning on the Web site.

109
00:07:21,200 --> 00:07:22,730
And that's what CSX does.

110
00:07:22,790 --> 00:07:23,210
So H.

111
00:07:23,210 --> 00:07:25,130
Theno main information on the Web site.

112
00:07:25,460 --> 00:07:28,860
CSX is going to be the design and style of that H.T. mill.

113
00:07:29,300 --> 00:07:34,880
And then finally, there may be JavaScript, which is used to define the interactive elements of a Web

114
00:07:34,880 --> 00:07:35,300
page.

115
00:07:35,720 --> 00:07:39,290
And often for our scraping jobs, we will actually have to worry that much about JavaScript.

116
00:07:39,710 --> 00:07:41,990
Really, it's the H.T. mail that we're looking for.

117
00:07:42,110 --> 00:07:44,360
And we can use CFS to help us find it.

118
00:07:44,690 --> 00:07:48,050
But let's go into a little more depth on each of these.

119
00:07:50,170 --> 00:07:54,640
So really, as I mentioned, for effective basic web scraping, we only need to have a basic understanding

120
00:07:54,730 --> 00:07:56,320
of each Timal NC assess.

121
00:07:56,670 --> 00:08:03,250
And what's going to happen is Python can view these HDL and CNS elements programmatically and then extract

122
00:08:03,310 --> 00:08:04,840
information from the Web site.

123
00:08:05,410 --> 00:08:06,770
So let's explore some example.

124
00:08:06,860 --> 00:08:09,250
Each L and C assess in a little more detail.

125
00:08:09,700 --> 00:08:11,980
And don't worry about fully understanding this right now.

126
00:08:12,180 --> 00:08:15,250
We're gonna see a lot more examples of this when we're Web scraping of Python.

127
00:08:15,280 --> 00:08:16,450
In subsequent lectures.

128
00:08:18,120 --> 00:08:24,080
So, as I mentioned, HST, HDL is hypertext markup language, and it's present on every single Web

129
00:08:24,080 --> 00:08:24,930
site on the Internet.

130
00:08:25,140 --> 00:08:28,800
That is essentially the base of a Web page.

131
00:08:29,070 --> 00:08:34,440
And what's interesting is you can right click on a Web site and select view page source in order to

132
00:08:34,440 --> 00:08:36,990
get an example of what each team out looks like.

133
00:08:37,620 --> 00:08:41,190
So we're going to be showing that actual view page source later on in next.

134
00:08:41,990 --> 00:08:44,160
But let's see a small example of some each team.

135
00:08:44,170 --> 00:08:48,150
I'll code and just highlight a couple of important components here.

136
00:08:48,150 --> 00:08:53,130
We have an example of what is very, very basic Web site would have for its H team L code.

137
00:08:53,600 --> 00:08:59,400
That very top line typically says something like doctype h t mail, which is essentially telling whatever

138
00:08:59,400 --> 00:09:00,660
program is reading this file.

139
00:09:00,720 --> 00:09:02,670
Hey, this is an h t e-mail file.

140
00:09:02,700 --> 00:09:04,130
So the document type is H team.

141
00:09:04,830 --> 00:09:07,770
And then what we have are various tags or elements.

142
00:09:08,760 --> 00:09:15,270
So we have this general H, team L tag and notice we have a starting tag and then a closing tag and

143
00:09:15,270 --> 00:09:16,070
the closing tags.

144
00:09:16,140 --> 00:09:18,600
Each team, Timal, are denoted with a forward slash.

145
00:09:18,990 --> 00:09:20,100
So you'll notice that pattern.

146
00:09:20,150 --> 00:09:23,070
A lot of an opening tag and then a closing tag.

147
00:09:24,610 --> 00:09:28,570
And then at the top of an H t Amelle file, we have the head portion.

148
00:09:29,080 --> 00:09:35,830
So the head portion basically contains certain high level information of the Web page itself, such

149
00:09:35,830 --> 00:09:41,650
as what the title on the browser tab will show or connections to things like in other CFS file.

150
00:09:42,550 --> 00:09:45,760
And then we have the body component of an H t Mel file.

151
00:09:46,120 --> 00:09:51,340
And this contains the vast majority of the information that the Web site or Web page is going to display.

152
00:09:51,490 --> 00:09:56,680
And we have things like headers, which is bolder tax and things like paragraphs, which is just a paragraph

153
00:09:56,680 --> 00:09:57,820
of text information.

154
00:09:58,300 --> 00:10:02,500
The main thing to understand here is that there's actual tags we can search for.

155
00:10:02,950 --> 00:10:07,960
So for some reason later on, programmatically, we wanted to grab the text, some paragraph from this

156
00:10:07,960 --> 00:10:08,450
document.

157
00:10:08,830 --> 00:10:13,750
I can notice that it's in between these P tags, which is actually stands for a paragraph tags.

158
00:10:14,080 --> 00:10:18,700
And I'll be able to use Python to look for things that are in between those paragraph tags.

159
00:10:19,120 --> 00:10:23,530
So main thing you need to know about each team l in regards to Python Web scraping is the fact that

160
00:10:23,530 --> 00:10:25,630
there's an opening tag and a closing tag.

161
00:10:25,930 --> 00:10:28,600
And we can look for information between those tags.

162
00:10:30,350 --> 00:10:33,380
CSX stands for cascading style sheets.

163
00:10:33,860 --> 00:10:36,190
CSX gives style to a Web site.

164
00:10:36,590 --> 00:10:41,250
And what I mean by style are things like changing the colors or fonts or general look of a Web site.

165
00:10:41,840 --> 00:10:46,280
CSX uses tags to define what H.T. male elements will be styled.

166
00:10:47,030 --> 00:10:47,300
All right.

167
00:10:47,330 --> 00:10:50,630
So here we see again a very simple H team file.

168
00:10:50,990 --> 00:10:58,240
However, we've added a connection to CSX and we filed a particular component or element from this H.

169
00:10:58,260 --> 00:10:58,860
Timal file.

170
00:10:59,570 --> 00:11:06,140
The first change your notice is that inside the head section, we've actually linked to a CSX file.

171
00:11:06,620 --> 00:11:11,480
So we have this link tag and we say, OK, this relationship is going to be stylesheet, essentially

172
00:11:11,480 --> 00:11:13,580
telling it that this is going to be a CSX file.

173
00:11:13,940 --> 00:11:14,570
And then H.

174
00:11:14,570 --> 00:11:21,200
RF is essentially the reference for the actual file, essentially telling this A.T.M. document where

175
00:11:21,200 --> 00:11:28,090
to find this CSX file, which has things like descriptions on font size, color position, et cetera.

176
00:11:28,850 --> 00:11:35,960
And now the way CSX works is once you link to the CSX file, you have things like I.D. and classes.

177
00:11:36,380 --> 00:11:38,060
There's actually more levels to this.

178
00:11:38,120 --> 00:11:38,900
And CSX.

179
00:11:39,190 --> 00:11:43,790
But for our purposes, we really just need to know about I.D. and classes when it comes to CSX.

180
00:11:44,240 --> 00:11:50,840
So notice here inside this paragraph tag that had some text in between the opening tag and closing tag

181
00:11:51,280 --> 00:11:52,190
in the opening tag.

182
00:11:52,310 --> 00:11:56,380
I now have this link to I.D. is equal to Perret.

183
00:11:56,380 --> 00:12:01,790
To compare it to is an I.D. call inside the CSX file.

184
00:12:02,390 --> 00:12:08,300
And so now that this paragraph tag has a connection to I.D. equal to Parratt two, it's going to grab

185
00:12:08,450 --> 00:12:14,060
its style choices from the pair to I.D. inside that dot CSX file.

186
00:12:14,660 --> 00:12:16,550
So what is it that CSX file looks like?

187
00:12:16,730 --> 00:12:17,450
Well, let's take a look.

188
00:12:18,560 --> 00:12:24,130
This is an example of what the style that CSI style looks like, and it will essentially have this hash

189
00:12:24,130 --> 00:12:27,560
tag symbol pair to and then notice.

190
00:12:27,650 --> 00:12:31,190
It almost looks like a dictionary here of the property.

191
00:12:31,670 --> 00:12:32,690
And then the value for it.

192
00:12:32,780 --> 00:12:39,710
So it says color red, which means anything tagged or linked to this I.D. of Pareto or a parameter to

193
00:12:39,740 --> 00:12:44,420
whatever you want to call it is going to be colored red when it's read in by a browser.

194
00:12:45,260 --> 00:12:50,550
So what this is doing is it's going to color this some text inside this paragraph with red.

195
00:12:51,110 --> 00:12:55,240
And this hash tag symbol is basically denoting that it's an I.D..

196
00:12:55,670 --> 00:13:00,320
And typically ideas are only used once per each HTML document.

197
00:13:01,680 --> 00:13:03,540
Now could have also been a class call.

198
00:13:03,990 --> 00:13:09,480
So it could have been class as equal to something like cool or whatever, arbitrary string here that

199
00:13:09,480 --> 00:13:11,070
links the CSX documents.

200
00:13:12,240 --> 00:13:16,500
And then it would look something like this, we would have a dot instead of a hashtag, because this

201
00:13:16,500 --> 00:13:20,190
is now a class call and classes denoted by a period or dot.

202
00:13:20,640 --> 00:13:23,970
And then we have the properties, something like color is red.

203
00:13:24,000 --> 00:13:25,930
And then the font family is poor, Don.

204
00:13:26,720 --> 00:13:26,960
OK.

205
00:13:27,400 --> 00:13:34,790
So classes are typically used when you want to add the same style to multiple elements across each tamale

206
00:13:34,800 --> 00:13:35,160
file.

207
00:13:35,580 --> 00:13:41,070
So think of ideas as typically a one use and then classes as multi use.

208
00:13:42,890 --> 00:13:48,980
So then in a much larger CSF file, you could have something that looks like this, you could have P,

209
00:13:49,100 --> 00:13:55,340
which basically says all paragraph elements in an H t Amelle file are gonna have color red, specific

210
00:13:55,340 --> 00:13:57,260
font, family, specific font size.

211
00:13:57,860 --> 00:14:03,380
Or we can adjust to something like some class, the noted by the period and then color font, family,

212
00:14:03,380 --> 00:14:10,280
font size or an I.D., which is going to effect typically one element inside the H Timmo file with in

213
00:14:10,280 --> 00:14:11,420
this case, the color blue.

214
00:14:11,570 --> 00:14:14,660
So this is what a full CSF file could look like.

215
00:14:15,260 --> 00:14:15,620
All right.

216
00:14:15,980 --> 00:14:20,810
So I know there was a lot of information that we just threw at you, but don't worry about memorizing

217
00:14:20,810 --> 00:14:21,070
this.

218
00:14:21,080 --> 00:14:22,640
We're going to see lots of examples.

219
00:14:22,940 --> 00:14:27,290
And it's going to be a lot clearer when you actually see how this all connects with Web scraping and

220
00:14:27,290 --> 00:14:27,830
python.

221
00:14:28,370 --> 00:14:33,500
The main ideas that I really want you to get out of this lecture is the fact that the H t m. file is

222
00:14:33,500 --> 00:14:34,810
going to contain the information.

223
00:14:35,310 --> 00:14:41,300
A C assess file contains the styling information, and then we can use H.T., Mel and C assess tags

224
00:14:41,510 --> 00:14:43,850
to locate specific information on a page.

225
00:14:44,210 --> 00:14:46,000
And the whole idea was scraped off Python.

226
00:14:46,280 --> 00:14:52,730
If you're gonna point Python to a particular either CSF tag or ASML element and then just grab information

227
00:14:52,820 --> 00:14:53,810
based off that.

228
00:14:55,760 --> 00:14:57,170
Now to Web scrape of Python.

229
00:14:57,230 --> 00:15:00,090
We can use the beautiful soup and requests libraries.

230
00:15:00,450 --> 00:15:02,960
And these are external libraries outside of basic python.

231
00:15:02,990 --> 00:15:06,320
So you need to install them with either Konda or Pip at your command line.

232
00:15:07,230 --> 00:15:12,780
So directly at your command line, use PIP, install requests, pip, install Elex M.L., which is going

233
00:15:12,780 --> 00:15:14,430
to be used with a beautiful super library.

234
00:15:14,790 --> 00:15:19,620
And then pip install B.S. four, which is how we can install a beautiful soup for and I know a beautiful

235
00:15:19,620 --> 00:15:23,460
soup, kind of a strange name, but you'll see why they call it a soup later on.

236
00:15:24,300 --> 00:15:28,440
Now if you're using an anaconda distribution, you should be able to use KONDA install instead of PIP

237
00:15:28,440 --> 00:15:28,830
install.

238
00:15:29,190 --> 00:15:33,000
And I'm actually going to install all of these in the next lecture so you can see what I mean.

239
00:15:34,390 --> 00:15:37,750
So let's go ahead and work through some examples of Web scraping of Python.

240
00:15:38,260 --> 00:15:38,680
Thanks.

241
00:15:38,770 --> 00:15:39,760
And I'll see you in the next lecture.
