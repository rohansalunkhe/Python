1
00:00:05,230 --> 00:00:09,040
Welcome, everyone, to this lecture where we're going to make sure we're all set up for web scraping.

2
00:00:10,060 --> 00:00:14,270
So we're going to be doing here is we will install the necessary libraries at the command line.

3
00:00:14,770 --> 00:00:18,970
And then we're going to explore how to inspect elements and view the source of a Web page.

4
00:00:19,420 --> 00:00:24,640
Keep in mind for this second part, we do highly suggest you use Google Chrome as your Web browser,

5
00:00:24,940 --> 00:00:27,400
because that way you can follow along exactly with us.

6
00:00:27,610 --> 00:00:33,760
But if you're using something like Firefox or Microsoft Edge, then you should be able to follow along

7
00:00:33,790 --> 00:00:36,730
anyways because the tools are available in all major browsers.

8
00:00:37,050 --> 00:00:38,830
They might just be named slightly differently.

9
00:00:38,890 --> 00:00:43,420
But in general, you should be able to inspect elements and view the page source in any browser.

10
00:00:43,900 --> 00:00:46,300
But if you're having trouble finding it, go ahead and just use Chrome.

11
00:00:46,900 --> 00:00:50,050
Let's get started by installing the necessary libraries to do this.

12
00:00:50,170 --> 00:00:56,470
I'm going to open up my command line so you can either open up your terminal on Ubuntu or Mac OS or

13
00:00:56,470 --> 00:00:59,980
if you're on windows, you can open up your anaconda prompt or command the prompt.

14
00:01:00,340 --> 00:01:00,940
Let's get started.

15
00:01:01,780 --> 00:01:02,020
All right.

16
00:01:02,110 --> 00:01:03,700
Here's my anaconda prompt.

17
00:01:03,970 --> 00:01:06,400
If you have any issues with your normal command prompt of windows.

18
00:01:06,670 --> 00:01:08,800
Go ahead and give a go at the anaconda prompt.

19
00:01:09,130 --> 00:01:12,060
And what we need to do here is we to install three libraries.

20
00:01:12,070 --> 00:01:13,090
Just make sure they're installed.

21
00:01:13,480 --> 00:01:16,780
If you're using Anaconda as your main setup, you should be able to use KONDA install.

22
00:01:17,050 --> 00:01:19,330
If not, you can always use PIP install as well.

23
00:01:19,900 --> 00:01:24,820
So the first one we're gonna install is called requests, which actually allows us to make a request

24
00:01:24,820 --> 00:01:27,700
to a Web site and then grab the information off of it.

25
00:01:28,030 --> 00:01:30,010
Sean, go ahead and say PIP install requests.

26
00:01:30,130 --> 00:01:31,810
Notice that mine's already satisfied.

27
00:01:32,620 --> 00:01:39,110
Next, will you want to do is pip install l x m l and that's going to be used later on by the beautiful

28
00:01:39,130 --> 00:01:43,210
soupe library in order to essentially decipher what's inside.

29
00:01:43,510 --> 00:01:44,930
What requests returns.

30
00:01:45,310 --> 00:01:47,830
So use Pip, install Alex Amelle again.

31
00:01:47,900 --> 00:01:48,160
Sorry.

32
00:01:48,190 --> 00:01:49,060
Satisfied for me.

33
00:01:49,630 --> 00:01:52,430
And then finally, I'm going to pip install B.

34
00:01:52,520 --> 00:01:56,440
S four, which stands for Beautiful Soup, version four.

35
00:01:57,370 --> 00:01:57,790
So go ahead.

36
00:01:57,790 --> 00:01:59,740
Is A pip install B as for.

37
00:02:01,930 --> 00:02:03,460
And this case, we already got it.

38
00:02:04,740 --> 00:02:04,990
OK.

39
00:02:05,110 --> 00:02:07,150
So that's it for these libraries.

40
00:02:07,210 --> 00:02:08,560
Again, it's requests.

41
00:02:09,040 --> 00:02:10,270
L x m l.

42
00:02:10,330 --> 00:02:12,050
Those aren't ones or else lowercase.

43
00:02:12,460 --> 00:02:13,030
And then B.

44
00:02:13,080 --> 00:02:13,520
S four.

45
00:02:13,720 --> 00:02:17,110
And you can check out the previous slide in case you need to understand that.

46
00:02:17,260 --> 00:02:18,670
And you can also check out the notebook.

47
00:02:18,700 --> 00:02:19,240
We have the list.

48
00:02:19,300 --> 00:02:20,470
They need to install there as well.

49
00:02:20,980 --> 00:02:23,950
So once we have those three libraries, we should be good to go in a notebook.

50
00:02:24,340 --> 00:02:28,240
We can confirm this by go ahead and you might need to restart your computer.

51
00:02:28,570 --> 00:02:33,850
But after you restart, you should be able to open up a new notebook and then import requests.

52
00:02:34,910 --> 00:02:40,140
You should also be able to import B.S. for and if you can do that inside of a notebook, then you're

53
00:02:40,140 --> 00:02:41,250
pretty much good to go.

54
00:02:41,730 --> 00:02:48,510
OK, so now what I want to do is show you how you can view a page source as well as inspect a particular

55
00:02:48,570 --> 00:02:51,690
element inside the Chrome browser to do this.

56
00:02:51,810 --> 00:02:54,330
I'm going to go to Wikipedia article page.

57
00:02:54,870 --> 00:02:55,170
All right.

58
00:02:55,200 --> 00:03:00,180
So here I am at the Wikipedia article page for Jonas Salk, which if you don't know who he is, he actually

59
00:03:00,180 --> 00:03:03,870
developed the polio vaccine, saving lots of lives.

60
00:03:04,500 --> 00:03:10,140
And now what we're going to do is take a look at the source code that your browser is receiving from

61
00:03:10,140 --> 00:03:14,250
Wikipedia and then converting it to something that you can read as a human.

62
00:03:14,790 --> 00:03:16,680
So what we're gonna do here is go in and.

63
00:03:16,680 --> 00:03:21,060
Right, click on the page and you should see the option to view page source.

64
00:03:21,390 --> 00:03:22,820
Now, be careful not to Akseli.

65
00:03:22,850 --> 00:03:23,010
Right.

66
00:03:23,010 --> 00:03:25,420
Click on something that is actually a link such as here.

67
00:03:25,440 --> 00:03:26,340
University of Michigan.

68
00:03:26,370 --> 00:03:30,780
If you right click that, we'll ask you to do things related to the link and said choose somewhere on

69
00:03:30,780 --> 00:03:35,700
the Wikipedia article that looks like just general whitespace and they should be able to view page source.

70
00:03:35,940 --> 00:03:39,570
There's also a shortcut which on my browser is control plus you.

71
00:03:40,500 --> 00:03:40,680
All right.

72
00:03:40,830 --> 00:03:47,130
So if we hit view page source, we actually now get this source h m l documents inside of a new tab

73
00:03:47,220 --> 00:03:49,680
and we can see that just like our simple examples.

74
00:03:49,730 --> 00:03:51,380
There's doctype h Amelle.

75
00:03:51,780 --> 00:03:54,000
There's this head tag, a bunch of stuff in the head.

76
00:03:54,270 --> 00:03:57,660
Things like the title, which is what the information goes in the tab here.

77
00:03:58,050 --> 00:04:00,420
And if you keep going you'll see a bunch of different links.

78
00:04:00,510 --> 00:04:05,100
And if you keep going further after that, you'll start seeing things like H1 or header tags.

79
00:04:05,610 --> 00:04:08,490
And then you can see the I.D., class calls, et cetera.

80
00:04:09,540 --> 00:04:13,980
Now, something to keep in mind is when you're web scraping, you're not going to be opening up this

81
00:04:13,980 --> 00:04:19,500
huge file and then trying to find something here that's actually really hard to do given how much information

82
00:04:19,500 --> 00:04:21,120
is here is suddenly possible.

83
00:04:21,180 --> 00:04:24,990
And it is part of the process, depending on what you're trying to achieve.

84
00:04:25,290 --> 00:04:30,300
But in general, what we really want to do is just be able to hone in on a certain section and then

85
00:04:30,300 --> 00:04:32,100
see what ideas it might have.

86
00:04:32,520 --> 00:04:37,620
So to do that, well, we can do is we'll come back to this article and let's imagine I was interested

87
00:04:37,710 --> 00:04:40,340
in trying to grab this image of Mr. Selke.

88
00:04:40,620 --> 00:04:42,700
So what I can do is I can go ahead and.

89
00:04:42,750 --> 00:04:43,110
Right.

90
00:04:43,110 --> 00:04:45,930
Click here and then hit inspect.

91
00:04:46,890 --> 00:04:53,430
And what inspect is going to do is is essentially just going to open and highlight that particular section

92
00:04:53,790 --> 00:04:54,320
of the H.

93
00:04:54,330 --> 00:04:59,850
Timal document that is related to whatever happened to be inspecting, which in this case, if you kind

94
00:04:59,850 --> 00:05:02,590
of hover over, you'll highlight is that actual image.

95
00:05:02,670 --> 00:05:03,840
So I can see the image.

96
00:05:04,170 --> 00:05:07,740
And then later on, we're going to learn how we can actually grab this image, download it and save

97
00:05:07,740 --> 00:05:08,700
it onto our computers.

98
00:05:09,150 --> 00:05:12,720
And you can do this sort of inspection for really anything on the page.

99
00:05:12,930 --> 00:05:18,600
So, for example, if you happen to be interested in the table of contents here, you could right.

100
00:05:18,600 --> 00:05:22,080
Click this here, inspect and then you can become.

101
00:05:22,510 --> 00:05:28,890
Or you could come here and start looking at what class calls are related to things in the content or

102
00:05:28,890 --> 00:05:29,880
table of contents.

103
00:05:30,240 --> 00:05:34,940
And you can actually kind of open these and expand these, see that they're a list item, et cetera.

104
00:05:35,520 --> 00:05:38,410
Now, this sort of thing is where it definitely helps to know H.

105
00:05:38,480 --> 00:05:39,600
L and CSX.

106
00:05:39,900 --> 00:05:42,180
It will make your Web scraping a lot easier.

107
00:05:42,540 --> 00:05:47,640
So if you do plan to be using Python for a lot of Web scraping tasks, I highly recommend that you learn.

108
00:05:47,700 --> 00:05:47,910
H.

109
00:05:47,910 --> 00:05:53,370
Tim L. and CSX first Earley's try to boost your skills in that because it's going to make your life

110
00:05:53,430 --> 00:05:58,530
a lot easier when it comes to Web scraping, because you'll be able to read this information easily.

111
00:05:59,820 --> 00:06:04,440
OK, so if that being said, there was just a couple of things I wanted to show you here, which was,

112
00:06:04,800 --> 00:06:10,920
again, how you can inspect the source, which is right, click view page source, and then we get this

113
00:06:10,920 --> 00:06:15,690
giant source and then also how to inspect a particular element, which is just right.

114
00:06:15,690 --> 00:06:16,980
Click and then inspect.

115
00:06:17,430 --> 00:06:20,220
And it basically highlights any particular element that we're looking at.

116
00:06:20,550 --> 00:06:25,590
For example, we're highlighting this second paragraph and you can expand on that and then view any

117
00:06:25,590 --> 00:06:27,870
links that were there and the actual material itself.

118
00:06:28,470 --> 00:06:31,110
And if you really wanted to, you can actually start editing this.

119
00:06:31,560 --> 00:06:33,210
So, for example, I can start deleting here.

120
00:06:34,020 --> 00:06:35,310
And then if I click back.

121
00:06:35,430 --> 00:06:36,840
Notice it deleted here as well.

122
00:06:37,320 --> 00:06:39,570
Keep in mind, not leading Wikipedia for everybody.

123
00:06:39,630 --> 00:06:44,340
I'm just leading this particular instance of Wikipedia that my browser is reading because I'm editing

124
00:06:44,340 --> 00:06:45,030
that source code.

125
00:06:45,690 --> 00:06:51,990
OK, so let's go ahead and explore how we can use Python along with our web scraping skills in order

126
00:06:51,990 --> 00:06:55,410
to grab information off something like a Wikipedia article page.

127
00:06:55,800 --> 00:06:56,730
I'll see you at the next lecture.
