1
00:00:05,490 --> 00:00:09,770
Welcome back everyone to the section on Python object and data structure basics.

2
00:00:11,150 --> 00:00:14,900
So in this section of the course we're going to start off by discussing basic data types.

3
00:00:15,730 --> 00:00:17,590
And these are your basic building blocks of code.

4
00:00:17,620 --> 00:00:22,420
When constructing larger pieces of code and we're going to quickly discuss all the possible data types

5
00:00:22,420 --> 00:00:26,590
that we're going to talk about in this section and then we'll have lectures that go into a lot more

6
00:00:26,590 --> 00:00:28,270
detail about each one.

7
00:00:28,270 --> 00:00:30,990
So don't worry about memorizing all the information shown here.

8
00:00:31,000 --> 00:00:34,930
This is more just a quick overview and there's going to be a lecture on each of these data types as

9
00:00:34,930 --> 00:00:36,400
we go on throughout this section.

10
00:00:37,980 --> 00:00:42,630
So the data types that we're going to be discussing are the following in this table here we have the

11
00:00:42,630 --> 00:00:44,640
name of the data type on the left hand column.

12
00:00:44,640 --> 00:00:46,380
And then there's the type.

13
00:00:46,380 --> 00:00:51,930
So what that means is just Python has an internal key word for this name and you're not going to get

14
00:00:52,200 --> 00:00:56,950
the whole word integer the whole word floating point instead Python has kind of a shortened keyword.

15
00:00:57,060 --> 00:01:03,900
So that's the second column such as I.A. for integer SDR for strings etc. then we have the description

16
00:01:03,900 --> 00:01:07,650
column and that's just going to be what the actual description of the data type is.

17
00:01:07,650 --> 00:01:09,330
So let's quickly go through these.

18
00:01:09,360 --> 00:01:14,550
We start off with integers and those are just basic whole numbers such as three three hundred two hundred.

19
00:01:14,670 --> 00:01:16,380
Then we have floating point numbers.

20
00:01:16,470 --> 00:01:20,960
And those are numbers of a decimal point 2.3 4.6 hundred point zero.

21
00:01:21,150 --> 00:01:25,530
And notice here that if even if you have a hundred point zero that still counts as a floating point

22
00:01:25,530 --> 00:01:28,970
number that's no longer an integer because it has a decimal point.

23
00:01:29,130 --> 00:01:34,080
Then after that we have strings and strings are in ordered sequence of characters.

24
00:01:34,140 --> 00:01:39,600
You can think of these as just kind of words but you should also know they can be numbers as well as

25
00:01:39,810 --> 00:01:41,940
foreign characters inside of a string.

26
00:01:41,940 --> 00:01:46,620
The main thing that denotes a string is that it either has double quotes or single quotes.

27
00:01:46,620 --> 00:01:50,070
Here we can see we have hello and double quotes Sammy in single quotes.

28
00:01:50,070 --> 00:01:53,420
We have the digits of 2000 inside double quotes.

29
00:01:53,520 --> 00:01:54,540
That makes a string.

30
00:01:54,540 --> 00:01:57,550
It's no longer just an integer because it has double quotes around it.

31
00:01:57,780 --> 00:02:01,370
And then you can also see we have some Japanese characters inside the quotes.

32
00:02:01,710 --> 00:02:08,190
Then we come to data structures and data structures are a little more specialized in basic data types

33
00:02:08,190 --> 00:02:14,370
and data objects because they can actually hold data objects in some sort of sequence or in some sort

34
00:02:14,370 --> 00:02:15,620
of mapping.

35
00:02:16,110 --> 00:02:19,640
So here we have lists and these are an ordered sequence of objects.

36
00:02:19,680 --> 00:02:24,210
Here we can see that we've been able to use square brackets and commas to you have a sequence of other

37
00:02:24,210 --> 00:02:27,470
objects such as the number 10 the string hello.

38
00:02:27,510 --> 00:02:33,220
And then the number two hundred point three dictionaries are another data structure that can store other

39
00:02:33,220 --> 00:02:36,940
data types except in this case instead of having an ordered sequence.

40
00:02:37,090 --> 00:02:39,190
They have unordered key value pairs.

41
00:02:39,250 --> 00:02:42,630
And we'll be discussing in a lot more detail how dictionaries work.

42
00:02:42,730 --> 00:02:48,400
But here we can see that we have a key than a colon and then a value and it's denoted by curly braces

43
00:02:48,490 --> 00:02:53,890
as far as if it's a dictionary instead of just being an on a list which has square brackets then we

44
00:02:53,890 --> 00:02:59,010
can have something like name and then colon and then the value of that name is Franki.

45
00:02:59,140 --> 00:03:04,260
Again we'll be discussing dictionaries in a lot more detail after dictionaries we have tuples sometimes

46
00:03:04,260 --> 00:03:08,380
are also called tuples and these are ordered immutable sequences of objects.

47
00:03:08,380 --> 00:03:13,570
They look a lot like lists except they have parentheses and they're immutable meaning you cannot change

48
00:03:13,870 --> 00:03:16,150
an object that's already in that sequence.

49
00:03:16,150 --> 00:03:17,970
So we'll be discussing what immutable means.

50
00:03:17,980 --> 00:03:23,320
And a lot more detail when discussing tuples than we have sets which are an unordered collection of

51
00:03:23,320 --> 00:03:24,730
unique objects.

52
00:03:24,730 --> 00:03:27,310
And again we'll talk about that and a lot more detail there.

53
00:03:27,340 --> 00:03:30,040
A little similar to a dictionary but they don't have key value pairs.

54
00:03:30,040 --> 00:03:33,090
Instead they're just unique objects in an ordered collection.

55
00:03:33,100 --> 00:03:35,000
So you don't have that colon key value pair.

56
00:03:35,260 --> 00:03:37,410
Again we'll discuss it in more detail later on.

57
00:03:37,630 --> 00:03:43,030
Then finally we'll talk about booleans which are just logical value values indicating true or false.

58
00:03:43,030 --> 00:03:47,120
Notice how we have capital T in capital F that sort of syntax is necessary for Python.

59
00:03:47,410 --> 00:03:48,830
So those are all the basic data types.

60
00:03:48,860 --> 00:03:53,260
Again don't worry if you didn't understand any of this or didn't want to memorize any of this we're

61
00:03:53,260 --> 00:03:57,160
going to have a lecture on Basically each of these topics in a lot more detail and show you how you

62
00:03:57,160 --> 00:03:58,690
can use them to write your own code.

63
00:03:59,520 --> 00:04:00,880
OK let's get started.

64
00:04:00,880 --> 00:04:01,840
I'll see you at the next lecture.
