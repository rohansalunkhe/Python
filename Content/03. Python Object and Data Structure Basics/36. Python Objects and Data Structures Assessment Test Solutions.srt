1
00:00:05,420 --> 00:00:07,290
Welcome back everyone in this lecture.

2
00:00:07,310 --> 00:00:12,390
We're going to quickly go over the solutions for the objects and data structures assessment test.

3
00:00:12,390 --> 00:00:16,560
So first off we have the first question which just asks you for a brief description of the falling object

4
00:00:16,560 --> 00:00:17,850
types and data structures.

5
00:00:17,850 --> 00:00:20,500
We're going to go through that first and then head over to the notebook.

6
00:00:20,520 --> 00:00:22,040
So numbers pretty straightforward.

7
00:00:22,050 --> 00:00:26,460
Basically two types of numerical information that we can store those are integers for whole numbers

8
00:00:26,810 --> 00:00:29,220
and floating points are numbers of a decimal.

9
00:00:29,400 --> 00:00:33,630
Then we have strings which are an ordered sequence of characters lists which are an order sequence of

10
00:00:33,660 --> 00:00:35,490
objects and lists are immutable.

11
00:00:35,490 --> 00:00:40,740
Many can change items that are in a list tuples very similar to lists except that they're immutable.

12
00:00:40,890 --> 00:00:45,570
They're still an ordered sequence of objects themselves and then dictionaries those are key value pairings.

13
00:00:45,580 --> 00:00:47,390
Remember those are unordered.

14
00:00:47,430 --> 00:00:50,150
So now let's hop over to the other questions in the notebook.

15
00:00:51,110 --> 00:00:53,420
All right here I am at these solutions lecture.

16
00:00:53,420 --> 00:00:54,620
Pretty straightforward stuff.

17
00:00:54,620 --> 00:00:59,470
We just want to the first question which was just answering about the data types that we have numbers.

18
00:00:59,510 --> 00:01:04,190
So all we had to do for this one was write an equation that uses all the various arithmetic methods

19
00:01:04,190 --> 00:01:05,130
we just discussed.

20
00:01:05,270 --> 00:01:09,950
And your answer is probably different than mine but all you have to do is use multiplication division

21
00:01:10,040 --> 00:01:14,630
exponents addition subtraction to somehow get back to 100 point to 5.

22
00:01:14,630 --> 00:01:16,910
So a lot of this is just practicing order of operations.

23
00:01:16,940 --> 00:01:22,790
And you can just work backwards from 100 points to five to make up your equation right Syrians for probably

24
00:01:22,790 --> 00:01:23,360
different than mine.

25
00:01:23,420 --> 00:01:28,020
Here's an example answer they wanted you to answer these three questions for typing any code.

26
00:01:28,040 --> 00:01:29,570
So what's the value of this expression.

27
00:01:29,600 --> 00:01:33,350
And this one and this one this is just to make sure you understand order of operations.

28
00:01:33,350 --> 00:01:38,390
So in this one the first one we're going to add these together before multiplying and this one we're

29
00:01:38,390 --> 00:01:41,570
going to multiply these two together before adding 5.

30
00:01:42,140 --> 00:01:47,980
And in this one we're going to multiply these together before adding four and then the next question

31
00:01:48,000 --> 00:01:50,170
What is the type of the result of the expression.

32
00:01:50,280 --> 00:01:52,510
Three plus one point five plus four.

33
00:01:52,520 --> 00:01:54,700
So the answer for that one is a floating point number.

34
00:01:54,700 --> 00:01:59,270
Because once you introduce a floating point number into this equation it doesn't matter how many integers

35
00:01:59,270 --> 00:02:00,040
there are there.

36
00:02:00,110 --> 00:02:04,060
We're going to get a floating point number.

37
00:02:04,060 --> 00:02:07,580
Now the question What would you use to find the number square root as well as that square.

38
00:02:07,750 --> 00:02:13,240
So to get a square root you can just mathematically say something to the power of 0.5 is the square

39
00:02:13,330 --> 00:02:14,410
of the number.

40
00:02:14,410 --> 00:02:19,450
Later on we're going to introduce the math library that will allow you to actually import a math function

41
00:02:19,480 --> 00:02:21,680
for doing the square automatically.

42
00:02:21,730 --> 00:02:26,920
But in this case this is a way to use a clever trick with exponents to get square root.

43
00:02:26,980 --> 00:02:34,160
Then for the square you know that's just Asterix Asterix and then to next up with serious questions.

44
00:02:34,160 --> 00:02:39,660
So for this one given the string hello we want you to list out the commands to grab the letter e.

45
00:02:39,860 --> 00:02:42,010
So in this case we just come over from zero.

46
00:02:42,020 --> 00:02:44,860
Let's zoom in one more level so we can clearly see this year.

47
00:02:45,090 --> 00:02:45,890
So 0.

48
00:02:45,910 --> 00:02:46,270
H.

49
00:02:46,280 --> 00:02:52,090
That means is that one we just had a say s of one returns a then we wanted to reverse the string Hello

50
00:02:52,100 --> 00:02:53,090
using slicing.

51
00:02:53,330 --> 00:02:54,340
So we should do this trick.

52
00:02:54,350 --> 00:02:59,530
But basically it means go from the beginning all the way to the end and the step size of negative ones

53
00:02:59,540 --> 00:03:00,870
so nice little trick there.

54
00:03:00,980 --> 00:03:02,420
They'll reverse the string.

55
00:03:02,420 --> 00:03:06,350
And the reason that it's working is because you're going backwards through string because your step

56
00:03:06,350 --> 00:03:08,750
size is a negative one.

57
00:03:09,910 --> 00:03:12,850
Then we want you to give two methods of producing the letter.

58
00:03:13,170 --> 00:03:18,850
So the ways to do that is using s to the negative or as indexing at negative 1 to get grabbed the very

59
00:03:18,850 --> 00:03:22,630
last letter of that string or just counting from the beginning up to four.

60
00:03:22,630 --> 00:03:27,060
So those are two different ways of doing that.

61
00:03:27,120 --> 00:03:32,700
Then we had lists for this one who wanted you to build the list 000 in two separate ways.

62
00:03:32,700 --> 00:03:36,610
So the first one you probably did is actually the second method listed here which is just actually constructing

63
00:03:36,610 --> 00:03:38,280
the list using square brackets.

64
00:03:38,280 --> 00:03:39,240
Pretty straightforward.

65
00:03:39,360 --> 00:03:44,280
But we also mentioned that you can use multiplication of a single list earlier in the course.

66
00:03:44,280 --> 00:03:47,180
So here we have this list 0 multiplied by three.

67
00:03:47,310 --> 00:03:50,800
And now we see here three times.

68
00:03:50,880 --> 00:03:55,170
Up next we wanted you to reassign hello and this nested list to say goodbye instead.

69
00:03:55,320 --> 00:03:58,550
And the way you do that is you have to use indexing twice.

70
00:03:58,560 --> 00:04:04,410
So here we go 0 1 and 2 to grab the entire list and then we go 0 1 2 to grab.

71
00:04:04,410 --> 00:04:05,070
Hello.

72
00:04:05,310 --> 00:04:08,330
And this is why we have two two re-assigning to goodbye.

73
00:04:08,470 --> 00:04:13,470
When we get back the results here after that we want you to sort the list below.

74
00:04:13,650 --> 00:04:15,510
So there's different methods you could've done.

75
00:04:15,930 --> 00:04:20,210
One method that we showed you was this sort method and then you just called list again.

76
00:04:20,340 --> 00:04:24,690
But there's also a built in function that I want to introduce to you now which is called sorted and

77
00:04:24,690 --> 00:04:26,300
that allows you to pass in the list.

78
00:04:26,340 --> 00:04:28,980
And this one actually returns the list itself.

79
00:04:28,980 --> 00:04:30,430
So notice what's happening here.

80
00:04:30,700 --> 00:04:36,670
The sort method does in place meaning you need to call the list again in order to see the results the

81
00:04:36,710 --> 00:04:42,060
sorted function will actually not do this in place but instead return the sorted version.

82
00:04:42,060 --> 00:04:48,720
So that's the difference between using sorted and calling that sort moving along let's talk about dictionaries.

83
00:04:48,800 --> 00:04:51,950
So using keys and indexing grab Hello from the following dictionaries.

84
00:04:51,980 --> 00:04:56,440
And this starts off simple but then gets harder and harder first one pretty straightforward.

85
00:04:56,580 --> 00:04:59,010
You just pass in simple Keets Hello.

86
00:04:59,010 --> 00:05:01,480
And the second one you have two key calls here.

87
00:05:01,500 --> 00:05:03,180
So first you do K-1.

88
00:05:03,330 --> 00:05:09,740
And that brings back the dictionary and then to get hello from this dictionary you just say K-2 then

89
00:05:09,750 --> 00:05:11,250
this one's getting a little trickier.

90
00:05:11,250 --> 00:05:13,210
So first we have to call k 1.

91
00:05:13,350 --> 00:05:19,230
Which brings us back this entire list right here that we have to call zero on that list because notice

92
00:05:19,260 --> 00:05:24,010
and this is kind of the really tricky part is that this is a list of 1 elements.

93
00:05:24,060 --> 00:05:26,550
So in order to grab that dictionary you have to call 0 there.

94
00:05:26,550 --> 00:05:28,190
So it's really tough.

95
00:05:28,260 --> 00:05:30,860
And then the next one is Nesse key.

96
00:05:30,870 --> 00:05:35,720
So from there you just kind of dealing for normal dictionary and then you have that list coming out.

97
00:05:35,730 --> 00:05:37,790
So we want to grab the second item in that list.

98
00:05:37,860 --> 00:05:39,980
And a lot of people get tripped up by this last one.

99
00:05:40,020 --> 00:05:41,510
Why is there a zero there.

100
00:05:41,520 --> 00:05:45,730
And that's because just like before we have a list of one element there.

101
00:05:45,750 --> 00:05:49,510
So if we just want to grab that one string element we can say 0 and then get back.

102
00:05:49,560 --> 00:05:51,810
Hello.

103
00:05:51,950 --> 00:05:54,580
And then finally the really tough one which was this is hard and annoying.

104
00:05:54,590 --> 00:05:55,760
This one's a little crazy.

105
00:05:55,760 --> 00:05:57,710
And again don't worry about a real life code.

106
00:05:57,710 --> 00:05:59,280
Hopefully you never have to deal something with that.

107
00:05:59,300 --> 00:06:03,840
And if you do this is really about programming but it's a pretty good question for practice.

108
00:06:03,860 --> 00:06:09,050
So what we do here is we say grab the first key then grab the second item from the list that returns

109
00:06:09,410 --> 00:06:11,520
then grab K-2 from that dictionary.

110
00:06:11,540 --> 00:06:15,020
And here grab grab index 1.

111
00:06:15,080 --> 00:06:16,470
This dictionary right here.

112
00:06:16,620 --> 00:06:22,070
Then we say grab tough and then we just do some more indexing to finally grab hello and again there's

113
00:06:22,070 --> 00:06:25,550
a zero at the end because this is a single item in a list.

114
00:06:25,670 --> 00:06:30,260
And what's really useful for you if you need to kind of break this down understand it is just build

115
00:06:30,260 --> 00:06:32,000
it out step by step and some cells.

116
00:06:32,000 --> 00:06:35,920
So first run this one then run the next step.

117
00:06:35,930 --> 00:06:38,550
DK one with two then run the next step.

118
00:06:38,570 --> 00:06:40,970
DK one or two of K-2 and so on.

119
00:06:41,060 --> 00:06:45,430
So you get the result then can you see why or why not.

120
00:06:45,440 --> 00:06:46,510
The answer is definitely no.

121
00:06:46,510 --> 00:06:51,080
You cannot say dictionary because normal dictionaries are mappings and they're not a sequence.

122
00:06:51,080 --> 00:06:54,410
And if you're a beginner and you're dealing with really small dictionaries This definitely isn't clear

123
00:06:54,410 --> 00:06:55,030
at first.

124
00:06:55,130 --> 00:06:59,630
But I can't stress enough that normal dictionaries are mappings and not a sequence and they cannot be

125
00:06:59,660 --> 00:07:00,240
ordered.

126
00:07:00,410 --> 00:07:04,670
Later on we're going to learn about some special libraries that do allow us to use kind of a dictionary

127
00:07:04,670 --> 00:07:07,830
like object which is called an order dictionary.

128
00:07:08,210 --> 00:07:12,790
But right now normal dictionaries you cannot order them or sort them.

129
00:07:12,990 --> 00:07:13,770
Then we have tuples.

130
00:07:13,770 --> 00:07:18,720
What's the difference between tuples inless will tuples as we learn are immutable and how do you create

131
00:07:18,720 --> 00:07:21,720
a tuple just with princes as shown here.

132
00:07:21,720 --> 00:07:22,930
What's unique about a set.

133
00:07:22,950 --> 00:07:25,850
The answer is that they don't allow for duplicate items.

134
00:07:25,860 --> 00:07:30,150
So kind of a hint there are just the word unique and then we wanted you to use a set to find unique

135
00:07:30,150 --> 00:07:31,670
values of the list below.

136
00:07:31,680 --> 00:07:35,470
So here you have this list and then you pass it in through the set.

137
00:07:35,550 --> 00:07:39,230
And now we get all the unique elements of that list.

138
00:07:39,240 --> 00:07:40,330
Finally we have booleans.

139
00:07:40,350 --> 00:07:42,620
We had a little table here for operators.

140
00:07:42,660 --> 00:07:46,620
This is the bonus section because basically the very next section of the Course is going to discuss

141
00:07:46,620 --> 00:07:48,760
comparison operators and logical operators.

142
00:07:49,610 --> 00:07:54,820
So you can see here the operators we have to splain this in a lot more detail in the very next lecture.

143
00:07:54,870 --> 00:07:58,620
We have a description here and then an example a lot of these are pretty straightforward.

144
00:07:58,700 --> 00:08:04,160
This is checking for equality inequality greater than less than greater than or equal to and then less

145
00:08:04,160 --> 00:08:06,170
than or equal to.

146
00:08:06,280 --> 00:08:10,700
So hopefully just by learning that little table there you can see that two is greater than three.

147
00:08:10,720 --> 00:08:11,680
That's false.

148
00:08:11,770 --> 00:08:13,380
It's 3 less equal to two.

149
00:08:13,390 --> 00:08:16,780
Well that's false as well is 3 equal to 2.0.

150
00:08:16,780 --> 00:08:17,990
That's definitely false.

151
00:08:18,150 --> 00:08:19,170
And this was kind of interesting.

152
00:08:19,180 --> 00:08:21,290
It's 3.0 equal to three.

153
00:08:21,430 --> 00:08:22,330
Well that's actually true.

154
00:08:22,330 --> 00:08:28,600
So Python in this case doesn't actually care that this floating point and this one was an integer as

155
00:08:28,600 --> 00:08:35,510
long as they hold the same value it says are true they're And it finally is four to the power of 0.5

156
00:08:35,600 --> 00:08:36,820
not equal to two.

157
00:08:37,040 --> 00:08:40,190
Well remember to the power 0.5 that same as the square root.

158
00:08:40,190 --> 00:08:43,290
So for scripta square root of force to so two is equal to two.

159
00:08:43,320 --> 00:08:45,570
Meaning the question is to not equal to two.

160
00:08:45,590 --> 00:08:49,510
Returns false and then a final question here.

161
00:08:49,720 --> 00:08:52,780
Also a bonus with the boolean output of the cell block below.

162
00:08:53,050 --> 00:08:54,200
Well let's take a look at it.

163
00:08:54,220 --> 00:08:55,990
L 1 2 0.

164
00:08:56,030 --> 00:09:00,200
So where is that we have 0 1 index 2 0 1 2.

165
00:09:00,280 --> 00:09:02,710
We have another list and then we want zero.

166
00:09:02,710 --> 00:09:03,730
So that's three.

167
00:09:03,760 --> 00:09:09,040
So we know L-1 right here at these scenes is actually asking for the number three.

168
00:09:09,160 --> 00:09:12,850
So is number three greater than or equal to whatever this is.

169
00:09:13,300 --> 00:09:14,760
So let's take a look at that.

170
00:09:14,800 --> 00:09:18,790
We want 0 1 2 0 1 2 key ones.

171
00:09:18,790 --> 00:09:19,360
That's four.

172
00:09:19,360 --> 00:09:21,910
So it's asking is 3 greater than or equal to four.

173
00:09:21,940 --> 00:09:23,040
And we know that's false.

174
00:09:23,050 --> 00:09:25,360
So that's the final bonus question.

175
00:09:25,360 --> 00:09:25,900
OK.

176
00:09:26,140 --> 00:09:28,440
Definitely these questions are a little more open ended.

177
00:09:28,480 --> 00:09:29,950
They're not quite as structured.

178
00:09:29,950 --> 00:09:34,630
Later on we're going to see a lot more structured questions that kind of have more technical right and

179
00:09:34,630 --> 00:09:39,640
wrong answers and you're going to be able to kind of more efficiently program your way through the future

180
00:09:39,640 --> 00:09:40,750
tests.

181
00:09:40,750 --> 00:09:45,430
Thanks everyone and I will see you at the next lecture where we discuss comparison operators and logical

182
00:09:45,430 --> 00:09:46,100
operators.

183
00:09:46,120 --> 00:09:46,800
We'll see you there.
