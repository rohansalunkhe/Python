1
00:00:05,720 --> 00:00:07,460
Welcome back everyone in this lecture.

2
00:00:07,460 --> 00:00:14,180
We're going to be discussing strings strings or sequences of characters using the syntax of either single

3
00:00:14,180 --> 00:00:15,770
quotes or double quotes.

4
00:00:15,800 --> 00:00:17,540
Here we can see three examples.

5
00:00:17,540 --> 00:00:19,890
First we have HELLO of single quotes than hello.

6
00:00:19,910 --> 00:00:20,850
Double quotes.

7
00:00:20,930 --> 00:00:24,380
And then what's nice about having both options of single quotes or double quotes.

8
00:00:24,380 --> 00:00:29,020
It means that if you have a single quote in your string that you want to keep and not have that end

9
00:00:29,030 --> 00:00:34,030
your actual string you can wrap it in the other type quote such as double quotes or vice versa.

10
00:00:34,070 --> 00:00:38,810
So here we can see I don't do that has a single quote in there but we don't want that single quote to

11
00:00:38,810 --> 00:00:42,230
suddenly end the string so we can wrap the whole thing in double quotes.

12
00:00:42,260 --> 00:00:45,010
Now we'll see an example of that later on in the Juber notebook.

13
00:00:46,200 --> 00:00:51,570
Now it's important to note here is that strings are ordered sequences and that means we can use indexing

14
00:00:51,570 --> 00:00:56,670
or slicing to grab subsections of the string because we know each character has a specific position

15
00:00:56,670 --> 00:01:02,670
to be in and indexing notation eases that square bracket notation after the string or the name of the

16
00:01:02,670 --> 00:01:04,040
variable assigned to the string.

17
00:01:04,050 --> 00:01:08,170
Now we're going to see lots of examples of indexing slicing in just a little bit.

18
00:01:08,400 --> 00:01:13,740
So indexing is the term used when you want to grab a single character from the string.

19
00:01:13,780 --> 00:01:18,790
So the way this works is that every single character has an index position assigned to it.

20
00:01:18,820 --> 00:01:20,040
So you start at zero.

21
00:01:20,080 --> 00:01:23,500
That's another important note in Python that indexing starts at zero.

22
00:01:23,500 --> 00:01:30,940
So H has a corresponding number of zero E has a corresponding number of 1 2 3 0 4.

23
00:01:30,940 --> 00:01:36,250
So if I wanted to grab the E What I would use is inside the square brackets I would pass one after the

24
00:01:36,250 --> 00:01:39,790
string and then it would return back E and I will see examples of that later on.

25
00:01:40,950 --> 00:01:44,510
What's also interesting about pi thumb is you can actually use reverse indexing.

26
00:01:44,730 --> 00:01:49,530
So maybe you wanted to grab the last letter of a string but you didn't actually know how long the string

27
00:01:49,530 --> 00:01:49,870
was.

28
00:01:49,890 --> 00:01:52,010
All you knew was you wanted to grab the last letter.

29
00:01:52,200 --> 00:01:57,180
Well luckily there's reverse indexing available to you so you can go just grab negative one it'll grab

30
00:01:57,180 --> 00:02:04,640
the last letter of the string regardless of how long that string actually is slicing allows you to grab

31
00:02:04,640 --> 00:02:09,760
a subsection of multiple characters otherwise known as a slice of the string.

32
00:02:09,770 --> 00:02:11,630
And this has slightly different syntax.

33
00:02:11,630 --> 00:02:15,250
Again it's going to be in square brackets because we're grabbing a subsection.

34
00:02:15,260 --> 00:02:20,840
We're going to be able to define three parts of this we're going to be able say start stop and step.

35
00:02:20,840 --> 00:02:25,840
So again this goes with square brackets with a colon separating each of these three terms.

36
00:02:25,880 --> 00:02:28,690
Star is going to be the numerical index of the slice.

37
00:02:28,700 --> 00:02:33,850
Start stop is going to be the index you will go up to but not include.

38
00:02:33,970 --> 00:02:35,070
That's an important note there.

39
00:02:35,080 --> 00:02:39,280
And we're going to really focus on that and the examples we'll see in just a bit and then that is the

40
00:02:39,280 --> 00:02:44,470
size of the jump you take from start to stop.

41
00:02:44,490 --> 00:02:46,350
OK so let's explore all these concepts.

42
00:02:46,380 --> 00:02:49,240
They're going to make a lot more sense when we actually see the code examples.

43
00:02:49,440 --> 00:02:50,920
Let's hop over to a notebook.

44
00:02:51,450 --> 00:02:54,140
OK let's quickly show a couple of examples of a string.

45
00:02:54,150 --> 00:02:58,880
Again we can use single quotes hello or you can use double quotes.

46
00:02:59,070 --> 00:03:01,320
So here I say double quotes of world.

47
00:03:01,410 --> 00:03:07,650
You can also have an entire phrase doesn't need just be one word so we can say this is also a string.

48
00:03:07,920 --> 00:03:09,200
So we have a whole phrase there.

49
00:03:09,240 --> 00:03:13,350
And the white spaces count as characters inside of the string.

50
00:03:13,350 --> 00:03:18,240
Now something we should note here is that we can mix single quotes and double quotes.

51
00:03:18,240 --> 00:03:26,820
So if I and say something like for instance I'm going on a run.

52
00:03:26,820 --> 00:03:28,290
So what's happening here.

53
00:03:28,590 --> 00:03:34,430
Because I'm using single quotes on the outside only part of this is getting highlight of the syntax

54
00:03:34,490 --> 00:03:39,660
is going to confuse Python because it thinks that you're trying to end the string here when really I'm

55
00:03:39,660 --> 00:03:40,700
trying to end the string here.

56
00:03:40,710 --> 00:03:43,530
So if I try to run the cell will say an error here.

57
00:03:43,530 --> 00:03:44,910
Invalid syntax.

58
00:03:44,910 --> 00:03:53,620
What I really want to do is wrap this in double quotes and that way I won't have an error when I have

59
00:03:53,620 --> 00:03:54,860
this single quote in here.

60
00:03:54,970 --> 00:03:59,920
And then when I run this python has no problem and says OK I get we are trying to do here trying to

61
00:03:59,950 --> 00:04:05,590
have a single quote there stay and that's actually not part of the definition of the string.

62
00:04:05,600 --> 00:04:08,930
So now let's discuss actually printing out a string.

63
00:04:08,930 --> 00:04:15,320
So far we're actually just asking the string to be returned and that's the reason we see in and out

64
00:04:15,380 --> 00:04:16,400
with these cells.

65
00:04:16,410 --> 00:04:21,160
That's also the reason we actually see the quotes in the output below the cell.

66
00:04:21,470 --> 00:04:25,490
But we can use the print function to actually print out a string.

67
00:04:25,490 --> 00:04:28,610
So we're going to say prints hello.

68
00:04:28,640 --> 00:04:33,670
And if we run this note well we actually get back we no longer see the out in the cell.

69
00:04:33,800 --> 00:04:36,110
And instead we no longer see the quotes themselves.

70
00:04:36,110 --> 00:04:39,500
We're actually just printing out the actual string.

71
00:04:39,590 --> 00:04:46,070
And the reason this is important is because let's imagine I wanted to say hello world one and then I

72
00:04:46,070 --> 00:04:51,280
also wanted to say hello world two if I were to run this.

73
00:04:51,500 --> 00:04:56,270
What ends up happening is I only get back that last string in order to see everything.

74
00:04:56,390 --> 00:05:02,630
I actually have to print out the results so I will say Prince and raptus in print sees prints hello

75
00:05:02,630 --> 00:05:08,170
world one that also prints and then say Hello World 2.

76
00:05:08,240 --> 00:05:11,330
And then when I run this I get to see bowstrings printed out.

77
00:05:11,540 --> 00:05:13,100
So I no longer see the output.

78
00:05:13,100 --> 00:05:15,180
I'm actually just printing the results.

79
00:05:15,440 --> 00:05:20,850
Now soughing also want to mention is that there's actually escape sequences and escape sequences like

80
00:05:20,900 --> 00:05:25,220
to kind of have special commands inside of your string.

81
00:05:25,250 --> 00:05:27,690
So let's just go ahead and say Prince.

82
00:05:27,890 --> 00:05:29,660
Hello world.

83
00:05:29,660 --> 00:05:35,490
And if I run this I see Hello World printed on one line but I can actually add an escape sequence.

84
00:05:35,600 --> 00:05:42,980
So it's going to be a backslash n and what this does is it basically tells Python hey this and right

85
00:05:42,980 --> 00:05:48,830
here that's no longer the character n I actually want you to because of this backslash.

86
00:05:48,980 --> 00:05:50,510
Treat this as a new line.

87
00:05:51,600 --> 00:05:59,160
So if that escape character there it's going to say Prince Hello space than a new line and then space

88
00:05:59,310 --> 00:05:59,940
world.

89
00:05:59,970 --> 00:06:00,980
So that's what we see here.

90
00:06:00,990 --> 00:06:04,320
Hello space and world if I want the world to be lined up.

91
00:06:04,350 --> 00:06:04,860
Hello.

92
00:06:05,100 --> 00:06:08,190
I could actually just touch it like this.

93
00:06:08,190 --> 00:06:13,920
Run that and then I see Hello world in Python knows not to include this letter end there because it's

94
00:06:13,980 --> 00:06:16,770
essentially attached to that backslash.

95
00:06:16,770 --> 00:06:20,200
So another popular escape sequence is t for tab.

96
00:06:20,370 --> 00:06:22,030
And if I run that I get back.

97
00:06:22,050 --> 00:06:23,180
Hello tab.

98
00:06:23,190 --> 00:06:24,870
So four spaces world.

99
00:06:25,100 --> 00:06:29,550
Well we're talking a lot more about this when we discuss print formatting coming up next.

100
00:06:29,580 --> 00:06:36,810
For now another built in function that I want to show you is the Eliane function or the length function.

101
00:06:36,900 --> 00:06:41,790
So this allows you to check the length of the string so I can say hello and higher on this I get back

102
00:06:41,790 --> 00:06:44,680
length of 5 because there's five characters in that string.

103
00:06:44,790 --> 00:06:50,220
If there happens to be a space in the string so it will say I am hungry.

104
00:06:50,340 --> 00:06:53,390
Let's make it more obvious we'll just say I am.

105
00:06:53,450 --> 00:06:54,280
We run this.

106
00:06:54,350 --> 00:06:55,990
Here we can see there's four.

107
00:06:56,030 --> 00:06:59,090
So we have I Space am.

108
00:06:59,140 --> 00:07:02,410
So that counts as a length of four characters in the string.

109
00:07:02,420 --> 00:07:03,140
All right.

110
00:07:03,140 --> 00:07:04,370
We'll stop here for now.

111
00:07:04,390 --> 00:07:08,930
In the very next lecture we'll pick up right where we left off discussing string indexing and string

112
00:07:08,930 --> 00:07:09,630
slicing.

113
00:07:09,650 --> 00:07:10,670
I'll see you there.
