1
00:00:05,360 --> 00:00:06,350
Welcome back, everyone.

2
00:00:06,860 --> 00:00:10,730
To continue on this lecture series, it's time to create a deck class.

3
00:00:11,060 --> 00:00:14,060
Let's talk a little bit about what this debt class should be able to do.

4
00:00:15,800 --> 00:00:19,490
There's three main things that we want to achieve with this deck class.

5
00:00:19,940 --> 00:00:26,120
The first is whenever we create a debt class, we would like it to instantiate a new deck, which essentially

6
00:00:26,120 --> 00:00:32,990
means create all 52 required card objects and then hold them as a list of card objects.

7
00:00:33,440 --> 00:00:36,320
And this is something a little bit new that we haven't seen before.

8
00:00:36,680 --> 00:00:41,930
And that is to say that when we instantiate that new deck, we're not just creating something like a

9
00:00:41,930 --> 00:00:44,540
list of strings or a list of integers.

10
00:00:44,990 --> 00:00:50,300
Instead, as an attribute within the deck class is going to be a list of all the cards.

11
00:00:50,660 --> 00:00:58,040
And then each variable inside that list is actually representing a full card object, which means that

12
00:00:58,040 --> 00:01:02,720
card object has that suit rank and value that were created in the previous lecture.

13
00:01:03,740 --> 00:01:06,400
Next, we want to shuffle a deck through a method call.

14
00:01:06,780 --> 00:01:12,650
So after creating a deck of those 52 card objects, we want to be able to simply shuffle the deck to

15
00:01:12,650 --> 00:01:14,090
make sure they're in a random order.

16
00:01:14,460 --> 00:01:17,210
And we can do this with the random libraries shuffle function.

17
00:01:18,190 --> 00:01:23,410
And finally, we want to make sure we can deal cards from this deck object so we know that the deck

18
00:01:23,410 --> 00:01:27,280
class is essentially holding 52 card objects at the beginning.

19
00:01:27,670 --> 00:01:33,340
And then if we want to deal cards from this deck object, we'll simply use the pop method to grab from

20
00:01:33,340 --> 00:01:36,070
the essentially top or bottom of the deck.

21
00:01:38,110 --> 00:01:39,160
So we'll be seeing.

22
00:01:39,280 --> 00:01:44,050
And this is again, the new part is that the deck class holds a list of card objects.

23
00:01:44,380 --> 00:01:45,880
And we haven't seen this before in the lectures.

24
00:01:45,890 --> 00:01:48,430
So this is one that we're gonna take a little bit of time with.

25
00:01:48,550 --> 00:01:54,780
And it's the fact that we have a class object using instances of another class as part of its attribute.

26
00:01:55,300 --> 00:01:57,530
So it's not really inheritance or anything like that.

27
00:01:57,790 --> 00:01:59,050
And it's not polymorphism.

28
00:01:59,350 --> 00:02:03,970
It's just the fact that instead of holding integers or strings, what we're doing is we're actually

29
00:02:03,970 --> 00:02:05,200
holding cards.

30
00:02:05,290 --> 00:02:08,140
And that is our specific card object.

31
00:02:08,890 --> 00:02:09,150
OK.

32
00:02:09,580 --> 00:02:12,460
Let's go ahead and get started to see how this all can be done.

33
00:02:13,180 --> 00:02:15,220
OK, here I am back on my notebook.

34
00:02:15,700 --> 00:02:19,940
What I want to point out is at the top of this notebook, if we scroll all the way to the top.

35
00:02:19,960 --> 00:02:26,500
Recall that we imported random and then we also have the suits and possible ranks and the corresponding

36
00:02:26,680 --> 00:02:28,420
values dictionary here.

37
00:02:29,050 --> 00:02:34,900
So if you want to follow along with me, you should have these four lines ready to go, as well as our

38
00:02:34,960 --> 00:02:40,480
updated instance of the card class that has a suit and rank and then uses this values dictionary to

39
00:02:40,480 --> 00:02:43,030
look up the value based off the provided rank.

40
00:02:43,420 --> 00:02:46,270
So let's go ahead and rerun these just to make sure we have them ready to go.

41
00:02:46,750 --> 00:02:51,970
We can delete that purpose air that we saw in the previous lecture and then I'll scroll down and begin

42
00:02:51,970 --> 00:02:53,180
creating my deck class.

43
00:02:53,290 --> 00:02:55,630
So say class deck.

44
00:02:57,950 --> 00:03:00,410
Up to you, whether you want to put parentheses or not, they're not required.

45
00:03:01,160 --> 00:03:03,620
And then the instantiation method.

46
00:03:03,890 --> 00:03:06,650
So in its underscore underscore two underscores.

47
00:03:07,190 --> 00:03:08,330
And then the self keyword.

48
00:03:09,370 --> 00:03:13,020
And now we want to do here is we want to create a deck.

49
00:03:13,230 --> 00:03:15,060
So what should a deck consist of?

50
00:03:15,510 --> 00:03:18,030
Well, it should be all 52 cards.

51
00:03:18,180 --> 00:03:21,540
In this case, we want all 52 card objects.

52
00:03:21,990 --> 00:03:27,120
So the way I'm going to do this is I'm going to have an attribute called all cards.

53
00:03:28,450 --> 00:03:34,240
And note here that I'm not going to actually taken any user input because the user input isn't really

54
00:03:34,240 --> 00:03:37,720
required, every DEC or every new deck should be the same.

55
00:03:38,080 --> 00:03:43,000
So if I were to create a new instance of a deck, it should be exactly the same as the previous instance

56
00:03:43,000 --> 00:03:45,190
of a deck that I previously instantiated.

57
00:03:45,340 --> 00:03:51,160
So what that means is self all cards will start off as an empty list with no input from the user.

58
00:03:51,610 --> 00:03:54,490
And then what we'll do here is the following.

59
00:03:56,630 --> 00:04:04,730
I know I need to create essentially all 52 unique cards, so I'll say four suits in suits and recall

60
00:04:04,730 --> 00:04:09,110
suits as that tuple that I have a pair of hearts, diamond spades and clubs.

61
00:04:09,410 --> 00:04:10,190
So if every suit.

62
00:04:10,250 --> 00:04:12,290
So for every heart, I need to create a rank.

63
00:04:12,320 --> 00:04:16,640
Two of hearts, three of hearts, four of hearts, then I need the same for diamonds, spades and clubs.

64
00:04:17,150 --> 00:04:23,420
So what I'll do here is say four suits in suits and then four rank in ranks.

65
00:04:24,540 --> 00:04:26,670
And this is where I will create.

66
00:04:28,570 --> 00:04:29,500
The card object.

67
00:04:31,520 --> 00:04:31,770
OK.

68
00:04:32,230 --> 00:04:36,430
So we'll be creating the card object here, which means, I'll say self-taught.

69
00:04:37,440 --> 00:04:38,220
All cards.

70
00:04:39,360 --> 00:04:40,380
And I will append.

71
00:04:40,620 --> 00:04:45,870
And the reason I can call a pen here on all cards is because all cards right now is just a normal python

72
00:04:45,870 --> 00:04:46,260
list.

73
00:04:46,590 --> 00:04:47,520
And that's how it will be.

74
00:04:47,970 --> 00:04:49,950
So what I need to do is say.

75
00:04:51,740 --> 00:04:54,800
Created card and what is created card here?

76
00:04:55,160 --> 00:04:56,660
Well, let's create it.

77
00:04:56,990 --> 00:04:58,130
I'll say created card.

78
00:05:00,250 --> 00:05:02,400
Is equal to card.

79
00:05:03,860 --> 00:05:07,820
Suit and rank, and that will actually create a new deck for us.

80
00:05:08,000 --> 00:05:09,230
So what does this actually mean here?

81
00:05:09,740 --> 00:05:13,640
Well, I have this class deck upon creating a new deck.

82
00:05:14,240 --> 00:05:19,070
It creates a brand new empty list and then fills it up with all the required cards.

83
00:05:19,220 --> 00:05:22,670
So that's four suit and suits, essentially hearts, diamonds, spades, clubs.

84
00:05:23,090 --> 00:05:26,060
And then for each rank, it creates that card.

85
00:05:26,540 --> 00:05:30,090
And recall, each of these cards will then be unique because it's just a for loop through these.

86
00:05:30,110 --> 00:05:31,160
It doesn't repeat anything.

87
00:05:31,640 --> 00:05:35,570
And then we take that newly created card and we append it to the list of all cards.

88
00:05:35,930 --> 00:05:37,910
Let's make sure this works as expected.

89
00:05:39,500 --> 00:05:43,520
So let's create a brand new deck of 52 cards.

90
00:05:44,600 --> 00:05:47,390
Open, closed princes here and there we go.

91
00:05:47,480 --> 00:05:50,780
So upon creating it, it should have already ran through this for loop.

92
00:05:51,200 --> 00:05:56,030
So let's say a new deck and check out all cards.

93
00:05:57,280 --> 00:05:57,820
There we go.

94
00:05:58,480 --> 00:06:00,730
And so sometimes students get confused here.

95
00:06:00,880 --> 00:06:04,300
They expect to see something like two of hearts, three of hearts and so on.

96
00:06:04,660 --> 00:06:10,010
But recall that all these things are actually cart objects stored somewhere in memory.

97
00:06:10,480 --> 00:06:12,640
However, it is a list of them.

98
00:06:13,180 --> 00:06:14,440
And what we can do here is this.

99
00:06:14,440 --> 00:06:17,740
Just grab the very first card in this list.

100
00:06:18,880 --> 00:06:20,310
So we have this card object.

101
00:06:20,740 --> 00:06:23,800
So we'll say this is the first card.

102
00:06:25,570 --> 00:06:30,880
In this new deck, and if I take a look at first cart now after running that cell, I should be able

103
00:06:30,880 --> 00:06:38,310
to see Renk suit and value, which means since this is a cart object, I can also print first card and

104
00:06:38,330 --> 00:06:40,060
reports back two of hearts.

105
00:06:40,390 --> 00:06:41,560
Now, is this what I expected?

106
00:06:42,010 --> 00:06:43,150
Well, if you scroll back up here.

107
00:06:43,570 --> 00:06:45,220
Notice that Hearts is the first suit.

108
00:06:45,760 --> 00:06:47,970
And then two is the first rank.

109
00:06:48,010 --> 00:06:50,650
So the two of hearts should have been at the very top of the deck.

110
00:06:50,950 --> 00:06:54,250
And the first one created, let's confirm that with the very last value.

111
00:06:54,670 --> 00:06:58,690
The very last thing at the bottom of the deck, so to speak, should be clubs.

112
00:06:59,140 --> 00:07:01,360
And then if we go all the way here, should be ace.

113
00:07:01,780 --> 00:07:07,930
So that means forward a scroll down here and instead grab the very last card by saying negative one,

114
00:07:07,960 --> 00:07:09,340
go to the very end of the list.

115
00:07:09,910 --> 00:07:11,530
So we'll call this the bottom card.

116
00:07:12,970 --> 00:07:16,420
And it's up to you whether you want to think of the beginning of the list as a top or bottom of the

117
00:07:16,420 --> 00:07:16,840
deck.

118
00:07:17,590 --> 00:07:18,670
So there's the bottom card.

119
00:07:18,880 --> 00:07:22,940
We'll say, and if I print the bottom card, I get back.

120
00:07:22,960 --> 00:07:24,040
The ace of clubs.

121
00:07:24,460 --> 00:07:25,660
So this is actually working.

122
00:07:26,130 --> 00:07:38,260
And I could say for card object in new deck, all cards, go ahead and print that card object.

123
00:07:38,770 --> 00:07:40,810
And I see two of hearts, three of hearts and so on.

124
00:07:41,170 --> 00:07:48,340
So what this is doing is all cards holds various card objects and then recall card objects when you

125
00:07:48,340 --> 00:07:51,430
print them, return back their rank of their suit.

126
00:07:51,940 --> 00:07:52,480
Perfect.

127
00:07:53,050 --> 00:07:54,670
So what else do we need to add to this?

128
00:07:54,850 --> 00:07:58,420
Well, right now, if I create a deck, it's always going to be an order.

129
00:07:58,840 --> 00:08:02,380
It would be nice to be able to shuffle a deck through a nice method call.

130
00:08:03,220 --> 00:08:04,180
So now let's create that.

131
00:08:06,010 --> 00:08:10,000
And make sure you pay very close attention to the indentation here, because we're actually several

132
00:08:10,000 --> 00:08:10,630
levels deep.

133
00:08:10,940 --> 00:08:14,920
I want to make sure this new method is in line with all the other method calls.

134
00:08:15,520 --> 00:08:20,140
So we will call this the shuffle method or shuffle deck method, whatever you want to call it.

135
00:08:20,710 --> 00:08:21,700
We'll say self here.

136
00:08:23,360 --> 00:08:27,830
And then what we need to do is we have this internal list of card objects.

137
00:08:28,310 --> 00:08:33,530
And it should already exist because upon instantiation it magically, not really magically.

138
00:08:33,560 --> 00:08:35,060
But it just creates it for us.

139
00:08:35,570 --> 00:08:37,940
And what I need to do is now shuffle that list.

140
00:08:38,390 --> 00:08:42,830
And I know that if I have a list, for example, if I have the list.

141
00:08:44,770 --> 00:08:46,030
One, two, three, four, five.

142
00:08:46,340 --> 00:08:47,230
And I want to shuffle it.

143
00:08:47,800 --> 00:08:50,020
I can say from the random library.

144
00:08:52,550 --> 00:08:53,540
Random shuffle.

145
00:08:54,580 --> 00:08:55,210
My list.

146
00:08:56,100 --> 00:08:58,780
And now if I take a look at my list, it's been shuffled.

147
00:08:59,470 --> 00:09:04,720
So a couple of things to note here is that in this case, I'm calling random dot shuffle.

148
00:09:05,050 --> 00:09:11,830
The other way to do this is to say something like ferme random import shuffle.

149
00:09:12,130 --> 00:09:15,340
And then if you wanted to, you could just call shuffle like that.

150
00:09:15,880 --> 00:09:21,100
But in our case, since we only imported random I will call random dot shuffle.

151
00:09:21,160 --> 00:09:22,660
So that's the way the imports work here.

152
00:09:23,790 --> 00:09:28,750
And you can check out the modules and package series of lectures to further understand this.

153
00:09:28,870 --> 00:09:33,310
But the thing you need to know right now is that what I'm doing is I'm calling the shuffle function

154
00:09:33,640 --> 00:09:37,990
from pythons built in random library, and we know that it shuffles the list.

155
00:09:38,410 --> 00:09:41,890
OK, so what's important to note here, though, is random.

156
00:09:41,890 --> 00:09:44,140
That shuffle actually does not return anything.

157
00:09:44,500 --> 00:09:46,360
It does all of this in place.

158
00:09:46,450 --> 00:09:52,210
That is to say that I can't actually assign the result of the call random, not shuffle.

159
00:09:52,810 --> 00:09:56,440
So if I have my list in hour to try to do this.

160
00:09:57,810 --> 00:10:00,020
Shuffled list as equal to random shuffle.

161
00:10:00,810 --> 00:10:03,210
What's gonna happen is my list will still be shuffled.

162
00:10:04,210 --> 00:10:07,660
However, they shuffled list is nothing.

163
00:10:07,720 --> 00:10:08,980
It doesn't actually exist.

164
00:10:09,880 --> 00:10:13,060
So if I check type, it returns nothing.

165
00:10:13,330 --> 00:10:16,900
So that is to say random, that shuffle does not return anything.

166
00:10:16,930 --> 00:10:22,660
When you call a function instead, it just does everything in place to your original list.

167
00:10:23,350 --> 00:10:27,850
So if we come back up here and start thinking about our shuffle method for the deck, what do we actually

168
00:10:27,850 --> 00:10:28,720
want to achieve here?

169
00:10:29,320 --> 00:10:33,340
All we need to do is internally shuffle all the cards.

170
00:10:33,700 --> 00:10:36,460
So what I should be doing is call random.

171
00:10:36,460 --> 00:10:39,460
That shuffle on self-taught.

172
00:10:41,070 --> 00:10:41,880
All cards.

173
00:10:42,360 --> 00:10:43,770
Now, do I need to return this?

174
00:10:44,220 --> 00:10:45,340
In this case, no.

175
00:10:45,480 --> 00:10:48,630
Because we call random shuffle does not return anything.

176
00:10:49,020 --> 00:10:51,960
And I don't actually really care about storing the results anywhere.

177
00:10:52,410 --> 00:10:57,510
Instead, I want to know that the particular instance of the deck I've created has been shuffled.

178
00:10:58,170 --> 00:11:03,720
So what we're gonna do here is I'm going to rerun this cell to make sure that new method calls in there.

179
00:11:04,410 --> 00:11:07,680
And then we're gonna do here is create a new deck.

180
00:11:08,310 --> 00:11:12,630
Let me delete these three cells that we created and let's go ahead and grab that bottom card.

181
00:11:14,540 --> 00:11:16,400
So what is my bottom card right now?

182
00:11:18,900 --> 00:11:20,010
It's ace of clubs.

183
00:11:20,250 --> 00:11:25,980
However, let's now take that new deck and call the shuffle method on it.

184
00:11:26,670 --> 00:11:28,860
That's shuffle, dot or dot shuffle.

185
00:11:29,850 --> 00:11:32,640
And now let's take new deck.

186
00:11:33,120 --> 00:11:36,300
In fact, Luscious Prince, new deck of cards.

187
00:11:36,390 --> 00:11:38,070
Let me just copy and paste it here.

188
00:11:39,820 --> 00:11:40,110
And no.

189
00:11:40,240 --> 00:11:46,300
Now it's a jack of hearts, which means internally, this stop all cards list has been shuffled and

190
00:11:46,300 --> 00:11:49,870
everything's now in a different order, which is exactly what we want to achieve.

191
00:11:50,320 --> 00:11:56,800
So this happens all internally with the built in all cards attribute and nothing's actually being returned

192
00:11:56,800 --> 00:11:58,420
to you when you call shuffle.

193
00:11:58,870 --> 00:12:01,780
Instead, it's all happening within that deck class.

194
00:12:02,890 --> 00:12:09,160
More specifically, it's all happening to the all cards attribute, which is just a list of card objects,

195
00:12:09,220 --> 00:12:10,450
which is why I'm able to print it.

196
00:12:10,640 --> 00:12:10,860
Earley's.

197
00:12:11,200 --> 00:12:12,820
One of them here, the very bottom one.

198
00:12:13,390 --> 00:12:17,680
And if we take a look at the very first one, it's no longer the two of hearts.

199
00:12:17,740 --> 00:12:19,810
It's a seven of diamonds because it's all been shuffled.

200
00:12:21,880 --> 00:12:25,690
Now, the last thing to note here is currently all cards.

201
00:12:25,750 --> 00:12:27,970
We'll always have those 52 cards.

202
00:12:28,450 --> 00:12:34,420
I've been able to create those unique 52 cards and I have been able to shuffle them around in a real

203
00:12:34,420 --> 00:12:39,550
game, though, when I have that deck of cards, I need to be able to remove actual cards.

204
00:12:40,120 --> 00:12:42,660
And let's go ahead and create a method that can deal.

205
00:12:42,790 --> 00:12:47,020
One card essentially grabbing one of the cards from somewhere in the list.

206
00:12:48,070 --> 00:12:49,780
So I will say D.F..

207
00:12:51,180 --> 00:12:53,030
Deal one takes himself.

208
00:12:53,610 --> 00:12:55,080
No parameters necessary here.

209
00:12:55,890 --> 00:12:59,550
And what I want to do is I want to return a single card.

210
00:13:00,480 --> 00:13:04,590
So I will say return self-taught all cards.

211
00:13:05,080 --> 00:13:10,620
And because all cards is a list, what I can simply do is call the pop method off of this.

212
00:13:11,100 --> 00:13:12,480
So let's think about what this is doing.

213
00:13:12,900 --> 00:13:17,070
I have this attribute all cards, which is a list of these card objects.

214
00:13:17,400 --> 00:13:23,310
If I want to remove one of these card objects and return it in order to use it for whatever other classes

215
00:13:23,310 --> 00:13:23,670
need it.

216
00:13:24,090 --> 00:13:30,900
Then I will call deal one and it will take that list of all cards and then pop one off and essentially

217
00:13:30,930 --> 00:13:31,680
return it to me.

218
00:13:32,820 --> 00:13:35,010
So let's rerun this and make sure that works.

219
00:13:35,070 --> 00:13:38,170
So I will run DEC again to make sure it has the deal won.

220
00:13:38,820 --> 00:13:40,830
And then let's create some new cells here.

221
00:13:41,430 --> 00:13:43,410
I'll read the fine new deck.

222
00:13:44,880 --> 00:13:46,380
Let's go ahead and shuffle.

223
00:13:48,720 --> 00:13:50,910
The new deck, and now I'm going to say.

224
00:13:52,040 --> 00:13:59,690
My card is equal to New Deck, and I will say deal one.

225
00:14:01,500 --> 00:14:05,220
And now notice my car should be a card object somewhere in memory.

226
00:14:06,210 --> 00:14:06,810
And there it is.

227
00:14:06,930 --> 00:14:11,130
I have this card object somewhere in memory, and if I print this out, I'll report back to me.

228
00:14:11,160 --> 00:14:12,330
What card actually have.

229
00:14:12,810 --> 00:14:15,300
So I got the six of spades from this deal.

230
00:14:15,870 --> 00:14:21,580
However, the important part to know is let's check the length of new deck.

231
00:14:23,060 --> 00:14:28,970
All cards and notice now it's 50 one cards when a standard deck, it's 52 cards.

232
00:14:29,120 --> 00:14:30,020
So what happened here?

233
00:14:30,350 --> 00:14:36,320
I have now essentially permanently affected the current cards within the deck because I removed one

234
00:14:36,320 --> 00:14:38,270
without pop all cards.

235
00:14:38,360 --> 00:14:40,160
Now only has 51.

236
00:14:40,580 --> 00:14:45,260
And eventually what's going to happen is you deal them all out, then you may get an error on deal one

237
00:14:45,260 --> 00:14:49,490
by saying there's no more items in this all cards list to pop off.

238
00:14:50,170 --> 00:14:50,420
OK.

239
00:14:50,780 --> 00:14:57,470
So again, what we did here is we created a class deck and then we're gonna do is upon instantiating

240
00:14:57,470 --> 00:14:57,650
it.

241
00:14:57,980 --> 00:15:01,190
You essentially refill all those 52 cards that are unique.

242
00:15:01,550 --> 00:15:06,560
You have the ability to now shuffle the deck and you have the ability to remove a single card from the

243
00:15:06,560 --> 00:15:06,950
deck.

244
00:15:07,490 --> 00:15:14,180
And because you can remove a single card, what I could do is simply run a for loop and say deal one

245
00:15:14,180 --> 00:15:17,450
52 times if I wanted to remove all the cards from the deck.

246
00:15:17,840 --> 00:15:21,230
So actually, just being able to deal one card is enough for our use cases.

247
00:15:21,590 --> 00:15:25,580
If you want, you could expand on this to deal to cards at all times, three cards at a time or so on,

248
00:15:25,910 --> 00:15:27,920
or split the deck in half, et cetera.

249
00:15:28,190 --> 00:15:33,680
But really with just the deal, one method, we have enough that we can use outside Python logic in

250
00:15:33,680 --> 00:15:36,770
order to call deal one method as many times as necessary.

251
00:15:37,500 --> 00:15:39,410
OK, so that's the deck class.

252
00:15:39,680 --> 00:15:44,810
Coming up next, we're going to do is create a player class and the player could then actually hold

253
00:15:44,900 --> 00:15:47,240
cards in what it will call their hand.

254
00:15:47,720 --> 00:15:50,300
So we're going to do is explore that in the next lecture.

255
00:15:50,690 --> 00:15:51,230
I'll see you there.
