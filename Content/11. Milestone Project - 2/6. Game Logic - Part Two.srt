1
00:00:05,350 --> 00:00:06,330
Welcome back, everyone.

2
00:00:06,490 --> 00:00:09,970
In the previous lecture, we visualized and discussed our game logic.

3
00:00:10,390 --> 00:00:12,160
Now it's time to actually program it.

4
00:00:12,370 --> 00:00:13,960
Let's head back to the Jupiter notebook.

5
00:00:14,590 --> 00:00:14,820
OK.

6
00:00:14,890 --> 00:00:17,230
Here I am at the notebook that we've been working with.

7
00:00:17,410 --> 00:00:19,630
So in this notebook, there's a couple of important things.

8
00:00:20,020 --> 00:00:28,150
I have my player class to find my deck class defined and then my card class defined as well as suits,

9
00:00:28,150 --> 00:00:29,140
ranks and values.

10
00:00:29,260 --> 00:00:34,360
And this import random call that's already all defined and has been run in this notebook.

11
00:00:35,050 --> 00:00:38,290
And basically, there's going to be a just three major parts of this.

12
00:00:38,320 --> 00:00:41,860
One is the game set up actually creating the players?

13
00:00:41,920 --> 00:00:43,630
So we'll do that kind of on one cell.

14
00:00:44,170 --> 00:00:48,700
And then the next part is going to be essentially the while loops and there's two of them.

15
00:00:49,720 --> 00:00:51,820
It's game on the main logic.

16
00:00:52,090 --> 00:00:55,450
And then we'll have to check in an indented while loop.

17
00:00:55,510 --> 00:01:00,370
So while the game is on, in case we are at war, so to speak.

18
00:01:00,910 --> 00:01:01,170
OK.

19
00:01:01,360 --> 00:01:02,350
So let's start with the game.

20
00:01:02,350 --> 00:01:03,580
Set up is the easiest one.

21
00:01:04,030 --> 00:01:05,260
We create those two players.

22
00:01:06,190 --> 00:01:09,850
We will say player one is equal to an instance of the player class.

23
00:01:10,210 --> 00:01:12,520
And let's just give this person the name one.

24
00:01:13,210 --> 00:01:14,410
So it's easy to identify.

25
00:01:15,040 --> 00:01:17,320
And then we'll say player two is equal to.

26
00:01:18,400 --> 00:01:19,540
We will say player two.

27
00:01:22,080 --> 00:01:24,140
And then we have to create a new deck.

28
00:01:25,570 --> 00:01:30,250
So say a new deck is equal to an instance of deck and let's shuffle that deck.

29
00:01:31,340 --> 00:01:36,800
By calling the shuffle methadone, it recall nothing's returned here, just shuffles that list inside

30
00:01:36,800 --> 00:01:37,280
of deck.

31
00:01:38,290 --> 00:01:43,780
And then after we're done shuffling it, we have to split this deck between both players, lots of different

32
00:01:43,780 --> 00:01:44,560
ways you can do this.

33
00:01:44,620 --> 00:01:52,870
One simple way is to simply say for X and range twenty six Y twenty six, because a deck has 52 cards.

34
00:01:53,110 --> 00:01:55,180
Half of fifty two is twenty six.

35
00:01:55,510 --> 00:02:01,300
And the reason we use half here is because we'll say player one and we're gonna say add cards.

36
00:02:03,200 --> 00:02:09,750
And we'll say from that new deck, go ahead and deal one card, open and close parentheses, since that's

37
00:02:09,750 --> 00:02:14,850
a method call and we will do the exact same thing for player to.

38
00:02:16,830 --> 00:02:22,480
So what's happening here is basically two cards at a time, we had one, two, player one and then one,

39
00:02:22,480 --> 00:02:25,420
two, player two, which is why I don't go through all 52.

40
00:02:25,710 --> 00:02:27,990
I just go through this 26 times.

41
00:02:28,170 --> 00:02:31,950
So I'm adding two cards for each iteration in this for loop.

42
00:02:32,820 --> 00:02:33,210
So I run.

43
00:02:33,210 --> 00:02:35,700
That looks like it worked.

44
00:02:35,790 --> 00:02:41,580
And one quick way to confirm this is if I say player one, all cards and just check the length of that.

45
00:02:43,040 --> 00:02:49,040
Then I can see they have twenty six cards and I can then also, for instance, print out the very first

46
00:02:49,040 --> 00:02:51,480
card I see they have here.

47
00:02:51,530 --> 00:02:52,910
The King of Hearts is their first card.

48
00:02:53,630 --> 00:02:53,880
All right.

49
00:02:54,380 --> 00:02:57,950
So finally, we'll say the game.

50
00:02:59,030 --> 00:03:00,920
On is equal to true.

51
00:03:01,850 --> 00:03:06,830
In order to make sure that we actually start off this while loop correctly, so we have this wild game

52
00:03:06,830 --> 00:03:07,400
on loop.

53
00:03:08,670 --> 00:03:09,960
So what are we going to do here?

54
00:03:10,080 --> 00:03:11,580
We'll say while.

55
00:03:13,050 --> 00:03:15,360
Game on and outside of this.

56
00:03:15,450 --> 00:03:21,510
Let's go ahead and have a counter called round number and we'll set it at zero.

57
00:03:22,020 --> 00:03:24,870
And then for each round, I will say.

58
00:03:26,480 --> 00:03:32,450
The round number is plus equal to one, essentially adding one to my current counter.

59
00:03:32,900 --> 00:03:38,690
That way I can print this out and see how many rounds it's taking to actually play these matches.

60
00:03:39,290 --> 00:03:41,270
So, say, currently on round.

61
00:03:43,340 --> 00:03:48,080
And then round underscore numb using f string, literal to basically print round number.

62
00:03:48,560 --> 00:03:51,590
So this is round and then we'll start off at one.

63
00:03:51,620 --> 00:03:52,100
Then two.

64
00:03:52,100 --> 00:03:52,490
Then three.

65
00:03:52,490 --> 00:03:52,800
Then four.

66
00:03:52,850 --> 00:03:53,360
Then so on.

67
00:03:53,990 --> 00:03:56,090
OK, so we're printing out the rounds.

68
00:03:56,390 --> 00:03:57,490
What do we have to do first?

69
00:03:57,590 --> 00:04:00,110
We should probably check to see if a player is out of cards.

70
00:04:00,860 --> 00:04:04,220
We know that won't happen on the very first round, but it may happen later on.

71
00:04:04,460 --> 00:04:07,550
So we'll just keep doing the check at the top of this game on.

72
00:04:08,000 --> 00:04:13,220
So we'll say if the length of player one, all cards.

73
00:04:15,450 --> 00:04:16,350
Is equal to zero.

74
00:04:16,680 --> 00:04:17,910
That means they have no cards left.

75
00:04:18,440 --> 00:04:19,200
So in this case.

76
00:04:19,860 --> 00:04:20,760
Player one has lost.

77
00:04:20,820 --> 00:04:21,960
So we say player one.

78
00:04:23,930 --> 00:04:24,680
Out of cards.

79
00:04:27,280 --> 00:04:28,870
And we can then say also as well.

80
00:04:29,590 --> 00:04:30,430
Player two wins.

81
00:04:32,230 --> 00:04:34,810
And once we're done with that, we'll go ahead and say.

82
00:04:37,120 --> 00:04:44,620
That game on is now equal to false and to just not even bother continuing over thing, we can break

83
00:04:44,620 --> 00:04:49,000
out of this while loop, which is essentially going to happen anyways because game on is equal to false

84
00:04:49,000 --> 00:04:50,010
here will say break.

85
00:04:50,350 --> 00:04:52,060
And that essentially ends the game there.

86
00:04:52,540 --> 00:04:53,770
Now, we need to do this as well.

87
00:04:53,890 --> 00:04:58,840
For player two is going to copy this and paste it here.

88
00:05:00,280 --> 00:05:08,990
Except instead of player one, I'll say player two, all cards and then say player two loses.

89
00:05:09,880 --> 00:05:11,890
And player one wins.

90
00:05:12,430 --> 00:05:17,920
So if the length of player one's cards is equal to zero, then they're out of cards and player two wins.

91
00:05:18,160 --> 00:05:19,310
Game on is equal to false.

92
00:05:19,390 --> 00:05:20,860
Break out of this whole loop.

93
00:05:20,950 --> 00:05:26,280
Break out of all logic that's happening here in the game's over, round number, whatever then.

94
00:05:26,680 --> 00:05:30,340
Player two If their cards are equal to zero, the length their cards equal zero.

95
00:05:30,700 --> 00:05:31,600
They're out of cards.

96
00:05:31,630 --> 00:05:32,500
Player one wins.

97
00:05:32,680 --> 00:05:33,250
Same thing.

98
00:05:33,880 --> 00:05:35,950
Otherwise, the game is still on.

99
00:05:37,730 --> 00:05:44,750
So what I will do here in order for the game to continue is it's time to start a new round and the new

100
00:05:44,750 --> 00:05:51,380
round we're gonna say player one cards and this variable, you can think about it.

101
00:05:51,470 --> 00:05:52,220
As the cards.

102
00:05:52,290 --> 00:05:53,960
The player is going to leave on the table.

103
00:05:54,710 --> 00:05:55,970
So it's going to be an empty list.

104
00:05:56,330 --> 00:06:01,640
Something want to make clear here is that player one, all cards is not going to be the same thing as

105
00:06:01,640 --> 00:06:02,660
player one cards.

106
00:06:02,720 --> 00:06:08,810
You can think of player one cards as the current cards in play versus all cards, which is the cards

107
00:06:09,080 --> 00:06:14,600
that player has essentially close to them face over on their hand.

108
00:06:15,740 --> 00:06:17,040
Or face down, I should say.

109
00:06:17,670 --> 00:06:22,740
OK, so player one cards, and then we'll also say player two cards.

110
00:06:24,850 --> 00:06:26,320
And they start off as empty lists.

111
00:06:26,620 --> 00:06:31,750
If we're going to start a new round done, all we need to do is remove a card from their list of all

112
00:06:31,750 --> 00:06:35,140
cards and appended to the cards currently in play.

113
00:06:36,660 --> 00:06:39,720
So will say player one cards.

114
00:06:41,530 --> 00:06:44,960
And I'm going to append in this case, player one.

115
00:06:46,870 --> 00:06:47,680
Remove one.

116
00:06:49,400 --> 00:06:54,470
And then will say here the exact same thing we have, so I'll just copy and paste this.

117
00:06:56,600 --> 00:07:01,130
And switch this to two cards or player two cards.

118
00:07:02,910 --> 00:07:03,190
OK.

119
00:07:03,860 --> 00:07:09,880
And then we'll have to start checking the actual comparisons, which will be in the next lecture, bullish

120
00:07:09,890 --> 00:07:10,070
still.

121
00:07:10,100 --> 00:07:11,540
Quick overview of what we did here.

122
00:07:12,080 --> 00:07:13,010
We set up the game.

123
00:07:13,160 --> 00:07:14,780
Player one and player two are ready to go.

124
00:07:15,240 --> 00:07:16,220
We create a new deck.

125
00:07:16,250 --> 00:07:16,880
We shuffled it.

126
00:07:17,330 --> 00:07:18,890
We split the deck between the players.

127
00:07:19,100 --> 00:07:20,390
Tons of different ways you could do this.

128
00:07:20,900 --> 00:07:25,220
Then we set a variable just to have the game continue, kind of until someone loses.

129
00:07:25,700 --> 00:07:28,610
We put out the round numbers check to see if anyone is losing.

130
00:07:28,940 --> 00:07:32,270
If they have lost, then they'll go ahead and just exit out of the while loop.

131
00:07:32,720 --> 00:07:36,800
If not time start a new round player, one cards starts off empty.

132
00:07:36,860 --> 00:07:41,900
We grab a single card, remove it player two cards, starts a fancy, grab a single card and remove

133
00:07:41,900 --> 00:07:42,020
it.

134
00:07:42,530 --> 00:07:49,190
Next, we're going have to do during this kind of while at war it start checking to see if the cards

135
00:07:49,400 --> 00:07:55,130
are greater than each other, less than each other, or if they happen to be equal, then we will use

136
00:07:55,220 --> 00:07:59,390
an actual war comparison in order to keep appending more cards.

137
00:07:59,890 --> 00:08:03,290
OK, so what we'll do is continue with this in the next lecture.

138
00:08:03,500 --> 00:08:04,010
I'll see you there.
