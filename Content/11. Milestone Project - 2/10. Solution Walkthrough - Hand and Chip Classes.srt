1
00:00:05,780 --> 00:00:09,340
Welcome back everyone to continue where we left off last time.

2
00:00:09,380 --> 00:00:12,460
We're going to create a hand class and a Chip's class.

3
00:00:12,500 --> 00:00:16,700
I want to quickly review what we did last time because we're going to be using it to actually create

4
00:00:16,760 --> 00:00:18,210
our hand class.

5
00:00:18,230 --> 00:00:21,110
Let's come back up to the very top very quickly.

6
00:00:21,110 --> 00:00:26,090
Remember that we have these suits ranks and values these are global variables that we can easily use

7
00:00:26,510 --> 00:00:28,890
and most importantly we have this values dictionary.

8
00:00:28,910 --> 00:00:35,630
That means I can pass in a rank such as two or three or four and immediately get back its actual value

9
00:00:35,630 --> 00:00:36,450
for the sum.

10
00:00:36,560 --> 00:00:38,370
We're going to be using that in the hand.

11
00:00:38,510 --> 00:00:44,120
Well we want to calculate the value of a hand what we'll be able to do is in the actual rank of the

12
00:00:44,120 --> 00:00:48,280
card and then get back the value itself.

13
00:00:48,410 --> 00:00:52,330
After that we actually created instances of card classes.

14
00:00:52,340 --> 00:00:57,230
So what that means is we basically have this card object where the card objects has a suit and has a

15
00:00:57,230 --> 00:00:57,900
rank.

16
00:00:58,010 --> 00:01:04,400
That means we should when passing in a card grab the rank attribute and pass that into the values dictionary

17
00:01:04,670 --> 00:01:06,930
in order to get back in actual numerical value.

18
00:01:08,790 --> 00:01:14,640
Then moving on to the debt class this was just the way to quickly create 52 unique card objects to represent

19
00:01:14,730 --> 00:01:19,530
each card in the actual deck so we can see here that we used nested for loop.

20
00:01:19,560 --> 00:01:22,230
So first suit in suits for rank and ranks.

21
00:01:22,230 --> 00:01:26,580
We went ahead and appended these card objects repasts and the suit in ranks.

22
00:01:26,700 --> 00:01:31,710
We also created a way to print them out shuffle the deck and then deal from deck.

23
00:01:31,710 --> 00:01:35,240
Now it's time we scroll down here to actually create a hand class.

24
00:01:35,240 --> 00:01:40,040
The hand class is basically going to represent what cards are currently in someone's hand.

25
00:01:40,470 --> 00:01:44,310
So the hand-clasp when you start off it's going to start with an empty list and then we're going to

26
00:01:44,400 --> 00:01:46,530
add cards from the deck.

27
00:01:46,530 --> 00:01:53,160
So we'll be if you come up here calling deal from the deck and then grabbing that single card and then

28
00:01:53,250 --> 00:01:58,220
adding it to someone's hand which is what we're going to use when we call this add a card method.

29
00:01:58,350 --> 00:02:01,400
So remember that's going to be an actual card object.

30
00:02:01,620 --> 00:02:06,980
Then we have it value attribute to the hand which is just the current sum of the value for that hand.

31
00:02:07,320 --> 00:02:11,580
And then we also want to keep track of aces in the hand because recall the aces have that special rule

32
00:02:11,820 --> 00:02:16,240
where they can either be a 1 or 11.

33
00:02:16,250 --> 00:02:21,370
So let's start with this method for adding a card.

34
00:02:21,500 --> 00:02:28,410
What we're going to do here is say self-taught cards and then we're going to append the card that's

35
00:02:28,470 --> 00:02:29,030
added.

36
00:02:29,220 --> 00:02:37,570
So here we have an empty list and I will say self-talk cards of that empty list append the card object.

37
00:02:37,940 --> 00:02:41,920
So don't get confused here the fact this is a lower case c..

38
00:02:41,930 --> 00:02:53,050
Keep in mind that the card passed in is actually going to be from this deck right here meaning we're

39
00:02:53,050 --> 00:02:59,070
going to call deal with this deck in order to actually grab a single card from it.

40
00:03:00,640 --> 00:03:02,200
So that's what this card represents.

41
00:03:02,200 --> 00:03:04,390
Later on we're saying add a card.

42
00:03:04,510 --> 00:03:11,830
This is going to be from the deck and it's going to be deal because remember deck that deal.

43
00:03:12,190 --> 00:03:19,580
That's just a single card object and the card object itself has a suit and ring to it.

44
00:03:20,200 --> 00:03:23,370
So I'm going to say soft but cards a pen that card.

45
00:03:23,860 --> 00:03:29,590
And since we know that this card object has a ring to it we're going to be able to use that rank to

46
00:03:29,590 --> 00:03:31,270
get the value.

47
00:03:31,280 --> 00:03:32,370
So how do we do this.

48
00:03:32,600 --> 00:03:39,330
Well we grabbed that card Rinck remember that's going to be two three King ace and then we pass that

49
00:03:39,330 --> 00:03:47,040
in to the values dictionary as a key and that's going to return back some number representing the value

50
00:03:47,100 --> 00:03:48,860
for that particular cards rank.

51
00:03:49,020 --> 00:03:52,320
And we'll take that value and add it to our current value.

52
00:03:52,680 --> 00:03:59,180
So we'll say self the value plus equals values card that rank.

53
00:03:59,190 --> 00:04:03,040
So a lot of stuff is happening in just two lines of code here.

54
00:04:03,120 --> 00:04:06,740
We're taking in a card object that will eventually call from deck deal.

55
00:04:06,840 --> 00:04:09,970
We're appending it to this hand's current list of cards.

56
00:04:10,110 --> 00:04:15,030
And then we're adjusting our value to the card that was just added to this list.

57
00:04:15,060 --> 00:04:20,550
We're going to take that cards rank look it up in the values dictionary that we did at the very beginning

58
00:04:20,970 --> 00:04:25,530
and then we'll add that to our self-taught value which is just some number of what the current value

59
00:04:25,530 --> 00:04:26,240
of the hand is.

60
00:04:26,250 --> 00:04:31,940
And we know when self-taught values greater and 21 than whoever has this hand has lost.

61
00:04:31,940 --> 00:04:37,640
OK so let's do a little bit of testing before we tackle this issue of adjusting for an ace.

62
00:04:38,150 --> 00:04:44,310
So I'm going to alt enter here to create a new cell below to enter a couple of times.

63
00:04:44,360 --> 00:04:46,370
Create a couple of cells for us.

64
00:04:46,880 --> 00:04:53,960
Let's say that our test deck is equal to an instance of deck that I'm going to shuffle the deck.

65
00:04:54,110 --> 00:05:02,920
So say test deck called shuffle on it and now we're going to create a test player.

66
00:05:02,920 --> 00:05:08,730
So this is the human player and we'll be using a very similar thing for the computer where we say the

67
00:05:08,730 --> 00:05:14,330
Test player is now represented by this hand and the Test player starts off with no cards no values and

68
00:05:14,330 --> 00:05:21,090
choices then we'll say Test player and let's add a card to their hand.

69
00:05:23,060 --> 00:05:31,470
So to add a card to their hand we need to grab one from the deck so I can say that I pull the card from

70
00:05:31,470 --> 00:05:32,120
the deck.

71
00:05:32,260 --> 00:05:45,330
So puled card is equal to deck equal to test deck and then notice I can say deal so let's break this

72
00:05:45,330 --> 00:05:53,980
down with we find the player going to deal one card from the deck member that is a card object.

73
00:05:53,980 --> 00:05:57,840
So it's actually a card that has a suit and a ring to it.

74
00:05:59,130 --> 00:06:03,150
Let's go ahead and then print out that pooled card

75
00:06:06,270 --> 00:06:12,600
and then once we print out that pooled card I'm going to add that card to the test player's hand.

76
00:06:12,810 --> 00:06:20,940
So we'll say add in the pooled card there and then let's say What is the test player's current value

77
00:06:20,940 --> 00:06:24,160
will say Test player value.

78
00:06:24,240 --> 00:06:25,570
Just call that attribute.

79
00:06:25,680 --> 00:06:28,150
Let's run this.

80
00:06:28,270 --> 00:06:28,840
All right.

81
00:06:28,840 --> 00:06:34,690
So what happened was we created a deck we shuffled that deck we created a Test player represented as

82
00:06:34,690 --> 00:06:35,670
a hand object.

83
00:06:35,690 --> 00:06:38,550
So that test player's hand we got to test that.

84
00:06:38,590 --> 00:06:39,780
We did something out.

85
00:06:39,800 --> 00:06:42,710
Meaning we just got one single card printed that card.

86
00:06:42,740 --> 00:06:44,270
So reports back to spades.

87
00:06:44,440 --> 00:06:47,060
Remember each card has a string representation.

88
00:06:47,380 --> 00:06:52,210
And then we added that pooled card to the Test player and then we printed out the test player's value.

89
00:06:53,610 --> 00:06:56,250
Now we can actually do this in much fewer lines.

90
00:06:56,440 --> 00:07:01,190
What we can do is with that same test player right now I'm not going to rerun the cell.

91
00:07:01,540 --> 00:07:09,390
I will say add a card and inside of this number the pool card was just test deck that deal.

92
00:07:09,420 --> 00:07:16,000
So I can copy and paste it in here and that's going to be adding another card to the test player's hand.

93
00:07:16,040 --> 00:07:16,880
So I run that.

94
00:07:16,970 --> 00:07:24,910
And now let's check our test players current value and now it has a 12.

95
00:07:25,140 --> 00:07:30,580
And now we can do this over and over again to keep adding cards to this Test player sand.

96
00:07:30,750 --> 00:07:36,080
However Well we still need to do is adjust for the ACE OK.

97
00:07:36,080 --> 00:07:39,160
Now it's time to actually tackle the issue of the aces.

98
00:07:39,230 --> 00:07:45,050
First off let's take a look at our attribute self-taught aces psaltery aces remember allows us to actually

99
00:07:45,050 --> 00:07:47,990
keep track of the number of aces we have.

100
00:07:47,990 --> 00:07:49,270
So might be a good idea.

101
00:07:49,340 --> 00:07:56,320
Inside of this card method we can do is track our aces.

102
00:07:56,380 --> 00:08:01,750
What I will say is if the rank of that card is equal to ace

103
00:08:05,110 --> 00:08:10,440
then I will say self thought aces and add 1 to the count.

104
00:08:10,520 --> 00:08:13,160
So that means now aces is going to be zero.

105
00:08:13,280 --> 00:08:14,490
Or some other number.

106
00:08:14,810 --> 00:08:17,260
As I tracked the actual aces here.

107
00:08:17,270 --> 00:08:24,760
So if card is equal to ace then self-taught aces plus 1 then we need to adjust for ACE.

108
00:08:24,790 --> 00:08:27,350
So there's two things they're going to have to happen here.

109
00:08:27,370 --> 00:08:43,100
When you say while self value is greater than 21 and self-taught aces take a current value subtract

110
00:08:43,120 --> 00:08:51,980
10 from it and then take a current number of aces and subtract an ace from it.

111
00:08:52,020 --> 00:08:56,020
OK so this is some really clever code here which is why it's a little bit hard to read.

112
00:08:56,020 --> 00:08:58,930
So I want to break down what's actually happening here.

113
00:08:58,950 --> 00:09:02,250
We first have the check self-taught value grade and 21.

114
00:09:02,250 --> 00:09:08,670
That makes sense because we're not going to adjust an ace to be equal to one unless the value is over

115
00:09:08,670 --> 00:09:15,150
21 because if we take a look at our initial values dictionary when you first get an ace you're going

116
00:09:15,150 --> 00:09:17,880
to consider it to be 11.

117
00:09:17,900 --> 00:09:22,240
So keep that in mind what's going to happen is come back up here.

118
00:09:22,250 --> 00:09:25,840
When we first get that Ace we're going to consider it to be in 11.

119
00:09:25,880 --> 00:09:29,630
Then if we have that ace We want to adjust for it.

120
00:09:29,660 --> 00:09:40,720
So I'll say if total value greater than 21 and I still have an ace

121
00:09:44,240 --> 00:09:54,560
then change my ace to be a 1 instead of an 11.

122
00:09:54,560 --> 00:09:57,030
So that's the logic that we're trying to explain here.

123
00:09:57,320 --> 00:09:59,930
And that's what these three lines of code are doing.

124
00:10:00,150 --> 00:10:06,050
They're checking hey is my current value greater than 21 and self-taught aces this one.

125
00:10:06,080 --> 00:10:13,070
And self-taught aces is a little bit tricky because you're using an integer as what's known as truthiness.

126
00:10:13,070 --> 00:10:18,950
So this means that we're actually treating this aces integer as a boolean value and I want to show you

127
00:10:18,950 --> 00:10:20,520
what that looks like.

128
00:10:20,540 --> 00:10:24,460
Let's imagine they have some number equal to zero.

129
00:10:24,690 --> 00:10:33,430
And then let's actually just call it 0 and then say 1 is 1 2 is 2.

130
00:10:33,530 --> 00:10:36,230
If we check if these things are true or not.

131
00:10:36,260 --> 00:10:41,310
So if 0 prints

132
00:10:46,220 --> 00:10:48,420
those were happens we don't get anything printed out.

133
00:10:48,800 --> 00:10:53,900
So that means if 0 the actual integer it's treated as false.

134
00:10:54,170 --> 00:11:00,230
So 0 has a truthiness to it where it actually is being treated as false and other numbers that are not

135
00:11:00,230 --> 00:11:02,900
zero are treated as true.

136
00:11:03,230 --> 00:11:07,630
So you can think of zeros or any number besides zero is going to be printed is true.

137
00:11:07,640 --> 00:11:13,340
So this says if one print true says treating the integer one as true and it's going to continue for

138
00:11:13,340 --> 00:11:15,240
integers that are not zero.

139
00:11:15,500 --> 00:11:21,500
So we're able to take advantage of that in our call right here by saying and self-taught aces.

140
00:11:21,740 --> 00:11:26,350
The other we could do this is say and self-taught aces greater than zero.

141
00:11:26,420 --> 00:11:28,350
And that's pretty much the exact same thing.

142
00:11:28,400 --> 00:11:33,710
The code in the notebook just says self-dual aces because it's a kind of clever use case but it's the

143
00:11:33,710 --> 00:11:37,320
same thing to say and sulpha aces is greater than zero.

144
00:11:37,400 --> 00:11:42,980
So you can keep either version here but all the saying is while my value Grant's 21 and I still have

145
00:11:42,980 --> 00:11:49,830
aces then take my value subtract 10 from it and then take my aces and subtract one from them.

146
00:11:49,910 --> 00:11:53,520
Because now I've officially switched the ace that instead of it's an 11.

147
00:11:53,540 --> 00:12:00,810
It's not going to be a 1 OK let's rerun that and then move along to creating our chips class or chip

148
00:12:00,810 --> 00:12:03,400
class is actually going to be super simple.

149
00:12:03,420 --> 00:12:07,290
Basically what we do is we just have sum total of chips.

150
00:12:07,320 --> 00:12:13,830
And if you want well you could do is instead of just setting the total by default to be 100 you could

151
00:12:13,830 --> 00:12:18,200
say self total equal to 100.

152
00:12:18,210 --> 00:12:23,160
It's really up to you if you want it to be supplied by user input so they can say i start of 1000 chips

153
00:12:23,160 --> 00:12:28,830
or 5000 chips and then set total here or the way the note book shows is just automatically set equal

154
00:12:28,830 --> 00:12:29,680
to 100.

155
00:12:29,760 --> 00:12:35,050
Don't allow for people to kind of make up money for themselves then for the win bet.

156
00:12:35,280 --> 00:12:47,220
Well imagine that if they won a bet you will say self that total plus equal to whatever their bet was.

157
00:12:47,440 --> 00:12:54,280
And if they lose bet the reverse will say self-taught total minus self-taught.

158
00:12:55,690 --> 00:12:57,050
And that's chips that's all it is.

159
00:12:57,050 --> 00:13:01,840
We just keep track sum total and the current bet that they've placed and then we either subtract that.

160
00:13:01,960 --> 00:13:06,080
If they lose it or add that if they win it OK.

161
00:13:06,100 --> 00:13:08,080
So that's it for this lecture.

162
00:13:08,080 --> 00:13:12,360
Coming up next we're going to find some functions that will actually help us play the game.

163
00:13:12,670 --> 00:13:18,340
Let's quickly review what we've created so far with our hand class in our chips class our hand class

164
00:13:18,430 --> 00:13:23,730
is basically a representation of the player you know the computer dealer or the human player.

165
00:13:23,800 --> 00:13:25,240
They start off with cards.

166
00:13:25,330 --> 00:13:30,970
They have some value associated with their current hand and then they have some count on aces to add

167
00:13:30,970 --> 00:13:31,640
cards.

168
00:13:31,690 --> 00:13:36,820
They just pick a card from the deck append that to their current list of cards and then they adjust

169
00:13:36,820 --> 00:13:41,280
their value accordingly by looking up that card rank in the values dictionary.

170
00:13:41,320 --> 00:13:42,930
Then they also keep track of aces.

171
00:13:43,090 --> 00:13:46,220
So they do an additional check if that card rank is equal to Ace.

172
00:13:46,270 --> 00:13:49,670
They add an ace number to it just count 1.

173
00:13:49,760 --> 00:13:55,700
Then we can adjust for Ace which means if the self-taught value of my hand is currently great and 21

174
00:13:56,300 --> 00:14:01,630
and I still have Aces I'm going to adjust the ace to reduce the value to be a 1.

175
00:14:01,670 --> 00:14:08,950
So go from 11 to 1 we subtract 10 and then subtract 1 from ace count and then I've done the adjustment.

176
00:14:09,020 --> 00:14:09,260
All right.

177
00:14:09,260 --> 00:14:15,590
Coming up next we're going to go along with Step Six we're write functions for actually playing the

178
00:14:15,590 --> 00:14:16,280
game.

179
00:14:16,340 --> 00:14:20,720
The chips class itself was just adding and subtracting some from the total.

180
00:14:20,720 --> 00:14:21,790
We'll see you at the next lecture.
