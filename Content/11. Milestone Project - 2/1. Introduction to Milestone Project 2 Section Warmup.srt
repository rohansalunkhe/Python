1
00:00:05,340 --> 00:00:08,220
Welcome, everyone, to your second milestone project.

2
00:00:08,880 --> 00:00:12,780
In this section of the course, since you've learned about object oriented programming, we're going

3
00:00:12,780 --> 00:00:17,280
to test your new skills with another milestone project, just like we had a milestone project after

4
00:00:17,280 --> 00:00:18,240
learning about functions.

5
00:00:18,570 --> 00:00:20,650
We now have one after learning about OPIS.

6
00:00:21,300 --> 00:00:26,010
But first, just like the last milestone project to warm up, we're first going to guide you through

7
00:00:26,010 --> 00:00:31,080
creating a simple card game, and then you're gonna attempt to actually complete the second milestone

8
00:00:31,080 --> 00:00:32,190
project exercise.

9
00:00:33,280 --> 00:00:37,270
So let's talk about your warm up project, which is just a guided Kotal on project.

10
00:00:38,750 --> 00:00:43,520
To warm up for that second milestone project, we're going to recreate the card game called War and

11
00:00:43,520 --> 00:00:45,410
we're going to do it together in a code along.

12
00:00:45,860 --> 00:00:47,960
Let me have a quick overview of how this game works.

13
00:00:47,990 --> 00:00:52,460
In case you haven't heard about it, but in case you have heard about it or just need a quick refresher,

14
00:00:52,520 --> 00:00:57,260
rather read the rules rather than watch this, you can just check out this Wikipedia link on the actual

15
00:00:57,260 --> 00:00:57,770
card game.

16
00:00:59,030 --> 00:01:03,800
But as a quick, brief overview of how the game war works and how we're gonna program it, we essentially

17
00:01:03,800 --> 00:01:05,600
start off with a 52 card deck.

18
00:01:06,350 --> 00:01:09,830
And then after shuffling the deck, we split it up into two decks.

19
00:01:09,860 --> 00:01:11,270
So half goes to one player.

20
00:01:11,540 --> 00:01:12,740
Half goes to the second player.

21
00:01:13,590 --> 00:01:17,300
And now these two players will grab one of their cards and lay on the table.

22
00:01:17,630 --> 00:01:22,850
So, for example, we have Jack versus a King and the card that has the higher ranking wins.

23
00:01:23,120 --> 00:01:28,220
And when a player wins this matchup, they take both cards and they go in to their deck.

24
00:01:28,640 --> 00:01:32,450
And the player that loses is the one that has no more cards remaining.

25
00:01:33,200 --> 00:01:38,360
Now, there is a caveat here, and that's when you pull out the cards and they happen to be the exact

26
00:01:38,360 --> 00:01:38,960
same value.

27
00:01:39,410 --> 00:01:42,460
So here player one player to have both pulled out a jack.

28
00:01:42,510 --> 00:01:43,610
Those are equal to each other.

29
00:01:44,060 --> 00:01:47,810
This is an instance of war and that's where the card game gets its name from.

30
00:01:48,560 --> 00:01:53,570
So in a war situation, we'll go ahead and then take out an additional set of cards.

31
00:01:53,900 --> 00:01:56,060
And this varies depending on what variation you're playing.

32
00:01:56,330 --> 00:01:57,560
Some people take out three cards.

33
00:01:57,560 --> 00:01:59,180
Some people take out an extra five cards.

34
00:01:59,480 --> 00:02:04,760
But essentially the point being is you're gonna take out an extra set of cards and set aside along those

35
00:02:04,760 --> 00:02:08,120
matching jacks and then you pull out a new set of cards.

36
00:02:08,480 --> 00:02:13,490
So here now, drawing again from their decks, player one now pulls out a jack platoon up, pulls out

37
00:02:13,490 --> 00:02:13,910
a king.

38
00:02:14,360 --> 00:02:15,260
King wins again.

39
00:02:15,590 --> 00:02:17,510
And now player two has won the war.

40
00:02:17,810 --> 00:02:20,210
And so they get all those cards pass into their deck.

41
00:02:20,570 --> 00:02:24,740
And these cycles end up repeating over and over again a one to one comparison of cards.

42
00:02:25,010 --> 00:02:29,780
And if they happen to be equal, then you get a war, which means you draw another set of cards, basically

43
00:02:29,780 --> 00:02:32,090
a pile of three for each, and then you draw again.

44
00:02:32,420 --> 00:02:34,640
So when you win a war, you want a lot more cards.

45
00:02:35,600 --> 00:02:38,270
And this continues until some player has all the cards.

46
00:02:38,300 --> 00:02:40,700
So in this case, a player two has in theory one.

47
00:02:42,060 --> 00:02:46,620
OK, so like I mentioned, that war process can be repeated as many times as necessary.

48
00:02:46,890 --> 00:02:52,140
In case of back to back ties, you can have like a double or triple war now to construct this game.

49
00:02:52,150 --> 00:02:55,140
But we're going to do is use Python's object oriented programming.

50
00:02:55,590 --> 00:03:01,770
And we're also going to be exploring how classes can actually be connected to other classes, not just

51
00:03:01,770 --> 00:03:03,300
through something like inheritance.

52
00:03:03,660 --> 00:03:08,820
So we're gonna start off with a card class, which is going to just hold the entities or attributes

53
00:03:08,880 --> 00:03:11,620
of a single card like it, suit, rank and value.

54
00:03:12,150 --> 00:03:17,370
And then we'll have a deck class, which is actually constructed using instances of the card class.

55
00:03:18,000 --> 00:03:23,100
And then what we're gonna do is also create a player class that can then hold cards are called hold

56
00:03:23,370 --> 00:03:24,390
half a deck of cards.

57
00:03:24,780 --> 00:03:29,160
And then finally, this is actually usually the hardest part is construct these classes in a way and

58
00:03:29,160 --> 00:03:33,240
create instance of these classes in a way where we can actually perform the game logic.

59
00:03:33,690 --> 00:03:36,390
And in our case, we're not actually going to have a human player.

60
00:03:36,700 --> 00:03:40,650
Instead, we're going to just be simulating a game of war between two computer players.

61
00:03:41,010 --> 00:03:44,880
So the actual playing will happen between two virtual agents.

62
00:03:45,360 --> 00:03:45,570
All right.

63
00:03:46,140 --> 00:03:47,340
Let's go ahead and get started.

64
00:03:47,670 --> 00:03:48,630
I'll see at the next lecture.
