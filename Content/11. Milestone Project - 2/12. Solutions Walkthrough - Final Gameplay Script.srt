1
00:00:05,420 --> 00:00:08,910
All right let's get started with actually programming out the game.

2
00:00:09,080 --> 00:00:11,580
We're going to print some opening statement in this case.

3
00:00:11,580 --> 00:00:16,060
We'll do something simple like Welcome to Black Jack.

4
00:00:16,280 --> 00:00:18,450
And then we want to create and shuffle the deck.

5
00:00:18,630 --> 00:00:23,540
So I'll say deck is equal to an instance of a deck object.

6
00:00:24,820 --> 00:00:29,560
And then we'll grab that deck and we will shuffle it.

7
00:00:29,680 --> 00:00:37,030
After that we want to set up the players themselves so set up two hands for each player or I should

8
00:00:37,030 --> 00:00:39,660
say a hand per player hand per dealer.

9
00:00:39,700 --> 00:00:46,860
So player hands is equal to an instance of the hand and we're going to grab that player hand and then

10
00:00:46,960 --> 00:00:48,010
add a card to it.

11
00:00:49,630 --> 00:00:53,600
Then the card itself is going to be just dealing from that deck.

12
00:00:53,650 --> 00:01:02,740
So I'll say deck that deal and we're going to do this twice and we actually do a very similar thing

13
00:01:02,740 --> 00:01:04,300
for the dealer.

14
00:01:04,360 --> 00:01:06,160
So we want to copy and paste this code.

15
00:01:06,640 --> 00:01:09,490
Except instead of the player hand it will be the dealer hand.

16
00:01:11,850 --> 00:01:16,150
So say dealer create a hand out of cards did this twice.

17
00:01:16,380 --> 00:01:17,540
So who created the deck.

18
00:01:17,540 --> 00:01:23,550
And we had now we have a player hand and a dealer hand up next the set up the players trips.

19
00:01:23,550 --> 00:01:32,100
So for this will say player chips is equal to chips object.

20
00:01:32,100 --> 00:01:34,120
Remember the default value is 100.

21
00:01:34,230 --> 00:01:39,010
But you could adjust this depending on how you define the actual chips object above.

22
00:01:39,160 --> 00:01:42,630
Then Up next we want to prompt the player for their bet.

23
00:01:42,690 --> 00:01:45,480
So we already have to take that function that does this for us.

24
00:01:45,480 --> 00:01:49,770
We say take bet and we're going to pass in the player's chips.

25
00:01:51,590 --> 00:01:53,270
And then we want to show cards.

26
00:01:53,390 --> 00:01:55,490
So in this case is still the beginning of the game.

27
00:01:55,580 --> 00:02:02,280
So we'll only call the show some function will say show some and then we'll pass in the player hand.

28
00:02:04,290 --> 00:02:11,050
The dealer hand and then we'll also pass and actually that's all we need a person for show some and

29
00:02:11,050 --> 00:02:12,100
then while playing.

30
00:02:12,100 --> 00:02:16,900
Recall this variable from our hit or stand functions is a global variable here.

31
00:02:16,930 --> 00:02:20,610
So while we're playing we're going to ask the player do they want to hit or stand.

32
00:02:20,620 --> 00:02:27,340
So we actually have this stand that takes and the deck we created as well as the player hand that you

33
00:02:27,340 --> 00:02:30,290
want to show the cards so the player is still playing.

34
00:02:30,520 --> 00:02:35,020
We're going to say show some again and it's actually Same here as above.

35
00:02:35,020 --> 00:02:40,480
Show the player hand and the dealer hand we will copy this and paste it in.

36
00:02:40,540 --> 00:02:46,770
At the player's hand exceeds twenty one we're going to check run player bus.

37
00:02:46,900 --> 00:02:54,840
So we'll see if the player hand I should say the value of the player has an attribute.

38
00:02:55,110 --> 00:02:58,770
If it's greater than 21 than this player has busted and they've lost.

39
00:02:58,770 --> 00:03:06,490
So we will call player bus an impasse and they need information including the player hand the dealer

40
00:03:06,490 --> 00:03:07,120
hand.

41
00:03:08,380 --> 00:03:14,290
And then the player chips after that we can just break.

42
00:03:14,290 --> 00:03:16,350
So let's go over what we've done so far.

43
00:03:16,510 --> 00:03:18,040
We set up the deck.

44
00:03:18,040 --> 00:03:22,300
We set up the players and we set up the dealer and we set up the players chips.

45
00:03:22,300 --> 00:03:27,400
We've taken a bet from the player and then we show some of the cards basically show some means still

46
00:03:27,400 --> 00:03:30,760
show that dealers hand as one of the cards face down.

47
00:03:30,760 --> 00:03:33,940
Then while playing we'll say or stand for that player.

48
00:03:34,120 --> 00:03:36,930
We'll show some of the cards to make sure that lines up there.

49
00:03:37,330 --> 00:03:43,790
And then if that player's hand value is greater than 21 we're going to bust out of that.

50
00:03:43,840 --> 00:03:46,520
Now for the next step we want to check if the player hasn't busted.

51
00:03:46,660 --> 00:03:50,980
We're going to play the dealer's hand until the dealer reaches 17 and I'm going to explain that in just

52
00:03:50,980 --> 00:03:51,700
a second.

53
00:03:51,830 --> 00:03:53,670
But let's write out the code for it.

54
00:03:53,830 --> 00:03:59,080
We'll see if the player hand value is less than or equal to 21.

55
00:03:59,080 --> 00:04:00,740
That means the player hasn't busted.

56
00:04:01,360 --> 00:04:11,960
And we're going to say while the dealer hand value is less than 17 go ahead and hit using the deck and

57
00:04:11,960 --> 00:04:12,670
the dealer hand

58
00:04:15,710 --> 00:04:19,820
something I want to know is it depends on how you want to set up your game whether or not you want to

59
00:04:19,820 --> 00:04:21,470
use the value 17 here.

60
00:04:21,650 --> 00:04:26,300
If you're familiar with the game of blackjack in a casino they have this rule called soft 17 that the

61
00:04:26,300 --> 00:04:29,990
dealer only hits until they reach 17.

62
00:04:29,990 --> 00:04:36,950
Now the way you could have also structured it is say while dealer hand value is less then the player

63
00:04:36,950 --> 00:04:38,870
hand value to keep on hitting.

64
00:04:38,930 --> 00:04:43,490
It's up to you whether you want to do this soft 17 rule or if you want to just keep going until you

65
00:04:43,490 --> 00:04:45,880
either beat the player or bust.

66
00:04:45,890 --> 00:04:48,060
So either one of those methods is OK.

67
00:04:48,110 --> 00:04:53,390
Again it really depends on how much the detail of the game blackjack you want to go into right now really

68
00:04:53,390 --> 00:04:56,470
just focus on coding so either one of the options is fine.

69
00:04:57,150 --> 00:05:03,320
So since the dealer is now playing we need to show all the dealer cards he will say show all and we'll

70
00:05:03,320 --> 00:05:06,960
say play your hand and the dealer hand.

71
00:05:07,240 --> 00:05:09,600
And then just using tabs autocomplete there.

72
00:05:09,920 --> 00:05:13,940
Then we run the different situations where someone may win or lose.

73
00:05:13,970 --> 00:05:22,010
So if the dealer's hand value is greater than 21 then the dealer has busted.

74
00:05:22,040 --> 00:05:31,410
So we say dealer bus and we pass in the player hand the dealer hands and the player chips

75
00:05:35,440 --> 00:05:43,970
Elif the dealer hands value is greater then the player hands value.

76
00:05:44,220 --> 00:05:45,630
Then the dealer has one.

77
00:05:45,630 --> 00:05:52,640
So will say a dealer wins and we pass in the same three parameters as before so it's copy and paste

78
00:05:52,640 --> 00:05:54,920
them in LCF.

79
00:05:54,990 --> 00:05:59,250
We have another situation checks we'll see if the dealer hand value

80
00:06:01,980 --> 00:06:06,430
is less then the player hand value.

81
00:06:06,560 --> 00:06:10,580
And this is more dependent on that soft 17 rule because it kind of depends if you initiate.

82
00:06:10,580 --> 00:06:15,540
This is 17 then this situation may come up if you just keep going until you bust.

83
00:06:15,550 --> 00:06:17,500
Then this situation will ever come up.

84
00:06:18,580 --> 00:06:20,000
But we'll still person in here.

85
00:06:20,020 --> 00:06:26,210
So we match up the notes I'll say player wins the dealer hand values less than the player had a value.

86
00:06:27,000 --> 00:06:32,400
And again we just pass and the player hands the dealer hand the player chips else.

87
00:06:32,650 --> 00:06:38,080
And then finally we check the situation for a push where they both happen to get 21 and they tied up

88
00:06:38,080 --> 00:06:45,670
with a push on the player hand and the dealer hands which are the functions we find before that we want

89
00:06:45,670 --> 00:06:47,520
to inform the player of the remaining chips.

90
00:06:47,530 --> 00:06:54,160
So we're going say Prince we'll say print a new line just so we get some space here.

91
00:06:54,170 --> 00:07:07,500
Players total chips are at will and the string there and format in player chips total attribute.

92
00:07:07,660 --> 00:07:12,890
And then finally we'll just have some little small loop here to ask them if they want to play again.

93
00:07:13,030 --> 00:07:17,430
So I'll say a new game is equal to input.

94
00:07:17,570 --> 00:07:21,720
Would you like to play in other hands.

95
00:07:21,950 --> 00:07:23,440
Well say yes or no.

96
00:07:24,810 --> 00:07:30,730
And if the results of the new game if that first letter let's say if that first letter to lowercase

97
00:07:33,490 --> 00:07:41,480
is equal to a Y we'll set playing equal to true and then it's the player's turn again and we'll continue

98
00:07:41,480 --> 00:07:42,500
along.

99
00:07:43,100 --> 00:07:52,070
Else we're going to do is say Prince thank you for playing and then we'll break out of that entire loop.

100
00:07:52,610 --> 00:07:52,880
OK.

101
00:07:52,880 --> 00:07:54,840
So we just did a lot of code here.

102
00:07:54,950 --> 00:07:59,710
So let's zoom out and run through all of the steps here and then we'll debug.

103
00:07:59,720 --> 00:08:01,960
Just in case we have any typos.

104
00:08:02,090 --> 00:08:07,700
So the game you say While true we print out the blackjack This is the pretty straightforward stuff.

105
00:08:07,700 --> 00:08:09,340
We create a deck we shuffle it.

106
00:08:09,500 --> 00:08:13,600
Then we create a hand object to represent the player add two cards of that hand.

107
00:08:13,820 --> 00:08:18,370
Then we create a hand object to represent dealer's hand add two cards to that dealer's hand.

108
00:08:19,400 --> 00:08:25,340
We said at the player's chips then we take a bet from the player and then we show some of the cards

109
00:08:25,340 --> 00:08:30,320
so we say player hand and dealer hand and show some means that the dealer's hand is still hit and then

110
00:08:30,320 --> 00:08:32,560
we have this global variable called playing.

111
00:08:32,660 --> 00:08:35,590
And while we're still playing that is the human player still playing.

112
00:08:35,660 --> 00:08:38,180
We ask them if they want to hit or stand.

113
00:08:38,270 --> 00:08:41,510
Then we show some of the cards to the dealers card is still hidden.

114
00:08:41,690 --> 00:08:43,910
And then we check the player buster.

115
00:08:44,360 --> 00:08:46,790
If they bust it then we just break out of this.

116
00:08:46,980 --> 00:08:51,560
If the player hasn't busted that means their value is still less and equal to 21 we check while the

117
00:08:51,560 --> 00:08:52,520
dealer hands value.

118
00:08:52,550 --> 00:08:57,140
And you can say either less than the player hand value or less than 17 depending on how you actually

119
00:08:57,140 --> 00:08:58,590
want to implement that rule.

120
00:08:58,610 --> 00:09:03,290
You'll keep hitting then you're going to show all the cards and then you'll check for the different

121
00:09:03,290 --> 00:09:04,520
situations.

122
00:09:04,520 --> 00:09:09,730
Either the dealer is busting the dealer or wins the player wins or it's a push.

123
00:09:09,770 --> 00:09:11,890
We informed the player what the total chips are.

124
00:09:11,900 --> 00:09:13,280
We ask them if they want to play again.

125
00:09:13,400 --> 00:09:17,600
And then we have this little loop that will either reset the plane to true or will break out of this

126
00:09:17,600 --> 00:09:18,650
while loop.

127
00:09:18,650 --> 00:09:21,820
Let's run this and see if we get any errors.

128
00:09:21,880 --> 00:09:23,480
Any tips what I like to bet.

129
00:09:23,510 --> 00:09:24,370
First off let's bet.

130
00:09:24,370 --> 00:09:29,240
Way too many chips and see if we get that Err so sorry you don't have enough chips.

131
00:09:29,260 --> 00:09:30,240
You have 100.

132
00:09:30,360 --> 00:09:30,620
OK.

133
00:09:30,640 --> 00:09:31,850
So far so good.

134
00:09:31,990 --> 00:09:35,250
Let's bet 50 chips dealer hand.

135
00:09:35,260 --> 00:09:37,960
We get one card hit in and they have the ten of hearts.

136
00:09:37,960 --> 00:09:40,360
I have the Queen of Hearts and the Queen of Spades.

137
00:09:40,500 --> 00:09:43,820
Remember that total value is 20 because it's 10 plus 10.

138
00:09:43,840 --> 00:09:47,030
So I'm definitely going to stand when to enter here.

139
00:09:47,110 --> 00:09:52,280
So I stood and then I have dealer's hand Ace of Hearts ten of hearts.

140
00:09:52,330 --> 00:09:53,610
So that's a straight 21.

141
00:09:53,620 --> 00:09:55,070
That's pretty freakin lucky.

142
00:09:55,220 --> 00:09:58,430
But it looks like we lost even a great hand at 20.

143
00:09:58,450 --> 00:10:00,710
So I have total chips or 50.

144
00:10:00,900 --> 00:10:01,880
I want to play another hand.

145
00:10:01,910 --> 00:10:02,650
Yes let's do it.

146
00:10:02,710 --> 00:10:04,840
Let's see how good our luck is welcome the blackjack.

147
00:10:04,840 --> 00:10:06,330
How many chips would you like to bet.

148
00:10:06,340 --> 00:10:09,030
Let's see if I bet 100 again what happens.

149
00:10:09,040 --> 00:10:13,730
So no way but 100 actually don't get an error because being reset the way we designed it.

150
00:10:13,820 --> 00:10:16,730
So scroll up real quick just to show you this.

151
00:10:16,780 --> 00:10:24,720
So each time you call chips here it's being reset because of the way we define chips all the way back

152
00:10:24,990 --> 00:10:25,550
up here.

153
00:10:25,590 --> 00:10:27,980
We said by default the total is equal to 100.

154
00:10:28,200 --> 00:10:34,770
Well we could have done is kept going with some sort of recurring chips object and not have it reset

155
00:10:34,770 --> 00:10:35,620
100.

156
00:10:35,640 --> 00:10:36,400
It's up to you.

157
00:10:36,420 --> 00:10:43,470
You want to think about this but you could assign chips instead to maybe a player so let's continue

158
00:10:43,470 --> 00:10:43,940
along.

159
00:10:43,950 --> 00:10:46,190
We basically reset our number of chips.

160
00:10:46,530 --> 00:10:51,690
We'll come back down here and let's check what we have player sand is an 18 on the stand again.

161
00:10:51,690 --> 00:10:53,480
That's a pretty good handstand.

162
00:10:54,180 --> 00:10:59,700
And we have a dealer Ceann was up to six of clubs and an ace of diamonds.

163
00:10:59,730 --> 00:11:00,740
That's 19.

164
00:11:00,840 --> 00:11:02,710
Dealer wins again player chips total.

165
00:11:02,730 --> 00:11:08,340
Let's say Yes I want to play again and I'm going to just put in a bet here.

166
00:11:08,550 --> 00:11:10,090
One card hit in ten of diamonds.

167
00:11:10,110 --> 00:11:12,060
I have five of diamonds queen of hearts.

168
00:11:12,070 --> 00:11:15,130
Seconds tell this game is definitely favorite towards the dealer.

169
00:11:15,150 --> 00:11:24,400
Let's hit just so we can practice hitting and I have five 10 an ace so that's 16.

170
00:11:24,430 --> 00:11:28,890
That's pretty lucky we'll stand there and scroll all the way down you notice in Juber notebook if you

171
00:11:28,890 --> 00:11:30,510
have too much.

172
00:11:30,510 --> 00:11:32,880
So looks like the dealer ended up busting.

173
00:11:32,880 --> 00:11:34,090
So we won.

174
00:11:34,200 --> 00:11:36,210
So the other hand was too much.

175
00:11:36,210 --> 00:11:38,880
It kept going and now we're at 120.

176
00:11:38,880 --> 00:11:40,210
What I like to play another hand.

177
00:11:40,230 --> 00:11:41,280
No thank you.

178
00:11:41,310 --> 00:11:42,250
Say no and no.

179
00:11:42,300 --> 00:11:43,360
Thank you for playing.

180
00:11:43,730 --> 00:11:44,300
OK.

181
00:11:44,460 --> 00:11:45,970
That's it for the entire game.

182
00:11:45,970 --> 00:11:47,350
That's definitely a lot of code.

183
00:11:47,400 --> 00:11:50,610
And keep in mind your solution could look way different than this.

184
00:11:50,610 --> 00:11:54,930
There's basically an infinite number of ways you could have program that this game you could've made

185
00:11:54,930 --> 00:12:00,840
more classes used more functions assigned maybe a player class and a dealer class instead of just hands

186
00:12:01,170 --> 00:12:02,070
for them both.

187
00:12:02,070 --> 00:12:06,940
So it's really up to you the flexibility here if you have any questions feel free to post the CUNY forums.

188
00:12:07,050 --> 00:12:09,910
But this basically concludes the second milestone project.

189
00:12:09,990 --> 00:12:11,330
We'll see you at the next lecture.
