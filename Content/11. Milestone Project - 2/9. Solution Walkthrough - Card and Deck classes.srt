1
00:00:05,360 --> 00:00:06,370
Welcome back everyone.

2
00:00:06,460 --> 00:00:11,210
And this lecture we're going start going through the walk through steps workbook solution.

3
00:00:11,210 --> 00:00:14,090
I wanted to keep in mind this is just an example solution.

4
00:00:14,100 --> 00:00:17,960
There's basically an unlimited number of ways you could have gone through this project.

5
00:00:18,090 --> 00:00:20,840
This case we're going to go through two steps in the workbook.

6
00:00:20,840 --> 00:00:21,840
Let's get started.

7
00:00:22,130 --> 00:00:28,070
OK so here we are at the workbook lots of different things to get started with on the gameplay aspect.

8
00:00:28,130 --> 00:00:32,930
We're going to do and this lecture is just go through these various steps and then fill out all the

9
00:00:32,930 --> 00:00:37,430
cells and see if we can create some logic at the end to actually get the game running.

10
00:00:37,850 --> 00:00:43,020
So here we see the game and we want to set some imports and global variables so global variables are

11
00:00:43,040 --> 00:00:46,930
variables we're going to be able to reference in other functions and objects.

12
00:00:47,210 --> 00:00:50,390
So we first wanted to import the ran the module that was actually already done for us.

13
00:00:50,450 --> 00:00:53,530
And that's going to allow us to shuffle the deck prior to dealing.

14
00:00:53,570 --> 00:00:58,370
And you can check out the useful operators lecture if you want to review the how to shuffle from the

15
00:00:58,370 --> 00:00:59,440
random library.

16
00:00:59,660 --> 00:01:05,860
But we wanted to know is to clear a couple of variables to store the card suits the ranks and the values.

17
00:01:05,930 --> 00:01:11,570
And you can do your own system but we actually have a lot of the code here for you where suits are going

18
00:01:11,570 --> 00:01:16,600
to set it as a tuple site going to copy and paste this here.

19
00:01:16,810 --> 00:01:22,250
And then the rinks we have two three four five six all the way through jack queen king ace.

20
00:01:22,510 --> 00:01:23,920
So we can set that up as well.

21
00:01:24,040 --> 00:01:25,560
Again you don't have to do it this way.

22
00:01:25,560 --> 00:01:30,550
This is just so to follow our system along the workbook and then probably the most important part was

23
00:01:30,550 --> 00:01:36,310
this values and this values is a nice dictionary where you can passen the rank of a card and then get

24
00:01:36,310 --> 00:01:38,010
out its actual numerical value.

25
00:01:38,020 --> 00:01:42,520
This is really useful to us because when we're dealing cards like Jack Queen King we're not going to

26
00:01:42,520 --> 00:01:47,200
be able to directly read off the number itself such as 2 3 4.

27
00:01:47,320 --> 00:01:52,030
So we can do is to use the dictionary as a look up so we can enter jack into the values dictionary and

28
00:01:52,030 --> 00:01:53,080
get back 10.

29
00:01:53,230 --> 00:01:54,980
We'll say Ace right now to 11.

30
00:01:55,000 --> 00:01:59,460
But later on we'll deal that special factor where it can be either a 1 or 11.

31
00:01:59,530 --> 00:02:03,840
So we're going to copy this and also paste this here.

32
00:02:04,020 --> 00:02:08,760
And then finally we're going to declare a boolean value to be used for controlling the while loop just

33
00:02:08,760 --> 00:02:10,710
like we did in that Ticketek to project.

34
00:02:10,830 --> 00:02:14,280
It might be nice to have a while loop that's dependent on some variables such as.

35
00:02:14,280 --> 00:02:14,990
Game on.

36
00:02:14,990 --> 00:02:17,660
And in this case we'll set it as playing as equal to true.

37
00:02:17,700 --> 00:02:24,320
So later on at the end we can set playing to false if the game is over going to run that sell and move

38
00:02:24,320 --> 00:02:26,360
on to the class definitions.

39
00:02:26,360 --> 00:02:32,110
So we want to do now was create a card class and the card object actually really only needs two attributes

40
00:02:32,140 --> 00:02:38,240
the way we're going to set it up here it just needs a suit and the rank and then we want to have a string

41
00:02:38,240 --> 00:02:42,860
method on it so that when we actually print a single card we get back something in the form of two of

42
00:02:42,860 --> 00:02:44,690
hearts or three of clubs.

43
00:02:44,690 --> 00:02:50,390
So let's set this up here we can see the init method and we're going to add in two attributes suits

44
00:02:50,570 --> 00:02:57,870
in Reinke and then we'll assign those self-taught suit is equal to suit softball.

45
00:02:57,880 --> 00:03:02,030
Rank is equal to rank and then for the string representation.

46
00:03:02,170 --> 00:03:09,210
Again you have a lot of flexibility here but we're going to return something like self-taught rank concatenated

47
00:03:09,210 --> 00:03:17,720
with of notice my space their concatenated were self-taught SU So we run that and now we have a basic

48
00:03:17,720 --> 00:03:23,240
card class and I want to note that it's OK if you either have open Glos princes here or no open close

49
00:03:23,240 --> 00:03:24,000
parentheses.

50
00:03:24,020 --> 00:03:26,610
It's only when you're using inheritance that you really need them.

51
00:03:26,870 --> 00:03:31,220
I usually prefer having the princes there the note book itself doesn't have them fully up to you which

52
00:03:31,220 --> 00:03:33,370
styling prefer.

53
00:03:33,410 --> 00:03:35,960
And now we move on to Step Three.

54
00:03:36,110 --> 00:03:42,920
So for Step 3 this is where we wanted to actually store the 52 card objects into some sort of list that

55
00:03:42,920 --> 00:03:44,970
we could later shuffle around.

56
00:03:44,980 --> 00:03:49,250
You know there's already a lot of code that's built out for you here so we can see what the code is

57
00:03:49,250 --> 00:03:50,000
doing.

58
00:03:50,090 --> 00:03:53,350
We set self deck as an empty list.

59
00:03:53,480 --> 00:03:59,930
And what's important here to note is even though this deck object has this attribute self-taught deck

60
00:04:00,110 --> 00:04:01,620
you can call it really whatever you want.

61
00:04:01,880 --> 00:04:07,080
It's not taking it in as a parameter when you actually first initialize instance of the deck.

62
00:04:07,340 --> 00:04:11,480
And that's because when you initialize a deck of cards you want it to be the same every time.

63
00:04:11,480 --> 00:04:16,070
You don't want the user to be able to input a parameter that would make something different with the

64
00:04:16,070 --> 00:04:16,560
deck.

65
00:04:16,620 --> 00:04:19,310
Instead a deck be a standardized thing.

66
00:04:19,310 --> 00:04:26,750
So we do here is we say set the deck is an empty list and the for every suit in suits member suits come

67
00:04:26,750 --> 00:04:27,610
back up here.

68
00:04:27,650 --> 00:04:28,920
That's a global variable.

69
00:04:29,030 --> 00:04:32,760
So we're going to be able to access it very suit in suits.

70
00:04:32,770 --> 00:04:38,060
And then for every rank in ranks we're going to do is like the instructions say here is actually append

71
00:04:38,090 --> 00:04:44,970
these using the card class will say self-taught deck.

72
00:04:45,540 --> 00:04:52,000
So remember that's an empty list and then I'm going to append using the card class that we defined above.

73
00:04:52,010 --> 00:04:56,150
Remember the card class just takes in a suit and a rank.

74
00:04:56,270 --> 00:04:57,690
We're going to pass this in as well.

75
00:04:57,770 --> 00:04:59,490
Suit Rick.

76
00:04:59,780 --> 00:05:04,220
And now after initialize this I should have a list of a bunch of card classes

77
00:05:07,350 --> 00:05:12,240
now for the actual string representation of this it's really up to you how you want to represent your

78
00:05:12,240 --> 00:05:13,100
deck.

79
00:05:13,200 --> 00:05:19,220
In our case we decided to do something like this or remember this function only really gets called if

80
00:05:19,220 --> 00:05:21,660
you actually print out your deck.

81
00:05:21,760 --> 00:05:33,250
So we'll start off with that composition as an empty string I'll say for every card in self-taught deck.

82
00:05:33,380 --> 00:05:41,820
We're going to say that the deck composition we're going to concatenate a new line make sure our quotes

83
00:05:41,830 --> 00:05:50,690
there plus the string representation of the card room or card here has its own string representation

84
00:05:50,820 --> 00:05:53,620
and we can call that using the following method.

85
00:05:53,780 --> 00:06:01,750
We can say that composition new line plus card and then string and to actually execute it.

86
00:06:01,980 --> 00:06:07,470
We have open close princes here which basically just calls Prince out the actual Stringer presentation

87
00:06:07,650 --> 00:06:09,050
of each individual card.

88
00:06:09,330 --> 00:06:15,020
So now we have this giant string of new lines and at the end we can just return back the string that

89
00:06:15,020 --> 00:06:26,690
says the deck has an ad in this composition that we can shuffle the day we actually should already here

90
00:06:26,690 --> 00:06:27,110
for you.

91
00:06:27,110 --> 00:06:32,150
But using that random library that we imported earlier we just call shuffle on the list and it will

92
00:06:32,150 --> 00:06:33,040
shuffle that list.

93
00:06:33,050 --> 00:06:35,710
Notice here I'm not saying return.

94
00:06:35,720 --> 00:06:37,730
This actually happens in place.

95
00:06:37,730 --> 00:06:40,270
So this goes back to our discussion of object oriented programming.

96
00:06:40,430 --> 00:06:43,160
We're not every single method actually return something.

97
00:06:43,190 --> 00:06:45,810
And this case is shuffling that list in place.

98
00:06:45,830 --> 00:06:51,950
Finally I want to have a deal method which is a way to actually grab a card from myself but that attribute

99
00:06:53,130 --> 00:06:54,930
that list of card objects.

100
00:06:55,360 --> 00:07:03,680
So say a single card is just equal to self-taught desktop pop and this sort of syntax can be confusing

101
00:07:03,680 --> 00:07:04,360
for beginners.

102
00:07:04,370 --> 00:07:06,130
So I want to take a closer look at it.

103
00:07:06,170 --> 00:07:15,470
All we're saying here is basically two things grab the deck attribute of this that class and then pop

104
00:07:15,560 --> 00:07:20,330
off that card item from that list and then set that to single card.

105
00:07:20,360 --> 00:07:24,380
What's confusing sometimes for beginners is that they see these two dots in a row which is a little

106
00:07:24,380 --> 00:07:25,970
weird if you haven't seen it before.

107
00:07:26,060 --> 00:07:33,260
But in this case it is just doing one single method off a list object just like we did up here where

108
00:07:33,260 --> 00:07:35,420
he said append office.

109
00:07:35,450 --> 00:07:38,370
Self-taught that list attributes which is this.

110
00:07:38,720 --> 00:07:44,490
Once you pop off that single card you can just return it and technically they didn't have to do this

111
00:07:44,580 --> 00:07:46,330
in two lines you could just set a return.

112
00:07:46,430 --> 00:07:47,610
Self-taught that pop.

113
00:07:47,630 --> 00:07:50,880
So hopefully this commentary helps you understand what's actually happening here.

114
00:07:51,940 --> 00:07:53,990
So now we have our tech object ready to go.

115
00:07:54,280 --> 00:07:55,670
So let's do a little bit of testing.

116
00:07:55,720 --> 00:08:01,080
We should be able to create a test there and then print out the test that we should then print out the

117
00:08:01,100 --> 00:08:04,630
deck has and this deck composition that we find up here.

118
00:08:04,630 --> 00:08:09,700
So let's run this and see that it has two of three of hearts four of hearts five of hearts.

119
00:08:09,700 --> 00:08:11,790
So here everything is in order.

120
00:08:11,830 --> 00:08:19,740
So let's do a little bit of testing by actually shuffling say after creating the test that grabbed that

121
00:08:19,740 --> 00:08:26,130
test deck and shuffle it and then instead of printing everything on order we should see something printed

122
00:08:26,130 --> 00:08:27,540
out shuffled.

123
00:08:27,670 --> 00:08:32,900
We run that and we see some diamonds hearts clubs etc. and we can see that it's all been shuffled.

124
00:08:32,950 --> 00:08:36,360
So looks like our class is working.

125
00:08:36,400 --> 00:08:40,190
All right let's have a quick review before moving on the next lecture.

126
00:08:40,210 --> 00:08:45,360
We've done so far at the very beginning what we did was we set up some global variables.

127
00:08:45,370 --> 00:08:52,570
We also imported the random library the global variables are a tuple to represent the suits some ranks

128
00:08:52,630 --> 00:08:56,570
which is another tuple to represent the ranks as well as values.

129
00:08:56,590 --> 00:09:00,460
And this is a really important variable because it's a dictionary look up where you just pass and the

130
00:09:00,460 --> 00:09:06,870
rank and you get back its numerical value so that you can easily take sums of someone's hand.

131
00:09:07,000 --> 00:09:09,670
Then we create a class for the card itself.

132
00:09:09,670 --> 00:09:11,770
So this represents just the single card.

133
00:09:11,780 --> 00:09:17,860
It has two attributes a suit in a rank and you can print to a card and it will report back rank of a

134
00:09:17,860 --> 00:09:18,600
suit.

135
00:09:20,030 --> 00:09:26,000
Then for the class we wanted to leverage that card class in order to quickly create 52 of every single

136
00:09:26,000 --> 00:09:27,320
card to do that.

137
00:09:27,320 --> 00:09:32,570
We just have a nested for loop where we say four suit in suits for rank and ranks and by creating a

138
00:09:32,660 --> 00:09:39,650
empty list for this first attribute we went ahead and took that list and the Penda a bunch of cards

139
00:09:39,680 --> 00:09:40,150
to it.

140
00:09:40,150 --> 00:09:44,680
Remember that card is an actual card object where we sit for every suit in that global variables suits.

141
00:09:44,720 --> 00:09:46,430
So that's the one up here.

142
00:09:46,430 --> 00:09:51,920
And then for every rank in that global variable ranks that's one of their we went ahead credit card

143
00:09:51,920 --> 00:09:52,400
class.

144
00:09:52,440 --> 00:09:54,960
Now we have a card object and we appended it to that list.

145
00:09:55,160 --> 00:10:00,410
So self-taught there is a list of these card objects that we wanted to be able to print out the actual

146
00:10:00,410 --> 00:10:00,750
deck.

147
00:10:00,760 --> 00:10:02,840
So if you said print deck you'd get something.

148
00:10:02,900 --> 00:10:08,720
And in that case all we do is we say OK start off with an empty string and then for every card in this

149
00:10:08,810 --> 00:10:11,000
list of card objects.

150
00:10:11,000 --> 00:10:16,390
Go ahead and concatenate a new line and then the string representation of that card.

151
00:10:16,430 --> 00:10:20,700
And then we can print out the deck has and it's just one giant string a bunch of new line calls.

152
00:10:20,700 --> 00:10:25,700
So then when you actually printed out you get back a list of all the cards then we can shuffle the deck

153
00:10:25,730 --> 00:10:30,980
which is basically just calling that random that shuffle method on that self-talk that CList and then

154
00:10:30,980 --> 00:10:36,560
we have a deal where we pop off a card object and we grab it by returning back that single card.

155
00:10:36,980 --> 00:10:37,520
OK.

156
00:10:37,550 --> 00:10:41,590
And then we tested it out by creating it shuffling it and printing of the test.

157
00:10:41,900 --> 00:10:46,850
Coming up next we're going to work on is creating a hand class and then creating a Chip's class.

158
00:10:46,910 --> 00:10:47,990
We'll see you at the next lecture.
