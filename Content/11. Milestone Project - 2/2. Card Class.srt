1
00:00:05,220 --> 00:00:06,260
Welcome back, everyone.

2
00:00:06,600 --> 00:00:11,520
In this lecture, we're going to begin by creating the card class, as well as some global variables

3
00:00:11,520 --> 00:00:13,560
that will be used throughout the project.

4
00:00:13,800 --> 00:00:15,450
Let's head to a notebook and get started.

5
00:00:16,960 --> 00:00:17,210
All right.

6
00:00:17,230 --> 00:00:19,000
Here I am at the Jupiter notebook.

7
00:00:19,270 --> 00:00:23,560
Before we begin actually coating anything, I just want to briefly sketch out the logic of what we're

8
00:00:23,560 --> 00:00:24,340
going to be doing.

9
00:00:24,940 --> 00:00:30,460
So we're going to create a card class and that card class should essentially just have three main properties.

10
00:00:30,880 --> 00:00:36,370
It should be able to understand the suit of the card, whether it's Heart Steinmann Spades clubs.

11
00:00:36,790 --> 00:00:39,280
It should be able to understand the rank of the card.

12
00:00:39,580 --> 00:00:43,090
Two, three, four, five, all the way to Jack Queen, King and so on.

13
00:00:43,480 --> 00:00:48,460
And then corresponding to that rank, there should also be a easy to use integer value.

14
00:00:48,910 --> 00:00:54,730
So that way later on when I'm comparing one card class to another or one instance of a card class to

15
00:00:54,730 --> 00:01:00,670
another instance, if it's Jack versus Queen, I can simply use the value of something like eleven versus

16
00:01:00,670 --> 00:01:05,650
twelve or some sort of integer in order to quickly decide which card has the higher value.

17
00:01:06,100 --> 00:01:10,690
So we're going to do here is we'll go ahead and create the card class.

18
00:01:10,780 --> 00:01:16,220
And then in the next series of lectures, we will create a deck class, which is essentially the 52

19
00:01:16,240 --> 00:01:19,180
unique card classes that come within a deck.

20
00:01:19,330 --> 00:01:22,240
So we'll show you in the next lecture how to generate a deck.

21
00:01:22,600 --> 00:01:29,080
And then after that, we'll create a player who can then hold cards from a deck and then we'll start

22
00:01:29,080 --> 00:01:29,860
the game logic.

23
00:01:30,190 --> 00:01:33,370
But let's start with the first step, which is creating that card class.

24
00:01:34,360 --> 00:01:39,400
And this one actually requires us to create a couple of global variables that will be easy to use later

25
00:01:39,400 --> 00:01:43,060
on, especially when we create the actual deck.

26
00:01:43,540 --> 00:01:47,440
But what I'm going to do first is create the card class.

27
00:01:47,470 --> 00:01:48,610
I'm just going to call it card.

28
00:01:50,000 --> 00:01:54,890
And then you can use open and closed princes here that aren't necessary, since we won't be using inheritance

29
00:01:54,890 --> 00:01:55,190
at all.

30
00:01:56,410 --> 00:01:57,380
Then we'll say dieser.

31
00:01:58,160 --> 00:01:59,770
And we have the instantiation method.

32
00:02:00,220 --> 00:02:02,030
Always remember it's double underscores here.

33
00:02:02,050 --> 00:02:03,760
So one underscore, another one.

34
00:02:04,690 --> 00:02:06,980
We always see yourself as a self reference.

35
00:02:07,050 --> 00:02:07,820
Keyword here.

36
00:02:08,260 --> 00:02:13,240
And then one I'm going to do is when someone creates a card, what they should be able to provide is

37
00:02:13,240 --> 00:02:17,440
the suit of that card and the rank of the card as far as the value.

38
00:02:17,680 --> 00:02:22,120
We don't need the user to actually provide a value because we'll figure that out automatically based

39
00:02:22,120 --> 00:02:24,460
off a dictionary of values that will create later on.

40
00:02:26,150 --> 00:02:28,760
So when you create a card, you give it the suit and the rank.

41
00:02:28,870 --> 00:02:35,630
So we'll say self-taught suit is equal to suit and then self-taught rank.

42
00:02:36,680 --> 00:02:38,210
Is equal to ranked.

43
00:02:40,130 --> 00:02:42,890
And in a little bit, we'll figure out how to actually add in the value.

44
00:02:44,160 --> 00:02:48,240
The other method I'm going to provide here is a string method just for convenience.

45
00:02:48,270 --> 00:02:51,660
If I ever want to print out a card, that's easy for a human to understand.

46
00:02:52,050 --> 00:02:57,300
I'll go ahead and create an underscore, underscore, SDR, underscore, underscore method or string

47
00:02:57,300 --> 00:02:57,720
method.

48
00:02:58,320 --> 00:03:00,690
And let's decide what you actually want to print out.

49
00:03:00,780 --> 00:03:06,480
If I were to print out an instance of a card class, I'll go ahead and return cell Frank.

50
00:03:07,500 --> 00:03:11,550
And then I will concatenate it with space of space.

51
00:03:12,090 --> 00:03:15,240
And then I will concatenate that with a string self-taught suit.

52
00:03:15,660 --> 00:03:19,470
So it basically assumes that suits and rank that I'm passing in are strings.

53
00:03:20,280 --> 00:03:21,390
So go ahead and run this.

54
00:03:22,520 --> 00:03:26,190
And let's create an example of what I would expect a card class to look like.

55
00:03:26,700 --> 00:03:32,460
So if I wanted to create the two of hearts as an instance of a card class, what would I need to provide

56
00:03:32,860 --> 00:03:34,020
our needs, provide the suit?

57
00:03:34,560 --> 00:03:36,290
So that would be something like hearts.

58
00:03:37,620 --> 00:03:39,840
And then I would need to provide the rank.

59
00:03:40,080 --> 00:03:42,660
So I'll go ahead and provide this as to.

60
00:03:43,970 --> 00:03:48,270
And now if I have the two of hearts just like this, it's going to be an instance of the card class

61
00:03:48,510 --> 00:03:49,710
at some location in memory.

62
00:03:50,430 --> 00:03:51,390
So that's what we have here.

63
00:03:51,660 --> 00:03:54,810
Hey, it's an instance of the card class at this location in your memory.

64
00:03:55,350 --> 00:03:56,730
What happens if we print this out?

65
00:03:58,630 --> 00:04:03,760
Will recall when you print something, it looks to what the string double underscore method returns,

66
00:04:04,060 --> 00:04:06,860
which in this case should be the rank of suits.

67
00:04:06,940 --> 00:04:08,950
So it says this is the two of hearts.

68
00:04:09,070 --> 00:04:09,580
Perfect.

69
00:04:10,030 --> 00:04:11,710
So I also have those attributes.

70
00:04:13,310 --> 00:04:14,180
Which is the suit.

71
00:04:16,200 --> 00:04:20,220
And then if we say hearts here, the rank, too.

72
00:04:20,940 --> 00:04:24,400
Now, unfortunately for us, these are both strings.

73
00:04:24,480 --> 00:04:29,760
So it's gonna be hard to compare one rank to another because I can't really compare the string to versus

74
00:04:29,760 --> 00:04:31,290
the string three directly.

75
00:04:31,740 --> 00:04:36,050
I'm going to need somehow to translate this to into the actual integer, too.

76
00:04:36,570 --> 00:04:44,910
And for that, I'm going to use a dictionary of values so I won't copy and paste this from the actual

77
00:04:44,970 --> 00:04:45,730
lecture notebook.

78
00:04:45,780 --> 00:04:49,800
But just to give you an idea of what we're trying to do here is I want to create a dictionary where

79
00:04:49,800 --> 00:04:53,400
the values will have something like two as a key.

80
00:04:53,940 --> 00:04:57,990
And then the integer value two as the actual corresponding value.

81
00:04:58,590 --> 00:05:04,620
So what I'll do here is instead of typing this out all manually, go ahead and open up this actual code

82
00:05:04,620 --> 00:05:08,520
along notebook and then copy and paste a dictionary that we have there for you.

83
00:05:08,610 --> 00:05:09,540
And it should look like this.

84
00:05:09,570 --> 00:05:11,050
Basically, two goes to two.

85
00:05:11,080 --> 00:05:13,020
Three goes the three foregoes four and so on.

86
00:05:13,440 --> 00:05:14,810
And then 10 goes to ten, Jack.

87
00:05:14,820 --> 00:05:15,510
Eleven, twelve.

88
00:05:15,690 --> 00:05:17,760
And in our game, we'll treat Ace as the high.

89
00:05:19,790 --> 00:05:27,890
So you run that, and now if I take a look at values and I pass in the two of hearts rank, which recall

90
00:05:27,900 --> 00:05:31,780
is just this string to, I should get back to corresponding value integer two.

91
00:05:32,900 --> 00:05:35,360
So now when I run that, I get back to.

92
00:05:35,990 --> 00:05:40,280
Now, it's really cool about this is because we're actually defining this at a global level.

93
00:05:40,370 --> 00:05:42,230
It's not within a class or not within a function.

94
00:05:42,560 --> 00:05:47,480
What it should be able to do is use values within the card class itself.

95
00:05:47,900 --> 00:05:52,700
And typically, when you're defining things like a large global variable like this, you'll put it at

96
00:05:52,700 --> 00:05:53,750
the top of your script.

97
00:05:54,110 --> 00:05:58,130
So just for readability, I'm going to cut and paste this up here.

98
00:05:59,110 --> 00:06:06,460
At the top and then we're going to do here is the actual value will then be an attribute, we'll say

99
00:06:06,460 --> 00:06:10,870
self-taught value is equal to valued dictionary.

100
00:06:11,910 --> 00:06:17,460
And then we can passan rank here, so why rank and out, why self-taught rank?

101
00:06:17,520 --> 00:06:21,790
Well, it's because inside the instantiation method, we're actually already passing rank.

102
00:06:21,990 --> 00:06:24,960
So we expect the person to pass in a two or three.

103
00:06:25,470 --> 00:06:29,550
Also, notice that we're expecting this person to pass it in exactly as shown here.

104
00:06:29,970 --> 00:06:30,620
We'll get an error.

105
00:06:30,630 --> 00:06:35,880
For example, if I said a lowercase T because values only has this uppercase T.

106
00:06:36,270 --> 00:06:41,070
So keep that in mind that when we're actually constructing many cards, we'll have to make sure that

107
00:06:41,070 --> 00:06:43,950
the key value corresponds exactly as shown here.

108
00:06:44,880 --> 00:06:47,310
So make sure you run the Values Dictionary.

109
00:06:47,730 --> 00:06:48,510
We will again.

110
00:06:49,800 --> 00:06:55,230
Redefine this card class and then let's go ahead and now create, let's say, the three.

111
00:06:56,510 --> 00:06:57,280
Of clubs.

112
00:07:00,380 --> 00:07:01,700
So that's a card class.

113
00:07:01,900 --> 00:07:03,920
No, I don't need to actually provide value.

114
00:07:03,950 --> 00:07:05,570
It still only uses to rank.

115
00:07:06,740 --> 00:07:08,090
So we'll say this is the three.

116
00:07:10,540 --> 00:07:15,400
And then we'll say FIPS actually suit goes first or say clubs.

117
00:07:17,260 --> 00:07:17,900
And it's three.

118
00:07:19,470 --> 00:07:19,980
Run that.

119
00:07:20,310 --> 00:07:23,820
And now if I see three of clubs, I should be able to see the suit.

120
00:07:25,170 --> 00:07:25,830
Is clubs.

121
00:07:26,530 --> 00:07:29,190
Now, let's check out the rink is three.

122
00:07:29,820 --> 00:07:32,370
And unlike before, we should now have value.

123
00:07:33,210 --> 00:07:37,590
So you run that and now you get this integer value and after you've run everything in a new cell, you

124
00:07:37,590 --> 00:07:41,910
should be able to hit tab here and see Reinke suit and value as the attributes.

125
00:07:42,240 --> 00:07:44,910
And that's essentially what we need for our card class.

126
00:07:45,390 --> 00:07:48,960
We have the suit, the rank, and importantly, we have values.

127
00:07:49,050 --> 00:07:53,400
So now if I were to redefine two of hearts, let's go ahead and rename this.

128
00:07:54,060 --> 00:07:57,300
So run these cells again, print two of hearts, suit rank.

129
00:07:57,820 --> 00:08:03,450
Now, if I see over here made delete the cell and now I should be able to actually perform a comparison

130
00:08:03,810 --> 00:08:05,430
like we would during the game.

131
00:08:05,640 --> 00:08:12,210
So I can take a two of hearts cards value and compare it, whether it's less same greater than or equal

132
00:08:12,210 --> 00:08:14,640
to the three of clubs value.

133
00:08:16,540 --> 00:08:17,230
So run that.

134
00:08:17,440 --> 00:08:20,290
And it looks like two of hearts is true.

135
00:08:20,470 --> 00:08:22,450
Less than three of clubs not value.

136
00:08:22,780 --> 00:08:29,410
Keep in mind, I can't compare directly the card classes to each other because these are card classes.

137
00:08:29,440 --> 00:08:30,610
They're not actually integers.

138
00:08:30,700 --> 00:08:36,970
I would need to do it with the actual integer value here in order to get something that makes sense.

139
00:08:37,300 --> 00:08:40,120
And I can do things like check for equality, false.

140
00:08:40,270 --> 00:08:45,970
And I can already begin to see how the card game would work because I should do in equality check to

141
00:08:45,970 --> 00:08:49,900
make sure that we're actually not at an instance of war and drawing more cards.

142
00:08:50,570 --> 00:08:53,170
OK, so that's basically it for our card class.

143
00:08:53,260 --> 00:09:01,090
Again, we have the provided suit rank and then we have this values Look-Up, which corresponds to this

144
00:09:01,150 --> 00:09:02,050
global variable.

145
00:09:02,410 --> 00:09:08,230
And you'll see that quite often object oriented programming where a card class can reference either

146
00:09:08,290 --> 00:09:12,910
something like a global variable at the top or even an imported library.

147
00:09:13,270 --> 00:09:16,930
So, for example, later on we'll see that I will import random.

148
00:09:17,800 --> 00:09:22,840
And then inside of this card class, because I imported random, I could do something like random shuffle,

149
00:09:22,930 --> 00:09:26,380
which we will actually use within the deck class to shuffle a deck.

150
00:09:27,100 --> 00:09:30,730
Now, the last thing I want to do here is create two more objects.

151
00:09:32,150 --> 00:09:37,900
There will be global variables, and that is a list of all the suits as well as a list of all the ranks.

152
00:09:37,930 --> 00:09:39,280
In fact, we can just make them tuples.

153
00:09:39,910 --> 00:09:43,690
So what I will do is I will copy and paste these from.

154
00:09:45,150 --> 00:09:48,300
Our solutions, notebook or code along notebook.

155
00:09:48,720 --> 00:09:51,120
So here we have suits, Hearts, Diamond Spades clubs.

156
00:09:51,180 --> 00:09:56,340
It's gonna be a tuple or list of sabr as a tuple just to kind of iterate that I don't really want to

157
00:09:56,340 --> 00:09:56,960
change this.

158
00:09:57,540 --> 00:10:00,570
And then we'll have Rink's here.

159
00:10:00,600 --> 00:10:02,790
So I'm going to copy and paste this from the notebook.

160
00:10:02,820 --> 00:10:04,800
You can do the same, can manually type this out.

161
00:10:04,880 --> 00:10:05,520
You really want to.

162
00:10:05,940 --> 00:10:07,230
But here I have the ranks.

163
00:10:07,350 --> 00:10:11,760
And the reason I have the ranks and the suits is just to make sure everything is formatted correctly.

164
00:10:12,000 --> 00:10:14,460
When we began actually creating an entire deck.

165
00:10:14,740 --> 00:10:19,440
So what it could do is for every suit, create a ranked card class here.

166
00:10:19,860 --> 00:10:24,420
And I want to make sure that these ranks match up the strings exactly as the value is here.

167
00:10:25,440 --> 00:10:29,820
So just go ahead and run that and we'll be using these global variables in the next lecture.

168
00:10:29,880 --> 00:10:35,070
When we create our deck class and as I mentioned, something to keep in mind is because we're using

169
00:10:35,520 --> 00:10:43,590
this values global variable up here and relies on rank that when I pass in the specific rank, it has

170
00:10:43,590 --> 00:10:45,840
to match up exactly as the key here.

171
00:10:46,290 --> 00:10:51,810
So if I were to do something like have a lowercase three, for instance, you'll get an error because

172
00:10:51,810 --> 00:10:57,690
it says, hey, I can't find three as a key within this values dictionary, which makes sense because

173
00:10:57,750 --> 00:10:59,100
it's a capital three.

174
00:10:59,490 --> 00:10:59,910
Same thing.

175
00:10:59,910 --> 00:11:04,790
If you're to provide something like three here, run this, you'll get another key error.

176
00:11:05,640 --> 00:11:10,410
Now, if you wanted to, you could just have the user or creation method or instantiation method.

177
00:11:10,680 --> 00:11:12,960
Also provide value instead of just suit and rank.

178
00:11:13,320 --> 00:11:18,840
But this method basically looks it up for you and make sure that every instance of the card class is

179
00:11:19,050 --> 00:11:20,580
performed or created correctly.

180
00:11:21,360 --> 00:11:21,610
All right.

181
00:11:22,080 --> 00:11:25,230
That's it for the Khadka class Simplist class will have in this project.

182
00:11:25,510 --> 00:11:29,430
It's just a suit rank and a corresponding value suit and rank.

183
00:11:29,640 --> 00:11:33,480
That really helps us if we want to print out the cards throughout the game, kind of show what each

184
00:11:33,480 --> 00:11:34,200
player has.

185
00:11:34,620 --> 00:11:37,950
And really the workhorse here is gonna be this value Look-Up.

186
00:11:38,340 --> 00:11:40,540
That's what's going to allow us to play the game logic.

187
00:11:41,300 --> 00:11:46,710
OK, now that we have a single card class ready to go, we have to create a deck of cards for the players

188
00:11:46,710 --> 00:11:48,870
to split, shuffle and play around with.

189
00:11:49,260 --> 00:11:54,830
So in the next lecture, we'll see how we can actually create instances of the card class within another

190
00:11:54,870 --> 00:11:55,350
class.

191
00:11:55,710 --> 00:11:56,250
I'll see you there.
