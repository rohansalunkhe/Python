1
00:00:05,540 --> 00:00:06,350
Welcome back.

2
00:00:06,350 --> 00:00:10,160
Now it's time to actually create the functions that we'll be using when we run the larger piece of code

3
00:00:10,160 --> 00:00:12,620
at the end that actually plays the game.

4
00:00:12,620 --> 00:00:15,010
And there's a lot of flexibility having these functions.

5
00:00:15,020 --> 00:00:19,480
So don't take the instructions here as the only correct way to do this.

6
00:00:19,520 --> 00:00:24,380
We're going start off by creating a function called Take bet that is going to ask the user for a bet

7
00:00:24,440 --> 00:00:27,530
and then set that as the actual bet.

8
00:00:27,590 --> 00:00:29,030
So let's do the following here.

9
00:00:29,210 --> 00:00:34,880
We'll say Take bet and we'll pass some chips and then we will say.

10
00:00:34,890 --> 00:00:41,020
While true we're going to try the following will say chips.

11
00:00:41,060 --> 00:00:46,990
That bet is equal to the integer of the input.

12
00:00:47,030 --> 00:00:51,190
How many chips would you like to bet.

13
00:00:51,200 --> 00:00:56,700
Question mark space and then we'll say except.

14
00:00:56,730 --> 00:01:03,350
So if they didn't actually inputting into Germany they put in the letter we'll say print sorry please

15
00:01:03,350 --> 00:01:04,910
provide an integer

16
00:01:08,530 --> 00:01:17,270
then we can say else if chips thought that is greater then the total number of chips available in this

17
00:01:17,270 --> 00:01:21,000
object you will say print.

18
00:01:21,000 --> 00:01:28,270
Sorry you do not have enough chips you have.

19
00:01:28,830 --> 00:01:31,950
And then we can insert how much they have.

20
00:01:32,000 --> 00:01:40,850
So we'll say the format chips total number of chips they're else we're going to break out of this while

21
00:01:40,850 --> 00:01:41,600
loop.

22
00:01:41,600 --> 00:01:43,880
So this is just one way we could have done this.

23
00:01:43,880 --> 00:01:46,570
You didn't actually have to pass in the chips here.

24
00:01:46,580 --> 00:01:52,160
There's an example in the notebook which I'm going to drag over real quick just so we can see it here

25
00:01:52,190 --> 00:01:56,860
can see that there's another method you could run this that would not have taken in the chips object.

26
00:01:57,050 --> 00:02:01,280
Instead if you knew in advance the were going to call a particular player's chips object you could just

27
00:02:01,280 --> 00:02:03,510
called Player chips here at the BET.

28
00:02:03,530 --> 00:02:08,030
So it's really up to you the Pentagon how you can call this function whether you want to pass in any

29
00:02:08,030 --> 00:02:09,390
arguments or not.

30
00:02:09,470 --> 00:02:15,040
Usually when you're calling a function you're going to want to pass in arguments that way you can't

31
00:02:15,110 --> 00:02:19,560
have control of the global variables without needing to call that global keyword.

32
00:02:19,640 --> 00:02:23,480
Again it's really up to you whichever version you prefer.

33
00:02:23,480 --> 00:02:25,220
So we'll go with the version that we have.

34
00:02:25,220 --> 00:02:27,430
We're actually asking for the chips.

35
00:02:27,470 --> 00:02:29,410
So let's break this down what we just did.

36
00:02:29,510 --> 00:02:31,090
We taken some chips.

37
00:02:31,160 --> 00:02:37,070
That's the class we just said here and it has a total and a bet attribute.

38
00:02:37,140 --> 00:02:42,870
We set the bet equal to whatever the player wants to bet we say except if they're not providing an integer

39
00:02:42,960 --> 00:02:47,460
and then we do a quick little check that even if they did provide an integer we want to make sure that

40
00:02:47,550 --> 00:02:51,000
the bet is not greater than the chips total.

41
00:02:51,000 --> 00:02:55,460
And if that's the case we'll print out a little message telling them how much they have left.

42
00:02:55,560 --> 00:03:00,690
Else if that's OK and there's an integer there we'll just break out of this while true loop and we've

43
00:03:00,690 --> 00:03:03,740
set chips up successfully.

44
00:03:03,800 --> 00:03:09,450
So let's run this and move on to Step 7 which is to write a function for taking hits.

45
00:03:09,470 --> 00:03:11,960
This is actually pretty straightforward.

46
00:03:11,960 --> 00:03:18,380
All we need to do here is say hand and we're going to add a card to the hand from the actual deck.

47
00:03:18,380 --> 00:03:23,060
You could do this all in one line or you could separate it out into two lines saying something like

48
00:03:23,990 --> 00:03:28,410
single card is equal to deck deal.

49
00:03:28,540 --> 00:03:31,880
And then add in that hand the single card you just find.

50
00:03:31,950 --> 00:03:38,800
And we want to make sure that we adjust for the ACE afterward so we can say hand that just for ACE and

51
00:03:38,800 --> 00:03:40,440
we'll call that method as well.

52
00:03:42,800 --> 00:03:48,240
All right so now we have hit it takes a deck and someone's hand grabs a single card from the deck as

53
00:03:48,240 --> 00:03:54,720
it's in the hand and then checks for an ace adjustment step is to write a function prompting the player

54
00:03:54,750 --> 00:03:56,520
to hit or stand.

55
00:03:56,520 --> 00:04:00,630
So this function what it's going to do is going to accept the deck and the players hand the arguments

56
00:04:00,980 --> 00:04:03,300
in a sign playing as a global variable.

57
00:04:03,300 --> 00:04:05,930
And this is actually the control an upcoming while loop.

58
00:04:06,240 --> 00:04:11,000
You could just provide playing here in the function as well and then return it back.

59
00:04:11,160 --> 00:04:13,220
It's kind of up to you how you want to deal with it.

60
00:04:15,010 --> 00:04:17,460
But this is just going to be another wild true loop.

61
00:04:17,650 --> 00:04:30,640
Very similar to before you will say x is equal to input asking hit or stand enter H or S

62
00:04:35,860 --> 00:04:44,200
and then we'll say if X lower case is equal to h.

63
00:04:44,200 --> 00:04:45,590
So why am I doing this.

64
00:04:45,760 --> 00:04:51,310
Well maybe someone's going to misunderstand this line and then type out something like hits or something

65
00:04:51,310 --> 00:04:56,400
like h h maybe they do a lowercase stand.

66
00:04:56,590 --> 00:05:01,060
In that case what I'm going to do is be able to grab the string grab the first letter of lowercase and

67
00:05:01,060 --> 00:05:02,140
say it equals H.

68
00:05:02,260 --> 00:05:04,530
They probably meant to hit.

69
00:05:04,780 --> 00:05:10,710
So I'm going to say hit from the deck on their hand.

70
00:05:12,450 --> 00:05:19,950
LCF x 0 thought lower case is equal to S..

71
00:05:19,970 --> 00:05:22,300
I'm going to assume they wanted to stand.

72
00:05:22,670 --> 00:05:28,290
And I'm going to say print player stands dealers turn

73
00:05:31,320 --> 00:05:38,770
and I'll sit playing equal to false and this is because I'm going to use this in a while loop later

74
00:05:38,770 --> 00:05:39,170
on.

75
00:05:39,340 --> 00:05:40,630
You didn't have to do it this way.

76
00:05:40,630 --> 00:05:43,480
Again there's many different ways you can control that while player on.

77
00:05:43,660 --> 00:05:47,470
This is just the way we're going to do it so that we can kind of clearly see it in the while loop later

78
00:05:47,470 --> 00:05:49,410
on.

79
00:05:49,600 --> 00:05:59,820
Finally we'll have an else statement that says print sorry I did not understand that.

80
00:06:00,000 --> 00:06:06,810
Please enter H or s only because that basically means they didn't enter something that starts with an

81
00:06:06,840 --> 00:06:11,670
H or an S then I'm going to continue.

82
00:06:11,670 --> 00:06:17,350
And if none of those gets triggered then what happens is we just go ahead and break OK the next thing

83
00:06:17,350 --> 00:06:21,630
you want to do is write functions to handle each of the game situations where it's going to end.

84
00:06:21,750 --> 00:06:25,930
And remember to pass in the player's hand that dealer sand and the chips as needed.

85
00:06:25,950 --> 00:06:27,540
So let's go ahead and pass those in.

86
00:06:27,570 --> 00:06:30,650
We'll say pasand the player who's represented by a hand.

87
00:06:30,660 --> 00:06:32,210
Let's make sure we spell that right.

88
00:06:32,300 --> 00:06:37,410
Passing the dealer who's represented by hand object and then passes the chips themselves we'll grab

89
00:06:37,410 --> 00:06:42,500
these and pass them into all of them except for push.

90
00:06:42,500 --> 00:06:47,580
Because if there's a push that means both player and dealer got 21 so we won't actually do anything

91
00:06:47,580 --> 00:06:48,400
with the chips.

92
00:06:50,510 --> 00:06:56,690
So here I'll push this the easiest one won't really do anything which just say dealer and player Ty

93
00:07:00,280 --> 00:07:06,750
push for the other ones we're basically going to print the message out and then adjust the chips themselves.

94
00:07:07,300 --> 00:07:17,320
So if the player buss will print out just player will take those chips and then we'll call the lose

95
00:07:17,380 --> 00:07:21,130
bet method off of them player wins.

96
00:07:21,160 --> 00:07:22,410
Very similar thing.

97
00:07:23,190 --> 00:07:27,230
We print out player wins.

98
00:07:28,920 --> 00:07:38,690
We take those chips and then we say when bet if the dealer bus is going to be the same thing up here.

99
00:07:39,090 --> 00:07:44,240
The player will win the dealer bus except at another message your player wins.

100
00:07:44,610 --> 00:07:48,400
Dealer busted if the dealer wins.

101
00:07:48,450 --> 00:07:53,860
It's going to be a similar thing to the player busting because the message will be different.

102
00:07:53,880 --> 00:08:01,780
They'll still lose their bet except the same dealer wins OK run that sell.

103
00:08:02,000 --> 00:08:05,790
And now the last thing we need to do is actually run the logic of the game.

104
00:08:05,870 --> 00:08:07,430
We'll be doing it in the next lecture.

105
00:08:07,440 --> 00:08:11,290
But let's quickly review what we did in this lecture.

106
00:08:11,410 --> 00:08:15,860
So we started off by creating the actual functions that were going to be calling in the next lecture.

107
00:08:15,940 --> 00:08:21,010
We first start off with Take bet which is just in some form or manner getting an input from the user

108
00:08:21,190 --> 00:08:22,820
and then setting it to the current bet.

109
00:08:22,960 --> 00:08:27,580
Lots of different ways you could do this they wanted to write a function for taking hits.

110
00:08:27,590 --> 00:08:33,020
So we take in the deck we take a hand we take in a single card from the deck add it to the hand and

111
00:08:33,020 --> 00:08:35,060
then adjust the hand for an ace.

112
00:08:35,240 --> 00:08:38,130
They wanted to write a function prompted the player to hit Kristan.

113
00:08:38,150 --> 00:08:43,340
Basically we just wanted to figure out how am I going to call hits or am I going to just stand and say

114
00:08:43,400 --> 00:08:49,520
that this current player is no longer playing or it could just continue if they didn't provide an entrance

115
00:08:50,810 --> 00:08:53,090
that I wanted to write functions to display the cards.

116
00:08:53,120 --> 00:08:54,410
Lots of different ways you can do this.

117
00:08:54,410 --> 00:09:00,740
You can check out the notebook for some more advanced one liner ways of displaying all of this information.

118
00:09:00,960 --> 00:09:04,470
And then we wanted to write functions to handle each of the game situations where we basically either

119
00:09:04,470 --> 00:09:07,800
call loose bet or win bet and print out a message.

120
00:09:07,800 --> 00:09:10,810
Coming up next we're going to cut out the logic for the entire game.
