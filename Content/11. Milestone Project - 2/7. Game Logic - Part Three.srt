1
00:00:05,250 --> 00:00:09,090
Welcome back, everyone, to Part three of this Game Logic series.

2
00:00:10,110 --> 00:00:13,140
So now it's time to check the players cards against each other.

3
00:00:13,710 --> 00:00:18,060
And I want to point out and make it clear that there's tons of different ways this can be done.

4
00:00:18,510 --> 00:00:23,970
Definitely do not take the while loop method we show here as the only way to do this.

5
00:00:24,120 --> 00:00:28,590
And as I previously mentioned at the bottom of the notebook, there's lots of other implementations

6
00:00:28,650 --> 00:00:33,870
of the war card game in Python that you can check out just to explore other ways of thinking about it.

7
00:00:34,470 --> 00:00:39,900
But as we continue in general, regardless of how you actually program this, there's really only three

8
00:00:39,900 --> 00:00:46,650
possible situations when the players first compare that first card, and that is either player one has

9
00:00:46,650 --> 00:00:48,180
a card greater than player 2s.

10
00:00:48,660 --> 00:00:53,010
Player one has a card that's less than player 2s, essentially saying player to script and player one,

11
00:00:53,490 --> 00:00:56,790
or in the case of war, their cards have equal value.

12
00:00:58,860 --> 00:01:04,410
The way we're going to write this is within, if L.F. else within a while loop.

13
00:01:04,800 --> 00:01:09,780
And that assumes that a war has happened, which is a little strange at first.

14
00:01:09,810 --> 00:01:12,780
But hopefully when you see the logic, it actually makes a lot of sense to you.

15
00:01:13,230 --> 00:01:18,170
So what we're going to do is we're going to state that at war it's only equal to false.

16
00:01:18,330 --> 00:01:24,090
If the players resolve the match up on that first draft card in those first two situations, we'll just

17
00:01:24,090 --> 00:01:25,710
say, hey, at war is equal to false.

18
00:01:25,980 --> 00:01:27,660
No need to worry about this at war loop.

19
00:01:27,960 --> 00:01:28,830
We'll just break out of it.

20
00:01:29,040 --> 00:01:31,310
So we start off assuming at war.

21
00:01:31,350 --> 00:01:31,740
True.

22
00:01:32,000 --> 00:01:35,640
And we break out of at war while loop by saying it's false.

23
00:01:35,970 --> 00:01:40,920
If the players resolve their matchup, otherwise we won't say anything and at war will remain true.

24
00:01:41,340 --> 00:01:47,280
And then we add cards to the current cards on the table, drawing those extra cards for the actual war.

25
00:01:47,910 --> 00:01:52,320
As a quick note, we're going to be playing with some special rules, and that is the rules.

26
00:01:52,320 --> 00:01:56,640
And this version is that each player needs to draw five additional cards.

27
00:01:56,730 --> 00:01:57,750
If there is a tie.

28
00:01:58,200 --> 00:02:03,870
So that is to say, when a war does occur, we'll go ahead and add another five cards from the deck.

29
00:02:04,410 --> 00:02:05,790
A lot of times people use three.

30
00:02:06,120 --> 00:02:10,080
But the reason we're going to up to five is just to make sure the games don't last too long.

31
00:02:10,860 --> 00:02:13,860
The lower that number is, the longer the games are going to last.

32
00:02:14,040 --> 00:02:16,410
And sometimes they can last thousands and thousands of turns.

33
00:02:16,740 --> 00:02:20,460
So the higher the number, the higher or the faster the simulation will go.

34
00:02:21,120 --> 00:02:26,550
And the other point we're going to make is that the player will lose if they don't have at least five

35
00:02:26,550 --> 00:02:28,740
cards to play in that war.

36
00:02:29,100 --> 00:02:31,890
So obviously, this logic can be easily edited.

37
00:02:31,980 --> 00:02:34,740
So you can change that number five to really any number you want.

38
00:02:35,040 --> 00:02:36,660
You can change it to three or so on.

39
00:02:37,170 --> 00:02:42,300
And you can also say that maybe the player has to use all their cards at the end if they have less than

40
00:02:42,300 --> 00:02:42,750
five.

41
00:02:42,780 --> 00:02:46,980
But we'll go ahead and say, if they don't have at least five or at least some specific number, they

42
00:02:46,980 --> 00:02:48,000
go ahead and lose.

43
00:02:48,240 --> 00:02:53,190
And all of this is just to avoid having to run the simulation for thousands of occurrences.

44
00:02:53,970 --> 00:02:59,220
And if you check out the lecture notebook at the very end, I've linked some interesting statistics

45
00:02:59,310 --> 00:03:00,450
of this war card game.

46
00:03:00,520 --> 00:03:05,610
So here's a simulation of the number of occurrences versus the number of turns to win.

47
00:03:06,420 --> 00:03:11,160
So essentially, how many times do the cards actually match up and get flipped?

48
00:03:11,370 --> 00:03:12,210
How many rounds occur?

49
00:03:12,540 --> 00:03:15,930
And it's also different whether you shuffle the cards or you don't shuffle the cards.

50
00:03:16,290 --> 00:03:21,750
And in some instances, if you don't shuffle each player's cards before each round, they sometimes

51
00:03:21,750 --> 00:03:27,270
get perfectly stacked in a loop where you kind of have this infinite loop where war will never occur

52
00:03:27,630 --> 00:03:32,730
because the each players cards of 26 cards each are perfectly stacked.

53
00:03:32,760 --> 00:03:36,420
So it's one time one player has beaten player two.

54
00:03:36,750 --> 00:03:38,910
And the next time, player two has been player one.

55
00:03:38,910 --> 00:03:39,840
And so on and so on.

56
00:03:40,110 --> 00:03:41,820
And you get kind of stuck in this weird infinite loop.

57
00:03:42,150 --> 00:03:46,510
In fact, this chart right here actually artificially is cut off at 5000.

58
00:03:46,650 --> 00:03:50,700
And you can check out the lecture notebook for the link to this full blog post.

59
00:03:51,150 --> 00:03:55,950
OK, so we're gonna quickly explore this loop visually before we cut it out, just to make sure it's

60
00:03:55,950 --> 00:03:56,850
clear to everyone.

61
00:03:57,870 --> 00:04:04,080
So the actual comparison, game logic, this is happening within this while loop of while at war, that's

62
00:04:04,080 --> 00:04:05,790
within the wild game on loop.

63
00:04:07,020 --> 00:04:11,580
So we start off outside of this loop by saying, hey, we're going to assume that war is true.

64
00:04:12,060 --> 00:04:14,640
So while at war, we'll start the comparisons.

65
00:04:15,480 --> 00:04:18,180
The first comparison is going to check if one is greater than two.

66
00:04:18,630 --> 00:04:23,250
If that happens to be true, then we will add the cards currently present on the so-called table.

67
00:04:23,260 --> 00:04:27,340
Those two cards will add those cards to one or player one's hand.

68
00:04:27,660 --> 00:04:29,400
And then we'll say at war sequel to False.

69
00:04:29,460 --> 00:04:30,870
So we break out of this entire loop.

70
00:04:32,640 --> 00:04:38,040
Elif, so the other situation we're checking for is if one, the value of clear one's card is less than

71
00:04:38,040 --> 00:04:39,330
the value of player to card.

72
00:04:39,780 --> 00:04:43,470
If that's the case, then we add the cards, the player too, and then we say, hey, there was no war

73
00:04:43,470 --> 00:04:44,970
here at what is equal to false.

74
00:04:45,630 --> 00:04:51,570
However, for the Ellis case, we haven't set at Oracle defaults yet, so we assume that Atwar is true.

75
00:04:52,290 --> 00:04:54,390
So now we're going to do the following.

76
00:04:54,750 --> 00:04:56,850
We're going to check if the players have enough cards.

77
00:04:57,060 --> 00:05:00,960
As I mentioned, the rules were using here is if you don't have at least three cards that draw the war,

78
00:05:01,350 --> 00:05:02,640
so you'll lose the game.

79
00:05:02,850 --> 00:05:08,130
So if the players have enough to actually commit to that war, then we draw those additional cards.

80
00:05:08,520 --> 00:05:10,350
Once you draw those additional cards.

81
00:05:10,440 --> 00:05:11,940
And this is why we use a while loop.

82
00:05:12,180 --> 00:05:17,970
We'll say, hey, start back at the top and not check that top card that has been newly drawn with these

83
00:05:18,000 --> 00:05:18,960
if statements again.

84
00:05:19,800 --> 00:05:22,980
So what's nice about using this kind of atwar starts off is true.

85
00:05:23,310 --> 00:05:25,050
And then we continue while at war.

86
00:05:25,410 --> 00:05:29,400
Is this accounts for the fact that you may have multiple wars in a row?

87
00:05:29,880 --> 00:05:36,000
So because of this, if we happen to then again fail that if elif check, we come back to the else,

88
00:05:36,060 --> 00:05:41,380
draw the additional cards again and then keep going over and over again until the series of wars or

89
00:05:41,380 --> 00:05:42,960
a single war has been resolved.

90
00:05:43,320 --> 00:05:48,210
Once that's true, we'll start back up the top with the wild game on and draw the single card and start

91
00:05:48,210 --> 00:05:49,110
this all over again.

92
00:05:49,590 --> 00:05:52,950
Okay, so let's see how this is actually programmed out in Jupiter.

93
00:05:53,100 --> 00:05:54,690
Here I am back at the Jupiter notebook.

94
00:05:54,690 --> 00:05:56,610
We are working the last time recall.

95
00:05:56,610 --> 00:05:59,080
We have our wild game on portion of the while loop.

96
00:05:59,490 --> 00:06:01,680
We check to see that the players still have cards to play.

97
00:06:02,010 --> 00:06:05,430
If they do, we start off a new round, grabbing one card each.

98
00:06:05,590 --> 00:06:07,530
And now it's time for this while at War Loop.

99
00:06:08,250 --> 00:06:14,610
So, as I mentioned, we're going to assume that the wars happening and then we'll look for cases to

100
00:06:14,700 --> 00:06:14,860
end.

101
00:06:15,930 --> 00:06:20,160
So, say, while you're at war and note, the indentation here is extremely important.

102
00:06:20,190 --> 00:06:24,150
So if you're getting an error, it's very likely you have an indentation error or you made a simple

103
00:06:24,150 --> 00:06:24,630
typo.

104
00:06:24,960 --> 00:06:26,410
Especially when we're calling things like.

105
00:06:26,450 --> 00:06:28,920
Player two all cards versus player one cards.

106
00:06:29,280 --> 00:06:33,090
If that happens to be the case, go ahead and copy and paste from our lecture notebook.

107
00:06:33,680 --> 00:06:33,900
OK.

108
00:06:34,380 --> 00:06:37,020
So while you're at war, we'll start doing that if check.

109
00:06:37,050 --> 00:06:37,770
We'll say if.

110
00:06:38,950 --> 00:06:43,810
Player one cards and one I'm going to do here, say negative one.

111
00:06:45,190 --> 00:06:47,290
That value is greater than.

112
00:06:49,540 --> 00:06:50,650
Player two cards.

113
00:06:54,090 --> 00:06:55,440
Card's negative one.

114
00:06:57,410 --> 00:06:58,140
That value.

115
00:06:59,340 --> 00:07:02,470
So in this case, player one has beaten player two.

116
00:07:02,820 --> 00:07:05,550
Now you're probably wondering, why are you using negative one here?

117
00:07:06,090 --> 00:07:12,660
Well, that's because on the assumption that we get to war player one cards is eventually going to be

118
00:07:13,140 --> 00:07:14,640
a stack of cards.

119
00:07:14,790 --> 00:07:20,190
And when I want to make sure is that by choosing negative one, I'm not going to be constantly choosing

120
00:07:20,250 --> 00:07:21,900
the very first card that was played.

121
00:07:22,080 --> 00:07:25,800
And that will be more clear since we're going to be appending cards to the end.

122
00:07:26,220 --> 00:07:27,900
So just show you quick visual example.

123
00:07:28,800 --> 00:07:31,800
Let's imagine that player one drew a queen.

124
00:07:33,290 --> 00:07:36,110
And let's imagine eventually it goes down to war.

125
00:07:36,680 --> 00:07:39,250
So next, they add some more cards there.

126
00:07:39,650 --> 00:07:43,580
Let's say they add Jack and King and then their final card.

127
00:07:43,610 --> 00:07:45,050
They're going to draw will be a new one.

128
00:07:45,080 --> 00:07:46,340
So that might be a four.

129
00:07:47,090 --> 00:07:53,030
So what happens is by calling negative one, if we happen to go to war, I am going to draw this new

130
00:07:53,030 --> 00:07:53,390
card.

131
00:07:53,900 --> 00:07:55,940
If I didn't have negative one, if I had zero there.

132
00:07:56,330 --> 00:08:00,530
Then if I append these towards the end, what's going to happen is I'm going to constantly be comparing

133
00:08:00,890 --> 00:08:05,750
against that first card, which will actually make this kind of go on forever because the players will

134
00:08:05,750 --> 00:08:09,650
constantly drawing the very top card that they had originally drawn.

135
00:08:10,130 --> 00:08:13,910
So the reason I use negative one here is because it will always draw that last card.

136
00:08:14,230 --> 00:08:19,550
And conveniently, if you only have one card in that list, it's just going to draw that card.

137
00:08:19,730 --> 00:08:21,190
So that's why we're using negative one there.

138
00:08:22,790 --> 00:08:28,010
So if player one card's negative one in this case at the very beginning, it's just one card, if that's

139
00:08:28,010 --> 00:08:32,780
greater than player two cards, negative one value, then player one gets the cards.

140
00:08:32,870 --> 00:08:33,920
So we say player one.

141
00:08:35,320 --> 00:08:36,100
At cards.

142
00:08:37,360 --> 00:08:39,120
And we'll say player one card's.

143
00:08:42,030 --> 00:08:45,600
And then we'll say player won at cards.

144
00:08:46,830 --> 00:08:48,120
Add player to Cartes.

145
00:08:49,170 --> 00:08:52,890
Now, what's really nice about the way we set this up, and that's why I mentioned the logic, you're

146
00:08:52,890 --> 00:08:54,600
often thinking about the classes as well.

147
00:08:55,020 --> 00:08:59,700
Is that ad cards works for both a singular card as well as multiple cards.

148
00:09:00,180 --> 00:09:06,060
So what's nice about this is I can now say player cards is just a single card object or a list of card

149
00:09:06,060 --> 00:09:06,540
objects.

150
00:09:06,960 --> 00:09:12,150
So player one gets all their cards back and they're also adding the addition of player two cards.

151
00:09:12,720 --> 00:09:14,100
And now we're no longer at war.

152
00:09:14,790 --> 00:09:20,850
So we will say Atwar is equal to false, which breaks us out of this comparison loop for at war.

153
00:09:21,540 --> 00:09:25,330
We come back up here and then we'll continue by checking.

154
00:09:25,370 --> 00:09:26,820
Hey, is someone lost yet?

155
00:09:26,940 --> 00:09:29,590
If not, start the next round and then we continue here.

156
00:09:30,150 --> 00:09:32,190
So it's up to you if you want to call this at war.

157
00:09:32,280 --> 00:09:36,600
You can also call this something like comparison still going, which might be a more appropriate variable

158
00:09:36,600 --> 00:09:36,900
name.

159
00:09:37,230 --> 00:09:38,840
But just want to make it clear to you here.

160
00:09:39,690 --> 00:09:44,890
Now, we also have the check to see if player two has one.

161
00:09:45,360 --> 00:09:48,600
So this is so similar that I can just essentially copy and paste this code, actually.

162
00:09:49,020 --> 00:09:51,510
So I'm going to do that and a copy and paste this.

163
00:09:51,870 --> 00:09:57,270
And whenever one player, one is switched that player to another player to his switch that to player

164
00:09:57,270 --> 00:09:57,510
one.

165
00:09:58,410 --> 00:10:07,530
So what I'm going to do here is say Elif player one cards is less than player two cards, then player

166
00:10:07,530 --> 00:10:08,160
two has one.

167
00:10:08,220 --> 00:10:12,270
So player two will be the one adding those cards.

168
00:10:12,390 --> 00:10:14,040
So just three changes we have to make here.

169
00:10:14,610 --> 00:10:16,020
Check the comparison the other way.

170
00:10:16,380 --> 00:10:18,540
So now player one is less than player two.

171
00:10:18,900 --> 00:10:24,390
So we will add the cards to player to both player one cards and player two cards.

172
00:10:26,010 --> 00:10:28,380
Finally comes the Elsah statement.

173
00:10:28,680 --> 00:10:35,250
So if the card is not greater than or less than then by default, they have to be equal to each other.

174
00:10:35,670 --> 00:10:37,230
And this is where we have a war.

175
00:10:37,820 --> 00:10:42,690
And to visualize this a little bit, I will say war that way.

176
00:10:42,750 --> 00:10:45,480
As this game is running, you can see how many times war pops up.

177
00:10:46,050 --> 00:10:50,370
So essentially, we should be seeing something like round one, round two, round three, and then all

178
00:10:50,370 --> 00:10:52,230
of us sudden we'll see something like war pop up.

179
00:10:52,720 --> 00:10:52,960
OK.

180
00:10:53,610 --> 00:10:59,040
So I previously described that you're going to lose the game if you don't have enough cards to actually

181
00:10:59,040 --> 00:10:59,700
play at war.

182
00:11:00,300 --> 00:11:02,310
So up to you if you want to add in this logic or not.

183
00:11:02,890 --> 00:11:03,870
But we'll say if.

184
00:11:05,100 --> 00:11:05,910
PLAYER one.

185
00:11:07,020 --> 00:11:07,920
All cards.

186
00:11:09,360 --> 00:11:10,320
Is less than three.

187
00:11:10,740 --> 00:11:18,240
Essentially, they can't play a war, we print something like player one, unable to.

188
00:11:19,570 --> 00:11:22,200
Declare war or play war, however you want to phrase it.

189
00:11:24,070 --> 00:11:26,080
And then let's go and print one more line saying.

190
00:11:27,860 --> 00:11:28,790
Player two wins.

191
00:11:30,690 --> 00:11:30,920
OK.

192
00:11:31,780 --> 00:11:35,470
And now we're gonna say here is because the game is over at this instant.

193
00:11:36,040 --> 00:11:39,550
We can say game on is now equal to false.

194
00:11:40,340 --> 00:11:43,180
And I was going to break out of this while at war.

195
00:11:43,750 --> 00:11:49,210
So break here is going to break out of this while at war and game on being false will end this overall

196
00:11:49,210 --> 00:11:49,780
while loop.

197
00:11:51,130 --> 00:11:54,460
And I'm going to do the same thing now for a player to.

198
00:11:55,760 --> 00:11:56,470
So we'll say.

199
00:11:57,890 --> 00:11:59,870
L.F. length of player to.

200
00:12:01,920 --> 00:12:03,240
All cars as less than three.

201
00:12:03,600 --> 00:12:05,520
Go ahead and say game on, sick with a false player.

202
00:12:05,660 --> 00:12:06,660
Two has now lost.

203
00:12:06,690 --> 00:12:09,030
So we'll say player one wins.

204
00:12:10,370 --> 00:12:15,240
And player two, unable to declare war, break out of this else.

205
00:12:15,600 --> 00:12:16,350
What are we going to do?

206
00:12:16,830 --> 00:12:19,300
Well, in this case, we simply just have to add more cards.

207
00:12:20,040 --> 00:12:20,640
So up to you.

208
00:12:20,660 --> 00:12:22,140
How many cards you want to add in here?

209
00:12:23,070 --> 00:12:27,840
We'll say for NUM in range three, we'll say they have to add on three cards.

210
00:12:28,740 --> 00:12:31,230
Then player one cards.

211
00:12:32,450 --> 00:12:33,050
Append.

212
00:12:34,240 --> 00:12:34,990
PLAYER one.

213
00:12:36,040 --> 00:12:39,160
Remove one and then we'll do the exact same thing.

214
00:12:39,430 --> 00:12:41,680
So just copy and paste this for player to.

215
00:12:43,040 --> 00:12:46,280
So they each essentially calling or move one three times.

216
00:12:49,000 --> 00:12:49,290
OK.

217
00:12:49,650 --> 00:12:50,160
There you have it.

218
00:12:50,730 --> 00:12:52,560
And this is the total game logic.

219
00:12:53,070 --> 00:12:57,570
So before we actually run this and check for typos, let's do a quick overview.

220
00:12:57,630 --> 00:13:01,590
I'm going to zoom out a little bit so we can see the whole thing, create the two players, create the

221
00:13:01,590 --> 00:13:05,310
deck, shuffle it, split the deck between the two players, the game on.

222
00:13:05,340 --> 00:13:07,400
So game automatically starts off as on.

223
00:13:07,800 --> 00:13:10,350
We have this round number counter right off the top.

224
00:13:10,620 --> 00:13:15,000
While the game is occurring, we have to continually check, are the players still eligible to play?

225
00:13:15,150 --> 00:13:16,440
Has one of them run out of cards?

226
00:13:16,800 --> 00:13:18,530
If not, we start a new round.

227
00:13:18,540 --> 00:13:22,090
So that removes one card and then we have this comparison check.

228
00:13:22,410 --> 00:13:28,590
So we'll kind of assume that war could happen if player one's graded in player to war doesn't happen.

229
00:13:28,620 --> 00:13:29,940
We said it's a false player.

230
00:13:29,940 --> 00:13:31,110
One gets both those cards.

231
00:13:31,900 --> 00:13:37,230
Else, if player one is less than player two, then player two gets both those cards.

232
00:13:37,350 --> 00:13:38,070
Wars over.

233
00:13:38,550 --> 00:13:40,170
Otherwise, wars happened.

234
00:13:40,410 --> 00:13:43,320
And within a war, really, the comparison is not occurring.

235
00:13:43,680 --> 00:13:49,050
All that's happening is or the final goal is there's at those cards so quickly, depending on the way

236
00:13:49,050 --> 00:13:53,340
you want to write these rules, you have these checks here to say, hey, they're unable to play war.

237
00:13:53,430 --> 00:13:54,780
So you can have a loss here.

238
00:13:55,320 --> 00:13:59,520
Otherwise you can make a check here to just say, OK, get the remaining cards.

239
00:13:59,550 --> 00:14:04,440
So maybe if they have two or one cards left, you could write this to just say grab all remaining cards

240
00:14:04,440 --> 00:14:10,170
of player one or player two, and then we'll go ahead and add those cards here and we append them,

241
00:14:10,200 --> 00:14:14,190
which is part of the reason of why we use negative one up here, because they're going at least the

242
00:14:14,190 --> 00:14:15,840
new cards are going towards the end.

243
00:14:16,990 --> 00:14:22,240
OK, so before we run this at the beginning of this lecture, I did mentioned that the higher this value

244
00:14:22,240 --> 00:14:26,680
is for how many cards you have to draw during war are the sure the game is going to be.

245
00:14:26,740 --> 00:14:28,750
So let's go and just change us to five.

246
00:14:30,020 --> 00:14:32,120
That way, they draw more cards upon war.

247
00:14:33,650 --> 00:14:36,980
And there's a higher likelihood for them to lose sooner.

248
00:14:37,400 --> 00:14:39,590
OK, so I just change these to five.

249
00:14:39,770 --> 00:14:41,090
Again, these are numbers you can edit.

250
00:14:41,210 --> 00:14:44,720
And this essentially we can kind of change the code to change the rules of the game.

251
00:14:45,020 --> 00:14:49,520
You can check out the Wikipedia article that I also linked to at the bottom of the lecture notebook

252
00:14:49,610 --> 00:14:52,260
to essentially explore different gameplay rules.

253
00:14:52,310 --> 00:14:55,010
But let's go ahead and run this and make sure it all works.

254
00:14:56,880 --> 00:14:59,880
OK, so we went for about two hundred twenty three rounds.

255
00:14:59,910 --> 00:15:01,590
There was a war at the very end.

256
00:15:01,980 --> 00:15:04,500
And then at the very end, Player two was unable to play war.

257
00:15:04,530 --> 00:15:06,420
So game over at war and player one wins.

258
00:15:06,820 --> 00:15:11,370
And if we scroll back up here, it looks like we had two wars in a row right there.

259
00:15:11,730 --> 00:15:15,180
So it looks like a while Loop was working and that's quite a bit of rounds there.

260
00:15:15,270 --> 00:15:16,560
So let's run it one more time.

261
00:15:16,860 --> 00:15:17,550
Let's take a quick note.

262
00:15:17,640 --> 00:15:18,570
Who won the last one?

263
00:15:18,630 --> 00:15:22,530
So, player two or excuse me, player one one last time.

264
00:15:22,950 --> 00:15:26,700
So to run this again, I have to reset the player's game on is true.

265
00:15:26,820 --> 00:15:29,270
And let's run that cell again, OK?

266
00:15:29,400 --> 00:15:30,810
So one hundred and four rounds.

267
00:15:30,840 --> 00:15:32,280
Player one wins again.

268
00:15:32,640 --> 00:15:36,750
And let's run this just a few more times to make sure there's no mistakes that way.

269
00:15:36,780 --> 00:15:40,280
We'd like to see Player to win a match eventually.

270
00:15:40,320 --> 00:15:41,520
So we scroll all the way down.

271
00:15:42,180 --> 00:15:44,160
Player one is out of cards and player two wins.

272
00:15:44,190 --> 00:15:44,730
Perfect.

273
00:15:44,820 --> 00:15:48,360
So always good to check that both players have the capability of winning here.

274
00:15:48,480 --> 00:15:50,280
And it looks like our code is working.

275
00:15:50,790 --> 00:15:51,090
Definitely.

276
00:15:51,090 --> 00:15:52,320
Check out the lecture notebook.

277
00:15:52,590 --> 00:15:54,180
There's a bunch of links at the bottom of it.

278
00:15:54,200 --> 00:15:56,850
Just discussing the game of war, how to program it.

279
00:15:56,880 --> 00:15:59,700
The statistics behind that, the rules behind it and so on.

280
00:16:00,090 --> 00:16:05,580
But overall, we should have understood is how to use object oriented programming to create a simple

281
00:16:05,610 --> 00:16:06,330
application.

282
00:16:06,390 --> 00:16:12,660
The main things being here are the classes we created at the beginning, and then the fact that the,

283
00:16:12,690 --> 00:16:18,240
for instance, deck class could use instances of the card class and that the player class could hold

284
00:16:18,330 --> 00:16:21,840
instances of the card class as well within its card list.

285
00:16:21,990 --> 00:16:25,740
And the fact that then you can connect everything through some logic.

286
00:16:26,130 --> 00:16:27,030
Okay, thanks.

287
00:16:27,350 --> 00:16:31,710
And we'll see at the next lecture where we will begin discussing your second milestone project.
