1
00:00:05,430 --> 00:00:09,240
Welcome back everyone to your second milestone project.

2
00:00:09,290 --> 00:00:14,240
So we've now learned enough about Python with object oriented programming errors and exception handling

3
00:00:14,540 --> 00:00:18,900
as well as functions enough to actually start a larger set project.

4
00:00:18,920 --> 00:00:22,880
And this case is going to be our second Moulson project where we're going to be creating a black chess

5
00:00:22,880 --> 00:00:24,830
game with Python.

6
00:00:24,830 --> 00:00:28,100
Now you can approach this project one of a few different ways.

7
00:00:28,100 --> 00:00:32,150
First off if you're still feeling a little bit fuzzy and object oriented programming or you need more

8
00:00:32,150 --> 00:00:37,400
of a helping hand as you go along for your first large project we can do is just code along this project

9
00:00:37,400 --> 00:00:42,080
of the solutions lecture a lot of students really like that approach especially if they still feel a

10
00:00:42,080 --> 00:00:44,360
little bit too new to object oriented programming.

11
00:00:44,480 --> 00:00:49,100
They'll jump straight to the solutions video and then code along with that actual video.

12
00:00:49,160 --> 00:00:53,930
And it's a nice way for someone to help guide you on creating a larger project.

13
00:00:53,930 --> 00:00:58,430
The second approach is almost the complete opposite which is just to attempt the projects on your own.

14
00:00:58,580 --> 00:01:03,620
So he actually suggests that students who feel very confident in object oriented programming or who

15
00:01:03,620 --> 00:01:08,690
just like that extra challenge too to after reviewing this lecture just jump straight to a blank notebook

16
00:01:08,690 --> 00:01:14,780
or a blank script and get started debug testing Zao and play around with Python to see if he can build

17
00:01:14,780 --> 00:01:16,980
a game completely on your own.

18
00:01:17,000 --> 00:01:20,140
Then we kind of have an in-between which is the workbook.

19
00:01:20,180 --> 00:01:24,530
So just like the first Moulson project we actually have a full workbook which breaks down the entire

20
00:01:24,530 --> 00:01:31,430
project into smaller more Hanibal subproblems for you to basically help guide you along as you attempt

21
00:01:31,430 --> 00:01:32,360
this project.

22
00:01:32,360 --> 00:01:36,040
And then there's a solution's lecture that goes through that workbook as well.

23
00:01:36,050 --> 00:01:41,390
So again feel free to take the approach that is best suited for you depending on how you fill with the

24
00:01:41,390 --> 00:01:42,850
material that we just covered.

25
00:01:44,770 --> 00:01:48,880
So as I mentioned for this project you're going to be using object oriented programming to create a

26
00:01:48,880 --> 00:01:50,630
blackjack game with Python.

27
00:01:50,830 --> 00:01:54,790
So I want to quickly go over the main idea of the game discuss objects or the program should be used

28
00:01:54,790 --> 00:02:00,880
for this project and then give you a brief overview of the workbook notebook for this project.

29
00:02:01,100 --> 00:02:04,890
So for our version of the game it's going to be a simplified version of blackjack.

30
00:02:04,940 --> 00:02:09,080
We're only going to have a computer dealer and a human player and we're going to start with a normal

31
00:02:09,080 --> 00:02:13,760
deck of cards and you're going to create a representation of that deck with Python using object oriented

32
00:02:13,760 --> 00:02:15,610
programming.

33
00:02:15,640 --> 00:02:20,000
So let's quickly just go over what are actual game will look like this for people who are unfamiliar

34
00:02:20,080 --> 00:02:22,310
blackjack or if you are familiar blackjack.

35
00:02:22,320 --> 00:02:26,410
We're going to discuss things that we don't need to consider or we'll just ignore for our version of

36
00:02:26,410 --> 00:02:27,430
the game.

37
00:02:27,460 --> 00:02:31,870
So the very basic concept of the game we're going to implement with Python is that we have the computer

38
00:02:31,870 --> 00:02:37,040
dealer and then the human player at the keyboard and a deck of 52 cards.

39
00:02:37,070 --> 00:02:41,180
Then what happens is the human player they have a bankroll so you can hopefully already start thinking

40
00:02:41,180 --> 00:02:45,230
that they're going to have an attribute for their bankroll just like we discussed when we talked about

41
00:02:45,230 --> 00:02:49,120
creating a bank account for our previous object oriented programming challenge.

42
00:02:49,220 --> 00:02:53,570
And from that bankroll the human player is going to place a bet indicating whether or not they think

43
00:02:53,570 --> 00:02:56,120
they're going to win this set of hands.

44
00:02:57,690 --> 00:03:02,270
Then what happens is the player starts with two cards face up and the dealer starts with one card face

45
00:03:02,270 --> 00:03:10,610
up in one card face down the player goes first in the game play the player goal is to get closer to

46
00:03:10,610 --> 00:03:16,190
a total value of 21 than the dealer does and the total value is just the sum of the current face up

47
00:03:16,190 --> 00:03:17,980
cards the human player has.

48
00:03:18,010 --> 00:03:20,630
And there's two possible actions that a human player can take.

49
00:03:20,660 --> 00:03:25,490
They can either hit which is to receive another card from the deck or they can stay the stop receiving

50
00:03:25,490 --> 00:03:26,360
cards.

51
00:03:26,360 --> 00:03:32,420
We're going to ignore actions like insurance splits or Double Downs which happen in a real casino playing

52
00:03:32,420 --> 00:03:33,030
blackjack.

53
00:03:33,170 --> 00:03:34,890
But we want a simplified version of this game.

54
00:03:34,910 --> 00:03:39,890
So all the player has to be able to do is hit which is to grab a card from the deck and put it in their

55
00:03:39,890 --> 00:03:44,450
hand and then take you some there or stay which means stop receiving cards.

56
00:03:45,730 --> 00:03:47,810
So for example they can hit get a new card.

57
00:03:47,830 --> 00:03:49,260
And then they check with their summus.

58
00:03:49,300 --> 00:03:53,870
Remember they're trying to get closer to the value of 21 than the dealer will.

59
00:03:54,170 --> 00:03:58,370
So once the player has gone their turn has ended and then it's the computer's turn.

60
00:03:58,370 --> 00:04:04,190
If the player is still under 21 then the dealer hits until they either beat the player or the dealer

61
00:04:04,190 --> 00:04:08,620
busts so by bus we mean that you go over 21.

62
00:04:08,630 --> 00:04:12,050
So let's see a possible ways that the game can end.

63
00:04:12,050 --> 00:04:17,570
First off if the player keeps hitting before the computer even goes and they go over 21 then they bust

64
00:04:17,660 --> 00:04:21,590
and they lost or bet the game is then over and the dealer collects the money.

65
00:04:22,320 --> 00:04:25,010
So that's one way that the game can end which is the player busting.

66
00:04:25,110 --> 00:04:31,570
And this is before the computer dealer even has to go the second way the game can end is if the computer

67
00:04:31,570 --> 00:04:32,650
beats the dealer.

68
00:04:32,650 --> 00:04:37,990
So what happens is the human player goes they start collecting cards and then they decide to eventually

69
00:04:37,990 --> 00:04:38,510
stay.

70
00:04:38,530 --> 00:04:43,390
So for example their current sum is 19 then the computer dealer goes and they start hitting cards so

71
00:04:43,390 --> 00:04:47,350
they collect cards from the deck and if they have a son that's higher than the human players and is

72
00:04:47,350 --> 00:04:48,580
still under 21.

73
00:04:48,730 --> 00:04:51,550
Then the computer has beat the dealer.

74
00:04:51,740 --> 00:04:55,880
And then finally the third possible option that the way the game could end is that the player actually

75
00:04:55,880 --> 00:04:56,280
wins.

76
00:04:56,300 --> 00:04:57,900
After the computer has its turn.

77
00:04:58,100 --> 00:05:02,390
So in our simplified version of the game what's going to happen is the computer dealer keeps hitting

78
00:05:02,540 --> 00:05:05,420
until they either beat the human player or until they bust.

79
00:05:05,600 --> 00:05:08,630
So eventually what may happen is to keep hitting and then they bust.

80
00:05:08,660 --> 00:05:10,820
I mean they go have a some over 21.

81
00:05:10,850 --> 00:05:14,660
And if that's the case the human player wins and they actually then doubled their bet money.

82
00:05:14,690 --> 00:05:15,970
And that goes to their bankroll.

83
00:05:15,980 --> 00:05:18,240
Now we have a happy human player.

84
00:05:18,800 --> 00:05:21,950
So let's talk about a few special rules that are going to have to implant.

85
00:05:21,950 --> 00:05:27,540
One is that face cards such as the Jack Queen and King are going to count as a value of 10.

86
00:05:27,830 --> 00:05:32,360
So we're going to be walking you through had actually create a representation of a deck of cards and

87
00:05:32,360 --> 00:05:35,740
those face cards are going to have a some value of 10.

88
00:05:36,050 --> 00:05:42,530
And then here's the tricky one is that aces can count as either a 1 or 11 whichever value is preferable

89
00:05:42,530 --> 00:05:43,480
to the player.

90
00:05:43,490 --> 00:05:49,640
So if you have an ace in your hand you can either treat it as a 1 or 11.

91
00:05:49,820 --> 00:05:53,650
You can check out the resource links for more explanations of blackjack and more information on that.

92
00:05:53,840 --> 00:05:57,600
But we're going to do now is explore the project itself and the workbook.

93
00:05:57,830 --> 00:06:00,240
Let's get started by jumping over to that repository.

94
00:06:00,530 --> 00:06:00,770
OK.

95
00:06:00,770 --> 00:06:02,720
So here we are at the repository.

96
00:06:02,750 --> 00:06:07,150
If you go to complete Python 3 bootcamp you should see a folder that has the milestone project.

97
00:06:07,160 --> 00:06:11,270
Keep in mind that the numbering might be a little different if we add folders in the future or rearrange

98
00:06:11,270 --> 00:06:16,660
things but the names should be the same milestone project to and what we see here are a few notebooks

99
00:06:16,670 --> 00:06:18,010
we have the assignment notebook.

100
00:06:18,090 --> 00:06:21,650
And if you click on the assignment notebook basically just describes what the blackjack game is going

101
00:06:21,650 --> 00:06:22,160
to look like.

102
00:06:22,160 --> 00:06:26,330
We create a simple text based blackjack game player Stan hit.

103
00:06:26,330 --> 00:06:27,600
Keep track of players money.

104
00:06:27,610 --> 00:06:29,980
This is basically what I just described in those slides.

105
00:06:30,110 --> 00:06:33,500
And then the next couple of notebooks is a walk through steps workbook.

106
00:06:33,650 --> 00:06:37,040
We check that out here we see walk through steps workbook.

107
00:06:37,040 --> 00:06:41,220
It's basically a kind of set up guide to how you should try to approach this project.

108
00:06:41,270 --> 00:06:45,740
And these are the basic steps you should follow when creating the game a blank check and then if you

109
00:06:45,740 --> 00:06:51,620
keep scrolling down we have cells with some essentially skeleton code here for you to fill in so you

110
00:06:51,620 --> 00:06:53,690
can see we've created a class card for you.

111
00:06:53,690 --> 00:06:57,190
We have an empty method an empty string method for you to fill out.

112
00:06:57,290 --> 00:07:01,170
And then we have Step Three create a class next step for the help guide you.

113
00:07:01,220 --> 00:07:06,710
And it's kind of an in-between between trying to do it all on your own at first or from trying to just

114
00:07:06,710 --> 00:07:10,300
jump straight and try to as a long project although that's totally fine.

115
00:07:10,430 --> 00:07:12,240
Either of those approaches is ok too.

116
00:07:12,260 --> 00:07:16,020
We try to give you as many options as possible for different learning styles.

117
00:07:16,040 --> 00:07:20,840
So this is a workbook just like you have for the first milestone project where we slowly guide you through

118
00:07:20,840 --> 00:07:23,240
the various steps of creating this game.

119
00:07:23,450 --> 00:07:28,010
And at the very end you're going to have what is essentially just a while loop here that goes through

120
00:07:28,010 --> 00:07:29,890
all the steps of the game.

121
00:07:29,980 --> 00:07:31,940
Then after that the other two notebooks.

122
00:07:31,940 --> 00:07:35,440
One is the solution note book that we're going to be going over in the next lecture.

123
00:07:35,570 --> 00:07:40,970
And then we also have a solution code notebook and that's basically all the code that is in this workbook.

124
00:07:40,970 --> 00:07:42,790
For the solutions in one cell.

125
00:07:42,800 --> 00:07:47,720
So in case you prefer using Sublime Text Editor or pi charm or something that uses a dot PI script you

126
00:07:47,720 --> 00:07:53,090
can use that solution code that we don't need a copy and paste from all the various cells in the workbook.

127
00:07:53,090 --> 00:07:53,580
OK.

128
00:07:53,720 --> 00:07:55,300
So that's basically it.

129
00:07:55,340 --> 00:07:57,080
Again you kind of have three approaches here.

130
00:07:57,100 --> 00:08:00,420
Treat it as a code along lecture by just jumping straight to the solutions.

131
00:08:00,530 --> 00:08:04,880
And I would recommend doing that if you feel a little uneasy still some of the object oriented programming

132
00:08:04,880 --> 00:08:05,480
topics.

133
00:08:05,570 --> 00:08:09,300
This is pretty much the largest piece of code you're going to be writing so far.

134
00:08:09,320 --> 00:08:14,990
So a lot of students tend to prefer that code along project the other way you can do it is by going

135
00:08:14,990 --> 00:08:15,970
through the workbook itself.

136
00:08:15,980 --> 00:08:20,360
The one we just described here seeing how far you can get reference's solutions when you need to.

137
00:08:20,360 --> 00:08:24,170
And then the third way for more experienced programmers maybe people coming in from another language

138
00:08:24,410 --> 00:08:28,670
or people who just want that extra challenge open up a fresh notebook and just get started and see how

139
00:08:28,670 --> 00:08:29,830
far you can go.

140
00:08:29,840 --> 00:08:32,000
Again we're always here to help out in the community forums.

141
00:08:32,000 --> 00:08:34,450
If you have any questions you can feel free to ask there.

142
00:08:34,640 --> 00:08:37,240
We'll see you at the next lecture where we begin going over the solutions.
