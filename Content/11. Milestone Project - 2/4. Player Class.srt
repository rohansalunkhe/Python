1
00:00:05,470 --> 00:00:08,440
Welcome, everyone, to this lecture on the player class.

2
00:00:10,070 --> 00:00:15,800
The player class will be used to hold a players current list of cards a player should be able to add

3
00:00:15,860 --> 00:00:17,480
or remove cards from their hand.

4
00:00:17,510 --> 00:00:19,640
That is their list of card objects.

5
00:00:21,050 --> 00:00:26,630
And we're going to want the player to be able to either add a single card or multiple cards to their

6
00:00:26,630 --> 00:00:27,110
list.

7
00:00:27,650 --> 00:00:30,130
And we're going to explore how to do this in a single method.

8
00:00:30,180 --> 00:00:30,470
Call.

9
00:00:31,960 --> 00:00:37,660
The last thing we need to think about is translating a deck or hand of cards that has a top and bottom

10
00:00:37,690 --> 00:00:38,860
to a python list.

11
00:00:39,460 --> 00:00:41,470
Now, we're going to try to visualize this, to explain it.

12
00:00:41,770 --> 00:00:47,530
But the basic premise is if you have a real physical deck of cards in your hand, then you have a top

13
00:00:47,590 --> 00:00:48,220
and bottom.

14
00:00:48,310 --> 00:00:51,700
And when you want to draw a card, you typically draw from the top.

15
00:00:51,940 --> 00:00:56,020
And if you're going to add cards that you won in the game of war, you're going to put them at the bottom

16
00:00:56,020 --> 00:00:56,620
of the deck.

17
00:00:57,100 --> 00:00:59,200
Now, a python list goes from left to right.

18
00:00:59,350 --> 00:01:03,430
So essentially, we're going to do is translate top of bottom to left and right.

19
00:01:03,460 --> 00:01:05,890
Using a python list and using indexing.

20
00:01:06,040 --> 00:01:10,810
Well, we know zero is towards the left and negative one is all the way towards the right or the very

21
00:01:10,810 --> 00:01:11,500
last item.

22
00:01:11,950 --> 00:01:13,000
So let's visualize this.

23
00:01:14,510 --> 00:01:16,410
The player class will have a self-taught.

24
00:01:16,490 --> 00:01:20,120
All cards list very similar to what we saw in the deck class.

25
00:01:21,080 --> 00:01:25,290
And for simplicity here, I'm just going to create a list called Cards, a BNC.

26
00:01:25,730 --> 00:01:29,000
And let's explore the different methods of adding and removing cards.

27
00:01:29,480 --> 00:01:33,380
So I have three cards here, A, B and C, AIZA Index zero.

28
00:01:33,740 --> 00:01:41,150
And C is an index to or really index negative one or negative one is the very last item in the list,

29
00:01:41,210 --> 00:01:42,650
regardless of how long it is.

30
00:01:44,230 --> 00:01:49,030
So for a BNC, let's say a player is going to play a single card.

31
00:01:49,450 --> 00:01:51,880
We want to play it from the top of their hand.

32
00:01:52,900 --> 00:01:57,130
So that means when the player plays a single card, we're going to use pop.

33
00:01:57,310 --> 00:02:02,650
However, we'll specify that pop is at zero, which is the very first item on the list that we want

34
00:02:02,650 --> 00:02:03,130
to pop.

35
00:02:03,670 --> 00:02:06,910
So that means we're left with cards is equal to B and C..

36
00:02:07,390 --> 00:02:12,010
So when a player plays a single card, which is the way we're gonna do this, they're going to pop at

37
00:02:12,010 --> 00:02:12,400
zero.

38
00:02:14,410 --> 00:02:16,780
Now, let's say a player is going to add a single card.

39
00:02:17,140 --> 00:02:19,780
We want to add this card to the bottom of the list.

40
00:02:20,050 --> 00:02:25,930
Now, lucky for us, if we're going to add something to the bottom of this deck or the end of a list,

41
00:02:26,380 --> 00:02:31,840
then that's simply just using the append method because by default, a pen is going to go to the end

42
00:02:31,930 --> 00:02:32,560
of a list.

43
00:02:32,920 --> 00:02:34,960
So I'll simply say a pen than in this case.

44
00:02:35,200 --> 00:02:36,450
This card is called W..

45
00:02:36,730 --> 00:02:39,580
And now my cards in my hand is B, C, W.

46
00:02:41,130 --> 00:02:44,590
Now, let's imagine that we went back to my hand being B.C..

47
00:02:44,970 --> 00:02:50,120
However, now I need to add in multiple cards because let's say I won a very large round the four.

48
00:02:50,520 --> 00:02:54,420
And now there's multiple cards on the table that I now have to add in back to my hand.

49
00:02:54,840 --> 00:02:58,470
So I have cards is B and C and new is X anzi.

50
00:03:00,800 --> 00:03:07,190
So what I will do here is I will use the extend method, because the extend method allows me to now

51
00:03:07,190 --> 00:03:13,520
have a list B, C, X, Z, and where all of those are going to be a single item.

52
00:03:13,880 --> 00:03:18,800
So Exten takes a list and then essentially merges it with the existing list.

53
00:03:18,860 --> 00:03:20,690
So this is cardstock extend new.

54
00:03:21,320 --> 00:03:23,780
Where if we looked back, knew was X and Z.

55
00:03:24,890 --> 00:03:29,400
Now, this sometimes often confuses students because they were thinking of just using a pen again.

56
00:03:30,290 --> 00:03:31,980
However, we cannot use a pen.

57
00:03:32,510 --> 00:03:33,080
Why not?

58
00:03:33,470 --> 00:03:39,230
Because if we were to append a list, suddenly I'm getting a nested list and that's going to mess stuff

59
00:03:39,230 --> 00:03:45,410
up when I start trying to draw cards again because I'll end up drawing a list instead of a single card.

60
00:03:45,950 --> 00:03:50,780
So do not use a pen in this case because you're going to have some nested lists, which is going to

61
00:03:50,780 --> 00:03:54,140
ruin the logic later on of removing cards from your hand.

62
00:03:54,440 --> 00:03:56,170
So that's why we're going to use extend.

63
00:03:57,380 --> 00:04:01,010
OK, let's get started and head to a Jupiter notebook and actually code this out.

64
00:04:02,700 --> 00:04:05,250
All right, here in the notebook where we left off last time.

65
00:04:05,990 --> 00:04:08,790
And let's begin creating our player class.

66
00:04:09,690 --> 00:04:10,550
First things first.

67
00:04:10,560 --> 00:04:13,410
We need our instantiation method underscore, underscore.

68
00:04:13,910 --> 00:04:15,930
I in I.T. underscore, underscore.

69
00:04:16,680 --> 00:04:17,970
And this is going to take self.

70
00:04:18,180 --> 00:04:21,630
And then we have to think, what should a brand new created player have?

71
00:04:22,080 --> 00:04:24,090
Well, in order to distinguish one player from another.

72
00:04:24,540 --> 00:04:26,640
Let's go ahead and have each player have a name.

73
00:04:27,240 --> 00:04:30,870
So we'll say self-taught name is equal to name.

74
00:04:31,890 --> 00:04:35,640
Each new player should also have what is essentially an empty hand.

75
00:04:36,630 --> 00:04:42,690
So I'll create an attribute called all cards, similar to the attribute in the deck class.

76
00:04:42,870 --> 00:04:44,730
And it's gonna start off as an empty list.

77
00:04:45,720 --> 00:04:50,940
And then we want a player to be able to do is remove cards and add cards to this.

78
00:04:51,060 --> 00:04:54,060
All cards list well, essentially their current hand of cards.

79
00:04:54,600 --> 00:05:01,740
So, again, make sure we check our puncture or excuse me, or indentation here and then we'll say remove

80
00:05:01,740 --> 00:05:02,160
one.

81
00:05:03,820 --> 00:05:10,270
And right now just have passed there and then I'll create a method for also adding cards.

82
00:05:11,860 --> 00:05:18,400
Has self, and then it should probably take whatever new cards are going to be added and then we'll

83
00:05:18,400 --> 00:05:20,890
have a pass there, they're just as filler for right now.

84
00:05:21,370 --> 00:05:28,600
And let's go ahead and create a string method now where we can print out a player and it's really up

85
00:05:28,600 --> 00:05:30,740
to you what you want to return for the print statement.

86
00:05:31,480 --> 00:05:36,790
But in this case, let's go ahead and I'm going to use an F string literal here to essentially fill

87
00:05:36,790 --> 00:05:37,630
out information.

88
00:05:38,170 --> 00:05:42,160
I will say player and then call self but name.

89
00:05:42,400 --> 00:05:45,160
So essentially it's going to grab that player's name.

90
00:05:46,030 --> 00:05:48,190
So we'll say player self that name has.

91
00:05:49,060 --> 00:05:51,250
And let's go ahead and report how many cards they have.

92
00:05:51,700 --> 00:05:55,000
So that should be the length of self-taught.

93
00:05:56,740 --> 00:05:57,520
All cards.

94
00:05:58,090 --> 00:06:01,000
And then outside of this bracket, I'll say cards.

95
00:06:01,650 --> 00:06:04,510
So let's first check that our instantiation method works.

96
00:06:04,570 --> 00:06:05,980
And then we can print out a player.

97
00:06:07,050 --> 00:06:07,920
So we run this.

98
00:06:08,370 --> 00:06:09,630
Let's create a new player.

99
00:06:11,680 --> 00:06:15,280
So this new player will be an instance of a player object.

100
00:06:15,730 --> 00:06:16,950
And we'll give him the name Hosain.

101
00:06:18,680 --> 00:06:20,960
And then let's go ahead and print out.

102
00:06:22,510 --> 00:06:23,200
This new player.

103
00:06:25,360 --> 00:06:29,680
TAB autocomplete there run this and it says player who's a has zero cards.

104
00:06:29,890 --> 00:06:30,070
OK.

105
00:06:30,140 --> 00:06:31,510
Looks like everything's working so far.

106
00:06:32,020 --> 00:06:36,280
So now we have to decide how do we remove a card from this list of cards?

107
00:06:36,430 --> 00:06:39,070
Well, in this case, we can simply do just like we did before.

108
00:06:39,790 --> 00:06:41,980
We'll say return self-taught.

109
00:06:43,500 --> 00:06:45,720
All cards and say, Pop.

110
00:06:46,320 --> 00:06:53,190
Now, remember, what I need to do here is specify pop at zero, because when I remove one card from

111
00:06:53,190 --> 00:06:57,360
the list, like we previously discussed at the beginning of this lecture, I want to make sure it happens

112
00:06:57,360 --> 00:06:59,520
from the top or from the beginning of the list.

113
00:07:00,090 --> 00:07:02,420
Now it's time to figure out how to add cards.

114
00:07:03,300 --> 00:07:04,160
New cards.

115
00:07:04,230 --> 00:07:09,780
We're gonna say new cards can either be a single card object or a list of card objects.

116
00:07:10,350 --> 00:07:12,340
So we'll do an if statement here.

117
00:07:12,830 --> 00:07:14,910
We'll say if this is a list.

118
00:07:15,330 --> 00:07:23,220
So if the type of new cards is equal to the type of an empty list, which is a list, then we have multiple

119
00:07:23,220 --> 00:07:24,180
cards that we're adding.

120
00:07:24,780 --> 00:07:26,010
So I will say self-taught.

121
00:07:28,290 --> 00:07:29,190
All cards.

122
00:07:31,960 --> 00:07:35,890
Not extend that list of new cards.

123
00:07:37,040 --> 00:07:44,120
Now, if we don't have a list of cards and we have a single card, so we'll say else self-taught.

124
00:07:45,950 --> 00:07:50,420
All cards append new cards.

125
00:07:50,840 --> 00:07:52,670
So what this is doing is this first one.

126
00:07:53,210 --> 00:07:57,560
This is for a list of multiple card objects.

127
00:07:58,610 --> 00:08:03,530
And the second one is for a single card object.

128
00:08:04,720 --> 00:08:12,580
OK, so we're able to now remove a single card and now we're able to add multiple cards at once or just

129
00:08:12,670 --> 00:08:13,600
add in a single card.

130
00:08:14,200 --> 00:08:15,430
So we'll go ahead and run this.

131
00:08:16,070 --> 00:08:17,530
Let's go ahead and create that new player.

132
00:08:18,590 --> 00:08:20,360
New player still has zero cards.

133
00:08:20,510 --> 00:08:22,880
So let's go ahead and add a card to them.

134
00:08:23,600 --> 00:08:24,620
We'll say new player.

135
00:08:26,120 --> 00:08:26,990
At cards.

136
00:08:27,740 --> 00:08:31,250
And now we're gonna do is if we take a look up here.

137
00:08:31,880 --> 00:08:33,320
Recall that we have this.

138
00:08:33,410 --> 00:08:34,070
My card.

139
00:08:34,130 --> 00:08:34,640
Object.

140
00:08:34,730 --> 00:08:35,960
Let's make sure it's actually working.

141
00:08:37,310 --> 00:08:43,850
So we'll say my card lips looks like it's not fine and looks has no space or underscore there.

142
00:08:44,780 --> 00:08:45,170
There we go.

143
00:08:45,410 --> 00:08:45,570
OK.

144
00:08:45,620 --> 00:08:47,090
So we have this card object.

145
00:08:47,510 --> 00:08:48,620
And if we print it out.

146
00:08:51,090 --> 00:08:52,410
It's the six of spades.

147
00:08:52,740 --> 00:08:55,920
So what I will do here is in this call, new player at the ad cards.

148
00:08:56,310 --> 00:08:58,680
I'm going to add my card.

149
00:09:00,060 --> 00:09:02,190
And now if I take a look at new player.

150
00:09:03,950 --> 00:09:11,240
I have this player object, and if I print the new player, it says player has a now has one card.

151
00:09:11,900 --> 00:09:16,040
And so what I could do then is say new player.

152
00:09:16,670 --> 00:09:19,280
All cards at index zero.

153
00:09:20,380 --> 00:09:23,510
Print it out and you'll notice I should get the six of spades.

154
00:09:23,870 --> 00:09:24,350
Perfect.

155
00:09:24,770 --> 00:09:29,990
Now let's go ahead and add a multiple six of spades just to make sure it's working here.

156
00:09:30,020 --> 00:09:33,230
We're going to say new player ad cards.

157
00:09:34,280 --> 00:09:37,730
And then going to create a list here of my card.

158
00:09:39,000 --> 00:09:44,730
My card, my card essentially grabbing that separate card object that we made, which was a six spades,

159
00:09:45,030 --> 00:09:46,560
and I'm going to add it multiple times.

160
00:09:46,710 --> 00:09:51,840
We won't really be doing this in the game logic because there's only one instance of each unique card.

161
00:09:52,170 --> 00:09:57,120
But in this case, essentially, we're imagining that I'm duplicating that six of spades outside and

162
00:09:57,120 --> 00:09:58,770
then I keep adding it to my hand.

163
00:09:59,850 --> 00:10:05,850
OK, so now I should have four cards that original six spades that I added and then these three more.

164
00:10:06,000 --> 00:10:12,660
And what's nice is I can add this as a single card object or as a list of card object due to the fact

165
00:10:12,780 --> 00:10:18,630
that I'm checking here if it's a list to extend or if it's a single one to append, which means if I

166
00:10:18,630 --> 00:10:19,080
print out.

167
00:10:20,110 --> 00:10:23,800
My new player right now says player Hosie has four cards.

168
00:10:24,070 --> 00:10:24,520
Perfect.

169
00:10:24,880 --> 00:10:27,220
Let's make sure that our removal method works.

170
00:10:27,250 --> 00:10:28,180
We'll say a new player.

171
00:10:29,320 --> 00:10:31,960
Remove one open and close parentheses.

172
00:10:32,650 --> 00:10:33,150
Run that.

173
00:10:33,580 --> 00:10:35,860
It basically returns back that card object.

174
00:10:36,340 --> 00:10:38,000
And now let's print the new player again.

175
00:10:38,110 --> 00:10:39,730
And they should only have three cards now.

176
00:10:40,330 --> 00:10:40,990
And there it is.

177
00:10:41,410 --> 00:10:41,620
All right.

178
00:10:41,980 --> 00:10:44,110
So looks like our player class is working.

179
00:10:44,470 --> 00:10:45,610
We can add a single card.

180
00:10:45,820 --> 00:10:46,990
We can add multiple cards.

181
00:10:47,110 --> 00:10:51,910
And we can clearly remove a single card later on due to the way we write the game logic.

182
00:10:52,240 --> 00:10:55,060
We'll see that we don't actually need to remove multiple cards.

183
00:10:55,240 --> 00:10:58,990
Instead, we'll just use a for loop to remove as many cards as necessary.

184
00:10:59,590 --> 00:11:02,830
Coming up next, we'll begin working on the actual logic of the game.

185
00:11:03,220 --> 00:11:03,790
I'll see you there.
