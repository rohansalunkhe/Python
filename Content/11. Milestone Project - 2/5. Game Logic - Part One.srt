1
00:00:05,360 --> 00:00:08,840
Welcome, everyone, to finish off this war card game project.

2
00:00:09,170 --> 00:00:11,810
We have to actually continue with the game logic.

3
00:00:11,960 --> 00:00:12,910
So this is part one.

4
00:00:12,930 --> 00:00:17,330
We're going to be visualizing the logic and in parts two and three will actually coded out.

5
00:00:19,160 --> 00:00:24,020
So keep in mind that creating the overall logic is actually often the hardest part of a project like

6
00:00:24,020 --> 00:00:24,410
this.

7
00:00:25,070 --> 00:00:31,110
It's also really important to note that in general, we plan the classes around the upcoming logic.

8
00:00:31,140 --> 00:00:36,080
So in a real world situation, you're often thinking above the logic that your application or in this

9
00:00:36,080 --> 00:00:40,520
case a simple game will have along with the class structures at the same time.

10
00:00:41,090 --> 00:00:46,580
And the reason for that is you'll notice that as we actually go throughout this game logic, the classes

11
00:00:46,580 --> 00:00:48,080
we've created fit perfectly.

12
00:00:48,260 --> 00:00:54,530
And that's because we designed the logic along with the classes when creating this notebook and project

13
00:00:54,530 --> 00:00:56,000
for you in general.

14
00:00:56,000 --> 00:01:00,650
In the real world, you wouldn't just randomly create classes and then hope they kind of fit into your

15
00:01:00,650 --> 00:01:01,040
logic.

16
00:01:01,370 --> 00:01:05,750
You're actually going to be thinking about both of them at the same time as you sketch out your logic.

17
00:01:05,960 --> 00:01:10,700
You'll realize what classes are necessary and then you'll start to construct those as well.

18
00:01:11,120 --> 00:01:13,640
So keep that in mind in a real world situation.

19
00:01:13,730 --> 00:01:18,860
You're typically working on both the logic and the class structures more or less simultaneously in order

20
00:01:18,860 --> 00:01:21,080
to make sure that they're a good fit for each other.

21
00:01:21,800 --> 00:01:25,880
And the last note is at the end of this actual warm up project notebook.

22
00:01:26,210 --> 00:01:31,040
You'll notice there's a bunch of links to other different implementations of the war card game you can

23
00:01:31,040 --> 00:01:31,520
check out.

24
00:01:31,970 --> 00:01:36,890
For instance, some of those implementations, instead of having or in addition to a player class,

25
00:01:37,190 --> 00:01:42,980
they have a table class or the table class represents what cards are actually currently on the table.

26
00:01:43,250 --> 00:01:46,250
So that may be something you may want to explore as well.

27
00:01:46,750 --> 00:01:50,030
Before our case, our classes should be enough to play the simple game.

28
00:01:51,300 --> 00:01:53,220
OK, so let's outline our logic for the game.

29
00:01:53,550 --> 00:01:54,690
We're going to do this visually.

30
00:01:55,380 --> 00:02:00,770
So the first thing we have to do is create two instances of the player class player one, player two.

31
00:02:00,900 --> 00:02:02,790
And essentially, these will both be computers.

32
00:02:03,660 --> 00:02:06,540
And then we're going to create an instance of a new deck.

33
00:02:06,750 --> 00:02:09,930
And recall, a new deck has everything perfectly in order.

34
00:02:10,290 --> 00:02:14,490
So when we actually split the deck between the players, we want to make sure that we call the shuffle

35
00:02:14,490 --> 00:02:15,300
method first.

36
00:02:15,540 --> 00:02:20,160
Once the deck has been shuffled and it's in random order, then we can split it between player one and

37
00:02:20,160 --> 00:02:20,820
player two.

38
00:02:21,360 --> 00:02:23,920
There's lots of different ways we could do this in our case.

39
00:02:23,940 --> 00:02:29,160
We'll just use a simple for loop to kind of stack half the deck to player one and have to deck the player

40
00:02:29,160 --> 00:02:29,490
to.

41
00:02:31,220 --> 00:02:37,100
So now that the players each have have to deck, it's time to actually go through the game to know whether

42
00:02:37,100 --> 00:02:41,210
we have to go through the game logic or not, we should probably check to see if somebody has already

43
00:02:41,210 --> 00:02:42,520
lost at the beginning.

44
00:02:42,530 --> 00:02:46,970
We know this won't be the case, but we can have this check anyways, since we're going to be since

45
00:02:46,970 --> 00:02:49,340
we're going to be calling it after each round.

46
00:02:50,120 --> 00:02:53,550
So here we're going to check throughout that Python list member.

47
00:02:53,630 --> 00:02:58,490
Each player has a python list of the cards and we essentially check do they still have cards left?

48
00:02:58,580 --> 00:03:00,350
Is the length of this list equal to zero?

49
00:03:00,740 --> 00:03:05,030
If it's not, then we can have some boolean value called the game on equal to true.

50
00:03:05,390 --> 00:03:07,280
And we know the game can continue playing.

51
00:03:07,670 --> 00:03:11,330
So obviously at the first round, both players still have cards.

52
00:03:11,390 --> 00:03:12,470
So we continue with the game.

53
00:03:13,600 --> 00:03:19,060
And so now we're gonna have kind of this outer while loop called game on or wild game on, and we can

54
00:03:19,060 --> 00:03:23,800
either say game on is equal to false or we can even call break to break out of this while loop.

55
00:03:24,070 --> 00:03:25,450
Once a player has lost.

56
00:03:25,630 --> 00:03:28,480
But in this case, both players still have cards.

57
00:03:28,570 --> 00:03:29,680
So the game still goes.

58
00:03:31,050 --> 00:03:32,040
So what happens now?

59
00:03:32,430 --> 00:03:38,060
Each player draws a card or removes a card, and then we do a comparison against those cards.

60
00:03:39,260 --> 00:03:42,740
Depending on who won that comparison in this case will assume it's not a tie.

61
00:03:43,190 --> 00:03:46,910
Then that player wins both those cards and we add both those cards.

62
00:03:47,180 --> 00:03:51,830
Technically, those cards should go to the bottom of that stack, as we previously discussed.

63
00:03:52,430 --> 00:03:55,430
However, recall, there's the case for war.

64
00:03:55,580 --> 00:04:01,250
Essentially, what happens when both players draw a card and those cards happen to be equal to each

65
00:04:01,250 --> 00:04:01,550
other?

66
00:04:02,150 --> 00:04:07,880
In this case, we have the instance of war, which means the players need to draw additional cards.

67
00:04:08,570 --> 00:04:11,660
This can vary depending on what game rules you use.

68
00:04:11,930 --> 00:04:14,600
What we're gonna do is we're gonna draw an additional three cards.

69
00:04:15,560 --> 00:04:20,510
So we're also going to have a while at war loop within the wild game on.

70
00:04:21,050 --> 00:04:25,490
Now, you may be thinking, why do I have to do a while loop for at war?

71
00:04:25,940 --> 00:04:30,170
And the reason for that is because sometimes you could get two wars in a row.

72
00:04:30,710 --> 00:04:34,490
You can have two cars that are equal to each other, draw the extra cards.

73
00:04:34,580 --> 00:04:39,260
And then upon drawing that new card for the new matchup, you may get another tie.

74
00:04:39,380 --> 00:04:46,610
So in which case, it's easier to program this out thinking of while at war, where at war is conditioned

75
00:04:46,700 --> 00:04:52,460
on whether or not the current match up has the cards equal to each other so that we don't need to worry

76
00:04:52,490 --> 00:04:55,130
about having to deal with multiple times in a row.

77
00:04:55,520 --> 00:05:01,220
So we just say while at war, you're going to keep drawing those extra cards and then do the comparison

78
00:05:01,610 --> 00:05:03,950
so we can do this comparison then over and over again.

79
00:05:04,200 --> 00:05:10,940
If we have multiple times in a row, so let's say in this case, the tie is ended and one of these players

80
00:05:10,940 --> 00:05:11,470
has one.

81
00:05:11,540 --> 00:05:13,850
We'll go ahead and say player two has one.

82
00:05:13,940 --> 00:05:16,640
So now they get all those cards added to their hand.

83
00:05:17,180 --> 00:05:22,250
And then before we continue with that wild game on loop, we're going to check has a player lost in

84
00:05:22,250 --> 00:05:22,650
this case?

85
00:05:22,670 --> 00:05:24,950
Looks like player one has zero cards.

86
00:05:25,040 --> 00:05:27,530
So then we can say something like game on is equal to false.

87
00:05:27,890 --> 00:05:31,940
Or alternatively, you could just use break to break out of that upper while loop.

88
00:05:32,330 --> 00:05:34,680
And in this case, then we announced player two has won.

89
00:05:35,410 --> 00:05:37,490
OK, so that's the logic for the game.

90
00:05:37,940 --> 00:05:41,120
What we're gonna do in the next series of lectures is actually program it out.

91
00:05:41,600 --> 00:05:42,140
I'll see you there.
