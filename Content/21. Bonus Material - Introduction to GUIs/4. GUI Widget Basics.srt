1
00:00:05,670 --> 00:00:07,680
Welcome back everyone in this lecture.

2
00:00:07,680 --> 00:00:12,330
We're going to continue building off our previous understanding of interact and interactive to begin

3
00:00:12,390 --> 00:00:13,740
using full widgets.

4
00:00:13,920 --> 00:00:18,810
Let's get started by opening up a Jupiter notebook to begin using our widget framework.

5
00:00:18,830 --> 00:00:32,130
We need to say import I pi widgets as widgets so widgets have their own display functionality.

6
00:00:32,180 --> 00:00:34,590
When you say widgets make sure we spell that right.

7
00:00:35,750 --> 00:00:39,340
Dots and then the actual type of widget you want.

8
00:00:39,380 --> 00:00:41,470
You notice that there's tons of options here.

9
00:00:41,480 --> 00:00:45,200
We're going to show you a few of them and we'll start off with the first one that we've kind of already

10
00:00:45,200 --> 00:00:47,820
talked about which is the integer slider.

11
00:00:48,290 --> 00:00:53,750
So if I just call this by itself I get back an integer slider with some sort of default from 0 minimum

12
00:00:53,750 --> 00:00:56,310
to 100 maximum and then I believe it starts at zero.

13
00:00:56,320 --> 00:00:58,280
There.

14
00:00:58,500 --> 00:01:02,700
Now right now we're just calling the widget directly in a cell and seeing its output below.

15
00:01:02,850 --> 00:01:08,790
But I want to call it or line up a bunch of widgets to call later on when I need to do is call display

16
00:01:08,880 --> 00:01:11,100
on the widget that's been defined.

17
00:01:11,100 --> 00:01:15,390
So what I need to do is say W is equal to this particular widget.

18
00:01:16,050 --> 00:01:27,230
And then from Python does display import display and then I can display my widget.

19
00:01:28,070 --> 00:01:32,060
And so now I have this variable that I can display wherever I want an integer slyer.

20
00:01:32,070 --> 00:01:36,000
If you do shift tab here and expand it it will tell you the various parameters value.

21
00:01:36,040 --> 00:01:38,640
Min max step that it takes.

22
00:01:38,640 --> 00:01:43,740
So if we want to do multiple display calls for example if we want to display the same widget twice the

23
00:01:43,740 --> 00:01:48,580
displayed instances in the front end will actually remain in sync for each other.

24
00:01:48,600 --> 00:01:54,980
So let me show you what I mean by that if I say display w again here and I start moving them around.

25
00:01:54,990 --> 00:01:59,100
These are actually in sync because they're referring to the same widget.

26
00:01:59,130 --> 00:02:02,040
W is just connected to a singular integer slider.

27
00:02:02,040 --> 00:02:04,830
So now I can see how these widgets are connected.

28
00:02:05,130 --> 00:02:11,970
If I want to close it which is what I need to do is call its close method so I can say w.

29
00:02:12,050 --> 00:02:13,260
Close.

30
00:02:13,410 --> 00:02:19,200
And now the switch has been closed and I no longer see it at any of the cells where it was being displayed.

31
00:02:19,200 --> 00:02:20,940
Now we just also have properties.

32
00:02:20,970 --> 00:02:25,180
So all of the Python which is share some sort of similar name in Scheme.

33
00:02:25,290 --> 00:02:29,370
And to read the value of a widget you can query its value property.

34
00:02:29,370 --> 00:02:38,860
So what I want to do is reconnect that widget so say W is equal to widgets the integer slider

35
00:02:41,530 --> 00:02:49,250
and then display it w and while Dobias be displayed that means I can play around with its current value.

36
00:02:49,500 --> 00:02:55,950
So if I do w that tab here I can see that it has some variables that I can call and one of them is the

37
00:02:55,950 --> 00:03:00,190
value so I can see the current value it was that when the cell was run.

38
00:03:00,220 --> 00:03:01,820
No this is not going to be updated.

39
00:03:01,950 --> 00:03:08,460
If I start moving it around I would have to rerun this to then check on the current value I can reset

40
00:03:08,460 --> 00:03:12,700
the value though saying something like w that value is equal to 50.

41
00:03:12,720 --> 00:03:16,920
And when I rerun this notice that it's connected to this current display.

42
00:03:16,950 --> 00:03:21,540
So this display suddenly goes back to 50 when you reassign that value.

43
00:03:21,540 --> 00:03:25,480
So in addition to value most widgets actually share keys.

44
00:03:25,480 --> 00:03:29,690
So you can call w that keys and you'll get back a list of keys here.

45
00:03:29,700 --> 00:03:33,830
So this is the entire synchronized list of stateful properties.

46
00:03:33,870 --> 00:03:38,850
And by Stiefel properties I mean these are the properties are connected to the State of the current

47
00:03:38,850 --> 00:03:39,570
display.

48
00:03:39,570 --> 00:03:44,640
That way we can actually grab that property and reassign it and it should affect the current widget

49
00:03:44,670 --> 00:03:47,430
display.

50
00:03:47,450 --> 00:03:55,510
So here I can see for example underneath my keys I had a max value so I could say w that Max and let's

51
00:03:55,510 --> 00:03:57,170
put that equal to 2000.

52
00:03:57,170 --> 00:04:02,300
So now when I run this and it come back up here if I scroll all the way back up it's now been changed

53
00:04:02,300 --> 00:04:03,590
to 2000.

54
00:04:03,680 --> 00:04:09,740
So I can grab the list the keys and then see the stateful values here that I can play around with a

55
00:04:09,740 --> 00:04:16,580
lot of these we won't really play around with the main ones like Max Min step and then other options

56
00:04:16,580 --> 00:04:19,890
depending on what kind of widget you're actually playing around with.

57
00:04:20,000 --> 00:04:22,700
So we can then link to similar widgets.

58
00:04:22,700 --> 00:04:28,130
So if you ever need to display the same value to different ways we can do is link two different widgets

59
00:04:28,130 --> 00:04:29,150
together.

60
00:04:29,270 --> 00:04:30,600
Let me show what I mean by that.

61
00:04:30,620 --> 00:04:43,610
Let's create a that is float text which means if I just display a I get back this text box which takes

62
00:04:43,610 --> 00:04:51,160
in floating point values instead of just a normal text then I'm going to create inside the same cell

63
00:04:53,160 --> 00:04:57,860
widgets and I create a float slider.

64
00:04:58,010 --> 00:04:59,760
And now it's display.

65
00:05:00,170 --> 00:05:04,190
Let me close this off right here with the X. let's display a comma.

66
00:05:04,190 --> 00:05:08,170
Be here and when we run this notice right now they're not linked.

67
00:05:08,210 --> 00:05:13,390
If I raise this up and down I don't see a change change in value here.

68
00:05:13,400 --> 00:05:16,590
And if I change the text box value I don't see it reflect the change here.

69
00:05:16,850 --> 00:05:27,580
What I can do is link them up by saying after display my link is equal to widgets and you can call it

70
00:05:27,640 --> 00:05:35,320
j s link for a javascript link and what you do here is you say grab the name of the widget and the string

71
00:05:35,320 --> 00:05:37,030
key of what you actually want to connect.

72
00:05:37,030 --> 00:05:40,720
In our case we want to connect the value.

73
00:05:40,720 --> 00:05:45,940
So we're going to do is passing the string value here and then you pass in a list of what else you want

74
00:05:45,940 --> 00:05:51,850
to connect is a tuple where it's a widget and then the key you want to connect so value.

75
00:05:51,880 --> 00:05:54,490
So if I run this now and I start moving this up and down.

76
00:05:54,520 --> 00:05:56,320
Notice how they're connected.

77
00:05:56,380 --> 00:06:02,530
So if I type in a number here it's going to be reflected in the slider itself.

78
00:06:02,570 --> 00:06:07,240
So if we get to 100 you can see how it's changing or if I slide this you'll see how it's changing.

79
00:06:07,240 --> 00:06:12,730
So again you create two widgets you display them and then you can link and it doesn't have to just be

80
00:06:12,730 --> 00:06:17,110
for two which is going to be for two or more you call which is thought Jaslene.

81
00:06:17,230 --> 00:06:20,440
And then you pasand tuples of the widget name.

82
00:06:20,590 --> 00:06:22,860
And then the key of what you actually want to link.

83
00:06:22,900 --> 00:06:28,120
So all we could do here is instead of linking value with value even though it's a very common link we

84
00:06:28,120 --> 00:06:38,410
could link a max together so I could say linked the value of a to be the max of B.

85
00:06:38,410 --> 00:06:41,980
And then when I rerun this right now my value is zero.

86
00:06:41,980 --> 00:06:43,630
So that's the max of my slider.

87
00:06:43,780 --> 00:06:51,070
But if I change my value to 1000 then suddenly my slider can go up to 1000 then if I change it back

88
00:06:51,070 --> 00:06:54,860
to 100 then I can only go up to 100.

89
00:06:54,910 --> 00:06:59,770
So this gives you an idea of how you can link the different keys of one widget to the different key

90
00:06:59,860 --> 00:07:00,720
of another widget.

91
00:07:00,760 --> 00:07:04,820
And it's just we just thought Jay Eslick if you want to unlink the widgets.

92
00:07:04,840 --> 00:07:06,370
It's actually quite simple.

93
00:07:06,370 --> 00:07:11,770
You just call my link again and then call the method unlink.

94
00:07:11,770 --> 00:07:13,410
Run that and other no longer linked.

95
00:07:13,420 --> 00:07:18,340
If I start moving this around or if I start changing this it doesn't actually affect the original link

96
00:07:18,360 --> 00:07:19,250
anymore.

97
00:07:19,710 --> 00:07:20,180
OK.

98
00:07:20,320 --> 00:07:22,910
So let's quickly review what we just covered.

99
00:07:23,130 --> 00:07:24,020
We talked about widgets.

100
00:07:24,030 --> 00:07:29,220
We've actually seen a lot of this in the previous lecture but we saw how we could call display on a

101
00:07:29,220 --> 00:07:30,730
widget in order to see it.

102
00:07:30,990 --> 00:07:36,990
And then we also noticed that we just themselves have these keys that allow us to view their stateful

103
00:07:36,990 --> 00:07:37,620
properties.

104
00:07:37,620 --> 00:07:44,760
And again the stateful properties that term is just to say that's a property that shows up when you

105
00:07:44,760 --> 00:07:46,030
view the actual widget.

106
00:07:46,110 --> 00:07:51,180
So it's a state property that you can then edit or change and then you can link widgets together using

107
00:07:51,180 --> 00:07:55,320
this widget that G-S link and then passen the which itself.

108
00:07:55,560 --> 00:08:00,580
And then what you want to link in which case what key you want to link such as the value or here which

109
00:08:00,580 --> 00:08:02,330
it be link the Macs together.

110
00:08:02,370 --> 00:08:05,550
And if you ever want to unlink them you just call unlink.

111
00:08:05,880 --> 00:08:08,550
That's it for the lecture on which it's basics.

112
00:08:08,550 --> 00:08:10,970
Up next we're going to talk about widget list.

113
00:08:10,980 --> 00:08:11,880
We'll see you there.
