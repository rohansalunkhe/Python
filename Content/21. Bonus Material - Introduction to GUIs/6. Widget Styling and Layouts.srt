1
00:00:05,490 --> 00:00:07,430
Welcome back everyone in this lecture.

2
00:00:07,440 --> 00:00:12,300
We're going to learn about the various ways to style widgets and we'll discuss how to use the style

3
00:00:12,330 --> 00:00:16,610
versus the layout functionality of the API widget system.

4
00:00:16,620 --> 00:00:19,170
Let's open up a Jupiter notebook and get started.

5
00:00:19,170 --> 00:00:23,670
First off before we continue I want to mention that there's actually quite a bit of information in this

6
00:00:23,670 --> 00:00:28,010
which is styling notebook as well as discussion on style versus layout.

7
00:00:28,050 --> 00:00:34,560
A lot of what we're going to talk about here assumes some knowledge of each T.M. or CSSA as far as the

8
00:00:34,560 --> 00:00:35,650
terms are concerned.

9
00:00:35,850 --> 00:00:40,500
So if you're already familiar of H T.M. and C Ss a lot of these terms such as border margin padding

10
00:00:40,740 --> 00:00:47,000
top left bottom flex box those We're going to sound familiar to you if you're totally new to that.

11
00:00:47,130 --> 00:00:53,400
Just keep in mind that the layout and styling is based on H.M.S. SS So you may want to learn about those

12
00:00:53,400 --> 00:00:58,680
first through just some simple web tutorials or in other course of mine on Django of development and

13
00:00:58,680 --> 00:01:01,240
then come back to understand layout here fully.

14
00:01:01,320 --> 00:01:06,390
We are going to show you some examples that should make sense even if you don't know H.M.S. assess that

15
00:01:06,390 --> 00:01:18,200
well we're going to get started by importing IPA widgets as widgets and then from Python dot display

16
00:01:19,350 --> 00:01:21,960
import display.

17
00:01:22,170 --> 00:01:25,010
Then I will create my widget slider that we've seen before.

18
00:01:26,350 --> 00:01:28,900
So an integer slider.

19
00:01:29,130 --> 00:01:32,580
And then let's display that integer slider.

20
00:01:32,610 --> 00:01:35,660
So right now this is the default integer slider display.

21
00:01:35,880 --> 00:01:39,360
But let's imagine that actually wanted to change the size of the slider.

22
00:01:39,360 --> 00:01:41,010
I wanted to change the way it looks.

23
00:01:41,010 --> 00:01:48,720
What I can do is grab w and then grab Leo off of this and if I hit tab here you'll notice you have a

24
00:01:48,720 --> 00:01:52,590
bunch of traits based off of CSSA that you can't adjust.

25
00:01:52,620 --> 00:02:01,610
So forget for example you can adjust the margin to go the string auto and calling lay out again.

26
00:02:01,620 --> 00:02:08,380
You could adjust the height of the slider to be defined by a certain number of pixels like 75 pixels.

27
00:02:08,490 --> 00:02:13,680
And now when you run this you'll notice that we've been able to shift this around with a margin and

28
00:02:13,680 --> 00:02:14,250
the height.

29
00:02:14,250 --> 00:02:21,960
So the layout here basically allows you to center the slider and then increase the height so the slider

30
00:02:21,960 --> 00:02:23,700
change positions on the page immediately.

31
00:02:23,790 --> 00:02:28,060
And let's say it can be passed from one widget to another widget of the same type.

32
00:02:28,200 --> 00:02:34,760
So I going say x is equal to widgets dot integer slider.

33
00:02:35,000 --> 00:02:43,700
And let's give this a value of 15 and also give it a description to say something like new slider and

34
00:02:43,700 --> 00:02:45,410
then I will display X.

35
00:02:45,500 --> 00:02:48,920
So here's my new slider looks like it has default styling.

36
00:02:49,130 --> 00:02:54,770
But if I wanted to change the layout to match lay all I would need to do is say X thought lay out is

37
00:02:54,770 --> 00:03:01,660
equal to w that layout and then we have centered on you slider.

38
00:03:01,670 --> 00:03:06,350
So before we investigate the actual style attribute it should be noted that many widgets offer a list

39
00:03:06,350 --> 00:03:10,930
of predefined styles that can be passed in as arguments or parameters during their creation.

40
00:03:10,970 --> 00:03:17,510
And these are based on bootstrap terms such as primary success in full warning and danger when I'm going

41
00:03:17,510 --> 00:03:20,370
to do here is already importing the widgets.

42
00:03:20,410 --> 00:03:26,380
I will call widgets dot button Slick's make sure I spell we right.

43
00:03:26,730 --> 00:03:32,080
We just stop button and then I will pass the description here.

44
00:03:33,130 --> 00:03:38,620
Well say ordinary button.

45
00:03:38,840 --> 00:03:44,150
And then we can say the button style is equal to an empty string.

46
00:03:44,150 --> 00:03:47,810
So if I just do that and run that I have this ordinary button.

47
00:03:47,840 --> 00:03:50,040
And we've actually seen this one before.

48
00:03:50,240 --> 00:03:56,900
What it could do though is give it a style based on a bootstrap code and these codes are linked to in

49
00:03:56,900 --> 00:03:58,200
the notebook.

50
00:03:58,280 --> 00:04:04,640
So danger is kind of this danger red but there's other things like warning is an orange or you have

51
00:04:04,730 --> 00:04:10,220
info which is another color this kind of teal color et cetera.

52
00:04:10,340 --> 00:04:14,770
And you can check out the various labels over here priories success info.

53
00:04:15,260 --> 00:04:17,450
So now let's talk about the style attribute.

54
00:04:17,510 --> 00:04:20,360
So those with the built in ones that are based off of bootstrap.

55
00:04:20,360 --> 00:04:28,010
So while the laser attribute really only exposes layout related CSI properties for top level elements

56
00:04:28,310 --> 00:04:32,780
the style attribute is used to expose non layout related styling attributes.

57
00:04:32,780 --> 00:04:39,150
So those are things like custom colors so I'm going to make it a couple of your cells here so I will

58
00:04:39,150 --> 00:04:42,950
say BE1 is equal to widgets.

59
00:04:43,030 --> 00:04:52,930
The button and I will say description and we can say something like custom color and then I will grab

60
00:04:52,930 --> 00:05:01,300
one and instead of calling L'eau offer this I will call style and then call button color and then say

61
00:05:01,360 --> 00:05:05,010
light green and there's a bunch of string codes you can use here.

62
00:05:05,140 --> 00:05:06,490
And if I just called me one.

63
00:05:06,580 --> 00:05:08,590
Now I see this custom color.

64
00:05:09,040 --> 00:05:13,420
And if you're interested in the list of all the style attributes available for any particular widget

65
00:05:13,690 --> 00:05:18,070
so certain widgets will have colors such as buttons other widgets or sliders are going to have button

66
00:05:18,070 --> 00:05:18,860
colors.

67
00:05:19,390 --> 00:05:26,010
But if we check out BE1 style keys we get to see a keys of everything we can edit.

68
00:05:26,050 --> 00:05:29,790
So not a whole lot here can edit the font weight the button color.

69
00:05:29,860 --> 00:05:32,650
And that's really all there is for buttons.

70
00:05:33,620 --> 00:05:42,350
Now we could do as well if we had a new button such as B2 is equal to widgets button I could set the

71
00:05:42,410 --> 00:05:48,920
style of BE2 to be completely equal to the style of be one note here and then actually calling style

72
00:05:48,930 --> 00:05:49,530
button color.

73
00:05:49,550 --> 00:05:53,560
But I'm just saying the entire style would be to match the entire style be one.

74
00:05:53,570 --> 00:05:56,840
So then I called B to get back like green here.

75
00:05:56,840 --> 00:05:59,650
So it doesn't actually say anything because I didn't pass in the scription.

76
00:05:59,810 --> 00:06:08,080
So let's say a description is equal to knew and then when I read this I could see the new there.

77
00:06:08,210 --> 00:06:14,870
So finally we'll create some other let's say slider here.

78
00:06:14,900 --> 00:06:19,870
I'll say widgets integer slider.

79
00:06:20,020 --> 00:06:28,100
Let's give it description to be something like my handle.

80
00:06:28,860 --> 00:06:31,960
And if I check what S1 looks like it's just this normal slider.

81
00:06:32,060 --> 00:06:36,710
But let's go ahead and check it style will say S1 style.

82
00:06:36,800 --> 00:06:45,060
And if you call S1 here style dot tab you should see a bunch of options and you can then call keys to

83
00:06:45,060 --> 00:06:46,080
see a list everything.

84
00:06:46,100 --> 00:06:48,170
So for example you can change the handle color.

85
00:06:48,410 --> 00:06:55,760
So let's grab that say handle color and set that equal to let's say blue run that and I can see that

86
00:06:55,770 --> 00:07:01,890
little handle is now colored blue and pretty much any major string is going to be accepted.

87
00:07:02,150 --> 00:07:05,610
I should say any common string so you can see black is accepted.

88
00:07:05,630 --> 00:07:08,140
It could go if light blue.

89
00:07:08,330 --> 00:07:10,940
We can go of light blue and that would be accepted as well.

90
00:07:11,240 --> 00:07:12,820
And as quick note.

91
00:07:12,880 --> 00:07:16,900
If you come back down here to the notebook all the way down we have some treats here you that are common

92
00:07:16,990 --> 00:07:20,290
for widgets such as Button color and font weight.

93
00:07:20,290 --> 00:07:22,910
Those are conferee buttons description with handle color.

94
00:07:22,930 --> 00:07:25,560
Those are for all the sliders and them for the progress bars.

95
00:07:25,630 --> 00:07:28,430
You may want to check the bar color or description with.

96
00:07:28,430 --> 00:07:32,620
There's lots of other ones that really only have description with as in just apple tree.

97
00:07:33,000 --> 00:07:33,290
OK.

98
00:07:33,310 --> 00:07:36,040
So now should I have an understanding of how to style widgets.

99
00:07:36,040 --> 00:07:40,990
Keep in mind you don't have that much access to a lot of the styling and that's because when you're

100
00:07:40,990 --> 00:07:46,930
working of Jupiter Jupiter has to be able to only color the button here or the handle here and not affect

101
00:07:46,960 --> 00:07:48,780
everything else that's showing in your browser.

102
00:07:48,970 --> 00:07:53,470
So it's quite a challenge for the people who are developing IPA widgets to give you access to this sort

103
00:07:53,470 --> 00:07:57,790
of styling without screwing up everything that was being rendered in your browser.

104
00:07:57,790 --> 00:07:58,030
All right.

105
00:07:58,030 --> 00:08:01,340
Coming up next we're going to talk about is how to create a custom widget.

106
00:08:01,390 --> 00:08:06,820
So we're going to explore just the huge custom example to see what do all of these widgets in functionality

107
00:08:06,850 --> 00:08:08,640
look like in practice.

108
00:08:08,650 --> 00:08:09,340
We'll see if they're.
