1
00:00:05,320 --> 00:00:07,120
Welcome back everyone in this lecture.

2
00:00:07,120 --> 00:00:11,740
We're going to show you a custom which example and we're actually just going to be running the notebook

3
00:00:11,850 --> 00:00:16,660
directly for this example so you can go ahead and download the notebook and then run through it for

4
00:00:16,660 --> 00:00:17,250
this notebook.

5
00:00:17,260 --> 00:00:19,800
You will need to install a few extra libraries.

6
00:00:19,930 --> 00:00:21,240
And those are some pie.

7
00:00:21,250 --> 00:00:23,600
Matt Plaut lib and site pie and we'll show you those.

8
00:00:23,620 --> 00:00:28,460
Just a second in the notebook but let's open up the notebook and explore this example.

9
00:00:28,900 --> 00:00:30,810
OK here we are at the Custom widget.

10
00:00:30,880 --> 00:00:35,680
This is basically going to be in advanced example of what's possible in the custom widget to try to

11
00:00:35,680 --> 00:00:41,350
give you some ideas of how you would be able to connect these widgets to external libraries and in order

12
00:00:41,350 --> 00:00:47,140
to explore those external libraries we're going to need to work with some pie map lib and site pie and

13
00:00:47,140 --> 00:00:52,900
you can install all of those using condo install and then these names non-pilot matplotlib or Sibe pie

14
00:00:53,200 --> 00:00:54,520
or pipin stall.

15
00:00:54,580 --> 00:00:58,190
Pie Pipp installment Cutlip or Pipp install site at your command line.

16
00:00:58,300 --> 00:01:03,070
And if you're really interested in this sort of thing of the more advanced aspects of Python basically

17
00:01:03,070 --> 00:01:05,960
you've already finished the course by now reviewing this lecture.

18
00:01:06,070 --> 00:01:09,970
You can check out the class Python for data science machine learning boot camp if you're interested

19
00:01:09,970 --> 00:01:15,730
in visualization and differential equations machine learning data science analyzing data of python and

20
00:01:15,730 --> 00:01:16,680
that sort of thing.

21
00:01:16,810 --> 00:01:21,760
Here we're just going to show you one final Vance example of how you would be able to use widgets with

22
00:01:21,760 --> 00:01:25,720
some of these more advanced libraries and to do that we're going to be exploring the Lorenz system of

23
00:01:25,720 --> 00:01:27,010
differential equations.

24
00:01:27,010 --> 00:01:31,960
Basically this is just a system of equations that are connected based on the behavior of three different

25
00:01:31,960 --> 00:01:32,720
parameters.

26
00:01:32,830 --> 00:01:37,390
And we're going to connect those two widgets to actually modify those parameters and see how that changes

27
00:01:37,390 --> 00:01:39,380
the visualization in real time.

28
00:01:40,670 --> 00:01:42,400
So we have a couple of lines here.

29
00:01:42,450 --> 00:01:48,040
Matt plotless in line that allows us to actually view this visualization in the notebook setting that

30
00:01:48,050 --> 00:01:50,640
we have or IP widgets and I put it on display.

31
00:01:50,810 --> 00:01:55,100
Here we have interactive Interactive which have already talked about we have display in HTL which We've

32
00:01:55,100 --> 00:02:01,530
also already discussed and then clear output which we discussed during milestone one project and then

33
00:02:01,530 --> 00:02:03,790
we have some tools here from those external libraries.

34
00:02:03,810 --> 00:02:05,930
And this is really just for visualization purposes.

35
00:02:06,060 --> 00:02:11,760
We have non-pilot as we downloaded installed Type-I and then we have mapped plotted and mapped plot

36
00:02:11,760 --> 00:02:16,170
lib is basically a plotting library for Python which will allow us to plot some of this out.

37
00:02:16,210 --> 00:02:17,690
So then we're going to run this.

38
00:02:18,330 --> 00:02:19,810
And here is what we're going to do.

39
00:02:19,830 --> 00:02:25,690
And you can basically take this idea and apply it to any sort of function that's returning anything.

40
00:02:25,830 --> 00:02:31,770
What we have here is a function called solve Loren's that takes in various parameters that relate to

41
00:02:31,770 --> 00:02:32,940
the Lorentz equations.

42
00:02:33,210 --> 00:02:35,180
And then it creates a figure.

43
00:02:35,460 --> 00:02:38,430
It's going to set the limits of that figure.

44
00:02:38,470 --> 00:02:42,780
It has another function here that is actually going to compute the time derivative of the Lawerence

45
00:02:42,780 --> 00:02:46,860
system using some math then we choose some random starting points.

46
00:02:46,860 --> 00:02:51,470
We actually solve the equation that we saw up above or that set of equations.

47
00:02:51,630 --> 00:02:56,000
We choose some different colors and then we end up just plotting everything per line.

48
00:02:56,010 --> 00:03:00,840
I definitely would expect you to understand any of this because this is all pie in that plot lib this

49
00:03:00,840 --> 00:03:04,980
morning to show you what is possible with Python and if you're really interested in this you can check

50
00:03:04,980 --> 00:03:09,540
out the other course that's linked to that notebook so let's run that sell.

51
00:03:09,630 --> 00:03:15,010
And it's going to return t and x of T and then we're going to call the function solve.

52
00:03:15,030 --> 00:03:17,010
So once it's solved it looks like this.

53
00:03:17,140 --> 00:03:19,740
Lotusphere it's taking parameters angle and end.

54
00:03:19,980 --> 00:03:25,680
But if we wanted to see how those different parameters actually effected some sort of function or we

55
00:03:25,680 --> 00:03:31,140
would have to do is call interactive pass that function itself and then pass in our options here.

56
00:03:31,140 --> 00:03:33,750
Remember using tuples as slider's.

57
00:03:33,750 --> 00:03:36,440
So those are the widget abbreviations.

58
00:03:37,260 --> 00:03:42,870
Then we're going to display W and when we run this we get to see all the sliders here and we actually

59
00:03:42,870 --> 00:03:45,530
see how they affect the equation itself.

60
00:03:45,540 --> 00:03:49,830
So for instance we can change the angle and that will make it look like it's routine because we're changing

61
00:03:49,830 --> 00:03:55,950
the angle that we're plotting yet or we can change the number of lines using and just 2 or and all the

62
00:03:55,950 --> 00:04:01,110
way up to 43 and we can explore that again with the angle or with the max time.

63
00:04:01,110 --> 00:04:04,920
See how that affects things change Bayda change Sigma.

64
00:04:05,110 --> 00:04:08,090
You can change RHO has lots of different parameters here.

65
00:04:08,130 --> 00:04:13,710
The main idea being is you just have a function that's returning something and then you pass in whatever

66
00:04:13,710 --> 00:04:16,060
parameters that you want to play around with.

67
00:04:16,140 --> 00:04:17,910
And this is a really advanced example of that.

68
00:04:18,000 --> 00:04:22,980
But we've already worked with tons of functions that return something and taken prompts from the user.

69
00:04:22,980 --> 00:04:30,570
And if you ever wanted a slider parameter or those text checkboxes text entering etc. all those other

70
00:04:30,630 --> 00:04:34,950
widgets that we discussed you would just pass them in here and then they would be connected to whatever

71
00:04:34,950 --> 00:04:36,050
function and passed then.

72
00:04:36,180 --> 00:04:38,680
And this is kind of an advanced example of that.

73
00:04:38,700 --> 00:04:41,820
Now the object returned by Interactive is actually a widget object.

74
00:04:42,090 --> 00:04:46,860
So then you can grab the result off of that and grab the arguments.

75
00:04:46,860 --> 00:04:49,380
So we see here this is the arguments right now.

76
00:04:49,500 --> 00:04:51,770
And then we would be able to explore those further.

77
00:04:51,810 --> 00:04:57,390
So you'd be able to plot out histograms of wherever you actually were in that particular time period

78
00:04:57,930 --> 00:05:05,040
and then if you edited some of the more advanced parameters Sigma changes around and and then you rerun

79
00:05:05,040 --> 00:05:06,160
these again.

80
00:05:06,220 --> 00:05:11,670
Notice now you have the different angles that you had on then you could just keep going with this and

81
00:05:11,670 --> 00:05:14,310
make new Hister ramps to plot them out.

82
00:05:14,370 --> 00:05:14,800
All right.

83
00:05:14,820 --> 00:05:16,350
That's it for this lecture.

84
00:05:16,350 --> 00:05:17,980
Really this section of the course.

85
00:05:18,150 --> 00:05:24,050
I hope this helped you realize what is actually possible using gudis and PI widgets.

86
00:05:24,060 --> 00:05:29,100
The main idea being that once you have a function that's going to return something you can easily give

87
00:05:29,100 --> 00:05:33,740
user interaction but just setting those parameters and then connecting them to some sort of widget.

88
00:05:33,750 --> 00:05:35,360
In this case are all connected sliders.

89
00:05:35,480 --> 00:05:41,820
But we've already explored the many other widgets available such as buttons checkboxes text boxes expandable

90
00:05:41,820 --> 00:05:43,820
text boxes straight H.T. mail.

91
00:05:43,830 --> 00:05:47,560
And then we also saw how we could style and lay out our widgets OK.

92
00:05:47,700 --> 00:05:52,110
Hope you have fun exploring your own use cases for graphical user interfaces and the Jubran notebook

93
00:05:52,160 --> 00:05:52,590
system.
