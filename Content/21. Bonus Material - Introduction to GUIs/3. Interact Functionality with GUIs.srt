1
00:00:05,620 --> 00:00:13,630
Welcome everyone to this bonus section on Python Julys or graphical user interfaces with IPA widgets.

2
00:00:13,850 --> 00:00:17,650
So we're going to show you how to build simple graphical user interfaces with Jupiter.

3
00:00:17,840 --> 00:00:23,210
And because we're going to do this exclusive live Jupiter that's why we consider this bonus material.

4
00:00:23,210 --> 00:00:27,800
There are lots of libraries out there for graphical user interfaces each with their pros and cons.

5
00:00:27,860 --> 00:00:32,720
There's the built in T.K. enter which doesn't look that great but it's built into Python.

6
00:00:32,720 --> 00:00:38,260
There's also solutions like pie cutesie which then if you actually want to use it as a product there's

7
00:00:38,270 --> 00:00:40,180
some licensing issues you need to deal with.

8
00:00:40,400 --> 00:00:45,050
But there's the open source IPA widgets and widgets styling structure of Jupiter.

9
00:00:45,200 --> 00:00:49,180
So that's why we decided to use it for this kind of bonus material section.

10
00:00:49,190 --> 00:00:52,040
So we're going to explore how to build interactive elements.

11
00:00:52,070 --> 00:00:57,290
These are things like buttons or sliders that you can have directly in a notebook for someone else to

12
00:00:57,290 --> 00:00:58,530
play around with.

13
00:00:58,580 --> 00:01:02,180
You're going to want to keep the electronic books handy because there's a lot of reference code and

14
00:01:02,180 --> 00:01:03,770
information there for you.

15
00:01:03,830 --> 00:01:07,430
So let's get started with our very first lecture in this topic.

16
00:01:07,460 --> 00:01:10,750
We're going to discuss the interactive functionality.

17
00:01:10,760 --> 00:01:12,460
Let's open up a super notebook.

18
00:01:12,790 --> 00:01:13,190
OK.

19
00:01:13,220 --> 00:01:18,830
So here I am at a Jupiter notebook as a very quick note for the notebooks that go along with this lecture

20
00:01:18,830 --> 00:01:20,420
that are hosted at the depot.

21
00:01:20,480 --> 00:01:26,030
You actually won't be able to see the sliders that we create or any of the interactive elements that

22
00:01:26,030 --> 00:01:27,190
we're going to be creating.

23
00:01:27,200 --> 00:01:29,560
That's because this is just hosting the code itself.

24
00:01:29,660 --> 00:01:31,080
It's not actually running it.

25
00:01:31,090 --> 00:01:35,560
So any viewer or get a hub will actually be able to execute the code here.

26
00:01:35,570 --> 00:01:37,540
They're just showing you the code itself.

27
00:01:37,550 --> 00:01:42,020
So keep that in mind if you come visit to the hub and you're wondering why you don't see sliders here.

28
00:01:42,110 --> 00:01:46,760
You need to download snow pack and run it you know book in order to actually see or view the widgets

29
00:01:46,790 --> 00:01:47,800
and interact with them.

30
00:01:47,810 --> 00:01:49,020
So keep that in mind.

31
00:01:49,070 --> 00:01:56,070
And again this is all under bonus material introduction to DUIs or gudis So let's get started with the

32
00:01:56,070 --> 00:02:04,990
Interact function in order to do that we need to say from IPA widgets import interact and you can use

33
00:02:04,990 --> 00:02:19,460
tab autocomplete here interactive and also fixed and then we'll say import I-Spy widgets as widgets

34
00:02:21,610 --> 00:02:26,340
so once we're done with those imports we're going to create probably the most basic interactive function.

35
00:02:26,410 --> 00:02:32,410
So the most basic level interact does it auto generates a user interface control for some sort of function

36
00:02:32,440 --> 00:02:37,330
argument and then it calls the function with those arguments that you can allow yourself to manipulate

37
00:02:37,510 --> 00:02:38,700
interactively.

38
00:02:38,740 --> 00:02:43,620
So to use a track we first need to define a function that we actually want to play around with or explore.

39
00:02:43,630 --> 00:02:48,640
So we're going to create the most basic function possible which is just well it's not the most basic

40
00:02:48,640 --> 00:02:50,770
function possible but it's definitely a basic one.

41
00:02:50,890 --> 00:02:53,930
You take an x and we're turnback x.

42
00:02:53,980 --> 00:03:00,310
So a super simple function here and then we're going to call interact and you pass and the function

43
00:03:00,310 --> 00:03:08,820
itself and then you set some default value for all the arguments of the function in this case there's

44
00:03:08,820 --> 00:03:14,690
one argument and then we run this we should see a slider here.

45
00:03:14,730 --> 00:03:22,290
So then know how as I'm sliding I have this x value and this is my input X I have highlighted here and

46
00:03:22,290 --> 00:03:23,570
this is my output x.

47
00:03:23,610 --> 00:03:28,540
So if I scroll around or slide around I get to see the actual results.

48
00:03:28,620 --> 00:03:30,290
So let's explore this concept further.

49
00:03:30,300 --> 00:03:34,430
Let's imagine I wanted to return the square of x.

50
00:03:34,470 --> 00:03:37,350
If I rerun my function and rerun interact here.

51
00:03:37,350 --> 00:03:39,180
Now I'm returning back to square at the bottom.

52
00:03:39,210 --> 00:03:45,060
So I can see here as my slider goes from negative 7 to 25 I'm just returning the square there.

53
00:03:45,240 --> 00:03:50,370
So I can see here how I can vary the input and then see some corresponding output.

54
00:03:50,370 --> 00:03:53,480
Let's go back to that simple function where we just return x.

55
00:03:53,970 --> 00:03:55,540
So we saw that we get a slider.

56
00:03:55,560 --> 00:03:57,000
We we're working with integers.

57
00:03:57,000 --> 00:04:00,330
Let's see what happens when we pasand booleans.

58
00:04:00,930 --> 00:04:06,250
So of booleans it auto creates a checkbox which makes sense because Blansett are true or false.

59
00:04:06,270 --> 00:04:12,450
So here now we automatically created the checkbox market check for true on market for false and then

60
00:04:12,450 --> 00:04:19,020
if we actually pass in a string such as Hello then I get back this auto text box.

61
00:04:19,030 --> 00:04:24,630
So right now this function is just accepting X and returning X and interact is smart enough to know

62
00:04:25,170 --> 00:04:29,090
what kind of feature it should show you based on the data type is being passed.

63
00:04:29,100 --> 00:04:32,740
So for integers or numbers it passes in a slider.

64
00:04:32,850 --> 00:04:38,490
We just saw four booleans that passes in a check box and then for strings it passes in a little text

65
00:04:38,490 --> 00:04:39,130
box here.

66
00:04:39,150 --> 00:04:42,020
So I can type whatever I want.

67
00:04:42,070 --> 00:04:44,450
You notice that this function is just returning x.

68
00:04:44,550 --> 00:04:46,690
So lots of cool functionality here.

69
00:04:46,710 --> 00:04:53,820
Now we're going to do is instead of passing in this function into interact we can actually do all this

70
00:04:53,880 --> 00:04:55,350
with a decorator.

71
00:04:55,350 --> 00:04:56,630
So let's see how we can do that.

72
00:04:56,640 --> 00:05:02,090
I'm going to click exit here to kind of end this I'll come back to the function.

73
00:05:02,160 --> 00:05:09,930
Let's create a new function called G and G takes an X and Y and then all it does.

74
00:05:09,950 --> 00:05:15,680
It just returns back a tuple of X and Y again a very simple function.

75
00:05:15,680 --> 00:05:19,940
Now what we could have done is pass this function into interact again and then provided something for

76
00:05:19,940 --> 00:05:21,430
x and something right.

77
00:05:21,470 --> 00:05:23,470
But we can do all of this with the decorator.

78
00:05:23,510 --> 00:05:28,890
And if you're unfamiliar with decorator's make sure to go back and review that decorator's lecture.

79
00:05:29,060 --> 00:05:31,040
But there's a decorator called interact.

80
00:05:31,490 --> 00:05:35,300
And here we can pasand the default values that we want to start off with.

81
00:05:35,360 --> 00:05:42,530
So I can say something like X is equal to true and Y is equal to a floating point let's say one point

82
00:05:42,530 --> 00:05:43,750
zero.

83
00:05:44,120 --> 00:05:48,620
And now when I run this I actually get back these options so I can say true false.

84
00:05:48,610 --> 00:05:49,820
That is my output here.

85
00:05:49,940 --> 00:05:52,300
And then I have this slider.

86
00:05:52,310 --> 00:05:55,460
Now let's imagine we actually wanted to keep one of these values fixed.

87
00:05:55,460 --> 00:05:59,050
We don't want everything to be manipulated by the user.

88
00:05:59,180 --> 00:06:04,990
Well we could do is pass in this parameter into the fixed function.

89
00:06:05,090 --> 00:06:11,030
So we would do here is to say why is equal to fixed 1.0.

90
00:06:11,110 --> 00:06:16,140
Make sure my princes balance up and then when you run this you no longer have the option to pass and

91
00:06:16,150 --> 00:06:18,480
why it's been fixed at one point zero.

92
00:06:18,500 --> 00:06:21,210
And this also works inside the Interact function.

93
00:06:21,260 --> 00:06:25,400
So if we rerun phunk here and I say x is equal to hello.

94
00:06:25,550 --> 00:06:29,100
And I say fixed hello.

95
00:06:29,220 --> 00:06:32,200
Then when I run this I don't get the option of that text box anymore.

96
00:06:32,250 --> 00:06:36,000
It just fixes this parameter to always be whatever that the fault was.

97
00:06:36,000 --> 00:06:38,220
So that's how you can use the fixed function.

98
00:06:38,400 --> 00:06:42,690
And so now we have the Interact function as well as the Interact decorator.

99
00:06:42,690 --> 00:06:45,410
Now let's talk about which abbreviations.

100
00:06:45,570 --> 00:06:53,760
Recall that when we said interact pass in our function and then set x is equal to let's say one we get

101
00:06:53,760 --> 00:06:59,750
back this value of a slider and I can go to some minimum and to see a maximum.

102
00:07:00,030 --> 00:07:01,320
But notice what's happening here.

103
00:07:01,350 --> 00:07:05,310
I can only go to a minimum of the negative one and to a maximum of three.

104
00:07:05,430 --> 00:07:12,840
And if I were to expand this to 10 Suddenly I can go to a max of 30 and a moona minimum a negative 10.

105
00:07:12,840 --> 00:07:18,970
So what happens is your minimum is defined by the X and your maximum is defined by x times 3.

106
00:07:19,050 --> 00:07:23,910
So you may be wondering well how do I actually change my minimum maximum and even my step size or my

107
00:07:23,910 --> 00:07:25,120
starting value.

108
00:07:25,380 --> 00:07:30,390
Well you can do is instead of just saying equal to 10 and have it automatically create this integer

109
00:07:30,390 --> 00:07:42,060
slider for you you can call widgets WIPs I.A. slider and then you can pasan if you do shift tab here

110
00:07:42,060 --> 00:07:48,360
and expand this you'll actually see the arguments pasan you can pass in a minimum maximum step and value.

111
00:07:48,360 --> 00:07:49,390
So let's do that as well.

112
00:07:49,400 --> 00:07:53,010
Remember this is an integer slider for step size needs to be an integer.

113
00:07:53,010 --> 00:07:55,720
There's also a float slider in case you want that.

114
00:07:55,980 --> 00:07:58,670
But now I can say my minimum goes to negative 100.

115
00:07:58,920 --> 00:08:01,360
My maximum goes to positive 100.

116
00:08:01,440 --> 00:08:03,900
We can create a step size of one.

117
00:08:04,320 --> 00:08:06,400
And then our starting value of 0.

118
00:08:06,420 --> 00:08:11,360
So now I have all these options with the integer slider and if we run this notice I'm starting at zero.

119
00:08:11,380 --> 00:08:16,480
I have a step size of one and I go from negative 100 all the way to positive 100.

120
00:08:16,500 --> 00:08:22,500
So these are the widgets that you can specifically call if you want more options and the functionality

121
00:08:22,500 --> 00:08:29,870
of the widgets instead of just the ones that are auto created for you now because sliders are so commonly

122
00:08:29,870 --> 00:08:35,040
used with widgets there's actually ways to abbreviate all this functionality here.

123
00:08:35,330 --> 00:08:42,350
And the way you do that is by calling interact passing your function and say x is equal to and it's

124
00:08:42,350 --> 00:08:48,400
very similar to a range we do you have a tuple here where you have a min max and a step size.

125
00:08:48,500 --> 00:08:55,670
So I could say go from negative 100 to 100 step size of one.

126
00:08:56,000 --> 00:09:01,290
And then when I run this notice back I get back the same thing from negative 100 to 100.

127
00:09:01,310 --> 00:09:07,640
Or I can go from negative 9:50 rerun this and here is negative tend to 10 step size of 1.

128
00:09:07,640 --> 00:09:14,060
This is known as an abbreviation of this which is right here because now I'm just passing in an abbreviated

129
00:09:14,060 --> 00:09:18,420
tuple instead of calling integer slider.

130
00:09:18,560 --> 00:09:19,850
This is an integer slider.

131
00:09:19,880 --> 00:09:25,130
And if we wanted this to be a floating point slider all you had to do was specify one of these to be

132
00:09:25,130 --> 00:09:30,340
a float here and then if we specify the rest to be floats as well.

133
00:09:30,420 --> 00:09:34,350
So say point 1 and we rerun this.

134
00:09:34,380 --> 00:09:38,060
Now Jupiter which is realizes oh he wants a foot slider.

135
00:09:38,180 --> 00:09:40,780
So then we have the floating point slider here.

136
00:09:41,210 --> 00:09:46,400
And notice now our step size is point 1 and we're going from negative ten point zero all the way to

137
00:09:46,400 --> 00:09:48,350
positive 10.0 and step sizes.

138
00:09:48,350 --> 00:09:49,610
Point 1 again.

139
00:09:49,640 --> 00:09:51,950
Using this abbreviated format.

140
00:09:52,310 --> 00:09:56,880
And as always you can do this with the interactive decorator as well.

141
00:09:56,900 --> 00:10:09,130
So if I say at interact I could say x is equal to a zero point zero to twenty point zero step size of

142
00:10:09,130 --> 00:10:17,910
0.5 create a function let's just have this function B H and we can have some default value like five

143
00:10:18,260 --> 00:10:23,020
point zero and then just return x.

144
00:10:23,040 --> 00:10:25,730
So then when I call it I have the default value of five point zero.

145
00:10:25,770 --> 00:10:31,450
It goes down to zero point zero all the way up to 20 steps 0.5.

146
00:10:31,840 --> 00:10:32,350
OK.

147
00:10:32,610 --> 00:10:35,890
So this is widget abreviation again lots of different options here.

148
00:10:37,680 --> 00:10:41,630
Those were the abbreviations for integers and floating point sliders.

149
00:10:41,670 --> 00:10:45,660
You can also have dropdown menus and those are abbreviated as well.

150
00:10:45,750 --> 00:10:53,670
Remember that if I said interact pass and my function and I said x is equal to a string such as Hello.

151
00:10:54,090 --> 00:10:58,590
It just has a text box here automatically generates a hello how are you.

152
00:10:58,620 --> 00:11:00,910
And it's just returning back that string.

153
00:11:01,140 --> 00:11:08,010
If you pass in a list of strings as an abbreviation of a for call.

154
00:11:08,160 --> 00:11:17,310
It will create a dropdown menu so it can say hello option to comma the string option 3.

155
00:11:17,630 --> 00:11:18,530
And now we run this.

156
00:11:18,530 --> 00:11:22,630
We get back this nice little dropdown menu option to hello.

157
00:11:22,700 --> 00:11:27,940
Option 3 so this abbreviation of using a list of strings is a dropdown menu.

158
00:11:27,950 --> 00:11:32,270
And we have a little table in the notebook so you can reference this and I have to memorize what tuples

159
00:11:32,270 --> 00:11:37,280
stand for what lists stand for but when you get used to it it's pretty straightforward.

160
00:11:37,280 --> 00:11:41,730
These tuples are slider's the list with strings is going to be a dropdown menu.

161
00:11:42,020 --> 00:11:48,600
And there's also when you pass in a dictionary the Mishu happens will say interact.

162
00:11:48,620 --> 00:11:58,890
Our simple function say X is equal to 1 10 and then we'll say to goes with 20.

163
00:11:59,060 --> 00:12:05,750
And when you run this you get back adopt them and you want to but then the actual output is equal to

164
00:12:06,290 --> 00:12:07,860
the value here.

165
00:12:07,880 --> 00:12:11,720
So your dropdown menu is the key and your output is the actual value.

166
00:12:11,870 --> 00:12:16,340
So notice the difference here between that list and that dictionary being passed in there.

167
00:12:16,350 --> 00:12:22,090
Now in addition to this interact function I Python or Jupiter in general provides another function called

168
00:12:22,150 --> 00:12:27,340
interactive instead of interact and it's useful when you want to reuse the widgets that are already

169
00:12:27,340 --> 00:12:31,840
been produced or access the data that is bound to the user interface controls.

170
00:12:32,080 --> 00:12:38,200
So unlike interact the return value of the function will not be displayed automatically but you can

171
00:12:38,200 --> 00:12:42,810
display a value inside the function using python that display.

172
00:12:42,820 --> 00:12:52,120
So you what I mean by all this say from a python that display import display and let's create a function

173
00:12:52,120 --> 00:13:00,970
here simple function f just takes an A and B and then instead of just returning a plus b We're also

174
00:13:00,970 --> 00:13:07,550
going to then call display on a plus b and this will allow us to use the interactive function.

175
00:13:08,050 --> 00:13:13,780
So I could say w equal to interactive pass an F again.

176
00:13:13,900 --> 00:13:18,130
And I will say is equal to 10 B is equal to 20.

177
00:13:18,730 --> 00:13:25,950
And if I check what type w is notice it's now an interactive object so the widget isn't interactive

178
00:13:26,340 --> 00:13:31,260
which if you look at the documentation is a subclass of something called the box which is just a container

179
00:13:31,260 --> 00:13:32,780
for other widgets.

180
00:13:32,790 --> 00:13:38,070
Now the children of interactive we check out the children attribute.

181
00:13:38,500 --> 00:13:42,490
These are actually two integer sliders here as well as an output.

182
00:13:42,520 --> 00:13:48,240
So it's basically building out a larger UI for you using the interactive functionality.

183
00:13:48,460 --> 00:13:53,950
So then if you actually want to finally see what W actually looks like what it was defined by this interactive

184
00:13:54,190 --> 00:13:59,890
function you can call display on w makes her spell that right.

185
00:14:01,370 --> 00:14:07,730
And then you get back both sliders so you get 27 negative two and you get to see the sumps 27 minus

186
00:14:07,730 --> 00:14:09,100
to 25.

187
00:14:09,380 --> 00:14:14,720
And I can see how we can use the interactive functionality to save something as a variable and then

188
00:14:14,720 --> 00:14:18,030
just display the variable later on.

189
00:14:18,070 --> 00:14:23,550
OK so that's really it for the interact and interactive functionality.

190
00:14:23,560 --> 00:14:26,610
Let's very briefly go over everything we just covered.

191
00:14:27,070 --> 00:14:32,440
The main idea here is that you're going to be able to create functions that taken some sort of input

192
00:14:32,560 --> 00:14:34,200
then return some sort of output.

193
00:14:34,420 --> 00:14:39,970
Then if you want your user to have some sort of interface to change the possible inputs and then see

194
00:14:39,970 --> 00:14:45,190
the outputs you can use the Interact function or you can use the interactive decorator.

195
00:14:45,340 --> 00:14:51,040
And then we saw how we could specify actual widgets inside of the Interact call itself.

196
00:14:51,430 --> 00:14:57,420
And we saw how there is abbreviations for them such as a tuple for a slider or the list for a dropdown

197
00:14:57,430 --> 00:15:05,160
menu or a dictionary for a dropdown menu that has a connection to some key value pairs.

198
00:15:05,200 --> 00:15:10,330
Then finally we saw how we could use the interactive functionality in order to save this to a variable.

199
00:15:10,330 --> 00:15:14,800
So I can later display that variable and again see the inputs and outputs.

200
00:15:14,800 --> 00:15:19,750
Last thing I want to note here is when you use that in book you may have noticed that there is this

201
00:15:20,020 --> 00:15:22,740
report here that the output is some sort of function.

202
00:15:22,930 --> 00:15:27,760
If you don't want to see that all you have to do is have a semi-colon here and then when you run this

203
00:15:27,790 --> 00:15:29,560
you will no longer see the output.

204
00:15:29,680 --> 00:15:32,540
And that's what's essentially happening when you call display on the W.

205
00:15:32,680 --> 00:15:35,740
You don't see that little output cell OK.

206
00:15:35,960 --> 00:15:40,090
We'll see you in the next lecture or we begin to discuss more capabilities of widgets.
