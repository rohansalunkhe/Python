1
00:00:05,340 --> 00:00:07,440
Welcome back everyone in this lecture.

2
00:00:07,440 --> 00:00:11,490
We're going to have a reference of a list of all the widgets available to you.

3
00:00:11,640 --> 00:00:16,620
So we're just going to actually run the corresponding note book that goes along with this lecture because

4
00:00:16,620 --> 00:00:19,460
we're just going to show you every possible widget that way later on.

5
00:00:19,470 --> 00:00:23,580
You can come back to this lecture come back to the notebook and reference it when you're thinking of

6
00:00:23,580 --> 00:00:24,790
a particular widget.

7
00:00:24,900 --> 00:00:26,600
So let's go through that notebook now.

8
00:00:26,880 --> 00:00:27,110
All right.

9
00:00:27,120 --> 00:00:29,330
Here I am at the widget list notebook.

10
00:00:29,340 --> 00:00:33,690
It's the third notebook underneath the bonus material introduction to goolies folder.

11
00:00:33,840 --> 00:00:39,680
And if you want a complete list of all the gooey widgets available to you just import EPA widgets as

12
00:00:39,720 --> 00:00:41,510
widgets and then for item.

13
00:00:41,510 --> 00:00:46,710
And here's the call widgets that widget dot widget underscored type stuff items and then you can print

14
00:00:46,740 --> 00:00:48,030
out item.

15
00:00:48,210 --> 00:00:53,850
If you just print out the item itself it'll give you this large information in the tuple about the actual

16
00:00:53,850 --> 00:00:54,940
class it's in.

17
00:00:54,960 --> 00:01:00,390
So just say item index that 0 and then you'll get back the actual items themselves.

18
00:01:00,420 --> 00:01:02,180
See here there's checkboxes.

19
00:01:02,340 --> 00:01:07,220
There's the sliders we were talking about color pickers picker's math.

20
00:01:07,230 --> 00:01:11,240
There's a lot of stuff here and what we're going to do is just explore a lot of these.

21
00:01:11,250 --> 00:01:14,440
So first off there's 10 numeric widgets in Python.

22
00:01:14,460 --> 00:01:17,280
So these are all designed to display some sort of the Merak value.

23
00:01:17,520 --> 00:01:18,860
There's the integer slider.

24
00:01:19,020 --> 00:01:23,290
And we've also shown you all the various parameters that can go with this.

25
00:01:23,340 --> 00:01:25,300
So the integer slider you can name it.

26
00:01:25,290 --> 00:01:28,350
So notice here how it has test as a string.

27
00:01:28,360 --> 00:01:29,590
So that's description.

28
00:01:29,660 --> 00:01:34,850
And you can edit that to change what text shows up here and you can also see that it has an orientation.

29
00:01:35,040 --> 00:01:40,260
And if you have the foot slider very similar thing except it's float and you notice that it has some

30
00:01:40,260 --> 00:01:42,500
sort of precision precisions So the step side can be a float.

31
00:01:42,660 --> 00:01:46,280
Unlike the integer we can also display these vertically.

32
00:01:46,280 --> 00:01:48,890
So there's an orientation split vertically.

33
00:01:48,970 --> 00:01:51,710
You get this sort of output.

34
00:01:51,760 --> 00:01:54,600
There's the range slider for an integer.

35
00:01:54,620 --> 00:01:56,900
So this allows you to actually grab a range.

36
00:01:56,900 --> 00:02:01,940
So notice here how there's two thoughts now a start and a stop and you can see which one you have selected.

37
00:02:02,150 --> 00:02:04,850
If it's filled in or the same thing for flow.

38
00:02:05,060 --> 00:02:05,970
Start stop.

39
00:02:05,990 --> 00:02:07,300
You can have a range there.

40
00:02:07,310 --> 00:02:09,580
It'll display back the range.

41
00:02:09,600 --> 00:02:13,850
There's integer progress so you can see here it's kind of like a loading bar so you can have something

42
00:02:13,850 --> 00:02:19,970
like this technically be updating as you are running some sort of program so you can see here as a description

43
00:02:20,090 --> 00:02:22,660
loading doesn't have to be loading whatever you want.

44
00:02:22,670 --> 00:02:26,350
There is also a bar style so you can replace it with the strings right here.

45
00:02:26,560 --> 00:02:28,410
If to see it in a different color.

46
00:02:28,410 --> 00:02:31,330
So if you say success I'll change it to green.

47
00:02:31,520 --> 00:02:38,100
If you say warning it will change it to orange and these colors are just from bootstraps if you're not

48
00:02:38,100 --> 00:02:39,360
familiar that strap.

49
00:02:39,380 --> 00:02:44,330
It's this very popular front and styling that you can use.

50
00:02:44,370 --> 00:02:50,960
And a lot of these keywords come from that bootstrap library so that's into your progress.

51
00:02:50,960 --> 00:02:54,490
Same thing as Flute progress you can see here it's loading.

52
00:02:54,680 --> 00:02:56,570
And then there's bound to integer attacks.

53
00:02:56,600 --> 00:03:02,770
So the numerical text boxes that impose some limit on the data so you can see here we have text and

54
00:03:02,830 --> 00:03:09,990
then we can say 8 enter and then it would end up connecting a flutter which you had or where function

55
00:03:09,990 --> 00:03:11,250
at which it was connected to.

56
00:03:11,720 --> 00:03:14,870
And then we have just entered your text and floating text.

57
00:03:15,060 --> 00:03:19,430
There's also boolean widgets there's basically three widgets are designed to display a boolean value.

58
00:03:19,500 --> 00:03:21,270
There's the toggle button.

59
00:03:21,470 --> 00:03:26,810
So click me click I'm on and off you can see here it's shaded again you can use the sort of bootstrap

60
00:03:26,810 --> 00:03:30,350
codes that change the actual styling as far as color is concerned.

61
00:03:30,350 --> 00:03:36,240
So now you have green click on and off will have the checkbox which you've seen before check on enough.

62
00:03:36,440 --> 00:03:39,470
And then we have valid which provides a read only indicator.

63
00:03:39,470 --> 00:03:41,080
So you can can't really interact with this.

64
00:03:41,090 --> 00:03:43,030
I'll just say whether or not it's valid.

65
00:03:43,040 --> 00:03:44,410
So this is to show something.

66
00:03:44,480 --> 00:03:46,730
If it's false they'll have an X there.

67
00:03:46,960 --> 00:03:54,270
And if we change this to true and run this you'll see it has a checkmark that we have selection widgets.

68
00:03:54,290 --> 00:04:00,170
So several widgets here like to select from a list you can see we have options as a parameter.

69
00:04:00,250 --> 00:04:03,680
The collator link up to are functions save the dropdown.

70
00:04:03,710 --> 00:04:08,520
One two three you can also use a dictionary here instead of a list.

71
00:04:08,530 --> 00:04:09,690
So one two three.

72
00:04:09,700 --> 00:04:13,290
And the output will correspond to the value going of that key.

73
00:04:13,480 --> 00:04:15,580
We have radio buttons that we can choose from.

74
00:04:15,590 --> 00:04:20,330
So pizza topping pepperoni pineapple anchovies we have select.

75
00:04:20,360 --> 00:04:27,720
So again a similar dropdown we have selection slider so you can see here as you go.

76
00:04:28,030 --> 00:04:29,410
You get the different options.

77
00:04:29,410 --> 00:04:32,360
So not super popular but it's there for you.

78
00:04:32,470 --> 00:04:34,630
Then the next thing we have is toggle buttons.

79
00:04:34,660 --> 00:04:39,160
So if you run that you could see these different buttons you can toggle slow regular fast.

80
00:04:39,160 --> 00:04:46,220
You see here you have the options you can toggle on and off you can have multiple with select multiple.

81
00:04:46,240 --> 00:04:51,400
So here I can say use shift to select multiple or he can also do control or command.

82
00:04:51,500 --> 00:04:57,070
Depending on your operating system we have stream widgets we have the normal text box which allows you

83
00:04:57,070 --> 00:04:59,150
to type stuff out there.

84
00:04:59,170 --> 00:05:00,610
We'll set text area.

85
00:05:00,610 --> 00:05:03,630
So this one allows you to expand that to a larger area.

86
00:05:05,730 --> 00:05:09,270
We have label widgets which is useful if you need to build some sort of custom description next to a

87
00:05:09,270 --> 00:05:10,740
controller.

88
00:05:10,740 --> 00:05:14,140
So here we can see we're actually using what's known as latex.

89
00:05:14,210 --> 00:05:14,580
Right.

90
00:05:14,610 --> 00:05:16,980
Equals MC squared using latex.

91
00:05:17,220 --> 00:05:20,590
If you're familiar of that you can have that option if you're not familiar with it.

92
00:05:20,610 --> 00:05:24,510
It's not really a big deal just basically allows you to really customize the text here.

93
00:05:24,630 --> 00:05:28,820
But as we just saw a lot of those which is already have a description parameter for that.

94
00:05:28,830 --> 00:05:29,730
Then there's HDMI.

95
00:05:29,760 --> 00:05:34,940
This allows you to basically just put straight HTL here so I can see some a simple Hello World.

96
00:05:35,090 --> 00:05:39,630
Simple math so familiar with a simple math you can put that in as well.

97
00:05:39,870 --> 00:05:45,030
Basically just allowing you to display h m l in case you prefer writing your own rights team here.

98
00:05:45,050 --> 00:05:49,080
There's images in this case we actually don't have this image but you would be able to connect that

99
00:05:49,080 --> 00:05:50,550
to an image and it would see it.

100
00:05:50,550 --> 00:05:55,340
Right now I get file not found because I don't have a pin P and g file there but you would just connect

101
00:05:55,340 --> 00:05:58,880
that and it would display that image for you want to comment this out.

102
00:05:58,900 --> 00:06:04,350
Now that we have the button which is just again click me click off.

103
00:06:04,350 --> 00:06:06,500
Very similar to the button we saw earlier.

104
00:06:06,510 --> 00:06:06,940
OK.

105
00:06:07,110 --> 00:06:09,060
Those are a list of all the widgets.

106
00:06:09,060 --> 00:06:12,390
There's even more which is described in the advanced notebook.

107
00:06:12,420 --> 00:06:15,240
So just use that one as a reference for yourself.

108
00:06:15,240 --> 00:06:16,530
We'll see you at the next lecture.

109
00:06:16,560 --> 00:06:18,390
We're going to be discussing which events.
