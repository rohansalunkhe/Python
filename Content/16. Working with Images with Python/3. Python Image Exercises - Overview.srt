1
00:00:05,200 --> 00:00:06,190
Welcome back, everyone.

2
00:00:06,250 --> 00:00:08,650
It's time to test your skills with an exercise.

3
00:00:08,800 --> 00:00:12,700
So this lecture will just give an overview of what the general exercise is.

4
00:00:13,030 --> 00:00:15,850
This one's a bit more like a puzzle and it's more open ended.

5
00:00:15,850 --> 00:00:17,290
So we won't give you too much guidance.

6
00:00:17,620 --> 00:00:22,060
But if you get stuck or get lost, you can go ahead and check out the Solutions Notebook or the solutions

7
00:00:22,060 --> 00:00:23,380
video, which is up next.

8
00:00:23,410 --> 00:00:28,510
But for now, let's do a little bit of an exploration of that image exercise overview notebook.

9
00:00:28,740 --> 00:00:29,530
I'm going to go to it now.

10
00:00:30,040 --> 00:00:30,220
All right.

11
00:00:30,250 --> 00:00:30,910
Here's the image.

12
00:00:30,940 --> 00:00:32,230
Exercise notebook.

13
00:00:32,560 --> 00:00:33,730
So notice in the folder.

14
00:00:33,850 --> 00:00:37,030
Working with images, which is the same folder this notebook is located in.

15
00:00:37,450 --> 00:00:39,430
There's two images we're going to be working with.

16
00:00:39,910 --> 00:00:41,830
One is called word matrix, Panji.

17
00:00:41,980 --> 00:00:43,810
Another one is called Mask that Panji.

18
00:00:43,840 --> 00:00:46,120
So these are located right here working off images.

19
00:00:46,540 --> 00:00:48,700
There's right here word matrix.

20
00:00:48,790 --> 00:00:49,750
And then mask.

21
00:00:49,780 --> 00:00:51,350
So these are the two we'll be working with.

22
00:00:52,480 --> 00:00:55,990
And the word matrix is a Panji image file that essentially looks like this.

23
00:00:56,020 --> 00:00:57,730
It looks like a spreadsheet of words.

24
00:00:58,360 --> 00:01:01,720
And then the mask image file looks like this.

25
00:01:02,260 --> 00:01:08,470
So what we want you to figure out is essentially how can you work with these two images to attempt to

26
00:01:08,470 --> 00:01:12,910
reveal what is essentially a hidden message in this spreadsheet?

27
00:01:13,390 --> 00:01:17,600
A couple of things you got to figure out are how to stack these images on top of each other, whether

28
00:01:17,600 --> 00:01:22,330
you need to work with transparency in order to see through the images, as well as maybe resizing the

29
00:01:22,330 --> 00:01:22,720
images.

30
00:01:22,750 --> 00:01:25,540
It looks like these images currently are not the same size.

31
00:01:25,990 --> 00:01:28,210
So, again, this is a little bit more of a puzzle.

32
00:01:28,270 --> 00:01:28,900
It's open ended.

33
00:01:28,930 --> 00:01:31,960
So I don't want to guide you if steps to give you too much information.

34
00:01:32,140 --> 00:01:35,890
Instead, really let yourself explore and try to figure things out on your own.

35
00:01:36,010 --> 00:01:38,950
As if this was a real world puzzle situation.

36
00:01:39,220 --> 00:01:39,420
OK.

37
00:01:39,490 --> 00:01:41,200
So, again, you have these two images.

38
00:01:41,470 --> 00:01:46,210
There's a secret message that you will be able to reveal if you can figure out how to connect these

39
00:01:46,210 --> 00:01:47,410
two images together.

40
00:01:47,830 --> 00:01:48,730
I'll just leave it at that.

41
00:01:48,910 --> 00:01:51,280
If you get stuck, feel free to move to the next lecture.

42
00:01:51,320 --> 00:01:53,590
We'll will slowly go through the solution process.

43
00:01:54,040 --> 00:01:54,430
Thanks.

44
00:01:54,820 --> 00:01:55,450
And best of luck.
