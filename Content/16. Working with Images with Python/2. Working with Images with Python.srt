1
00:00:05,430 --> 00:00:09,230
Welcome, everyone, to this lecture where we're going to give a general overview of how you can work

2
00:00:09,360 --> 00:00:10,560
images with Python.

3
00:00:11,880 --> 00:00:14,850
So in this lecture, we've been exploring how to work off these images of Python.

4
00:00:15,180 --> 00:00:20,190
And we're going to be using the pillow library for this, which is a fork of the PLL library, which

5
00:00:20,190 --> 00:00:21,990
is the python imaging library.

6
00:00:22,380 --> 00:00:27,180
And the pool library is really nice with his fork because he has a lot of easy to use function calls.

7
00:00:27,270 --> 00:00:30,580
So a lot of functionality is just a simple method call.

8
00:00:31,080 --> 00:00:34,030
So we're going to do is go ahead and install this additional library.

9
00:00:34,770 --> 00:00:39,490
So to install a pool library, simply open up your command line or command prompt or terminal.

10
00:00:39,570 --> 00:00:45,420
If you're a Mac OS or Ubuntu user or Linux user and then just type PIP install pillow directly at your

11
00:00:45,420 --> 00:00:48,480
command line, you can find the official documentation for it.

12
00:00:48,600 --> 00:00:50,460
That pillow that read the docs that I.

13
00:00:50,570 --> 00:00:52,740
Oh, let's open up a notebook and get started.

14
00:00:53,310 --> 00:00:53,580
All right.

15
00:00:53,610 --> 00:00:55,380
Here I am at my Jupiter notebook.

16
00:00:55,710 --> 00:01:00,420
Before we get started, I want to recommend that you actually start a notebook in the same folder that

17
00:01:00,420 --> 00:01:01,200
we're working with.

18
00:01:01,560 --> 00:01:06,150
So under NSD working with Images folder, you'll notice we have our three notebooks and we're going

19
00:01:06,150 --> 00:01:07,770
to be working with throughout this section.

20
00:01:08,220 --> 00:01:12,240
I started a new one called Just Untitled for Now by hitting New Python notebook.

21
00:01:12,690 --> 00:01:17,910
But the reason I recommend this is because there's a lot of PMG and JPEG files, essentially image files

22
00:01:18,210 --> 00:01:19,290
that were going to be working with.

23
00:01:19,410 --> 00:01:24,210
So if you don't have if you don't want to worry about passing in the entire file path because you do

24
00:01:24,210 --> 00:01:25,520
want to get a file, not found error.

25
00:01:25,830 --> 00:01:30,930
Just go ahead and start the notebook in the exact same location underneath this working image folder.

26
00:01:31,110 --> 00:01:35,520
Otherwise, you'll need to provide the full file path for these specific files that we'll be working

27
00:01:35,520 --> 00:01:35,760
with.

28
00:01:36,390 --> 00:01:40,440
And the lecture notebook that goes along, what we're working with here is overview of working with

29
00:01:40,440 --> 00:01:40,980
images.

30
00:01:41,700 --> 00:01:46,440
So two final notes is you should have installed pillow by now, as mentioned previously, if you have

31
00:01:46,440 --> 00:01:46,840
not.

32
00:01:46,860 --> 00:01:49,500
Go ahead and just run PIP, install pillow at your command line.

33
00:01:49,990 --> 00:01:55,260
And the second thing is sometimes when you're working with large images, the Juber notebook may complain

34
00:01:55,290 --> 00:01:56,700
that your data rate is exceeded.

35
00:01:57,090 --> 00:02:02,190
Essentially, it doesn't want to keep sending so much so much information out to your browser because

36
00:02:02,310 --> 00:02:03,570
it's afraid that something's wrong.

37
00:02:03,960 --> 00:02:08,070
So the default data rate limit may be too low for some of the images we're working with.

38
00:02:08,490 --> 00:02:11,730
If you get this error of IO pub data rate exceeded.

39
00:02:12,150 --> 00:02:13,890
Don't worry, it's just a warning.

40
00:02:14,010 --> 00:02:18,840
All you need to do is go ahead and shut down your notebook and restarted at your command line, command

41
00:02:18,840 --> 00:02:21,770
prompt or terminal by running Jupiter or notebook.

42
00:02:21,900 --> 00:02:23,430
And then we add what's known as a flag.

43
00:02:23,830 --> 00:02:28,800
And essentially this fly just says that take their notebook application and set the data rate limit

44
00:02:29,160 --> 00:02:33,420
to something much higher, which is in this case, one times ten to the power of 10.

45
00:02:33,510 --> 00:02:38,640
So essentially maxing out that data rate limit and then it shouldn't be any issue with you showing very

46
00:02:38,640 --> 00:02:40,170
large images in your notebook.

47
00:02:40,500 --> 00:02:45,180
So we'll be working with some images that are large enough to really be desktop background images.

48
00:02:45,240 --> 00:02:50,010
And luckily, the notebook will kind of resize these automatically for us, but it will still display

49
00:02:50,280 --> 00:02:51,600
quite large images itself.

50
00:02:51,870 --> 00:02:56,970
So just wants you to be aware that in case you happen to get this data rate exceeded error, you can

51
00:02:56,970 --> 00:03:01,710
also just Google search IO pub data rate exceeded and it'll link you to a stack overflow, explaining

52
00:03:01,710 --> 00:03:02,550
this in more detail.

53
00:03:03,090 --> 00:03:08,100
But for now, let's hop back to this untitled notebook that we're working with and we can show you how

54
00:03:08,100 --> 00:03:09,810
to open images with Python.

55
00:03:10,260 --> 00:03:10,560
So.

56
00:03:11,590 --> 00:03:13,550
Because we're using the pillow library recall.

57
00:03:13,660 --> 00:03:15,050
It's a fork from the P.

58
00:03:15,280 --> 00:03:15,940
L library.

59
00:03:16,300 --> 00:03:21,030
So even though you install it as pillow, you actually import it with P i.

60
00:03:21,030 --> 00:03:21,240
L.

61
00:03:21,280 --> 00:03:23,200
So from p l in all caps.

62
00:03:23,610 --> 00:03:25,360
We'll import image.

63
00:03:25,780 --> 00:03:28,080
So the image object here class.

64
00:03:28,690 --> 00:03:32,620
And then we're going to do is say image dot open.

65
00:03:33,130 --> 00:03:34,960
And this repass in the file path.

66
00:03:35,140 --> 00:03:39,050
So if I do shift tab here, it's F.P. which stands for File Path.

67
00:03:39,490 --> 00:03:44,400
And we're gonna do here is I'm going to work with the file example that J.

68
00:03:44,410 --> 00:03:47,750
Pen, which recall is located here under working with images.

69
00:03:47,820 --> 00:03:48,340
It's right here.

70
00:03:48,660 --> 00:03:49,530
Example of JPEG.

71
00:03:50,020 --> 00:03:50,860
And we'll sign this.

72
00:03:51,040 --> 00:03:53,380
I'm going to sign this to the variable Mac.

73
00:03:54,730 --> 00:03:57,010
And something to notice here is the type of file.

74
00:03:57,100 --> 00:04:02,180
This is this is a specialized jpeg image file from p i.

75
00:04:02,230 --> 00:04:02,530
L.

76
00:04:02,710 --> 00:04:07,240
So that means there's gonna be various properties, attributes and methods, calls that we can do this.

77
00:04:07,650 --> 00:04:10,390
Now, if you're running this on a Jupiter notebook.

78
00:04:10,750 --> 00:04:15,730
If you simply type Mac Jupiter notebook is actually smart enough to display the image itself.

79
00:04:15,820 --> 00:04:20,650
So it should be just a singular Macintosh computer, kind of one of the older models.

80
00:04:20,980 --> 00:04:22,540
And then against a white background.

81
00:04:22,990 --> 00:04:27,970
If you're running this as a script or in some other I.D., such as PI charm, you will probably need

82
00:04:27,970 --> 00:04:30,940
to do Mac Dot show and run that.

83
00:04:31,450 --> 00:04:32,470
So Jupiter notebook.

84
00:04:32,590 --> 00:04:34,000
You just need to type in the variable.

85
00:04:34,540 --> 00:04:39,970
Other environments you may need to type that show open and close parentheses in order to see the images.

86
00:04:40,060 --> 00:04:41,020
So keep that in mind.

87
00:04:41,640 --> 00:04:41,870
Okay.

88
00:04:42,520 --> 00:04:44,980
Now let's see what attributes this has.

89
00:04:45,610 --> 00:04:52,810
There's image information such as the size and the size is reported back with width and height.

90
00:04:53,380 --> 00:04:54,970
And then it'll also tell you the file name.

91
00:04:55,540 --> 00:04:58,870
We technically already know it, but we can call for file name as an attribute.

92
00:04:59,240 --> 00:05:00,710
We'll tell you it's called the example that J.

93
00:05:00,710 --> 00:05:01,050
Peg.

94
00:05:01,570 --> 00:05:06,850
And if you want some more specific information about its format, you can do format, description.

95
00:05:06,970 --> 00:05:09,660
And I'll tell you exactly the international standard of J.

96
00:05:09,670 --> 00:05:10,310
Peg it is.

97
00:05:11,150 --> 00:05:12,900
OK, so we know how to open a file now.

98
00:05:13,240 --> 00:05:15,130
Let's go ahead and perform some operations on it.

99
00:05:15,610 --> 00:05:17,770
We're gonna go and show you how to crop the image.

100
00:05:17,980 --> 00:05:21,040
Cropping is just grabbing a subsection of the image.

101
00:05:21,910 --> 00:05:27,970
And the way we can do that is by calling the crop method off of our variable, storing the actual image.

102
00:05:28,000 --> 00:05:29,740
We can say, Mac, the crop.

103
00:05:30,780 --> 00:05:36,670
And then you'll notice that it expects a box, which is a four item tuple, defining the left, upper

104
00:05:36,670 --> 00:05:39,160
right and lower pixel coordinates.

105
00:05:39,550 --> 00:05:41,020
This is a little confusing at first.

106
00:05:41,080 --> 00:05:46,000
So let's quickly jump to an example of what those coordinates actually represent.

107
00:05:46,870 --> 00:05:50,770
Let's imagine that we just had a single blue rectangle as our image.

108
00:05:51,970 --> 00:05:59,080
Then what we can do is that the top left corner of this image as zero zero, and then we can make the

109
00:05:59,290 --> 00:06:03,520
x axis go along horizontally and on the vertical axis.

110
00:06:03,550 --> 00:06:04,770
We can call that Y.

111
00:06:05,290 --> 00:06:11,830
So basically now the first two coordinates in that tuple of four are going to be your starting X and

112
00:06:11,830 --> 00:06:16,390
you're starting Y, whereas zero zero is measured from the top left corner of the image.

113
00:06:16,750 --> 00:06:19,810
The next two are W and H for width and height.

114
00:06:19,930 --> 00:06:21,340
And those are also coordinates.

115
00:06:21,760 --> 00:06:24,820
So you would pick the additional two coordinates of with height.

116
00:06:25,300 --> 00:06:30,190
And essentially from these four items, the starting X and Y.

117
00:06:30,580 --> 00:06:35,680
And then the width and height coordinates of the actual pixels themselves.

118
00:06:36,010 --> 00:06:40,570
Then you can end up creating a rectangle and crop out that image.

119
00:06:40,990 --> 00:06:42,230
So let's see this in practice.

120
00:06:42,280 --> 00:06:43,960
Now by jumping back to the notebook.

121
00:06:44,470 --> 00:06:46,960
So off of this, we will say.

122
00:06:48,390 --> 00:06:49,800
Zero, comma zero.

123
00:06:49,920 --> 00:06:51,450
So we'll start at the very top corner.

124
00:06:51,900 --> 00:06:57,090
And then we can say one hundred one hundred for these starting coordinates of that with in height to

125
00:06:57,090 --> 00:06:58,260
be one hundred one hundred.

126
00:06:58,950 --> 00:07:03,110
So when we run that, we basically get the top left corner of that Mach picture.

127
00:07:03,150 --> 00:07:07,050
If we scroll back up, that's essentially this corner up here in the top left.

128
00:07:07,630 --> 00:07:10,950
So the Mac image isn't really a useful demonstration of cropping.

129
00:07:11,340 --> 00:07:12,870
So we have another image for you.

130
00:07:14,040 --> 00:07:15,250
Called PENCIL's.

131
00:07:16,260 --> 00:07:18,490
So we will say equals to image.

132
00:07:18,990 --> 00:07:19,560
Open.

133
00:07:20,590 --> 00:07:22,230
And we will open pencils, J.

134
00:07:22,240 --> 00:07:25,120
Peg, in a particular look at what pencils is.

135
00:07:25,600 --> 00:07:29,650
It's a very large image of some colored pencils on the left hand side.

136
00:07:29,770 --> 00:07:31,150
Everything else is gray.

137
00:07:31,840 --> 00:07:35,080
Let's attempt to grab some of the top pencils from the corner.

138
00:07:35,740 --> 00:07:37,870
In order to do this, we need to know the size of the image.

139
00:07:38,170 --> 00:07:39,940
We'll say pencils size.

140
00:07:40,600 --> 00:07:44,140
We see it's nineteen fifty pixels by thirteen hundred pixels.

141
00:07:44,650 --> 00:07:50,530
So what we're going to do is if we just wanted, for instance, these top few colored pencils, maybe

142
00:07:50,530 --> 00:07:52,690
only up to this orange color pencil.

143
00:07:52,690 --> 00:07:56,470
So the top three, we're going to start off at the top corner.

144
00:07:57,070 --> 00:08:00,430
So we'll say X is zero and Y is zero.

145
00:08:01,630 --> 00:08:03,160
And then the width from there.

146
00:08:03,550 --> 00:08:07,540
Let's go ahead and grab about 10 percent in the Y direction.

147
00:08:08,140 --> 00:08:10,030
So we'll say that the width.

148
00:08:11,040 --> 00:08:18,530
Or actually, this is about a third, so let's say 30 percent in the X direction and 10 percent in the

149
00:08:18,530 --> 00:08:19,250
Y direction.

150
00:08:20,270 --> 00:08:21,440
So that means the with.

151
00:08:22,830 --> 00:08:29,460
Is going to be nineteen fifty divided by three, so we're essentially grabbing 30 percent of the width

152
00:08:29,460 --> 00:08:39,390
here and we'll grab about 10 percent of this height to say H is equal to thirteen hundred, divided

153
00:08:39,390 --> 00:08:39,840
by ten.

154
00:08:42,040 --> 00:08:47,530
Then we will say pencil's crop and then we'll pass on our tuple X Y.

155
00:08:48,510 --> 00:08:49,580
W h.

156
00:08:50,790 --> 00:08:54,150
And here you can see we're able to successfully grab those three.

157
00:08:55,020 --> 00:08:57,660
Now let's imagine wanted to grab pencils from the bottom.

158
00:08:58,110 --> 00:09:00,780
So we wanted to just grab these three pencils right here.

159
00:09:00,900 --> 00:09:01,740
Or maybe these four.

160
00:09:02,770 --> 00:09:09,310
In that case, we'll say X starts at zero, but now Y is going to start all the way.

161
00:09:09,340 --> 00:09:12,700
So starting at zero, Y is going to start all the way down here.

162
00:09:13,420 --> 00:09:17,530
Maybe just a little bit off of the heights or the heights.

163
00:09:17,560 --> 00:09:18,430
Thirteen hundred.

164
00:09:18,880 --> 00:09:23,170
So let's go ahead and say that now if we just want the bottom.

165
00:09:24,150 --> 00:09:25,750
This is for bottom pencils.

166
00:09:26,410 --> 00:09:31,270
We'll start X zero, Y is going to be equal to eleven hundred.

167
00:09:31,690 --> 00:09:35,140
So we're starting X zero Y all the way down here.

168
00:09:35,560 --> 00:09:36,280
Eleven hundred.

169
00:09:36,820 --> 00:09:40,000
And then we're going to go all the way to the bottom for this height.

170
00:09:40,450 --> 00:09:45,790
So now we'll say the height just goes to thirteen hundred and then the width is going to be the same.

171
00:09:45,820 --> 00:09:47,320
Nineteen fifty divided by three.

172
00:09:47,620 --> 00:09:50,140
So we want about 30 percent of that with coming off of that.

173
00:09:51,190 --> 00:09:52,420
So we do this again.

174
00:09:53,110 --> 00:09:57,280
And then if we do the same crop now, we get those bottom colored pencils.

175
00:09:57,700 --> 00:10:00,130
So, again, keep in mind that these are coordinates.

176
00:10:00,370 --> 00:10:05,350
It's not exactly a measurement of width and height, which is why H is equal to thirteen hundred here

177
00:10:05,620 --> 00:10:07,120
instead of some smaller number.

178
00:10:07,210 --> 00:10:11,740
So it's essentially a start finish and then kind of start finish as well.

179
00:10:13,200 --> 00:10:16,960
So let's go back to the Mac photo and see if we can just grab the computer itself.

180
00:10:17,430 --> 00:10:19,950
So if I say, Mac, I have the computer here.

181
00:10:20,340 --> 00:10:21,870
Let's see it size.

182
00:10:21,900 --> 00:10:26,040
So Mac size is nineteen ninety three by twelve fifty seven.

183
00:10:26,550 --> 00:10:33,030
So about the halfway point, we want to grab half halfway here and then a little bit off of it so we

184
00:10:33,030 --> 00:10:40,140
can define halfway as equal to nineteen ninety three divided by two.

185
00:10:41,190 --> 00:10:48,630
And then we'll say that our X position that we're going to start off at is half way minus two hundred

186
00:10:48,630 --> 00:10:55,080
pixels and that the width that we're going to end that is halfway way plus two hundred pixels.

187
00:10:56,240 --> 00:10:57,780
And then we'll say something like wise.

188
00:10:57,830 --> 00:11:00,950
A hundred and you can kind of just guess and check for these numbers.

189
00:11:01,280 --> 00:11:02,990
These are essentially just lucky guesses.

190
00:11:03,120 --> 00:11:05,360
Except H because that's all the way to the end.

191
00:11:06,860 --> 00:11:07,540
And let's try this.

192
00:11:07,550 --> 00:11:07,930
We'll see it.

193
00:11:07,940 --> 00:11:09,020
Mack thought Krupp.

194
00:11:10,470 --> 00:11:13,080
X, y, w, h.

195
00:11:13,830 --> 00:11:18,120
And then here we can see that we were able to successfully grab the computer image.

196
00:11:18,990 --> 00:11:23,010
Now let's discuss copying and pasting images if we want to create a copy.

197
00:11:23,040 --> 00:11:26,010
Well, we can do is reassign that crop.

198
00:11:26,070 --> 00:11:33,000
We can say computer is equal to map crop X, y, w, h.

199
00:11:33,980 --> 00:11:39,410
And they can call the paste method off of this, and then I provide the actual image.

200
00:11:39,500 --> 00:11:44,240
This case, it's computer and then the where the top left corner is going to go.

201
00:11:44,450 --> 00:11:46,880
And that's going to be a tuple of just two coordinates.

202
00:11:47,270 --> 00:11:51,950
So we'll go ahead and put it in the top left corner of the original Mac and then check out the Mac.

203
00:11:51,950 --> 00:11:56,000
Looks like now we are able to copy and paste this to the top left corner.

204
00:11:56,560 --> 00:11:59,900
And you can do this multiple times if you want one further in.

205
00:12:00,350 --> 00:12:02,350
You can find this further.

206
00:12:02,360 --> 00:12:05,030
And so it can say seven, nine, six or something like that.

207
00:12:05,630 --> 00:12:06,320
Run this again.

208
00:12:06,440 --> 00:12:07,250
And now we paste it.

209
00:12:07,280 --> 00:12:08,600
Another piece of that, Mac.

210
00:12:09,110 --> 00:12:13,550
So keep in mind, this is personally affecting the variable, but it's not permanently affecting the

211
00:12:13,550 --> 00:12:15,050
actual image itself.

212
00:12:15,140 --> 00:12:16,880
This is all just happening in memory.

213
00:12:18,200 --> 00:12:23,240
You can also use the resize method to resize an image, so to show you what I mean by that.

214
00:12:23,450 --> 00:12:28,520
If we say Mac signs, we right now have nineteen ninety three by twelve fifty seven.

215
00:12:28,970 --> 00:12:35,030
If we wanted to make it much smaller or stretch and squeeze it, we can say Mac the resize and you can

216
00:12:35,030 --> 00:12:37,310
just choose whatever size you want here.

217
00:12:37,820 --> 00:12:42,290
For instance, if I want to stretch it out or squeeze it, I can say make it three thousand pixels by

218
00:12:42,290 --> 00:12:44,620
five hundred pixels and it will woops.

219
00:12:45,470 --> 00:12:47,270
Let's make sure we pass this in as a tuple.

220
00:12:48,720 --> 00:12:49,200
There we go.

221
00:12:49,290 --> 00:12:53,640
And it will stretch and squeeze to whatever sizes you want, so you can make it twice as big, half

222
00:12:53,640 --> 00:12:54,120
as big.

223
00:12:54,450 --> 00:12:55,290
That's really up to you.

224
00:12:55,350 --> 00:12:57,150
You just set the new size you want.

225
00:12:57,840 --> 00:12:59,520
You can also rotate images.

226
00:12:59,670 --> 00:13:02,910
And that's just done with the rotate method.

227
00:13:02,940 --> 00:13:05,520
So we can say make that rotate.

228
00:13:05,670 --> 00:13:07,200
And you can rotate by 90 degrees.

229
00:13:07,230 --> 00:13:13,380
And then you can see here that we're able to rotate our image by 90 degrees to finish off our discussion

230
00:13:13,380 --> 00:13:14,550
and working off images.

231
00:13:14,880 --> 00:13:17,190
Let's go ahead and take a look at transparency.

232
00:13:18,500 --> 00:13:25,940
Here we can see that a lot of images have a red, green, blue, A or RGV be a system and that stands

233
00:13:25,940 --> 00:13:27,410
for a red, green, blue, alpha.

234
00:13:28,010 --> 00:13:32,780
And all the values of red, green, blue and alpha can go from zero to two hundred fifty five.

235
00:13:33,350 --> 00:13:37,850
If the alpha value is zero, then the image is completely transparent.

236
00:13:38,120 --> 00:13:41,090
If it's two hundred fifty five and the image is completely opaque.

237
00:13:42,800 --> 00:13:48,170
Let's explore this idea of transparency by opening up to images that we have for you.

238
00:13:49,460 --> 00:13:51,530
So these are in your blue mission folders.

239
00:13:51,620 --> 00:13:53,000
We can say image the open.

240
00:13:53,420 --> 00:13:55,780
We have a red color jpeg.

241
00:13:56,240 --> 00:13:57,020
So take a look at it.

242
00:13:57,050 --> 00:13:58,040
It's just a red square.

243
00:13:58,550 --> 00:14:02,840
And we also have a blue jay page for you, which is going to be able to image.

244
00:14:03,930 --> 00:14:07,380
Open and we will open up that blue color PMG.

245
00:14:08,400 --> 00:14:10,130
And now we can see we have a blue there.

246
00:14:10,790 --> 00:14:16,400
So if you want to reset the Alpha on one of these, you can say put Alpha.

247
00:14:16,970 --> 00:14:20,630
And then pass on some integer value from zero to two hundred fifty five.

248
00:14:21,080 --> 00:14:25,010
So if you pass in zero, it means it's completely transparent.

249
00:14:27,240 --> 00:14:30,180
So now I only see white because the background is white.

250
00:14:30,690 --> 00:14:36,720
If I reset the Alpha to be 255, completely opaque and then run this again, then I actually see the

251
00:14:36,720 --> 00:14:37,110
blue.

252
00:14:37,530 --> 00:14:39,930
If I choose somewhere between has to be an integer.

253
00:14:39,990 --> 00:14:45,420
So, for example, 128, then I get back this sort of lighter blue.

254
00:14:45,570 --> 00:14:48,480
Here we can see that some white has done extensive, this blue.

255
00:14:48,720 --> 00:14:56,190
And to give an idea of what that looks like of Fred, let's say red, put Alpha to about 128 and check

256
00:14:56,190 --> 00:14:57,360
out what red looks like now.

257
00:14:57,690 --> 00:14:59,550
Now it looks even a little more pink.

258
00:14:59,940 --> 00:15:03,930
Now you're probably wondering, how can I actually mix these two together?

259
00:15:03,990 --> 00:15:06,300
This blue and this red to get something like purple?

260
00:15:06,780 --> 00:15:12,300
Well, you can do that with the paste functionality, which essentially Paice one image on top of another,

261
00:15:12,870 --> 00:15:16,740
pasting one image on top of another when the top image is completely opaque.

262
00:15:17,040 --> 00:15:19,020
Well, essentially, just hide the image underneath.

263
00:15:19,380 --> 00:15:24,810
But since we've added transparency with this put Alpha, we will actually be able to see basically half

264
00:15:24,930 --> 00:15:27,030
of the bottom image and half of the top image.

265
00:15:27,690 --> 00:15:28,890
Let's go ahead and do that.

266
00:15:29,760 --> 00:15:36,150
If you say blue paste and you ship tab here, it basically takes the current image object that you want

267
00:15:36,150 --> 00:15:41,850
to paste on top D-Box, which essentially how you want to line this up if you want to light it up to

268
00:15:41,850 --> 00:15:43,440
the center, to the top left.

269
00:15:44,340 --> 00:15:47,730
So essentially the same kind of X Y mapping that we saw earlier.

270
00:15:48,060 --> 00:15:52,320
And then the mask and the mask should essentially be the same as the image.

271
00:15:52,580 --> 00:15:53,730
If you want more details on this.

272
00:15:53,790 --> 00:15:55,260
You can check out the entire Doch string.

273
00:15:55,530 --> 00:15:59,310
But essentially the way to remember this is what is the image you want?

274
00:15:59,550 --> 00:16:02,580
So the image I want to put on top of blue is red.

275
00:16:03,480 --> 00:16:06,090
How do I want to align it based off that top left?

276
00:16:06,150 --> 00:16:12,300
So we'll say box is equal to zero zero, essentially align it with the top left corner alignment and

277
00:16:12,300 --> 00:16:14,470
the box parameter become important.

278
00:16:14,580 --> 00:16:18,370
If you have two different size images, but in our case, red and blue are the same.

279
00:16:18,390 --> 00:16:21,630
So we'll just set it at zero zero and then mask.

280
00:16:21,870 --> 00:16:23,970
It's going to be the same as this image, which is red.

281
00:16:24,330 --> 00:16:29,340
So we have images equal to red box lined up on the top left corner and then Mesko to red.

282
00:16:29,710 --> 00:16:30,570
Go ahead and run this.

283
00:16:31,320 --> 00:16:33,150
And then check out what blue looks like.

284
00:16:33,310 --> 00:16:38,330
This is all happening in place and we get something that essentially should look purplish.

285
00:16:39,130 --> 00:16:39,330
Okay.

286
00:16:39,960 --> 00:16:45,360
So we just got back an image that looks more purple, which means transparency, masking can actually

287
00:16:45,360 --> 00:16:46,600
be kind of more complex.

288
00:16:46,600 --> 00:16:48,660
So we've shown here is a really simple example.

289
00:16:48,960 --> 00:16:52,890
You can check out the read the Docs pillow documentation link that we discussed earlier.

290
00:16:53,190 --> 00:16:57,930
But for most use cases, just knowing your images and then where you want to play some should be enough,

291
00:16:58,080 --> 00:17:00,990
depending on what the exact situation you're trying to achieve is.

292
00:17:01,770 --> 00:17:02,010
All right.

293
00:17:02,160 --> 00:17:03,900
Finally, let's show you how to save this image.

294
00:17:04,200 --> 00:17:07,140
Imagine you really like this new kind of purplish object.

295
00:17:07,560 --> 00:17:12,870
Then you simply say blue save and then choose the full file path.

296
00:17:12,990 --> 00:17:16,920
Or if you want to save it at this location, just whatever you want to call the image.

297
00:17:16,950 --> 00:17:20,630
So in this case, we'll call it Purple Panji and keep mine.

298
00:17:20,700 --> 00:17:23,870
You could also do something like see users, et cetera.

299
00:17:24,030 --> 00:17:29,010
So wherever you want to save it, you can pass that full file path and then I'm going to save it as

300
00:17:29,010 --> 00:17:29,820
purple Panji.

301
00:17:30,150 --> 00:17:31,590
Let's go ahead and make sure that worked.

302
00:17:31,800 --> 00:17:35,010
I'll say purple is equal to image open.

303
00:17:35,100 --> 00:17:37,930
And let's open up that new file, Panji.

304
00:17:38,590 --> 00:17:39,540
Check out Purple.

305
00:17:39,930 --> 00:17:41,040
And there it is.

306
00:17:41,460 --> 00:17:48,330
Keep in mind that running save like this will immediately overwrite whatever file is called Purple Dot

307
00:17:48,330 --> 00:17:48,880
Panji.

308
00:17:49,320 --> 00:17:50,460
It'll create the new file.

309
00:17:50,480 --> 00:17:51,450
If it doesn't exist yet.

310
00:17:51,750 --> 00:17:57,460
But if Purple Panji already exists, then saving it with this file path will actually cause an override.

311
00:17:57,690 --> 00:17:58,650
So just keep that in mind.

312
00:17:59,340 --> 00:17:59,580
Okay.

313
00:18:00,210 --> 00:18:00,810
Great work.

314
00:18:01,020 --> 00:18:03,390
Now it's time to test your new skills with an exercise.

315
00:18:03,600 --> 00:18:04,590
I'll see you at the next lecture.
