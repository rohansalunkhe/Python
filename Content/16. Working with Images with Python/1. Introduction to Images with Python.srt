1
00:00:05,290 --> 00:00:08,350
Welcome, everyone, to the section of the course of working with images.

2
00:00:09,410 --> 00:00:13,580
In this section, we're going to focus on learning how to work with images with the pillow library.

3
00:00:13,980 --> 00:00:16,020
We're going to install it with PIP install pillow.

4
00:00:16,080 --> 00:00:19,770
So if you want, I can take the time to go to your command line and just type PIP install pillow to

5
00:00:19,770 --> 00:00:20,100
run it.

6
00:00:20,700 --> 00:00:24,960
And then we're going to do is show you in the notebook how to open and save image files with this pillow

7
00:00:24,960 --> 00:00:30,600
library, as well as interactive images such as grabs sections of an image or have an image overlap

8
00:00:30,600 --> 00:00:33,060
with another or have transparency in an image.

9
00:00:33,600 --> 00:00:34,260
Let's get started.

10
00:00:34,530 --> 00:00:35,490
I'll see you at the next lecture.
