1
00:00:05,370 --> 00:00:06,690
Welcome, everyone, to this lecture.

2
00:00:06,720 --> 00:00:10,110
Well, we're going to go through the solution for the image exercise puzzle.

3
00:00:10,620 --> 00:00:11,280
Let's get started.

4
00:00:11,700 --> 00:00:11,930
All right.

5
00:00:11,940 --> 00:00:14,070
Here I am at the exercise notebook.

6
00:00:14,460 --> 00:00:18,810
Recall that we essentially gave you these two images and left it up to you to figure out how to reveal

7
00:00:18,870 --> 00:00:19,860
the secret message.

8
00:00:20,280 --> 00:00:25,530
I'm going to just delete these cells and then start off by importing both images.

9
00:00:26,220 --> 00:00:28,980
So I will say from PLL import.

10
00:00:30,590 --> 00:00:31,080
Image.

11
00:00:31,760 --> 00:00:38,020
And now let's actually open up those images first through the words, which is image dot open.

12
00:00:38,630 --> 00:00:44,750
And since I'm located in the same folder as that Panji file, I'll simply say word matrix that Panji,

13
00:00:45,050 --> 00:00:47,300
you should be able to just tab autocomplete this.

14
00:00:48,140 --> 00:00:50,990
And then the next one I'll do is the mask file.

15
00:00:51,410 --> 00:00:53,980
So I will say MASC is equal to image the open.

16
00:00:54,830 --> 00:00:56,540
And this one is Mask PMG.

17
00:00:57,080 --> 00:00:57,920
And I can check them out.

18
00:00:58,550 --> 00:00:59,570
Here are the words.

19
00:01:00,320 --> 00:01:02,210
And then here is the mask.

20
00:01:02,900 --> 00:01:08,240
So if I try to just directly paste the mask on top of the words, it won't work.

21
00:01:08,300 --> 00:01:09,380
And let me show you why.

22
00:01:09,890 --> 00:01:17,030
Because even if I just say words that paste mask with the box at 00.

23
00:01:18,160 --> 00:01:18,830
And the mask.

24
00:01:20,090 --> 00:01:21,200
And then check out words.

25
00:01:22,460 --> 00:01:23,540
Well, I have a problem.

26
00:01:23,780 --> 00:01:25,640
One is they're not the same size.

27
00:01:25,670 --> 00:01:26,990
So it didn't really reveal everything.

28
00:01:27,380 --> 00:01:29,630
And the other one is that words is completely opaque.

29
00:01:29,660 --> 00:01:30,710
There's no transparency.

30
00:01:31,160 --> 00:01:32,900
So that means there's two things I first thing to do.

31
00:01:32,960 --> 00:01:35,510
I need to resize one of these to fit the other.

32
00:01:35,870 --> 00:01:41,750
It probably makes the most sense to resize the mask because I'll probably get some errors.

33
00:01:41,810 --> 00:01:46,220
Well, not really errors, but it might blur the words if I tried to shrink them too small.

34
00:01:46,640 --> 00:01:51,700
So it makes more sense to enlarge the mask to the size of the word matrix.

35
00:01:52,130 --> 00:01:57,110
Then the second thing I need to do is actually add transparency to the mask in order to see through

36
00:01:57,110 --> 00:02:00,800
it, to see what words are being highlighted in these white boxes.

37
00:02:01,280 --> 00:02:06,290
So let's go ahead and do that first, because I accidentally kind of transformed the words.

38
00:02:06,650 --> 00:02:12,560
Let's come back here and re-import both of them to make sure where it's working and a mask is working.

39
00:02:12,650 --> 00:02:13,130
Perfect.

40
00:02:13,550 --> 00:02:17,210
Let's go ahead then and resize mask to the size of words.

41
00:02:18,230 --> 00:02:21,600
To do this or simply check what size words is.

42
00:02:21,750 --> 00:02:23,040
So check word size.

43
00:02:23,550 --> 00:02:25,110
It's ten, fifteen by five.

44
00:02:25,110 --> 00:02:25,670
Fifty nine.

45
00:02:25,830 --> 00:02:32,370
So I will take my mask and set it equal to the resized version of 10-15.

46
00:02:33,340 --> 00:02:34,000
Five, five, nine.

47
00:02:35,290 --> 00:02:38,250
And now my mask size is the same as the words.

48
00:02:38,950 --> 00:02:40,480
Next, I need to add transparency.

49
00:02:40,740 --> 00:02:47,020
So a mask that put Alpha and really can put this kind of it's up to you.

50
00:02:47,050 --> 00:02:48,550
How much Alpha you want to put in.

51
00:02:48,820 --> 00:02:51,160
But I found 200 was a value that worked for me.

52
00:02:51,760 --> 00:02:54,780
So if I take a look at MASC now, you should notice it looks a little grayer.

53
00:02:55,120 --> 00:02:57,820
So this should be enough alpha for me to see the words through it.

54
00:02:58,360 --> 00:03:00,440
So let's try that pasting again.

55
00:03:00,580 --> 00:03:01,360
I'll say words.

56
00:03:02,200 --> 00:03:03,820
Paste the mask.

57
00:03:04,710 --> 00:03:09,190
Line up at the top left corner mask again an alley.

58
00:03:09,260 --> 00:03:10,110
Check out the words.

59
00:03:10,950 --> 00:03:11,570
We run that.

60
00:03:11,880 --> 00:03:12,600
And here's the result.

61
00:03:12,840 --> 00:03:16,320
So you should have been able to see great work with images.

62
00:03:16,440 --> 00:03:17,820
You are the best.

63
00:03:17,860 --> 00:03:19,260
So those are the highlighted words.

64
00:03:19,670 --> 00:03:19,930
OK.

65
00:03:20,340 --> 00:03:21,200
I hope you enjoyed this one.

66
00:03:21,210 --> 00:03:24,360
It's a little more like a puzzle than just kind of a guided exercise.

67
00:03:24,720 --> 00:03:27,140
If you have any questions, feel free to post the Q&amp;A forms.

68
00:03:27,510 --> 00:03:28,950
If not, we'll see you at the next lecture.

69
00:03:29,250 --> 00:03:29,640
Thanks.
